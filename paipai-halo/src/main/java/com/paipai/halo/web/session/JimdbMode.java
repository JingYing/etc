package com.paipai.halo.web.session;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import com.jd.jim.cli.Cluster;

/**
 *TODO 待测
 */
public class JimdbMode implements MockSession {
	
	private HttpServletRequest req;
	private Cluster jimClient;
	private ObjectSerializer objSerializer;
	
	public JimdbMode(HttpServletRequest req, Cluster jimClient, ObjectSerializer objSerializer){
		this.req = req;
		this.jimClient = jimClient;
		this.objSerializer = objSerializer;
	}
	
	@Override
	public void set(String key, Object value) {
		Map<String, Object> container = getContainer();
		if(container == null)
			container = new HashMap<String,Object>();
		container.put(key, value);
		serializeContainer(container);
	}
	
	@Override
	public Object get(String key) {
		Map<String, Object> container = getContainer();
		if(container == null)
			return null;
		else	{
			return container.get(key);
		}
		//TODO 是否在每次get时续期?避免MockSession过期.
	}
	

	@Override
	public void remove(String... key) {
		Map<String, Object> container = getContainer();
		if(container != null)	{
			for(String s : key)	{
				container.remove(s);
			}
		}
		serializeContainer(container);
	}

	private byte[] encodeKey(String key)	{
		try {
			return key == null ? null : key.getBytes("utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Map<String,Object> getContainer()	{
		byte[] cacheKey = encodeKey("session/" + req.getSession().getId());
		byte[] bs = jimClient.get(cacheKey);
		if(bs == null)	
			return null;
		else 
			return (Map<String,Object>)objSerializer.deserialize(bs);
	}
	
	private void serializeContainer(Map<String,Object> container)	{
		jimClient.setEx(encodeKey("session/" + req.getSession().getId()), 
				objSerializer.serialize(container), 24, TimeUnit.HOURS);
	}



}
