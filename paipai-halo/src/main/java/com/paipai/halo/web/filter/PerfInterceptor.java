package com.paipai.halo.web.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.paipai.halo.client.log.ReqInfoContainer;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.login.TokenUtil;
import com.paipai.halo.common.ump.Monitor;
import com.paipai.halo.common.ump.UmpKeyGen;

/**
 * controllern性能日志,记录到controller.log, 并上报至ump
 */
public class PerfInterceptor implements HandlerInterceptor{
	
	static Logger log = LogManager.getLogger();
	static final String LOCALHOST_IP = "0:0:0:0:0:0:0:1";
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String key = UmpKeyGen.gen1stHttp(request.getRequestURI());
		ReqInfoContainer.set(Monitor.registerInfo(key), 
							System.currentTimeMillis(), 
							parseWid(request), Util.randChar(10));
		return true;
	}
	
	/**
	 * 取参数中的wid, 没有的话解token
	 * @param request
	 * @return
	 */
	private long parseWid(HttpServletRequest request)	{
		long wid = 0;
		String widStr = request.getParameter("wid");
		if(widStr != null)	{
			try {
				wid = Long.parseLong(widStr);
			} catch (NumberFormatException e) {
			}
		} 
		if(wid == 0)	{
			try {
				wid = TokenUtil.getAppToken(request.getParameter("appToken")).getWid();
			} catch (Exception e) {
			}
		}
		return wid;
	}
	
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest req,
			HttpServletResponse response, Object handler, Exception ex) throws Exception {
		
		Object caller = ReqInfoContainer.getUmpCaller();
		//如果不使用SimpleMappingExceptionResolver统一处理错误,req.getAttribute('exception')无值
		if(req.getAttribute("exception") != null || ex != null)	{	
			Monitor.functionError(caller);
		}
		log.debug(genLog(req));
		Monitor.registerInfoEnd(caller);
	}
	
	private String genLog(HttpServletRequest req)	{
		StringBuffer fullUrl = req.getRequestURL();
		String query = Util.paramMapToString(req.getParameterMap(), "utf-8");	//取get和post参数,并把参数还原成utf-8编码
		if(query != null && !"".equals(query))	{
			fullUrl.append("?").append(query);
		}
		return new StringBuilder()
			.append(req.getRequestURI()).append("\t")	       								//url接口名称
			.append(ReqInfoContainer.getRequestId()).append("\t")							//请求id
			.append(System.currentTimeMillis() - ReqInfoContainer.getStartTime()).append("\t")					//花费时间
			.append(getHttpHeadIp(req)).append("\t")										//请求IP
			.append(ReqInfoContainer.getWid()).append("\t")										//用户账号
			.append(req.getMethod()).append(" ")											//GET-POST
			.append(fullUrl.toString()).append("\t")										//完整url
			.toString();
	}
	
	/**
	 * 获得nginx请求头的ip
	 * @param req
	 * @return
	 */
	private String getHttpHeadIp(HttpServletRequest req)	{
		try {
			String ip = req.getHeader("x-forwarded-for");	//京东环境下好像为x-forwarded-for
			if(ip == null)	{
				ip = req.getRemoteAddr();
			}
			if(LOCALHOST_IP.equals(ip))	{
				ip = "127.0.0.1";
			}
			return ip;
		} catch (Exception e) {
			return null;
		}
	}

}
