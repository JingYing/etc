package com.paipai.halo.web;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 向前台输出的标准json格式
 */
public class JsonOutput  {
	private String errCode = "0", retCode = "0", msg = "";
	private JsonElement data = new JsonObject();
	
	@Override
	public String toString()	{
		return toJsonStr();
	}

	public JsonOutput setErrCode(String errCode) {
		this.errCode = errCode;
		return this;
	}
	
	public JsonOutput setErrCode(long errCode) {
		this.errCode = errCode + "";
		return this;
	}

	public JsonOutput setRetCode(String retCode) {
		this.retCode = retCode;
		return this;
	}
	
	public JsonOutput setRetCode(long retCode) {
		this.retCode = retCode+"";
		return this;
	}

	public JsonOutput setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public JsonOutput setData(JsonElement data) {
		this.data = data;
		return this;
	}
	
	public String toJsonStr()	{
		return new GsonBuilder().serializeNulls().create().toJson(this);	//不把GSON放在成员变量中, 否则会被序列化
	}
	
	public JsonElement toJsonTree()	{
		return new GsonBuilder().serializeNulls().create().toJsonTree(this);
	}
}
