package com.paipai.halo.web.session;

/**
 * 序列化java对象
 */
public interface ObjectSerializer {
	
	byte[] serialize(Object o);
	
	Object deserialize(byte[] bytes);

}
