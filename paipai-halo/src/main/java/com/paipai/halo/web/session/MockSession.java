package com.paipai.halo.web.session;


/**
 * 为了在无session的服务器上实现类似session的功能, 封装了session/cookie/redis/jimdb的一致性操作
 * @author JingYing
 * @date 2015年2月3日
 */
public interface MockSession {
	
	
	/**
	 * 向用户容器中存放字符串或对象
	 * cookie模式下:设置是否加密,加密算法,对象序列化方式. 详见mockSessionFactory.
	 * redis/jimdb模式下: 设置对象序列化器,默认使用java对象序列化, 所以被存入的对象要implements Serializable.
	 * 		(TODO)如果redis使用场景多,考虑使用第三方开源工具做序列化
	 * @param value
	 */
	void set(String key, Object value);
	
	/**
	 * 取得对象
	 * @return
	 */
	Object get(String key);
	
	/**
	 * 删除
	 * @param key
	 */
	void remove(String...key);
	
	
	
}
