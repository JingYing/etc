package com.paipai.halo.web.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * 把MockSession绑定到controller的方法参数上
 * <pre>
 * <code>
	public void test(HttpServletRequest request, @SessionBinder MockSession session, String param)	{
		System.out.println(session.getErpUser());
	}
 * </code>
 * </pre>
 * 
 */
public class MockSessionArgResolver implements HandlerMethodArgumentResolver{
	
	static Logger log = LoggerFactory.getLogger(MockSessionArgResolver.class);
	
	private MockSessionFactory mockSessionFactory;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(SessionBinder.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {

       HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
       HttpServletResponse response = webRequest.getNativeResponse(HttpServletResponse.class);
    		   
       MockSession s = mockSessionFactory.create(request, response);
       return s;
    }

	public void setMockSessionFactory(MockSessionFactory mockSessionFactory) {
		this.mockSessionFactory = mockSessionFactory;
	}
}
