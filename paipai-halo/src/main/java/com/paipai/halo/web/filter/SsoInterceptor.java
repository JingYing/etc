package com.paipai.halo.web.filter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.component.configagent.Configs;
import com.paipai.halo.client.ao.WebStubFactory;
import com.paipai.halo.client.ao.login.protocal.ApiGetUserLoginLevelReq;
import com.paipai.halo.client.ao.login.protocal.ApiGetUserLoginLevelResp;
import com.paipai.halo.common.Config;

/**
 * 单点登录拦截器
 */
public class SsoInterceptor implements HandlerInterceptor	{
	static Logger log = LoggerFactory.getLogger(SsoInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		if(Config.isDevEnv())	return true;	//本地开发环境时不用拦截器
		boolean isRedirect = false;
		try {
			String wg_uin = getCookie(request, "wg_uin");
			String wg_skey = getCookie(request, "wg_skey");
			if(StringUtils.isNotBlank(wg_uin)&&(("345433386").equals(wg_uin)||("3115117569").equals(wg_uin)||("541137025").equals(wg_uin))){
				AsynWebStub webStub = (AsynWebStub) WebStubFactory.getWebStub4PaiPai();
				
				webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
				
		        webStub.setUin(Long.parseLong(wg_uin));
		        webStub.setOperator(Long.parseLong(wg_uin));
		       // webStub.setSkey(skey.getBytes());
		        webStub.setSkeyToExtHead(wg_skey);
		        //webStub.setMachineKey(machineKey.getBytes());
		        webStub.setRouteKey(Long.parseLong(wg_uin));
		        //webStub.setIpAndPort("10.213.142.101", 53101);
		        webStub.setDomainId(126);
		        
//		        System.out.println("99999999999999999999");
		        
				long longinlevel = 0;
//				long result = 1;
				ApiGetUserLoginLevelReq req= new ApiGetUserLoginLevelReq();
				req.setUin(Long.parseLong(wg_uin));
				req.setMachineKey("fispweb");
				req.setSource("fispweb");
				req.setScene(0);
				
//				System.out.println("5555555555555555555");
				
				ApiGetUserLoginLevelResp resp = new ApiGetUserLoginLevelResp();
				try {
//					System.out.println("1111111111111111111");
					webStub.invoke(req, resp);
//					result = resp.getResult();
//					System.out.println("222222222222222");
					longinlevel = resp.getLoginLevel();
					System.out.println("--获取用户登录状态，校验是否登录---end--longinlevel:" + longinlevel);
				} catch (Exception e) {
					System.out.println("出错了111");
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				
				if(longinlevel == 0){//未登录
					isRedirect = true;
				}
			} else	{
				isRedirect = true;
			}
			
			System.out.println("-----------------444444444444444444::"+isRedirect);
			request.setAttribute("isRedirect", isRedirect);
			return true;
		} catch (Exception e) {
			System.out.println("出错了222");
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
//		request.setAttribute("isRedirect", true);
		return true;
	}
	
	private String getCookie(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();
		if(cookies != null)	{
	        for (Cookie c : cookies) {
	            if (c.getName().equals(key)) {
	                return c.getValue();
	            }
	        }
		}
        return null;
    }
	
	
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

}
