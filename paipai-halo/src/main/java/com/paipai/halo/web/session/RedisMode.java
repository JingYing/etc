//package com.paipai.halo.web.session;
//
//import java.io.UnsupportedEncodingException;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
//import javax.servlet.http.HttpServletRequest;
//
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//
///**
// */
//public class RedisMode implements MockSession {
//	
//	private HttpServletRequest req;
//	private JedisPool jedisPool;
//	private ObjectSerializer objSerializer;
//	
//	public RedisMode(HttpServletRequest req, JedisPool jedisPool, ObjectSerializer objSerializer){
//		this.req = req;
//		this.jedisPool = jedisPool;
//		this.objSerializer = objSerializer;
//	}
//	
//	@Override
//	public void set(String key, Object value) {
//		Map<String, Object> container = getContainer();
//		if(container == null)
//			container = new HashMap<String,Object>();
//		container.put(key, value);
//		serializeContainer(container);
//	}
//	
//	@Override
//	public Object get(String key) {
//		Map<String, Object> container = getContainer();
//		if(container == null)
//			return null;
//		else	{
//			return container.get(key);
//		}
//		//TODO 是否在每次get时续期?避免MockSession过期.
//	}
//	
//
//	@Override
//	public void remove(String... key) {
//		Map<String, Object> container = getContainer();
//		if(container != null)	{
//			for(String s : key)	{
//				container.remove(s);
//			}
//		}
//		serializeContainer(container);
//	}
//
//	private byte[] encodeKey(String key)	{
//		try {
//			return key == null ? null : key.getBytes("utf-8");
//		} catch (UnsupportedEncodingException e) {
//			throw new RuntimeException(e);
//		}
//	}
//	
//	private Map<String,Object> getContainer()	{
//		byte[] cacheKey = encodeKey("session/" + req.getSession().getId());
//		Jedis jedis = jedisPool.getResource();
//		try {
//			byte[] bs = jedis.get(cacheKey);
//			if(bs == null)	
//				return null;
//			else 
//				return (Map<String,Object>)objSerializer.deserialize(bs);
//		} finally	{
//			jedisPool.returnResource(jedis);
//		}
//	}
//	
//	private void serializeContainer(Map<String,Object> container)	{
//		Jedis jedis = jedisPool.getResource();
//		try {
//			jedis.setex(encodeKey("session/" + req.getSession().getId()), 3600*24, objSerializer.serialize(container));
//		} finally	{
//			jedisPool.returnResource(jedis);
//		}
//	}
//
//
//
//}
