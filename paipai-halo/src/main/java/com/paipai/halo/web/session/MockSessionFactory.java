package com.paipai.halo.web.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jd.jim.cli.Cluster;
import com.paipai.halo.common.exception.ApplicationException;

/**
 * 生成MockSession
 */
public class MockSessionFactory {
	
	private Class<? extends MockSession> defaultMode = CookieMode.class;//默认的mocksession实现方式
	
	private Cluster jimClient;
	private ObjectSerializer jdkSerializerImpl = new JdkSerializerImpl();	//使用java对象序列化
	private CookieEncryptor cookieEncryptor = new DesEncryptor();	//使用3DES加密cookie
	
	/**
	 * 根据配置文件或request创建session/cookie/redis容器
	 * @param request
	 * @return
	 */
	public MockSession create(HttpServletRequest request, HttpServletResponse response)	{
		try {
			if(defaultMode == SessionMode.class)	{
				return new SessionMode(request);
			} else if(defaultMode == JimdbMode.class)	{
				return new JimdbMode(request, jimClient, jdkSerializerImpl);	//使用jdk的对象序列化
			} else if(defaultMode == CookieMode.class)	{
				return new CookieMode(request, response, jdkSerializerImpl, true, cookieEncryptor);	//使用3DES加密cookie
//			} else if(defaultImpl == RedisMode.class)	{
//				return new RedisMode(request, jedisPool, jdkSerializerImpl);	
			} else	{
				throw new ApplicationException("不识别的defaultImpl:" + defaultMode.getName());
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
	
	public void setJimClient(Cluster jimClient)	{
		this.jimClient = jimClient;
	}

//	public void setJedisPool(JedisPool jedisPool) {
//		this.jedisPool = jedisPool;
//	}
}
