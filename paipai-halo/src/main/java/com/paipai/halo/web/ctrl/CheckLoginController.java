package com.paipai.halo.web.ctrl;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.JsonObject;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.web.JsonOutput;

@Controller
@RequestMapping("/checkLogin")
public class CheckLoginController {

	/**
	 * 校验是否登录 uin:登录UIN sk:拍拍登录态sk
	 * 
	 * @return
	 */
	@RequestMapping(value = "/checkLoginForH5")
	public void checkLoginForH5(String wid, String sk, HttpServletResponse resp) {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(wid) || StringUtils.isEmpty(sk)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			} 
			long uinL = 0 ;
			try {
				uinL = Long.parseLong(wid) ;
			} catch (NumberFormatException ex) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			if (!LoginUtil.checkLoginForH5(uinL, sk)) {
				out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
				out.setMsg("用户未登录");
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
		}
		doPrint(resp, out.toJsonStr()) ;
	}

	/**
	 * 校验是否登录appToken:拍拍登录态加密串
	 * 
	 * @return
	 */
	@RequestMapping(value = "/checkLoginForApp")
	public void checkLoginForApp(String appToken, String mk, HttpServletResponse resp) {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(appToken) || StringUtils.isEmpty(mk)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
			if (null == appTokenObj) {
				out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
				out.setMsg("用户未登录");
			} else {
				JsonObject data = new JsonObject();
				data.addProperty("wid", appTokenObj.getWid());
				out.setData(data);
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
		}
		doPrint(resp, out.toJsonStr()) ;
	}
	
	
	/**
	 * 使用servlet方式, 向response写入字符串
	 * @param obj
	 * @return
	 */
	protected String doPrint(HttpServletResponse resp, String result) {
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter pw = null;
		try {
			pw = resp.getWriter();
			pw.print(result);// 不要使用println,防止客户端接收到/r/n
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			pw.close();
		}
		return null;
	}


}
