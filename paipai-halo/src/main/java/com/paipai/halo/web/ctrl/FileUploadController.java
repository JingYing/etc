package com.paipai.halo.web.ctrl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.image.common.ImageUpload;
import com.paipai.halo.common.Config;
import com.paipai.halo.web.JsonOutput;

@Controller
@RequestMapping("/fileUpload")
public class FileUploadController {
	
	public static Logger run = LogManager.getLogger("run");


	/**
	 * 图片上传--单张
	 * 数据放到请求body体中
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "/imgUpload")
	@ResponseBody
	public String imgUpload(HttpServletRequest request, HttpServletResponse response) {
		JsonOutput output = new JsonOutput();
		try {
			ByteArrayOutputStream out=new ByteArrayOutputStream(1024);  
			InputStream in = request.getInputStream();
			byte[] temp=new byte[1024];  
	        
	        int size=0;  
	          
	        while((size=in.read(temp))!=-1)  
	        {  
	            out.write(temp,0,size);  
	        }  
	          
	        in.close();  
	        byte[] paramArrayOfByte = out.toByteArray();  
	        run.debug("bytes size got is:" + paramArrayOfByte.length);  
	        out.close();
			
	        
			FileOutputStream fileOutputStream = null;
			String realPath = request.getSession().getServletContext().getRealPath(File.separator + "WEB-INF" + File.separator + "upload");
			File file = new File(realPath);
			if(!file.exists()){
				file.mkdirs();
			}
			String fileName = realPath + File.separator + UUID.randomUUID() + ".jpg";
			
            fileOutputStream = new FileOutputStream(fileName);
            fileOutputStream.write(paramArrayOfByte);
            if(null != fileOutputStream)fileOutputStream.close();
            
			String result = ImageUpload.uploadFile(new File(fileName), ImageUpload.getAucoode());
			JsonParser parser = new JsonParser();
			JsonElement parse = parser.parse(result);
//			JsonArray asJsonArray = parse.getAsJsonArray();
//			for (JsonElement jsonElement : asJsonArray) {
//				JsonObject asJsonObject = jsonElement.getAsJsonObject();
//				if(asJsonObject.get("id").getAsInt() == 1){//成功
//					asJsonObject.addProperty("preUrl", ImageUpload.getUrl());
//				}
//			}
			output.setData(parse);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return doPrint(response, new Gson().toJson(output));
	}
	

	protected String doPrint(HttpServletResponse resp, String result) {
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter pw = null;
		try {
			pw = resp.getWriter();
			pw.print(result);// 不要使用println,防止客户端接收到/r/n
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			pw.close();
		}
		return null;
	}
	
}
