package com.paipai.halo.web.ctrl.cms;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * 首页
 * @author JingYing
 * @date 2015年3月25日
 */
@Controller
@RequestMapping("cms/main")
public class MainController extends CmsBaseController{
	
	@RequestMapping("index")
	public ModelAndView index()	{
		return new ModelAndView("/frame/index");
	}
	
	@RequestMapping("left")
	public ModelAndView left()	{
		JsonArray array = new JsonArray();
		
		JsonObject j1 = new JsonObject();
		j1.addProperty("name", "用户评论管理");
		j1.addProperty("rightUrl", "cms/comment/index.action");
		j1.addProperty("isParent", false);
		
		array.add(j1);
		return new ModelAndView("/frame/left").addObject("nodes", array.toString());
	}
	
	@RequestMapping("top")
	public ModelAndView top()	{
		return new ModelAndView("/frame/top");
	}
	
}

