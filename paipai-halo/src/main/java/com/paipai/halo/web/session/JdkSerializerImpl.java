package com.paipai.halo.web.session;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class JdkSerializerImpl implements ObjectSerializer {

	@Override
	public byte[] serialize(Object obj) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			return bos.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (oos != null)
					oos.close();
			} catch (IOException e) {
			}
		}
	}

	@Override
	public Object deserialize(byte[] bs) {
		ByteArrayInputStream bos = new ByteArrayInputStream(bs);
		try {
			ObjectInputStream ios = new ObjectInputStream(bos);
			return ios.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				bos.close();
			} catch (IOException e) {
			}
		}
	}
	
	public static void main(String[] args) {
		byte[] bs = new JdkSerializerImpl().serialize("啊啊啊");
		String bb = (String)new JdkSerializerImpl().deserialize(bs);
		System.out.println(bb);
	}
	
}
