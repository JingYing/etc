package com.paipai.halo.web.ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.domain.login.AppToken;

public class BaseController {

	/**
	 * 使用servlet方式, 向response写入字符串
	 * 
	 * @param obj
	 * @return
	 */
	protected String doPrint(HttpServletResponse resp, String result) {
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter pw = null;
		try {
			pw = resp.getWriter();
			pw.print(result);// 不要使用println,防止客户端接收到/r/n
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			pw.close();
		}
		return null;
	}

	/**
	 * 判断是否登陆
	 * @param appToken
	 * @param mk
	 * @param wid
	 * @param skey
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public AppToken checkegLogin(String appToken, String mk, String wid,
			String skey) throws BusinessException, Exception {
		AppToken appTokenResult = new AppToken();
		if (!StringUtils.isEmpty(appToken) && !StringUtils.isEmpty(mk)) {
			AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
			if (null != appTokenObj) {
				appTokenResult = appTokenObj;
			} else {
				return null ;
			}
		} else {
			if (StringUtils.isEmpty(wid) || StringUtils.isEmpty(skey)) {
				return null ;
			}
			long widLParam = 0L;
			try {
				widLParam = Long.parseLong(wid);
			} catch (Exception ex) {
				return null ;
			}
			if (LoginUtil.checkLoginForH5(widLParam, skey)) {
				appTokenResult.setWid(widLParam);
				appTokenResult.setLsk(skey);
			} else {
				return null ;
			}
		}
		return appTokenResult;
	}

}
