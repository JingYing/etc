 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.DirtyDAO.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *敏感词过滤(单)之请求,utf-8
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class  SenseWordCheck_UReq implements IServiceObject
{
	/**
	 * 待过滤的文本
	 *
	 * 版本 >= 0
	 */
	 private String InText = new String();

	/**
	 * 来源信息
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 请求端IP
	 *
	 * 版本 >= 0
	 */
	 private String Ip = new String();

	/**
	 * 控制项
	 *
	 * 版本 >= 0
	 */
	 private long Control;

	/**
	 * 场景ID
	 *
	 * 版本 >= 0
	 */
	 private long SenceId;

	/**
	 * 过滤级别
	 *
	 * 版本 >= 0
	 */
	 private long Level;

	/**
	 * 输入预留项
	 *
	 * 版本 >= 0
	 */
	 private String InReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(InText);
		bs.pushString(Source);
		bs.pushString(Ip);
		bs.pushUInt(Control);
		bs.pushUInt(SenceId);
		bs.pushUInt(Level);
		bs.pushString(InReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		InText = bs.popString();
		Source = bs.popString();
		Ip = bs.popString();
		Control = bs.popUInt();
		SenceId = bs.popUInt();
		Level = bs.popUInt();
		InReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x60001806L;
	}


	/**
	 * 获取待过滤的文本
	 * 
	 * 此字段的版本 >= 0
	 * @return InText value 类型为:String
	 * 
	 */
	public String getInText()
	{
		return InText;
	}


	/**
	 * 设置待过滤的文本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInText(String value)
	{
		this.InText = value;
	}


	/**
	 * 获取来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取请求端IP
	 * 
	 * 此字段的版本 >= 0
	 * @return Ip value 类型为:String
	 * 
	 */
	public String getIp()
	{
		return Ip;
	}


	/**
	 * 设置请求端IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIp(String value)
	{
		this.Ip = value;
	}


	/**
	 * 获取控制项
	 * 
	 * 此字段的版本 >= 0
	 * @return Control value 类型为:long
	 * 
	 */
	public long getControl()
	{
		return Control;
	}


	/**
	 * 设置控制项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setControl(long value)
	{
		this.Control = value;
	}


	/**
	 * 获取场景ID
	 * 
	 * 此字段的版本 >= 0
	 * @return SenceId value 类型为:long
	 * 
	 */
	public long getSenceId()
	{
		return SenceId;
	}


	/**
	 * 设置场景ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSenceId(long value)
	{
		this.SenceId = value;
	}


	/**
	 * 获取过滤级别
	 * 
	 * 此字段的版本 >= 0
	 * @return Level value 类型为:long
	 * 
	 */
	public long getLevel()
	{
		return Level;
	}


	/**
	 * 设置过滤级别
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLevel(long value)
	{
		this.Level = value;
	}


	/**
	 * 获取输入预留项
	 * 
	 * 此字段的版本 >= 0
	 * @return InReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return InReserve;
	}


	/**
	 * 设置输入预留项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.InReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SenseWordCheck_UReq)
				length += ByteStream.getObjectSize(InText, null);  //计算字段InText的长度 size_of(String)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(Ip, null);  //计算字段Ip的长度 size_of(String)
				length += 4;  //计算字段Control的长度 size_of(uint32_t)
				length += 4;  //计算字段SenceId的长度 size_of(uint32_t)
				length += 4;  //计算字段Level的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(InReserve, null);  //计算字段InReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SenseWordCheck_UReq)
				length += ByteStream.getObjectSize(InText, encoding);  //计算字段InText的长度 size_of(String)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(Ip, encoding);  //计算字段Ip的长度 size_of(String)
				length += 4;  //计算字段Control的长度 size_of(uint32_t)
				length += 4;  //计算字段SenceId的长度 size_of(uint32_t)
				length += 4;  //计算字段Level的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(InReserve, encoding);  //计算字段InReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
