//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.BatchCleanResp.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *过滤结果类
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class DirtyRet  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long dwVersion;

	/**
	 * 过滤返回码
	 *
	 * 版本 >= 0
	 */
	 private long dwResult;

	/**
	 * 过滤后文本
	 *
	 * 版本 >= 0
	 */
	 private String sOutputText = new String();

	/**
	 * 安全中心命中数
	 *
	 * 版本 >= 0
	 */
	 private long dwApiHitNum;

	/**
	 * 黑名单命中数
	 *
	 * 版本 >= 0
	 */
	 private long dwBlackHitNum;

	/**
	 * 白名单命中数
	 *
	 * 版本 >= 0
	 */
	 private long dwWhiteHitNum;

	/**
	 * anxiXSS是否命中
	 *
	 * 版本 >= 0
	 */
	 private long dwIsAntiXSSHit;

	/**
	 * 用于扩展的字段
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> MapExt = new HashMap<String,String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(dwVersion);
		bs.pushUInt(dwResult);
		bs.pushString(sOutputText);
		bs.pushUInt(dwApiHitNum);
		bs.pushUInt(dwBlackHitNum);
		bs.pushUInt(dwWhiteHitNum);
		bs.pushUInt(dwIsAntiXSSHit);
		bs.pushObject(MapExt);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		dwVersion = bs.popUInt();
		dwResult = bs.popUInt();
		sOutputText = bs.popString();
		dwApiHitNum = bs.popUInt();
		dwBlackHitNum = bs.popUInt();
		dwWhiteHitNum = bs.popUInt();
		dwIsAntiXSSHit = bs.popUInt();
		MapExt = (Map<String,String>)bs.popMap(String.class,String.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return dwVersion value 类型为:long
	 * 
	 */
	public long getDwVersion()
	{
		return dwVersion;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwVersion(long value)
	{
		this.dwVersion = value;
	}


	/**
	 * 获取过滤返回码
	 * 
	 * 此字段的版本 >= 0
	 * @return dwResult value 类型为:long
	 * 
	 */
	public long getDwResult()
	{
		return dwResult;
	}


	/**
	 * 设置过滤返回码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwResult(long value)
	{
		this.dwResult = value;
	}


	/**
	 * 获取过滤后文本
	 * 
	 * 此字段的版本 >= 0
	 * @return sOutputText value 类型为:String
	 * 
	 */
	public String getSOutputText()
	{
		return sOutputText;
	}


	/**
	 * 设置过滤后文本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSOutputText(String value)
	{
		this.sOutputText = value;
	}


	/**
	 * 获取安全中心命中数
	 * 
	 * 此字段的版本 >= 0
	 * @return dwApiHitNum value 类型为:long
	 * 
	 */
	public long getDwApiHitNum()
	{
		return dwApiHitNum;
	}


	/**
	 * 设置安全中心命中数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwApiHitNum(long value)
	{
		this.dwApiHitNum = value;
	}


	/**
	 * 获取黑名单命中数
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBlackHitNum value 类型为:long
	 * 
	 */
	public long getDwBlackHitNum()
	{
		return dwBlackHitNum;
	}


	/**
	 * 设置黑名单命中数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBlackHitNum(long value)
	{
		this.dwBlackHitNum = value;
	}


	/**
	 * 获取白名单命中数
	 * 
	 * 此字段的版本 >= 0
	 * @return dwWhiteHitNum value 类型为:long
	 * 
	 */
	public long getDwWhiteHitNum()
	{
		return dwWhiteHitNum;
	}


	/**
	 * 设置白名单命中数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwWhiteHitNum(long value)
	{
		this.dwWhiteHitNum = value;
	}


	/**
	 * 获取anxiXSS是否命中
	 * 
	 * 此字段的版本 >= 0
	 * @return dwIsAntiXSSHit value 类型为:long
	 * 
	 */
	public long getDwIsAntiXSSHit()
	{
		return dwIsAntiXSSHit;
	}


	/**
	 * 设置anxiXSS是否命中
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwIsAntiXSSHit(long value)
	{
		this.dwIsAntiXSSHit = value;
	}


	/**
	 * 获取用于扩展的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return MapExt value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getMapExt()
	{
		return MapExt;
	}


	/**
	 * 设置用于扩展的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setMapExt(Map<String,String> value)
	{
		if (value != null) {
				this.MapExt = value;
		}else{
				this.MapExt = new HashMap<String,String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DirtyRet)
				length += 4;  //计算字段dwVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段dwResult的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sOutputText, null);  //计算字段sOutputText的长度 size_of(String)
				length += 4;  //计算字段dwApiHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwBlackHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwWhiteHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwIsAntiXSSHit的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(MapExt, null);  //计算字段MapExt的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(DirtyRet)
				length += 4;  //计算字段dwVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段dwResult的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sOutputText, encoding);  //计算字段sOutputText的长度 size_of(String)
				length += 4;  //计算字段dwApiHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwBlackHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwWhiteHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwIsAntiXSSHit的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(MapExt, encoding);  //计算字段MapExt的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
