package com.paipai.halo.dirty;

import java.util.Map;
import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

@HeadApiProtocol(cPlusNamespace = "c2cent::dao::dirty", needInit = true, needLib = false)
public class DirtyDAO {	
	@Member(cPlusNamespace="c2cent::ddo::dirty",desc = "过滤参数类")
	public class DirtyDo {
		@Field(desc = "版本")
		uint32_t dwVersion;
		@Field(desc = "控制位")
		uint32_t dwControl;
		@Field(desc = "过滤级别")
		uint32_t dwLevel;
		@Field(desc = "场景id")
		uint32_t dwSenceId;
		@Field(desc = "过滤文本")
		String sInputText;
		@Field(desc = "用于扩展的字段")
		Map<String,String> MapExt;
	}

	@Member(cPlusNamespace="c2cent::ddo::dirty",desc = "过滤结果类")
	public class DirtyRet {
		@Field(desc = "版本")
		uint32_t dwVersion;
		@Field(desc = "过滤返回码")
		uint32_t dwResult;
		@Field(desc = "过滤后文本")
		String sOutputText;
		@Field(desc = "安全中心命中数")
		uint32_t dwApiHitNum;
		@Field(desc = "黑名单命中数")
		uint32_t dwBlackHitNum;
		@Field(desc = "白名单命中数")
		uint32_t dwWhiteHitNum;
		@Field(desc = "anxiXSS是否命中")
		uint32_t dwIsAntiXSSHit;
		@Field(desc = "用于扩展的字段")
		Map<String,String> MapExt;
	}
	
	/**
	 * 敏感词过滤（单）
	 * 
	 * @author jeanqiang
	 * 
	 * @date Aug 23, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x60001801L", desc = "敏感词过滤(单)")
	class Clean
	{
		/**
		 * 敏感词过滤（单）Req对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60001801L", desc = "敏感词过滤(单)之请求")
		class Req {
			@Field(desc = "来源信息")
			String Source;
			@Field(desc = "请求端IP")
			String Ip;
			@Field(desc = "控制项")
			uint32_t Control;
			@Field(desc = "场景ID")
			uint32_t SenceId;
			@Field(desc = "过滤级别")
			uint32_t Level;
			@Field(desc = "输入预留项")
			String InReserve;
		}
		
		/**
		 * 敏感词过滤（单）Resp对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60008801L", desc = "敏感词过滤(单)之回复")
		class Resp {
			@Field(desc = "要过滤的文本及成功返回过滤后的文本")
			String Text;
			@Field(desc = "安全中心敏感词命中数量")
			uint32_t ApiHitNum;
			@Field(desc = "拍拍黑名单命中数量")
			uint32_t BlackHitNum;
			@Field(desc = "拍拍白名单命中数量")
			uint32_t WhiteHitNum;
			@Field(desc = "是否产生了anti Xss过滤")
			uint32_t AntiXssHit;
			@Field(desc = "输出预留项，用于存储DEBUG信息")
			String OutReserve;
		}
	}

	/**
	 * 获取黑白名单信息（门户PORTAL使用，业务禁用）
	 * 
	 * @author jeanqiang
	 * 
	 * @date Aug 23, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x60001802L", desc = "获取黑白名单信息")
	class GetList
	{
		/**
		 * 获取黑白名单信息Req对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60001802L", desc = "获取黑白名单信息之请求")
		class Req {
			@Field(desc = "来源信息")
			String Source;
			@Field(desc = "输入预留项")
			String InReserve;
			@Field(desc = "控制项")
			uint32_t Control;
		}
		
		/**
		 * 获取黑白名单信息Resp对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60008802L", desc = "获取黑白名单信息之回复")
		class Resp {
			@Field(desc = "黑名单")
			String BlackList;
			@Field(desc = "黑名单更新时间")
			uint32_t BlackTimeStamp;
			@Field(desc = "白名单")
			String WhiteList;
			@Field(desc = "白名单更新时间")
			uint32_t WhiteTimeStamp;
			@Field(desc = "输出预留项")
			String OutReserve;
		}
	}
	
	/**
	 * 更新黑白名单信息（门户PORTAL使用，业务禁用）
	 * 
	 * @author jeanqiang
	 * 
	 * @date Aug 23, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x60001803L", desc = "更新黑白名单信息")
	class UpdateList
	{
		/**
		 * 更新黑白名单信息Req对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60001803L", desc = "更新黑白名单信息之请求")
		class Req {
			@Field(desc = "来源信息")
			String Source;
			@Field(desc = "输入预留项")
			String InReserve;
			@Field(desc = "控制项")
			uint32_t Control;
			@Field(desc = "黑名单")
			String BlackList;
			@Field(desc = "黑名单更新时间")
			uint32_t BlackTimeStamp;
			@Field(desc = "白名单")
			String WhiteList;
			@Field(desc = "白名单更新时间")
			uint32_t WhiteTimeStamp;
		}
		
		/**
		 * 更新黑白名单信息Resp对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60008803L", desc = "更新黑白名单信息之回复")
		class Resp {
			@Field(desc = "输出预留项")
			String OutReserve;
		}
	}
	
	/**
	 * 敏感词过滤（批量）
	 * 
	 * @author jeanqiang
	 * 
	 * @date Aug 23, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x60001804L", desc = "敏感词过滤(批量)")
	class BatchClean
	{
		/**
		 * 敏感词过滤（批量）Req对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60001804L", desc = "敏感词过滤(批量)之请求")
		class Req {
			@Field(desc = "来源信息")
			String Source;
			@Field(desc = "请求端IP")
			String Ip;
			@Field(desc = "过滤参数", cPlusNamespace="DirtyDo|c2cent::ddo::dirty")
			Vector<DirtyDo> DirtyDo;
		}
		
		/**
		 * 敏感词过滤（批量）Resp对象
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 17, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x60008804L", desc = "敏感词过滤(批量)之回复")
		class Resp {
			@Field(desc = "过滤结果信息", cPlusNamespace="DirtyRet|c2cent::ddo::dirty")
			Vector<DirtyRet> DirtyRetDo;
		}
	}

	 /**
	 * 敏感词过滤（单）
	 * 
	 * @author betrolcheng
	 * 
	 * @date july 3, 2011
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x60001805L", desc = "敏感词过滤(单)")
	class SenseWordCheck
	{
		/**
		 * 敏感词过滤（单）Req对象
		 */
		@ApiProtocol(cmdid = "0x60001805L", desc = "敏感词过滤(单)之请求")
		class Req {
			@Field(desc = "来源信息")
			String Source;
			@Field(desc = "请求端IP")
			String Ip;
			@Field(desc = "控制项")
			uint32_t Control;
			@Field(desc = "场景ID")
			uint32_t SenceId;
			@Field(desc = "过滤级别")
			uint32_t Level;
			@Field(desc = "输入预留项")
			String InReserve;
		}
		
		/**
		 * 敏感词过滤（单）Resp对象
		 */
		@ApiProtocol(cmdid = "0x60008805L", desc = "敏感词过滤(单)之回复")
		class Resp {
			@Field(desc = "要过滤的文本及成功返回过滤后的文本")
			String Text;
			@Field(desc = "安全中心敏感词命中数量")
			uint32_t ApiHitNum;
			@Field(desc = "拍拍黑名单命中数量")
			uint32_t BlackHitNum;
			@Field(desc = "拍拍白名单命中数量")
			uint32_t WhiteHitNum;
			@Field(desc = "是否产生了anti Xss过滤")
			uint32_t AntiXssHit;
			@Field(desc = "命中名感词列表")
			Vector<String> vctHitSenseWord;
			@Field(desc = "输出预留项，用于存储DEBUG信息")
			String OutReserve;
		}
	}

	/**
	 * 敏感词过滤（单）, utf-8 接口
	 * 
	 * @author kevenzhou
	 * 
	 * @date july 28, 2011
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x60001806L", desc = "敏感词过滤(单),utf-8")
	class SenseWordCheck_U
	{
		/**
		 * 敏感词过滤（单）Req对象
		 */
		@ApiProtocol(cmdid = "0x60001806L", desc = "敏感词过滤(单)之请求,utf-8")
		class Req {
			@Field(desc = "待过滤的文本")
			String InText;
			@Field(desc = "来源信息")
			String Source;
			@Field(desc = "请求端IP")
			String Ip;
			@Field(desc = "控制项")
			uint32_t Control;
			@Field(desc = "场景ID")
			uint32_t SenceId;
			@Field(desc = "过滤级别")
			uint32_t Level;
			@Field(desc = "输入预留项")
			String InReserve;
		}
		
		/**
		 * 敏感词过滤（单）Resp对象
		 */
		@ApiProtocol(cmdid = "0x60008806L", desc = "敏感词过滤(单)之回复,utf-8")
		class Resp {
			@Field(desc = "过滤后的文本")
			String OutText;
			@Field(desc = "安全中心敏感词命中数量")
			uint32_t ApiHitNum;
			@Field(desc = "拍拍黑名单命中数量")
			uint32_t BlackHitNum;
			@Field(desc = "拍拍白名单命中数量")
			uint32_t WhiteHitNum;
			@Field(desc = "是否产生了anti Xss过滤")
			uint32_t AntiXssHit;
			@Field(desc = "命中名感词列表")
			Vector<String> vctHitSenseWord;
			@Field(desc = "输出预留项，用于存储DEBUG信息")
			String OutReserve;
		}
	}

	/**
 	 * 敏感词过滤(批量), utf-8, 支持灰名单逻辑
	 *
	 *@param String Source: 来源信息 				
	 *@param String Ip: 请求端IP 				
	 *@param Vector DirtyDo: 过滤参数 				
	 *@param Vector DirtyRetDo: 过滤结果信息, 如果命中灰名单, 则dwDwResult为1, 且
	 *                                         在mapMapExt 中返回一条包含敏感词的记录, 
	 *                                         key 为"dirty_str", value 为"xx1|xx2|xx3",  xxn 为敏感词
	 *                                         
     *@return uint32_t : 0(成功) 其它失败
     *
	 */	
	@ApiProtocol(cmdid = "0x60001807L", desc = "敏感词过滤(批量),utf-8")
	class BatchClean_U
	{
		/**
		 * 敏感词过滤（单）Req对象
		 */
		@ApiProtocol(cmdid = "0x60001807L", desc = "敏感词过滤(批量)之请求,utf-8")
		class Req {
			@Field(desc = "来源信息")
			String Source;
			@Field(desc = "请求端IP")
			String Ip;
			@Field(desc = "过滤参数", cPlusNamespace="DirtyDo|c2cent::ddo::dirty")
			Vector<DirtyDo> DirtyDo;
		}
		
		/**
		 * 敏感词过滤（单）Resp对象
		 */
		@ApiProtocol(cmdid = "0x60008807L", desc = "敏感词过滤(单)之回复,utf-8")
		class Resp {
			@Field(desc = "过滤结果信息", cPlusNamespace="DirtyRet|c2cent::ddo::dirty")
			Vector<DirtyRet> DirtyRetDo;
		}
	}
}
