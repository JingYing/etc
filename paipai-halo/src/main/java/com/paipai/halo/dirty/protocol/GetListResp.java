 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.DirtyDAO.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *获取黑白名单信息之回复
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class  GetListResp implements IServiceObject
{
	public long result;
	/**
	 * 黑名单
	 *
	 * 版本 >= 0
	 */
	 private String BlackList = new String();

	/**
	 * 黑名单更新时间
	 *
	 * 版本 >= 0
	 */
	 private long BlackTimeStamp;

	/**
	 * 白名单
	 *
	 * 版本 >= 0
	 */
	 private String WhiteList = new String();

	/**
	 * 白名单更新时间
	 *
	 * 版本 >= 0
	 */
	 private long WhiteTimeStamp;

	/**
	 * 输出预留项
	 *
	 * 版本 >= 0
	 */
	 private String OutReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(BlackList);
		bs.pushUInt(BlackTimeStamp);
		bs.pushString(WhiteList);
		bs.pushUInt(WhiteTimeStamp);
		bs.pushString(OutReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		BlackList = bs.popString();
		BlackTimeStamp = bs.popUInt();
		WhiteList = bs.popString();
		WhiteTimeStamp = bs.popUInt();
		OutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x60008802L;
	}


	/**
	 * 获取黑名单
	 * 
	 * 此字段的版本 >= 0
	 * @return BlackList value 类型为:String
	 * 
	 */
	public String getBlackList()
	{
		return BlackList;
	}


	/**
	 * 设置黑名单
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBlackList(String value)
	{
		this.BlackList = value;
	}


	/**
	 * 获取黑名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return BlackTimeStamp value 类型为:long
	 * 
	 */
	public long getBlackTimeStamp()
	{
		return BlackTimeStamp;
	}


	/**
	 * 设置黑名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBlackTimeStamp(long value)
	{
		this.BlackTimeStamp = value;
	}


	/**
	 * 获取白名单
	 * 
	 * 此字段的版本 >= 0
	 * @return WhiteList value 类型为:String
	 * 
	 */
	public String getWhiteList()
	{
		return WhiteList;
	}


	/**
	 * 设置白名单
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWhiteList(String value)
	{
		this.WhiteList = value;
	}


	/**
	 * 获取白名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WhiteTimeStamp value 类型为:long
	 * 
	 */
	public long getWhiteTimeStamp()
	{
		return WhiteTimeStamp;
	}


	/**
	 * 设置白名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWhiteTimeStamp(long value)
	{
		this.WhiteTimeStamp = value;
	}


	/**
	 * 获取输出预留项
	 * 
	 * 此字段的版本 >= 0
	 * @return OutReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return OutReserve;
	}


	/**
	 * 设置输出预留项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.OutReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetListResp)
				length += ByteStream.getObjectSize(BlackList, null);  //计算字段BlackList的长度 size_of(String)
				length += 4;  //计算字段BlackTimeStamp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(WhiteList, null);  //计算字段WhiteList的长度 size_of(String)
				length += 4;  //计算字段WhiteTimeStamp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(OutReserve, null);  //计算字段OutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetListResp)
				length += ByteStream.getObjectSize(BlackList, encoding);  //计算字段BlackList的长度 size_of(String)
				length += 4;  //计算字段BlackTimeStamp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(WhiteList, encoding);  //计算字段WhiteList的长度 size_of(String)
				length += 4;  //计算字段WhiteTimeStamp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(OutReserve, encoding);  //计算字段OutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
