//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.BatchCleanReq.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *过滤参数类
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class DirtyDo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long dwVersion;

	/**
	 * 控制位
	 *
	 * 版本 >= 0
	 */
	 private long dwControl;

	/**
	 * 过滤级别
	 *
	 * 版本 >= 0
	 */
	 private long dwLevel;

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long dwSenceId;

	/**
	 * 过滤文本
	 *
	 * 版本 >= 0
	 */
	 private String sInputText = new String();

	/**
	 * 用于扩展的字段
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> MapExt = new HashMap<String,String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(dwVersion);
		bs.pushUInt(dwControl);
		bs.pushUInt(dwLevel);
		bs.pushUInt(dwSenceId);
		bs.pushString(sInputText);
		bs.pushObject(MapExt);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		dwVersion = bs.popUInt();
		dwControl = bs.popUInt();
		dwLevel = bs.popUInt();
		dwSenceId = bs.popUInt();
		sInputText = bs.popString();
		MapExt = (Map<String,String>)bs.popMap(String.class,String.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return dwVersion value 类型为:long
	 * 
	 */
	public long getDwVersion()
	{
		return dwVersion;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwVersion(long value)
	{
		this.dwVersion = value;
	}


	/**
	 * 获取控制位
	 * 
	 * 此字段的版本 >= 0
	 * @return dwControl value 类型为:long
	 * 
	 */
	public long getDwControl()
	{
		return dwControl;
	}


	/**
	 * 设置控制位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwControl(long value)
	{
		this.dwControl = value;
	}


	/**
	 * 获取过滤级别
	 * 
	 * 此字段的版本 >= 0
	 * @return dwLevel value 类型为:long
	 * 
	 */
	public long getDwLevel()
	{
		return dwLevel;
	}


	/**
	 * 设置过滤级别
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwLevel(long value)
	{
		this.dwLevel = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSenceId value 类型为:long
	 * 
	 */
	public long getDwSenceId()
	{
		return dwSenceId;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSenceId(long value)
	{
		this.dwSenceId = value;
	}


	/**
	 * 获取过滤文本
	 * 
	 * 此字段的版本 >= 0
	 * @return sInputText value 类型为:String
	 * 
	 */
	public String getSInputText()
	{
		return sInputText;
	}


	/**
	 * 设置过滤文本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSInputText(String value)
	{
		this.sInputText = value;
	}


	/**
	 * 获取用于扩展的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return MapExt value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getMapExt()
	{
		return MapExt;
	}


	/**
	 * 设置用于扩展的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setMapExt(Map<String,String> value)
	{
		if (value != null) {
				this.MapExt = value;
		}else{
				this.MapExt = new HashMap<String,String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DirtyDo)
				length += 4;  //计算字段dwVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段dwControl的长度 size_of(uint32_t)
				length += 4;  //计算字段dwLevel的长度 size_of(uint32_t)
				length += 4;  //计算字段dwSenceId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sInputText, null);  //计算字段sInputText的长度 size_of(String)
				length += ByteStream.getObjectSize(MapExt, null);  //计算字段MapExt的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(DirtyDo)
				length += 4;  //计算字段dwVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段dwControl的长度 size_of(uint32_t)
				length += 4;  //计算字段dwLevel的长度 size_of(uint32_t)
				length += 4;  //计算字段dwSenceId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sInputText, encoding);  //计算字段sInputText的长度 size_of(String)
				length += ByteStream.getObjectSize(MapExt, encoding);  //计算字段MapExt的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
