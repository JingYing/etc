 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.DirtyDAO.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *敏感词过滤(单)之回复
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class  SenseWordCheckResp implements IServiceObject
{
	public long result;
	/**
	 * 要过滤的文本及成功返回过滤后的文本
	 *
	 * 版本 >= 0
	 */
	 private String Text = new String();

	/**
	 * 安全中心敏感词命中数量
	 *
	 * 版本 >= 0
	 */
	 private long ApiHitNum;

	/**
	 * 拍拍黑名单命中数量
	 *
	 * 版本 >= 0
	 */
	 private long BlackHitNum;

	/**
	 * 拍拍白名单命中数量
	 *
	 * 版本 >= 0
	 */
	 private long WhiteHitNum;

	/**
	 * 是否产生了anti Xss过滤
	 *
	 * 版本 >= 0
	 */
	 private long AntiXssHit;

	/**
	 * 命中名感词列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> vctHitSenseWord = new Vector<String>();

	/**
	 * 输出预留项，用于存储DEBUG信息
	 *
	 * 版本 >= 0
	 */
	 private String OutReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(Text);
		bs.pushUInt(ApiHitNum);
		bs.pushUInt(BlackHitNum);
		bs.pushUInt(WhiteHitNum);
		bs.pushUInt(AntiXssHit);
		bs.pushObject(vctHitSenseWord);
		bs.pushString(OutReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		Text = bs.popString();
		ApiHitNum = bs.popUInt();
		BlackHitNum = bs.popUInt();
		WhiteHitNum = bs.popUInt();
		AntiXssHit = bs.popUInt();
		vctHitSenseWord = (Vector<String>)bs.popVector(String.class);
		OutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x60008805L;
	}


	/**
	 * 获取要过滤的文本及成功返回过滤后的文本
	 * 
	 * 此字段的版本 >= 0
	 * @return Text value 类型为:String
	 * 
	 */
	public String getText()
	{
		return Text;
	}


	/**
	 * 设置要过滤的文本及成功返回过滤后的文本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setText(String value)
	{
		this.Text = value;
	}


	/**
	 * 获取安全中心敏感词命中数量
	 * 
	 * 此字段的版本 >= 0
	 * @return ApiHitNum value 类型为:long
	 * 
	 */
	public long getApiHitNum()
	{
		return ApiHitNum;
	}


	/**
	 * 设置安全中心敏感词命中数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setApiHitNum(long value)
	{
		this.ApiHitNum = value;
	}


	/**
	 * 获取拍拍黑名单命中数量
	 * 
	 * 此字段的版本 >= 0
	 * @return BlackHitNum value 类型为:long
	 * 
	 */
	public long getBlackHitNum()
	{
		return BlackHitNum;
	}


	/**
	 * 设置拍拍黑名单命中数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBlackHitNum(long value)
	{
		this.BlackHitNum = value;
	}


	/**
	 * 获取拍拍白名单命中数量
	 * 
	 * 此字段的版本 >= 0
	 * @return WhiteHitNum value 类型为:long
	 * 
	 */
	public long getWhiteHitNum()
	{
		return WhiteHitNum;
	}


	/**
	 * 设置拍拍白名单命中数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWhiteHitNum(long value)
	{
		this.WhiteHitNum = value;
	}


	/**
	 * 获取是否产生了anti Xss过滤
	 * 
	 * 此字段的版本 >= 0
	 * @return AntiXssHit value 类型为:long
	 * 
	 */
	public long getAntiXssHit()
	{
		return AntiXssHit;
	}


	/**
	 * 设置是否产生了anti Xss过滤
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAntiXssHit(long value)
	{
		this.AntiXssHit = value;
	}


	/**
	 * 获取命中名感词列表
	 * 
	 * 此字段的版本 >= 0
	 * @return vctHitSenseWord value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getVctHitSenseWord()
	{
		return vctHitSenseWord;
	}


	/**
	 * 设置命中名感词列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setVctHitSenseWord(Vector<String> value)
	{
		if (value != null) {
				this.vctHitSenseWord = value;
		}else{
				this.vctHitSenseWord = new Vector<String>();
		}
	}


	/**
	 * 获取输出预留项，用于存储DEBUG信息
	 * 
	 * 此字段的版本 >= 0
	 * @return OutReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return OutReserve;
	}


	/**
	 * 设置输出预留项，用于存储DEBUG信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.OutReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SenseWordCheckResp)
				length += ByteStream.getObjectSize(Text, null);  //计算字段Text的长度 size_of(String)
				length += 4;  //计算字段ApiHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段BlackHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段WhiteHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段AntiXssHit的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vctHitSenseWord, null);  //计算字段vctHitSenseWord的长度 size_of(Vector)
				length += ByteStream.getObjectSize(OutReserve, null);  //计算字段OutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SenseWordCheckResp)
				length += ByteStream.getObjectSize(Text, encoding);  //计算字段Text的长度 size_of(String)
				length += 4;  //计算字段ApiHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段BlackHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段WhiteHitNum的长度 size_of(uint32_t)
				length += 4;  //计算字段AntiXssHit的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vctHitSenseWord, encoding);  //计算字段vctHitSenseWord的长度 size_of(Vector)
				length += ByteStream.getObjectSize(OutReserve, encoding);  //计算字段OutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
