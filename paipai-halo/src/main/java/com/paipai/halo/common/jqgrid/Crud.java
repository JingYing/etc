package com.paipai.halo.common.jqgrid;

public interface Crud {
	
	/**
	 * 新增
	 * @param req
	 * @return 向前台输出的字符串
	 */
	String add(JqgridReq req);

	/**
	 * 修改
	 * @param req
	 * @return 向前台输出的字符串
	 */
	String edit(JqgridReq req);
	
	/**
	 * 删除
	 * @param req
	 * @return 向前台输出的字符串
	 */
	String del(JqgridReq req);
	
	/**
	 * 普通查询
	 * @param req
	 * @return 向前台输出的字符串
	 */
	String query(JqgridReq req);

	/**
	 * 搜索
	 * @param req
	 * @return 向前台输出的字符串
	 */
	String search(JqgridReq req);
	
	/**
	 * 复合过滤搜索
	 * @param req
	 * @return 向前台输出的字符串
	 */
	String filterSearch(JqgridReq req);



}
