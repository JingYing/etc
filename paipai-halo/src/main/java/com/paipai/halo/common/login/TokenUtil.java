package com.paipai.halo.common.login;

import java.net.URLEncoder;

import com.paipai.halo.common.ThreeDesUtil;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.domain.login.JDSdkAppToken;
import com.paipai.halo.domain.login.QQSdkAppToken;
import com.paipai.halo.domain.login.WXSdkAppToken;
import com.paipai.halo.domain.login.WtLoginAppToken;


public class TokenUtil {
	public static final String LOGIN_THREEDES_KEY = "jdppxltdwAoimbvy862wzqvy";// 生成token进行des加密的秘钥
	public static final String LOGIN_KEY = "paipai@YPFEwe8eiouFHSAfjauief%&Fas";// 参数加密密钥
	public static final String LOGIN_TYPE_QQ = "qq";// qq opensdk login
	public static final String LOGIN_TYPE_WT = "wt";// wtlgoin
	public static final String LOGIN_TYPE_WT_SK = "wtsk";// wtlgoin 用户wt调用获取sk接口
	public static final String LOGIN_TYPE_JD = "jd";// jd opensdk login
	public static final String LOGIN_TYPE_WX = "wx";// wx openSdk login
	public static final String LOGIN_TYPE_PP = "pp";// paipai登陆态根据WID，SK换取TOKEN
	public static final char TOKEN_PARTITION = 1;//token 各个字段分割符

	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String wtTokenToStr(WtLoginAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLk()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getQq()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()) ;			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static WtLoginAppToken strToWtToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 12 == tokenArr.length) {
			WtLoginAppToken appToken = new WtLoginAppToken();
			appToken.setAppID(tokenArr[0]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[1])) ;
			appToken.setLatitude(tokenArr[2]) ;
			appToken.setLk(tokenArr[3]) ;
			appToken.setLongitude(tokenArr[4]);
			appToken.setLsk(tokenArr[5]) ;
			appToken.setMk(tokenArr[6]) ;
			appToken.setMt(tokenArr[7]) ;
			appToken.setQq(Long.parseLong(tokenArr[8])) ;
			appToken.setSalt(Long.parseLong(tokenArr[9])) ;
			appToken.setVersionCode(tokenArr[10]) ;
			appToken.setWid(Long.parseLong(tokenArr[11])) ;
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}

	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String qqTokenToStr(QQSdkAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAccessToken()).append(TOKEN_PARTITION)
			.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getExpiresIn()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getOpenId()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()) ;			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static QQSdkAppToken strToQqToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 13 == tokenArr.length) {
			QQSdkAppToken appToken = new QQSdkAppToken();
			appToken.setAccessToken(tokenArr[0]) ;
			appToken.setAppID(tokenArr[1]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[2])) ;
			appToken.setExpiresIn(tokenArr[3]);
			appToken.setLatitude(tokenArr[4]) ;
			appToken.setLongitude(tokenArr[5]);
			appToken.setLsk(tokenArr[6]) ;
			appToken.setMk(tokenArr[7]) ;
			appToken.setMt(tokenArr[8]) ;
			appToken.setOpenId(tokenArr[9]) ;
			appToken.setSalt(Long.parseLong(tokenArr[10])) ;
			appToken.setVersionCode(tokenArr[11]) ;
			appToken.setWid(Long.parseLong(tokenArr[12]));
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}
	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String jdTokenToStr(JDSdkAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getA2()).append(TOKEN_PARTITION)
			.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getPin()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()) ;			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static JDSdkAppToken strToJdToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 12 == tokenArr.length) {
			JDSdkAppToken appToken = new JDSdkAppToken();
			appToken.setA2(tokenArr[0]) ;
			appToken.setAppID(tokenArr[1]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[2])) ;
			appToken.setLatitude(tokenArr[3]) ;
			appToken.setLongitude(tokenArr[4]);
			appToken.setLsk(tokenArr[5]) ;
			appToken.setMk(tokenArr[6]) ;
			appToken.setMt(tokenArr[7]) ;
			appToken.setPin(tokenArr[8]) ;
			appToken.setSalt(Long.parseLong(tokenArr[9])) ;
			appToken.setVersionCode(tokenArr[10]) ;
			appToken.setWid(Long.parseLong(tokenArr[11]));
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}
	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String wxTokenToStr(WXSdkAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getCode()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()).append(TOKEN_PARTITION)
			.append(token.getOpenId()).append(TOKEN_PARTITION)
			.append(token.getAccessToken()).append(TOKEN_PARTITION)
			.append(token.getUnionid());			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static WXSdkAppToken strToWxToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 14 == tokenArr.length) {
			WXSdkAppToken appToken = new WXSdkAppToken();
			appToken.setAppID(tokenArr[0]) ;
			appToken.setCode(tokenArr[1]);
			appToken.setDeadline(Long.parseLong(tokenArr[2])) ;
			appToken.setLatitude(tokenArr[3]) ;
			appToken.setLongitude(tokenArr[4]);
			appToken.setLsk(tokenArr[5]) ;
			appToken.setMk(tokenArr[6]) ;
			appToken.setMt(tokenArr[7]) ;
			appToken.setSalt(Long.parseLong(tokenArr[8])) ;
			appToken.setVersionCode(tokenArr[9]) ;
			appToken.setWid(Long.parseLong(tokenArr[10]));
			appToken.setOpenId(tokenArr[11]) ;
			appToken.setAccessToken(tokenArr[12]) ;
			appToken.setUnionid(tokenArr[13]) ;
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}
	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String appTokenToStr(AppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid());
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static AppToken strToAppToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 10 == tokenArr.length) {
			AppToken appToken = new AppToken();
			appToken.setAppID(tokenArr[0]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[1])) ;
			appToken.setLatitude(tokenArr[2]) ;
			appToken.setLongitude(tokenArr[3]);
			appToken.setLsk(tokenArr[4]) ;
			appToken.setMk(tokenArr[5]) ;
			appToken.setMt(tokenArr[6]) ;
			appToken.setSalt(Long.parseLong(tokenArr[7])) ;
			appToken.setVersionCode(tokenArr[8]) ;
			appToken.setWid(Long.parseLong(tokenArr[9]));
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}
	/**
	 * 根据appTokeStr获得appToken对象
	 * @param appTokenStr
	 * @return
	 * @throws Exception
	 */
	public static AppToken getAppToken(String appTokenStr) throws Exception {
		if (appTokenStr.startsWith(TokenUtil.LOGIN_TYPE_WT)) {
			return TokenUtil.strToWtToken(appTokenStr); 
		} else if (appTokenStr.startsWith(TokenUtil.LOGIN_TYPE_JD)) {
			return TokenUtil.strToJdToken(appTokenStr); 
		} else if (appTokenStr.startsWith(TokenUtil.LOGIN_TYPE_QQ)) {
			return TokenUtil.strToQqToken(appTokenStr); 
		} else if (appTokenStr.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
			return TokenUtil.strToWxToken(appTokenStr);			
		} else if (appTokenStr.startsWith(TokenUtil.LOGIN_TYPE_PP)) {
			return TokenUtil.strToAppToken(appTokenStr);			
		} else {
			return null ;
		}
	}

}
