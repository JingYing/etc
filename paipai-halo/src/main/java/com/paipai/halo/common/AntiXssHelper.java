package com.paipai.halo.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 从3rd.jar里反编译
 */
public class AntiXssHelper {
	private static final Map<Character, String> ignoreHtmlElementsTable;
	private static final Map<Character, String> ignoreHtmlAttributesTable;
	private static final Map<Character, String> ignoreXmlElementsTable;
	private static final Map<Character, String> ignoreXmlAttributesTable;
	private static final Pattern urlPattern;
	private static final Set<Character> unFilterChars;

	static {
		ignoreHtmlElementsTable = new HashMap();
		ignoreHtmlElementsTable.put(Character.valueOf('<'), "&lt;");
		ignoreHtmlElementsTable.put(Character.valueOf('>'), "&gt;");
		ignoreHtmlElementsTable.put(Character.valueOf('\''), "&#39;");
		ignoreHtmlElementsTable.put(Character.valueOf('"'), "&quot;");
		ignoreHtmlElementsTable.put(Character.valueOf('&'), "&amp;");

		ignoreHtmlAttributesTable = new HashMap();
		ignoreHtmlAttributesTable.putAll(ignoreHtmlElementsTable);
		ignoreHtmlAttributesTable.put(Character.valueOf('='), "&#61;");
		ignoreHtmlAttributesTable.put(Character.valueOf('`'), "&#96;");

		ignoreXmlElementsTable = new HashMap();
		ignoreXmlElementsTable.putAll(ignoreHtmlElementsTable);
		ignoreXmlElementsTable.put(Character.valueOf('\''), "&apos;");

		ignoreXmlAttributesTable = new HashMap();
		ignoreXmlAttributesTable.putAll(ignoreHtmlAttributesTable);
		ignoreXmlAttributesTable.put(Character.valueOf('\''), "&apos;");

		urlPattern = Pattern.compile("^(ht|f)tp(s?)://");

		unFilterChars = new HashSet();
		unFilterChars.add(Character.valueOf('!'));
		unFilterChars.add(Character.valueOf('#'));
		unFilterChars.add(Character.valueOf('$'));
		unFilterChars.add(Character.valueOf('%'));
		unFilterChars.add(Character.valueOf('&'));
		unFilterChars.add(Character.valueOf('('));
		unFilterChars.add(Character.valueOf(')'));
		unFilterChars.add(Character.valueOf('*'));
		unFilterChars.add(Character.valueOf('+'));
		unFilterChars.add(Character.valueOf(','));
		unFilterChars.add(Character.valueOf('-'));
		unFilterChars.add(Character.valueOf('.'));
		unFilterChars.add(Character.valueOf('/'));
		unFilterChars.add(Character.valueOf(':'));
		unFilterChars.add(Character.valueOf(';'));
		unFilterChars.add(Character.valueOf('='));
		unFilterChars.add(Character.valueOf('?'));
		unFilterChars.add(Character.valueOf('@'));
		unFilterChars.add(Character.valueOf('_'));
	}

	public static String htmlEncode(String source) {
		if (source == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder(source.length() + 32);
		char[] chars = source.toCharArray();

		for (int _ = 0; _ < chars.length; _++) {
			char ch = chars[_];
			String rep = (String) ignoreHtmlElementsTable.get(Character
					.valueOf(ch));
			if (rep != null) {
				sb.append(rep);
			} else {
				sb.append(ch);
			}
		}

		return sb.toString();
	}
	
	public static String htmlAttributeEncode(String source) {
		if (source == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder(source.length() + 32);
		char[] chars = source.toCharArray();

		for (int _ = 0; _ < chars.length; _++) {
			char ch = chars[_];
			String rep = (String) ignoreHtmlAttributesTable.get(Character
					.valueOf(ch));
			if (rep != null) {
				sb.append(rep);
			} else {
				sb.append(ch);
			}
		}

		return sb.toString();
	}

	public static String javaScriptEncode(String source) {
		if (source == null)
			return "";
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < source.length(); i++) {
			char ch = source.charAt(i);

			if (ch > '࿿')
				sb.append(ch);
			else if (ch > 'ÿ')
				sb.append(ch);
			else if (ch > '')
				sb.append(new StringBuilder().append("\\u00").append(hex(ch))
						.toString());
			else if (ch < ' ')
				switch (ch) {
				case '\b':
					sb.append('\\');
					sb.append('b');
					break;
				case '\n':
					sb.append('\\');
					sb.append('n');
					break;
				case '\t':
					sb.append('\\');
					sb.append('t');
					break;
				case '\f':
					sb.append('\\');
					sb.append('f');
					break;
				case '\r':
					sb.append('\\');
					sb.append('r');
					break;
				case '\013':
				default:
					if (ch > '\017')
						sb.append(new StringBuilder().append("\\u00")
								.append(hex(ch)).toString());
					else {
						sb.append(new StringBuilder().append("\\u000")
								.append(hex(ch)).toString());
					}
					break;
				}
			else {
				switch (ch) {
				case '\'':
					sb.append('\\');
					sb.append('\'');
					break;
				case '"':
					sb.append('\\');
					sb.append('"');
					break;
				case '\\':
					sb.append('\\');
					sb.append('\\');
					break;
				default:
					sb.append(ch);
				}
			}
		}

		return sb.toString();
	}

	private static String hex(char ch) {
		return Integer.toHexString(ch).toUpperCase();
	}

	public static String uriComponentEncode(String source) {
		if (source == null) {
			return null;
		}
		try {
			return URLEncoder.encode(source,
					System.getProperty("file.encoding"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String urlFilterAndEncode(String source) {
		if (source == null)
			return "";
		Matcher matcher = urlPattern.matcher(source);
		if (!matcher.find()) {
			source = new StringBuilder().append("http://").append(source)
					.toString();
		}

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < source.length(); i++) {
			char ch = source.charAt(i);
			if (((ch >= '0') && (ch <= ';')) || ((ch >= 'A') && (ch <= 'Z'))
					|| ((ch >= 'a') && (ch <= 'z'))
					|| (unFilterChars.contains(Character.valueOf(ch)))) {
				sb.append(ch);
			} else {
				sb.append("\\x").append(Integer.toHexString(ch));
			}
		}
		return sb.toString();
	}

	public static String xmlEncode(String source) {
		if (source == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder(source.length() + 32);
		char[] chars = source.toCharArray();

		for (int _ = 0; _ < chars.length; _++) {
			char ch = chars[_];
			String rep = (String) ignoreXmlElementsTable.get(Character
					.valueOf(ch));
			if (rep != null) {
				sb.append(rep);
			} else if (((ch < ' ') && (ch != '\t') && (ch != '\n') && (ch != '\r'))
					|| (ch > 65533)) {
				sb.append(' ');
			} else {
				sb.append(ch);
			}

		}

		return sb.toString();
	}

	public static String xmlAttributeEncode(String source) {
		if (source == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder(source.length() + 32);
		char[] chars = source.toCharArray();

		for (int _ = 0; _ < chars.length; _++) {
			char ch = chars[_];
			String rep = (String) ignoreXmlAttributesTable.get(Character
					.valueOf(ch));
			if (rep != null) {
				sb.append(rep);
			} else {
				sb.append(ch);
			}
		}

		return sb.toString();
	}
}
