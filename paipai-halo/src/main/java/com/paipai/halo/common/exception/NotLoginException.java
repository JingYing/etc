package com.paipai.halo.common.exception;

/**
 * 用户未登陆
 */
public class NotLoginException extends RuntimeException{

	public NotLoginException() {
		super();
	}

	public NotLoginException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotLoginException(String message) {
		super(message);
	}

	public NotLoginException(Throwable cause) {
		super(cause);
	}

}
