package com.paipai.halo.common.ump;

/**
 * 按照服务类型,生成UMP方法监控所使用的key, 以便在UMP管理端查找和编辑
 * @author JingYing
 * @date 2015年4月28日
 */
public class UmpKeyGen {
	
	public static String gen1stHttp(String uri)	{
		return "1st:http://" + uri.replaceFirst("/", "");
	}
	
	public static String gen1stDao(String daoName)	{
		return "db:" +  daoName;
	}
	
	public static String gen1stCache(String cacheKeyPrefix)	{
		return "cache:" +  cacheKeyPrefix;
	}
	
	public static String gen3rdAo(String aoDetail)	{
		return "3rd:ao://" + aoDetail;
	}
	
	public static String gen3rdUdp(String address)	{
		return "3rd:udp://" + address;
	}
	
	public static String gen3rdTcp(String address)	{
		return "3rd:tcp://" + address;
	}
	
	public static String gen3rdHttp(String url)	{
		if(url.startsWith("http://") || url.startsWith("https://"))	{
			return "3rd:" + url;
		} else	{
			return "3rd:http://" + url;
		}
	}

}
