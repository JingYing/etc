package com.paipai.halo.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.map.util.StdDateFormat;

/**
 * 时间格式化类
 * @author leishouguo
 *
 */
public class JdStdDateFormat extends SimpleDateFormat {
	private String formatStr;

	public JdStdDateFormat(String formatStr){
		this.formatStr = formatStr;
	}
	/**
	 *
	 */
	private static final long serialVersionUID = 5497209795386981683L;

	public Date parse(String dateStr) throws ParseException {

		 SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
		 try {
			return sdf.parse(dateStr);
		} catch (Exception e) {
			try {
				return StdDateFormat.instance.parse(dateStr);
			} catch (ParseException e1) {
				throw e1;
			}
		}
	 }

}
