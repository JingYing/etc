package com.paipai.halo.common.sign;

public class PrizeRuleManager {

	public static long deductionPoint(int prizeLevel) {

		switch (prizeLevel) {
		case 1:
			return 21L;
		case 2:
			return 43L;
		case 3:
			return 91L;
		case 4:
			return 183L;
		case 5:
			return 241L;
		default:
			return -1L;
		}

	}

	public static long levelLimit(int prizeLevel) {

		switch (prizeLevel) {
		case 1:
			return 685L;
		case 2:
			return 79L;
		case 3:
			return 63L;
		case 4:
			return 46L;
		case 5:
			return 3L;
		default:
			return -1L;
		}

	}

}
