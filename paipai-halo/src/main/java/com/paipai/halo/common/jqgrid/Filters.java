package com.paipai.halo.common.jqgrid;

import java.util.List;

/**
 * jqgrid的搜索过滤器模型 {"groupOp":"AND","rules":[{"field":"id","op":"bw","data":"1"},
 * {"field":"name","op":"bw","data":"quick"},
 * {"field":"osName","op":"bw","data":"android"} ]}
 * 
 * @author JingYing 2013-8-29
 */
public class Filters {
	
	private String groupOp;
	private List<Rule> rules;
	
	public String getGroupOp() {
		return groupOp;
	}

	public void setGroupOp(String groupOp) {
		this.groupOp = groupOp;
	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

}
