package com.paipai.halo.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.paipai.halo.common.Pager;
import com.paipai.halo.common.jqgrid.Rule;
import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.domain.Comment;
/**
  * @ClassName: ShopAroServiceImpl
  * @Description:业务逻辑实现类
  * @author Comsys-lianghaining1
  * @date 2015年3月17日 下午5:20:43
  *
 */
@Component
public class ShopAroCommentDao extends BaseDaoImpl<Comment, Long>  {
	private String namespace = "Comment";
	 
	public ShopAroCommentDao() {
	 super.setNamespace(this.namespace);
	 }

	@SuppressWarnings("rawtypes")
	public Pager<Map> filterSearchBeginWith(List<Rule> rules, String orderField,
			String order, Integer offset, Integer pageSize) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("rules", rules);
		map.put("orderField", orderField);
		map.put("order", order);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		
		List<Map> list = getSqlSession().selectList(namespace + ".filterSearchBeginWith", map);
		Integer count = getSqlSession().selectOne(namespace + ".filterSearchBeginWith-count", map);
		return new Pager<Map>(list, count);
	}
	

	public void updateIsshow(List<Long> ids, Integer isshow) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("ids", ids);
		map.put("isshow", isshow);
		getSqlSession().update(namespace + ".updateIsshow", map);
	}

	public Pager<Map> find(String orderField, String order, Integer offset, Integer pageSize) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("orderField", orderField);
		map.put("order", order);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Map> list = getSqlSession().selectList(namespace + ".find", map);
		Integer count = getSqlSession().selectOne(namespace + ".find-count", map);
		return new Pager<Map>(list, count);
	}

	public List<Comment> selectByidParam(Map<String, Object> map) {
		return getSqlSession().selectList(namespace + ".selectByidParam", map);
	}
	
	/**
	 * 
	  * @Title:根据用户id查出该人所有的评论
	  * @Description: TODO
	  * @param @param map
	  * @param @return    设定文件
	  * @return List<Comment>    返回类型
	  * @throws
	 */
	public List<Comment> selectBywidParam(Map<String, Object> map){
		return getSqlSession().selectList(namespace + ".selectBywidParam", map);
	
	}

}
