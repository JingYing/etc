/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.dao.paipaisign.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.dao.paipaisign.PpappBehaviorInfoDao;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfo;

/**
 * PpappBehaviorInfoDao 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Repository("ppappBehaviorInfoDao")
public class PpappBehaviorInfoDaoImpl extends BaseDaoImpl<PpappBehaviorInfo,String> implements PpappBehaviorInfoDao {
	private final static String NAMESPACE = "com.paipai.halo.dao.paipaisign.PpappBehaviorInfoDao";
	private final static String COUNT_PRIZE_SEND_NUM = ".selectEntryListCountByMap" ;
	
	public PpappBehaviorInfoDaoImpl() {
		 super.setNamespace(NAMESPACE);
	}
	
	//返回本DAO命名空间,并添加statement
	public String getNameSpace(String statement) {
		return NAMESPACE + statement;
	}


	@Override
	public Integer countPrizeNum(Map<String, Object> paramMap) throws Exception {		
		// TODO Auto-generated method stub
		return this.getSqlSession().selectOne(getNameSpace(COUNT_PRIZE_SEND_NUM), paramMap) ;
	}


	@Override
	public List<PpappBehaviorInfo> behaviorInfo(Map<String, Object> param) {
		return this.getSqlSession().selectList(NAMESPACE + ".selectBehaviorInfo",param);
	}

		
}