/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.dao.paipaisign;

import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.domain.paipaisign.PpappShareLog;

/**
 * PpappShareLogDao 接口
 * @author J-ONE
 * @since 2015-04-09
 */
public interface PpappShareLogDao extends BaseDao<PpappShareLog,String>{
	//自定义扩展
	
}