package com.paipai.halo.dao.paipaisign;


/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */

import java.util.Map;

import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.domain.paipaisign.PpappActivePoint;

/**
 * PpappActivePointDao 接口
 * @author J-ONE
 * @since 2015-04-09
 */
public interface PpappActivePointDao extends BaseDao<PpappActivePoint,String>{
	//自定义扩展
	/**
	 * 根据WID和ACTIVEID活动用户的点券信息。
	 * @param wid
	 * @param activeId
	 * @param readOnly  true:只读  false:读写
	 * @return
	 * @throws Exception
	 */
	public PpappActivePoint selectEntryByMap(Map<String,Object> paramMap, boolean readOnly) throws Exception ;
}