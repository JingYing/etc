/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.dao.paipaisign.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.dao.paipaisign.PpappActivePointDao;
import com.paipai.halo.domain.paipaisign.PpappActivePoint;

/**
 * PpappActivePointDao 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Repository("ppappActivePointDao")
public class PpappActivePointDaoImpl extends BaseDaoImpl<PpappActivePoint,String> implements PpappActivePointDao {
	private final static String NAMESPACE = "com.paipai.halo.dao.paipaisign.PpappActivePointDao";
	private final static String SELECT_ENTRY_BY_MAP = ".selectEntryByMap" ;
	
	public PpappActivePointDaoImpl() {
		 super.setNamespace(NAMESPACE);
	}
	
	//返回本DAO命名空间,并添加statement
	public String getNameSpace(String statement) {
		return NAMESPACE + statement;
	}

	@Override
	public PpappActivePoint selectEntryByMap(Map<String,Object> paramMap, boolean readOnly)
			throws Exception {
		// TODO Auto-generated method stub
		if (!readOnly) {
			paramMap.put("forUpdate", 1) ;
		}		
		return this.getSqlSession().selectOne(getNameSpace(SELECT_ENTRY_BY_MAP), paramMap) ;
	}
		
}