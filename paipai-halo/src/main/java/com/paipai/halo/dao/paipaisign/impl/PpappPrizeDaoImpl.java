/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.dao.paipaisign.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.dao.paipaisign.PpappPrizeDao;
import com.paipai.halo.domain.paipaisign.PpappPrize;

/**
 * PpappPrizeDao 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Repository("ppappPrizeDao")
public class PpappPrizeDaoImpl extends BaseDaoImpl<PpappPrize,String> implements PpappPrizeDao {
	private final static String NAMESPACE = "com.paipai.halo.dao.paipaisign.PpappPrizeDao";
	private final static String SELECT_ENTRY_BY_MAP = ".selectEntryByMap" ;

	public PpappPrizeDaoImpl() {
		 super.setNamespace(NAMESPACE);
	}
	
	//返回本DAO命名空间,并添加statement
	public String getNameSpace(String statement) {
		return NAMESPACE + statement;
	}	

	@Override
	public PpappPrize selectEntryByMap(Map<String, Object> paramMap, boolean readOnly)
			throws Exception {
		if (!readOnly) {
			paramMap.put("forUpdate", 1) ;
		}
		// TODO Auto-generated method stub
		return this.getSqlSession().selectOne(getNameSpace(SELECT_ENTRY_BY_MAP), paramMap) ;
	}

	@Override
	public List<PpappPrize> selectListByMap(Map<String, Object> paramMap)
			throws Exception {		
		// TODO Auto-generated method stub
		return this.getSqlSession().selectList(getNameSpace(SELECT_ENTRY_BY_MAP), paramMap) ;
	}
}