package com.paipai.halo.domain;

import java.io.Serializable;

/**
 * 
 * @ClassName: Comment 
 * @Description: 评论类
 * @author lhn
 * @date 2015-3-10 上午10:27:57 
 *
 */
public class Comment implements Serializable{
	/**
	 * 自增惟一id
	 */
	private Long id;
	/**
	 * 评论的id
	 */
	private String itemid;
	/**
	 * 评论的类型
	 */
	private int itemtype;
	/**
	 * 评论日期
	 */
	private Long commenttime;
	/**
	 * 评论标题
	 */
	private String title;
	/**
	 * 评论内容
	 */
	private String context;
	/**
	 * 评论IP
	 */
	private String IP;
	/**
	 * 是否显示
	 */
	private int isshow;
	/**
	 * 用户id
	 */
	private Long wid;
	
	private String  editerid;
	
	private Long edittime;
	
	private String nickname;
	private String head;
	
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public int getItemtype() {
		return itemtype;
	}
	public void setItemtype(int itemtype) {
		this.itemtype = itemtype;
	}
	public Long getCommenttime() {
		return commenttime;
	}
	public void setCommenttime(Long commenttime) {
		this.commenttime = commenttime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
			this.context=context;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public int getIsshow() {
		return isshow;
	}
	public void setIsshow(int isshow) {
		this.isshow = isshow;
	}
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}
	public String getEditerid() {
		return editerid;
	}
	public void setEditerid(String editerid) {
		this.editerid = editerid;
	}
	public Long getEdittime() {
		return edittime;
	}
	public void setEdittime(Long edittime) {
		this.edittime = edittime;
	}
	
}
