/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.domain.paipaisign;



/**
 * ppappPrize
 * @author J-ONE
 * @since 2015-04-14
 */
public class PpappPrize {
	private String prizeId;
	private String itemCode;
	private String prizeName;
	private String prizePic;
	private String prizePrice;
	private Integer prizeLevel;
	private String prizeLevelDesc;
	private Long prizeTotalNum;
	private Long prizeLimitNum;
	private Long prizeUsedNum;
	private Integer activeId;
	private Integer status;
	private Long insertTime;
	private Long updateTime;
	private String ext1;
	private String ext2;
	private String ext3;

	public PpappPrize(){
		//默认无参构造方法
	}

	

	public String getPrizeId() {
		return prizeId;
	}



	public void setPrizeId(String prizeId) {
		this.prizeId = prizeId;
	}



	/**
	 * 获取 itemCode
	 * @return
	 */
	public String getItemCode(){
		return itemCode;
	}
	
	/**
	 * 设置 itemCode
	 * @param itemCode
	 */
	public void setItemCode(String itemCode){
		this.itemCode = itemCode;
	}

	/**
	 * 获取 prizeName
	 * @return
	 */
	public String getPrizeName(){
		return prizeName;
	}
	
	/**
	 * 设置 prizeName
	 * @param prizeName
	 */
	public void setPrizeName(String prizeName){
		this.prizeName = prizeName;
	}

	/**
	 * 获取 prizePic
	 * @return
	 */
	public String getPrizePic(){
		return prizePic;
	}
	
	/**
	 * 设置 prizePic
	 * @param prizePic
	 */
	public void setPrizePic(String prizePic){
		this.prizePic = prizePic;
	}

	/**
	 * 获取 prizePrice
	 * @return
	 */
	public String getPrizePrice(){
		return prizePrice;
	}
	
	/**
	 * 设置 prizePrice
	 * @param prizePrice
	 */
	public void setPrizePrice(String prizePrice){
		this.prizePrice = prizePrice;
	}

	/**
	 * 获取 prizeLevel
	 * @return
	 */
	public Integer getPrizeLevel(){
		return prizeLevel;
	}
	
	/**
	 * 设置 prizeLevel
	 * @param prizeLevel
	 */
	public void setPrizeLevel(Integer prizeLevel){
		this.prizeLevel = prizeLevel;
	}

	/**
	 * 获取 prizeLevelDesc
	 * @return
	 */
	public String getPrizeLevelDesc(){
		return prizeLevelDesc;
	}
	
	/**
	 * 设置 prizeLevelDesc
	 * @param prizeLevelDesc
	 */
	public void setPrizeLevelDesc(String prizeLevelDesc){
		this.prizeLevelDesc = prizeLevelDesc;
	}

	/**
	 * 获取 prizeTotalNum
	 * @return
	 */
	public Long getPrizeTotalNum(){
		return prizeTotalNum;
	}
	
	/**
	 * 设置 prizeTotalNum
	 * @param prizeTotalNum
	 */
	public void setPrizeTotalNum(Long prizeTotalNum){
		this.prizeTotalNum = prizeTotalNum;
	}

	/**
	 * 获取 prizeLimitNum
	 * @return
	 */
	public Long getPrizeLimitNum(){
		return prizeLimitNum;
	}
	
	/**
	 * 设置 prizeLimitNum
	 * @param prizeLimitNum
	 */
	public void setPrizeLimitNum(Long prizeLimitNum){
		this.prizeLimitNum = prizeLimitNum;
	}

	/**
	 * 获取 prizeUsedNum
	 * @return
	 */
	public Long getPrizeUsedNum(){
		return prizeUsedNum;
	}
	
	/**
	 * 设置 prizeUsedNum
	 * @param prizeUsedNum
	 */
	public void setPrizeUsedNum(Long prizeUsedNum){
		this.prizeUsedNum = prizeUsedNum;
	}

	/**
	 * 获取 activeId
	 * @return
	 */
	public Integer getActiveId(){
		return activeId;
	}
	
	/**
	 * 设置 activeId
	 * @param activeId
	 */
	public void setActiveId(Integer activeId){
		this.activeId = activeId;
	}

	/**
	 * 获取 status
	 * @return
	 */
	public Integer getStatus(){
		return status;
	}
	
	/**
	 * 设置 status
	 * @param status
	 */
	public void setStatus(Integer status){
		this.status = status;
	}

	/**
	 * 获取 insertTime
	 * @return
	 */
	public Long getInsertTime(){
		return insertTime;
	}
	
	/**
	 * 设置 insertTime
	 * @param insertTime
	 */
	public void setInsertTime(Long insertTime){
		this.insertTime = insertTime;
	}

	/**
	 * 获取 updateTime
	 * @return
	 */
	public Long getUpdateTime(){
		return updateTime;
	}
	
	/**
	 * 设置 updateTime
	 * @param updateTime
	 */
	public void setUpdateTime(Long updateTime){
		this.updateTime = updateTime;
	}

	/**
	 * 获取 ext1
	 * @return
	 */
	public String getExt1(){
		return ext1;
	}
	
	/**
	 * 设置 ext1
	 * @param ext1
	 */
	public void setExt1(String ext1){
		this.ext1 = ext1;
	}

	/**
	 * 获取 ext2
	 * @return
	 */
	public String getExt2(){
		return ext2;
	}
	
	/**
	 * 设置 ext2
	 * @param ext2
	 */
	public void setExt2(String ext2){
		this.ext2 = ext2;
	}

	/**
	 * 获取 ext3
	 * @return
	 */
	public String getExt3(){
		return ext3;
	}
	
	/**
	 * 设置 ext3
	 * @param ext3
	 */
	public void setExt3(String ext3){
		this.ext3 = ext3;
	}
}