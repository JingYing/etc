package com.paipai.halo.domain;

public class JmqMsgInfo {
	public ItemLikes bizData;
	public String bizType;
	public String context;
	public String islike;
	public Long wid;
	public long time;
	public ItemLikes getBizData() {
		return bizData;
	}
	public void setBizData(ItemLikes bizData) {
		this.bizData = bizData;
	}
	public String getBizType() {
		return bizType;
	}
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getIslike() {
		return islike;
	}
	public void setIslike(String islike) {
		this.islike = islike;
	}
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
}
