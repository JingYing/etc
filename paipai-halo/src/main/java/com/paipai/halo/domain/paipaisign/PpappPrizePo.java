package com.paipai.halo.domain.paipaisign;

import java.io.Serializable;


public class PpappPrizePo  implements Serializable {

	
	private String prizeId;
	private String itemCode;
	private String prizeName;
	private String prizePic;
	private String prizePrice;
	private Long prizeTotalNum ;
	private Long prizeLeaveNum;
	private Long prizeLimitNum ;
	private int prizeLevel ;
	

	public PpappPrizePo() {
		// 默认无参构造方法
	}




	public String getPrizeId() {
		return prizeId;
	}




	public void setPrizeId(String prizeId) {
		this.prizeId = prizeId;
	}




	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getPrizeName() {
		return prizeName;
	}


	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}


	public String getPrizePic() {
		return prizePic;
	}


	public void setPrizePic(String prizePic) {
		this.prizePic = prizePic;
	}


	public String getPrizePrice() {
		return prizePrice;
	}


	public void setPrizePrice(String prizePrice) {
		this.prizePrice = prizePrice;
	}


	public Long getPrizeLeaveNum() {
		return prizeLeaveNum;
	}


	public void setPrizeLeaveNum(Long prizeLeaveNum) {
		this.prizeLeaveNum = prizeLeaveNum;
	}


	public Long getPrizeLimitNum() {
		return prizeLimitNum;
	}


	public void setPrizeLimitNum(Long prizeLimitNum) {
		this.prizeLimitNum = prizeLimitNum;
	}


	public int getPrizeLevel() {
		return prizeLevel;
	}


	public void setPrizeLevel(int prizeLevel) {
		this.prizeLevel = prizeLevel;
	}


	public Long getPrizeTotalNum() {
		return prizeTotalNum;
	}


	public void setPrizeTotalNum(Long prizeTotalNum) {
		this.prizeTotalNum = prizeTotalNum;
	}

	
}
