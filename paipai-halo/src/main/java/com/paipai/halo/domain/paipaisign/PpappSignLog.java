/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.domain.paipaisign;


/**
 * ppappSignLog
 * @author J-ONE
 * @since 2015-04-09
 */
public class PpappSignLog {
	private static final long serialVersionUID = 1L;
	private String id;
	private Long wid;
	private Integer activeId;
	private Long insertTime;
	private String insertDay;
	private String mk2;
	private String mk;
	private String ip;
	private String ext1;
	private String ext2;
	private String ext3;

	public PpappSignLog(){
		//默认无参构造方法
	}

	/**
	 * 获取 wid
	 * @return
	 */
	public Long getWid(){
		return wid;
	}
	
	/**
	 * 设置 wid
	 * @param wid
	 */
	public void setWid(Long wid){
		this.wid = wid;
	}

	/**
	 * 获取 activeId
	 * @return
	 */
	public Integer getActiveId(){
		return activeId;
	}
	
	/**
	 * 设置 activeId
	 * @param activeId
	 */
	public void setActiveId(Integer activeId){
		this.activeId = activeId;
	}

	/**
	 * 获取 insertTime
	 * @return
	 */
	public Long getInsertTime(){
		return insertTime;
	}
	
	/**
	 * 设置 insertTime
	 * @param insertTime
	 */
	public void setInsertTime(Long insertTime){
		this.insertTime = insertTime;
	}

	/**
	 * 获取 insertDay
	 * @return
	 */
	public String getInsertDay(){
		return insertDay;
	}
	
	/**
	 * 设置 insertDay
	 * @param insertDay
	 */
	public void setInsertDay(String insertDay){
		this.insertDay = insertDay;
	}

	/**
	 * 获取 mk2
	 * @return
	 */
	public String getMk2(){
		return mk2;
	}
	
	/**
	 * 设置 mk2
	 * @param mk2
	 */
	public void setMk2(String mk2){
		this.mk2 = mk2;
	}

	/**
	 * 获取 mk
	 * @return
	 */
	public String getMk(){
		return mk;
	}
	
	/**
	 * 设置 mk
	 * @param mk
	 */
	public void setMk(String mk){
		this.mk = mk;
	}

	/**
	 * 获取 ip
	 * @return
	 */
	public String getIp(){
		return ip;
	}
	
	/**
	 * 设置 ip
	 * @param ip
	 */
	public void setIp(String ip){
		this.ip = ip;
	}

	/**
	 * 获取 ext1
	 * @return
	 */
	public String getExt1(){
		return ext1;
	}
	
	/**
	 * 设置 ext1
	 * @param ext1
	 */
	public void setExt1(String ext1){
		this.ext1 = ext1;
	}

	/**
	 * 获取 ext2
	 * @return
	 */
	public String getExt2(){
		return ext2;
	}
	
	/**
	 * 设置 ext2
	 * @param ext2
	 */
	public void setExt2(String ext2){
		this.ext2 = ext2;
	}

	/**
	 * 获取 ext3
	 * @return
	 */
	public String getExt3(){
		return ext3;
	}
	
	/**
	 * 设置 ext3
	 * @param ext3
	 */
	public void setExt3(String ext3){
		this.ext3 = ext3;
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}