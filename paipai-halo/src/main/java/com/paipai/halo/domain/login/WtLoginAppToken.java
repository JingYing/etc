package com.paipai.halo.domain.login;

public class WtLoginAppToken extends AppToken{

	/**
	 * 登陆QQ号
	 */
	private long qq  ;
	/**
	 * 登陆标识
	 */
	private String lk ;
	
	public long getQq() {
		return qq;
	}
	public void setQq(long qq) {
		this.qq = qq;
	}
	public String getLk() {
		return lk;
	}
	public void setLk(String lk) {
		this.lk = lk;
	}
	
}
