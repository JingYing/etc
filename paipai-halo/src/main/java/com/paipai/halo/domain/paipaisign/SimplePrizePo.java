package com.paipai.halo.domain.paipaisign;

public class SimplePrizePo {

	private static final long serialVersionUID = 1L;
	private String prizeId;
	private String itemCode;
	private String prizeName;
	private String prizePic;
	private String prizePrice;
	private int prizeLevel ;
	private String remark ;
	

	public SimplePrizePo() {
		// 默认无参构造方法
	}

	


	public String getPrizeId() {
		return prizeId;
	}




	public void setPrizeId(String prizeId) {
		this.prizeId = prizeId;
	}




	/**
	 * 获取 itemCode
	 * 
	 * @return
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * 设置 itemCode
	 * 
	 * @param itemCode
	 */
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * 获取 prizeName
	 * 
	 * @return
	 */
	public String getPrizeName() {
		return prizeName;
	}

	/**
	 * 设置 prizeName
	 * 
	 * @param prizeName
	 */
	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}

	/**
	 * 获取 prizePic
	 * 
	 * @return
	 */
	public String getPrizePic() {
		return prizePic;
	}

	/**
	 * 设置 prizePic
	 * 
	 * @param prizePic
	 */
	public void setPrizePic(String prizePic) {
		this.prizePic = prizePic;
	}

	/**
	 * 获取 prizePrice
	 * 
	 * @return
	 */
	public String getPrizePrice() {
		return prizePrice;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setPrizePrice(String prizePrice) {
		this.prizePrice = prizePrice;
	}


	public int getPrizeLevel() {
		return prizeLevel;
	}


	public void setPrizeLevel(int prizeLevel) {
		this.prizeLevel = prizeLevel;
	}

	
	
}
