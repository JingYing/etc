package com.paipai.halo.client.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 记录bosswebstub2的日志条目.
 * @author JingYing 2014-10-13
 *
 */
public class BossLogItem extends AbstractLogItem{
	
	private String ipPort;
	private Throwable exception;
	private long costMillis, cmdId;
	private StackTraceElement[] invokeStack;
	private String bossCmd, invokeResult;		//bossCmd:boss请求命令字,不同于cmdId
	private Map<String,String> 
		reqArgs = new LinkedHashMap<String,String>(), 
		respArgs = new LinkedHashMap<String,String>();
	
	
	@Override
	public String getProtocol() {
		return PROTOCOL_BOSS服务;
	}

	@Override
	public String getServiceUrl() {
		return ipPort;
	}

	@Override
	public boolean hasError() {
		return exception!=null;
	}

	@Override
	public long getCostMillis() {
		return costMillis;
	}

	@Override
	public String toText() {
		StringBuilder sb = new StringBuilder();
		int i = 1;
		for(Entry<String,String> e : reqArgs.entrySet())	{
			sb.append(String.format("\n>> req参数%d[%s]:%s\n", i++, e.getKey(), e.getValue()));
		}
		if(exception == null)	{
			i = 1;
			for(Entry<String,String> e : respArgs.entrySet())	{
				sb.append(String.format("<< resp参数%d[%s]:%s\n", i++, e.getKey(), e.getValue()));
			}

			sb.append(String.format("<< 请求IP端口(%s),命令字(0x%s),bossCmd(%s),requestId(%s)\n", 
										ipPort, Long.toHexString(cmdId), bossCmd, getRequestId()));
			sb.append(String.format("<< BOSS2服务调用花费时间(%d)毫秒\n", costMillis));
			sb.append(String.format("<< 函数返回值:%s\n", invokeResult));
			
			sb.append(">> 调用栈轨迹(只跟踪开发代码):\n");
			if(invokeStack != null)	{
				for(String s : filterInvokeStack())	{
					sb.append("\t").append(s).append("\n");
				}
			} else	{
				sb.append("null\n");
			}
		} else	{
			StringWriter sw = new StringWriter(1000);
			exception.printStackTrace(new PrintWriter(sw));
			sb.append("<< BOSS服务抛出异常:").append(sw.toString()).append("\n");
		}
		sb.append("============================================================");	//60
		return sb.toString();
	}

	public void setCostMillis(long costMillis) {
		this.costMillis = costMillis;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public void setInvokeStack(StackTraceElement[] invokeStack) {
		this.invokeStack = invokeStack;
	}

	@Override
	public StackTraceElement[] getInvokeStack() {
		return invokeStack;
	}

	@Override
	public List<String> getParticularProp() {
		List<String> list = new ArrayList<String>();
		list.add("0x" + Long.toHexString(cmdId));
		for(Entry<String,String> e : reqArgs.entrySet())	{
			list.add(e.getKey());
			break;	//只记录第一个
		}
		if(exception == null)	{
			for(Entry<String,String> e : respArgs.entrySet())	{
				list.add(e.getKey());
				break;	//只记录第一个
			}
			list.add(invokeResult);
		} else	{
			list.add("");
			list.add("");
		}
		list.add(bossCmd);
		return list;
	}

	public void setIpPort(String ipPort) {
		this.ipPort = ipPort;
	}

	public void setBossCmd(String bossCmd) {
		this.bossCmd = bossCmd;
	}

	public void setInvokeResult(String invokeResult) {
		this.invokeResult = invokeResult;
	}

	public Map<String, String> getReqArgs() {
		return reqArgs;
	}

	public void setReqArgs(Map<String, String> reqArgs) {
		this.reqArgs = reqArgs;
	}

	public Map<String, String> getRespArgs() {
		return respArgs;
	}

	public void setRespArgs(Map<String, String> respArgs) {
		this.respArgs = respArgs;
	}

	public void setCmdId(long cmdId) {
		this.cmdId = cmdId;
	}
}
