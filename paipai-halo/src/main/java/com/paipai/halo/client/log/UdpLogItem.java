package com.paipai.halo.client.log;

import java.util.ArrayList;
import java.util.List;

public class UdpLogItem extends AbstractLogItem	{
	
	private String remoteSocketAddress;
	private long costMillis;
	private Throwable exception;				//调用函数时抛出的异常
	private StackTraceElement[] invokeStack;	//调用栈轨迹
	private String req, resp, serviceName;
	
	@Override
	public String getProtocol() {
		return PROTOCOL_UDP;
	}

	@Override
	public String getServiceUrl() {
		if(serviceName != null)
			return remoteSocketAddress + ":" + serviceName;
		else
			return remoteSocketAddress;
	}

	@Override
	public boolean hasError() {
		return exception != null;
	}

	@Override
	public long getCostMillis() {
		return costMillis;
	}

	@Override
	public String toText() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(">> 协议(%s) requestId(%s) 请求IP端口(%s) 服务名(%s)", 
						getProtocol(), getRequestId(), remoteSocketAddress, serviceName));
		if(req != null)	{
			sb.append(String.format(">>请求参数:%s\n", req));
		}
		sb.append(String.format("<< 响应时间(%d)毫秒\n", costMillis));
		if(resp != null)	{
			sb.append(String.format("<<响应参数:%s\n", resp));
		}
		sb.append(">> 调用栈轨迹(只跟踪开发代码):\n");
		if(invokeStack != null)	{
			for(String s : filterInvokeStack())	{
				sb.append("\t").append(s).append("\n");
			}
		} else	{
			sb.append("null\n");
		}
		sb.append("============================================================");	//60
		return sb.toString();
	}

	public void setRemoteSocketAddress(String remoteSocketAddress) {
		this.remoteSocketAddress = remoteSocketAddress;
	}

	public void setCostMillis(long costMillis) {
		this.costMillis = costMillis;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public void setInvokeStack(StackTraceElement[] invokeStack) {
		this.invokeStack = invokeStack;
	}

	@Override
	public List<String> getParticularProp() {
		return new ArrayList<String>();
	}

	@Override
	public StackTraceElement[] getInvokeStack() {
		return invokeStack;
	}

	public String getReq() {
		return req;
	}

	public void setReq(String req) {
		this.req = req;
	}

	public String getResp() {
		return resp;
	}

	public void setResp(String resp) {
		this.resp = resp;
	}

	public Throwable getException() {
		return exception;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
