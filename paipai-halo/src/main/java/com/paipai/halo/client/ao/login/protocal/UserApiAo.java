

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class UserApiAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0xf1601807L, desc = "用户打标去标接口", export = true)
	 public ApiModifyUserPropertyResp ApiModifyUserProperty(ApiModifyUserPropertyReq req){
		ApiModifyUserPropertyResp resp = new  ApiModifyUserPropertyResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xf1601804L, desc = "更新用户基本信息类", export = true)
	 public ApiModifyUserProfileResp ApiModifyUserProfile(ApiModifyUserProfileReq req){
		ApiModifyUserProfileResp resp = new  ApiModifyUserProfileResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xf1601805L, desc = "获取用户彩钻级别类", export = true)
	 public ApiGetUserVipLevelResp ApiGetUserVipLevel(ApiGetUserVipLevelReq req){
		ApiGetUserVipLevelResp resp = new  ApiGetUserVipLevelResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xf1601801L, desc = "获取用户简单信息类", export = true)
	 public ApiGetUserSimpleProfileResp ApiGetUserSimpleProfile(ApiGetUserSimpleProfileReq req){
		ApiGetUserSimpleProfileResp resp = new  ApiGetUserSimpleProfileResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xf1601803L, desc = "获取用户登录级别类", export = true)
	 public ApiGetUserLoginLevelResp ApiGetUserLoginLevel(ApiGetUserLoginLevelReq req){
		ApiGetUserLoginLevelResp resp = new  ApiGetUserLoginLevelResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xf1601802L, desc = "获取用户详情信息类", export = true)
	 public ApiGetUserDetailProfileResp ApiGetUserDetailProfile(ApiGetUserDetailProfileReq req){
		ApiGetUserDetailProfileResp resp = new  ApiGetUserDetailProfileResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xf1601806L, desc = "获取用户绑定京东pin账号", export = true)
	 public ApiGetUserBindJdpinResp ApiGetUserBindJdpin(ApiGetUserBindJdpinReq req){
		ApiGetUserBindJdpinResp resp = new  ApiGetUserBindJdpinResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
