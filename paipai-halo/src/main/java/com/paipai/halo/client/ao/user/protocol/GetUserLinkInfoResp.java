 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: bi.userlink.PaipaiUserLinkAO.java

package com.paipai.halo.client.ao.user.protocol;


import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.netframework.kernal.NetMessage;
import com.paipai.util.io.ByteStream;

/**
 *get UserLink resp
 *
 *@date 2015-04-22 10:17:29
 *
 *@since version:0
*/
public class  GetUserLinkInfoResp extends NetMessage
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * user id list
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> userIDList = new Vector<uint64_t>();

	/**
	 * weight list
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> UserWeightList = new Vector<uint32_t>();

	/**
	 * resp number of user link
	 *
	 * 版本 >= 0
	 */
	 private long respNum;

	/**
	 * rsp reserve
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> reserveOut = new HashMap<String,String>();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUInt(errCode);
		bs.pushString(errMsg);
		bs.pushObject(userIDList);
		bs.pushObject(UserWeightList);
		bs.pushUInt(respNum);
		bs.pushObject(reserveOut);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popUInt();
		errMsg = bs.popString();
		userIDList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		UserWeightList = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		respNum = bs.popUInt();
		reserveOut = (Map<String,String>)bs.popMap(String.class,String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x86118801L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:long
	 * 
	 */
	public long getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setErrCode(long value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取user id list
	 * 
	 * 此字段的版本 >= 0
	 * @return userIDList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getUserIDList()
	{
		return userIDList;
	}


	/**
	 * 设置user id list
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setUserIDList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.userIDList = value;
		}else{
				this.userIDList = new Vector<uint64_t>();
		}
	}


	/**
	 * 获取weight list
	 * 
	 * 此字段的版本 >= 0
	 * @return UserWeightList value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getUserWeightList()
	{
		return UserWeightList;
	}


	/**
	 * 设置weight list
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setUserWeightList(Vector<uint32_t> value)
	{
		if (value != null) {
				this.UserWeightList = value;
		}else{
				this.UserWeightList = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取resp number of user link
	 * 
	 * 此字段的版本 >= 0
	 * @return respNum value 类型为:long
	 * 
	 */
	public long getRespNum()
	{
		return respNum;
	}


	/**
	 * 设置resp number of user link
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRespNum(long value)
	{
		this.respNum = value;
	}


	/**
	 * 获取rsp reserve
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveOut value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveOut()
	{
		return reserveOut;
	}


	/**
	 * 设置rsp reserve
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveOut(Map<String,String> value)
	{
		if (value != null) {
				this.reserveOut = value;
		}else{
				this.reserveOut = new HashMap<String,String>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetUserLinkInfoResp)
				length += 4;  //计算字段errCode的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(userIDList, null);  //计算字段userIDList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(UserWeightList, null);  //计算字段UserWeightList的长度 size_of(Vector)
				length += 4;  //计算字段respNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveOut, null);  //计算字段reserveOut的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetUserLinkInfoResp)
				length += 4;  //计算字段errCode的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(userIDList, encoding);  //计算字段userIDList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(UserWeightList, encoding);  //计算字段UserWeightList的长度 size_of(Vector)
				length += 4;  //计算字段respNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveOut, encoding);  //计算字段reserveOut的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
