package com.paipai.halo.client.ao;

import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.halo.client.log.AsynWebStubLogProxy;
import com.paipai.halo.common.Util;

public class WebStubFactory
{
	private static final int connectTimeOut = 3000, readTimeOut = 3000;
	private static final String utf8 = "UTF-8", gbk = "GBK";
	
	/**
	 * 调拍拍的IDL，默认服务方为UTF-8
	 * @return
	 */
	public static AsynWebStub getWebStub4PaiPai() {
        return getWebStub4PaiPai(utf8);
	}
	
	/**
	 * 调拍拍的IDL，服务方为GBK
	 * @return
	 */
    public static AsynWebStub getWebStub4PaiPaiGBK() {
        return getWebStub4PaiPai(gbk);
	}

    public static AsynWebStub getWebStub4PaiPai(String charset) {
		AsynWebStub webStub = AsynWebStubLogProxy.newProxy();
		webStub.setTimeout(connectTimeOut, readTimeOut);
        webStub.setUin(System.currentTimeMillis()); // 应为setUin被服务端用来做负载均衡，故需要设置。
        //webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);	//京东下不能设
        webStub.setClientIP(Util.iP2Long(Util.getLocalIp()));
        webStub.setStringEncodecharset(charset);
        webStub.setStringDecodecharset(charset);
        return webStub;
	}
	
}
