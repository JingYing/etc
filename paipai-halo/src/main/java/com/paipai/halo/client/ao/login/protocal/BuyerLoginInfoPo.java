//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.UserIcsonAo.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *BuyerLoginInfoPo
 *
 *@date 2015-03-27 09:28:05
 *
 *@since version:0
*/
public class BuyerLoginInfoPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20131030;

	/**
	 * 易讯uid
	 *
	 * 版本 >= 0
	 */
	 private long icsonUid;

	/**
	 * 网购uid
	 *
	 * 版本 >= 0
	 */
	 private long wgUid;

	/**
	 * QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long QQNumber;

	/**
	 * 登陆时间
	 *
	 * 版本 >= 0
	 */
	 private long loginTime;

	/**
	 * 个性化账号
	 *
	 * 版本 >= 0
	 */
	 private String loginAccount = new String();

	/**
	 * 加密串
	 *
	 * 版本 >= 0
	 */
	 private String cryptogram = new String();

	/**
	 * visitkey
	 *
	 * 版本 >= 0
	 */
	 private String visitkey = new String();

	/**
	 * 登陆IP
	 *
	 * 版本 >= 0
	 */
	 private String loginIp = new String();

	/**
	 * reserve string
	 *
	 * 版本 >= 0
	 */
	 private String reserveStr = new String();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short icsonUid_u;

	/**
	 * 版本 >= 0
	 */
	 private short wgUid_u;

	/**
	 * 版本 >= 0
	 */
	 private short QQNumber_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginAccount_u;

	/**
	 * 版本 >= 0
	 */
	 private short cryptogram_u;

	/**
	 * 版本 >= 0
	 */
	 private short visitkey_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginIp_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserveStr_u;

	/**
	 * 登录类型，0-login登录，1-logout注销,默认是0.
	 *
	 * 版本 >= 20131030
	 */
	 private short logType = 0;

	/**
	 * 
	 *
	 * 版本 >= 20131030
	 */
	 private short logType_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushLong(icsonUid);
		bs.pushLong(wgUid);
		bs.pushLong(QQNumber);
		bs.pushLong(loginTime);
		bs.pushString(loginAccount);
		bs.pushString(cryptogram);
		bs.pushString(visitkey);
		bs.pushString(loginIp);
		bs.pushString(reserveStr);
		bs.pushUByte(version_u);
		bs.pushUByte(icsonUid_u);
		bs.pushUByte(wgUid_u);
		bs.pushUByte(QQNumber_u);
		bs.pushUByte(loginTime_u);
		bs.pushUByte(loginAccount_u);
		bs.pushUByte(cryptogram_u);
		bs.pushUByte(visitkey_u);
		bs.pushUByte(loginIp_u);
		bs.pushUByte(reserveStr_u);
		if(  this.version >= 20131030 ){
				bs.pushUByte(logType);
		}
		if(  this.version >= 20131030 ){
				bs.pushUByte(logType_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		icsonUid = bs.popLong();
		wgUid = bs.popLong();
		QQNumber = bs.popLong();
		loginTime = bs.popLong();
		loginAccount = bs.popString();
		cryptogram = bs.popString();
		visitkey = bs.popString();
		loginIp = bs.popString();
		reserveStr = bs.popString();
		version_u = bs.popUByte();
		icsonUid_u = bs.popUByte();
		wgUid_u = bs.popUByte();
		QQNumber_u = bs.popUByte();
		loginTime_u = bs.popUByte();
		loginAccount_u = bs.popUByte();
		cryptogram_u = bs.popUByte();
		visitkey_u = bs.popUByte();
		loginIp_u = bs.popUByte();
		reserveStr_u = bs.popUByte();
		if(  this.version >= 20131030 ){
				logType = bs.popUByte();
		}
		if(  this.version >= 20131030 ){
				logType_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取易讯uid
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonUid value 类型为:long
	 * 
	 */
	public long getIcsonUid()
	{
		return icsonUid;
	}


	/**
	 * 设置易讯uid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIcsonUid(long value)
	{
		this.icsonUid = value;
		this.icsonUid_u = 1;
	}

	public boolean issetIcsonUid()
	{
		return this.icsonUid_u != 0;
	}
	/**
	 * 获取网购uid
	 * 
	 * 此字段的版本 >= 0
	 * @return wgUid value 类型为:long
	 * 
	 */
	public long getWgUid()
	{
		return wgUid;
	}


	/**
	 * 设置网购uid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWgUid(long value)
	{
		this.wgUid = value;
		this.wgUid_u = 1;
	}

	public boolean issetWgUid()
	{
		return this.wgUid_u != 0;
	}
	/**
	 * 获取QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return QQNumber value 类型为:long
	 * 
	 */
	public long getQQNumber()
	{
		return QQNumber;
	}


	/**
	 * 设置QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setQQNumber(long value)
	{
		this.QQNumber = value;
		this.QQNumber_u = 1;
	}

	public boolean issetQQNumber()
	{
		return this.QQNumber_u != 0;
	}
	/**
	 * 获取登陆时间
	 * 
	 * 此字段的版本 >= 0
	 * @return loginTime value 类型为:long
	 * 
	 */
	public long getLoginTime()
	{
		return loginTime;
	}


	/**
	 * 设置登陆时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLoginTime(long value)
	{
		this.loginTime = value;
		this.loginTime_u = 1;
	}

	public boolean issetLoginTime()
	{
		return this.loginTime_u != 0;
	}
	/**
	 * 获取个性化账号
	 * 
	 * 此字段的版本 >= 0
	 * @return loginAccount value 类型为:String
	 * 
	 */
	public String getLoginAccount()
	{
		return loginAccount;
	}


	/**
	 * 设置个性化账号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLoginAccount(String value)
	{
		this.loginAccount = value;
		this.loginAccount_u = 1;
	}

	public boolean issetLoginAccount()
	{
		return this.loginAccount_u != 0;
	}
	/**
	 * 获取加密串
	 * 
	 * 此字段的版本 >= 0
	 * @return cryptogram value 类型为:String
	 * 
	 */
	public String getCryptogram()
	{
		return cryptogram;
	}


	/**
	 * 设置加密串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCryptogram(String value)
	{
		this.cryptogram = value;
		this.cryptogram_u = 1;
	}

	public boolean issetCryptogram()
	{
		return this.cryptogram_u != 0;
	}
	/**
	 * 获取visitkey
	 * 
	 * 此字段的版本 >= 0
	 * @return visitkey value 类型为:String
	 * 
	 */
	public String getVisitkey()
	{
		return visitkey;
	}


	/**
	 * 设置visitkey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setVisitkey(String value)
	{
		this.visitkey = value;
		this.visitkey_u = 1;
	}

	public boolean issetVisitkey()
	{
		return this.visitkey_u != 0;
	}
	/**
	 * 获取登陆IP
	 * 
	 * 此字段的版本 >= 0
	 * @return loginIp value 类型为:String
	 * 
	 */
	public String getLoginIp()
	{
		return loginIp;
	}


	/**
	 * 设置登陆IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLoginIp(String value)
	{
		this.loginIp = value;
		this.loginIp_u = 1;
	}

	public boolean issetLoginIp()
	{
		return this.loginIp_u != 0;
	}
	/**
	 * 获取reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr value 类型为:String
	 * 
	 */
	public String getReserveStr()
	{
		return reserveStr;
	}


	/**
	 * 设置reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveStr(String value)
	{
		this.reserveStr = value;
		this.reserveStr_u = 1;
	}

	public boolean issetReserveStr()
	{
		return this.reserveStr_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonUid_u value 类型为:short
	 * 
	 */
	public short getIcsonUid_u()
	{
		return icsonUid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIcsonUid_u(short value)
	{
		this.icsonUid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return wgUid_u value 类型为:short
	 * 
	 */
	public short getWgUid_u()
	{
		return wgUid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWgUid_u(short value)
	{
		this.wgUid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return QQNumber_u value 类型为:short
	 * 
	 */
	public short getQQNumber_u()
	{
		return QQNumber_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setQQNumber_u(short value)
	{
		this.QQNumber_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginTime_u value 类型为:short
	 * 
	 */
	public short getLoginTime_u()
	{
		return loginTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginTime_u(short value)
	{
		this.loginTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginAccount_u value 类型为:short
	 * 
	 */
	public short getLoginAccount_u()
	{
		return loginAccount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginAccount_u(short value)
	{
		this.loginAccount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cryptogram_u value 类型为:short
	 * 
	 */
	public short getCryptogram_u()
	{
		return cryptogram_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCryptogram_u(short value)
	{
		this.cryptogram_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return visitkey_u value 类型为:short
	 * 
	 */
	public short getVisitkey_u()
	{
		return visitkey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVisitkey_u(short value)
	{
		this.visitkey_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginIp_u value 类型为:short
	 * 
	 */
	public short getLoginIp_u()
	{
		return loginIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginIp_u(short value)
	{
		this.loginIp_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr_u value 类型为:short
	 * 
	 */
	public short getReserveStr_u()
	{
		return reserveStr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveStr_u(short value)
	{
		this.reserveStr_u = value;
	}


	/**
	 * 获取登录类型，0-login登录，1-logout注销,默认是0.
	 * 
	 * 此字段的版本 >= 20131030
	 * @return logType value 类型为:short
	 * 
	 */
	public short getLogType()
	{
		return logType;
	}


	/**
	 * 设置登录类型，0-login登录，1-logout注销,默认是0.
	 * 
	 * 此字段的版本 >= 20131030
	 * @param  value 类型为:short
	 * 
	 */
	public void setLogType(short value)
	{
		this.logType = value;
		this.logType_u = 1;
	}

	public boolean issetLogType()
	{
		return this.logType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20131030
	 * @return logType_u value 类型为:short
	 * 
	 */
	public short getLogType_u()
	{
		return logType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20131030
	 * @param  value 类型为:short
	 * 
	 */
	public void setLogType_u(short value)
	{
		this.logType_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BuyerLoginInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += 17;  //计算字段wgUid的长度 size_of(uint64_t)
				length += 17;  //计算字段QQNumber的长度 size_of(uint64_t)
				length += 17;  //计算字段loginTime的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(loginAccount, null);  //计算字段loginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(cryptogram, null);  //计算字段cryptogram的长度 size_of(String)
				length += ByteStream.getObjectSize(visitkey, null);  //计算字段visitkey的长度 size_of(String)
				length += ByteStream.getObjectSize(loginIp, null);  //计算字段loginIp的长度 size_of(String)
				length += ByteStream.getObjectSize(reserveStr, null);  //计算字段reserveStr的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段wgUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段QQNumber_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cryptogram_u的长度 size_of(uint8_t)
				length += 1;  //计算字段visitkey_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginIp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
				if(  this.version >= 20131030 ){
						length += 1;  //计算字段logType的长度 size_of(uint8_t)
				}
				if(  this.version >= 20131030 ){
						length += 1;  //计算字段logType_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(BuyerLoginInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += 17;  //计算字段wgUid的长度 size_of(uint64_t)
				length += 17;  //计算字段QQNumber的长度 size_of(uint64_t)
				length += 17;  //计算字段loginTime的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(loginAccount, encoding);  //计算字段loginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(cryptogram, encoding);  //计算字段cryptogram的长度 size_of(String)
				length += ByteStream.getObjectSize(visitkey, encoding);  //计算字段visitkey的长度 size_of(String)
				length += ByteStream.getObjectSize(loginIp, encoding);  //计算字段loginIp的长度 size_of(String)
				length += ByteStream.getObjectSize(reserveStr, encoding);  //计算字段reserveStr的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段wgUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段QQNumber_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cryptogram_u的长度 size_of(uint8_t)
				length += 1;  //计算字段visitkey_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginIp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
				if(  this.version >= 20131030 ){
						length += 1;  //计算字段logType的长度 size_of(uint8_t)
				}
				if(  this.version >= 20131030 ){
						length += 1;  //计算字段logType_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20131030所包含的字段*******
 *	long version;///<版本号
 *	long icsonUid;///<易讯uid
 *	long wgUid;///<网购uid
 *	long QQNumber;///<QQ号码
 *	long loginTime;///<登陆时间
 *	String loginAccount;///<个性化账号
 *	String cryptogram;///<加密串
 *	String visitkey;///<visitkey
 *	String loginIp;///<登陆IP
 *	String reserveStr;///<reserve string
 *	short version_u;
 *	short icsonUid_u;
 *	short wgUid_u;
 *	short QQNumber_u;
 *	short loginTime_u;
 *	short loginAccount_u;
 *	short cryptogram_u;
 *	short visitkey_u;
 *	short loginIp_u;
 *	short reserveStr_u;
 *	short logType;///<登录类型，0-login登录，1-logout注销,默认是0.
 *	short logType_u;
 *****以上是版本20131030所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
