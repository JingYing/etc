

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class UserIcsonAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0xA0A91828L, desc = "QQ用户或个性化帐号（字符串帐号）用户注册，不支持LoginQQ+openid的注册。已废弃，请使用统一登录接口替代", export = true)
	 public UserRegisterResp UserRegister(UserRegisterReq req){
		UserRegisterResp resp = new  UserRegisterResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91825L, desc = "重置个性化帐号的登录密码", export = true)
	 public ResetAccountPasswdByIcsonUidResp ResetAccountPasswdByIcsonUid(ResetAccountPasswdByIcsonUidReq req){
		ResetAccountPasswdByIcsonUidResp resp = new  ResetAccountPasswdByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91827L, desc = "移除绑定帐号名（目前支持email或手机号）和易迅用户id的绑定关系，即解绑", export = true)
	 public RemoveBindInfoWithIcsonUidResp RemoveBindInfoWithIcsonUid(RemoveBindInfoWithIcsonUidReq req){
		RemoveBindInfoWithIcsonUidResp resp = new  RemoveBindInfoWithIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91823L, desc = "修改用户的易迅会员等级", export = true)
	 public ModifyIcsonMemberLevelByIcsonUidResp ModifyIcsonMemberLevelByIcsonUid(ModifyIcsonMemberLevelByIcsonUidReq req){
		ModifyIcsonMemberLevelByIcsonUidResp resp = new  ModifyIcsonMemberLevelByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91822L, desc = "修改用户经验值", export = true)
	 public ModifyExperienceByIcsonUidResp ModifyExperienceByIcsonUid(ModifyExperienceByIcsonUidReq req){
		ModifyExperienceByIcsonUidResp resp = new  ModifyExperienceByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91846L, desc = "更新用户的经验值,易讯等级和虚拟经验值（会同步信息给易迅ERP）", export = true)
	 public ModifyExpLevVirExpByIcsonUidResp ModifyExpLevVirExpByIcsonUid(ModifyExpLevVirExpByIcsonUidReq req){
		ModifyExpLevVirExpByIcsonUidResp resp = new  ModifyExpLevVirExpByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91821L, desc = "修改用户基本信息（不能修改登录密码、经验值、易迅会员等级等）", export = true)
	 public ModifyBasicUserInfoByIcsonUidResp ModifyBasicUserInfoByIcsonUid(ModifyBasicUserInfoByIcsonUidReq req){
		ModifyBasicUserInfoByIcsonUidResp resp = new  ModifyBasicUserInfoByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91824L, desc = "修改个性化帐号的登录密码", export = true)
	 public ModifyAccountPasswdByIcsonUidResp ModifyAccountPasswdByIcsonUid(ModifyAccountPasswdByIcsonUidReq req){
		ModifyAccountPasswdByIcsonUidResp resp = new  ModifyAccountPasswdByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91805L, desc = "查询QQ号或个性帐号名是否已被注册", export = true)
	 public IsQQorAccountRegisteredResp IsQQorAccountRegistered(IsQQorAccountRegisteredReq req){
		IsQQorAccountRegisteredResp resp = new  IsQQorAccountRegisteredResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91804L, desc = "查询绑定帐号名（目前支持email或手机号）是否已被绑定", export = true)
	 public IsBindInfoBindedResp IsBindInfoBinded(IsBindInfoBindedReq req){
		IsBindInfoBindedResp resp = new  IsBindInfoBindedResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91844L, desc = "用户登出，必须设置rcntlinfo中的OperatorUin（易迅用户uid）和skey", export = true)
	 public IcsonUserLogoutResp IcsonUserLogout(IcsonUserLogoutReq req){
		IcsonUserLogoutResp resp = new  IcsonUserLogoutResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91841L, desc = "QQ用户或个性化帐号用户登录，对QQ用户，必须设置rcntlinfo中的skey；对个性化帐号用户，必须设置Passwd。不支持LoginQQ+openid帐号，待废弃，新的登录接入已经不再推荐使用该接口，请使用IcsonUniformLogin接口替代", export = true)
	 public IcsonUserLoginResp IcsonUserLogin(IcsonUserLoginReq req){
		IcsonUserLoginResp resp = new  IcsonUserLoginResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91845L, desc = "易迅用户统一登录", export = true)
	 public IcsonUniformLoginResp IcsonUniformLogin(IcsonUniformLoginReq req){
		IcsonUniformLoginResp resp = new  IcsonUniformLoginResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91806L, desc = "根据易迅用户id获取网购用户id", export = true)
	 public GetWgUidByIcsonUidResp GetWgUidByIcsonUid(GetWgUidByIcsonUidReq req){
		GetWgUidByIcsonUidResp resp = new  GetWgUidByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91802L, desc = "根据QQ号或个性化帐号获取用户信息，个性化帐号指易迅注册帐号、非qq帐号的第三方联合登录等字符串帐号（但不包括绑定邮箱、绑定手机号、LoginQQ+openid等）", export = true)
	 public GetUserInfoByQQorAccountResp GetUserInfoByQQorAccount(GetUserInfoByQQorAccountReq req){
		GetUserInfoByQQorAccountResp resp = new  GetUserInfoByQQorAccountResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91849L, desc = "根据QQ号获取用户信息，如果用户不存在则自动导入", export = true)
	 public GetUserInfoByQQResp GetUserInfoByQQ(GetUserInfoByQQReq req){
		GetUserInfoByQQResp resp = new  GetUserInfoByQQResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91801L, desc = "根据易迅用户id获取用户信息", export = true)
	 public GetUserInfoByIcsonUidResp GetUserInfoByIcsonUid(GetUserInfoByIcsonUidReq req){
		GetUserInfoByIcsonUidResp resp = new  GetUserInfoByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91803L, desc = "根据绑定帐号名（目前支持email或手机号）获取用户信息", export = true)
	 public GetUserInfoByBindInfoResp GetUserInfoByBindInfo(GetUserInfoByBindInfoReq req){
		GetUserInfoByBindInfoResp resp = new  GetUserInfoByBindInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91851L, desc = "获取电商私有skey.用于可口可乐微信号项目.临时接口，仅提供3个月的使用时间.", export = true)
	 public GetSkey4CocacolaResp GetSkey4Cocacola(GetSkey4CocacolaReq req){
		GetSkey4CocacolaResp resp = new  GetSkey4CocacolaResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91843L, desc = "获取用户skey，对QQ用户请将skey设置在rcntlinfo中的OperatorKey；对个性化帐号用户则请设置为空", export = true)
	 public GetSkeyResp GetSkey(GetSkeyReq req){
		GetSkeyResp resp = new  GetSkeyResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91850L, desc = "根据微信openid获取用户信息。如果不存在，则注册一个新用户。", export = true)
	 public GetOrRegisterUserInfoByWechatOpenidResp GetOrRegisterUserInfoByWechatOpenid(GetOrRegisterUserInfoByWechatOpenidReq req){
		GetOrRegisterUserInfoByWechatOpenidResp resp = new  GetOrRegisterUserInfoByWechatOpenidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91847L, desc = "获取易迅用户上次登录时间", export = true)
	 public GetLastLoginTimeByIcsonUidResp GetLastLoginTimeByIcsonUid(GetLastLoginTimeByIcsonUidReq req){
		GetLastLoginTimeByIcsonUidResp resp = new  GetLastLoginTimeByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91842L, desc = "用户验登录，请务必在rcntlinfo中设置易迅用户ID和skey， skey请取cookie里面的wg_skey", export = true)
	 public CheckLoginByIcsonUidResp CheckLoginByIcsonUid(CheckLoginByIcsonUidReq req){
		CheckLoginByIcsonUidResp resp = new  CheckLoginByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91852L, desc = "将支付宝帐号和一个易迅个性化帐号绑定。绑定成功，返回该帐号的用户信息。", export = true)
	 public BindIcsonLoginAccountWithAlipayOpenidResp BindIcsonLoginAccountWithAlipayOpenid(BindIcsonLoginAccountWithAlipayOpenidReq req){
		BindIcsonLoginAccountWithAlipayOpenidResp resp = new  BindIcsonLoginAccountWithAlipayOpenidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91848L, desc = "批量根据易迅用户id获取用户信息", export = true)
	 public BatchGetUserInfoByIcsonUidResp BatchGetUserInfoByIcsonUid(BatchGetUserInfoByIcsonUidReq req){
		BatchGetUserInfoByIcsonUidResp resp = new  BatchGetUserInfoByIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0xA0A91826L, desc = "新增邮箱/手机号/微信号和易迅用户id的绑定关系，即绑定辅助帐号", export = true)
	 public AddBindInfoWithIcsonUidResp AddBindInfoWithIcsonUid(AddBindInfoWithIcsonUidReq req){
		AddBindInfoWithIcsonUidResp resp = new  AddBindInfoWithIcsonUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
