//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.recvaddr.RecvaddrApiAo.java

package com.paipai.halo.client.ao.addr.protocol;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

/**
 *<h1>收货地址Po</h1><br/>
 *
 *@date 2013-05-09 04:22:53
 *
 *@since version:0
*/
public class ApiRecvaddr  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version = 2;

	/**
	 * 收货地址ID)
	 *
	 * 版本 >= 0
	 */
	 private long addrId;

	/**
	 * QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 地址ID
	 *
	 * 版本 >= 0
	 */
	 private long regionId;

	/**
	 * 收获地址类型
	 *
	 * 版本 >= 0
	 */
	 private long addrType;

	/**
	 * 最近一次使用时间
	 *
	 * 版本 >= 0
	 */
	 private long lastUsedTime;

	/**
	 * 最近修改时间
	 *
	 * 版本 >= 0
	 */
	 private long lastModifyTime;

	/**
	 * 使用次数
	 *
	 * 版本 >= 0
	 */
	 private long usedCount;

	/**
	 * 接收者的姓名
	 *
	 * 版本 >= 0
	 */
	 private String recvName = new String();

	/**
	 * 邮编
	 *
	 * 版本 >= 0
	 */
	 private String postCode = new String();

	/**
	 * 详细收货地址
	 *
	 * 版本 >= 0
	 */
	 private String recvAddress = new String();

	/**
	 * 电话
	 *
	 * 版本 >= 0
	 */
	 private String recvPhone = new String();

	/**
	 * 手机
	 *
	 * 版本 >= 0
	 */
	 private String recvMobile = new String();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short addrId_u;

	/**
	 * 版本 >= 0
	 */
	 private short uin_u;

	/**
	 * 版本 >= 0
	 */
	 private short regionId_u;

	/**
	 * 版本 >= 0
	 */
	 private short addrType_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastUsedTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastModifyTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short usedCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short recvName_u;

	/**
	 * 版本 >= 0
	 */
	 private short postCode_u;

	/**
	 * 版本 >= 0
	 */
	 private short recvAddress_u;

	/**
	 * 版本 >= 0
	 */
	 private short recvPhone_u;

	/**
	 * 版本 >= 0
	 */
	 private short recvMobile_u;

	/**
	 * 省
	 *
	 * 版本 >= 2
	 */
	 private String recvProvince = new String();

	/**
	 * 市
	 *
	 * 版本 >= 2
	 */
	 private String recvCity = new String();

	/**
	 * 区
	 *
	 * 版本 >= 2
	 */
	 private String recvDistric = new String();

	/**
	 * 省_u
	 *
	 * 版本 >= 2
	 */
	 private short recvProvince_u;

	/**
	 * 市_u
	 *
	 * 版本 >= 2
	 */
	 private short recvCity_u;

	/**
	 * 区_u
	 *
	 * 版本 >= 2
	 */
	 private short recvDistric_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUInt(addrId);
		bs.pushUInt(uin);
		bs.pushUInt(regionId);
		bs.pushUInt(addrType);
		bs.pushUInt(lastUsedTime);
		bs.pushUInt(lastModifyTime);
		bs.pushUInt(usedCount);
		bs.pushString(recvName);
		bs.pushString(postCode);
		bs.pushString(recvAddress);
		bs.pushString(recvPhone);
		bs.pushString(recvMobile);
		bs.pushUByte(version_u);
		bs.pushUByte(addrId_u);
		bs.pushUByte(uin_u);
		bs.pushUByte(regionId_u);
		bs.pushUByte(addrType_u);
		bs.pushUByte(lastUsedTime_u);
		bs.pushUByte(lastModifyTime_u);
		bs.pushUByte(usedCount_u);
		bs.pushUByte(recvName_u);
		bs.pushUByte(postCode_u);
		bs.pushUByte(recvAddress_u);
		bs.pushUByte(recvPhone_u);
		bs.pushUByte(recvMobile_u);
		if(  this.version >= 2 ){
				bs.pushString(recvProvince);
		}
		if(  this.version >= 2 ){
				bs.pushString(recvCity);
		}
		if(  this.version >= 2 ){
				bs.pushString(recvDistric);
		}
		if(  this.version >= 2 ){
				bs.pushUByte(recvProvince_u);
		}
		if(  this.version >= 2 ){
				bs.pushUByte(recvCity_u);
		}
		if(  this.version >= 2 ){
				bs.pushUByte(recvDistric_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		addrId = bs.popUInt();
		uin = bs.popUInt();
		regionId = bs.popUInt();
		addrType = bs.popUInt();
		lastUsedTime = bs.popUInt();
		lastModifyTime = bs.popUInt();
		usedCount = bs.popUInt();
		recvName = bs.popString();
		postCode = bs.popString();
		recvAddress = bs.popString();
		recvPhone = bs.popString();
		recvMobile = bs.popString();
		version_u = bs.popUByte();
		addrId_u = bs.popUByte();
		uin_u = bs.popUByte();
		regionId_u = bs.popUByte();
		addrType_u = bs.popUByte();
		lastUsedTime_u = bs.popUByte();
		lastModifyTime_u = bs.popUByte();
		usedCount_u = bs.popUByte();
		recvName_u = bs.popUByte();
		postCode_u = bs.popUByte();
		recvAddress_u = bs.popUByte();
		recvPhone_u = bs.popUByte();
		recvMobile_u = bs.popUByte();
		if(  this.version >= 2 ){
				recvProvince = bs.popString();
		}
		if(  this.version >= 2 ){
				recvCity = bs.popString();
		}
		if(  this.version >= 2 ){
				recvDistric = bs.popString();
		}
		if(  this.version >= 2 ){
				recvProvince_u = bs.popUByte();
		}
		if(  this.version >= 2 ){
				recvCity_u = bs.popUByte();
		}
		if(  this.version >= 2 ){
				recvDistric_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取收货地址ID)
	 * 
	 * 此字段的版本 >= 0
	 * @return addrId value 类型为:long
	 * 
	 */
	public long getAddrId()
	{
		return addrId;
	}


	/**
	 * 设置收货地址ID)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddrId(long value)
	{
		this.addrId = value;
		this.addrId_u = 1;
	}


	/**
	 * 获取QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
		this.uin_u = 1;
	}


	/**
	 * 获取地址ID
	 * 
	 * 此字段的版本 >= 0
	 * @return regionId value 类型为:long
	 * 
	 */
	public long getRegionId()
	{
		return regionId;
	}


	/**
	 * 设置地址ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegionId(long value)
	{
		this.regionId = value;
		this.regionId_u = 1;
	}


	/**
	 * 获取收获地址类型
	 * 
	 * 此字段的版本 >= 0
	 * @return addrType value 类型为:long
	 * 
	 */
	public long getAddrType()
	{
		return addrType;
	}


	/**
	 * 设置收获地址类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddrType(long value)
	{
		this.addrType = value;
		this.addrType_u = 1;
	}


	/**
	 * 获取最近一次使用时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastUsedTime value 类型为:long
	 * 
	 */
	public long getLastUsedTime()
	{
		return lastUsedTime;
	}


	/**
	 * 设置最近一次使用时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUsedTime(long value)
	{
		this.lastUsedTime = value;
		this.lastUsedTime_u = 1;
	}


	/**
	 * 获取最近修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:long
	 * 
	 */
	public long getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置最近修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastModifyTime(long value)
	{
		this.lastModifyTime = value;
		this.lastModifyTime_u = 1;
	}


	/**
	 * 获取使用次数
	 * 
	 * 此字段的版本 >= 0
	 * @return usedCount value 类型为:long
	 * 
	 */
	public long getUsedCount()
	{
		return usedCount;
	}


	/**
	 * 设置使用次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUsedCount(long value)
	{
		this.usedCount = value;
		this.usedCount_u = 1;
	}


	/**
	 * 获取接收者的姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return recvName value 类型为:String
	 * 
	 */
	public String getRecvName()
	{
		return recvName;
	}


	/**
	 * 设置接收者的姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvName(String value)
	{
		this.recvName = value;
		this.recvName_u = 1;
	}


	/**
	 * 获取邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return postCode value 类型为:String
	 * 
	 */
	public String getPostCode()
	{
		return postCode;
	}


	/**
	 * 设置邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPostCode(String value)
	{
		this.postCode = value;
		this.postCode_u = 1;
	}


	/**
	 * 获取详细收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddress value 类型为:String
	 * 
	 */
	public String getRecvAddress()
	{
		return recvAddress;
	}


	/**
	 * 设置详细收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddress(String value)
	{
		this.recvAddress = value;
		this.recvAddress_u = 1;
	}


	/**
	 * 获取电话
	 * 
	 * 此字段的版本 >= 0
	 * @return recvPhone value 类型为:String
	 * 
	 */
	public String getRecvPhone()
	{
		return recvPhone;
	}


	/**
	 * 设置电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvPhone(String value)
	{
		this.recvPhone = value;
		this.recvPhone_u = 1;
	}


	/**
	 * 获取手机
	 * 
	 * 此字段的版本 >= 0
	 * @return recvMobile value 类型为:String
	 * 
	 */
	public String getRecvMobile()
	{
		return recvMobile;
	}


	/**
	 * 设置手机
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvMobile(String value)
	{
		this.recvMobile = value;
		this.recvMobile_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return addrId_u value 类型为:short
	 * 
	 */
	public short getAddrId_u()
	{
		return addrId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddrId_u(short value)
	{
		this.addrId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return uin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.uin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return regionId_u value 类型为:short
	 * 
	 */
	public short getRegionId_u()
	{
		return regionId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRegionId_u(short value)
	{
		this.regionId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return addrType_u value 类型为:short
	 * 
	 */
	public short getAddrType_u()
	{
		return addrType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddrType_u(short value)
	{
		this.addrType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastUsedTime_u value 类型为:short
	 * 
	 */
	public short getLastUsedTime_u()
	{
		return lastUsedTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUsedTime_u(short value)
	{
		this.lastUsedTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime_u value 类型为:short
	 * 
	 */
	public short getLastModifyTime_u()
	{
		return lastModifyTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastModifyTime_u(short value)
	{
		this.lastModifyTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return usedCount_u value 类型为:short
	 * 
	 */
	public short getUsedCount_u()
	{
		return usedCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUsedCount_u(short value)
	{
		this.usedCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recvName_u value 类型为:short
	 * 
	 */
	public short getRecvName_u()
	{
		return recvName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvName_u(short value)
	{
		this.recvName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return postCode_u value 类型为:short
	 * 
	 */
	public short getPostCode_u()
	{
		return postCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPostCode_u(short value)
	{
		this.postCode_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddress_u value 类型为:short
	 * 
	 */
	public short getRecvAddress_u()
	{
		return recvAddress_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvAddress_u(short value)
	{
		this.recvAddress_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recvPhone_u value 类型为:short
	 * 
	 */
	public short getRecvPhone_u()
	{
		return recvPhone_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvPhone_u(short value)
	{
		this.recvPhone_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recvMobile_u value 类型为:short
	 * 
	 */
	public short getRecvMobile_u()
	{
		return recvMobile_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvMobile_u(short value)
	{
		this.recvMobile_u = value;
	}


	/**
	 * 获取省
	 * 
	 * 此字段的版本 >= 2
	 * @return recvProvince value 类型为:String
	 * 
	 */
	public String getRecvProvince()
	{
		return recvProvince;
	}


	/**
	 * 设置省
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvProvince(String value)
	{
		this.recvProvince = value;
		this.recvProvince_u = 1;
	}


	/**
	 * 获取市
	 * 
	 * 此字段的版本 >= 2
	 * @return recvCity value 类型为:String
	 * 
	 */
	public String getRecvCity()
	{
		return recvCity;
	}


	/**
	 * 设置市
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvCity(String value)
	{
		this.recvCity = value;
		this.recvCity_u = 1;
	}


	/**
	 * 获取区
	 * 
	 * 此字段的版本 >= 2
	 * @return recvDistric value 类型为:String
	 * 
	 */
	public String getRecvDistric()
	{
		return recvDistric;
	}


	/**
	 * 设置区
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvDistric(String value)
	{
		this.recvDistric = value;
		this.recvDistric_u = 1;
	}


	/**
	 * 获取省_u
	 * 
	 * 此字段的版本 >= 2
	 * @return recvProvince_u value 类型为:short
	 * 
	 */
	public short getRecvProvince_u()
	{
		return recvProvince_u;
	}


	/**
	 * 设置省_u
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvProvince_u(short value)
	{
		this.recvProvince_u = value;
	}


	/**
	 * 获取市_u
	 * 
	 * 此字段的版本 >= 2
	 * @return recvCity_u value 类型为:short
	 * 
	 */
	public short getRecvCity_u()
	{
		return recvCity_u;
	}


	/**
	 * 设置市_u
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvCity_u(short value)
	{
		this.recvCity_u = value;
	}


	/**
	 * 获取区_u
	 * 
	 * 此字段的版本 >= 2
	 * @return recvDistric_u value 类型为:short
	 * 
	 */
	public short getRecvDistric_u()
	{
		return recvDistric_u;
	}


	/**
	 * 设置区_u
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvDistric_u(short value)
	{
		this.recvDistric_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiRecvaddr)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段addrId的长度 size_of(uint32_t)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 4;  //计算字段regionId的长度 size_of(uint32_t)
				length += 4;  //计算字段addrType的长度 size_of(uint32_t)
				length += 4;  //计算字段lastUsedTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifyTime的长度 size_of(uint32_t)
				length += 4;  //计算字段usedCount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(recvName, null);  //计算字段recvName的长度 size_of(String)
				length += ByteStream.getObjectSize(postCode, null);  //计算字段postCode的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddress, null);  //计算字段recvAddress的长度 size_of(String)
				length += ByteStream.getObjectSize(recvPhone, null);  //计算字段recvPhone的长度 size_of(String)
				length += ByteStream.getObjectSize(recvMobile, null);  //计算字段recvMobile的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addrId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段uin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段regionId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addrType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastUsedTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastModifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段usedCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段postCode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvAddress_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvPhone_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvMobile_u的长度 size_of(uint8_t)
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(recvProvince, null);  //计算字段recvProvince的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(recvCity, null);  //计算字段recvCity的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(recvDistric, null);  //计算字段recvDistric的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段recvProvince_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段recvCity_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段recvDistric_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiRecvaddr)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段addrId的长度 size_of(uint32_t)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 4;  //计算字段regionId的长度 size_of(uint32_t)
				length += 4;  //计算字段addrType的长度 size_of(uint32_t)
				length += 4;  //计算字段lastUsedTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifyTime的长度 size_of(uint32_t)
				length += 4;  //计算字段usedCount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(recvName, encoding);  //计算字段recvName的长度 size_of(String)
				length += ByteStream.getObjectSize(postCode, encoding);  //计算字段postCode的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddress, encoding);  //计算字段recvAddress的长度 size_of(String)
				length += ByteStream.getObjectSize(recvPhone, encoding);  //计算字段recvPhone的长度 size_of(String)
				length += ByteStream.getObjectSize(recvMobile, encoding);  //计算字段recvMobile的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addrId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段uin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段regionId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addrType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastUsedTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastModifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段usedCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段postCode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvAddress_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvPhone_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvMobile_u的长度 size_of(uint8_t)
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(recvProvince, encoding);  //计算字段recvProvince的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(recvCity, encoding);  //计算字段recvCity的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(recvDistric, encoding);  //计算字段recvDistric的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段recvProvince_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段recvCity_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段recvDistric_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本2所包含的字段*******
 *	long version;///<版本
 *	long addrId;///<收货地址ID)
 *	long uin;///<QQ号码
 *	long regionId;///<地址ID
 *	long addrType;///<收获地址类型
 *	long lastUsedTime;///<最近一次使用时间
 *	long lastModifyTime;///<最近修改时间
 *	long usedCount;///<使用次数
 *	String recvName;///<接收者的姓名
 *	String postCode;///<邮编
 *	String recvAddress;///<详细收货地址
 *	String recvPhone;///<电话
 *	String recvMobile;///<手机
 *	short version_u;
 *	short addrId_u;
 *	short uin_u;
 *	short regionId_u;
 *	short addrType_u;
 *	short lastUsedTime_u;
 *	short lastModifyTime_u;
 *	short usedCount_u;
 *	short recvName_u;
 *	short postCode_u;
 *	short recvAddress_u;
 *	short recvPhone_u;
 *	short recvMobile_u;
 *	String recvProvince;///<省
 *	String recvCity;///<市
 *	String recvDistric;///<区
 *	short recvProvince_u;///<省_u
 *	short recvCity_u;///<市_u
 *	short recvDistric_u;///<区_u
 *****以上是版本2所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
