//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.IcsonUniformLoginReq.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *登录信息Po
 *
 *@date 2015-03-27 09:28:05
 *
 *@since version:0
*/
public class LoginInfoPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20131115;

	/**
	 * 登录来源，1-易迅 2-网购 3-拍拍 4-易迅无线 5-易迅无线sid 6-易迅个性化注册 7-微信登录 参见user_comm_define.h中的E_LOGIN_FROM_TYPE
	 *
	 * 版本 >= 0
	 */
	 private long loginFrom;

	/**
	 * 登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（skey有效期10天），2--无线弱登录（skey有效期30天），参见user_comm_define.h中的E_LOGIN_INTENSITY_TYPE
	 *
	 * 版本 >= 0
	 */
	 private long loginIntensityType;

	/**
	 * 用户登录帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber和QQSkey必填) 2-个性化帐号(填2则loginAccount和passwd必填) 3-openid(填3则openid和openidFrom必填) 参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
	 *
	 * 版本 >= 0
	 */
	 private long accountType;

	/**
	 * 用户QQ号，accountType填1时必填，目前仅支持32位
	 *
	 * 版本 >= 0
	 */
	 private long QQNumber;

	/**
	 * QQ号用户的skey，accountType填1时必填；loginIntensityType填2时，填lskey；loginFrom填5时表示无线sid登录，填sid
	 *
	 * 版本 >= 0
	 */
	 private String QQSkey = new String();

	/**
	 * 个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
	 *
	 * 版本 >= 0
	 */
	 private String loginAccount = new String();

	/**
	 * 个性化帐号登录密码，accountType填2时必填， accountType类型为微信openid时，填微信token
	 *
	 * 版本 >= 0
	 */
	 private String passwd = new String();

	/**
	 * Openid值， accountType填3时必填
	 *
	 * 版本 >= 0
	 */
	 private String openid = new String();

	/**
	 * openid来源，1-支付宝openid  2-返利openid 4-微信openid 参见user_comm_define.h中的E_LOGIN_OPENID_FROM
	 *
	 * 版本 >= 0
	 */
	 private long openidFrom;

	/**
	 * reserve int
	 *
	 * 版本 >= 0
	 */
	 private long reserveInt;

	/**
	 * reserve string
	 *
	 * 版本 >= 0
	 */
	 private String reserveStr = new String();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginFrom_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginIntensityType_u;

	/**
	 * 版本 >= 0
	 */
	 private short accountType_u;

	/**
	 * 版本 >= 0
	 */
	 private short QQNumber_u;

	/**
	 * 版本 >= 0
	 */
	 private short QQSkey_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginAccount_u;

	/**
	 * 版本 >= 0
	 */
	 private short passwd_u;

	/**
	 * 版本 >= 0
	 */
	 private short openid_u;

	/**
	 * 版本 >= 0
	 */
	 private short openidFrom_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserveInt_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserveStr_u;

	/**
	 * appid, 微信openid或微信tiket登录, 必填
	 *
	 * 版本 >= 20130814
	 */
	 private String appid = new String();

	/**
	 * 
	 *
	 * 版本 >= 20130814
	 */
	 private short appid_u;

	/**
	 * 鉴权方式.1---微信ticket鉴权 2--- 微信accessToken鉴权
	 *
	 * 版本 >= 20131115
	 */
	 private long AuthType;

	/**
	 * 
	 *
	 * 版本 >= 20131115
	 */
	 private short AuthType_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUInt(loginFrom);
		bs.pushUInt(loginIntensityType);
		bs.pushUInt(accountType);
		bs.pushLong(QQNumber);
		bs.pushString(QQSkey);
		bs.pushString(loginAccount);
		bs.pushString(passwd);
		bs.pushString(openid);
		bs.pushUInt(openidFrom);
		bs.pushUInt(reserveInt);
		bs.pushString(reserveStr);
		bs.pushUByte(version_u);
		bs.pushUByte(loginFrom_u);
		bs.pushUByte(loginIntensityType_u);
		bs.pushUByte(accountType_u);
		bs.pushUByte(QQNumber_u);
		bs.pushUByte(QQSkey_u);
		bs.pushUByte(loginAccount_u);
		bs.pushUByte(passwd_u);
		bs.pushUByte(openid_u);
		bs.pushUByte(openidFrom_u);
		bs.pushUByte(reserveInt_u);
		bs.pushUByte(reserveStr_u);
		if(  this.version >= 20130814 ){
				bs.pushString(appid);
		}
		if(  this.version >= 20130814 ){
				bs.pushUByte(appid_u);
		}
		if(  this.version >= 20131115 ){
				bs.pushUInt(AuthType);
		}
		if(  this.version >= 20131115 ){
				bs.pushUByte(AuthType_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		loginFrom = bs.popUInt();
		loginIntensityType = bs.popUInt();
		accountType = bs.popUInt();
		QQNumber = bs.popLong();
		QQSkey = bs.popString();
		loginAccount = bs.popString();
		passwd = bs.popString();
		openid = bs.popString();
		openidFrom = bs.popUInt();
		reserveInt = bs.popUInt();
		reserveStr = bs.popString();
		version_u = bs.popUByte();
		loginFrom_u = bs.popUByte();
		loginIntensityType_u = bs.popUByte();
		accountType_u = bs.popUByte();
		QQNumber_u = bs.popUByte();
		QQSkey_u = bs.popUByte();
		loginAccount_u = bs.popUByte();
		passwd_u = bs.popUByte();
		openid_u = bs.popUByte();
		openidFrom_u = bs.popUByte();
		reserveInt_u = bs.popUByte();
		reserveStr_u = bs.popUByte();
		if(  this.version >= 20130814 ){
				appid = bs.popString();
		}
		if(  this.version >= 20130814 ){
				appid_u = bs.popUByte();
		}
		if(  this.version >= 20131115 ){
				AuthType = bs.popUInt();
		}
		if(  this.version >= 20131115 ){
				AuthType_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取登录来源，1-易迅 2-网购 3-拍拍 4-易迅无线 5-易迅无线sid 6-易迅个性化注册 7-微信登录 参见user_comm_define.h中的E_LOGIN_FROM_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @return loginFrom value 类型为:long
	 * 
	 */
	public long getLoginFrom()
	{
		return loginFrom;
	}


	/**
	 * 设置登录来源，1-易迅 2-网购 3-拍拍 4-易迅无线 5-易迅无线sid 6-易迅个性化注册 7-微信登录 参见user_comm_define.h中的E_LOGIN_FROM_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLoginFrom(long value)
	{
		this.loginFrom = value;
		this.loginFrom_u = 1;
	}

	public boolean issetLoginFrom()
	{
		return this.loginFrom_u != 0;
	}
	/**
	 * 获取登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（skey有效期10天），2--无线弱登录（skey有效期30天），参见user_comm_define.h中的E_LOGIN_INTENSITY_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @return loginIntensityType value 类型为:long
	 * 
	 */
	public long getLoginIntensityType()
	{
		return loginIntensityType;
	}


	/**
	 * 设置登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（skey有效期10天），2--无线弱登录（skey有效期30天），参见user_comm_define.h中的E_LOGIN_INTENSITY_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLoginIntensityType(long value)
	{
		this.loginIntensityType = value;
		this.loginIntensityType_u = 1;
	}

	public boolean issetLoginIntensityType()
	{
		return this.loginIntensityType_u != 0;
	}
	/**
	 * 获取用户登录帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber和QQSkey必填) 2-个性化帐号(填2则loginAccount和passwd必填) 3-openid(填3则openid和openidFrom必填) 参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @return accountType value 类型为:long
	 * 
	 */
	public long getAccountType()
	{
		return accountType;
	}


	/**
	 * 设置用户登录帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber和QQSkey必填) 2-个性化帐号(填2则loginAccount和passwd必填) 3-openid(填3则openid和openidFrom必填) 参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAccountType(long value)
	{
		this.accountType = value;
		this.accountType_u = 1;
	}

	public boolean issetAccountType()
	{
		return this.accountType_u != 0;
	}
	/**
	 * 获取用户QQ号，accountType填1时必填，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @return QQNumber value 类型为:long
	 * 
	 */
	public long getQQNumber()
	{
		return QQNumber;
	}


	/**
	 * 设置用户QQ号，accountType填1时必填，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setQQNumber(long value)
	{
		this.QQNumber = value;
		this.QQNumber_u = 1;
	}

	public boolean issetQQNumber()
	{
		return this.QQNumber_u != 0;
	}
	/**
	 * 获取QQ号用户的skey，accountType填1时必填；loginIntensityType填2时，填lskey；loginFrom填5时表示无线sid登录，填sid
	 * 
	 * 此字段的版本 >= 0
	 * @return QQSkey value 类型为:String
	 * 
	 */
	public String getQQSkey()
	{
		return QQSkey;
	}


	/**
	 * 设置QQ号用户的skey，accountType填1时必填；loginIntensityType填2时，填lskey；loginFrom填5时表示无线sid登录，填sid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setQQSkey(String value)
	{
		this.QQSkey = value;
		this.QQSkey_u = 1;
	}

	public boolean issetQQSkey()
	{
		return this.QQSkey_u != 0;
	}
	/**
	 * 获取个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return loginAccount value 类型为:String
	 * 
	 */
	public String getLoginAccount()
	{
		return loginAccount;
	}


	/**
	 * 设置个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLoginAccount(String value)
	{
		this.loginAccount = value;
		this.loginAccount_u = 1;
	}

	public boolean issetLoginAccount()
	{
		return this.loginAccount_u != 0;
	}
	/**
	 * 获取个性化帐号登录密码，accountType填2时必填， accountType类型为微信openid时，填微信token
	 * 
	 * 此字段的版本 >= 0
	 * @return passwd value 类型为:String
	 * 
	 */
	public String getPasswd()
	{
		return passwd;
	}


	/**
	 * 设置个性化帐号登录密码，accountType填2时必填， accountType类型为微信openid时，填微信token
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPasswd(String value)
	{
		this.passwd = value;
		this.passwd_u = 1;
	}

	public boolean issetPasswd()
	{
		return this.passwd_u != 0;
	}
	/**
	 * 获取Openid值， accountType填3时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return openid value 类型为:String
	 * 
	 */
	public String getOpenid()
	{
		return openid;
	}


	/**
	 * 设置Openid值， accountType填3时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenid(String value)
	{
		this.openid = value;
		this.openid_u = 1;
	}

	public boolean issetOpenid()
	{
		return this.openid_u != 0;
	}
	/**
	 * 获取openid来源，1-支付宝openid  2-返利openid 4-微信openid 参见user_comm_define.h中的E_LOGIN_OPENID_FROM
	 * 
	 * 此字段的版本 >= 0
	 * @return openidFrom value 类型为:long
	 * 
	 */
	public long getOpenidFrom()
	{
		return openidFrom;
	}


	/**
	 * 设置openid来源，1-支付宝openid  2-返利openid 4-微信openid 参见user_comm_define.h中的E_LOGIN_OPENID_FROM
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOpenidFrom(long value)
	{
		this.openidFrom = value;
		this.openidFrom_u = 1;
	}

	public boolean issetOpenidFrom()
	{
		return this.openidFrom_u != 0;
	}
	/**
	 * 获取reserve int
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveInt value 类型为:long
	 * 
	 */
	public long getReserveInt()
	{
		return reserveInt;
	}


	/**
	 * 设置reserve int
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReserveInt(long value)
	{
		this.reserveInt = value;
		this.reserveInt_u = 1;
	}

	public boolean issetReserveInt()
	{
		return this.reserveInt_u != 0;
	}
	/**
	 * 获取reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr value 类型为:String
	 * 
	 */
	public String getReserveStr()
	{
		return reserveStr;
	}


	/**
	 * 设置reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveStr(String value)
	{
		this.reserveStr = value;
		this.reserveStr_u = 1;
	}

	public boolean issetReserveStr()
	{
		return this.reserveStr_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginFrom_u value 类型为:short
	 * 
	 */
	public short getLoginFrom_u()
	{
		return loginFrom_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginFrom_u(short value)
	{
		this.loginFrom_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginIntensityType_u value 类型为:short
	 * 
	 */
	public short getLoginIntensityType_u()
	{
		return loginIntensityType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginIntensityType_u(short value)
	{
		this.loginIntensityType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return accountType_u value 类型为:short
	 * 
	 */
	public short getAccountType_u()
	{
		return accountType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAccountType_u(short value)
	{
		this.accountType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return QQNumber_u value 类型为:short
	 * 
	 */
	public short getQQNumber_u()
	{
		return QQNumber_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setQQNumber_u(short value)
	{
		this.QQNumber_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return QQSkey_u value 类型为:short
	 * 
	 */
	public short getQQSkey_u()
	{
		return QQSkey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setQQSkey_u(short value)
	{
		this.QQSkey_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginAccount_u value 类型为:short
	 * 
	 */
	public short getLoginAccount_u()
	{
		return loginAccount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginAccount_u(short value)
	{
		this.loginAccount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return passwd_u value 类型为:short
	 * 
	 */
	public short getPasswd_u()
	{
		return passwd_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPasswd_u(short value)
	{
		this.passwd_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return openid_u value 类型为:short
	 * 
	 */
	public short getOpenid_u()
	{
		return openid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOpenid_u(short value)
	{
		this.openid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return openidFrom_u value 类型为:short
	 * 
	 */
	public short getOpenidFrom_u()
	{
		return openidFrom_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOpenidFrom_u(short value)
	{
		this.openidFrom_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveInt_u value 类型为:short
	 * 
	 */
	public short getReserveInt_u()
	{
		return reserveInt_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveInt_u(short value)
	{
		this.reserveInt_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr_u value 类型为:short
	 * 
	 */
	public short getReserveStr_u()
	{
		return reserveStr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveStr_u(short value)
	{
		this.reserveStr_u = value;
	}


	/**
	 * 获取appid, 微信openid或微信tiket登录, 必填
	 * 
	 * 此字段的版本 >= 20130814
	 * @return appid value 类型为:String
	 * 
	 */
	public String getAppid()
	{
		return appid;
	}


	/**
	 * 设置appid, 微信openid或微信tiket登录, 必填
	 * 
	 * 此字段的版本 >= 20130814
	 * @param  value 类型为:String
	 * 
	 */
	public void setAppid(String value)
	{
		this.appid = value;
		this.appid_u = 1;
	}

	public boolean issetAppid()
	{
		return this.appid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130814
	 * @return appid_u value 类型为:short
	 * 
	 */
	public short getAppid_u()
	{
		return appid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130814
	 * @param  value 类型为:short
	 * 
	 */
	public void setAppid_u(short value)
	{
		this.appid_u = value;
	}


	/**
	 * 获取鉴权方式.1---微信ticket鉴权 2--- 微信accessToken鉴权
	 * 
	 * 此字段的版本 >= 20131115
	 * @return AuthType value 类型为:long
	 * 
	 */
	public long getAuthType()
	{
		return AuthType;
	}


	/**
	 * 设置鉴权方式.1---微信ticket鉴权 2--- 微信accessToken鉴权
	 * 
	 * 此字段的版本 >= 20131115
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuthType(long value)
	{
		this.AuthType = value;
		this.AuthType_u = 1;
	}

	public boolean issetAuthType()
	{
		return this.AuthType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20131115
	 * @return AuthType_u value 类型为:short
	 * 
	 */
	public short getAuthType_u()
	{
		return AuthType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20131115
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuthType_u(short value)
	{
		this.AuthType_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(LoginInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段loginFrom的长度 size_of(uint32_t)
				length += 4;  //计算字段loginIntensityType的长度 size_of(uint32_t)
				length += 4;  //计算字段accountType的长度 size_of(uint32_t)
				length += 17;  //计算字段QQNumber的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(QQSkey, null);  //计算字段QQSkey的长度 size_of(String)
				length += ByteStream.getObjectSize(loginAccount, null);  //计算字段loginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(passwd, null);  //计算字段passwd的长度 size_of(String)
				length += ByteStream.getObjectSize(openid, null);  //计算字段openid的长度 size_of(String)
				length += 4;  //计算字段openidFrom的长度 size_of(uint32_t)
				length += 4;  //计算字段reserveInt的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveStr, null);  //计算字段reserveStr的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginFrom_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginIntensityType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段accountType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段QQNumber_u的长度 size_of(uint8_t)
				length += 1;  //计算字段QQSkey_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段passwd_u的长度 size_of(uint8_t)
				length += 1;  //计算字段openid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段openidFrom_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveInt_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
				if(  this.version >= 20130814 ){
						length += ByteStream.getObjectSize(appid, null);  //计算字段appid的长度 size_of(String)
				}
				if(  this.version >= 20130814 ){
						length += 1;  //计算字段appid_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20131115 ){
						length += 4;  //计算字段AuthType的长度 size_of(uint32_t)
				}
				if(  this.version >= 20131115 ){
						length += 1;  //计算字段AuthType_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(LoginInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段loginFrom的长度 size_of(uint32_t)
				length += 4;  //计算字段loginIntensityType的长度 size_of(uint32_t)
				length += 4;  //计算字段accountType的长度 size_of(uint32_t)
				length += 17;  //计算字段QQNumber的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(QQSkey, encoding);  //计算字段QQSkey的长度 size_of(String)
				length += ByteStream.getObjectSize(loginAccount, encoding);  //计算字段loginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(passwd, encoding);  //计算字段passwd的长度 size_of(String)
				length += ByteStream.getObjectSize(openid, encoding);  //计算字段openid的长度 size_of(String)
				length += 4;  //计算字段openidFrom的长度 size_of(uint32_t)
				length += 4;  //计算字段reserveInt的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveStr, encoding);  //计算字段reserveStr的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginFrom_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginIntensityType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段accountType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段QQNumber_u的长度 size_of(uint8_t)
				length += 1;  //计算字段QQSkey_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段passwd_u的长度 size_of(uint8_t)
				length += 1;  //计算字段openid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段openidFrom_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveInt_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
				if(  this.version >= 20130814 ){
						length += ByteStream.getObjectSize(appid, encoding);  //计算字段appid的长度 size_of(String)
				}
				if(  this.version >= 20130814 ){
						length += 1;  //计算字段appid_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20131115 ){
						length += 4;  //计算字段AuthType的长度 size_of(uint32_t)
				}
				if(  this.version >= 20131115 ){
						length += 1;  //计算字段AuthType_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20130814所包含的字段*******
 *	long version;///<版本号
 *	long loginFrom;///<登录来源，1-易迅 2-网购 3-拍拍 4-易迅无线 5-易迅无线sid 6-易迅个性化注册 7-微信登录 参见user_comm_define.h中的E_LOGIN_FROM_TYPE
 *	long loginIntensityType;///<登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（skey有效期10天），2--无线弱登录（skey有效期30天），参见user_comm_define.h中的E_LOGIN_INTENSITY_TYPE
 *	long accountType;///<用户登录帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber和QQSkey必填) 2-个性化帐号(填2则loginAccount和passwd必填) 3-openid(填3则openid和openidFrom必填) 参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	long QQNumber;///<用户QQ号，accountType填1时必填，目前仅支持32位
 *	String QQSkey;///<QQ号用户的skey，accountType填1时必填；loginIntensityType填2时，填lskey；loginFrom填5时表示无线sid登录，填sid
 *	String loginAccount;///<个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
 *	String passwd;///<个性化帐号登录密码，accountType填2时必填， accountType类型为微信openid时，填微信token
 *	String openid;///<Openid值， accountType填3时必填
 *	long openidFrom;///<openid来源，1-支付宝openid  2-返利openid 4-微信openid 参见user_comm_define.h中的E_LOGIN_OPENID_FROM
 *	long reserveInt;///<reserve int
 *	String reserveStr;///<reserve string
 *	short version_u;
 *	short loginFrom_u;
 *	short loginIntensityType_u;
 *	short accountType_u;
 *	short QQNumber_u;
 *	short QQSkey_u;
 *	short loginAccount_u;
 *	short passwd_u;
 *	short openid_u;
 *	short openidFrom_u;
 *	short reserveInt_u;
 *	short reserveStr_u;
 *	String appid;///<appid, 微信openid或微信tiket登录, 必填
 *	short appid_u;
 *****以上是版本20130814所包含的字段*******
 *
 *****以下是版本20131115所包含的字段*******
 *	long version;///<版本号
 *	long loginFrom;///<登录来源，1-易迅 2-网购 3-拍拍 4-易迅无线 5-易迅无线sid 6-易迅个性化注册 7-微信登录 参见user_comm_define.h中的E_LOGIN_FROM_TYPE
 *	long loginIntensityType;///<登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（skey有效期10天），2--无线弱登录（skey有效期30天），参见user_comm_define.h中的E_LOGIN_INTENSITY_TYPE
 *	long accountType;///<用户登录帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber和QQSkey必填) 2-个性化帐号(填2则loginAccount和passwd必填) 3-openid(填3则openid和openidFrom必填) 参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	long QQNumber;///<用户QQ号，accountType填1时必填，目前仅支持32位
 *	String QQSkey;///<QQ号用户的skey，accountType填1时必填；loginIntensityType填2时，填lskey；loginFrom填5时表示无线sid登录，填sid
 *	String loginAccount;///<个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
 *	String passwd;///<个性化帐号登录密码，accountType填2时必填， accountType类型为微信openid时，填微信token
 *	String openid;///<Openid值， accountType填3时必填
 *	long openidFrom;///<openid来源，1-支付宝openid  2-返利openid 4-微信openid 参见user_comm_define.h中的E_LOGIN_OPENID_FROM
 *	long reserveInt;///<reserve int
 *	String reserveStr;///<reserve string
 *	short version_u;
 *	short loginFrom_u;
 *	short loginIntensityType_u;
 *	short accountType_u;
 *	short QQNumber_u;
 *	short QQSkey_u;
 *	short loginAccount_u;
 *	short passwd_u;
 *	short openid_u;
 *	short openidFrom_u;
 *	short reserveInt_u;
 *	short reserveStr_u;
 *	String appid;///<appid, 微信openid或微信tiket登录, 必填
 *	short appid_u;
 *	long AuthType;///<鉴权方式.1---微信ticket鉴权 2--- 微信accessToken鉴权
 *	short AuthType_u;
 *****以上是版本20131115所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
