//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.UserApiAo.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint16_t;
import java.util.Map;
import com.paipai.lang.uint8_t;
import java.util.HashMap;

/**
 *用户po
 *
 *@date 2015-03-26 12:05:31
 *
 *@since version:0
*/
public class APIUserProfile  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 用户Po
	 *
	 * 版本 >= 0
	 */
	 private short version = 4;

	/**
	 * 性别
	 *
	 * 版本 >= 0
	 */
	 private short sex;

	/**
	 * 登录级别
	 *
	 * 版本 >= 0
	 */
	 private short loginLevel;

	/**
	 * 认证属性
	 *
	 * 版本 >= 0
	 */
	 private short authMask;

	/**
	 * 省份
	 *
	 * 版本 >= 0
	 */
	 private short province;

	/**
	 * 默认联系方式
	 *
	 * 版本 >= 0
	 */
	 private short commContactType;

	/**
	 * 证件类别
	 *
	 * 版本 >= 0
	 */
	 private short identitycardType;

	/**
	 * 最后登录类型
	 *
	 * 版本 >= 0
	 */
	 private short lastLoginType;

	/**
	 * 版本_u
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 性别_u
	 *
	 * 版本 >= 0
	 */
	 private short sex_u;

	/**
	 * 登录级别_u
	 *
	 * 版本 >= 0
	 */
	 private short loginLevel_u;

	/**
	 * 认证属性_u
	 *
	 * 版本 >= 0
	 */
	 private short authMask_u;

	/**
	 * 省份_u
	 *
	 * 版本 >= 0
	 */
	 private short province_u;

	/**
	 * 默认联系方式_u
	 *
	 * 版本 >= 0
	 */
	 private short commContactType_u;

	/**
	 * 证件类别_u
	 *
	 * 版本 >= 0
	 */
	 private short identitycardType_u;

	/**
	 * 最后登录类型_u
	 *
	 * 版本 >= 0
	 */
	 private short lastLoginType_u;

	/**
	 * 用户QQ号码_u
	 *
	 * 版本 >= 0
	 */
	 private short userId_u;

	/**
	 * 注册时间_u
	 *
	 * 版本 >= 0
	 */
	 private short regTime_u;

	/**
	 * 国家_u
	 *
	 * 版本 >= 0
	 */
	 private short country_u;

	/**
	 * 版本 >= 0
	 */
	 private short honorMask_u;

	/**
	 * 版本 >= 0
	 */
	 private short userInfoMask_u;

	/**
	 * 最后登录ip_u
	 *
	 * 版本 >= 0
	 */
	 private short lastLoginIp_u;

	/**
	 * 最后登录时间_u
	 *
	 * 版本 >= 0
	 */
	 private short lastLoginTime_u;

	/**
	 * 最后更新时间_u
	 *
	 * 版本 >= 0
	 */
	 private short modifyTime_u;

	/**
	 * 昵称_u
	 *
	 * 版本 >= 0
	 */
	 private short nickName_u;

	/**
	 * 交易密码_u
	 *
	 * 版本 >= 0
	 */
	 private short c2CPasswd_u;

	/**
	 * 手机_u
	 *
	 * 版本 >= 0
	 */
	 private short mobile_u;

	/**
	 * 邮箱_u
	 *
	 * 版本 >= 0
	 */
	 private short email_u;

	/**
	 * 参考信用_u
	 *
	 * 版本 >= 0
	 */
	 private short referCredit_u;

	/**
	 * 肖像_u
	 *
	 * 版本 >= 0
	 */
	 private short mainLogoPos_u;

	/**
	 * 真实姓名_u
	 *
	 * 版本 >= 0
	 */
	 private short name_u;

	/**
	 * 证件号码_u
	 *
	 * 版本 >= 0
	 */
	 private short identityCardNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short identityIndex_u;

	/**
	 * 固定电话_u
	 *
	 * 版本 >= 0
	 */
	 private short phone_u;

	/**
	 * 传真_u
	 *
	 * 版本 >= 0
	 */
	 private short fax_u;

	/**
	 * 邮政编码_u
	 *
	 * 版本 >= 0
	 */
	 private short postCode_u;

	/**
	 * 家庭住址_u
	 *
	 * 版本 >= 0
	 */
	 private short address_u;

	/**
	 * 用户全部属性_u
	 *
	 * 版本 >= 0
	 */
	 private short sproperty_u;

	/**
	 * 保留字_u
	 *
	 * 版本 >= 0
	 */
	 private short reserve_u;

	/**
	 * 买家信用_u
	 *
	 * 版本 >= 0
	 */
	 private short buyerCredit_u;

	/**
	 * 卖家信用_u
	 *
	 * 版本 >= 0
	 */
	 private short sellerCredit_u;

	/**
	 * 版本 >= 0
	 */
	 private short recvMsgMask_u;

	/**
	 * 城市_u
	 *
	 * 版本 >= 0
	 */
	 private short city_u;

	/**
	 * 用户属性map_u
	 *
	 * 版本 >= 0
	 */
	 private short prop_u;

	/**
	 * 用户QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long userId;

	/**
	 * 注册时间
	 *
	 * 版本 >= 0
	 */
	 private long regTime;

	/**
	 * 国家
	 *
	 * 版本 >= 0
	 */
	 private long country;

	/**
	 * 版本 >= 0
	 */
	 private long honorMask;

	/**
	 * 版本 >= 0
	 */
	 private long userInfoMask;

	/**
	 * 最后登录IP
	 *
	 * 版本 >= 0
	 */
	 private long lastLoginIp;

	/**
	 * 最后登录时间
	 *
	 * 版本 >= 0
	 */
	 private long lastLoginTime;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long modifyTime;

	/**
	 * 昵称
	 *
	 * 版本 >= 0
	 */
	 private String nickName = new String();

	/**
	 * 交易密码
	 *
	 * 版本 >= 0
	 */
	 private String c2CPasswd = new String();

	/**
	 * 手机
	 *
	 * 版本 >= 0
	 */
	 private String mobile = new String();

	/**
	 * 邮箱
	 *
	 * 版本 >= 0
	 */
	 private String email = new String();

	/**
	 * 版本 >= 0
	 */
	 private String referCredit = "";

	/**
	 * 肖像图
	 *
	 * 版本 >= 0
	 */
	 private String mainLogoPos = new String();

	/**
	 * 真实姓名
	 *
	 * 版本 >= 0
	 */
	 private String name = new String();

	/**
	 * 证件号码
	 *
	 * 版本 >= 0
	 */
	 private String identityCardNum = new String();

	/**
	 * 版本 >= 0
	 */
	 private String identityIndex = "";

	/**
	 * 固定电话
	 *
	 * 版本 >= 0
	 */
	 private String phone = new String();

	/**
	 * 传真
	 *
	 * 版本 >= 0
	 */
	 private String fax = new String();

	/**
	 * 邮政编码
	 *
	 * 版本 >= 0
	 */
	 private String postCode = new String();

	/**
	 * 家庭住址
	 *
	 * 版本 >= 0
	 */
	 private String address = new String();

	/**
	 * 用户全部属性，用于前台js
	 *
	 * 版本 >= 0
	 */
	 private String sproperty = new String();

	/**
	 * 保留字
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();

	/**
	 * 买家信用
	 *
	 * 版本 >= 0
	 */
	 private int buyerCredit;

	/**
	 * 卖家信用
	 *
	 * 版本 >= 0
	 */
	 private int sellerCredit;

	/**
	 * 版本 >= 0
	 */
	 private int recvMsgMask;

	/**
	 * 城市
	 *
	 * 版本 >= 0
	 */
	 private int city;

	/**
	 * 用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}
	 *
	 * 版本 >= 0
	 */
	 private Map<uint16_t,uint8_t> prop = new HashMap<uint16_t,uint8_t>();

	/**
	 * 虚拟信用_u
	 *
	 * 版本 >= 1
	 */
	 private short virtualCredit_u;

	/**
	 * 实物信用_u
	 *
	 * 版本 >= 1
	 */
	 private short objectCredit_u;

	/**
	 * 地区_u
	 *
	 * 版本 >= 1
	 */
	 private short region_u;

	/**
	 * 虚拟信用
	 *
	 * 版本 >= 1
	 */
	 private int virtualCredit;

	/**
	 * 实物信用
	 *
	 * 版本 >= 1
	 */
	 private int objectCredit;

	/**
	 * 地区
	 *
	 * 版本 >= 1
	 */
	 private long region;

	/**
	 * 卖家联系方式
	 *
	 * 版本 >= 2
	 */
	 private String contacts = new String();

	/**
	 * 卖家联系方式_u
	 *
	 * 版本 >= 2
	 */
	 private short contacts_u;

	/**
	 * 卖家uin
	 *
	 * 版本 >= 3
	 */
	 private long sellerUin;

	/**
	 * 卖家uin_u
	 *
	 * 版本 >= 3
	 */
	 private short sellerUin_u;

	/**
	 * 财付通账户
	 *
	 * 版本 >= 4
	 */
	 private String cftAccount = new String();

	/**
	 * 财付通账户_u
	 *
	 * 版本 >= 4
	 */
	 private short cftAccount_u;

	/**
	 * 备用字符串
	 *
	 * 版本 >= 4
	 */
	 private String tempStr = new String();

	/**
	 * 备用字符串_u
	 *
	 * 版本 >= 4
	 */
	 private short tempStr_u;

	/**
	 * 店铺类目id
	 *
	 * 版本 >= 4
	 */
	 private long shopClassId;

	/**
	 * 店铺类目id_u
	 *
	 * 版本 >= 4
	 */
	 private short shopClassId_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUByte(version);
		bs.pushUByte(sex);
		bs.pushUByte(loginLevel);
		bs.pushUByte(authMask);
		bs.pushUByte(province);
		bs.pushUByte(commContactType);
		bs.pushUByte(identitycardType);
		bs.pushUByte(lastLoginType);
		bs.pushUByte(version_u);
		bs.pushUByte(sex_u);
		bs.pushUByte(loginLevel_u);
		bs.pushUByte(authMask_u);
		bs.pushUByte(province_u);
		bs.pushUByte(commContactType_u);
		bs.pushUByte(identitycardType_u);
		bs.pushUByte(lastLoginType_u);
		bs.pushUByte(userId_u);
		bs.pushUByte(regTime_u);
		bs.pushUByte(country_u);
		bs.pushUByte(honorMask_u);
		bs.pushUByte(userInfoMask_u);
		bs.pushUByte(lastLoginIp_u);
		bs.pushUByte(lastLoginTime_u);
		bs.pushUByte(modifyTime_u);
		bs.pushUByte(nickName_u);
		bs.pushUByte(c2CPasswd_u);
		bs.pushUByte(mobile_u);
		bs.pushUByte(email_u);
		bs.pushUByte(referCredit_u);
		bs.pushUByte(mainLogoPos_u);
		bs.pushUByte(name_u);
		bs.pushUByte(identityCardNum_u);
		bs.pushUByte(identityIndex_u);
		bs.pushUByte(phone_u);
		bs.pushUByte(fax_u);
		bs.pushUByte(postCode_u);
		bs.pushUByte(address_u);
		bs.pushUByte(sproperty_u);
		bs.pushUByte(reserve_u);
		bs.pushUByte(buyerCredit_u);
		bs.pushUByte(sellerCredit_u);
		bs.pushUByte(recvMsgMask_u);
		bs.pushUByte(city_u);
		bs.pushUByte(prop_u);
		bs.pushUInt(userId);
		bs.pushUInt(regTime);
		bs.pushUInt(country);
		bs.pushUInt(honorMask);
		bs.pushUInt(userInfoMask);
		bs.pushUInt(lastLoginIp);
		bs.pushUInt(lastLoginTime);
		bs.pushUInt(modifyTime);
		bs.pushString(nickName);
		bs.pushString(c2CPasswd);
		bs.pushString(mobile);
		bs.pushString(email);
		bs.pushString(referCredit);
		bs.pushString(mainLogoPos);
		bs.pushString(name);
		bs.pushString(identityCardNum);
		bs.pushString(identityIndex);
		bs.pushString(phone);
		bs.pushString(fax);
		bs.pushString(postCode);
		bs.pushString(address);
		bs.pushString(sproperty);
		bs.pushString(reserve);
		bs.pushInt(buyerCredit);
		bs.pushInt(sellerCredit);
		bs.pushUShort(recvMsgMask);
		bs.pushUShort(city);
		bs.pushObject(prop);
		if(  this.version >= 1 ){
				bs.pushUByte(virtualCredit_u);
		}
		if(  this.version >= 1 ){
				bs.pushUByte(objectCredit_u);
		}
		if(  this.version >= 1 ){
				bs.pushUByte(region_u);
		}
		if(  this.version >= 1 ){
				bs.pushInt(virtualCredit);
		}
		if(  this.version >= 1 ){
				bs.pushInt(objectCredit);
		}
		if(  this.version >= 1 ){
				bs.pushUInt(region);
		}
		if(  this.version >= 2 ){
				bs.pushString(contacts);
		}
		if(  this.version >= 2 ){
				bs.pushUByte(contacts_u);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(sellerUin);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(sellerUin_u);
		}
		if(  this.version >= 4 ){
				bs.pushString(cftAccount);
		}
		if(  this.version >= 4 ){
				bs.pushUByte(cftAccount_u);
		}
		if(  this.version >= 4 ){
				bs.pushString(tempStr);
		}
		if(  this.version >= 4 ){
				bs.pushUByte(tempStr_u);
		}
		if(  this.version >= 4 ){
				bs.pushUInt(shopClassId);
		}
		if(  this.version >= 4 ){
				bs.pushUByte(shopClassId_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUByte();
		sex = bs.popUByte();
		loginLevel = bs.popUByte();
		authMask = bs.popUByte();
		province = bs.popUByte();
		commContactType = bs.popUByte();
		identitycardType = bs.popUByte();
		lastLoginType = bs.popUByte();
		version_u = bs.popUByte();
		sex_u = bs.popUByte();
		loginLevel_u = bs.popUByte();
		authMask_u = bs.popUByte();
		province_u = bs.popUByte();
		commContactType_u = bs.popUByte();
		identitycardType_u = bs.popUByte();
		lastLoginType_u = bs.popUByte();
		userId_u = bs.popUByte();
		regTime_u = bs.popUByte();
		country_u = bs.popUByte();
		honorMask_u = bs.popUByte();
		userInfoMask_u = bs.popUByte();
		lastLoginIp_u = bs.popUByte();
		lastLoginTime_u = bs.popUByte();
		modifyTime_u = bs.popUByte();
		nickName_u = bs.popUByte();
		c2CPasswd_u = bs.popUByte();
		mobile_u = bs.popUByte();
		email_u = bs.popUByte();
		referCredit_u = bs.popUByte();
		mainLogoPos_u = bs.popUByte();
		name_u = bs.popUByte();
		identityCardNum_u = bs.popUByte();
		identityIndex_u = bs.popUByte();
		phone_u = bs.popUByte();
		fax_u = bs.popUByte();
		postCode_u = bs.popUByte();
		address_u = bs.popUByte();
		sproperty_u = bs.popUByte();
		reserve_u = bs.popUByte();
		buyerCredit_u = bs.popUByte();
		sellerCredit_u = bs.popUByte();
		recvMsgMask_u = bs.popUByte();
		city_u = bs.popUByte();
		prop_u = bs.popUByte();
		userId = bs.popUInt();
		regTime = bs.popUInt();
		country = bs.popUInt();
		honorMask = bs.popUInt();
		userInfoMask = bs.popUInt();
		lastLoginIp = bs.popUInt();
		lastLoginTime = bs.popUInt();
		modifyTime = bs.popUInt();
		nickName = bs.popString();
		c2CPasswd = bs.popString();
		mobile = bs.popString();
		email = bs.popString();
		referCredit = bs.popString();
		mainLogoPos = bs.popString();
		name = bs.popString();
		identityCardNum = bs.popString();
		identityIndex = bs.popString();
		phone = bs.popString();
		fax = bs.popString();
		postCode = bs.popString();
		address = bs.popString();
		sproperty = bs.popString();
		reserve = bs.popString();
		buyerCredit = bs.popInt();
		sellerCredit = bs.popInt();
		recvMsgMask = bs.popUShort();
		city = bs.popUShort();
		prop = (Map<uint16_t,uint8_t>)bs.popMap(uint16_t.class,uint8_t.class);
		if(  this.version >= 1 ){
				virtualCredit_u = bs.popUByte();
		}
		if(  this.version >= 1 ){
				objectCredit_u = bs.popUByte();
		}
		if(  this.version >= 1 ){
				region_u = bs.popUByte();
		}
		if(  this.version >= 1 ){
				virtualCredit = bs.popInt();
		}
		if(  this.version >= 1 ){
				objectCredit = bs.popInt();
		}
		if(  this.version >= 1 ){
				region = bs.popUInt();
		}
		if(  this.version >= 2 ){
				contacts = bs.popString();
		}
		if(  this.version >= 2 ){
				contacts_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				sellerUin = bs.popUInt();
		}
		if(  this.version >= 3 ){
				sellerUin_u = bs.popUByte();
		}
		if(  this.version >= 4 ){
				cftAccount = bs.popString();
		}
		if(  this.version >= 4 ){
				cftAccount_u = bs.popUByte();
		}
		if(  this.version >= 4 ){
				tempStr = bs.popString();
		}
		if(  this.version >= 4 ){
				tempStr_u = bs.popUByte();
		}
		if(  this.version >= 4 ){
				shopClassId = bs.popUInt();
		}
		if(  this.version >= 4 ){
				shopClassId_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取用户Po
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:short
	 * 
	 */
	public short getVersion()
	{
		return version;
	}


	/**
	 * 设置用户Po
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion(short value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取性别
	 * 
	 * 此字段的版本 >= 0
	 * @return sex value 类型为:short
	 * 
	 */
	public short getSex()
	{
		return sex;
	}


	/**
	 * 设置性别
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSex(short value)
	{
		this.sex = value;
		this.sex_u = 1;
	}

	public boolean issetSex()
	{
		return this.sex_u != 0;
	}
	/**
	 * 获取登录级别
	 * 
	 * 此字段的版本 >= 0
	 * @return loginLevel value 类型为:short
	 * 
	 */
	public short getLoginLevel()
	{
		return loginLevel;
	}


	/**
	 * 设置登录级别
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginLevel(short value)
	{
		this.loginLevel = value;
		this.loginLevel_u = 1;
	}

	public boolean issetLoginLevel()
	{
		return this.loginLevel_u != 0;
	}
	/**
	 * 获取认证属性
	 * 
	 * 此字段的版本 >= 0
	 * @return authMask value 类型为:short
	 * 
	 */
	public short getAuthMask()
	{
		return authMask;
	}


	/**
	 * 设置认证属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuthMask(short value)
	{
		this.authMask = value;
		this.authMask_u = 1;
	}

	public boolean issetAuthMask()
	{
		return this.authMask_u != 0;
	}
	/**
	 * 获取省份
	 * 
	 * 此字段的版本 >= 0
	 * @return province value 类型为:short
	 * 
	 */
	public short getProvince()
	{
		return province;
	}


	/**
	 * 设置省份
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProvince(short value)
	{
		this.province = value;
		this.province_u = 1;
	}

	public boolean issetProvince()
	{
		return this.province_u != 0;
	}
	/**
	 * 获取默认联系方式
	 * 
	 * 此字段的版本 >= 0
	 * @return commContactType value 类型为:short
	 * 
	 */
	public short getCommContactType()
	{
		return commContactType;
	}


	/**
	 * 设置默认联系方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommContactType(short value)
	{
		this.commContactType = value;
		this.commContactType_u = 1;
	}

	public boolean issetCommContactType()
	{
		return this.commContactType_u != 0;
	}
	/**
	 * 获取证件类别
	 * 
	 * 此字段的版本 >= 0
	 * @return identitycardType value 类型为:short
	 * 
	 */
	public short getIdentitycardType()
	{
		return identitycardType;
	}


	/**
	 * 设置证件类别
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdentitycardType(short value)
	{
		this.identitycardType = value;
		this.identitycardType_u = 1;
	}

	public boolean issetIdentitycardType()
	{
		return this.identitycardType_u != 0;
	}
	/**
	 * 获取最后登录类型
	 * 
	 * 此字段的版本 >= 0
	 * @return lastLoginType value 类型为:short
	 * 
	 */
	public short getLastLoginType()
	{
		return lastLoginType;
	}


	/**
	 * 设置最后登录类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginType(short value)
	{
		this.lastLoginType = value;
		this.lastLoginType_u = 1;
	}

	public boolean issetLastLoginType()
	{
		return this.lastLoginType_u != 0;
	}
	/**
	 * 获取版本_u
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置版本_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取性别_u
	 * 
	 * 此字段的版本 >= 0
	 * @return sex_u value 类型为:short
	 * 
	 */
	public short getSex_u()
	{
		return sex_u;
	}


	/**
	 * 设置性别_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSex_u(short value)
	{
		this.sex_u = value;
	}


	/**
	 * 获取登录级别_u
	 * 
	 * 此字段的版本 >= 0
	 * @return loginLevel_u value 类型为:short
	 * 
	 */
	public short getLoginLevel_u()
	{
		return loginLevel_u;
	}


	/**
	 * 设置登录级别_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginLevel_u(short value)
	{
		this.loginLevel_u = value;
	}


	/**
	 * 获取认证属性_u
	 * 
	 * 此字段的版本 >= 0
	 * @return authMask_u value 类型为:short
	 * 
	 */
	public short getAuthMask_u()
	{
		return authMask_u;
	}


	/**
	 * 设置认证属性_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuthMask_u(short value)
	{
		this.authMask_u = value;
	}


	/**
	 * 获取省份_u
	 * 
	 * 此字段的版本 >= 0
	 * @return province_u value 类型为:short
	 * 
	 */
	public short getProvince_u()
	{
		return province_u;
	}


	/**
	 * 设置省份_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProvince_u(short value)
	{
		this.province_u = value;
	}


	/**
	 * 获取默认联系方式_u
	 * 
	 * 此字段的版本 >= 0
	 * @return commContactType_u value 类型为:short
	 * 
	 */
	public short getCommContactType_u()
	{
		return commContactType_u;
	}


	/**
	 * 设置默认联系方式_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommContactType_u(short value)
	{
		this.commContactType_u = value;
	}


	/**
	 * 获取证件类别_u
	 * 
	 * 此字段的版本 >= 0
	 * @return identitycardType_u value 类型为:short
	 * 
	 */
	public short getIdentitycardType_u()
	{
		return identitycardType_u;
	}


	/**
	 * 设置证件类别_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdentitycardType_u(short value)
	{
		this.identitycardType_u = value;
	}


	/**
	 * 获取最后登录类型_u
	 * 
	 * 此字段的版本 >= 0
	 * @return lastLoginType_u value 类型为:short
	 * 
	 */
	public short getLastLoginType_u()
	{
		return lastLoginType_u;
	}


	/**
	 * 设置最后登录类型_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginType_u(short value)
	{
		this.lastLoginType_u = value;
	}


	/**
	 * 获取用户QQ号码_u
	 * 
	 * 此字段的版本 >= 0
	 * @return userId_u value 类型为:short
	 * 
	 */
	public short getUserId_u()
	{
		return userId_u;
	}


	/**
	 * 设置用户QQ号码_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserId_u(short value)
	{
		this.userId_u = value;
	}


	/**
	 * 获取注册时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @return regTime_u value 类型为:short
	 * 
	 */
	public short getRegTime_u()
	{
		return regTime_u;
	}


	/**
	 * 设置注册时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRegTime_u(short value)
	{
		this.regTime_u = value;
	}


	/**
	 * 获取国家_u
	 * 
	 * 此字段的版本 >= 0
	 * @return country_u value 类型为:short
	 * 
	 */
	public short getCountry_u()
	{
		return country_u;
	}


	/**
	 * 设置国家_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCountry_u(short value)
	{
		this.country_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return honorMask_u value 类型为:short
	 * 
	 */
	public short getHonorMask_u()
	{
		return honorMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setHonorMask_u(short value)
	{
		this.honorMask_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return userInfoMask_u value 类型为:short
	 * 
	 */
	public short getUserInfoMask_u()
	{
		return userInfoMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserInfoMask_u(short value)
	{
		this.userInfoMask_u = value;
	}


	/**
	 * 获取最后登录ip_u
	 * 
	 * 此字段的版本 >= 0
	 * @return lastLoginIp_u value 类型为:short
	 * 
	 */
	public short getLastLoginIp_u()
	{
		return lastLoginIp_u;
	}


	/**
	 * 设置最后登录ip_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginIp_u(short value)
	{
		this.lastLoginIp_u = value;
	}


	/**
	 * 获取最后登录时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @return lastLoginTime_u value 类型为:short
	 * 
	 */
	public short getLastLoginTime_u()
	{
		return lastLoginTime_u;
	}


	/**
	 * 设置最后登录时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginTime_u(short value)
	{
		this.lastLoginTime_u = value;
	}


	/**
	 * 获取最后更新时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @return modifyTime_u value 类型为:short
	 * 
	 */
	public short getModifyTime_u()
	{
		return modifyTime_u;
	}


	/**
	 * 设置最后更新时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setModifyTime_u(short value)
	{
		this.modifyTime_u = value;
	}


	/**
	 * 获取昵称_u
	 * 
	 * 此字段的版本 >= 0
	 * @return nickName_u value 类型为:short
	 * 
	 */
	public short getNickName_u()
	{
		return nickName_u;
	}


	/**
	 * 设置昵称_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNickName_u(short value)
	{
		this.nickName_u = value;
	}


	/**
	 * 获取交易密码_u
	 * 
	 * 此字段的版本 >= 0
	 * @return c2CPasswd_u value 类型为:short
	 * 
	 */
	public short getC2CPasswd_u()
	{
		return c2CPasswd_u;
	}


	/**
	 * 设置交易密码_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setC2CPasswd_u(short value)
	{
		this.c2CPasswd_u = value;
	}


	/**
	 * 获取手机_u
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile_u value 类型为:short
	 * 
	 */
	public short getMobile_u()
	{
		return mobile_u;
	}


	/**
	 * 设置手机_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMobile_u(short value)
	{
		this.mobile_u = value;
	}


	/**
	 * 获取邮箱_u
	 * 
	 * 此字段的版本 >= 0
	 * @return email_u value 类型为:short
	 * 
	 */
	public short getEmail_u()
	{
		return email_u;
	}


	/**
	 * 设置邮箱_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmail_u(short value)
	{
		this.email_u = value;
	}


	/**
	 * 获取参考信用_u
	 * 
	 * 此字段的版本 >= 0
	 * @return referCredit_u value 类型为:short
	 * 
	 */
	public short getReferCredit_u()
	{
		return referCredit_u;
	}


	/**
	 * 设置参考信用_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReferCredit_u(short value)
	{
		this.referCredit_u = value;
	}


	/**
	 * 获取肖像_u
	 * 
	 * 此字段的版本 >= 0
	 * @return mainLogoPos_u value 类型为:short
	 * 
	 */
	public short getMainLogoPos_u()
	{
		return mainLogoPos_u;
	}


	/**
	 * 设置肖像_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMainLogoPos_u(short value)
	{
		this.mainLogoPos_u = value;
	}


	/**
	 * 获取真实姓名_u
	 * 
	 * 此字段的版本 >= 0
	 * @return name_u value 类型为:short
	 * 
	 */
	public short getName_u()
	{
		return name_u;
	}


	/**
	 * 设置真实姓名_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setName_u(short value)
	{
		this.name_u = value;
	}


	/**
	 * 获取证件号码_u
	 * 
	 * 此字段的版本 >= 0
	 * @return identityCardNum_u value 类型为:short
	 * 
	 */
	public short getIdentityCardNum_u()
	{
		return identityCardNum_u;
	}


	/**
	 * 设置证件号码_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdentityCardNum_u(short value)
	{
		this.identityCardNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return identityIndex_u value 类型为:short
	 * 
	 */
	public short getIdentityIndex_u()
	{
		return identityIndex_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdentityIndex_u(short value)
	{
		this.identityIndex_u = value;
	}


	/**
	 * 获取固定电话_u
	 * 
	 * 此字段的版本 >= 0
	 * @return phone_u value 类型为:short
	 * 
	 */
	public short getPhone_u()
	{
		return phone_u;
	}


	/**
	 * 设置固定电话_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPhone_u(short value)
	{
		this.phone_u = value;
	}


	/**
	 * 获取传真_u
	 * 
	 * 此字段的版本 >= 0
	 * @return fax_u value 类型为:short
	 * 
	 */
	public short getFax_u()
	{
		return fax_u;
	}


	/**
	 * 设置传真_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFax_u(short value)
	{
		this.fax_u = value;
	}


	/**
	 * 获取邮政编码_u
	 * 
	 * 此字段的版本 >= 0
	 * @return postCode_u value 类型为:short
	 * 
	 */
	public short getPostCode_u()
	{
		return postCode_u;
	}


	/**
	 * 设置邮政编码_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPostCode_u(short value)
	{
		this.postCode_u = value;
	}


	/**
	 * 获取家庭住址_u
	 * 
	 * 此字段的版本 >= 0
	 * @return address_u value 类型为:short
	 * 
	 */
	public short getAddress_u()
	{
		return address_u;
	}


	/**
	 * 设置家庭住址_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress_u(short value)
	{
		this.address_u = value;
	}


	/**
	 * 获取用户全部属性_u
	 * 
	 * 此字段的版本 >= 0
	 * @return sproperty_u value 类型为:short
	 * 
	 */
	public short getSproperty_u()
	{
		return sproperty_u;
	}


	/**
	 * 设置用户全部属性_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSproperty_u(short value)
	{
		this.sproperty_u = value;
	}


	/**
	 * 获取保留字_u
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve_u value 类型为:short
	 * 
	 */
	public short getReserve_u()
	{
		return reserve_u;
	}


	/**
	 * 设置保留字_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserve_u(short value)
	{
		this.reserve_u = value;
	}


	/**
	 * 获取买家信用_u
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerCredit_u value 类型为:short
	 * 
	 */
	public short getBuyerCredit_u()
	{
		return buyerCredit_u;
	}


	/**
	 * 设置买家信用_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerCredit_u(short value)
	{
		this.buyerCredit_u = value;
	}


	/**
	 * 获取卖家信用_u
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerCredit_u value 类型为:short
	 * 
	 */
	public short getSellerCredit_u()
	{
		return sellerCredit_u;
	}


	/**
	 * 设置卖家信用_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerCredit_u(short value)
	{
		this.sellerCredit_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recvMsgMask_u value 类型为:short
	 * 
	 */
	public short getRecvMsgMask_u()
	{
		return recvMsgMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvMsgMask_u(short value)
	{
		this.recvMsgMask_u = value;
	}


	/**
	 * 获取城市_u
	 * 
	 * 此字段的版本 >= 0
	 * @return city_u value 类型为:short
	 * 
	 */
	public short getCity_u()
	{
		return city_u;
	}


	/**
	 * 设置城市_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCity_u(short value)
	{
		this.city_u = value;
	}


	/**
	 * 获取用户属性map_u
	 * 
	 * 此字段的版本 >= 0
	 * @return prop_u value 类型为:short
	 * 
	 */
	public short getProp_u()
	{
		return prop_u;
	}


	/**
	 * 设置用户属性map_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProp_u(short value)
	{
		this.prop_u = value;
	}


	/**
	 * 获取用户QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return userId value 类型为:long
	 * 
	 */
	public long getUserId()
	{
		return userId;
	}


	/**
	 * 设置用户QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserId(long value)
	{
		this.userId = value;
		this.userId_u = 1;
	}

	public boolean issetUserId()
	{
		return this.userId_u != 0;
	}
	/**
	 * 获取注册时间
	 * 
	 * 此字段的版本 >= 0
	 * @return regTime value 类型为:long
	 * 
	 */
	public long getRegTime()
	{
		return regTime;
	}


	/**
	 * 设置注册时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegTime(long value)
	{
		this.regTime = value;
		this.regTime_u = 1;
	}

	public boolean issetRegTime()
	{
		return this.regTime_u != 0;
	}
	/**
	 * 获取国家
	 * 
	 * 此字段的版本 >= 0
	 * @return country value 类型为:long
	 * 
	 */
	public long getCountry()
	{
		return country;
	}


	/**
	 * 设置国家
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCountry(long value)
	{
		this.country = value;
		this.country_u = 1;
	}

	public boolean issetCountry()
	{
		return this.country_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return honorMask value 类型为:long
	 * 
	 */
	public long getHonorMask()
	{
		return honorMask;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setHonorMask(long value)
	{
		this.honorMask = value;
		this.honorMask_u = 1;
	}

	public boolean issetHonorMask()
	{
		return this.honorMask_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return userInfoMask value 类型为:long
	 * 
	 */
	public long getUserInfoMask()
	{
		return userInfoMask;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserInfoMask(long value)
	{
		this.userInfoMask = value;
		this.userInfoMask_u = 1;
	}

	public boolean issetUserInfoMask()
	{
		return this.userInfoMask_u != 0;
	}
	/**
	 * 获取最后登录IP
	 * 
	 * 此字段的版本 >= 0
	 * @return lastLoginIp value 类型为:long
	 * 
	 */
	public long getLastLoginIp()
	{
		return lastLoginIp;
	}


	/**
	 * 设置最后登录IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastLoginIp(long value)
	{
		this.lastLoginIp = value;
		this.lastLoginIp_u = 1;
	}

	public boolean issetLastLoginIp()
	{
		return this.lastLoginIp_u != 0;
	}
	/**
	 * 获取最后登录时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastLoginTime value 类型为:long
	 * 
	 */
	public long getLastLoginTime()
	{
		return lastLoginTime;
	}


	/**
	 * 设置最后登录时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastLoginTime(long value)
	{
		this.lastLoginTime = value;
		this.lastLoginTime_u = 1;
	}

	public boolean issetLastLoginTime()
	{
		return this.lastLoginTime_u != 0;
	}
	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return modifyTime value 类型为:long
	 * 
	 */
	public long getModifyTime()
	{
		return modifyTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setModifyTime(long value)
	{
		this.modifyTime = value;
		this.modifyTime_u = 1;
	}

	public boolean issetModifyTime()
	{
		return this.modifyTime_u != 0;
	}
	/**
	 * 获取昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return nickName value 类型为:String
	 * 
	 */
	public String getNickName()
	{
		return nickName;
	}


	/**
	 * 设置昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNickName(String value)
	{
		this.nickName = value;
		this.nickName_u = 1;
	}

	public boolean issetNickName()
	{
		return this.nickName_u != 0;
	}
	/**
	 * 获取交易密码
	 * 
	 * 此字段的版本 >= 0
	 * @return c2CPasswd value 类型为:String
	 * 
	 */
	public String getC2CPasswd()
	{
		return c2CPasswd;
	}


	/**
	 * 设置交易密码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setC2CPasswd(String value)
	{
		this.c2CPasswd = value;
		this.c2CPasswd_u = 1;
	}

	public boolean issetC2CPasswd()
	{
		return this.c2CPasswd_u != 0;
	}
	/**
	 * 获取手机
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile value 类型为:String
	 * 
	 */
	public String getMobile()
	{
		return mobile;
	}


	/**
	 * 设置手机
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobile(String value)
	{
		this.mobile = value;
		this.mobile_u = 1;
	}

	public boolean issetMobile()
	{
		return this.mobile_u != 0;
	}
	/**
	 * 获取邮箱
	 * 
	 * 此字段的版本 >= 0
	 * @return email value 类型为:String
	 * 
	 */
	public String getEmail()
	{
		return email;
	}


	/**
	 * 设置邮箱
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setEmail(String value)
	{
		this.email = value;
		this.email_u = 1;
	}

	public boolean issetEmail()
	{
		return this.email_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return referCredit value 类型为:String
	 * 
	 */
	public String getReferCredit()
	{
		return referCredit;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReferCredit(String value)
	{
		this.referCredit = value;
		this.referCredit_u = 1;
	}

	public boolean issetReferCredit()
	{
		return this.referCredit_u != 0;
	}
	/**
	 * 获取肖像图
	 * 
	 * 此字段的版本 >= 0
	 * @return mainLogoPos value 类型为:String
	 * 
	 */
	public String getMainLogoPos()
	{
		return mainLogoPos;
	}


	/**
	 * 设置肖像图
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMainLogoPos(String value)
	{
		this.mainLogoPos = value;
		this.mainLogoPos_u = 1;
	}

	public boolean issetMainLogoPos()
	{
		return this.mainLogoPos_u != 0;
	}
	/**
	 * 获取真实姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * 设置真实姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.name = value;
		this.name_u = 1;
	}

	public boolean issetName()
	{
		return this.name_u != 0;
	}
	/**
	 * 获取证件号码
	 * 
	 * 此字段的版本 >= 0
	 * @return identityCardNum value 类型为:String
	 * 
	 */
	public String getIdentityCardNum()
	{
		return identityCardNum;
	}


	/**
	 * 设置证件号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIdentityCardNum(String value)
	{
		this.identityCardNum = value;
		this.identityCardNum_u = 1;
	}

	public boolean issetIdentityCardNum()
	{
		return this.identityCardNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return identityIndex value 类型为:String
	 * 
	 */
	public String getIdentityIndex()
	{
		return identityIndex;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIdentityIndex(String value)
	{
		this.identityIndex = value;
		this.identityIndex_u = 1;
	}

	public boolean issetIdentityIndex()
	{
		return this.identityIndex_u != 0;
	}
	/**
	 * 获取固定电话
	 * 
	 * 此字段的版本 >= 0
	 * @return phone value 类型为:String
	 * 
	 */
	public String getPhone()
	{
		return phone;
	}


	/**
	 * 设置固定电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPhone(String value)
	{
		this.phone = value;
		this.phone_u = 1;
	}

	public boolean issetPhone()
	{
		return this.phone_u != 0;
	}
	/**
	 * 获取传真
	 * 
	 * 此字段的版本 >= 0
	 * @return fax value 类型为:String
	 * 
	 */
	public String getFax()
	{
		return fax;
	}


	/**
	 * 设置传真
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFax(String value)
	{
		this.fax = value;
		this.fax_u = 1;
	}

	public boolean issetFax()
	{
		return this.fax_u != 0;
	}
	/**
	 * 获取邮政编码
	 * 
	 * 此字段的版本 >= 0
	 * @return postCode value 类型为:String
	 * 
	 */
	public String getPostCode()
	{
		return postCode;
	}


	/**
	 * 设置邮政编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPostCode(String value)
	{
		this.postCode = value;
		this.postCode_u = 1;
	}

	public boolean issetPostCode()
	{
		return this.postCode_u != 0;
	}
	/**
	 * 获取家庭住址
	 * 
	 * 此字段的版本 >= 0
	 * @return address value 类型为:String
	 * 
	 */
	public String getAddress()
	{
		return address;
	}


	/**
	 * 设置家庭住址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress(String value)
	{
		this.address = value;
		this.address_u = 1;
	}

	public boolean issetAddress()
	{
		return this.address_u != 0;
	}
	/**
	 * 获取用户全部属性，用于前台js
	 * 
	 * 此字段的版本 >= 0
	 * @return sproperty value 类型为:String
	 * 
	 */
	public String getSproperty()
	{
		return sproperty;
	}


	/**
	 * 设置用户全部属性，用于前台js
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSproperty(String value)
	{
		this.sproperty = value;
		this.sproperty_u = 1;
	}

	public boolean issetSproperty()
	{
		return this.sproperty_u != 0;
	}
	/**
	 * 获取保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.reserve = value;
		this.reserve_u = 1;
	}

	public boolean issetReserve()
	{
		return this.reserve_u != 0;
	}
	/**
	 * 获取买家信用
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerCredit value 类型为:int
	 * 
	 */
	public int getBuyerCredit()
	{
		return buyerCredit;
	}


	/**
	 * 设置买家信用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerCredit(int value)
	{
		this.buyerCredit = value;
		this.buyerCredit_u = 1;
	}

	public boolean issetBuyerCredit()
	{
		return this.buyerCredit_u != 0;
	}
	/**
	 * 获取卖家信用
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerCredit value 类型为:int
	 * 
	 */
	public int getSellerCredit()
	{
		return sellerCredit;
	}


	/**
	 * 设置卖家信用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSellerCredit(int value)
	{
		this.sellerCredit = value;
		this.sellerCredit_u = 1;
	}

	public boolean issetSellerCredit()
	{
		return this.sellerCredit_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recvMsgMask value 类型为:int
	 * 
	 */
	public int getRecvMsgMask()
	{
		return recvMsgMask;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRecvMsgMask(int value)
	{
		this.recvMsgMask = value;
		this.recvMsgMask_u = 1;
	}

	public boolean issetRecvMsgMask()
	{
		return this.recvMsgMask_u != 0;
	}
	/**
	 * 获取城市
	 * 
	 * 此字段的版本 >= 0
	 * @return city value 类型为:int
	 * 
	 */
	public int getCity()
	{
		return city;
	}


	/**
	 * 设置城市
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCity(int value)
	{
		this.city = value;
		this.city_u = 1;
	}

	public boolean issetCity()
	{
		return this.city_u != 0;
	}
	/**
	 * 获取用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return prop value 类型为:Map<uint16_t,uint8_t>
	 * 
	 */
	public Map<uint16_t,uint8_t> getProp()
	{
		return prop;
	}


	/**
	 * 设置用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint16_t,uint8_t>
	 * 
	 */
	public void setProp(Map<uint16_t,uint8_t> value)
	{
		if (value != null) {
				this.prop = value;
				this.prop_u = 1;
		}
	}

	public boolean issetProp()
	{
		return this.prop_u != 0;
	}
	/**
	 * 获取虚拟信用_u
	 * 
	 * 此字段的版本 >= 1
	 * @return virtualCredit_u value 类型为:short
	 * 
	 */
	public short getVirtualCredit_u()
	{
		return virtualCredit_u;
	}


	/**
	 * 设置虚拟信用_u
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:short
	 * 
	 */
	public void setVirtualCredit_u(short value)
	{
		this.virtualCredit_u = value;
	}


	/**
	 * 获取实物信用_u
	 * 
	 * 此字段的版本 >= 1
	 * @return objectCredit_u value 类型为:short
	 * 
	 */
	public short getObjectCredit_u()
	{
		return objectCredit_u;
	}


	/**
	 * 设置实物信用_u
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:short
	 * 
	 */
	public void setObjectCredit_u(short value)
	{
		this.objectCredit_u = value;
	}


	/**
	 * 获取地区_u
	 * 
	 * 此字段的版本 >= 1
	 * @return region_u value 类型为:short
	 * 
	 */
	public short getRegion_u()
	{
		return region_u;
	}


	/**
	 * 设置地区_u
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:short
	 * 
	 */
	public void setRegion_u(short value)
	{
		this.region_u = value;
	}


	/**
	 * 获取虚拟信用
	 * 
	 * 此字段的版本 >= 1
	 * @return virtualCredit value 类型为:int
	 * 
	 */
	public int getVirtualCredit()
	{
		return virtualCredit;
	}


	/**
	 * 设置虚拟信用
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:int
	 * 
	 */
	public void setVirtualCredit(int value)
	{
		this.virtualCredit = value;
		this.virtualCredit_u = 1;
	}

	public boolean issetVirtualCredit()
	{
		return this.virtualCredit_u != 0;
	}
	/**
	 * 获取实物信用
	 * 
	 * 此字段的版本 >= 1
	 * @return objectCredit value 类型为:int
	 * 
	 */
	public int getObjectCredit()
	{
		return objectCredit;
	}


	/**
	 * 设置实物信用
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:int
	 * 
	 */
	public void setObjectCredit(int value)
	{
		this.objectCredit = value;
		this.objectCredit_u = 1;
	}

	public boolean issetObjectCredit()
	{
		return this.objectCredit_u != 0;
	}
	/**
	 * 获取地区
	 * 
	 * 此字段的版本 >= 1
	 * @return region value 类型为:long
	 * 
	 */
	public long getRegion()
	{
		return region;
	}


	/**
	 * 设置地区
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegion(long value)
	{
		this.region = value;
		this.region_u = 1;
	}

	public boolean issetRegion()
	{
		return this.region_u != 0;
	}
	/**
	 * 获取卖家联系方式
	 * 
	 * 此字段的版本 >= 2
	 * @return contacts value 类型为:String
	 * 
	 */
	public String getContacts()
	{
		return contacts;
	}


	/**
	 * 设置卖家联系方式
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:String
	 * 
	 */
	public void setContacts(String value)
	{
		this.contacts = value;
		this.contacts_u = 1;
	}

	public boolean issetContacts()
	{
		return this.contacts_u != 0;
	}
	/**
	 * 获取卖家联系方式_u
	 * 
	 * 此字段的版本 >= 2
	 * @return contacts_u value 类型为:short
	 * 
	 */
	public short getContacts_u()
	{
		return contacts_u;
	}


	/**
	 * 设置卖家联系方式_u
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:short
	 * 
	 */
	public void setContacts_u(short value)
	{
		this.contacts_u = value;
	}


	/**
	 * 获取卖家uin
	 * 
	 * 此字段的版本 >= 3
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家uin
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
		this.sellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.sellerUin_u != 0;
	}
	/**
	 * 获取卖家uin_u
	 * 
	 * 此字段的版本 >= 3
	 * @return sellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return sellerUin_u;
	}


	/**
	 * 设置卖家uin_u
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.sellerUin_u = value;
	}


	/**
	 * 获取财付通账户
	 * 
	 * 此字段的版本 >= 4
	 * @return cftAccount value 类型为:String
	 * 
	 */
	public String getCftAccount()
	{
		return cftAccount;
	}


	/**
	 * 设置财付通账户
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:String
	 * 
	 */
	public void setCftAccount(String value)
	{
		this.cftAccount = value;
		this.cftAccount_u = 1;
	}

	public boolean issetCftAccount()
	{
		return this.cftAccount_u != 0;
	}
	/**
	 * 获取财付通账户_u
	 * 
	 * 此字段的版本 >= 4
	 * @return cftAccount_u value 类型为:short
	 * 
	 */
	public short getCftAccount_u()
	{
		return cftAccount_u;
	}


	/**
	 * 设置财付通账户_u
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:short
	 * 
	 */
	public void setCftAccount_u(short value)
	{
		this.cftAccount_u = value;
	}


	/**
	 * 获取备用字符串
	 * 
	 * 此字段的版本 >= 4
	 * @return tempStr value 类型为:String
	 * 
	 */
	public String getTempStr()
	{
		return tempStr;
	}


	/**
	 * 设置备用字符串
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:String
	 * 
	 */
	public void setTempStr(String value)
	{
		this.tempStr = value;
		this.tempStr_u = 1;
	}

	public boolean issetTempStr()
	{
		return this.tempStr_u != 0;
	}
	/**
	 * 获取备用字符串_u
	 * 
	 * 此字段的版本 >= 4
	 * @return tempStr_u value 类型为:short
	 * 
	 */
	public short getTempStr_u()
	{
		return tempStr_u;
	}


	/**
	 * 设置备用字符串_u
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:short
	 * 
	 */
	public void setTempStr_u(short value)
	{
		this.tempStr_u = value;
	}


	/**
	 * 获取店铺类目id
	 * 
	 * 此字段的版本 >= 4
	 * @return shopClassId value 类型为:long
	 * 
	 */
	public long getShopClassId()
	{
		return shopClassId;
	}


	/**
	 * 设置店铺类目id
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopClassId(long value)
	{
		this.shopClassId = value;
		this.shopClassId_u = 1;
	}

	public boolean issetShopClassId()
	{
		return this.shopClassId_u != 0;
	}
	/**
	 * 获取店铺类目id_u
	 * 
	 * 此字段的版本 >= 4
	 * @return shopClassId_u value 类型为:short
	 * 
	 */
	public short getShopClassId_u()
	{
		return shopClassId_u;
	}


	/**
	 * 设置店铺类目id_u
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopClassId_u(short value)
	{
		this.shopClassId_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(APIUserProfile)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 1;  //计算字段sex的长度 size_of(uint8_t)
				length += 1;  //计算字段loginLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段authMask的长度 size_of(uint8_t)
				length += 1;  //计算字段province的长度 size_of(uint8_t)
				length += 1;  //计算字段commContactType的长度 size_of(uint8_t)
				length += 1;  //计算字段identitycardType的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginType的长度 size_of(uint8_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sex_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段authMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段province_u的长度 size_of(uint8_t)
				length += 1;  //计算字段commContactType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identitycardType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段regTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段country_u的长度 size_of(uint8_t)
				length += 1;  //计算字段honorMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userInfoMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginIp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段modifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nickName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段c2CPasswd_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mobile_u的长度 size_of(uint8_t)
				length += 1;  //计算字段email_u的长度 size_of(uint8_t)
				length += 1;  //计算字段referCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mainLogoPos_u的长度 size_of(uint8_t)
				length += 1;  //计算字段name_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityCardNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityIndex_u的长度 size_of(uint8_t)
				length += 1;  //计算字段phone_u的长度 size_of(uint8_t)
				length += 1;  //计算字段fax_u的长度 size_of(uint8_t)
				length += 1;  //计算字段postCode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段address_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sproperty_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyerCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvMsgMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段city_u的长度 size_of(uint8_t)
				length += 1;  //计算字段prop_u的长度 size_of(uint8_t)
				length += 4;  //计算字段userId的长度 size_of(uint32_t)
				length += 4;  //计算字段regTime的长度 size_of(uint32_t)
				length += 4;  //计算字段country的长度 size_of(uint32_t)
				length += 4;  //计算字段honorMask的长度 size_of(uint32_t)
				length += 4;  //计算字段userInfoMask的长度 size_of(uint32_t)
				length += 4;  //计算字段lastLoginIp的长度 size_of(uint32_t)
				length += 4;  //计算字段lastLoginTime的长度 size_of(uint32_t)
				length += 4;  //计算字段modifyTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(nickName, null);  //计算字段nickName的长度 size_of(String)
				length += ByteStream.getObjectSize(c2CPasswd, null);  //计算字段c2CPasswd的长度 size_of(String)
				length += ByteStream.getObjectSize(mobile, null);  //计算字段mobile的长度 size_of(String)
				length += ByteStream.getObjectSize(email, null);  //计算字段email的长度 size_of(String)
				length += ByteStream.getObjectSize(referCredit, null);  //计算字段referCredit的长度 size_of(String)
				length += ByteStream.getObjectSize(mainLogoPos, null);  //计算字段mainLogoPos的长度 size_of(String)
				length += ByteStream.getObjectSize(name, null);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(identityCardNum, null);  //计算字段identityCardNum的长度 size_of(String)
				length += ByteStream.getObjectSize(identityIndex, null);  //计算字段identityIndex的长度 size_of(String)
				length += ByteStream.getObjectSize(phone, null);  //计算字段phone的长度 size_of(String)
				length += ByteStream.getObjectSize(fax, null);  //计算字段fax的长度 size_of(String)
				length += ByteStream.getObjectSize(postCode, null);  //计算字段postCode的长度 size_of(String)
				length += ByteStream.getObjectSize(address, null);  //计算字段address的长度 size_of(String)
				length += ByteStream.getObjectSize(sproperty, null);  //计算字段sproperty的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve, null);  //计算字段reserve的长度 size_of(String)
				length += 4;  //计算字段buyerCredit的长度 size_of(int)
				length += 4;  //计算字段sellerCredit的长度 size_of(int)
				length += 2;  //计算字段recvMsgMask的长度 size_of(uint16_t)
				length += 2;  //计算字段city的长度 size_of(uint16_t)
				length += ByteStream.getObjectSize(prop, null);  //计算字段prop的长度 size_of(Map)
				if(  this.version >= 1 ){
						length += 1;  //计算字段virtualCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 1 ){
						length += 1;  //计算字段objectCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 1 ){
						length += 1;  //计算字段region_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段virtualCredit的长度 size_of(int)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段objectCredit的长度 size_of(int)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段region的长度 size_of(uint32_t)
				}
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(contacts, null);  //计算字段contacts的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段contacts_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += ByteStream.getObjectSize(cftAccount, null);  //计算字段cftAccount的长度 size_of(String)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段cftAccount_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += ByteStream.getObjectSize(tempStr, null);  //计算字段tempStr的长度 size_of(String)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段tempStr_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += 4;  //计算字段shopClassId的长度 size_of(uint32_t)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段shopClassId_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(APIUserProfile)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 1;  //计算字段sex的长度 size_of(uint8_t)
				length += 1;  //计算字段loginLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段authMask的长度 size_of(uint8_t)
				length += 1;  //计算字段province的长度 size_of(uint8_t)
				length += 1;  //计算字段commContactType的长度 size_of(uint8_t)
				length += 1;  //计算字段identitycardType的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginType的长度 size_of(uint8_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sex_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段authMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段province_u的长度 size_of(uint8_t)
				length += 1;  //计算字段commContactType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identitycardType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段regTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段country_u的长度 size_of(uint8_t)
				length += 1;  //计算字段honorMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userInfoMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginIp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastLoginTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段modifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nickName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段c2CPasswd_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mobile_u的长度 size_of(uint8_t)
				length += 1;  //计算字段email_u的长度 size_of(uint8_t)
				length += 1;  //计算字段referCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mainLogoPos_u的长度 size_of(uint8_t)
				length += 1;  //计算字段name_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityCardNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityIndex_u的长度 size_of(uint8_t)
				length += 1;  //计算字段phone_u的长度 size_of(uint8_t)
				length += 1;  //计算字段fax_u的长度 size_of(uint8_t)
				length += 1;  //计算字段postCode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段address_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sproperty_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyerCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recvMsgMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段city_u的长度 size_of(uint8_t)
				length += 1;  //计算字段prop_u的长度 size_of(uint8_t)
				length += 4;  //计算字段userId的长度 size_of(uint32_t)
				length += 4;  //计算字段regTime的长度 size_of(uint32_t)
				length += 4;  //计算字段country的长度 size_of(uint32_t)
				length += 4;  //计算字段honorMask的长度 size_of(uint32_t)
				length += 4;  //计算字段userInfoMask的长度 size_of(uint32_t)
				length += 4;  //计算字段lastLoginIp的长度 size_of(uint32_t)
				length += 4;  //计算字段lastLoginTime的长度 size_of(uint32_t)
				length += 4;  //计算字段modifyTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(nickName, encoding);  //计算字段nickName的长度 size_of(String)
				length += ByteStream.getObjectSize(c2CPasswd, encoding);  //计算字段c2CPasswd的长度 size_of(String)
				length += ByteStream.getObjectSize(mobile, encoding);  //计算字段mobile的长度 size_of(String)
				length += ByteStream.getObjectSize(email, encoding);  //计算字段email的长度 size_of(String)
				length += ByteStream.getObjectSize(referCredit, encoding);  //计算字段referCredit的长度 size_of(String)
				length += ByteStream.getObjectSize(mainLogoPos, encoding);  //计算字段mainLogoPos的长度 size_of(String)
				length += ByteStream.getObjectSize(name, encoding);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(identityCardNum, encoding);  //计算字段identityCardNum的长度 size_of(String)
				length += ByteStream.getObjectSize(identityIndex, encoding);  //计算字段identityIndex的长度 size_of(String)
				length += ByteStream.getObjectSize(phone, encoding);  //计算字段phone的长度 size_of(String)
				length += ByteStream.getObjectSize(fax, encoding);  //计算字段fax的长度 size_of(String)
				length += ByteStream.getObjectSize(postCode, encoding);  //计算字段postCode的长度 size_of(String)
				length += ByteStream.getObjectSize(address, encoding);  //计算字段address的长度 size_of(String)
				length += ByteStream.getObjectSize(sproperty, encoding);  //计算字段sproperty的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve, encoding);  //计算字段reserve的长度 size_of(String)
				length += 4;  //计算字段buyerCredit的长度 size_of(int)
				length += 4;  //计算字段sellerCredit的长度 size_of(int)
				length += 2;  //计算字段recvMsgMask的长度 size_of(uint16_t)
				length += 2;  //计算字段city的长度 size_of(uint16_t)
				length += ByteStream.getObjectSize(prop, encoding);  //计算字段prop的长度 size_of(Map)
				if(  this.version >= 1 ){
						length += 1;  //计算字段virtualCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 1 ){
						length += 1;  //计算字段objectCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 1 ){
						length += 1;  //计算字段region_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段virtualCredit的长度 size_of(int)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段objectCredit的长度 size_of(int)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段region的长度 size_of(uint32_t)
				}
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(contacts, encoding);  //计算字段contacts的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段contacts_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += ByteStream.getObjectSize(cftAccount, encoding);  //计算字段cftAccount的长度 size_of(String)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段cftAccount_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += ByteStream.getObjectSize(tempStr, encoding);  //计算字段tempStr的长度 size_of(String)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段tempStr_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += 4;  //计算字段shopClassId的长度 size_of(uint32_t)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段shopClassId_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	short version;///<用户Po
 *	short sex;///<性别
 *	short loginLevel;///<登录级别
 *	short authMask;///<认证属性
 *	short province;///<省份
 *	short commContactType;///<默认联系方式
 *	short identitycardType;///<证件类别
 *	short lastLoginType;///<最后登录类型
 *	short version_u;///<版本_u
 *	short sex_u;///<性别_u
 *	short loginLevel_u;///<登录级别_u
 *	short authMask_u;///<认证属性_u
 *	short province_u;///<省份_u
 *	short commContactType_u;///<默认联系方式_u
 *	short identitycardType_u;///<证件类别_u
 *	short lastLoginType_u;///<最后登录类型_u
 *	short userId_u;///<用户QQ号码_u
 *	short regTime_u;///<注册时间_u
 *	short country_u;///<国家_u
 *	short honorMask_u;
 *	short userInfoMask_u;
 *	short lastLoginIp_u;///<最后登录ip_u
 *	short lastLoginTime_u;///<最后登录时间_u
 *	short modifyTime_u;///<最后更新时间_u
 *	short nickName_u;///<昵称_u
 *	short c2CPasswd_u;///<交易密码_u
 *	short mobile_u;///<手机_u
 *	short email_u;///<邮箱_u
 *	short referCredit_u;///<参考信用_u
 *	short mainLogoPos_u;///<肖像_u
 *	short name_u;///<真实姓名_u
 *	short identityCardNum_u;///<证件号码_u
 *	short identityIndex_u;
 *	short phone_u;///<固定电话_u
 *	short fax_u;///<传真_u
 *	short postCode_u;///<邮政编码_u
 *	short address_u;///<家庭住址_u
 *	short sproperty_u;///<用户全部属性_u
 *	short reserve_u;///<保留字_u
 *	short buyerCredit_u;///<买家信用_u
 *	short sellerCredit_u;///<卖家信用_u
 *	short recvMsgMask_u;
 *	short city_u;///<城市_u
 *	short prop_u;///<用户属性map_u
 *	long userId;///<用户QQ号码
 *	long regTime;///<注册时间
 *	long country;///<国家
 *	long honorMask;
 *	long userInfoMask;
 *	long lastLoginIp;///<最后登录IP
 *	long lastLoginTime;///<最后登录时间
 *	long modifyTime;///<最后更新时间
 *	String nickName;///<昵称
 *	String c2CPasswd;///<交易密码
 *	String mobile;///<手机
 *	String email;///<邮箱
 *	String referCredit;
 *	String mainLogoPos;///<肖像图
 *	String name;///<真实姓名
 *	String identityCardNum;///<证件号码
 *	String identityIndex;
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	String postCode;///<邮政编码
 *	String address;///<家庭住址
 *	String sproperty;///<用户全部属性，用于前台js
 *	String reserve;///<保留字
 *	int buyerCredit;///<买家信用
 *	int sellerCredit;///<卖家信用
 *	int recvMsgMask;
 *	int city;///<城市
 *	Map<uint16_t,uint8_t> prop;///<用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}
 *	short virtualCredit_u;///<虚拟信用_u
 *	short objectCredit_u;///<实物信用_u
 *	short region_u;///<地区_u
 *	int virtualCredit;///<虚拟信用
 *	int objectCredit;///<实物信用
 *	long region;///<地区
 *****以上是版本1所包含的字段*******
 *
 *****以下是版本2所包含的字段*******
 *	short version;///<用户Po
 *	short sex;///<性别
 *	short loginLevel;///<登录级别
 *	short authMask;///<认证属性
 *	short province;///<省份
 *	short commContactType;///<默认联系方式
 *	short identitycardType;///<证件类别
 *	short lastLoginType;///<最后登录类型
 *	short version_u;///<版本_u
 *	short sex_u;///<性别_u
 *	short loginLevel_u;///<登录级别_u
 *	short authMask_u;///<认证属性_u
 *	short province_u;///<省份_u
 *	short commContactType_u;///<默认联系方式_u
 *	short identitycardType_u;///<证件类别_u
 *	short lastLoginType_u;///<最后登录类型_u
 *	short userId_u;///<用户QQ号码_u
 *	short regTime_u;///<注册时间_u
 *	short country_u;///<国家_u
 *	short honorMask_u;
 *	short userInfoMask_u;
 *	short lastLoginIp_u;///<最后登录ip_u
 *	short lastLoginTime_u;///<最后登录时间_u
 *	short modifyTime_u;///<最后更新时间_u
 *	short nickName_u;///<昵称_u
 *	short c2CPasswd_u;///<交易密码_u
 *	short mobile_u;///<手机_u
 *	short email_u;///<邮箱_u
 *	short referCredit_u;///<参考信用_u
 *	short mainLogoPos_u;///<肖像_u
 *	short name_u;///<真实姓名_u
 *	short identityCardNum_u;///<证件号码_u
 *	short identityIndex_u;
 *	short phone_u;///<固定电话_u
 *	short fax_u;///<传真_u
 *	short postCode_u;///<邮政编码_u
 *	short address_u;///<家庭住址_u
 *	short sproperty_u;///<用户全部属性_u
 *	short reserve_u;///<保留字_u
 *	short buyerCredit_u;///<买家信用_u
 *	short sellerCredit_u;///<卖家信用_u
 *	short recvMsgMask_u;
 *	short city_u;///<城市_u
 *	short prop_u;///<用户属性map_u
 *	long userId;///<用户QQ号码
 *	long regTime;///<注册时间
 *	long country;///<国家
 *	long honorMask;
 *	long userInfoMask;
 *	long lastLoginIp;///<最后登录IP
 *	long lastLoginTime;///<最后登录时间
 *	long modifyTime;///<最后更新时间
 *	String nickName;///<昵称
 *	String c2CPasswd;///<交易密码
 *	String mobile;///<手机
 *	String email;///<邮箱
 *	String referCredit;
 *	String mainLogoPos;///<肖像图
 *	String name;///<真实姓名
 *	String identityCardNum;///<证件号码
 *	String identityIndex;
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	String postCode;///<邮政编码
 *	String address;///<家庭住址
 *	String sproperty;///<用户全部属性，用于前台js
 *	String reserve;///<保留字
 *	int buyerCredit;///<买家信用
 *	int sellerCredit;///<卖家信用
 *	int recvMsgMask;
 *	int city;///<城市
 *	Map<uint16_t,uint8_t> prop;///<用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}
 *	short virtualCredit_u;///<虚拟信用_u
 *	short objectCredit_u;///<实物信用_u
 *	short region_u;///<地区_u
 *	int virtualCredit;///<虚拟信用
 *	int objectCredit;///<实物信用
 *	long region;///<地区
 *	String contacts;///<卖家联系方式
 *	short contacts_u;///<卖家联系方式_u
 *****以上是版本2所包含的字段*******
 *
 *****以下是版本3所包含的字段*******
 *	short version;///<用户Po
 *	short sex;///<性别
 *	short loginLevel;///<登录级别
 *	short authMask;///<认证属性
 *	short province;///<省份
 *	short commContactType;///<默认联系方式
 *	short identitycardType;///<证件类别
 *	short lastLoginType;///<最后登录类型
 *	short version_u;///<版本_u
 *	short sex_u;///<性别_u
 *	short loginLevel_u;///<登录级别_u
 *	short authMask_u;///<认证属性_u
 *	short province_u;///<省份_u
 *	short commContactType_u;///<默认联系方式_u
 *	short identitycardType_u;///<证件类别_u
 *	short lastLoginType_u;///<最后登录类型_u
 *	short userId_u;///<用户QQ号码_u
 *	short regTime_u;///<注册时间_u
 *	short country_u;///<国家_u
 *	short honorMask_u;
 *	short userInfoMask_u;
 *	short lastLoginIp_u;///<最后登录ip_u
 *	short lastLoginTime_u;///<最后登录时间_u
 *	short modifyTime_u;///<最后更新时间_u
 *	short nickName_u;///<昵称_u
 *	short c2CPasswd_u;///<交易密码_u
 *	short mobile_u;///<手机_u
 *	short email_u;///<邮箱_u
 *	short referCredit_u;///<参考信用_u
 *	short mainLogoPos_u;///<肖像_u
 *	short name_u;///<真实姓名_u
 *	short identityCardNum_u;///<证件号码_u
 *	short identityIndex_u;
 *	short phone_u;///<固定电话_u
 *	short fax_u;///<传真_u
 *	short postCode_u;///<邮政编码_u
 *	short address_u;///<家庭住址_u
 *	short sproperty_u;///<用户全部属性_u
 *	short reserve_u;///<保留字_u
 *	short buyerCredit_u;///<买家信用_u
 *	short sellerCredit_u;///<卖家信用_u
 *	short recvMsgMask_u;
 *	short city_u;///<城市_u
 *	short prop_u;///<用户属性map_u
 *	long userId;///<用户QQ号码
 *	long regTime;///<注册时间
 *	long country;///<国家
 *	long honorMask;
 *	long userInfoMask;
 *	long lastLoginIp;///<最后登录IP
 *	long lastLoginTime;///<最后登录时间
 *	long modifyTime;///<最后更新时间
 *	String nickName;///<昵称
 *	String c2CPasswd;///<交易密码
 *	String mobile;///<手机
 *	String email;///<邮箱
 *	String referCredit;
 *	String mainLogoPos;///<肖像图
 *	String name;///<真实姓名
 *	String identityCardNum;///<证件号码
 *	String identityIndex;
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	String postCode;///<邮政编码
 *	String address;///<家庭住址
 *	String sproperty;///<用户全部属性，用于前台js
 *	String reserve;///<保留字
 *	int buyerCredit;///<买家信用
 *	int sellerCredit;///<卖家信用
 *	int recvMsgMask;
 *	int city;///<城市
 *	Map<uint16_t,uint8_t> prop;///<用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}
 *	short virtualCredit_u;///<虚拟信用_u
 *	short objectCredit_u;///<实物信用_u
 *	short region_u;///<地区_u
 *	int virtualCredit;///<虚拟信用
 *	int objectCredit;///<实物信用
 *	long region;///<地区
 *	String contacts;///<卖家联系方式
 *	short contacts_u;///<卖家联系方式_u
 *	long sellerUin;///<卖家uin
 *	short sellerUin_u;///<卖家uin_u
 *****以上是版本3所包含的字段*******
 *
 *****以下是版本4所包含的字段*******
 *	short version;///<用户Po
 *	short sex;///<性别
 *	short loginLevel;///<登录级别
 *	short authMask;///<认证属性
 *	short province;///<省份
 *	short commContactType;///<默认联系方式
 *	short identitycardType;///<证件类别
 *	short lastLoginType;///<最后登录类型
 *	short version_u;///<版本_u
 *	short sex_u;///<性别_u
 *	short loginLevel_u;///<登录级别_u
 *	short authMask_u;///<认证属性_u
 *	short province_u;///<省份_u
 *	short commContactType_u;///<默认联系方式_u
 *	short identitycardType_u;///<证件类别_u
 *	short lastLoginType_u;///<最后登录类型_u
 *	short userId_u;///<用户QQ号码_u
 *	short regTime_u;///<注册时间_u
 *	short country_u;///<国家_u
 *	short honorMask_u;
 *	short userInfoMask_u;
 *	short lastLoginIp_u;///<最后登录ip_u
 *	short lastLoginTime_u;///<最后登录时间_u
 *	short modifyTime_u;///<最后更新时间_u
 *	short nickName_u;///<昵称_u
 *	short c2CPasswd_u;///<交易密码_u
 *	short mobile_u;///<手机_u
 *	short email_u;///<邮箱_u
 *	short referCredit_u;///<参考信用_u
 *	short mainLogoPos_u;///<肖像_u
 *	short name_u;///<真实姓名_u
 *	short identityCardNum_u;///<证件号码_u
 *	short identityIndex_u;
 *	short phone_u;///<固定电话_u
 *	short fax_u;///<传真_u
 *	short postCode_u;///<邮政编码_u
 *	short address_u;///<家庭住址_u
 *	short sproperty_u;///<用户全部属性_u
 *	short reserve_u;///<保留字_u
 *	short buyerCredit_u;///<买家信用_u
 *	short sellerCredit_u;///<卖家信用_u
 *	short recvMsgMask_u;
 *	short city_u;///<城市_u
 *	short prop_u;///<用户属性map_u
 *	long userId;///<用户QQ号码
 *	long regTime;///<注册时间
 *	long country;///<国家
 *	long honorMask;
 *	long userInfoMask;
 *	long lastLoginIp;///<最后登录IP
 *	long lastLoginTime;///<最后登录时间
 *	long modifyTime;///<最后更新时间
 *	String nickName;///<昵称
 *	String c2CPasswd;///<交易密码
 *	String mobile;///<手机
 *	String email;///<邮箱
 *	String referCredit;
 *	String mainLogoPos;///<肖像图
 *	String name;///<真实姓名
 *	String identityCardNum;///<证件号码
 *	String identityIndex;
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	String postCode;///<邮政编码
 *	String address;///<家庭住址
 *	String sproperty;///<用户全部属性，用于前台js
 *	String reserve;///<保留字
 *	int buyerCredit;///<买家信用
 *	int sellerCredit;///<卖家信用
 *	int recvMsgMask;
 *	int city;///<城市
 *	Map<uint16_t,uint8_t> prop;///<用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}
 *	short virtualCredit_u;///<虚拟信用_u
 *	short objectCredit_u;///<实物信用_u
 *	short region_u;///<地区_u
 *	int virtualCredit;///<虚拟信用
 *	int objectCredit;///<实物信用
 *	long region;///<地区
 *	String contacts;///<卖家联系方式
 *	short contacts_u;///<卖家联系方式_u
 *	long sellerUin;///<卖家uin
 *	short sellerUin_u;///<卖家uin_u
 *	String cftAccount;///<财付通账户
 *	short cftAccount_u;///<财付通账户_u
 *	String tempStr;///<备用字符串
 *	short tempStr_u;///<备用字符串_u
 *	long shopClassId;///<店铺类目id
 *	short shopClassId_u;///<店铺类目id_u
 *****以上是版本4所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
