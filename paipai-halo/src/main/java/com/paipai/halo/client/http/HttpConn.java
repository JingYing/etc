package com.paipai.halo.client.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;

import com.paipai.halo.client.log.PacketLogParser;

public interface HttpConn {
	
	HttpResp connect(String method, String url, ReqHead head, InputStream body, PacketLogParser logParser) throws IOException;
	
	void setProxy(Proxy proxy);
	
}
