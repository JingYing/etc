//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.UserWangGouBjAo.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *微信公众号登陆下默认登陆类型do
 *
 *@date 2015-03-27 09:38:57
 *
 *@since version:0
*/
public class WechatDfLoginTypeDo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 微信openid
	 *
	 * 版本 >= 0
	 */
	 private String openid = new String();

	/**
	 * 默认登陆类型
	 *
	 * 版本 >= 0
	 */
	 private long defaultLoginType;

	/**
	 * 添加时间
	 *
	 * 版本 >= 0
	 */
	 private long addTime;

	/**
	 * 预留
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushString(openid);
		bs.pushUInt(defaultLoginType);
		bs.pushUInt(addTime);
		bs.pushString(Reserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		openid = bs.popString();
		defaultLoginType = bs.popUInt();
		addTime = bs.popUInt();
		Reserve = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取微信openid
	 * 
	 * 此字段的版本 >= 0
	 * @return openid value 类型为:String
	 * 
	 */
	public String getOpenid()
	{
		return openid;
	}


	/**
	 * 设置微信openid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenid(String value)
	{
		this.openid = value;
	}


	/**
	 * 获取默认登陆类型
	 * 
	 * 此字段的版本 >= 0
	 * @return defaultLoginType value 类型为:long
	 * 
	 */
	public long getDefaultLoginType()
	{
		return defaultLoginType;
	}


	/**
	 * 设置默认登陆类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDefaultLoginType(long value)
	{
		this.defaultLoginType = value;
	}


	/**
	 * 获取添加时间
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return addTime;
	}


	/**
	 * 设置添加时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.addTime = value;
	}


	/**
	 * 获取预留
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(WechatDfLoginTypeDo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(openid, null);  //计算字段openid的长度 size_of(String)
				length += 4;  //计算字段defaultLoginType的长度 size_of(uint32_t)
				length += 4;  //计算字段addTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(WechatDfLoginTypeDo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(openid, encoding);  //计算字段openid的长度 size_of(String)
				length += 4;  //计算字段defaultLoginType的长度 size_of(uint32_t)
				length += 4;  //计算字段addTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
