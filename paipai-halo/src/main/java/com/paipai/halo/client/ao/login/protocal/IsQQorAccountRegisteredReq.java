 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.UserIcsonAo.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *req
 *
 *@date 2015-03-27 09:28:06
 *
 *@since version:0
*/
public class  IsQQorAccountRegisteredReq extends NetMessage
{
	/**
	 * 机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源，必需, 请填调用方自己的源文件名称
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id，默认填0, 请联系joelwzli获取
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * 用户帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber必填) 2-个性化帐号(填2则loginAccount必填)
	 *
	 * 版本 >= 0
	 */
	 private short accountType;

	/**
	 * 用户QQ号，accountType填1时必填，目前仅支持32位
	 *
	 * 版本 >= 0
	 */
	 private long qQNumber;

	/**
	 * 个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
	 *
	 * 版本 >= 0
	 */
	 private String loginAccount = new String();

	/**
	 * 输入保留字，无需设置，传空字符串即可
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(sceneId);
		bs.pushUByte(accountType);
		bs.pushLong(qQNumber);
		bs.pushString(loginAccount);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		sceneId = bs.popUInt();
		accountType = bs.popUByte();
		qQNumber = bs.popLong();
		loginAccount = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xA0A91805L;
	}


	/**
	 * 获取机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id，默认填0, 请联系joelwzli获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景id，默认填0, 请联系joelwzli获取
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取用户帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber必填) 2-个性化帐号(填2则loginAccount必填)
	 * 
	 * 此字段的版本 >= 0
	 * @return accountType value 类型为:short
	 * 
	 */
	public short getAccountType()
	{
		return accountType;
	}


	/**
	 * 设置用户帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber必填) 2-个性化帐号(填2则loginAccount必填)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAccountType(short value)
	{
		this.accountType = value;
	}


	/**
	 * 获取用户QQ号，accountType填1时必填，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @return qQNumber value 类型为:long
	 * 
	 */
	public long getQQNumber()
	{
		return qQNumber;
	}


	/**
	 * 设置用户QQ号，accountType填1时必填，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setQQNumber(long value)
	{
		this.qQNumber = value;
	}


	/**
	 * 获取个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return loginAccount value 类型为:String
	 * 
	 */
	public String getLoginAccount()
	{
		return loginAccount;
	}


	/**
	 * 设置个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLoginAccount(String value)
	{
		this.loginAccount = value;
	}


	/**
	 * 获取输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(IsQQorAccountRegisteredReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += 1;  //计算字段accountType的长度 size_of(uint8_t)
				length += 17;  //计算字段qQNumber的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(loginAccount, null);  //计算字段loginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(IsQQorAccountRegisteredReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += 1;  //计算字段accountType的长度 size_of(uint8_t)
				length += 17;  //计算字段qQNumber的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(loginAccount, encoding);  //计算字段loginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
