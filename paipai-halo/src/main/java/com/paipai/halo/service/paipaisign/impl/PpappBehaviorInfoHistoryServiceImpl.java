/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.dao.paipaisign.PpappBehaviorInfoHistoryDao;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfoHistory;
import com.paipai.halo.service.paipaisign.PpappBehaviorInfoHistoryService;

/**
 * PpappBehaviorInfoHistoryService 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Service("ppappBehaviorInfoHistoryService")
@Transactional
public class PpappBehaviorInfoHistoryServiceImpl  implements PpappBehaviorInfoHistoryService {
	
	@Resource private PpappBehaviorInfoHistoryDao ppappBehaviorInfoHistoryDao;
	
	public BaseDao<PpappBehaviorInfoHistory,String> getDao() {
		return ppappBehaviorInfoHistoryDao;
	}
}