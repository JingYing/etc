/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign;

import java.util.List;

import com.paipai.halo.domain.paipaisign.PpappBehaviorInfo;

/**
 * PpappBehaviorInfoService接口
 * @author J-ONE
 * @since 2015-04-09
 */
public interface PpappBehaviorInfoService  {
	
	public List<PpappBehaviorInfo> behaviorInfo(long wid, int activeId, int type, String beginTime, String endTime);
}