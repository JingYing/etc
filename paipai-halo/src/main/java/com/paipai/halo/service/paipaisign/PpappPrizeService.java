/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.domain.paipaisign.PpappPrizePo;

/**
 * PpappPrizeService接口
 * @author J-ONE
 * @since 2015-04-09
 */
public interface PpappPrizeService   {
	
	/**
	 * 根据活动ID，奖品等级及时间活动对应的奖品及库存信息
	 * @param activeId
	 * @param prizeLevel
	 * @param type
	 * @param activeDay
	 * @return
	 * @throws Exception
	 */
	public List<PpappPrizePo> findPrize(int activeId, int prizeLevel, int type, String activeDay) throws BusinessException,Exception ;
	
	/**
	 * 领奖
	 * @param activeId
	 * @param prizeId
	 * @param wid
	 * @param sk
	 * @param addId
	 * @param mk
	 * @param mk2
	 * @param request
	 * @param remark
	 * @return map [num:当前使用的点券数,totalNum:总点券数,activeId:当前活动ID]
	 * @throws BusinessException
	 * @throws Exception
	 */
	public Map<String, Object> receivePrize(int activeId, String prizeId, long wid, String sk, long addId, String mk, String mk2, HttpServletRequest request, String remark) throws BusinessException,Exception ;
	/**
	 * 领券
	 * @param activeId
	 * @param wid
	 * @param sk
	 * @param mk
	 * @param mk2
	 * @param request
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public Map<String, Object> receiveVouchers(int activeId, long wid, String sk, String mk, String mk2, HttpServletRequest request) throws BusinessException,Exception ;

	/**
	 * 通过缓存获取奖品详情
	 * @param prizeId
	 * @param activeId
	 * @param type 0：今日奖品快照   1：非今日奖品情况
	 * @param activeDay
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public PpappPrizePo getPpappPrizePoByCache(String prizeId, int activeId, int type, String activeDay) throws BusinessException,Exception ;
	
	/**
	 * 根据奖品ID获取奖品信息
	 * @param activeId
	 * @param prizeIdList
	 * @param type
	 * @param activeDay
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public List<PpappPrizePo> prizeInfo(int activeId, String prizeIdList, int type, String activeDay) throws BusinessException,Exception ;
	
	/**
	 * 删除奖品列表缓存，活动上线，奖品确认后可以屏蔽该接口
	 */
	public void removePrizeListCache() throws Exception ;
	
}