package com.paipai.halo.service;

import java.util.List;

import com.paipai.halo.common.PageEntity;
import com.paipai.halo.common.PagingResult;
import com.paipai.halo.domain.Comment;
import com.paipai.halo.domain.ItemLikes;
import com.paipai.halo.domain.LikeLogInfo;
import com.paipai.halo.domain.UserBaseInfo;

/**
 * 
  * @ClassName: ShopAroService
  * @Description: 购物圏业务逻辑的接口
  * @author Comsys-lianghaining1
  * @date 2015年3月17日 下午5:14:54
  *
 */
public interface ShopAroService {
	/**
	 * @throws Exception 
	 * 
	  * 更新头像
	  * @Title: saveorupdate
	  * @Description: TODO
	  * @param @param userBaseInfo    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	void saveorupdate(UserBaseInfo userBaseInfo) throws Exception;
	/**
	 * @throws Exception 
	 * 
	  * 设计点赞
	  * @Title: setLike
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @param ownerid
	  * @param @return    设定文件
	  * @return String    返回类型
	  * @throws
	 */
	String setLike(String itemid, Integer itemtype, Long wid, Long ownerid) throws Exception;
	/**
	 * @throws Exception 
	 * 
	  * 取消点赞
	  * @Title: cancelLike
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @return    设定文件
	  * @return String    返回类型
	  * @throws
	 */
	String cancelLike(String itemid, Integer itemtype, Long wid) throws Exception;
	/**
	 * 
	  * @Title: getLikeNum
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @return    设定文件
	  * @return String    返回类型
	  * @throws
	 */
	ItemLikes getLikeNum(String itemid, Integer itemtype);
	/**
	 * 
	  * 获取用户信息
	  * @Title: getUserInfo
	  * @Description: TODO
	  * @param @param wid
	  * @param @return    设定文件
	  * @return UserBaseInfo    返回类型
	  * @throws
	 */
	UserBaseInfo getUserInfo(Long wid);
	/**
	 * 
	  *分页查询
	  * @Title: getPgaRanges
	  * @Description: TODO
	  * @param @param pe
	  * @param @return    设定文件
	  * @return PagingResult<Comment>    返回类型
	  * @throws
	 */
	PagingResult<Comment> getPgaRanges(PageEntity pe);
	/**
	 * 查出点赞人信息
	  * @Title: getLikePgaRanges
	  * @Description: TODO
	  * @param @param pe
	  * @param @return    设定文件
	  * @return PagingResult<LikeLogInfo>    返回类型
	  * @throws
	 */
	PagingResult<LikeLogInfo> getLikePgaRanges(PageEntity pe);
	/**
	 * @param strictLeave 
	 * @throws Exception 
	 * 
	  * @Title: 保存评论并返回 果josn
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @param ip
	  * @param @param title
	  * @param @param context
	  * @param @param ownerid
	  * @param @return    设定文件
	  * @return String    返回类型
	  * @throws
	 */
	String saveConmment(String itemid, int itemtype, Long wid, String ip,
			String title, String context, Long ownerid, int strictLeave) throws Exception;
	/**
	 * 
	  * @Title:指获取用户头像
	  * @Description: TODO
	  * @param @param wid
	  * @param @return    设定文件
	  * @return List<UserBaseInfo>    返回类型
	  * @throws
	 */
	List<UserBaseInfo> getUserList(Long [] wid);
	void lhntest(String context) throws Exception;
	/**
	 * 
	  * @Title:查出该人对该文张的信息
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @return    设定文件
	  * @return LikeLogInfo    返回类型
	  * @throws
	 */
	LikeLogInfo getLikeinfo(String itemid, int itemtype, Long wid);
	/**
	 * 
	  * @Title: 从缓存中查出前十个头像
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @return    设定文件
	  * @return List<UserBaseInfo>    返回类型
	  * @throws
	 */
	PagingResult<LikeLogInfo> getUserBaseByCache(String itemid, int itemtype);
	/**
	 * 
	  * @Title: 从缓存中取，如果取不到则放入缓存
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @return    设定文件
	  * @return PagingResult<Comment>    返回类型
	  * @throws
	 */
	PagingResult<Comment> getPageRangeByCache(String itemid, int itemtype);
	/**
	  * @Title: 查出该文章的点赞评论信息
	  * @Description: TODO
	  * @param @param id
	  * @param @param itemtype
	  * @param @return    设定文件
	  * @return ItemLikes    返回类型
	  * @throws
	 */
	ItemLikes getItemLike(String id, Integer itemtype);
	/**
	 * 
	显示没删除的评论
	  * @Description: TODO
	  * @param @return    设定文件
	  * @return List<ItemLikes>    返回类型
	  * @throws
	 */
	List<Comment> getNoshow();
	/**
	  * @Title: 放黑名单暂时放入缓存
	  * @Description: TODO
	  * @param @param wid
	  * @param @return    设定文件
	  * @return List<Long>    返回类型
	  * @throws
	 */
	String addBloack(long wid);
	String getBlackList();
	/**
	 * 
	  * @Title: 批量查出评论
	  * @Description: TODO
	  * @param @param ids
	  * @param @return    设定文件
	  * @return List<Comment>    返回类型
	  * @throws
	 */
	List<Comment> getCommentList(List<Long> ids);
	/**
	 * @param ids 
	 * @throws Exception 
	 * 
	  * @Title: 指更新用户的状态
	  * @Description: TODO
	  * @param @param uids    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	void updateUserBl(List<Long> uids, List<Long> ids,int status) throws Exception;
	/**
	 * @param i 
	 * 
	  * @Title: 查出所有的黑名单用户
	  * @Description: TODO
	  * @param @return    设定文件
	  * @return List<UserBaseInfo>    返回类型
	  * @throws
	 */
	List<UserBaseInfo> getBlUsers(int i);
	/**
	 * 
	  * @Title: 查出用户所有的评论
	  * @Description: TODO
	  * @param @param uids
	  * @param @return    设定文件
	  * @return List<Comment>    返回类型
	  * @throws
	 */
	List<Comment> getCommentListByUid(List<Long> uids);

}
