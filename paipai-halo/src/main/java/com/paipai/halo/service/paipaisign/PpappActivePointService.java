package com.paipai.halo.service.paipaisign;

import java.util.List;
import java.util.Map;

import com.google.gson.JsonObject;
import com.paipai.halo.domain.paipaisign.PpappActivePoint;
import com.paipai.halo.domain.paipaisign.SignPointCachePo;


/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */


/**
 * PpappActivePointService接口
 * @author J-ONE
 * @since 2015-04-09
 */
public interface PpappActivePointService {
	
	public List<PpappActivePoint> selectAll();
	
	public int add(PpappActivePoint activePoint) throws Exception;
	
	public List<PpappActivePoint> selectParam(Map<String, Object> param);
	
	/**
	 * 查询当前拥有的点数
	 * @param wid
	 * @param activeId
	 * @return
	 */
	public long selectCurrentPoint(long wid, int activeId);
	
	/**
	 * 从缓存中查询连续登陆天数和当前橡果数，没有则从数据库中取
	 * @param wid
	 * @param activeId
	 * @return
	 */
	public SignPointCachePo selectSignPointCache(long wid, int activeId);
	
	public JsonObject sign(long wid, int activeId, String mk, String mk2, String ip) throws Exception;
	
}