/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign;

import java.util.List;
import java.util.Map;

import com.paipai.halo.domain.paipaisign.PpappSignLog;

/**
 * PpappSignLogService接口
 * @author J-ONE
 * @since 2015-04-09
 */
public interface PpappSignLogService   {
	
	public List<PpappSignLog> getByParam(Map<String, Object> param);
	
	/**
	 * 查询连续登陆的天数
	 * @param wid
	 * @param activeId
	 * @return
	 */
	public int getSignDays(long wid, int activeId);
}