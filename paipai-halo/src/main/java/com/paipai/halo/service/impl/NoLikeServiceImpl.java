package com.paipai.halo.service.impl;

import javax.annotation.Resource;
import javax.mail.FetchProfile.Item;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.paipai.halo.common.TimeUtils;
import com.paipai.halo.dao.ShopAroItemDao;
import com.paipai.halo.dao.ShopAroNoLikeDao;
import com.paipai.halo.dao.ShopAroUserDao;
import com.paipai.halo.domain.ItemLikes;
import com.paipai.halo.domain.NoLikeInfo;
import com.paipai.halo.service.NoLikeService;

@Component(value = "nolikeService")
@Transactional
public class NoLikeServiceImpl implements NoLikeService {

	@Resource
	public ShopAroNoLikeDao shopAroNoLikeDao;

	@Resource
	public ShopAroUserDao shopAroUserDao;
	
	@Resource
	public ShopAroItemDao shopAroItemDao;

	@Override
	public NoLikeInfo getNolike(String itemid, Integer itemtype, Long wid) {
		NoLikeInfo noLikeInfo = new NoLikeInfo();
		noLikeInfo.setItemid(itemid);
		noLikeInfo.setItemtype(itemtype);
		noLikeInfo.setWid(wid);
		return this.shopAroNoLikeDao.getNoLikeInfo(noLikeInfo);
	}

	@Override
	public String setNoLike(String itemid, Integer itemtype, Long wid,
			Long ownerid) throws Exception {
		NoLikeInfo noLikeInfo = new NoLikeInfo();
		noLikeInfo.setItemid(itemid);
		noLikeInfo.setItemtype(itemtype);
		noLikeInfo.setWid(wid);
		
		ItemLikes itemLikes = new ItemLikes();
		itemLikes.setItemid(itemid);
		itemLikes.setItemtype(itemtype);
		itemLikes.setOwnerid(ownerid);
		//判断本次操作是踩尼还是取消踩呢？
		 NoLikeInfo nl = this.shopAroNoLikeDao.getNoLikeInfo(noLikeInfo);
		 if(nl==null||nl.getStatus()==0){//如果是空或者非空，但状态是0（踩过但已取消），则踩
			 if(nl==null){
				 noLikeInfo.setNolikenum(0);
				 noLikeInfo.setNoliketime(TimeUtils.getCurrentTime());
				 noLikeInfo.setStatus(1);
				 //插入数据
				 this.shopAroNoLikeDao.insert(noLikeInfo);
			 }else {
				nl.setStatus(1);
				nl.setNolikenum(nl.getNolikenum()+1);
				this.shopAroNoLikeDao.update(nl);
			}
			//然后更新最统计表
			 ItemLikes it = this.shopAroItemDao.getItemLikes(itemLikes);
			 if(it!=null){//如果有数据了，则更新
				// it.setNolikesnum(it.getNolikesnum()+1);
				 this.shopAroItemDao.updatenolikesnum(it);
			 }else {
				 itemLikes.setCommentnum(0);
				 //itemLikes.setNolikesnum(1);
				 itemLikes.setLikesnum(0);
				 this.shopAroItemDao.insert(itemLikes);
			}
			 
		 }else {//取消踩
			nl.setStatus(0);
			nl.setNolikenum((nl.getNolikenum()-1)<0?0:(nl.getNolikenum()-1));
			this.shopAroNoLikeDao.update(nl);
			//然后更新最统计表
			 ItemLikes it = this.shopAroItemDao.getItemLikes(itemLikes);
			 if(it!=null){//如果有数据了，则更新
				// it.setNolikesnum((it.getNolikesnum()-1)<0?0:(it.getNolikesnum()-1));
				 this.shopAroItemDao.updatenolikesnum(it);
			 }
		 }
		 
		 //更新缓存
		 
		 
		return null;
	}

}
