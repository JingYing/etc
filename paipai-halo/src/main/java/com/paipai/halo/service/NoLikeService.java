package com.paipai.halo.service;

import com.paipai.halo.domain.NoLikeInfo;

/**
 * 
  * @ClassName: 踩
  * @Description: TODO
  * @author Comsys-lianghaining1
  * @date 2015年4月22日 下午5:41:16
  *
 */
public interface NoLikeService {
	/**
	 * 
	  * @Title: 查出该人对该文章踩的信息
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @return    设定文件
	  * @return NoLikeInfo    返回类型
	  * @throws
	 */
	NoLikeInfo getNolike(String itemid, Integer itemtype, Long wid);
	/**
	 * @throws Exception 
	 * 
	  * @Title: setNoLike
	  * @Description: 点踩功能
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @param ownerid
	  * @param @return    设定文件
	  * @return String    返回类型
	  * @throws
	 */
	String setNoLike(String itemid, Integer itemtype, Long wid, Long ownerid) throws Exception;

}
