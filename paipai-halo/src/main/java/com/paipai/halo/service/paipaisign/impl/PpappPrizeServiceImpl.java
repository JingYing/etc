/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;



import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.jim.cli.Cluster;
import com.paipai.halo.client.ao.addr.RecvAddrClient;
import com.paipai.halo.client.ao.addr.protocol.ApiGetRecvaddrResp;
import com.paipai.halo.client.ao.addr.protocol.ApiRecvaddr;
import com.paipai.halo.common.Config;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.JIMConstant;
import com.paipai.halo.common.SerializeUtil;
import com.paipai.halo.common.TimeUtils;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.common.sign.PrizeRuleManager;
import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.dao.paipaisign.PpappActivePointDao;
import com.paipai.halo.dao.paipaisign.PpappBehaviorInfoDao;
import com.paipai.halo.dao.paipaisign.PpappPrizeDao;
import com.paipai.halo.domain.paipaisign.AddressPo;
import com.paipai.halo.domain.paipaisign.PpappActivePoint;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfo;
import com.paipai.halo.domain.paipaisign.PpappPrize;
import com.paipai.halo.domain.paipaisign.PpappPrizePo;
import com.paipai.halo.domain.paipaisign.SimplePrizePo;
import com.paipai.halo.service.paipaisign.PpappPrizeService;

/**
 * PpappPrizeService 实现类
 * 
 * @author J-ONE
 * @since 2015-04-09
 */
@Service("ppappPrizeService")
@Transactional
public class PpappPrizeServiceImpl implements PpappPrizeService {
	public static Logger run = LogManager.getLogger("run");
	public static Logger signActive = LogManager.getLogger("signActive");
	@Resource
	private PpappPrizeDao ppappPrizeDao;
	@Resource
	private PpappBehaviorInfoDao ppappBehaviorInfoDao;
	@Resource
	private PpappActivePointDao ppappActivePointDao;
	@Resource
	private Cluster jimClient;
	

	public BaseDao<PpappPrize, String> getDao() {
		return ppappPrizeDao;
	}

	@Override
	public List<PpappPrizePo> findPrize(int activeId, int prizeLevel, int type,
			String activeDay) throws BusinessException, Exception {
		// TODO Auto-generated method stub
		ArrayList<PpappPrizePo> ppappPrizePoList = new ArrayList<PpappPrizePo>() ;
		String key = JIMConstant.getPrizeListKey(prizeLevel, activeId) ;
		if (jimClient.exists(key)) {
			String[] prizeIdStrList = jimClient.get(key).split(JIMConstant.KEY_SEPARATOR) ;
			for (String prizeId : prizeIdStrList) {				
				PpappPrizePo ppappPrizePo = this.getPpappPrizePoByCache(prizeId, activeId, type, activeDay) ;
				if (ppappPrizePo == null) {
					continue ;
				}				
				ppappPrizePoList.add(ppappPrizePo);
			}
		} else {
			StringBuffer prizeListStr = new StringBuffer() ;
			HashMap<String, Object> paramMapForPrizeList = new HashMap<String, Object>();
			paramMapForPrizeList.put("activeId", activeId);
			paramMapForPrizeList.put("prizeLevel", prizeLevel);
			paramMapForPrizeList.put("status", 1);
			List<PpappPrize> ppappPrizeList = ppappPrizeDao
					.selectListByMap(paramMapForPrizeList);
			for (PpappPrize ppappPrize : ppappPrizeList) {
				PpappPrizePo ppappPrizePo = new PpappPrizePo();
				ppappPrizePo.setPrizeId(ppappPrize.getPrizeId());
				ppappPrizePo.setItemCode(ppappPrize.getItemCode());
				ppappPrizePo.setPrizeName(ppappPrize.getPrizeName());
				ppappPrizePo.setPrizeTotalNum(ppappPrize.getPrizeTotalNum());
				ppappPrizePo.setPrizePic(ppappPrize.getPrizePic());
				ppappPrizePo.setPrizePrice(ppappPrize.getPrizePrice());
				ppappPrizePo.setPrizeLevel(ppappPrize.getPrizeLevel()) ;
				if (0 == type) {
					String activeEndDay = Config.get("signActive."+ activeId +".endTime").substring(0, 8) ;
					long levelNum = 0;
					// 最后一天，奖品余额是所有没有使用的奖品
					if (activeDay.equals(activeEndDay)) {
						levelNum = ppappPrize.getPrizeTotalNum()
								- ppappPrize.getPrizeUsedNum();
						ppappPrizePo.setPrizeLimitNum(levelNum <= 0 ? 0 : levelNum);
						ppappPrizePo.setPrizeLeaveNum(levelNum <= 0 ? 0 : levelNum);
					} else {
						HashMap<String, Object> paramMapPrizeCount = new HashMap<String, Object>();
						paramMapPrizeCount.put("activeId", activeId);
						paramMapPrizeCount.put("ext5", ppappPrize.getPrizeId());
						paramMapPrizeCount.put("insertDay", activeDay);
						paramMapPrizeCount.put("type", 4);
						int sendNum = ppappBehaviorInfoDao
								.countPrizeNum(paramMapPrizeCount);
						levelNum = ppappPrize.getPrizeLimitNum() - sendNum;
						ppappPrizePo
								.setPrizeLimitNum(ppappPrize.getPrizeLimitNum());
						ppappPrizePo.setPrizeLeaveNum(levelNum <= 0 ? 0 : levelNum);
					}
				} else {
					ppappPrizePo.setPrizeLimitNum(ppappPrize.getPrizeLimitNum());
					ppappPrizePo.setPrizeLeaveNum(ppappPrize.getPrizeLimitNum());
				}
				ppappPrizePoList.add(ppappPrizePo);
				if (prizeListStr.toString().equals("")) {
					prizeListStr.append(ppappPrizePo.getPrizeId()) ;
				} else {
					prizeListStr.append(JIMConstant.KEY_SEPARATOR).append(ppappPrizePo.getPrizeId()) ;
				}				
			}	
			if (!StringUtils.isEmpty(prizeListStr.toString())) {
				jimClient.del(key) ;
				jimClient.setEx(key, prizeListStr.toString(), JIMConstant.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
			}
							
		}		
		return ppappPrizePoList;
	}

	@Override
	public Map<String, Object> receivePrize(int activeId, String prizeId,
			long wid, String sk, long addId, String mk, String mk2, HttpServletRequest request,
			String remark) throws BusinessException, Exception {
		ApiGetRecvaddrResp resp = null ;
		if (Config.isDevEnv()) {
			resp = new ApiGetRecvaddrResp() ;
		} else {
			resp = RecvAddrClient.getRecvaddr(addId, wid, sk, mk);
		}		
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException.createInstance(ErrConstant.IDL_FAILURE,
					"无法获取用户收货地址");
		}
		ApiRecvaddr apiRecvaddr = resp.getApiRecvaddr();
		// 地址快照
		AddressPo addressPo = new AddressPo();
		addressPo.setAddrId(apiRecvaddr.getAddrId());
		addressPo.setPostCode(apiRecvaddr.getPostCode());
		addressPo.setRecvAddress(apiRecvaddr.getRecvAddress());
		addressPo.setRecvCity(apiRecvaddr.getRecvCity());
		addressPo.setRecvDistric(apiRecvaddr.getRecvDistric());
		addressPo.setRecvMobile(apiRecvaddr.getRecvMobile());
		addressPo.setRecvName(apiRecvaddr.getRecvName());
		addressPo.setRecvPhone(apiRecvaddr.getRecvPhone());
		addressPo.setRecvProvince(apiRecvaddr.getRecvProvince());
		addressPo.setRegionId(apiRecvaddr.getRegionId());
		addressPo.setUin(apiRecvaddr.getUin());

		HashMap<String, Object> paramMapForPrizeObj = new HashMap<String, Object>();
		paramMapForPrizeObj.put("prizeId", prizeId);
		paramMapForPrizeObj.put("status", 1);
		// mysql 行锁，事务处理
		PpappPrize ppappPrize = ppappPrizeDao.selectEntryByMap(
				paramMapForPrizeObj, false);
		if (ppappPrize == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "奖品不存在");
		}
		
		String activeEndDay = Config.get("signActive."+ activeId +".endTime").substring(0, 8) ;
		String activeDay = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD) ;
		if (!activeDay.equals(activeEndDay)) {		
			HashMap<String, Object> paramMapPrizeCount = new HashMap<String, Object>();
			paramMapPrizeCount.put("activeId", activeId);
			paramMapPrizeCount.put("ext5", ppappPrize.getPrizeId());
			paramMapPrizeCount.put("insertDay", activeDay);
			paramMapPrizeCount.put("type", 4);
			int sendNum = ppappBehaviorInfoDao.countPrizeNum(paramMapPrizeCount);
			if (ppappPrize.getPrizeLimitNum() - sendNum < 1) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_PRIZE_USED_UP, "奖品已领完");
			}
			
			HashMap<String, Object> paramMapPrizeCount2 = new HashMap<String, Object>();
			paramMapPrizeCount2.put("activeId", activeId);
			paramMapPrizeCount2.put("ext4", ppappPrize.getPrizeLevel());
			paramMapPrizeCount2.put("insertDay", activeDay);
			paramMapPrizeCount2.put("type", 4);
			int sendNum2 = ppappBehaviorInfoDao.countPrizeNum(paramMapPrizeCount2);
			if (PrizeRuleManager.levelLimit(ppappPrize
					.getPrizeLevel()) - sendNum2 < 1) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_PRIZE_USED_UP, "奖品已领完");
			}
		}
		
		if (ppappPrize.getPrizeTotalNum() - ppappPrize.getPrizeUsedNum() < 1) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_PRIZE_USED_UP, "奖品已领完");
		}
		
		long deductionPoint = PrizeRuleManager.deductionPoint(ppappPrize
				.getPrizeLevel());
		if (deductionPoint <= 0) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_ACTION_UNKNOW_EXP, "扣点查询失败");
		}

		HashMap<String, Object> paramMapActivePoint = new HashMap<String, Object>();
		paramMapActivePoint.put("id", activeId + "_" + wid);
		// mysql 行锁，事务处理
		PpappActivePoint ppappActivePoint = ppappActivePointDao
				.selectEntryByMap(paramMapActivePoint, false);

		if (ppappActivePoint == null
				|| ppappActivePoint.getCurrentPoint() < deductionPoint) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_POINT_LACK, "点券不足");
		}

		// 更新点券表（扣点）
		PpappActivePoint ppappActivePointForUp = new PpappActivePoint();
		ppappActivePointForUp.setId(ppappActivePoint.getId());
		ppappActivePointForUp.setMk(mk);
		ppappActivePointForUp.setMk2(mk2);
		ppappActivePointForUp.setIp(request.getRemoteAddr());
		ppappActivePointForUp.setCurrentPoint(ppappActivePoint
				.getCurrentPoint() - deductionPoint);
		ppappActivePointForUp.setUpdateTime(System.currentTimeMillis());
		ppappActivePointForUp.setUsedPoint(ppappActivePoint.getUsedPoint()
				+ deductionPoint);
		ppappActivePointDao.update(ppappActivePointForUp);

		// 更新商品表（扣库存）
		PpappPrize ppappPrizeForUp = new PpappPrize();
		ppappPrizeForUp.setPrizeId(ppappPrize.getPrizeId());
		ppappPrizeForUp.setUpdateTime(System.currentTimeMillis());
		ppappPrizeForUp.setPrizeUsedNum(ppappPrize.getPrizeUsedNum() + 1);
		ppappPrizeDao.update(ppappPrizeForUp);

		// 奖品快照
		SimplePrizePo simplePrizePo = new SimplePrizePo();
		simplePrizePo.setPrizeId(ppappPrize.getPrizeId());
		simplePrizePo.setItemCode(ppappPrize.getItemCode()) ;
		simplePrizePo.setPrizeName(ppappPrize.getPrizeName());
		simplePrizePo.setPrizePic(ppappPrize.getPrizePic());
		simplePrizePo.setPrizePrice(ppappPrize.getPrizePrice());
		simplePrizePo.setPrizeLevel(ppappPrize.getPrizeLevel()) ;
		simplePrizePo.setRemark(remark);

		// 插入行为记录
		PpappBehaviorInfo behaviorInfo = new PpappBehaviorInfo();
		behaviorInfo.setId(UUID.randomUUID() + "_" + wid);
		behaviorInfo.setWid(wid);
		behaviorInfo.setActiveId(activeId);
		behaviorInfo.setCurrentNum(ppappActivePointForUp.getCurrentPoint());// 数据库需要修改，统一为INT类型
		behaviorInfo.setNum(deductionPoint * (-1));
		behaviorInfo.setInsertTime(System.currentTimeMillis());
		behaviorInfo.setInsertDay(TimeUtils.formatDateToDayString(
				TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD));
		behaviorInfo.setMk2(mk2);
		behaviorInfo.setMk(mk);
		behaviorInfo.setIp(request.getRemoteAddr());
		behaviorInfo.setType(4);
		JsonObject ext = new JsonObject();
		ext.add("addInfo", new Gson().toJsonTree(addressPo));
		ext.add("prizeInfo", new Gson().toJsonTree(simplePrizePo));
		behaviorInfo.setExt(ext.toString());
		behaviorInfo.setExt4(ppappPrize.getPrizeLevel().toString()) ;
		behaviorInfo.setExt5(ppappPrize.getPrizeId()) ;
		this.ppappBehaviorInfoDao.insert(behaviorInfo);
		
		signActive.info(new Gson().toJson(behaviorInfo)) ;
		jimClient.del(JIMConstant.getPrizeInfoKey(prizeId, activeDay, 0).getBytes()) ;
		jimClient.del(JIMConstant.getSignPointCachePoKey(wid, activeId).getBytes()) ;
		
		// 返回结果数据
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("num", deductionPoint);
		resultMap.put("totalNum", ppappActivePointForUp.getCurrentPoint());
		resultMap.put("activeId", activeId);
		return resultMap;
	}

	@Override
	public Map<String, Object> receiveVouchers(int activeId, long wid,
			String sk, String mk, String mk2, HttpServletRequest request)
			throws BusinessException, Exception {
		// TODO Auto-generated method stub
		//读配置文件活动减点数和代金券ID
		String vouchersId = Config.get("vouchers."+ activeId +".id");
		String deductionPoint = Config.get("vouchers."+ activeId +".deductionPoint");
		String sendLimit = Config.get("vouchers."+ activeId +".limit");
		String vouchersActiveId = Config.get("vouchers."+ activeId +".vouchersActiveId");
		String vouchersNum = Config.get("vouchers."+ activeId +".vouchersNum");
		if (StringUtils.isEmpty(vouchersId) || StringUtils.isEmpty(deductionPoint) 
				|| StringUtils.isEmpty(sendLimit) || StringUtils.isEmpty(vouchersNum)) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
		}
		long deductionPointL = Long.parseLong(deductionPoint) ;
		long sendLimitL = Long.parseLong(sendLimit) ;
		long vouchersNumL = Long.parseLong(vouchersNum) ;		
		HashMap<String, Object> paramMapPrizeCount = new HashMap<String, Object>();
		
		String activeEndDay = Config.get("signActive."+ activeId +".endTime").substring(0, 8) ;
		String activeDay = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD) ;
		if (!activeDay.equals(activeEndDay)) {
			paramMapPrizeCount.put("insertDay", activeDay);
		} 
		paramMapPrizeCount.put("activeId", activeId);
		paramMapPrizeCount.put("ext4", vouchersId);		
		paramMapPrizeCount.put("type", 3);
		int sendNum = ppappBehaviorInfoDao.countPrizeNum(paramMapPrizeCount);
		if (activeDay.equals(activeEndDay)) {
			if (vouchersNumL - sendNum < 1) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_PRIZE_USED_UP, "代金券已领完");
			}
		} else {
			if (sendLimitL - sendNum < 1) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_PRIZE_USED_UP, "代金券已领完");
			}
		}
		
		// mysql 行锁，事务处理
		HashMap<String, Object> paramMapActivePoint = new HashMap<String, Object>();
		paramMapActivePoint.put("id", activeId + "_" + wid);
		// mysql 行锁，事务处理
		PpappActivePoint ppappActivePoint = ppappActivePointDao
				.selectEntryByMap(paramMapActivePoint, false);

		if (ppappActivePoint == null
				|| ppappActivePoint.getCurrentPoint() < deductionPointL) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_POINT_LACK, "点券不足");
		}		
		// 更新点券表（扣点）
		PpappActivePoint ppappActivePointForUp = new PpappActivePoint();
		ppappActivePointForUp.setId(ppappActivePoint.getId());
		ppappActivePointForUp.setMk(mk);
		ppappActivePointForUp.setMk2(mk2);
		ppappActivePointForUp.setIp(request.getRemoteAddr());
		ppappActivePointForUp.setCurrentPoint(ppappActivePoint
				.getCurrentPoint() - deductionPointL);
		ppappActivePointForUp.setUpdateTime(System.currentTimeMillis());
		ppappActivePointForUp.setUsedPoint(ppappActivePoint.getUsedPoint()
				+ deductionPointL);
		ppappActivePointDao.update(ppappActivePointForUp);

		// 插入行为记录
		PpappBehaviorInfo behaviorInfo = new PpappBehaviorInfo();
		behaviorInfo.setId(UUID.randomUUID() + "_" + wid);
		behaviorInfo.setWid(wid);
		behaviorInfo.setActiveId(activeId);
		behaviorInfo.setCurrentNum(ppappActivePointForUp.getCurrentPoint());
		behaviorInfo.setNum(deductionPointL * (-1));
		behaviorInfo.setInsertTime(System.currentTimeMillis());
		behaviorInfo.setInsertDay(TimeUtils.formatDateToDayString(
				TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD));
		behaviorInfo.setMk2(mk2);
		behaviorInfo.setMk(mk);
		behaviorInfo.setIp(request.getRemoteAddr());
		behaviorInfo.setType(3);
		JsonObject ext = new JsonObject();
		ext.addProperty("vouchersId", vouchersId);
		behaviorInfo.setExt(ext.toString());
		behaviorInfo.setExt4(vouchersId) ;
		this.ppappBehaviorInfoDao.insert(behaviorInfo);
				
		// 调用发券CGI接口(先扣点后发券，保证不一致问题造成的扣点失败，发圈成功)
		String sendVouchersUrl = "http://act.paipai.com/docoupon/DispenseCoupon?t="
				+System.currentTimeMillis()+"&uin="+wid+"&couponid="+vouchersId
				+"&activeid="+vouchersActiveId+"&logintype=0&source=wuxian&sendtype=4" ;		
		HttpClient httpClient = new HttpClient();
		GetMethod getMethod = new GetMethod(sendVouchersUrl);		
		HttpConnectionManagerParams httpConnectionManagerParams = httpClient.getHttpConnectionManager().getParams();
		httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
		httpConnectionManagerParams.setConnectionTimeout(20000);
		httpConnectionManagerParams.setSoTimeout(20000);
		getMethod.setRequestHeader("User-Agent", request.getHeader("User-Agent")) ;
		getMethod.setRequestHeader("Referer", request.getHeader("Referer"));
		Long vk = Util.getVisitkey(request.getRemoteAddr(), request.getRemotePort()) ;
		getMethod.setRequestHeader("Cookie","wg_uin=" + wid + ";wg_skey=" + sk + ";visitkey=" + vk );
		int i = httpClient.executeMethod(getMethod);
		if (i != 200) {
			run.info("httpResult=" + i + ",requestUrl=" + sendVouchersUrl) ;
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_ACTION_UNKNOW_EXP, "发券失败");
		}
		String resultContent = getMethod.getResponseBodyAsString();
		run.info(resultContent) ;
		String result = resultContent.substring(resultContent.indexOf("SendRedpacket(") + 14,resultContent.indexOf("})") +1) ;							
		run.info(result) ;
		JsonParser jp = new JsonParser();
		JsonObject jsonResult = (JsonObject)jp.parse(result);
		if (!jsonResult.get("retCode").getAsString().equals("0")) {
			run.info("httpResult=" + i + ",requestUrl=" + sendVouchersUrl) ;
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_ACTION_UNKNOW_EXP, "发券失败");
		}
		signActive.info(new Gson().toJson(behaviorInfo) ) ;
		jimClient.del(JIMConstant.getSignPointCachePoKey(wid, activeId).getBytes()) ;
		// 返回结果数据
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("num", deductionPoint);
		resultMap.put("totalNum", ppappActivePointForUp.getCurrentPoint());
		resultMap.put("activeId", activeId);
		return resultMap;
	}	

	@Override
	public PpappPrizePo getPpappPrizePoByCache(String prizeId, int activeId,
			int type, String activeDay) throws BusinessException, Exception {
		// TODO Auto-generated method stub
		PpappPrizePo ppappPrizePo = new PpappPrizePo(); 
		String key = JIMConstant.getPrizeInfoKey(prizeId, activeDay, type) ;
		if (jimClient.exists(key.getBytes())) {
			return (PpappPrizePo) SerializeUtil.unserialize(jimClient.get(key.getBytes()));
		} else {
			HashMap<String, Object> paramMapForPrizeObj = new HashMap<String, Object>();
			paramMapForPrizeObj.put("prizeId", prizeId);
			paramMapForPrizeObj.put("status", 1);
			PpappPrize ppappPrize = ppappPrizeDao.selectEntryByMap(
					paramMapForPrizeObj, true);
			if (ppappPrize != null) {
				ppappPrizePo.setPrizeId(ppappPrize.getPrizeId());
				ppappPrizePo.setItemCode(ppappPrize.getItemCode());
				ppappPrizePo.setPrizeName(ppappPrize.getPrizeName());
				ppappPrizePo.setPrizeTotalNum(ppappPrize.getPrizeTotalNum());
				ppappPrizePo.setPrizePic(ppappPrize.getPrizePic());
				ppappPrizePo.setPrizePrice(ppappPrize.getPrizePrice());
				ppappPrizePo.setPrizeLevel(ppappPrize.getPrizeLevel()) ;
				if (0 == type) {
					String activeEndDay = Config.get("signActive."+ activeId +".endTime").substring(0, 8) ;					
					long levelNum = 0;
					// 最后一天，奖品余额是所有没有使用的奖品
					if (activeDay.equals(activeEndDay)) {
						levelNum = ppappPrize.getPrizeTotalNum()
								- ppappPrize.getPrizeUsedNum();
						ppappPrizePo.setPrizeLimitNum(levelNum <= 0 ? 0 : levelNum);
						ppappPrizePo.setPrizeLeaveNum(levelNum <= 0 ? 0 : levelNum);
					} else {						
						HashMap<String, Object> paramMapPrizeCount = new HashMap<String, Object>();
						paramMapPrizeCount.put("activeId", activeId);
						paramMapPrizeCount.put("ext5", prizeId);
						paramMapPrizeCount.put("insertDay", activeDay);
						paramMapPrizeCount.put("type", 4);
						int sendNum = ppappBehaviorInfoDao
								.countPrizeNum(paramMapPrizeCount);
						levelNum = ppappPrize.getPrizeLimitNum() - sendNum;
						ppappPrizePo
								.setPrizeLimitNum(ppappPrize.getPrizeLimitNum());
						ppappPrizePo.setPrizeLeaveNum(levelNum <= 0 ? 0 : levelNum);
					}
				} else {
					ppappPrizePo.setPrizeLimitNum(ppappPrize.getPrizeLimitNum());
					ppappPrizePo.setPrizeLeaveNum(ppappPrize.getPrizeLimitNum());
				}
				jimClient.del(key.getBytes()) ;
				jimClient.setEx(key.getBytes(), SerializeUtil.serialize(ppappPrizePo), JIMConstant.JIM_EFFECTIVE_TIME, TimeUnit.DAYS) ;
			
				return ppappPrizePo ;
			} else {
				return null ;
			}
		}
	}
	
	
	@Override
	public List<PpappPrizePo> prizeInfo(int activeId, String prizeIdList, int type, String activeDay) throws BusinessException, Exception {
		// TODO Auto-generated method stub
		ArrayList<PpappPrizePo> ppappPrizePoList = new ArrayList<PpappPrizePo>() ;
		String[] prizeIdStrList = prizeIdList.split(JIMConstant.KEY_SEPARATOR) ;
		for (String prizeId : prizeIdStrList) {				
			PpappPrizePo ppappPrizePo = this.getPpappPrizePoByCache(prizeId, activeId, type, activeDay) ;
			if (ppappPrizePo == null) {
				continue ;
			}				
			ppappPrizePoList.add(ppappPrizePo);
		}				
		return ppappPrizePoList;
	}

	@Override
	public void removePrizeListCache() throws Exception {
		// TODO Auto-generated method stub
		for (int i=1; i<6; i++) {
			String key = JIMConstant.getPrizeListKey(i, 1) ;
			jimClient.del(key) ;
		}
		
	}

}