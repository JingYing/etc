package com.paipai.halo.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.jim.cli.Cluster;
import com.jd.jmq.client.producer.MessageProducer;
import com.jd.jmq.common.exception.JMQException;
import com.jd.jmq.common.message.Message;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.JIMConstant;
import com.paipai.halo.common.PageEntity;
import com.paipai.halo.common.PagingResult;
import com.paipai.halo.common.SerializeUtil;
import com.paipai.halo.common.TimeUtils;
import com.paipai.halo.dao.ShopAroCommentDao;
import com.paipai.halo.dao.ShopAroItemDao;
import com.paipai.halo.dao.ShopAroLikeLogDao;
import com.paipai.halo.dao.ShopAroUserDao;
import com.paipai.halo.domain.Comment;
import com.paipai.halo.domain.ItemLikes;
import com.paipai.halo.domain.JmqMsgInfo;
import com.paipai.halo.domain.LikeLogInfo;
import com.paipai.halo.domain.UserBaseInfo;
import com.paipai.halo.service.ShopAroService;
import com.paipai.halo.web.JsonOutput;

/**
 * @ClassName: ShopAroServiceImpl
 * @Description:业务逻辑实现类
 * @author Comsys-lianghaining1
 * @date 2015年3月17日 下午5:20:43
 * 
 */
@Component(value = "shopAroService")
@Transactional
public class ShopAroServiceImpl implements ShopAroService {
	public static Logger run = LogManager.getLogger("run");
	
	private static final int LIMIT_TIME = 24;// 24小时内不能再点赞了
	// 用户基本信息更新Dao
	
	@Resource
	private ShopAroUserDao shopAroUserDao;
	// 用户个人点赞记录Dao
	@Resource
	private ShopAroLikeLogDao shopAroLikeLogDao;
	// 点赞总数
	@Resource
	private ShopAroItemDao shopAroItemDao;
	// 评论
	@Resource
	private ShopAroCommentDao shopAroCommentDao;
	// 缓存
	@Resource
	private Cluster jimClient;
	
	@Resource(name = "producer")
	private MessageProducer producer;
    @Value("#{configProperties['jmq.producer.topic.c']}")
	private String topic = "halo_comment";
	
	//用户的jimdb的key开头
	public static final String  JIM_USER_BEGIN	= "app_u_";
	//文章的总信息缓存开头
	public static final String  JIM_ITEM_BEGIN = "app_item_";
	//文章 列表点赞用户前10条列表
	public static final String  JIM_USER_LIKELIST_BEGIN = "app_item_likeuser_";
	//文章留言前20条缓存开头
	public static final String  JIM_ITEM_COMMLIST_BEGIN = "app_item_comm_";
	//记录某人最后一次留言时间用于防刷
	public static final String  JIM_USER_COMMENTTIME = "user_commenttime_";
	//防刷评论频率
	public static final Long    JIM_COMMENT_PL = 30L; //30秒
	//缓存有效时间
	public static final Long 	 JIM_EFFECTIVE_TIME = 1L;//缓存有效时间 ，单位天:
	//默认QQ头像开头
	public static final String QQ_HEAD="http://qlogo3.store.qq.com/qzone/";
	@Override
	public void saveorupdate(UserBaseInfo userBaseInfo) throws Exception {
		if (userBaseInfo != null) {
			// 先查看是否已经存在(直接从缓存中取，如果没有则再查数据库，)
			//UserBaseInfo us = getUserInfo(userBaseInfo.getWid());
						try {
							//保存时设置字符集
							this.shopAroUserDao.setNames();
						} catch (Exception e) {
							if(userBaseInfo.getNickname()!=null){
								userBaseInfo.setNickname(userBaseInfo.getNickname().replaceAll("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", ""));
							}
							e.printStackTrace();
						}
							
					/*if (us != null) {// 如果不为空则更新
						if (!us.getNickname().equals(userBaseInfo.getNickname())
								|| !us.getHead().equals(userBaseInfo.getHead())
								|| us.getUsertype() != userBaseInfo.getUsertype()) {
							int i = shopAroUserDao.update(userBaseInfo);
						}
					} else { // 否则插入
							int i = shopAroUserDao.insert(userBaseInfo);
					}*/
						//优化为只用一条SQL语句实现更新和插入。插入和更新交给mysql处理，依赖于 主键wid
					int i = this.shopAroUserDao.inOrupUser(userBaseInfo);
				  //检查是否缓存中已经存在?每次更新都更新缓存？
					if(jimClient.exists((JIM_USER_BEGIN+userBaseInfo.getWid()).getBytes())){
						//如果有则获取获取中的用户获取，主要是为了拿到缓存中的最一次评论的发表时间与评论内容
						jimClient.del((JIM_USER_BEGIN+userBaseInfo.getWid()).getBytes());
					}
					//更新缓存
					jimClient.setEx((JIM_USER_BEGIN+userBaseInfo.getWid()).getBytes(),SerializeUtil.serialize(userBaseInfo) , JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
			
		}

	}

	@Override
	public String setLike(String itemid, Integer itemtype, Long wid,
			Long ownerid) throws Exception {
				JsonOutput out = new JsonOutput();
				String msgstatus = "0";
				// 首先根据iD和类型来查出该人点赞记录
				LikeLogInfo likes = new LikeLogInfo();
				likes.setItemid(itemid);
				likes.setItemtype(itemtype);
				likes.setWid(wid);
				likes.setLiketime(TimeUtils.getCurrentTime());
				likes.setStatus(1);
				//为查询保存发文决信息表做准备
				ItemLikes itemLikes = new ItemLikes();
				itemLikes.setItemid(itemid);
				itemLikes.setItemtype(itemtype);
				itemLikes.setOwnerid(ownerid);
				//首先向个人记录表中插入或者更新数据 r=1时是插入 r=2:更新
				int r = this.shopAroLikeLogDao.saveOrupLike(likes);
				setLikeinfoCache(likes);
				// 点赞前先查一下是否总给中是否已经有数据（可能评论时已经有数据了）。如果没有则插入，如果有更新
				//该处的查询不能再优化，因为了，需要将数据放入缓存，该处不查询则存缓存时也要查询，少不了
				ItemLikes commonLikes = this.shopAroItemDao
						.getItemLikes(itemLikes);
				if (commonLikes == null) {// 如果为空则表示从来没有点过赞也没有评论过。则插入一条数据并记录
					itemLikes.setLikesnum(1);
					itemLikes.setCommentnum(0);
					this.shopAroItemDao.insert(itemLikes);
					//查出最新的点赞以及评论个数。放入缓存
					setItemLike(itemLikes,true);
					msgstatus = "10";
				} else {// 如果不为空则可能评论时已经插入了数据
					commonLikes.setLikesnum(commonLikes.getLikesnum() + 1);
					this.shopAroItemDao.updateItemLikes(commonLikes);
					//查出最新的点赞以及评论个数。放入缓存
					setItemLike(commonLikes,false);
				}
			//然后将该文章的用户头像id放至缓存
			setItemLikeUsers(itemLikes);
			// 将点赞后的总数写入返回值
			// 查询一下点赞总数
			ItemLikes ilike =getLikeNum(itemid, itemtype);
			JsonObject jb = new JsonObject();
			jb.addProperty("likesnum", ilike.getLikesnum());
			jb.addProperty("liketime", TimeUtils.getCurrentTime());
			jb.addProperty("islike",1);
			JsonElement je = new Gson().toJsonTree(jb);
			out.setData(je);
			out.setErrCode("0");
			if (msgstatus.equals("10")) {
				out.setMsg("您是第一个点赞的哦！");
			}  else {
				out.setMsg("点赞成功");
			 }

		return new Gson().toJson(out);
	}


	@Override
	public String cancelLike(String itemid, Integer itemtype, Long wid) throws Exception {
		JsonOutput out = new JsonOutput();
		String msgstatus = "0";
		// 首先根据iD和类型来查出该人点赞记录
		LikeLogInfo likes = new LikeLogInfo();
		likes.setItemid(itemid);
		likes.setItemtype(itemtype);
		likes.setWid(wid);
		likes.setLiketime(TimeUtils.getCurrentTime());
		//发布信息的总的信息表
		ItemLikes itemLikes = new ItemLikes();
		itemLikes.setItemid(itemid);
		itemLikes.setItemtype(itemtype);
				// 取消点赞前先查一下是否已经有数据。理论上肯定有
				ItemLikes commonLikes = this.shopAroItemDao
						.getItemLikes(itemLikes);
				// 再向个人记录中也插入(该处如果执行了。则虽然数据保持了一致但，逻辑已经出错)
				int r = this.shopAroLikeLogDao.upCancelLike(likes);
				setLikeinfoCache(likes);
				if(commonLikes!=null) {// 如果不为空则更新增加一
					commonLikes.setLikesnum(commonLikes.getLikesnum() - 1);
					this.shopAroItemDao.updateItemLikes(commonLikes);
					//查出最新的点赞以及评论个数。放入缓存
					setItemLike(commonLikes,false);
					// 同时更新个人记录
					msgstatus = "10";
					// 将点赞后的总数写入返回值
					JsonObject jb = new JsonObject();
					jb.addProperty("likesnum", commonLikes.getLikesnum());
					jb.addProperty("liketime", TimeUtils.getCurrentTime());
					jb.addProperty("islike", 0);
					JsonElement je = new Gson().toJsonTree(jb);
					out.setData(je);
				}
			//然后将该文章的用户头像id放至缓存
			setItemLikeUsers(itemLikes);
			//------------------------------------------------------------------------------------------------!!!!
			out.setErrCode("0");
			out.setMsg("取消点赞成功");
		return new Gson().toJson(out);
	}

	@Override
	public ItemLikes getLikeNum(String itemid, Integer itemtype) {
		//查看缓存中是否有该内容数据
		ItemLikes commonLikes = null;
		if(jimClient.exists((JIM_ITEM_BEGIN+itemid+itemtype).getBytes())){
			 commonLikes = (ItemLikes) SerializeUtil.unserialize(jimClient.get((JIM_ITEM_BEGIN+itemid+itemtype).getBytes()));
			 if(commonLikes==null){
				//如果没有。查数据库并写入缓存 
					ItemLikes itemLikes = new ItemLikes();
					itemLikes.setItemid(itemid);
					itemLikes.setItemtype(itemtype);
					// 取消点赞前先查一下是否已经有数据。理论上肯定有
					commonLikes = this.shopAroItemDao.getItemLikes(itemLikes);
					jimClient.setEx((JIM_ITEM_BEGIN+itemid+itemtype).getBytes(), SerializeUtil.serialize(commonLikes), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
			 }
		}else {
			//如果没有。查数据库并写入缓存 
			ItemLikes itemLikes = new ItemLikes();
			itemLikes.setItemid(itemid);
			itemLikes.setItemtype(itemtype);
			// 取消点赞前先查一下是否已经有数据。理论上肯定有
			commonLikes = this.shopAroItemDao.getItemLikes(itemLikes);
			jimClient.setEx((JIM_ITEM_BEGIN+itemid+itemtype).getBytes(), SerializeUtil.serialize(commonLikes), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}
		
		return commonLikes;
	}

	@Override
	public UserBaseInfo getUserInfo(Long wid) {
			//先从缓存中获取
		 UserBaseInfo ub = new UserBaseInfo();
		if(jimClient.exists((JIM_USER_BEGIN+wid).getBytes())){
			ub = (UserBaseInfo) SerializeUtil.unserialize(jimClient.get((JIM_USER_BEGIN+wid).getBytes()));
			if(ub==null){
				ub = this.shopAroUserDao.get(wid);
				//放入缓存
				jimClient.setEx((JIM_USER_BEGIN+wid).getBytes(), SerializeUtil.serialize(ub), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
			}
		}else {
			ub = this.shopAroUserDao.get(wid);
			//放入缓存
			jimClient.setEx((JIM_USER_BEGIN+wid).getBytes(), SerializeUtil.serialize(ub), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}
		return ub;
	}

	@Override
	public PagingResult<Comment> getPgaRanges(PageEntity pe) {
		return this.shopAroCommentDao.selectPagination(pe);
	}

	@Override
	public PagingResult<LikeLogInfo> getLikePgaRanges(PageEntity pe) {
		return this.shopAroLikeLogDao.selectPagination(pe);
	}

	@Override
	public String saveConmment(String itemid, int itemtype, Long wid,
			String ip, String title, String context, Long ownerid,int isshow) throws Exception {
			JsonOutput out = new JsonOutput();
			if(!isSubmit(itemid,context,wid)){
				out.setMsg("您操作太频繁了,换个话题试试！");
				out.setErrCode(ErrConstant.ERRCODE_SAVECOMM_FAIL_WID);
				return new Gson().toJson(out);
			}
			String state = "";
			try {
				//保存时设置字符集
				this.shopAroUserDao.setNames();
			} catch (Exception e) {
				if(context!=null){
					context = context.replaceAll("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", "");
				}
				//去掉表情
				e.printStackTrace();
			}
			
			// 构造出一个评论对象来
			Comment comment = new Comment();
			comment.setItemid(itemid);
			comment.setItemtype(itemtype);
			comment.setCommenttime(TimeUtils.getCurrentTime());
			comment.setTitle(title);
			comment.setContext(context);
			comment.setIP(ip);
			//0不用审批，1需要审批
			comment.setIsshow(isshow);
			comment.setWid(wid);
			// 构造评论点赞信息总表
			ItemLikes itemLikes = new ItemLikes();
			itemLikes.setItemid(itemid);
			itemLikes.setItemtype(itemtype);
			itemLikes.setOwnerid(ownerid);
			int r = this.shopAroCommentDao.insert(comment);
				// 同时更新评论点赞信息总表
				// 先查一下是否已经有了
				ItemLikes commonLikes = this.shopAroItemDao.getItemLikes(itemLikes);
				if (commonLikes != null) {// 更新之
					run.info("用户评论："+wid+","+itemid+"-saveConmment   update 》》");
					//如果发出的评论是审核过的。则更新
					if(isshow==1){
					    commonLikes.setCommentnum(commonLikes.getCommentnum() + 1);
						this.shopAroItemDao.updateItemcommnum(commonLikes);
						//更新缓存
						setItemLike(commonLikes,false);
					}
				} else {
					run.info("用户评论："+wid+","+itemid+"-saveConmment   add 》》");
					//如果发出的评论是审核过的。则更新
					if(isshow==1){
						itemLikes.setCommentnum(1);
					}else {
						itemLikes.setCommentnum(0);
					}
					itemLikes.setLikesnum(0);
					this.shopAroItemDao.insert(itemLikes);
					//更新缓存
					if(isshow==1){
						setItemLike(itemLikes,true);
					}
				}
				//根据id查出评论头像
				UserBaseInfo ub  = getUserInfo(wid);
				if(ub!=null){
					comment.setNickname(ub.getNickname());
					comment.setHead(ub.getHead());
				}
				//将最后一后一条评论时间放到缓存
				jimClient.setEx(JIM_USER_COMMENTTIME+wid, TimeUtils.getCurrentTime()+"", JIM_COMMENT_PL, TimeUnit.SECONDS);
				jimClient.setEx(JIMConstant.JIM_LASTCOMM_FIRST+wid, context, JIMConstant.JIM_LASTCOMM_PL_FT, TimeUnit.MINUTES);
				//如果wid少于39亿
				if(wid<3900000000L&&(comment.getHead()==null||"".equals(comment.getHead()))){
					comment.setHead(QQ_HEAD+wid+"/"+wid+"/50");
				}
				// 如果成功
				out.setErrCode(0);
				if(isshow==0){
					out.setMsg("发表成功，审核后生效");
				}else {
					//需要审核的不放入缓存
					//评论后将最新前20条放入缓存
					setCommentListCach(itemLikes);
					out.setMsg("成功");
				}
				out.setData(new Gson().toJsonTree(comment));
		return new Gson().toJson(out);
	}

	//判断是评论是否可以发表
	public boolean isSubmit(String itemid,String context,Long wid){
		//先判断是否内容重复了
		if(jimClient.exists(JIMConstant.JIM_LASTCOMM_FIRST+wid)){
			String ct  = jimClient.get(JIMConstant.JIM_LASTCOMM_FIRST+wid);
			if(ct!=null&&ct.equals(context==null?"":context)){
				return false;
			}
		}
		if(jimClient.exists(JIM_USER_COMMENTTIME+wid)){
			return false;
		}
		return true;
	}

	@Override
	public List<UserBaseInfo> getUserList(Long[] wid) {
		return this.shopAroUserDao.findUsrByMap(wid);
	}

	public static void main(String args[]) throws UnsupportedEncodingException {
		/*String paths[] = { "applicationContext-common.xml" };
		// 这个xml文件是Spring配置beans的文件，顺带一提，路径 在整个应用的根目录
		ApplicationContext ctx = new ClassPathXmlApplicationContext(paths);
		Cluster myBean = (Cluster) ctx.getBean("jimClient");*/
		// myBeanImpl是在Spring中注册的MyBean实现类的name或id
	}

	@Override
	public void lhntest(String context) throws Exception {
		//this.shopAroUserDao.setNames();
		UserBaseInfo userbaseInfo = new UserBaseInfo();
		userbaseInfo.setNickname(context);
		userbaseInfo.setUsertype(1);
		userbaseInfo.setWid(345433386L);
		//该方法set names utf8mb4;
	  	shopAroUserDao.setNames();
	  	jimClient.del((JIM_USER_BEGIN+345433386L).getBytes());
		shopAroUserDao.update(userbaseInfo);
		System.out.println(new Gson().toJson(shopAroUserDao.showvariables()) );
		
	}

	@Override
	public LikeLogInfo getLikeinfo(String itemid, int itemtype, Long wid) {
		LikeLogInfo likes = new LikeLogInfo();
		if(jimClient.exists((JIMConstant.JIM_USERLIKEINFO_FIRST+itemid+itemtype+wid).getBytes())){
			likes = (LikeLogInfo) SerializeUtil.unserialize(jimClient.get((JIMConstant.JIM_USERLIKEINFO_FIRST+itemid+itemtype+wid).getBytes()));
			if(likes==null){
				likes.setItemid(itemid);
				likes.setItemtype(itemtype);
				likes.setWid(wid);
				likes = this.shopAroLikeLogDao.getLikeLogInfo(likes);
				setLikeinfoCache(likes);
			}
			return likes;
		}else {
			// 首先根据iD和类型来查出该人点赞记录
			likes.setItemid(itemid);
			likes.setItemtype(itemtype);
			likes.setWid(wid);
			likes = this.shopAroLikeLogDao.getLikeLogInfo(likes);
			setLikeinfoCache(likes);
			return likes;
		}
		
	}
	
	//设置该对该文章的点赞记录数
	public void setLikeinfoCache(LikeLogInfo li){
		// 首先根据iD和类型来查出该人点赞记录
		if(li!=null){
			li = this.shopAroLikeLogDao.getLikeLogInfo(li);
			if(jimClient.exists((JIMConstant.JIM_USERLIKEINFO_FIRST+li.getItemid()+li.getItemtype()+li.getWid()).getBytes())){
				jimClient.del((JIMConstant.JIM_USERLIKEINFO_FIRST+li.getItemid()+li.getItemtype()+li.getWid()).getBytes());
				jimClient.setEx((JIMConstant.JIM_USERLIKEINFO_FIRST+li.getItemid()+li.getItemtype()+li.getWid()).getBytes(), SerializeUtil.serialize(li), JIMConstant.JIM_USERLIKEINFO_FT, TimeUnit.HOURS);
			}else {
				jimClient.setEx((JIMConstant.JIM_USERLIKEINFO_FIRST+li.getItemid()+li.getItemtype()+li.getWid()).getBytes(), SerializeUtil.serialize(li), JIMConstant.JIM_USERLIKEINFO_FT, TimeUnit.HOURS);
			}
		}
	}
	/**
	 * 
	  * @Title: 该该用 于向缓存中set最新的评论点赞信息
	  * @Description: TODO
	  * @param     设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public void setItemLike(ItemLikes itemLikes,boolean isupdate) {
		//查出最新的点赞以及评论个数。放入缓存
		ItemLikes itl  =null;
		if(isupdate){
			itl = this.shopAroItemDao.getItemLikes(itemLikes);
		}else{
			itl=itemLikes;
		}
		if(itl!=null){
				if(jimClient.exists((JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes())){
					run.info("del and setItemLike--"+JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()+"---------------》》"+(JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes());
					jimClient.del((JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes());
					jimClient.setEx((JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes(), SerializeUtil.serialize(itl), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
				}else {
					run.info("onlysetItemLike--"+JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()+"---------------》》"+(JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes());
					jimClient.setEx((JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes(), SerializeUtil.serialize(itl), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
				}
		}
	}
	/**
	 * 
	  * @Title:用于缓存文章的前10个用户id
	  * @Description: TODO
	  * @param @param itemLikes    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	private PagingResult<LikeLogInfo> setItemLikeUsers(ItemLikes itemLikes) {
		PageEntity pe = new PageEntity();
		pe.setPage(1);
		pe.setSize(10);
		pe.setOrderColumn("liketime");
		pe.setOrderTurn("desc");
		Map params = new HashMap<String, String>();
		params.put("itemid", itemLikes.getItemid());
		params.put("itemtype", itemLikes.getItemtype());
		params.put("id", 0);
		params.put("status", 1);
		pe.setParams(params);
		PagingResult<LikeLogInfo> pageRanges = null;
	    pageRanges = this.shopAroLikeLogDao.selectPagination(pe);
	    if(pageRanges!=null){
	    	if(jimClient.exists((JIM_USER_LIKELIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes())){
				run.info("del and setItemLikeUsers--"+JIM_USER_LIKELIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()+"---------------》》"+(JIM_ITEM_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes());
	    		jimClient.del((JIM_USER_LIKELIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes());
	    		jimClient.setEx((JIM_USER_LIKELIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes(), SerializeUtil.serialize(pageRanges), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
	    	}else {
				run.info("onlysetItemLikeUsers--"+JIM_USER_LIKELIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()+"---------------》》"+(JIM_ITEM_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes());
	    		jimClient.setEx((JIM_USER_LIKELIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes(), SerializeUtil.serialize(pageRanges), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
			}
	    	return pageRanges;
	    }else {
			return null;
		}
	}
	/**
	 * 
	  * @Title: 重新设置缓存
	  * @Description: TODO
	  * @param @param ub    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	private void setUserCache(UserBaseInfo ub){
		UserBaseInfo u = this.shopAroUserDao.get(ub.getWid());
		 //检查是否缓存中已经存在?每次更新都更新缓存？
		if(jimClient.exists((JIM_USER_BEGIN+u.getWid()).getBytes())){
			jimClient.del((JIM_USER_BEGIN+u.getWid()).getBytes());
			jimClient.setEx((JIM_USER_BEGIN+u.getWid()).getBytes(),SerializeUtil.serialize(u) , JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}else {
			jimClient.setEx((JIM_USER_BEGIN+u.getWid()).getBytes(),SerializeUtil.serialize(u) , JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}

	}
	/**
	 * 
	  * @Title:将文章的前20条放入缓存
	  * @Description: TODO
	  * @param @param itemLikes    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	private void setCommentListCach(ItemLikes itemLikes) {
		PageEntity pe = new PageEntity();
		pe.setPage(1);
		pe.setSize(20);
		pe.setOrderColumn("commenttime");
		pe.setOrderTurn("desc");
		Map params = new HashMap<String, String>();
		params.put("itemid", itemLikes.getItemid());
		params.put("id", 0);
		params.put("itemtype", itemLikes.getItemtype());
		pe.setParams(params);
		PagingResult<Comment> pageRanges  = null;
		pageRanges = this.shopAroCommentDao.selectPagination(pe);
		if(jimClient.exists((JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes())){
			run.info("将"+JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()+"删除并重新设置-  delsetCommentListCachadd 》》");
			jimClient.del((JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes());
			jimClient.setEx((JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes(), SerializeUtil.serialize(pageRanges), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}else {
			run.info("将"+JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()+"删除并重新设置-  setCommentListCachadd 》》");
			jimClient.setEx((JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes(), SerializeUtil.serialize(pageRanges), JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}
		
	}
	@Override
	public PagingResult<LikeLogInfo> getUserBaseByCache(String itemid, int itemtype) {
		if(jimClient.exists((JIM_USER_LIKELIST_BEGIN+itemid+itemtype).getBytes())){
			PagingResult<LikeLogInfo> ubBaseInfos =null;
			try {
				 ubBaseInfos  = (PagingResult<LikeLogInfo>) SerializeUtil.unserialize(jimClient.get((JIM_USER_LIKELIST_BEGIN+itemid+itemtype).getBytes()));

			} catch (Exception e) {
				e.printStackTrace();
			}
			if(ubBaseInfos==null){//该处说明缓存中的数据有错误重新设置缓存
				ItemLikes itemLikes  = new ItemLikes();
				itemLikes.setItemid(itemid);;
				itemLikes.setItemtype(itemtype);
				ubBaseInfos = setItemLikeUsers(itemLikes);
			}
			return ubBaseInfos;
		}else {
			ItemLikes itemLikes  = new ItemLikes();
			itemLikes.setItemid(itemid);;
			itemLikes.setItemtype(itemtype);
			PagingResult<LikeLogInfo> likeLogInfos = setItemLikeUsers(itemLikes);
			return likeLogInfos;
		}
		
	}

	@Override
	public PagingResult<Comment> getPageRangeByCache(String itemid, int itemtype) {
		//先去缓存中去取一下
		PagingResult<Comment>  commentpage =null;
		if(jimClient.exists((JIM_ITEM_COMMLIST_BEGIN+itemid+itemtype).getBytes())){
			   commentpage = (PagingResult<Comment>) SerializeUtil.unserialize(jimClient.get((JIM_ITEM_COMMLIST_BEGIN+itemid+itemtype).getBytes()));
			   if(commentpage==null){
				    ItemLikes its = new ItemLikes();
					its.setItemid(itemid);
					its.setItemtype(itemtype);
					setCommentListCach(its);
					commentpage = (PagingResult<Comment>) SerializeUtil.unserialize(jimClient.get((JIM_ITEM_COMMLIST_BEGIN+itemid+itemtype).getBytes()));
			   }
		}else {
				ItemLikes its = new ItemLikes();
				its.setItemid(itemid);
				its.setItemtype(itemtype);
				setCommentListCach(its);
				commentpage = (PagingResult<Comment>) SerializeUtil.unserialize(jimClient.get((JIM_ITEM_COMMLIST_BEGIN+itemid+itemtype).getBytes()));
		}
		 return commentpage;
	}
	@Override
	public ItemLikes getItemLike(String id, Integer itemtype) {
		ItemLikes it = getLikeNum(id,itemtype);
		return it;
	}

	@Override
	public List<Comment> getNoshow() {
		return this.shopAroCommentDao.select();
	}

	@Override
	public String addBloack(long wid) {
		String blackwids="";
		if(jimClient.exists("BLACKLIST")){
			 blackwids = jimClient.get("BLACKLIST");
			 if(blackwids!=null){
				 String [] wids = blackwids.split(",");
				 //是否加入
				 boolean ifadd=true;
				 for(String s:wids){
					 if(s!=null&&s.equals(String.valueOf(wid))){
						 ifadd = false;
					 }
				 }
				 if(ifadd){
					 blackwids=blackwids+","+wid; 
				 }
				 jimClient.setEx("BLACKLIST", blackwids,  30, TimeUnit.DAYS);
			 }else {
				 blackwids= wid+",";
				 jimClient.setEx("BLACKLIST", blackwids,  30, TimeUnit.DAYS);
			}
			
		}else {
			 blackwids= wid+",";
			 jimClient.setEx("BLACKLIST", blackwids,  30, TimeUnit.DAYS);
		}
		
		return blackwids;
	}

	@Override
	public String getBlackList() {
		if(jimClient.exists("BLACKLIST")){
			return jimClient.get("BLACKLIST");
		}else {
			return null;
		}
	}

	@Override
	public List<Comment> getCommentList(List<Long> ids) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("ids", ids);
		return this.shopAroCommentDao.selectByidParam(map);
	}

	@Override
	public void updateUserBl(List<Long> uids,List<Long> ids,int status) throws Exception {
		//先检验哪些uid里面没有。没有的则加入到用户表中。不然没能更新
		for(Long l:uids){
			UserBaseInfo u  = this.getUserInfo(l);
					if(u==null){
						u = new UserBaseInfo();
						u.setWid(l);
						u.setStatus(1);
						u.setUsertype(0);
						u.setHead("");
						u.setHeadhash(u.getHead().hashCode()+"");
						u.setNickname("");
						this.shopAroUserDao.insert(u);
						}
		}
		Map<String, Object> map =  new HashMap<String,Object>();
		map.put("wids", uids);
		map.put("status", status);
		 //更新用户表
		this.shopAroUserDao.updatestatusParam(map);
		//更新用户缓存
		updateUserCache(uids);
		//如果是加入黑名单时则删除，其它不删除'
		if(ids==null){
			ids = new ArrayList<Long>();
		}
		if(status==1){
			//查出该人所有评论
			Map<String, Object> umap =  new HashMap<String,Object>();
			umap.put("wids", uids);
			List<Comment> cl  = this.shopAroCommentDao.selectBywidParam(umap);
			if(cl!=null&&cl.size()>0){
				for(Comment c:cl){
					if(c.getIsshow()==1&&Collections.frequency(ids,c.getWid())==0){
						ids.add(c.getId());
					}
				}
			}
			//删除该人所有的评论，
			this.shopAroCommentDao.updateIsshow(ids, 0);
			//更新数量
			removeCache(ids);
			//发送mq捕获异常，避免事务
			try {
				sendCommentsMq(ids,"0");
			} catch (Exception e) {
				run.info("updateUserBl  sendcommentsmq errot");
				e.printStackTrace();
			}
			
		}
		
	}
	
	private void updateUserCache(List<Long> uids) {
		for(Long l: uids){
			UserBaseInfo u = this.shopAroUserDao.get(l);
			if(u!=null){
			  //检查是否缓存中已经存在?每次更新都更新缓存？
				if(jimClient.exists((JIM_USER_BEGIN+u.getWid()).getBytes())){
					jimClient.del((JIM_USER_BEGIN+u.getWid()).getBytes());
				}
				//更新缓存
				jimClient.setEx((JIM_USER_BEGIN+u.getWid()).getBytes(),SerializeUtil.serialize(u) , JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
				}
			}
	}

	private void removeCache(List<Long> commentIds)	{
		for(long l : commentIds)	{
			Comment c = shopAroCommentDao.get(l);
			if(c != null)	{
				 ItemLikes itemLikes = new ItemLikes();
				 itemLikes.setItemid(c.getItemid());
				 itemLikes.setItemtype(c.getItemtype());
				 //重新构造前20条缓存
				 setCommentListCach(itemLikes);
				 try {
					 ItemLikes it = shopAroItemDao.getItemLikes(itemLikes);
				     it.setCommentnum((it.getCommentnum()-1)<0?0:(it.getCommentnum()-1));
					 this.shopAroItemDao.updateItemcommnum(it);
					//重缓存数量
				   setItemLike(it,false);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		
		}
	}

	@Override
	public List<UserBaseInfo> getBlUsers(int status) {
		return this.shopAroUserDao.getBlUsers(status);
	}

	@Override
	public List<Comment> getCommentListByUid(List<Long> uids) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("wids", uids);
		return this.shopAroCommentDao.selectBywidParam(map);
	}
	/**
	 * 
	  * @Title: 发送MQ信息
	  * @Description: 发送文章的评论以及点赞数
	  * @param @param itemid 文章ID
	  * @param @param itemtype    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public void sendCommentsMq(List<Long> ids,String islike){
		//查出点赞的信息发一个Mq type:1:占先2：评论
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("ids", ids);
		List<Comment> comments = this.shopAroCommentDao.selectByidParam(map) ;
		for(Comment c:comments){
			JsonOutput out = new JsonOutput();
			ItemLikes i = getLikeNum(c.getItemid(), c.getItemtype());
			if(i!=null){
				JmqMsgInfo jmqMsgInfo = new JmqMsgInfo();
				jmqMsgInfo.setBizData(i);
				jmqMsgInfo.setBizType("2");
				jmqMsgInfo.setContext(c.getContext()==null?"":c.getContext());
				jmqMsgInfo.setIslike(islike);
				out.setData(new Gson().toJsonTree(jmqMsgInfo));
				Message message = new Message(topic, new Gson().toJson(out), topic+c.getItemid());
		        try {
					producer.send(message);
				} catch (JMQException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
