package com.paipai.component.configcenter.api.set.so;


public class Api4SetSoAdapter {
	public static Object lock = new Object();

	private static boolean ready = false;

	public static native boolean init();

	public static native String getServiceAddressBySetAndKey(
			String paramString, int paramInt, long paramLong);

	public static native String getServiceAddressBySet(String paramString,
			int paramInt, long paramLong);

	public static native int getServiceAddressCountBySet(String paramString,
			int paramInt);

	public static native String getCmdAddressBySetAndKey(long paramLong1,
			int paramInt, long paramLong2);

	public static native String getCmdAddressBySet(long paramLong1,
			int paramInt, long paramLong2);

	public static native int getCmdAddressCountBySet(long paramLong,
			int paramInt);

	public static native int Report(String paramString1, int paramInt1,
			String paramString2, int paramInt2, int paramInt3);

	public static native int Report(long paramLong, int paramInt1,
			String paramString, int paramInt2, int paramInt3);

	public static native int GetSetIdBySvcAndIp(String paramString1,
			String paramString2);

	public static native int GetSetIdByCmdAndIp(long paramLong,
			String paramString);

	public static boolean isReady() {
		return ready;
	}

	static {
		if (System.getProperty("os.name").toLowerCase().indexOf("window") < 0)	{
			String sopath = "so/confg4set_api_x86_64-suse-linux.so";
			System.load(Thread.currentThread().getContextClassLoader().getResource(sopath).getPath());
			ready = init();
		}
	}
}
