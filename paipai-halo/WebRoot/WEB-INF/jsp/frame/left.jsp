<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="static/ztree/zTreeStyle.css"/>
    <script src="static/jquery/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="static/ztree/jquery.ztree.all-3.5.min.js"></script>
 	<script language="javascript">
 		var nodes = ${nodes};
		var setting = {
			callback: {
				onClick: function(event, treeId, treeNode)	{
					window.parent.frames[2].location = "${pageContext.request.contextPath}/" + treeNode.rightUrl;
				}
			}
	    };
 		
		$(function()	{
		/*
			$("a").click(function()	{
				window.parent.frames[2].document.write("<br><br><center><img src='${pageContext.request.contextPath}/img/loading3.gif'/></center>");
				$("a").removeClass("amrow-sel");
				$(this).toggleClass("amrow-sel");
				$(this).blur();
			});
			$('#default').click();
			window.parent.frames[2].location = "${pageContext.request.contextPath}/" + $('#default').attr('href');
			*/
			$.fn.zTree.init($("#tree"), setting, nodes);
		});
		
	</script>
  </head>
  
  <body class="left">
  	<ul id="tree" class="ztree"></ul>
  </body>
</html>
