/*
Navicat MySQL Data Transfer

Source Server         : halo测试
Source Server Version : 50171
Source Host           : 192.168.146.102:3306
Source Database       : paipai_sign

Target Server Type    : MYSQL
Target Server Version : 50171
File Encoding         : 65001

Date: 2015-04-21 13:00:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ppapp_active_point`
-- ----------------------------
DROP TABLE IF EXISTS `PPAPP_Active_Point`;
CREATE TABLE `PPAPP_Active_Point` (
  `id` varchar(100) NOT NULL COMMENT '主键，activeId+wid拼成',
  `wid` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `activeId` int(11) DEFAULT NULL COMMENT '活动ID',
  `currentPoint` bigint(20) DEFAULT NULL COMMENT '当前点券数',
  `totalPoint` bigint(20) DEFAULT NULL COMMENT '总共获得的点券数',
  `usedPoint` bigint(20) DEFAULT NULL COMMENT '已使用的点券数',
  `mk2` varchar(100) DEFAULT NULL COMMENT '京东MK',
  `mk` varchar(100) DEFAULT NULL COMMENT '深圳MK',
  `ip` varchar(20) DEFAULT NULL COMMENT '用户IP',
  `insertTime` bigint(20) DEFAULT NULL COMMENT '插入时间',
  `updateTime` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `ext1` varchar(255) DEFAULT NULL COMMENT '扩展1',
  `ext2` varchar(255) DEFAULT NULL COMMENT '扩展2',
  `ext3` varchar(255) DEFAULT NULL COMMENT '扩展3',
  PRIMARY KEY (`id`),
  KEY `index_wid_point` (`wid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录用户的点券信息';

-- ----------------------------
-- Records of ppapp_active_point
-- ----------------------------

-- ----------------------------
-- Table structure for `ppapp_behavior_info`
-- ----------------------------
DROP TABLE IF EXISTS `PPAPP_Behavior_Info`;
CREATE TABLE `PPAPP_Behavior_Info` (
  `id` varchar(100) NOT NULL COMMENT 'uuid+wid,主键',
  `wid` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `activeId` int(11) DEFAULT NULL COMMENT '活动ID',
  `currentNum` bigint(20) DEFAULT NULL COMMENT '当前点券总数',
  `num` bigint(20) DEFAULT NULL COMMENT '该事件增、减点券数',
  `mk2` varchar(100) DEFAULT NULL COMMENT '京东MK',
  `mk` varchar(100) DEFAULT NULL COMMENT '深圳MK',
  `ip` varchar(20) DEFAULT NULL COMMENT '用户IP',
  `insertDay` varchar(10) DEFAULT NULL COMMENT '事件时间（yyyyMMdd）',
  `insertTime` bigint(20) DEFAULT NULL COMMENT '事件时间戳',
  `type` tinyint(4) DEFAULT NULL COMMENT '1：签到   2：分享  3：领券  4：领奖',
  `ext` text COMMENT '额为信息,JSON串。1：签到,记录fid,ftype及连续签到天数signDays; 2，分享：记录fid,ftype;  3：领券，记录券ID；4：领奖,记录addInfo,详细地址信息，prizeInfo:奖品信息',
  `ext1` varchar(255) DEFAULT NULL COMMENT '扩展1',
  `ext2` varchar(255) DEFAULT NULL COMMENT '扩展2',
  `ext3` varchar(255) DEFAULT NULL COMMENT '扩展3',
  `ext4` VARCHAR(100) DEFAULT NULL COMMENT '领奖时，记录奖品等级;发券时，记录券ID',
  `ext5` VARCHAR(100) DEFAULT NULL COMMENT '领奖时，记录奖品Id',
  PRIMARY KEY (`id`),
  KEY `index_wid_behavior` (`wid`),
  KEY `index_insertTime_behavior` (`insertTime`),
  KEY `index_insertDay_behavior` (`insertDay`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户行为记录表';

-- ----------------------------
-- Records of ppapp_behavior_info
-- ----------------------------

-- ----------------------------
-- Table structure for `ppapp_behavior_info_history`
-- ----------------------------
DROP TABLE IF EXISTS `PPAPP_Behavior_Info_History`;
CREATE TABLE `PPAPP_Behavior_Info_History` (
  `id` varchar(100) NOT NULL COMMENT 'uuid+wid 主键',
  `wid` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `activeId` int(11) DEFAULT NULL COMMENT '活动ID',
  `currentNum` bigint(20) DEFAULT NULL COMMENT '当前点券总数',
  `num` bigint(20) DEFAULT NULL COMMENT '该事件增、减点券数',
  `mk2` varchar(100) DEFAULT NULL COMMENT '京东MK',
  `mk` varchar(100) DEFAULT NULL COMMENT '深圳MK',
  `ip` varchar(20) DEFAULT NULL COMMENT '用户IP',
  `insertDay` varchar(10) DEFAULT NULL COMMENT '事件时间（yyyyMMdd）',
  `insertTime` bigint(20) DEFAULT NULL COMMENT '事件时间戳',
  `type` tinyint(4) DEFAULT NULL COMMENT '1：签到   2：分享  3：领券  4：领奖',
  `ext` text COMMENT '额为信息,JSON串。1：签到,记录fid,ftype及连续签到天数signDays; 2，分享：记录fid,ftype;  3：领券，记录券ID；4：领奖,记录addInfo,详细地址信息，prizeInfo:奖品信息',
  `ext1` varchar(255) DEFAULT NULL COMMENT '扩展1',
  `ext2` varchar(255) DEFAULT NULL COMMENT '扩展2',
  `ext3` varchar(255) DEFAULT NULL COMMENT '扩展3',
  `ext4` VARCHAR(100) DEFAULT NULL COMMENT '领奖时，记录奖品等级;发券时，记录券ID',
  `ext5` VARCHAR(100) DEFAULT NULL COMMENT '领奖时，记录奖品ID',
  PRIMARY KEY (`id`),
  KEY `index_wid_behavior` (`wid`),
  KEY `index_insertTime_behavior` (`insertTime`),
  KEY `index_insertDay_behavior` (`insertDay`) USING BTREE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户行为记录历史表';

-- ----------------------------
-- Records of ppapp_behavior_info_history
-- ----------------------------

-- ----------------------------
-- Table structure for `ppapp_prize`
-- ----------------------------
DROP TABLE IF EXISTS `PPAPP_Prize`;
CREATE TABLE `PPAPP_Prize` (
  `prizeId` varchar(100) NOT NULL COMMENT '主键，由itemcode + $ + activeId组成',
  `itemCode` varchar(100) DEFAULT NULL COMMENT '对应商品的ItemCode，调商祥使用',
  `prizeName` varchar(100) DEFAULT NULL COMMENT '奖品名称',
  `prizePic` varchar(500) DEFAULT NULL COMMENT '奖品图片',
  `prizePrice` decimal(10,2) DEFAULT NULL COMMENT '奖品价格',
  `prizeLevel` int(11) DEFAULT NULL,
  `prizeLevelDesc` varchar(100) DEFAULT NULL,
  `prizeTotalNum` bigint(20) DEFAULT NULL COMMENT '奖品总数',
  `prizeLimitNum` bigint(20) DEFAULT NULL,
  `prizeUsedNum` bigint(20) DEFAULT NULL COMMENT '已使用数',
  `activeId` int(11) DEFAULT NULL COMMENT '活动ID',
  `status` tinyint(4) DEFAULT NULL COMMENT '奖品状态 1：可用  0：不可用',
  `insertTime` bigint(20) DEFAULT NULL COMMENT '数据插入时间',
  `updateTime` bigint(20) DEFAULT NULL COMMENT '数据更新时间',
  `ext1` varchar(500) DEFAULT NULL COMMENT '扩展1',
  `ext2` varchar(500) DEFAULT NULL COMMENT '扩展2',
  `ext3` varchar(500) DEFAULT NULL COMMENT '扩展3',
  PRIMARY KEY (`prizeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动奖品记录表';

-- ----------------------------
-- Records of ppapp_prize
-- ----------------------------

-- ----------------------------
-- Table structure for `ppapp_share_log`
-- ----------------------------
DROP TABLE IF EXISTS `PPAPP_Share_Log`;
CREATE TABLE `PPAPP_Share_Log` (
  `id` varchar(100) NOT NULL COMMENT '主键，UUID+wid拼成',
  `wid` bigint(20) DEFAULT NULL COMMENT '领奖人wid',
  `activeId` int(11) DEFAULT NULL COMMENT '活动ID',
  `type` int(11) DEFAULT NULL COMMENT '分享类型(微信、微博等)',
  `mk2` varchar(100) DEFAULT NULL COMMENT '京东MK',
  `mk` varchar(100) DEFAULT NULL COMMENT '深圳MK',
  `ip` varchar(20) DEFAULT NULL COMMENT '用户IP',
  `insertTime` bigint(20) DEFAULT NULL COMMENT '领奖时间',
  `insertDay` varchar(10) DEFAULT NULL COMMENT '领奖天（YYYYmmDD）',
  `ext1` varchar(255) DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(255) DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(255) DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`id`),
  KEY `index_wid_share` (`wid`),
  KEY `index_insertTime_share` (`insertTime`),
  KEY `index_insertDay_share` (`insertDay`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分享记录表';


-- ----------------------------
-- Records of ppapp_share_log
-- ----------------------------

-- ----------------------------
-- Table structure for `ppapp_sign_log`
-- ----------------------------
DROP TABLE IF EXISTS `PPAPP_Sign_Log`;
CREATE TABLE `PPAPP_Sign_Log` (
  `id` varchar(100) NOT NULL COMMENT '主键，UUID+wid拼成',
  `wid` bigint(20) DEFAULT NULL COMMENT '领奖人wid',
  `activeId` int(11) DEFAULT NULL COMMENT '活动ID',
  `insertTime` bigint(20) DEFAULT NULL COMMENT '领奖时间',
  `insertDay` varchar(10) DEFAULT NULL COMMENT '领奖天（YYYYmmDD）',
  `mk2` varchar(100) DEFAULT NULL COMMENT '京东mk',
  `mk` varchar(100) DEFAULT NULL COMMENT '深圳MK',
  `ip` varchar(20) DEFAULT NULL COMMENT '用户IP',
  `ext1` varchar(255) DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(255) DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(255) DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`id`),
  KEY `index_wid_sign` (`wid`),
  KEY `index_insertTime_sign` (`insertTime`),
  KEY `index_insertDay_sign` (`insertDay`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签到记录表';

-- ----------------------------
-- Records of ppapp_sign_log
-- ----------------------------
