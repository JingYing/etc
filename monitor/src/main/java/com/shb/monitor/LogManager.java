package com.shb.monitor;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Hierarchy;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.LoggerRepository;
import org.apache.log4j.spi.RootLogger;

public class LogManager {
	public static Logger 
					REAL_TIME, HOUR, DAY, ERR;
	private static String 
					timestamp = new SimpleDateFormat("MMddHHmm-SSS-").format(new Date()),	//使用时间戳生成日志文件名;
					perflog, 
					middlelog;

	static {
		String[] dirs = getLogDirs();
		perflog = dirs[0];
		middlelog = dirs[1];
		
		//实时日志. 达到一定体积后滚动,滚动时记入到hour.log.
		Properties p = new Properties();
		p.setProperty("log4j.logger.L1", "INFO,L1");
		p.setProperty("log4j.appender.L1", RealTimeRollingAppender.class.getName());
		p.setProperty("log4j.appender.L1.File", middlelog + File.separator + timestamp + "realtime.log");
		p.setProperty("log4j.appender.L1.MaxFileSize", "10MB");
		p.setProperty("log4j.appender.L1.MaxBackupIndex", "1");
		p.setProperty("log4j.appender.L1.layout", PatternLayout.class.getName());
		p.setProperty("log4j.appender.L1.layout.ConversionPattern", "%m%n");
		p.setProperty("log4j.appender.L1.encoding", "UTF-8");

		p.setProperty("log4j.logger.L2", "INFO,L2");
		p.setProperty("log4j.appender.L2", HourStatRollingAppender.class.getName());
		p.setProperty("log4j.appender.L2.File", middlelog + File.separator + timestamp + "hour.log");
		p.setProperty("log4j.appender.L2.MaxBackupIndex", "2");
		p.setProperty("log4j.appender.L2.layout", PatternLayout.class.getName());
		p.setProperty("log4j.appender.L2.layout.ConversionPattern", "%m%n");
		p.setProperty("log4j.appender.L2.encoding", "UTF-8");

		//最终统计日志.不使用DailyRollingFile,只进行编程式滚动. 不初始化日志,使用前调用setDayLogName()手动设置文件. 
		p.setProperty("log4j.logger.L3", "INFO,L3");
		p.setProperty("log4j.appender.L3", FileAppender.class.getName());
//		p.setProperty("log4j.appender.L3.File", filePrefix + "stat3.log");
		p.setProperty("log4j.appender.L3.layout", PatternLayout.class.getName());
		p.setProperty("log4j.appender.L3.layout.ConversionPattern", "%m%n");
		p.setProperty("log4j.appender.L3.encoding", "UTF-8");

		p.setProperty("log4j.logger.L4", "INFO,L4");
		p.setProperty("log4j.appender.L4", RollingFileAppender.class.getName());
		p.setProperty("log4j.appender.L4.File", perflog + File.separator + "err.log");
		p.setProperty("log4j.appender.L4.MaxFileSize", "100MB");
		p.setProperty("log4j.appender.L4.MaxBackupIndex", "1");
		p.setProperty("log4j.appender.L4.layout", PatternLayout.class.getName());
		p.setProperty("log4j.appender.L4.layout.ConversionPattern", "%d{ABSOLUTE} %5p %c{1}:%L - %m%n");
		p.setProperty("log4j.appender.L4.encoding", "UTF-8");

		LoggerRepository logRepo = new Hierarchy(new RootLogger(Level.INFO));
		new PropertyConfigurator().doConfigure(p, logRepo);
		REAL_TIME = logRepo.getLogger("L1");
		HOUR = logRepo.getLogger("L2");
		DAY = logRepo.getLogger("L3");
		ERR = logRepo.getLogger("L4");

		startScheduleFlushThread();
	}
	
	/**
	 * 手动设置dayLog的输出文件名.
	 * @param suffix log的后缀名
	 * @return
	 */
	public static void setDayLog(String suffix)	{
		FileAppender a = (FileAppender)DAY.getAppender("L3");
		try {
			a.setFile(perflog + File.separator + timestamp + "day.log." + suffix, false, false, 0);
		} catch (IOException e) {
			ERR.error(e.getMessage(), e);
		}
	}

	/**
	 * 默认日志目录为tomcat/logs, 如果不是tomcat下运行,则使用自定义目录
	 * @return 0:主目录 1:中间日志目录
	 */
	private static String[] getLogDirs() {
		String dir = null;
		String tomcat = System.getProperty("catalina.base");
		if (tomcat != null) {
			if (!tomcat.endsWith(File.separator)) {
				tomcat += File.separator;
			}
			dir = tomcat + "logs" + File.separator + "perflog";
		} else	{
			dir = "/data/apps/perflog";
		}
		
		File middle = new File(dir + File.separator + "middlelog");
		if(!middle.exists()){
			middle.mkdirs();
		}
		return new String[]{dir, middle.getAbsolutePath()};
	}

	/**
	 * 每天0点稍后, 随便输出一条日志, 以便log4j进行日志滚动.
	 */
	private static void startScheduleFlushThread() {
		Calendar time = Calendar.getInstance();
		time.add(Calendar.DATE, 1);
		time.set(Calendar.HOUR_OF_DAY, 0);
		time.set(Calendar.MINUTE, 0);
		time.set(Calendar.SECOND, 30);

		new Timer("monitor.jar:flushHourLogAtMidnight", true).scheduleAtFixedRate(new TimerTask() {
			public void run() {
				LogManager.HOUR.info("====");
			}
		}, time.getTime(), 86400000L);
	}
}
