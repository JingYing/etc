package com.shb.monitor;

import java.math.BigInteger;

public class PerfKpi {
	private long count;	//调用次数
	private long maxCost;	//最大耗时毫秒
	private long minCost;	//最小耗时毫秒
	private long averageCost;	//平均耗时毫秒
	private BigInteger totalCostMillis = new BigInteger("0");	//总耗时

	public void addCount(long count) {
		this.count += count;
	}

	public void addTotalCostMillis(BigInteger value) {
		this.totalCostMillis = this.totalCostMillis.add(value);
	}

	public long getCount() {
		return this.count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public BigInteger getTotalCostMillis() {
		return this.totalCostMillis;
	}

	public void setTotalCostMillis(BigInteger totalCostMillis) {
		this.totalCostMillis = totalCostMillis;
	}

	public long getMaxCost() {
		return this.maxCost;
	}

	public void setMaxCost(long maxCost) {
		this.maxCost = maxCost;
	}

	public long getMinCost() {
		return this.minCost;
	}

	public void setMinCost(long minCost) {
		this.minCost = minCost;
	}

	public long getAverageCost() {
		return this.averageCost;
	}

	public void setAverageCost(long averageCost) {
		this.averageCost = averageCost;
	}
}
