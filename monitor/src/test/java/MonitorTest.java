

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.junit.Test;

import com.shb.monitor.LogManager;
import com.shb.monitor.MonitorLog;

public class MonitorTest  {
	@Test
	public void test1() {
		Logger log = Logger.getLogger(MonitorLog.class);
		for (int i = 0; i < 10000; i++) {
			log.info("3333333333333333333333333333333333333");
		}

		for (int i = 0; i < 500000; i++)
			MonitorLog.log("opc", "com.sdfd.dfd.fsdifsdfd",
					System.currentTimeMillis(), 50L);
	}

	@Test
	public void test2() {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < 100; i++) {
			list.add(randChar(30));
		}

		Random rand = new Random();
		int size = list.size();
		while (true) {
			try {
				Thread.sleep(rand.nextInt(2));
			} catch (InterruptedException localInterruptedException) {
			}
			int cost = rand.nextInt(3000)+1;
			MonitorLog.log("opc", (String) list.get(rand.nextInt(size)),
					System.currentTimeMillis(), cost);
		}
	}
	
	/**
	 * 生成几个随机小写字母
	 * @param count
	 * @return
	 */
	private String randChar(int count)	{
		byte[] arr = new byte[count];
		Random rand = new Random();
		for(int i=0; i<arr.length; i++)	{
			arr[i] = (byte)(rand.nextInt(26)+97);
		}
		return new String(arr);
	}

	@Test
	public void test3() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS");
		Random rand = new Random();
		while (true) {
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException localInterruptedException) {
			}
			if (rand.nextInt(10) == 1) {
				try {
					FileWriter w = new FileWriter("d:/logs/stat1.log");
					w.write("++++++++++++++++++++++++++++\r\n");
					w.flush();
					w.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			LogManager.REAL_TIME.info(sdf.format(new Date()));
		}
	}
	
	@Test
	public void testFileA()	{
		LogManager.DAY.error("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		LogManager.DAY.error("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		LogManager.DAY.error("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		LogManager.DAY.error("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		LogManager.DAY.error("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		FileAppender a = (FileAppender)LogManager.DAY.getAppender("L3");
		System.out.println(a.getFile());
		try {
			a.setFile("d:/data/apps/perflog/xxx-1225.log", false, false, 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(a.getFile());
		LogManager.DAY.error("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		LogManager.DAY.error("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		LogManager.DAY.error("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		LogManager.DAY.error("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		LogManager.DAY.error("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		
	}
}
