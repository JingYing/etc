<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath }/css/css.css"
			rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath }/css/soft.css"
			rel="stylesheet" type="text/css" />
		<%@ include file="/jsp/common2.jsp"%>
		<link rel="stylesheet" type="text/css"
			href="${pageContext.request.contextPath}/css/start/jquery-ui-1.8.16.custom.css" />
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript">
	function searchAnswer() {
		var keyword = $('#question').val();
		if ($.trim(keyword) == '')
			return;
		else
			window.location.href = '${pageContext.request.contextPath}/faq/search.action?keyword='
					+ keyword;
	}
</script>

	</head>
	<body>
		<table width="100%" border="0" align="center" cellpadding="0"
			cellspacing="0"
			background="${pageContext.request.contextPath}/img/xk.png">
			<tr>
				<td width="2%" height="37"></td>
				<td width="5%">
					<a href="javascript:history.go(-1)"> <img alt="返回"
							src="${pageContext.request.contextPath}/img/an3.png" width="30"
							height="24" border="0" />
					</a>
				</td>
				<td width="55%"></td>
				<td width="28%" align="right"></td>
				<td width="2%"></td>
			</tr>
		</table>
		
		
		<center>
		<div style="margin-top: 20px;">
			<table width="80%" border="1" align="center" cellpadding="0"
				cellspacing="0" bordercolor="#c8c8c8">

				<tr>
					<td>
						<div id="tiwen">
							<div id="shuru">
								<input type="text" id="question" class="text" value="${keyword}" />
							</div>
							<div id="wen">
								<input type="button" style="margin-top: 12px;" value="搜索答案"
									class="faq_btn1" onClick="searchAnswer()" />
							</div>
						</div>

						<pg:pager maxPageItems="20" maxIndexPages="15" isOffset="false"
							export="offset,currentPageNumber=pageNumber" scope="request"
							url="${pageContext.request.contextPath}/faq/search.action">
							<pg:param name="keyword" value="${param.keyword}" />
							<div id="wenti">
								<table border="0" cellSpacing="0" cellPadding="0" width="100%"
									align="center" bgcolor="#ffffff">
									<tr>
										<td height="32" width="1%"
											background="${pageContext.request.contextPath}/img/tj2.png"></td>
										<td class="STYLE5 STYLE9" width="98%"
											background="${pageContext.request.contextPath}/img/tj2.png">
											搜索到的问题...
										</td>
										<td width="1%"
											background="${pageContext.request.contextPath}/img/tj2.png"></td>
									</tr>
								</table>
								<ul>
									<c:if test="${empty faqsList}">
										<center>
											<font color="red">对不起, 没有找到相关的问题!</font>
										</center>
									</c:if>
									<c:forEach items="${faqsList}" var="faq">
										<pg:item>
											<li>
												<div class="ss_nei">
													<div class="ss_nei_wen">
														<div class="ss_nei_wen_left">
															${faq.question}
														</div>
														<div class="ss_nei_wen_right">
															<input type="button" value="发送问题及答案到手机" class="faq_btn2"
																onclick="javascript:window.location.href='${pageContext.request.contextPath}/blank/sendFaqSms.action?id=${faq.id}'" />
														</div>
													</div>
													<div class="ss_nei_ti">
														${faq.answer}
													</div>
												</div>
											</li>
										</pg:item>
									</c:forEach>
								</ul>
								<pg:index>
									<center>
										<font face="Helvetica"> <pg:first>
												<a href="${pageUrl}"><nobr>[首页]</nobr>
												</a>
											</pg:first> <pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a>
											</pg:prev> <pg:pages>
												<c:choose>
													<c:when test="${pageNumber eq currentPageNumber}">
														<b>&nbsp;&nbsp;${pageNumber}</b>
													</c:when>
													<c:otherwise>
														<a href="${pageUrl}">&nbsp;&nbsp;[${pageNumber}]</a>
													</c:otherwise>
												</c:choose>
											</pg:pages> <pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a>
											</pg:next> <pg:last>&nbsp;<a href="${pageUrl}"><nobr>[末页]</nobr>
												</a>&nbsp;&nbsp;共${pageNumber}页</pg:last> <br /> </font>
									</center>
									<br />
								</pg:index>
							</div>
						</pg:pager>
					</td>
				</tr>
			</table>
		</div>
		</center>
	</body>
</html>
