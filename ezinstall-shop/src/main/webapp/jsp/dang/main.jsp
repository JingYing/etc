<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>main</title>
		<link href="css/style_dang.css" rel="stylesheet" type="text/css" />
		<%@ include file="/jsp/common2.jsp"%>
		<script type="text/javascript">
			function down(obj)	{
				var name = $(obj).attr("name");
				var filePath = $(obj).attr("filePath");
				var url = "${pageContext.request.contextPath}/dang/docDown.action?name=" + name + "&filePath=" + filePath; 
				window.location = encodeURI(url);
			}
			
			function showNext(obj)	{
				var filePath = $(obj).attr("filePath");
				window.location = "${pageContext.request.contextPath}/dang/docMain.action?filePath=" + filePath;
			}
			
		</script>
	</head>
	
	<body>
		<c:if test="${empty diskElementList}"><br/><br/><br/><br/><center><font color="grey">该文件夹为空</font></center></c:if>
		<div class="content-con2">
			<c:forEach items="${diskElementList}" var="diskElement">
			<c:choose>
				<c:when test="${!empty diskElement.children}">
					<div class="file">
						<div class="file-img"><a title="${diskElement.name}" href="javascript:void(0);" ondblclick="showNext(this)" filePath="${diskElement.str}">
							<img src="img/dang/${fn:toLowerCase(diskElement.suffix)}.png" onerror="javascript:this.src='img/dang/unknown.png'"/></a></div>
						<div class="file-name">${diskElement.name}</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="file">
					<div class="file-img"><a title="${diskElement.name}" href="javascript:void(0);" ondblclick="down(this)" filePath="${diskElement.str}" name="${diskElement.name}">
							<img src="img/dang/${fn:toLowerCase(diskElement.suffix)}.png" onerror="javascript:this.src='img/dang/unknown.png'"/></a></div>
						<div class="file-name">${diskElement.name}</div>
					</div>
				</c:otherwise>
			</c:choose>
			</c:forEach>
			<div class="clear"></div>
		</div>
	</body>
</html>
