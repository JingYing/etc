<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>应用首页--二级类别</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<%@ include file="/jsp/common2.jsp"%>
		<script type="text/javascript" src="js/js_woshop/softlist.js"></script>

		<STYLE type=text/css>
			BODY {
				FONT-SIZE: 14px;
				FONT-FAMILY: "微软雅黑";
				background-color: #FFFFFF;
				margin-left: 0px;
				margin-top: 0px;
				margin-right: 0px;
				margin-bottom: 0px;
				top: 10px;
			}
			#tags {PADDING-LEFT: 0px; width: 80px;}
			#tags LI { FLOAT: left; HEIGHT: 38px; WIDTH: 80px; }
			#tags LI.selectTag {
				BACKGROUND: url(img/c-select.png); background-repeat: no-repeat; HEIGHT: 38px; background-position: center center; }
			p.hj { margin-top: 6px;line-height: 28px; font-size: 12px}
			a:link { color: #666666; text-decoration: none; }
			body,td,th {
				color: #333333;
				z-index: auto;
			}
			a:visited { text-decoration: none; color: #000000; }
			a:hover { text-decoration: none; color: #FF0000; }
			a:active { text-decoration: none; color: #003399; }
			.STYLE9 { font-size: 12px; }
			.STYLE13 { font-family: "微软雅黑"; color: #000000; font-size: 14px; }
			.STYLE14 { color: #FF0000; font-weight: bold; }
			.STYLE15 { color: #333333; }
			.STYLE17 { font-size:14px; font-weight:bold; height:31px; line-height:31px; overflow:hidden; }
			.STYLE18 {font-size: 9px}
			.STYLE19 {font-size: 10px}
		</STYLE>
	</head>

	<body>
        <table width="1%" border="0" align="left">
	  		<td width="1%" align="left" valign="top">
				<div style="position:fixed; top:-5px; align:center; background-color:#FFFFFF" >
					<br/>
					<a href="javascript:void(0);" onclick="javascript:self.parent.left.multiApp();" target="left" > 
						&nbsp;&nbsp;<img src="img_woshop/yjanz.png" width="89" height="32" />
					</a>
        			<br/>
        			<hr size="1" width="92" />
        			<br/>
        			<span align="center" class="STYLE13">&nbsp;&nbsp;&nbsp; <strong>分类导航</strong></span>
        		</div>
        		<br/>
        		<br/>
        		<br/>
        		<br/>
        		<br/>
        		<br/>
        		<br/>
        		<div id="tags">
        			<c:if test="${empty appTypeList }"> <center> 对不起, 没有找到相关的软件类别! </center> </c:if>
      				<c:forEach items="${appTypeList }" var="type">
	          			<div class="selectTag"> 
			            	<div align="center">
			            		<span class="hj"> 
			            			<strong>
			            			  <span class="STYLE18">&nbsp;■</span> 
			            			    <a href="${pageContext.request.contextPath}/woshop/left.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&typeId=${type.id }&imoduletype=10#type${type.id }" target="left"> ${type.name }</a>
			            			</strong>
			            		</span>
			            	</div><br/>
	          			</div>
	          		
          			</c:forEach>
        		</div>
        		
      		</td>
      	</table>
	</body>
</html>