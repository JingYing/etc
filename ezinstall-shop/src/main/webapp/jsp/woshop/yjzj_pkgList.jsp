<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()	+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<base href="<%=basePath%>"/>
		<title>一键装机</title>
		<link href="css_woshop/style_huodong.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css_woshop/soft.css" type="text/css"></link>
		<link rel="stylesheet" href="css_woshop/style.120518.css" type="text/css"></link>
		<%@ include file="/jsp/common2.jsp" %>
		<script type="text/javascript">
			$(function()	{
				utilInit();
			});
			
			function packDown(packId)	{
				var url = "${pageContext.request.contextPath}/woshop/packDown.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=5&pkgId=" + packId;
				window.location = url;
			}
		</script>
	</head>

	<body id="huodong">
		<div id="huodong-containner">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<br/>
				<c:forEach items="${pkgMap}" var="entry">
				<tr>
					<td valign="top">
						<div id="huodong-3g-l">
							<div id="huodong-3g-l-m">
				 				<c:if test="${entry.key ne null}">
									<table  border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="20" align="center" valign="top">&nbsp;</td>
											<td width="110" valign="top"><div class="huodong-3g-l-m-icon">
												<img src="img_woshop/loading.gif" data-original="img_woshop/huodong/qghd${entry.key.id}.png"/></div>
											</td>
											<td>
												<div id="huodong-3g-n">${entry.key.name}</div>
												<div id="huodong-3g-n">
													<input type="button" class="huodong-3g-btn" value="一键安装" onclick="packDown('${entry.key.id}')"/>
												</div>
											</td>
										</tr>
									</table>
				 				</c:if>
							</div>
						</div>
					</td>
					<td>
						<div id="huodong-3g-r">
							<div id="huodong-3g-r-t">${entry.key.name} 详情</div>
							<div id="huodong-3g-r-n">
								<c:if test="${empty entry.value}"><center><font color="grey">对不起，暂无相关应用</font></center></c:if>
								<%-- 只显示6条 --%>
								<c:forEach items="${entry.value}" var="app" begin="0" end="5">
									<div id="huodong-3g-k">
										<div id="huodong-3g-k-icon">
											<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
											<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
												<span class="tj"></span>
											</c:if>
											<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
												<span class="rm"></span>
											</c:if>
											<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
												<span class="jl"></span>
											</c:if>
											<c:if test="${app.markerType == null || '' eq app.markerType }">
												<span class=""></span>
											</c:if>
											<a title="${app.name}" href="woshop/appInfo.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=5">
												<img src="img/border.gif" data-original="${fileServerUrl}${app.icon}"/>
											</a>
										</div>
										<div id="huodong-3g-k-name">${fnx:abbreviate(app.name, 12, "...")}</div>
									</div>
								</c:forEach>
								<div class="clear"></div>
							</div>
						</div>
						<br/><br/>
					</td>
				</tr>
				</c:forEach>
			</table>
		</div>
	</body>
</html>
