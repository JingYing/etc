<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>厅 修 改</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/jsp/common2.jsp"%>
		<SCRIPT type=text/javascript>
			$(function() {
				//注册 渠道类型 的改变事件
				$("#ctid").change(function(){
					$("#ccid").children().remove();
					$("#ccid").append($("<option></option>").val("0").text("----请选择----"));
					$("#rccid").children().remove();
					$("#rccid").append($("<option></option>").val("98").text("----请选择----"));
					$.get("${pageContext.request.contextPath}/hall/ccList.action",
						{ctid:this.value,_:new Date().getTime()}, 
						function(data) {
							$.each(data, function() {
								$("#ccid").children().remove();
								$("#ccid").append($("<option></option>").val(this.id).text(this.name));
							});
						},"json"
					);
				});
				
				//注册 合作渠道类型 的改变事件
				$("#ccid").change(function(){
					$("#rccid").children().remove();
					$("#rccid").append($("<option></option>").val("98").text("----请选择----"));
					$.get("${pageContext.request.contextPath}/hall/rccList.action", 
						{ccid:this.value,_:new Date().getTime()}, 
						function(data) {
							$.each(data, function() {
								$("#rccid").children().remove();
								$("#rccid").append($("<option></option>").val(this.id).text(this.name));
							});
						},"json"
					);
				});
			});
			
			function tijiao(){
				if($("#hallAddr").val()=="" || $("#description").val()==""  || $("#ctid").val()==0 ||$("#ccid").val()==0)
				{
					alert("请填写基本网点信息！");
					return false;
				}
				$('#form').submit();
				if(true){
					alert("修改成功...");
				}
			}
			
		    function selectTag(showContent,selfObj){
				// 操作标签
				var tag = document.getElementById("tags").getElementsByTagName("li");
				var taglength = tag.length;
				for(i=0; i<taglength; i++){
					tag[i].className = "";
				}
				selfObj.parentNode.className = "selectTag";
				// 操作内容
				for(i=0; j=document.getElementById("tagContent"+i); i++){
					j.style.display = "none";
				}
				document.getElementById(showContent).style.display = "block";	
			}
		</SCRIPT>
		
		<STYLE type=text/css>
UL {
	LIST-STYLE-TYPE: none;
}

#con {
	FONT-SIZE: 14px;
	BORDER-RIGHT: #aecbd4 0px solid;
	BORDER-LEFT: #aecbd4 0px solid;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 0px;
	background-image: url(img/qbj.png);
	height: 35px;
}

#tags {
	PADDING-RIGHT: 10px;
	PADDING-LEFT: 10px;
	PADDING-BOTTOM: 0px;
	MARGIN: -9px 0px 0px 200px;
	HEIGHT: 35px;
	line-height: 35px;
}

#tags LI {
	BACKGROUND: url();
	FLOAT: left;
	MARGIN-RIGHT: 3%;
	HEIGHT: 30px;
	WIDTH: 80px;
	line-height: 35px;
	text-align: center;
}

#tags LI.selectTag {
	BACKGROUND: url(img/h-select.png);
	MARGIN-BOTTOM: 0px;
	HEIGHT: 30px;
	WIDTH: 80px;
	background-repeat: no-repeat;
	line-height: 35px;
}

a:link {
	color: #000000;
	text-decoration: none;
}

body,td,th {
	color: #333333;
	font-size: 14px;
}

a:visited {
	text-decoration: none;
	color: #000000;
}

a:hover {
	text-decoration: none;
	color: #FF0000;
}

a:active {
	text-decoration: none;
	color: #0033FF;
}

a {
	font-size: 14px;
}

p.hj {
	line-height: 35px
}

.STYLE20 {
	font-family: "微软雅黑";
	font-size: 15px;
}

.STYLE21 {
	font-family: "微软雅黑";
	font-size: 14px;
}

.STYLE22 {
	color: #FF0000
}

.STYLE23 {
	font-family: "微软雅黑";
	font-size: 12px;
	color: #FF0000;
}
		</STYLE>
	</head>

	<body>
		<form action="${pageContext.request.contextPath}/hall/updateHall.action" method="post" name="form" id="form" >
			<div id="con">
				<ul id="tags">
					<!-- <li class="current">
						<a onclick="selectTag('tagContent0',this)" href="javascript:void(0)" hidefocus="true">
							<span class="STYLE20"> <strong>个人信息</strong> </span>
						</a>
					</li> -->
	
					<li>
						<a onClick="selectTag('tagContent1',this)" href="javascript:void(0)" hidefocus="true">
							<span class="STYLE20"> <strong>厅 信 息<strong> </span>
						</a>
					</li>
	
					<!-- <li>
						<a href="javascript:void(0)" target="_top" onClick="selectTag('tagContent2',this)" hidefocus="true">
							<span class="STYLE20"> <strong>密码修改<strong> </span>
						</a>
					</li> -->
				</ul>
			</div>
		
			<div id=tagContent>
				<!-- <div class="tagContent selectTag" id=tagContent0>
					<div class="softindex-1">
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
							<tr>
								<td height="20" align="center" valign="middle">
									<p> &nbsp; </p>
									<table width="85%" border="0" align="center" cellpadding="0"
										cellspacing="8">
										<tr>
											<td width="50%" align="left" valign="middle">
												<p>
													<label>
														<span class="STYLE20">登录帐号：</span>
													</label>
													<label>
														<input name="textfield" type="text" maxlength="255"
															style="font-size: 14px" />
													</label>
													<span class="STYLE19 STYLE22"> * </span>
												</p>
											</td>
											<td width="50%" align="left" valign="middle">
												<label>
													<span class="STYLE20">真实姓名：</span>
												</label>
												<label>
													<input name="textfield4" type="text" maxlength="255" />
												</label>
												<span class="STYLE19 STYLE22"> * </span>
											</td>
										</tr>
										<tr>
											<td align="left" valign="middle">
												<p>
													<label>
														<span class="STYLE20">手机号码：</span>
													</label>
													<label>
														<input name="textfield3" type="text" maxlength="255" />
													</label>
													<span class="STYLE19 STYLE22"> * </span>
												</p>
											</td>
											<td align="left" valign="middle">
												<label>
													<span class="STYLE20">选择银行：</span>
												</label>
												<select name="select2" class="STYLE21">
													<option>
														请选择
													</option>
													<option selected="selected">
														中国工商银行&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</option>
													<option>
														中国农业银行
													</option>
													<option>
														中国建设银行
													</option>
													<option>
														中国银行
													</option>
													<option>
														招商银行
													</option>
													<option>
														北京银行
													</option>
													<option>
														交通银行
													</option>
													<option>
														华夏银行
													</option>
													<option>
														中信银行
													</option>
												</select>
											</td>
										</tr>
										<tr>
											<td align="left" valign="middle">
												<p>
													<span class="STYLE20">银行卡号：</span>
													<label>
														<input name="textfield2" type="text" maxlength="255" />
													</label>
												</p>
											</td>
											<td align="left" valign="middle">
												<span class="STYLE20">持卡人姓名：</span>
												<label>
													<input name="textfield22" type="text" maxlength="255" />
												</label>
											</td>
										</tr>
									</table>
									<p>
										&nbsp;
									</p>
								</td>
							</tr>
							<tr>
								<td height="28" align="left" valign="middle">
									<span class="STYLE21"> <label>
											<div align="center">
												<input name="Submit" type="submit" class="STYLE21"
													value="保  存" style="width: 100px; height: 30px;" />
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input name="Submit2" type="submit" class="STYLE21"
													value="返  回" style="width: 100px; height: 30px;" />
											</div>
										</label> </span>
								</td>
							</tr>
						</table>
					</div>
				</div> -->
				
				<div class=tagContent id=tagContent1>
					<div class="softindex">
						<table width="100%" border="0" align="center" cellpadding="0"
							cellspacing="2">
							<tr>
								<td height="20" align="center" valign="middle">
									<p>
										&nbsp;
									</p>
									<table width="85%" border="0" align="center" cellpadding="0"
										cellspacing="8">
										<tr>
											<td align="left" valign="middle">
												<label>
													<span class="STYLE20">网点名称：</span>
												</label>
												<label>
													<input name="textfield5" type="text" maxlength="255" value="${hall.hallName }" disabled="disabled"/>
												</label>
												
												<!-- 隐藏属性 -->
												<input type="hidden" name="hall.id" value="${hall.id}"/>
										    	<input type="hidden" name="hall.hallName" value="${hall.hallName}"/>
										    	<input type="hidden" name="hall.hallNo" value="${hall.hallNo}"/>
										    	
										    	<input type="hidden" name="hall.province.id" value="${hall.province.id}"/>
										    	<input type="hidden" name="hall.city.id" value="${hall.city.id}"/>
										    	<input type="hidden" name="hall.county.id" value="${hall.county.id}"/>
										    	
										    	<input type="hidden" name="hall.ctid" value="${hall.ctid}"/>
										    	<input type="hidden" name="hall.ccid" value="${hall.ccid}"/>
										    	<input type="hidden" name="hall.rccid" value="${hall.rccid}"/>
										    	<input type="hidden" name="hall.bhid" value="${hall.bhid}"/>
										    	
										    	<input type="hidden" name="hall.chanType" value="${hall.chanType}"/>
										    	<input type="hidden" name="hall.chanGrade" value="${hall.chanGrade}"/>
										    	<input type="hidden" name="hall.chanLevel" value="${hall.chanLevel}"/>
										    	
										    	<input type="hidden" name="hall.hasLogined" value="${hall.hasLogined}"/>
										    	<input type="hidden" name="hall.deployTime" value="${hall.deployTime}"/>
										    	
											</td>
											<td align="left" valign="middle">
												<label>
													<span class="STYLE20">网点地址：</span>
												</label>
												<label>
													<input name="textfield42" type="text" maxlength="255" value="${hall.hallAddr }"/>
												</label>
												<span class="STYLE19 STYLE22"> *</span>
											</td>
										</tr>
										<tr>
											<td align="left" valign="middle">
												<label>
													<span class="STYLE20">省&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分：</span>
												</label>
												<label>
													<select name="${hall.province.id }" id="pro" class="STYLE21" disabled="disabled">
														<c:if test="${!empty hall.province}">
															<option value="${hall.province.id }" selected="selected">
																${hall.province.name }
															</option>
														</c:if>
													</select>
												</label>
											</td>
											<td align="left" valign="middle">
												<label>
													<span class="STYLE20">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市：</span>
												</label>
												<select name="${hall.city.id }" id="city" class="STYLE21" disabled="disabled">
													<c:if test="${!empty hall.city }">
														<option value="${hall.city.id }" selected="selected">
															${hall.city.name }
														</option>
													</c:if>
												</select>
											</td>
										</tr>
										<tr>
											<td align="left" valign="middle">
												<span class="STYLE20">行政区域：</span>
												<label>
													<select name="${hall.county.id }" id="county" class="STYLE21" disabled="disabled">
														<c:if test="${!empty hall.county }">
															<option value="${hall.county.id }" selected="selected">
																${hall.county.name }
															</option>
														</c:if>
													</select>
												</label>
											</td>
											<td align="left" valign="middle">
												<span class="STYLE20">渠道类型：</span>
												<label>
													<select name="${hall.ctid }" id="ctid" class="STYLE21">
														<c:forEach items="${channelList}" var="ct">
															<option value="${ct.id}"} ${ct.id eq hall.ctid ? "selected" : "" }>
																${ct.ctName}
															</option>
														</c:forEach>
													</select>
												</label>
												<span class="STYLE19 STYLE22"> * </span>
											</td>
										</tr>
										<tr>
											<td align="left" valign="middle">
												<span class="STYLE20">合作类型：</span>
												<label>
													<select name="${hall.ccid }" id="ccid" class="STYLE21">
														<option value="${hall.ccid }">
															${cc.ccName }
														</option>
													</select>
												</label>
												<span class="STYLE22">*</span>
											</td>
											<td align="left" valign="middle">
												<span class="STYLE20">具体渠道：</span>
												<label>
													<select name="${hall.rccid }" id="rccid" class="STYLE21">
														<option value="${hall.rccid }">
															${rcc.rccName }
														</option>
													</select>
												</label>
											</td>
										</tr>
										<tr>
											<td colspan="2" align="left" valign="top">
												<span class="STYLE20">网点描述：</span>
												<label>
													<textarea name="description" id="description" cols="52"> ${hall.description } </textarea>
												</label>
											</td>
										</tr>
									</table>
									<p>
										&nbsp;
									</p>
								</td>
							</tr>
							<tr>
								<td height="28" align="left" valign="middle">
									<span class="STYLE21"> 
										<label>
											<div align="center">
												<input name="Submit3" type="submit" class="STYLE21"
													value="保  存" style="width: 100px; height: 30px;" />
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input name="Submit22" type="submit" class="STYLE21"
													value="返  回" style="width: 100px; height: 30px;" />
											</div>
										</label> 
									</span>
								</td>
							</tr>
						</table>
					</div>
				</div>
				
				<!--<div class=tagContent id=tagContent2>
					<div class="softindex">
						<table width="100%" border="0" align="center" cellpadding="0"
							cellspacing="2">
							<tr>
								<td height="20" align="center" valign="middle">
									<p>
										&nbsp;
									</p>
									<table width="68%" border="0" align="center" cellpadding="0"
										cellspacing="8">
										<tr>
											<td width="100%" align="left" valign="middle">
												<p>
													<label>
														<span class="STYLE20"><span class="STYLE22">
																*</span> 当前密码：</span>
													</label>
													<label>
														<input name="textfield6" type="text" maxlength="255" />
													</label>
													<span class="STYLE23"> * 输入当前登录密码</span>
												</p>
											</td>
										</tr>
										<tr>
											<td align="left" valign="middle">
												<p>
													<label>
														<span class="STYLE20"><span class="STYLE22">
																*</span> 新&nbsp;&nbsp;密&nbsp;&nbsp;码：</span>
													</label>
													<label>
														<input name="textfield33" type="text" maxlength="255" />
													</label>
													<span class="STYLE23"> * 密码长度最少六位字符 </span>
												</p>
											</td>
										</tr>
										<tr>
											<td align="left" valign="middle">
												<p>
													<label>
														<span class="STYLE20"><span class="STYLE22">
																* </span>确认密码：</span>
													</label>
													<label>
														<input name="textfield24" type="text" maxlength="255" />
													</label>
													<span class="STYLE23">* 密码长度最少六位字符 </span>
												</p>
											</td>
										</tr>
									</table>
									<p>
										&nbsp;
									</p>
								</td>
							</tr>
							<tr>
								<td height="28" align="left" valign="middle">
									<span class="STYLE21"> <label>
											<div align="center">
												<input name="Submit4" type="submit" class="STYLE21"
													value="保  存" style="width: 100px; height: 30px;" />
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input name="Submit23" type="submit" class="STYLE21"
													value="返  回" style="width: 100px; height: 30px;" />
											</div>
										</label> </span>
								</td>
							</tr>
						</table>
					</div>
				</div> -->
				
				<div class="tanchubj popUp" id="tanchulei" style="position: absolute; left: 620.5px; top: 745px; display: none;"> </div>
	
				<DIV style="LINE-HEIGHT: 57px; HEIGHT: 57px" class=ztd1>
					<CENTER>
						<FONT size=2 face=Helvetica><BR> </FONT>
					</CENTER>
				</DIV>
			
			</div>
		</form>
	</body>
</html>
