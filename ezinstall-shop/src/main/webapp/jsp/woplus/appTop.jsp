<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
	<head>
		<title>应用首页--右框架的顶部框架</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css_woshop/hot0406.css">
		<LINK rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<script src="js/jquery-1.7.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/js_woshop/softlist.js"></script>
		<script type="text/javascript">
			$(function()	{
				//键盘按下事件
	   			$(document).keydown(function(event) {
					if (event.keyCode == 13) {
						$('form').each(function() {
							//搜索的代码
							if($("#keyword").val() == null || $("#keyword").val() == ""){
								window.external.SearchFunc("请输入软件名称进行搜索！");
							}else{
								document.searchForm.submit(); 
							}
						});
					}
				});
			});
			
			function searchApp(){
				document.searchForm.submit(); 
			}
		</script>
		
		<STYLE type=text/css>
			.STYLE11 {
				font-size: 14px;
				font-family: "微软雅黑";
				color: #333333;
			}
			.STYLE23{border:solid 1px rgb(159,159,159); height:24px; width:120px; padding-bottom:4px;}
		</STYLE>
	</head>

	<body>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<form id="searchForm" name="searchForm" action="woplus/search.action" method="get" target="_top">
				<tbody>
					<tr height="28">
						<td align="right" valign="middle" bgcolor="RGB(225,225,225)">
							<input type="hidden" name="imoduletype" value="10"/>
								
							<input type="hidden" name="empId" value="${empId}"/>
							<input type="hidden" name="appOsType" value="${appOsType}"/>
							<input type="hidden" name="version" value="${version}"/>
							<input type="hidden" name="isIOS" value="${isIOS}"/>
							<input type="hidden" name="installType" value="${installType}"/>
							<input type="hidden" name="pageSize" value="${pageSize}"/>
								
							<input type="text" name="keyword" id="keyword" class="STYLE23" 
								onclick="javascript:$('#keyword').val('');" maxlength="255" value="${keyword }"/>
						</td>
						<td width="2.5%" valign="middle" bgcolor="RGB(225,225,225)">
							<div style="position:absolute; right:0px; top: 1px;">
								<a href="javascript:searchApp()">
									<img src="img_woshop/fdj.jpg" width="39" height="25" align="absmiddle" />
								</a>
							</div>
						</td>
					</tr>
				</tbody>
			</form>
		</table>
		
	</body>
</html>