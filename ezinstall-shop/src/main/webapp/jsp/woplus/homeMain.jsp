<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<html>
	<head>
		<title>首页框架集</title>
	</head>
	<frameset border="0" frameSpacing="0" frameBorder="no" cols="220,*">
		<!-- noResize: 设定不让使用者可以改变这个框框的大小，亦没有设定此参数，使用者可以很随 意地拉动框架，改变其大小。 -->
		<!-- scrolling: 设定是否要显示卷轴，YES 表示要显示卷轴，NO 表示无论如何都不要显示， AUTO是视情况显示。 -->
		<frame name="left" noResize scrolling="no" src="${pageContext.request.contextPath}/woplus/homeLeft.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}" >
		<frame name="right" id="main" src="${pageContext.request.contextPath}/woplus/homeRight.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}" >
		
		<noframes><body> 很抱歉，您的浏览器无法处理框架，请转用其它浏览器！ </body></noframes>
	</frameset>
</html>