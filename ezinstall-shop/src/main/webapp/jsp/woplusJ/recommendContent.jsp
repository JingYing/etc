<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>

<c:choose>
	<c:when test="${ servType == '01' }">
		<div class="softindex">
			<ul id="tag">
				<c:if test="${empty pkgList}"> <center> 对不起, 没有找到相关的套餐! </center> </c:if>
				<c:forEach items="${pkgList}" var="pack">
				<li class="selectTag" style="border-right: 1px solid #CCC;">
					<div align="left" style="margin:7px; position:relative; left:0%; padding:3px; width:100%; top: 0%;"> 
						<img src="img/loading.gif" data-original="img_woshop/pack1G.jpg" width="75" height="75" align="middle" />
						<div align="left" style="margin:0px 0px 0px 76px; position:relative; left:0px; top:-83px; padding:8px; width:80%;"> 
							<span class="STYLE19">${pack.name }</span><br/>
							<div align="right" style="padding-right:32px;">
								<input type="button" name="Submit22" value="订购"  style="width:72px;height:30px;border:0px;background-image: url(img_woshop/InstallButton_normal.png)" />
							</div>
						</div>
					</div>
				</li>
				</c:forEach>
			</ul>
		</div>
	</c:when>
	<c:otherwise>
		<div class="softindex">
			<ul id="tag">
				<c:if test="${ empty appList}"> <center> 对不起, 没有找到相关的软件! </center> </c:if>
				<c:forEach items="${appList}" var="app">
		       		<li class="selectTag" style="border-right: 1px solid #CCC;">
		       			<div align="left" style="margin:7px; position:relative; left:0%; padding:3px; width:100%; top: 0%;"> 
		       				<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" width="72" height="72" align="middle" />
		                	<div align="left" style="margin:0px 0px 0px 76px; position:relative; left:0px; top:-83px; padding:8px; width:80%;"> 
		                 		<span class="STYLE19"> ${app.name } </span><br/>
		                    	<span class="STYLE22"> ${app.rscTypeName } </span><br/>
		                    	<span class="STYLE22"> ${fnx:abbreviate(app.info,200, "...")} </span>
		                    	<div align="right" style="padding-right:32px;">
			                   		<input type="button" name="Submit2" style="width:89px;height:30px;border:0px;background-image: url(img_woshop/yjanz.png)" onclick="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}');"/>
		                   		</div>
							</div>
						</div>
					</li>
				</c:forEach>
			</ul>	
		</div>
	</c:otherwise>
</c:choose>


