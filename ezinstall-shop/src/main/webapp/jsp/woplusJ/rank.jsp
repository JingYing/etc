<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>下载排行</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css"
			href="css/css_woshop/hot0406.css">
		<link rel="stylesheet" type="text/css"
			href="css/css_woshop/global.css">
		<link rel="stylesheet" type="text/css"
			href="css/css_woshop/style.120517.css">
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<link rel="stylesheet" href="css/css_woshop/qh.css">
		<%@ include	file="/jsp/common2.jsp"%>
		<script type="text/javascript">
		
		var arr = new Array('m','t','w');

		function calcChecked()	{
			var checkedCount = 0, totalSize = 0.0;
			for(i in arr)	{
				$("input[id^='" + arr[i] + "Check_']").each(function()	{
					if(this.checked == true)	{
						checkedCount ++;
						var fileSize = $("input[id='" + arr[i] + "FileSize_" + this.value + "']").val();
						if(fileSize == null || "" == fileSize){
							fileSize = "0.00";
						}
						totalSize += parseInt(fileSize);
					}
				});
			}
			$('#checkCount').html(checkedCount);
			$('#totalSize').html((totalSize/1024.0).toFixed(1));
		}
		
			$(function()	{
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/zwtp.png"
	   			});
	   			
	   			//初始化按钮的状态
	   			$('img').imgAlter();
	   			
	   			//键盘按下事件
	   			$(document).keydown(function(event) {
					if (event.keyCode == 13) {
						$('form').each(function() {
							//搜索的代码
							if($("#keyword").val() == null || $("#keyword").val() == ""){
								window.external.SearchFunc("请输入软件名称进行搜索！");
							}else{
								document.searchForm.submit(); 
							}
						});
					}
				}); 
				
	   			/**全局排行，全选**/
				$('#ph_checkAll').change(function()	{
					for(i in arr)	{
						$("input[id^='" + arr[i] + "_checkAll']").attr('checked', this.checked);
						$("input[id^='" + arr[i] + "Check_']").attr('checked', this.checked);
					}
					calcChecked();
				});
				
				 //周排行, 月排行, 总排行的全选框单击事件
				for(i in arr)	{
					$("#" + arr[i] + "_checkAll").change(function()	{
						var index = this.id.replace("_checkAll", '');
						$("input[id^='" + index + "Check_']").attr('checked', this.checked);
						calcChecked();
					});
				}
				
				/**
				 * 复选框点击后发生的事件, 更新选中数和软件总大小
				 */ 
				$(':checkbox').change(function()	{
					calcChecked();
				});
				
				
				//鼠标事件--弹出框
   				$(document).ready(function () {
					$(".app").mouseover(function () {
						var b = $(this).find(".popUp").html();
						if (b) {
							$("#tanchulei").html(b);
							downDisplay(this);
						}
						$(this).find(".downtimes").hide().end().find(".down").show();
					}).mouseout(function () {
						$("#tanchulei").hide();
						$(this).find(".downtimes").show().end().find(".down").hide();
					});
				});
			});
			//鼠标事件--弹出框
			function downDisplay(k) {
				var i = $(k).parents().offset().left;
				var n = $(k).offset().left + 75;
				var j = $(k).offset().top + 5;
				var m = n - i;
				var h = $(k).parents().width() - m;
				var l = $("#tanchulei").width();
				if (l > h) {
					var n = $(k).offset().left - 180;
					var j = $(k).offset().top + 5;
					//$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				} else {
					$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				}
			}
			
			
			
			//下载排行 勾选安装处理
			function ph_multiApp()	{
				var selectFlags = document.getElementsByName("selectFlag");
				var num = 0;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							var url = "${pageContext.request.contextPath}/woplusJ/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=8&ids=" + ids ;
							if(num == 0){
								num = 1;
								myweb.MyOwnFunc(1,arr[0],arr[1], url);
							}else{
								myweb.MyOwnFunc(0,arr[0],arr[1], url);
							}
						}
					}
				}
			}
			
			//单个下载安装
			function downApp(appName, rscTypeId, ids){
				var url = "${pageContext.request.contextPath}/woplusJ/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=8&ids=" + ids;
				//alert(appName+", "+ rscTypeId +", "+url);
				myweb.MyOwnFunc(1,appName, rscTypeId, url);
			}
			
			function searchApp(){
				if($("#keyword").val() == null || $("#keyword").val() == ""){
					window.external.SearchFunc("请输入软件名称进行搜索！");
				}else{
					document.searchForm.submit(); 
				}
			}
			
			/**
			* 按钮状态变化
			*/
			function clientInvoke(u_sStr){
				//尹工打算将多个uuid和状态值一次性传过来，我这边可能需要做个循环处理
				var u_sArr = u_sStr.split(",");
				for(i=0; i<u_sArr.length; i++){
					var u_s = u_sArr[i].split(":");
					var uuid = u_s[0];
					var status = u_s[1];
	   				$("img").imgAlterTrigger(uuid, status);
				}
   			}
			
		</script>

		<STYLE type=text/css>
			BODY {
				FONT-SIZE: 14px;
				FONT-FAMILY: "微软雅黑";
				background-color: #FFFFFF;
				background-repeat: no-repeat;
				background-image: url(img/bd.png);
			}
			
			UL {
				LIST-STYLE-TYPE: none
			}
			
			#con {
				FONT-SIZE: 14px;
				BORDER-RIGHT: #aecbd4 1px solid;
				BORDER-LEFT: #aecbd4 1px solid;
				margin-top: 0px;
				margin-right: 10px;
				margin-bottom: 0px;
				margin-left: 10px;
				background-image: url(img_woshop/qbj.png);
			}
		
			#tags {
				PADDING-RIGHT: 3px;
				PADDING-LEFT: 3px;
				PADDING-BOTTOM: 0px;
				MARGIN: 0px 0px 0px 8px;
				HEIGHT: 35px
			}
			
			#tags LI {
				BACKGROUND: url();
				FLOAT: left;
				MARGIN-RIGHT: 1%;
				HEIGHT: 30px;
				WIDTH: 80px
			}
			
			#tags LI.selectTag {
				BACKGROUND: url(img_woshop/h-select.png);
				MARGIN-BOTTOM: 0px;
				HEIGHT: 30px
			}
			
			#tagContent {
				BORDER-RIGHT: #aecbd4 1px solid;
				BORDER-LEFT: #aecbd4 1px solid;
				BORDER-BOTTOM: #aecbd4 1px solid;
				margin-left: 10px;
				margin-right: 10px;
			}
		
			a:link {
				color: #000000;
				text-decoration: none;
			}
			
			body,td,th {
				color: #333333;
			}
			
			a:visited {
				text-decoration: none;
				color: #000000;
			}
			a:hover {
				text-decoration: none;
				color: #FF0000;
			}
			a:active {
				text-decoration: none;
				color: #0000cc
			}
			
			.STYLE13 {
				font-family: "微软雅黑";
				color: #000000;
				font-size: 14px;
			}
			
			.STYLE14 {
				color: #FF0000;
				font-weight: bold;
			}
			
			p.hj {
				line-height: 30px
			}
			
			.STYLE18 {
				font-family: "微软雅黑";
				color: #000000;
				font-size: 14px;
				font-weight: bold;
			}
			
			.STYLE19 {
				font-size: 14px;
				font-weight: bold;
			}
			
			.STYLE23 {
				color: #666666;
				font-size: 18px;
				font-weight: bold;
			}
		</STYLE>
	</head>

	<body>
		<form id="searchForm" name="searchForm" action="woplusJ/search.action" method="get">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td width="72%" height="28" background="img_woshop/searchBar.png">
							<table width="100%" border="0">
								<tr>
									<td width="2%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(-1);"> 
											<img src="img_woshop/houtui.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(1);">
											<img src="img_woshop/qianjing.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="woplusJ/rank.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}">
											<img src="img_woshop/shuaxin.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="62%">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
						<td width="22%" align="right" valign="middle" background="img_woshop/searchBar.png">
							<div class="ss_1">
								<input type="hidden" name="imoduletype" value="8"/>
								
								<input type="hidden" name="empId" value="${empId}"/>
								<input type="hidden" name="isIOS" value="${isIOS}"/>
								<input type="hidden" name="installType" value="${installType}"/>
								<input type="hidden" name="appOsType" value="${appOsType}"/>
								<input type="hidden" name="version" value="${version}"/>
								<input type="hidden" name="pageSize" value="${pageSize}"/>
								<input type="hidden" name="pageSize2" value="${pageSize2}"/>
								
								<input name="keyword" class="ss_text" id="keyword"
									onclick="javascript:$('#keyword').val('');" maxlength="255"  value="${keyword }"/>
							</div>
						</td>
						<td width="4%" valign="middle" background="img_woshop/searchBar.png">
							<div class="ss_2">
								<a href="javascript:searchApp()"><img src="img_woshop/search-k.png"></a>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		
		<table width="100%" border="0" align="center" cellspacing="18">
			<tr>
				<td width="66%" valign="top"  background="img_woshop/q-line.png" style="background-repeat:no-repeat" >
					<span class="STYLE18"> &nbsp;&nbsp; 排 行 榜</span>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="ph_checkbox" id="ph_checkAll" />
					<span class="STYLE13">全选</span>
					<span class="STYLE13"> 
						&nbsp;&nbsp;&nbsp;&nbsp; 已选择
						<span id="checkCount" class="STYLE14">0</span>款， 共&nbsp;
						<span id="totalSize" class="STYLE14">0.00 </span>&nbsp; MB 
					</span>
				</td>
				<td width="8%">
					<div align="right">
						<a href="javascript:ph_multiApp();" target="_self">
							<img src="img_woshop/yjanz.png" width="89" height="32" />
						</a>
					</div>
				</td>
				<td width="16%"></td>
			</tr>
		</table>
	
	
		<table width="100%" border="0" align="center" cellspacing="15">
			<tr>
				<!-- 循环三个榜单 -->
				&nbsp;&nbsp;&nbsp;
				<c:if test="${empty appMap }"> <center> 对不起, 没有找到相关的 排行榜! </center> </c:if>
				<c:forEach items="${appMap}" var="map" varStatus="stat">
					<td valign="top">
						<table width="242" border="0" align="center" cellspacing="0">
							<tr>
								<td height="54" valign="middle" background="img_woshop/list_bt.png">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<!-- 全选框, ID格式为"XXX_checkAll" 如：week_checkAll-->
									<input type="checkbox" name="${fn:substring(map.key, 0, 1)}_selectFlag" id="${fn:substring(map.key, 0, 1)}_checkAll" />
									&nbsp;
									<span class="STYLE19">  ${fn:substring(map.key, 1, fn:length(map.key) )} </span>
								</td>
							</tr>
						
							<!--<c:if test="${empty map.value.list }"> <center> 对不起, 没有找到相关的软件! </center> </c:if>-->
						
							<!-- 开始榜单明细循环 -->
							<c:forEach items="${map.value.list}" var="app" varStatus="i">
							<tr>
								<td height="100" valign="top" background="img_woshop/phbj${i.index % 2}.png" >
									<table width="100%" border="0" cellspacing="5">
										<tr>
											<td width="10%" height="90" align="center" valign="middle">
												<span class="tpc-ph">
													<c:if test="${i.index eq 0 }">
														<span class="list-r">${i.index + 1 } </span>
													</c:if>
													<c:if test="${i.index eq 1 }">
														<span class="list-h">${i.index + 1 } </span>
													</c:if>
													<c:if test="${i.index eq 2 }">
														<span class="list-l">${i.index + 1 } </span>
													</c:if>
													<c:if test="${i.index >= 3 }">
														<span class="list-q">${i.index + 1 } </span>
													</c:if>
												</span>
											</td>
											<td width="30%">
												<form id="form2" name="form2" method="post" action="">
													<input type="checkbox" id="${fn:substring(map.key, 0, 1)}Check_${app.uuid}" name="selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}"/>
													<input type="hidden" id="${fn:substring(map.key, 0, 1)}FileSize_${app.uuid}_${app.packUuid }_${app.discriminator}"  value="${app.packFileSize}"/>
						               				<input type="hidden" id="app.discriminator" value="${app.discriminator}"/>
						               				<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
												</form>
												<div align="center">
													<div class="tpc" id="div6">
														<a href="woplusJ/appInfo.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=8">
															<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
															<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
																<span class="tj"></span>
															</c:if>
															<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
																<span class="rm"></span>
															</c:if>
															<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
																<span class="jl"></span>
															</c:if>
															<c:if test="${app.markerType == null || '' eq app.markerType }">
																<span class=""></span>
															</c:if>
															<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" width="50" height="50" />
														</a>
													</div>
												</div>
											</td>
											<td width="40%" valign="middle">
												<ul id="bodyul">
													<li class="app">
														<p class="hj">
															${fnx:abbreviate(app.name,12, "...")}
														</p>
														<p class="downtimes" style="display: block;">
															${app.downCount } 次下载
														</p>
														
														<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
		                								<a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}');"> 
		                									<c:if test="${installType == 1 }"><!-- pc端直连 -->
			                									<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
			                								</c:if>
			                								<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
			                									<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
			                								</c:if>
			                								<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
			                									<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
			                								</c:if>
			                							</a>
													</li>
												</ul>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</c:forEach>
							
							<tr>
								<td><img src="img_woshop/bottom.png" /></td>
							</tr>
							<!-- 此处结束榜单明细循环  -->
						</table>
					</td>
				</c:forEach>
				<!-- 此处结束三个榜单循环 -->
			</tr>
		</table>
		<p> &nbsp; </p>
		<div id=tagContent>
			<div class="tanchubj popUp" id="tanchulei" style="position: absolute; left: 620.5px; top: 745px; display: none;"/> 
		</div>
		
	</body>
</html>