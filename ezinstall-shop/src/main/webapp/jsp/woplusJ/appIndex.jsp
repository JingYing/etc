<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>应用首页</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css_woshop/hot0406.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/global2.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/style.120517.css">
		<LINK rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<link rel="stylesheet" href="css/css_woshop/qh.css">
		<%@ include file="/jsp/common2.jsp"%>
		<script type="text/javascript">
		
			function calcChecked(str)	{
				var count = 0;
				var size = 0.00;
				$("input[id^='" + str + "Check_']").each(function()	{
					if(this.checked == true) {
						count ++;
						var v = $("#" + str + "FileSize_" + this.value + "").val();
						if(v == null || "" == v){
							v = " 0.00 ";
						}
						size += parseFloat(v);
					}
				});
				$("#" + str + "_checkCount").html(count);
				$("#" + str + "_totalSize").html((size/1024.0).toFixed(2));
			}
			
			function bindCheckbox(str)	{
				$("#" + str + "_checkAll").click(function()	{
					$("input[id^='" + str + "Check_']").attr('checked', this.checked);
					calcChecked(str);
				});
				$("input[type='checkbox'][id^='" + str + "Check_']").change(function()	{
					calcChecked(str);
				});
			}
			
			function resetCheckbox(str)	{
				$("#" + str + "_checkAll").attr("checked",false);
				$("#" + str + "bk_checkCount").html(" 0 ");
					$("#" + str + "bk_totalSize").html(" 0.00 ");
			}
			
			$(function()	{
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/zwtp.png"
	   			});
	   			
				//初始化按钮的状态
	   			$('img').imgAlter();
				
	   			bindCheckbox('zx');
	   			bindCheckbox('bk');
				
	   			//键盘按下事件
	   			$(document).keydown(function(event) {
					if (event.keyCode == 13) {
						$('form').each(function() {
							//搜索的代码
							if($("#keyword").val() == null || $("#keyword").val() == ""){
								window.external.SearchFunc("请输入软件名称进行搜索！");
							}else{
								document.searchForm.submit(); 
							}
						});
					}
				}); 
	   			
				$("#shuju").html("<center><br/>请稍后正在加载内容……<br/><br/><center>");
   				$.post("${pageContext.request.contextPath}/woplusJ/findAppByType.action",
	    		{empId:"${empId}",isIOS:"${isIOS}",installType:"${installType}",appOsType:"${appOsType}",version:"${version}",pageSize:"${pageSize}",pageSize2:"${pageSize2}",typeId:"${typeId }"},
	    		function(data){
	    			$("#shuju").html(data);
	    			$("img[id^='appimg_']").lazyload({
						failure_limit : 100,
						failImg			: "img_woshop/zwtp.png"
	   				});
	    			
	    			// 软件宝库 全选 
					$('#bk_checkAll').click(function()	{
						$("input[id^='bkCheck_']").attr('checked', this.checked);
					});
	   				
					resetCheckbox('bk');
					bindCheckbox('bk');
	   				
		   			//鼠标事件
	   				$(".app").mouseover(function () {
						var b = $(this).find(".popUp").html();
						if (b) {
							$("#tanchulei").html(b);
							downDisplay(this);
						}
						$(this).find(".downtimes").hide().end().find(".down").show();
					}).mouseout(function () {
						$("#tanchulei").hide();
						$(this).find(".downtimes").show().end().find(".down").hide();
					});
	    		});
   				
   				//鼠标事件
   				$(".app").mouseover(function () {
					var b = $(this).find(".popUp").html();
					if (b) {
						$("#tanchulei").html(b);
						downDisplay(this);
					}
					$(this).find(".downtimes").hide().end().find(".down").show();
				}).mouseout(function () {
					$("#tanchulei").hide();
					$(this).find(".downtimes").show().end().find(".down").hide();
				});
   			});
   			
   			
   			//根据软件类别查询宝库软件集合
			function selectTag(typeId, index, selfObj){
				var tag = document.getElementById("tags").getElementsByTagName("li");
				for(i=0; i<tag.length; i++){
					tag[i].className = "";
				}
				if(typeId == 101){
	    			$("#softLi").addClass("selectTag");
				}else{
					$("#gameLi").addClass("selectTag");
				}
				$("#shuju").html("<center><br/>请稍后正在加载内容……<br/><br/><center>");
				$.post("${pageContext.request.contextPath}/woplusJ/findAppByType.action",
	    		{empId:"${empId}",isIOS:"${isIOS}",installType:"${installType}",appOsType:"${appOsType}",version:"${version}",pageSize:"${pageSize}",pageSize2:"${pageSize2}",typeId:typeId},
	    		function(data){
						$("#shuju").html(data);
		    			$("img[id^='appimg_']").lazyload({
							failure_limit : 100,
							failImg			: "img_woshop/zwtp.png"
		   				});
		   				
		    			// 软件宝库 全选 
						$('#bk_checkAll').click(function()	{
							$("input[id^='bkCheck_']").attr('checked', this.checked);
						});
		   				
						resetCheckbox('bk');
						bindCheckbox('bk');
		   				
		   				//鼠标事件--弹出框
				   		$(".app").mouseover(function () {
							var b = $(this).find(".popUp").html();
							if (b) {
								$("#tanchulei").html(b);
								downDisplay(this);
							}
							$(this).find(".downtimes").hide().end().find(".down").show();
						}
						).mouseout(function () {
							$("#tanchulei").hide();
							$(this).find(".downtimes").show().end().find(".down").hide();
						});
		    		},
		    	"html"
		    	);
			}
			//鼠标事件--弹出框
			function downDisplay(k) {
				var i = $(k).parents().offset().left;
				var n = $(k).offset().left + 75;
				var j = $(k).offset().top + 5;
				var m = n - i;
				var h = $(k).parents().width() - m;
				var l = $("#tanchulei").width();
				if (l > h) {
					var n = $(k).offset().left - 180;
					var j = $(k).offset().top + 5;
					//$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				} else {
					$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				}
			}
			
			
			//最新上架 勾选安装处理
			function zx_multiApp()	{
				var selectFlags = document.getElementsByName("zx_selectFlag");
				var num = 0;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							//alert(arr[0]+"  ,  "+arr[1]);
							var url = "${pageContext.request.contextPath}/woplusJ/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=9&ids=" + ids ;
							if(num == 0){
								num = 1;
								myweb.MyOwnFunc(1,arr[0],arr[1], url);
							}else{
								myweb.MyOwnFunc(0,arr[0],arr[1], url);
							}
						}
					}
				}
			}
			
			
			//应用宝库  勾选安装处理
			function bk_multiApp()	{
				var selectFlags = document.getElementsByName("bk_selectFlag");
				var num = 0;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							//alert(arr[0]+"  ,  "+arr[1]);
							var url = "${pageContext.request.contextPath}/woplusJ/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=10&ids=" + ids ;
							if(num == 0){
								num = 1;
								myweb.MyOwnFunc(1,arr[0],arr[1], url);
							}else{
								myweb.MyOwnFunc(0,arr[0],arr[1], url);
							}
						}
					}
				}
			}
			
			//单个的下载安装
			function downApp(appName, rscTypeId, ids, imoduletype){
				var url = "${pageContext.request.contextPath}/woplusJ/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&ids=" + ids + "&imoduletype=" + imoduletype;
				myweb.MyOwnFunc(1,appName, rscTypeId, url);
			}
			
			function searchApp(){
				if($("#keyword").val() == null || $("#keyword").val() == ""){
					window.external.SearchFunc("请输入软件名称进行搜索！");
				}else{
					document.searchForm.submit(); 
				}
			}
			
			/**
			* 按钮状态变化
			*/
			function clientInvoke(u_sStr){
				//尹工打算将多个uuid和状态值一次性传过来，我这边可能需要做个循环处理
				var u_sArr = u_sStr.split(",");
				for(i=0; i<u_sArr.length; i++){
					var u_s = u_sArr[i].split(":");
					var uuid = u_s[0];
					var status = u_s[1];
	   				$("img").imgAlterTrigger(uuid, status);
				}
   			}
			
		</script>
		
		<STYLE type=text/css>
			BODY {
				FONT-SIZE: 14px;
				FONT-FAMILY: "微软雅黑";
				background-color: #FFFFFF;
				background-repeat: no-repeat;
				background-image: url(img_woshop/bd.png);
			}
			UL {
				LIST-STYLE-TYPE: none
			}
			#con {
				FONT-SIZE: 14px;
				BORDER-RIGHT: #aecbd4 1px solid;
				BORDER-LEFT: #aecbd4 1px solid;
				margin-top: 0px;
				margin-right: 10px;
				margin-bottom: 0px;
				margin-left: 10px;
				background-image: url(img_woshop/qbj.png);
			}
			#tags {
				PADDING-RIGHT: 3px;
				PADDING-LEFT: 3px;
				PADDING-BOTTOM: 0px;
				MARGIN: 0px 0px 0px 8px;
				HEIGHT: 36px
			}
			#tags LI {
				BACKGROUND: url();
				FLOAT: left;
				MARGIN-RIGHT: 1%;
				HEIGHT: 30px;
				WIDTH: 80px
			}
			#tags LI.selectTag {
				BACKGROUND: url(img_woshop/c-select.png);
				MARGIN-BOTTOM: 3px;
				HEIGHT: 34px
			}
			#tagContent {
				BORDER-RIGHT: #aecbd4 1px solid;
				BORDER-LEFT: #aecbd4 1px solid;
				BORDER-BOTTOM: #aecbd4 1px solid;
				margin-left: 10px;
				margin-right: 10px;
			}
			a:link {
				color: #000000;
				text-decoration: none;
			}
			body,td,th {
				color: #333333;
			}
			a:visited {
				text-decoration: none;
				color: #000000;
			}
			a:hover {
				text-decoration: none;
				color: #FF0000;
			}
			a:active {
				text-decoration: none;
				color: #0000cc
			}
			.STYLE9 {
				font-size: 12px
			}
			.STYLE13 {
				font-family: "微软雅黑";
				color: #000000;
				font-size: 14px;
			}
			.STYLE14 {
				color: #FF0000;
				font-weight: bold;
			}
			.STYLE15 {
				color: #333333
			}
			p.hj {
				line-height: 35px
			}
			.STYLE18 {
				font-family: "微软雅黑";
				color: #000000;
				font-size: 14px;
				font-weight: bold;
			}
		</STYLE>
	</head>

	<body>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<form id="searchForm" name="searchForm" action="woplusJ/search.action" method="get">
				<tbody>
					<tr>
						<td width="72%" height="28" background="img_woshop/searchBar.png">
							<table width="100%" border="0">
								<tr>
									<td width="2%">
										&nbsp;
									</td>
									<td width="2%">
										<!-- <a href="javascript:history.go(-1);"> 
											<img src="img_woshop/houtui.png" width="32" height="28" /> 
										</a> -->
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(1);">
											<img src="img_woshop/qianjing.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="woplusJ/appIndex.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}">
											<img src="img_woshop/shuaxin.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="62%">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
						<td width="22%" align="right" valign="middle" background="img_woshop/searchBar.png" height="28">
							<div class="ss_1">
								<input type="hidden" name="imoduletype" value="10"/>
								
								<input type="hidden" name="empId" value="${empId}"/>
								<input type="hidden" name="isIOS" value="${isIOS}"/>
								<input type="hidden" name="installType" value="${installType}"/>
								<input type="hidden" name="appOsType" value="${appOsType}"/>
								<input type="hidden" name="version" value="${version}"/>
								<input type="hidden" name="pageSize" value="${pageSize}"/>
								<input type="hidden" name="pageSize2" value="${pageSize2}"/>
								
								<input name="keyword" class="ss_text" id="keyword"
									onclick="javascript:$('#keyword').val('');" maxlength="255"  value="${keyword }"/>
							</div>
						</td>
						<td width="4%" valign="middle" background="img_woshop/searchBar.png" height="28">
							<div class="ss_2">
								<a href="javascript:searchApp()"><img src="img_woshop/search-k.png"></a>
							</div>
						</td>
					</tr>
				</tbody>
			</form>
		</table>
		
		<table width="100%" border="0" align="center" cellspacing="18">
			<tr>
				<td width="70%" valign="top" background="img_woshop/q-line.png" style="background-repeat:no-repeat" >
					<span class="STYLE18">&nbsp;最新上架</span> &nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="zx_checkbox" id="zx_checkAll" />
					<span class="STYLE13">全选</span>
					<span class="STYLE13"> 
						&nbsp;&nbsp;&nbsp;已选择
						<span id="zx_checkCount" class="STYLE14">0&nbsp;</span>款， 共
						<span id="zx_totalSize" class="STYLE14">0.00&nbsp;</span>MB 
					</span>
				</td>
				<td width="8%">
					<div align="left">
						<a href="javascript:void(0);" onclick="javascript:zx_multiApp();" target="_self">
							<img src="img_woshop/yjanz.png" width="89" height="32" /> 
						</a>
					</div>
				</td>
				<td width="12%"></td>
			</tr>
		</table>
		<div class="softindex" align="center">
			<ul id="bodyul">
				<c:if test="${empty appList}"> <center> 对不起, 没有找到相关的软件! </center> </c:if>
				<c:forEach items="${appList}" var="app">
					<li class="app">
						<div class="tpc" id="div6">
							<span class="gxk">
								<input type="checkbox" id="zxCheck_${app.uuid}" name="zx_selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}"/>
								<input type="hidden" id="zxFileSize_${app.uuid}_${app.packUuid }_${app.discriminator}"  value="${app.packFileSize}"/>
	               				<input type="hidden" id="app.discriminator" value="${app.discriminator}"/>
	               				<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
							</span>
							<a href="woplusJ/appInfo.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=9">
								<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
								<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
									<span class="tj"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
									<span class="rm"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
									<span class="jl"></span>
								</c:if>
								<c:if test="${app.markerType == null || '' eq app.markerType }">
									<span class=""></span>
								</c:if>
								<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" width="50" height="50" align="baseline"  /> 
							</a>
							<span class="downtimes STYLE9 STYLE15" style="display: block;">
								${fnx:abbreviate(app.name,12, "...")}
							</span>
							<br />
							
							<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
			                <a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}',9);"> 
	                			<c:if test="${installType == 1 }"><!-- pc端直连 -->
		                			<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
		                		</c:if>
		                		<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
		                			<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
		                		</c:if>
		                		<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
		                			<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
		                		</c:if>
		                	</a>
							<div class="popUp">
								<font> ${fnx:abbreviate(app.name,20, "...")} </font>
								<span>
									${app.packAppVer } &nbsp;&nbsp;&nbsp; ${app.downCount }次<br /> 
			                    	<span class="floatleft"> ${fnx:abbreviate(app.packFileSize/1024.0 ,4,"")} MB</span> &nbsp;&nbsp;&nbsp; 
			                    	<c:if test="${app.cpUpdateTime == null }"></c:if>
			                    	<c:if test="${app.cpUpdateTime != null }">${app.cpUpdateTime }</c:if>
			                    	简介：${fnx:abbreviate(app.info,90, "...")}
								</span>
							</div>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>
		
		<p> &nbsp; </p>
		
		<div id="con">
			<ul id="tags">
				<li class="selectTag" id="softLi">
					<A onClick="selectTag(101,0,this)" href="javascript:void(0)">
						<div align="center">
							<p class="hj"> <strong>应用宝库</strong> </p>
						</div>
					</a>
				</li>
				<li id="gameLi">
					<A onClick="selectTag(102,1,this)" href="javascript:void(0)">
						<div align="center">
							<p class="hj"> <strong>游戏宝库</strong> </p>
						</div>
					</a>
				</li>
				<li id="phLi">
					<A href="woplusJ/rank.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}">
						<div align="center">
							<p class="hj"> <strong>排 行 榜</strong></p>
						</div>
					</a>
			</ul>
		</div>
		
		<div id=tagContent>
			<div class="tagContent selectTag" id=tagContent0>
				<div class="softindex-1" id="shuju">
					
				</div>
			</div>
			
			<div class="tanchubj popUp" id="tanchulei"
				style="position: absolute; left: 620.5px; top: 745px; display: none;">
			</div>
		</div>
	</body>
</html>