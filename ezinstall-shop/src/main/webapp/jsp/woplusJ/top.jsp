<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
	<head>
		<title>软件首页--二级顶部</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css_woshop/hot0406.css">
		<LINK rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<script src="js/jquery-1.7.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="js/js_woshop/softlist.js"></script>
		<script type="text/javascript">
			$(function()	{
				//键盘按下事件
	   			$(document).keydown(function(event) {
					if (event.keyCode == 13) {
						$('form').each(function() {
							//搜索的代码
							if($("#keyword").val() == null || $("#keyword").val() == ""){
								window.external.SearchFunc("请输入软件名称进行搜索！");
							}else{
								document.searchForm.submit(); 
							}
						});
					}
				});
			});
			
			function searchApp(){
				document.searchForm.submit(); 
			}
		</script>
		
		<STYLE type=text/css>
			.STYLE11 {
				font-size: 14px;
				font-family: "微软雅黑";
				color: #333333;
			}
		</STYLE>
	</head>

	<body>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<form id="searchForm" name="searchForm" action="woplusJ/search.action" method="get" target="_top">
				<tbody>
					<tr>
						<td width="72%" height="28" background="img_woshop/searchBar.png">
							<table width="100%" border="0">
								<tr>
									<td width="2%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(-1);"> 
											<img src="img_woshop/houtui.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(1);">
											<img src="img_woshop/qianjing.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a target="left" href="${pageContext.request.contextPath}/woplusJ/left.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=10">
											<img src="img_woshop/shuaxin.png" width="32" height="28" /> 
										</a>
									</td>
									<td width="62%">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
						<td width="22%" align="right" valign="middle" background="img_woshop/searchBar.png">
							<div class="ss_1"> 
								<input type="hidden" name="imoduletype" value="10"/>
								
								<input type="hidden" name="empId" value="${empId}"/>
								<input type="hidden" name="isIOS" value="${isIOS}"/>
								<input type="hidden" name="installType" value="${installType}"/>
								<input type="hidden" name="appOsType" value="${appOsType}"/>
								<input type="hidden" name="version" value="${version}"/>
								<input type="hidden" name="pageSize" value="${pageSize}"/>
								<input type="hidden" name="pageSize2" value="${pageSize2}"/>
								
								<input name="keyword" class="ss_text" id="keyword"
									onclick="javascript:$('#keyword').val('');" maxlength="255" />
							</div>
						</td>
						<td width="4%" valign="middle" background="img_woshop/searchBar.png">
							<div class="ss_2">
								<a href="javascript:searchApp()" ><img src="img_woshop/search-k.png"></a>
							</div>
						</td>
					</tr>
				</tbody>
			</form>
		</table>

		<table width="100%" border="0">
			<tr>
				<td height="30" background="img_woshop/bd.png">
					<table width="100%" border="0">
						<tr>
							<td width="3%"> &nbsp; </td>
							<td width="10%" height="30" valign="top" background="img_woshop/select-ht.png">
								<div align="center" class="STYLE11" height="30">
									<strong>应用类别</strong>
								</div>
							</td>
							<td width="87%"> &nbsp; </td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>