<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>灵动知道</title>
		<link href="${pageContext.request.contextPath }/css/css.css"
			rel="stylesheet" type="text/css" />
		<link href="${pageContext.request.contextPath }/css/soft.css"
			rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${pageContext.request.contextPath}/css/start/jquery-ui-1.8.16.custom.css" />
		<%@ include file="/jsp/common2.jsp"%>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript">
	$(document).ready(function() {
		$('#tabs').tabs();
		$('#askDiv').hide();
		$('#myQuestion').hide();
		$('#askBtn').button();
		$('#myQuestionTabs').tabs();
	});

	function searchAnswer() {
		var keyword = $('#question').val();
		if ($.trim(keyword) == '')
			return;
		else
			window.location.href = '${pageContext.request.contextPath}/faq/search.action?keyword='
					+ keyword;
	}

	function showMyQuestion() {
		$('#myQuestion').dialog({
			modal : true,
			maxHeight : 700,
			minHeight : 500,
			width : 800,
			title : '我提问过的问题',
			buttons : {
				"关闭" : function() {
					$(this).dialog("close");
				}
			}
		});
	}

	function showAsk() {
		$('#askDiv')
				.dialog(
						{
							modal : true,
							resizable : false,
							height : 500,
							width : 500,
							title : '提问',
							buttons : {
								"我来提问" : function() {
									if ($.trim($('#questiont').val()) == '') {
										$('#commentHint').html('请输入问题');
									} else
										$
												.ajax({
													url : "${pageContext.request.contextPath}/faq/add.action",
													data : "questiont="
															+ $('#questiont')
																	.val(),
													type : "post",
													dataType : "json",
													success : function(data) {
														switch (data.errorCode) {
														case 1:
															$('#commentHint')
																	.html(
																			'提交成功！我们会在一周内给予答复，请耐心等候。');
															$('#questiont')
																	.val('');
															window.location
																	.reload(true);
															break;
														case 2:
															$('#commentHint')
																	.html(
																			'您提问的次数过多, 请改日再试。');
														}
													}
												});
								},
								"取消" : function() {
									$('#commentHint').html('');
									$('#questiont').val('');
									$(this).dialog("destroy");
								}
							}
						});
	}
</script>

	</head>
	<body>
		<table width="100%" border="0" align="center" cellpadding="0"
			cellspacing="0"
			background="${pageContext.request.contextPath}/img/xk.png">
			<tr>
				<td width="2%" height="37"></td>
				<td width="5%">
					<!-- <a href="javascript:history.go(-1)">
		        		<img alt="返回" src="${pageContext.request.contextPath}/img/an3.png" width="30" height="24" border="0"/>
		        	</a> -->
				</td>
				<td width="55%"></td>
				<td width="28%" align="right"></td>
				<td width="2%"></td>
			</tr>
		</table>
		
		<center>
		<div style="margin-top: 20px;">
			<table width="80%" border="1" align="center" cellpadding="0"
				cellspacing="0" bordercolor="#c8c8c8">
				<tr>
					<td>
						<table border="0" cellSpacing="0" cellPadding="0" width="100%"
							align="center" bgcolor="#ffffff">
							<tr>
								<td height="32" width="1%"
									background="${pageContext.request.contextPath}/img/tj2.png"></td>
								<td class="STYLE5 STYLE9" width="98%"
									background="${pageContext.request.contextPath}/img/tj2.png">
									用户常见问题
								</td>
								<td width="1%"
									background="${pageContext.request.contextPath}/img/tj2.png"></td>
							</tr>
						</table>

						<div id="tiwen">
							<div id="shuru">
								<input type="text" id="question" class="text" />
							</div>
							<div id="wen">
								<input type="button" style="margin-top: 12px;" class="faq_btn1"
									value="搜索答案" onclick="searchAnswer()" />
							</div>
							<c:if test="${emp != null}">
								<div id="chaxun">
									<a href="javascript:void(0);" onClick="showMyQuestion()">我的问题...</a>
								</div>
							</c:if>
						</div>
						<div id="div">
							<div id="body">
								<div id="wenti">
									<div id="tabs">
										<ul>
											<c:forEach items="${faqMap}" var="map" varStatus="stat">
												<li>
													<a href="#faq_${stat.index}">${map.key.typeName}</a>
												</li>
											</c:forEach>
										</ul>
										<c:forEach items="${faqMap}" var="map" varStatus="status">
											<div id="faq_${status.index}">
												<c:if test="${empty map.value}">
													<font size="3">对不起, 没有找到相关分类下的问题</font>
												</c:if>
												<c:forEach items="${map.value}" var="faq"
													varStatus="varStatus">
													<ul>
														<li>
															<div class="ss_nei">
																<div class="ss_nei_wen">
																	<div class="ss_nei_wen_left">
																		${faq.question}
																	</div>
																	<div class="ss_nei_wen_right">
																		<!-- <input type="button"
																			onclick="location='${pageContext.request.contextPath}/blank/sendFaqSms.action?id=${faq.id }'"
																			class="faq_btn2" value="发送问题及答案到手机" /> -->
																	</div>
																</div>
																<div class="ss_nei_ti">
																	${faq.answer }
																</div>
															</div>
														</li>
													</ul>
												</c:forEach>
											</div>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>

						<div class="weizhaodao">
							<!-- <input id="askBtn" type="button" onClick="showAsk()"
								value="没有找到想要的答案??" /> -->
						</div>
					</td>
				</tr>
			</table>
		</div>
		</center>

		<!-- 隐藏的对话框"没有找到想要的答案"  -->
		<div id="askDiv">
			<div class="center">
				<textarea id="questiont" name="questiont" cols="80" rows="10"></textarea>
			</div>
			<div>
				<font color="red"><span id="commentHint"></span>
				</font>
			</div>
		</div>

		<!-- 隐藏的"我提问过的问题" -->
		<div id="myQuestion">
			<div id="myQuestionTabs">
				<c:if test="${empty faqsList}">您还没有提问过……</c:if>
				<ul>
					<li>
						<a href="#myFaq_1">已审核</a>
					</li>
					<li>
						<a href="#myFaq_2">未审核</a>
					</li>
				</ul>
				<div id="myFaq_1">
					<c:forEach items="${faqsList}" var="faq">
						<c:if test="${faq.checked == 1}">
							<ul>
								<li>
									<div class="ss_nei">
										<div class="ss_nei_wen">
											<div class="ss_nei_wen_left">
												${faq.question}
											</div>
										</div>
										<div class="ss_nei_ti">
											${faq.answer}
										</div>
									</div>
								</li>
							</ul>
						</c:if>
					</c:forEach>
				</div>
				<div id="myFaq_2">
					<c:forEach items="${faqsList}" var="faq">
						<c:if test="${faq.checked == 0 || faq.checked == null}">
							<ul>
								<li>
									<div class="ss_nei">
										<div class="ss_nei_wen">
											<div class="ss_nei_wen_left">
												${faq.question}
											</div>
										</div>
										<div class="ss_nei_ti">
											${faq.answer}
										</div>
									</div>
								</li>
							</ul>
						</c:if>
					</c:forEach>
				</div>
			</div>
		</div>
	</body>
</html>
