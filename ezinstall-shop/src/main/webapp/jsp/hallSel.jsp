<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
  	<%@ include file="/jsp/common2.jsp" %>
  	<script type="text/javascript">
  		$(function()	{
  			$('#citySel').change(function()	{
  				var cid = $(this).val();
  				if(cid == '')	return;

  				$('span').html('');
  				$('#countyLoading').html("<img src='${pageContext.request.contextPath}/img/loading3.gif'/>");
  				$('#hallSel').html("<option value=''>-请选择营业厅-</option>");
  				
  				$.get("${pageContext.request.contextPath}/extra/countyList.action", {id : cid}, function(data){
	  				$('span').html("");
  					$('#countySel').html(data);
  					$('#countySel').change(function()	{
		  				
  						var coid = $(this).val();
  						if(coid == '')	return;

  						$('#hallLoading').html("<img src='${pageContext.request.contextPath}/img/loading3.gif'/>");
  						$('#hint').html('');
  						
  						$.get("${pageContext.request.contextPath}/extra/hallList.action", {id : coid}, function(data){
  							$('#hallLoading').html('');
  							$('#hallSel').html(data);
  						});
  					});
  				});
  			});
  		});
  		
  		function confirm()	{
  			var hallId = $('#hallSel').val();
  			if(hallId == '' || hallId == null)
  				$('#hint').html("请选择营业厅");
  			else
  				window.location.href = "${pageContext.request.contextPath}/extra/hallInfo.action?id=" + hallId;
  		}
  	
  	</script>
  </head>
  <body><br><br><br><br>
	  <div align="center">
		<select id="citySel" style="width: 150;" size="20">
			<option value="">-请选择所在城市-</option>
			<c:forEach items="${citySet}" var="city">
			<option value="${city.id}">${city.name}</option>		
			</c:forEach>
		</select><span style="width: 80" id="countyLoading"></span>
		
		<select id="countySel" style="width: 150" size="20">
			<option value="">-请选择区县-</option>
		</select><span style="width: 80" id="hallLoading"></span>
		
		<select id="hallSel" style="width: 150" size="20">
			<option value="">-请选择营业厅-</option>
		</select><br><br>
		<table>
			<tr>
			<td width="100"><input style="width:100" type="button" value="确&nbsp;定" onclick="confirm()"/></td>
			<td width="100"><span id="hint" style="color: red"></span></td>
			</tr>
		</table>
	  </div>
  </body>
</html>
