<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	  	<base href="<%=basePath%>"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="css/1.css" rel="stylesheet" type="text/css" />
		<%@ include file="/jsp/common2.jsp" %>
		<script type="text/javascript" src="js/util.js"></script>
		<script type="text/javascript">
			$(function()	{
				utilInit();
			});
		</script>
		
		<script type="text/javascript">
			function searchPhone()	{
				var s = document.getElementById("keyword").value;
				if($.trim(s) == '' || s == '请输入厂商名或手机名')	{
					$('#keyword').val('请输入厂商名或手机名');
				} else {
//					with(document.getElementById('searchForm'))	{
//						action='/gameshop/soft/search.action';
//						submit();
//					}
					window.location.href = "${pageContext.request.contextPath}/extra/phoneList.action?keyword=" + s;
				}
			}
		</script>
		
	</head>

	<body>
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="img/xk.png">
      <tr>
        <td width="2%" height="37"></td>
        <!-- 
        	<td width="5%" >
	        <a href="javascript:history.go(-1)">
	        <img src="img/an3.png" width="30" height="24" border="0"/></a></td>
         -->
        <td width="55%" ></td>
        <td width="28%"  align="right"></td>
        <td  width="215" align="right" valign="middle">
			<div class="ss_1">
			<input class="ss_text" type="text" id="keyword" name="keyword" value="${keyword}" onclick="javascript:$('#keyword').val('');" />
			</div>
		</td>
		<td width="26" valign="middle">
			<div class="ss_2">
			<img src="img/ss_02.png"  onclick="searchPhone()"/>
			</div>
		</td>
        <td width="2%" ></td>
      </tr>
    </table>
	<div class="div">
		<div class="biaoti">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td><div class="biaotiti">演示终端库</div></td>
	          </tr>
	        </table>
	    </div>
	    <div class="div_nei">
	    <pg:pager items="${typeInfoPager.totalCount}" maxPageItems="${pageSize}" maxIndexPages="10" 
	    		 isOffset="true" export="offset,currentPageNumber=pageNumber" scope="request" 
	    		 url="extra/phoneList.action"> 	   
	    	<pg:param name="keyword" value="${param.keyword}" />
	    		  
	    	<c:forEach items="${typeInfoPager.list}" var="typeInfo">
		        <div class="kid">
		        	<div class="kid_biao">
	            		<c:set value="${typeInfo.manuInfo.manuName}-${typeInfo.typeName}" var="content"></c:set>
			        	${fnx:abbreviate(content, 15, "...")}
			        </div>
		            <div class="kid_img">
		            	<a title="${typeInfo.typeName}" href="extra/phoneInfo.action?id=${typeInfo.id}">
		            	<img data-original="${fileserverUrl}${typeInfo.bigPic}" src="img/blank_phone.png"/></a>
		            </div>
		        </div>
	    	</c:forEach>
	        <div class="clear"></div>
	    </div>
	    <pg:index>
                    <center><font face="Helvetica">
                    <pg:first><a href="${pageUrl}"><nobr>[首页]</nobr></a></pg:first>
                    <pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a></pg:prev>
                    <pg:pages>
                        <c:choose>
                            <c:when test="${pageNumber eq currentPageNumber}"><b>&nbsp;&nbsp;${pageNumber}</b></c:when>
                            <c:otherwise><a href="${pageUrl}">&nbsp;&nbsp;[${pageNumber}]</a></c:otherwise>
                        </c:choose>
                    </pg:pages>
                    <pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a></pg:next>
                    <pg:last>&nbsp;<a href="${pageUrl}"><nobr>[末页]</nobr></a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
                    <br/>
                    </font></center>
                    <br/>
                </pg:index>
                </pg:pager>
	</div>
	</body>
</html>

