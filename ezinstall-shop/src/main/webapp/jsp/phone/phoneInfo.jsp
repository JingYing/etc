<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <base href="<%=basePath%>"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/css/1.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
    <tr>
		<td colspan="2">
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="${pageContext.request.contextPath}/img/xk.png">
      <tr >
        <td width="2%" height="37"></td>
        <td width="5%" >
        <a href="javascript:history.go(-1)">
        <img alt="返回" src="${pageContext.request.contextPath}/img/an3.png" width="30" height="24" border="0"/></a></td>
        <td width="55%" ></td>
        <td width="28%"  align="right"></td>
        <td width="2%" ></td>
      </tr>
    </table>
		</td>
		</tr>
		<tr>
		<td>
		<table border="0" cellspacing="0" cellpadding="0" align="center">
		
		  <tr>
		    <td width="366" valign="top">
		    	<div class="big_img">
	    		<c:choose>
	    			<c:when test="${fn:length(typeInfo.bigPic) gt 1}">
			    		<img src="${fileserverUrl}${typeInfo.bigPic}"  width="550" height="1024"/>
	    			</c:when>
	    			<c:otherwise>
			    		<img src="${pageContext.request.contextPath}/images/blank_phone.png" width="550" height="1024"/>
	    			</c:otherwise>
	    		</c:choose>
		    	</div>
		    	</td>
		    <td valign="top">
		    <div class="zi">
		        <div id="caozuo">	
		            <b class="b1"></b><b class="b2 d1"></b><b class="b3 d1"></b><b class="b4 d1"></b>
		                <div class="b d1 k">
		                	<div style="text-align:left;">
			                	<div class="biao1">终端信息：</div> 
			                    <div class="xinghao">终端型号：${typeInfo.manuInfo.manuName}-${typeInfo.typeName}</div>         
			                    <div class="xinghao">系统版本：${typeInfo.osInfo.osVersion}</div>   
			                    <div class="xinghao">分辨率：${typeInfo.resolution}
			                    	<c:if test="${fn:length(typeInfo.resolution) eq 0}">未知</c:if>
			                    </div>
			                    <div class="xinghao">网络模式：${typeInfo.networkModel}</div>
			                    <div class="biao3">合约计划：</div>
		                    </div>
		                    <div class="biao2">中国联通${typeInfo.manuInfo.manuName}-${typeInfo.typeName}购机合约套餐</div>
		                    <div class="big_img2">
		                    	<c:if test="${fn:length(typeInfo.contractPlanPic) gt 1}">
		                    		<img src="${fileserverUrl}${typeInfo.contractPlanPic}"/>
		                    	</c:if>
		                    </div>
		                </div>
		            <b class="b4b d1"></b><b class="b3b d1"></b><b class="b2b d1"></b><b class="b1b"></b>     
		        </div>
		    </div>
		    </td>
		  </tr>
		</table>
		</td>
		</tr>
		</table>
	</body>
</html>
