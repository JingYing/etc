<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>

<%-- 首页的软件列表  分2中内容，沃商店的使用阅联的快捷安装包 ；山西的使用原来通过业务获取的数据--%>
	<c:choose>
		<c:when test="${bizType == -1 && ( appOsType == 20 || appOsType == 0)}">
			<c:if test="${empty appInstallPackList}"><center style="font-size:14px;">对不起, 没有找到相关的软件。</center></c:if>
			<c:forEach items="${appInstallPackList}" var="packApp">
				<li class="app">
					<div class="tpc" id="tpc" style="z-index: 0;">
						<span class="gxk">
							<input type="checkbox" id="softCheck_${packApp.app.uuid}" name="selectFlag" value="${packApp.app.uuid}_${packApp.uuid }_2" />
							<input type="hidden" id="fileSize_${packApp.app.uuid}_${packApp.uuid }_2"  value="${packApp.fileSize}"/>
				         	<input type="hidden" id="app.discriminator" value="2"/>
				          	<input type="hidden" id="${packApp.app.uuid}_${packApp.uuid }_2" value="${packApp.app.name}_${packApp.app.rscType.id }"/>
						</span>
					  
						<a target="_top" href="woshopO/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${packApp.app.uuid}&packUuid=${packApp.uuid }&imoduletype=14" >
				       		<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
							<c:if test="${packApp.app.markerType != null && packApp.app.markerType == 'mark01'}">
								<span class="tj2"></span>
							</c:if>
							<c:if test="${packApp.app.markerType != null && packApp.app.markerType == 'mark02'}">
								<span class="rm2"></span>
							</c:if>
							<c:if test="${packApp.app.markerType != null && packApp.app.markerType == 'mark03'}">
								<span class="jl2"></span>
							</c:if>
							<c:if test="${packApp.app.markerType == null || '' eq packApp.app.markerType }">
								<span class=""></span>
							</c:if>
				       		<img src="img/border.gif" data-original="${fileserverUrl }${packApp.app.icon}" id="appimg_${packApp.uuid}" width="50" height="50" align="baseline" > 
				      	</a>
				 		<span class="downtimes STYLE9 STYLE15" style="display: block; ">
				 			${fnx:abbreviate(packApp.app.name,12, "...")}
				 		</span>
				       	<br />
				       
			          	<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
			          	<a href="javascript:downApp('${packApp.app.name }','${packApp.app.rscType.id }','${packApp.app.uuid}_${packApp.uuid }_2','${bizType }');"> 
			           		<c:if test="${installType == 1 }"><!-- pc端直连 -->
				      			<img src="img_woshop/InstallButton-a.png" imgAlterId1="${packApp.app.uuid }" imgAlterId2="${packApp.uuid }"> 
				          	</c:if>
				           	<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
				          		<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${packApp.app.uuid }" imgAlterId2="${packApp.uuid }"> 
				          	</c:if>
				           	<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
				               	<img src="img_woshop/InstallButton-a.png" imgAlterId1="${packApp.app.uuid }" imgAlterId2="${packApp.uuid }"> 
				          	</c:if>
			         	</a>
			          	<div class="popUp" >
			         		<font>${fnx:abbreviate(packApp.app.name,20, "...")}</font>
					        <span>
					       		${packApp.appVer } &nbsp;&nbsp;&nbsp; ${packApp.app.downCount }次<br /> 
					       		<span class="floatleft"> ${fnx:abbreviate(packApp.fileSize/1024.0 ,4,"")} MB</span> &nbsp;&nbsp;&nbsp; 
					          	<c:if test="${packApp.cpUpdateTime == null }"></c:if>
					            <c:if test="${packApp.cpUpdateTime != null }">${packApp.cpUpdateTime }</c:if>
					          	简介：${fnx:abbreviate(packApp.app.info,90, "...")}
					           	<br />
					        </span>
				     	</div>
					 </div>
			 	</li>
			</c:forEach>
		</c:when>
		
		<c:otherwise>
			<c:if test="${empty appPager.list}"><center style="font-size:14px;">对不起, 没有找到相关的软件。</center></c:if>
			<c:forEach items="${appPager.list}" var="app">
				<li class="app">
					<div class="tpc" id="tpc" style="z-index: 0;">
						<span class="gxk">
							<input type="checkbox" id="softCheck_${app.uuid}" name="selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}" />
							<input type="hidden" id="fileSize_${app.uuid}_${app.packUuid }_${app.discriminator}"  value="${app.packFileSize}"/>
				         	<input type="hidden" id="app.discriminator" value="${app.discriminator}"/>
				          	<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
						</span>
						<c:if test="${bizType == '9' }">
					       	<a target="_top" href="woshopO/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=11">
						</c:if>
						<c:if test="${bizType == '8' }">
					       	<a target="_top" href="woshopO/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=12">
						</c:if>
						<c:if test="${bizType == '1' }">
					       	<a target="_top" href="woshopO/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=13">
						</c:if>
				       		<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
							<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
								<span class="tj2"></span>
							</c:if>
							<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
								<span class="rm2"></span>
							</c:if>
							<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
								<span class="jl2"></span>
							</c:if>
							<c:if test="${app.markerType == null || '' eq app.markerType }">
								<span class=""></span>
							</c:if>
				       		<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" id="appimg_${app.uuid}" width="50" height="50" align="baseline" > 
				      	</a>
				 		<span class="downtimes STYLE9 STYLE15" style="display: block; ">
				 			${fnx:abbreviate(app.name,12, "...")}
				 		</span>
				       	<br />
				       
			          	<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
			          	<a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}','${bizType }');"> 
			           		<c:if test="${installType == 1 }"><!-- pc端直连 -->
				      			<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }" > 
				          	</c:if>
				           	<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
				          		<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }" > 
				          	</c:if>
				           	<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
				               	<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }" > 
				          	</c:if>
			         	</a>
			          	<div class="popUp" >
			         		<font>${fnx:abbreviate(app.name,20, "...")}</font>
					        <span>
					       		${app.packAppVer } &nbsp;&nbsp;&nbsp; ${app.downCount }次<br /> 
					       		<span class="floatleft"> ${fnx:abbreviate(app.packFileSize/1024.0 ,4,"")} MB</span> &nbsp;&nbsp;&nbsp; 
					          	<c:if test="${app.cpUpdateTime == null }"></c:if>
					            <c:if test="${app.cpUpdateTime != null }">${app.cpUpdateTime }</c:if>
					          	简介：${fnx:abbreviate(app.info,90, "...")}
					           	<br />
					        </span>
				     	</div>
					 </div>
			 	</li>
			</c:forEach>
		</c:otherwise>
	</c:choose>
