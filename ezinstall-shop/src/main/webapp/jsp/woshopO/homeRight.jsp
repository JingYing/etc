<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>首页--右框架</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css_woshop/hot0406.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/global2.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/style.120517.css">
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<%@ include file="/jsp/common2.jsp" %>
		<script type=text/javascript>
			$(function()	{
				
				$("a, input:button").focus(function()	{
					$(this).blur();
				});
				document.onselectstart = function()	{
					return false;
				}
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/pkg_def.png"
	   			});
	   			
	   			
	   			//初始化按钮的状态
	   			$('img').imgAlter();
	   			
	   			if(${empty pkgList && fn:length(pkgList_City) > 0 }){
	   				$("#city_a_0").click();
	   			}else{
		   			$("#a_0").click();
	   			}
	   			
	   			//键盘按下事件
	   			$(document).keydown(function(event) {
					if (event.keyCode == 13) {
						$('form').each(function() {
							//搜索的代码
							if($("#keyword").val() == null || $("#keyword").val() == ""){
								window.parent.myweb.SearchFunc("请输入软件名称进行搜索！");
							}else{
								document.searchForm.submit(); 
							}
						});
					}
				}); 
	   			
				$("#checkAll").change(function()	{
					if($(this).attr('checked'))	{
						$(':checkbox').attr('checked', true);
					} else	{
						$(':checkbox').removeAttr('checked');
					}
					calcChecked();
				});
				
				bindCheckboxAndReset();
	   			
   				//鼠标事件--弹出框
   				$(".app").mouseover(function () {
					var b = $(this).find(".popUp").html();
					if (b) {
						$("#tanchulei").html(b);
						downDisplay(this);
					}
					$(this).find(".downtimes").hide().end().find(".down").show();
				}).mouseout(function () {
					$("#tanchulei").hide();
					$(this).find(".downtimes").show().end().find(".down").hide();
				});
   			});
   			
			
			//计算勾选个数和大小
			function calcChecked()	{
				var checkedCount = 0;
				var totalSize = 0.0;
				$("input[id^='softCheck_']").each(function()	{
					if(this.checked == true) {
						checkedCount ++;
						var fileSize = $("#fileSize_" + this.value + "").val();
						if(fileSize == null || "" == fileSize){
							fileSize = "0.0";
						}
						totalSize += parseFloat(fileSize);
					}
				});
				$('#checkCount').html(checkedCount);
				$('#checkSize').html((totalSize/1024.0).toFixed(2));
			}
			
			//绑定checkboxchange,并重置全选框
			function bindCheckboxAndReset()	{
				$('#checkAll').removeAttr('checked');
				$('#checkCount').html(0);
				$('#checkSize').html(0);
				$(":checkbox[id!='checkAll']").change(function()	{
					calcChecked();
				});
			}
   			
			function selectTag(pkgId,pkgame,bizType,liIndex, selfObj){
				$.get("${pageContext.request.contextPath}/woshopO/findAppByPkg.action",
	    		{empId:"${empId}",appOsType:"${appOsType}",version:"${version}",isIOS:"${isIOS}",installType:"${installType}",pageSize:"${pageSize}",pkgId:pkgId,bizType:bizType,_:new Date().getTime()},
	    		function(data){
		    			var tag = document.getElementById("tags").getElementsByTagName("li");
						for(i=0; i<tag.length; i++){
							tag[i].className = "";
						}
	    				$("#"+liIndex+"").addClass("selectTag");
	    				$("#pkgName").html(" &nbsp;&nbsp;&nbsp;&nbsp;"+pkgame+"&nbsp;&nbsp;&nbsp;&nbsp; ");
	    				//每次换包都要把这个业务类别换了 和 包的id换了。
	    				$("#bizType").val(bizType);
	    				$("#pkgId").val(pkgId);
		    			$("#shuju").html(data);
		    			
		    			$("img[id^='appimg_']").lazyload({
							failure_limit : 100,
							failImg			: "img_woshop/zwtp.png"
		   				});
		    			
		    			bindCheckboxAndReset();
		    			
		    			//#################################### 默认全选
		    			//$(':checkbox').attr('checked', true);
		    			//calcChecked();
		    			//####################################
		    			
		   				//鼠标事件
				   		$(".app").mouseover(function () {
							var b = $(this).find(".popUp").html();
							if (b) {
								$("#tanchulei").html(b);
								downDisplay(this);
							}
							$(this).find(".downtimes").hide().end().find(".down").show();
						}
						).mouseout(function () {
							$("#tanchulei").hide();
							$(this).find(".downtimes").show().end().find(".down").hide();
						});
		    		},
		    	"html"
		    	);
			}
			
			function selectTag_Type(pkgId, pkgName, td_index, selfObj){
				$("td[id^='type_']").css("background","");
				$("#"+td_index+"").css("background","url(img_woshop/type_bj.png)");
				var url = "${pageContext.request.contextPath}/woshopO/classifyList.action?empId=${empId}&appOsType=${appOsType}&version=${version}&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&pkgId="+pkgId;
				window.parent.myweb.MyOwnFunc('classify', url, pkgName, '');
			}
			
			//鼠标事件--弹出框
			function downDisplay(k) {
				var i = $(k).parents().offset().left;
				var n = $(k).offset().left + 75;
				var j = $(k).offset().top + 5;
				var m = n - i;
				var h = $(k).parents().width() - m + 40;
				var l = $("#tanchulei").width();
				if (l > h) {
					var n = $(k).offset().left - 180;
					var j = $(k).offset().top + 5;
					//$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				}else{
					$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				}
			}
			
			//勾选安装
			function multiApp()	{
				var pkgId = $("#pkgId").val();
				var selectFlags = document.getElementsByName("selectFlag");
				var num = 1;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							
							var bizType = $("#bizType").val();
							var imoduletype;
							if(bizType == '9'){
								imoduletype = 11;//全国包
							}else if(bizType == '8'){
								imoduletype = 12;//地市包
							}else if(bizType == '1'){
								imoduletype = 13;//手机必备包
							}else if(bizType == '-1'){
								imoduletype = 14;//阅联的快捷安装包
							}
							
							var url = "${pageContext.request.contextPath}/woshopO/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&pkgId="+pkgId+"&imoduletype="+imoduletype+"&ids=" + ids ;
							if(num == 1){
								num = 0;
								window.parent.myweb.MyOwnFunc(1,arr[0],arr[1], url);
							}else{
								window.parent.myweb.MyOwnFunc(0,arr[0],arr[1], url);
							}
						}
					}
				}
			}
			
			//单个下载安装
			function downApp(appName, rscTypeId, ids, bizType){
				var pkgId = $("#pkgId").val();
				var imoduletype;
				if(bizType == '9'){
					imoduletype = 11;//全国包
				}else if(bizType == '8'){
					imoduletype = 12;//地市包
				}else if(bizType == '1'){
					imoduletype = 13;//手机必备包
				}else if(bizType == '-1'){
					imoduletype = 14;//阅联的快捷安装包
				}
				
				var url = "${pageContext.request.contextPath}/woshopO/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&pkgId="+pkgId+"&imoduletype="+imoduletype+"&ids=" + ids;
				window.parent.myweb.MyOwnFunc(1, appName, rscTypeId, url);
			}
			
			function searchApp(){
				if($("#keyword").val() == null || $("#keyword").val() == ""){
					window.parent.myweb.SearchFunc("请输入软件名称进行搜索！");
				}else{
					document.searchForm.submit(); 
				}
			}
			
			/**
			* 按钮状态变化
			*/
			function clientInvoke(u_sStr){
				//尹工打算将多个uuid和状态值一次性传过来，我这边可能需要做个循环处理
				var u_sArr = u_sStr.split(",");
				for(i=0; i<u_sArr.length; i++){
					var u_s = u_sArr[i].split(":");
					var uuid = u_s[0];
					var status = u_s[1];
	   				$("img").imgAlterTrigger(uuid, status);
				}
   			}
			
		</script>

<STYLE type=text/css>
BODY {
	FONT-SIZE: 14px;
	FONT-FAMILY: "微软雅黑";
	background-color: #FFFFFF;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
#con {
	FONT-SIZE: 20px;
	background-image: url(img_woshop/qbj.png);
	background-repeat: repeat-x;
	height: 35px;
}
#tags {PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px 0px 0px 20px; HEIGHT: 95px; }

#tags LI {
	FLOAT: left; MARGIN-RIGHT: 1%; HEIGHT: 95px; WIDTH: 82px; width: 13%; }
#tags LI.selectTag {
	BACKGROUND: url(img_woshop/pkg-bj.png); background-repeat: no-repeat; HEIGHT: 95px; background-position: center center;}

#tagContent {
	BORDER-RIGHT: #aecbd4 0px solid;
	BORDER-LEFT: #aecbd4 0px solid;
	BORDER-BOTTOM: #aecbd4 0px solid;	
	margin-left: 0px;
	margin-right: 0px;	
}
p.hj { margin-top: 7px;line-height: 28px; }
a:link { color: #666666; text-decoration: none; }
body,td,th { color: #333333; z-index: auto; }
a:visited {
				text-decoration: none;
				color: #000000;
			}
			a:hover {
				text-decoration: none;
				color: #FF0000;
			}
			a:active {
				text-decoration: none;
				color: #0000cc
			}

#warp {
	height:40px;
	line-height:30px;
	margin-top:0px;
	margin-bottom:0px;
	width:150px} 
#warp-1 {
	height:40px;
	line-height:30px;
	margin-top:-40px;
	margin-bottom:0px;
	width:auto;
	margin-right:100px;
	margin-left: 120px;
} 
.STYLE1 { font-family: "微软雅黑"; color: #000000; font-size: 12px; }
.STYLE2 { font-family: "微软雅黑"; color: #FF0000; font-size: 14px; }
.STYLE3 { font-family: "微软雅黑"; color: #FF0000; font-size: 16px; }
.STYLE9 { font-size: 12px; }
.STYLE13 { font-family: "微软雅黑"; color: #000000; font-size: 14px; }
.STYLE14 { color: #FF0000; font-weight: bold; }
.STYLE15 { color: #333333; }
.STYLE16 { font-size:15px; color: #333333; font-weight: bold; }
.STYLE17 { font-size:14px; font-weight:bold; height:31px; line-height:31px; overflow:hidden; }
.STYLE19 { font-size:16px }
.STYLE23{border:solid 1px rgb(159,159,159); height:21px; width:120px; padding-bottom:4px;}

</STYLE>

	
	</head>

	<body>
		<div style="z-index: 1; position:fixed; background-image: url('img_woshop/white.jpg'); background-repeat: repeat;" >
		<table width="100%" cellpadding="0" cellspacing="0" class="tabdao">
			<tr background="img_woshop/bj2.jpg" class="trs" height="32" >
				<td align="center" valign="middle" class="tds">&nbsp;</td>
				<td align="center" width="10%">
					<table border="0" align="center" cellpadding="0" cellspacing="0" class="tabdao">
						<form id="searchForm" name="searchForm" action="woshopO/search.action" method="get" target="_top">
							<tbody>
								<tr background="img_woshop/bj2.jpg" >
									<td align="right" valign="middle" background="img_woshop/bj2.jpg">
										<div>
											<input type="hidden" name="imoduletype" value="10"/>
											
											<input type="hidden" name="empId" value="${empId}"/>
											<input type="hidden" name="appOsType" value="${appOsType}"/>
											<input type="hidden" name="version" value="${version}"/>
											<input type="hidden" name="isIOS" value="${isIOS}"/>
											<input type="hidden" name="installType" value="${installType}"/>
											<input type="hidden" name="pageSize" value="${pageSize}"/>
											
											<input type="text" name="keyword" id="keyword" class="STYLE23" 
											onclick="javascript:$('#keyword').val('');" maxlength="255" value="${keyword }"/>
										</div>
									</td>
									<td width="3%" valign="middle" background="img_woshop/bj2.jpg" >
										<div style="position:absolute; right:0px; top: 1px;">
											<a href="javascript:searchApp()">
												<img src="img_woshop/fdj.jpg" width="39" height="27" align="absmiddle" />
											</a>
										</div>
									</td>
								</tr>
							</tbody>
						</form>
					</table>
				</td>
			</tr>
		</table>
		<table width="100%" border="0" align="center">
			<tr>
				<td width="100%" align="center" valign="top">
					<div id="div3">
						<ul id="tags">
							<c:if test="${empty pkgList}"><br/><center style="font-size:14px;">对不起, 没有找到相关的类别!</center></c:if>
							<c:if test="${!empty pkgList}">
								<c:forEach items="${pkgList}" var="pkg" varStatus="i" >
			       					<c:if test="${i.index == 0}">
			       						<li class="selectTag" id="li${i.index }">
			       							<input type="hidden" id="pkgId" value="${pkg.id }"/>
			       							<input type="hidden" id="bizType" value="${pkg.info }"/><!-- pkg.info 里面存放的是bizType，用这个字段先代替着  -->
								       		<a id="a_${i.index }"  href="javascript:void(0);" onclick="javascript:selectTag('${pkg.id }','${pkg.name }','${pkg.info }','li${i.index }',this);" >
								       			<div align="center" >
								       				<p class="hj">
								       					<c:if test="${pkg.info == -1 }"><!-- 为-1则是阅联的数据，图标是不需要拼接的 -->
									       					<img src="img_woshop/pkg_def.png" data-original="${pkg.icon}" width="60" height="60" align="absmiddle" />
								       					</c:if>
								       					<c:if test="${pkg.info != -1 }">
									       					<img src="img_woshop/pkg_def.png" data-original="${fileserverUrl}${pkg.icon}" width="60" height="60" align="absmiddle" />
								       					</c:if>
								       					<br />
								       					<strong>${pkg.name }</strong>
								       				</p>
								       			</div>
								       		</a>
								       	</li>
			       					</c:if>
			       					<c:if test="${i.index > 0 && i.index <= 6}">
			       						<li id="li${i.index }">
			       							<input type="hidden" id="pkgId" value="${pkg.id }"/>
			       							<input type="hidden" id="bizType" value="${pkg.info }"/><!-- pkg.info 里面存放的是bizType，用这个字段先代替着  -->
							          		<a id="a_${i.index }"  href="javascript:void(0);" onclick="javascript:selectTag('${pkg.id }','${pkg.name }','${pkg.info }','li${i.index }',this);" >
							          			<div align="center">
							          				<p class="hj">
							          					<c:if test="${pkg.info == -1 }"><!-- 为-1则是阅联的数据，图标是不需要拼接的 -->
							          						<img src="img_woshop/pkg_def.png" data-original="${pkg.icon}" width="60" height="60" align="absmiddle" />
							          					</c:if>
							          					<c:if test="${pkg.info != -1 }">
							          						<img src="img_woshop/pkg_def.png" data-original="${fileserverUrl}${pkg.icon}" width="60" height="60" align="absmiddle" />
							          					</c:if>
							          					<br />
							          					<strong>${pkg.name }</strong>
							          				</p>
							          			</div>
							          		</a>
							          	</li>
			       					</c:if>
							   	</c:forEach>
							</c:if>
						</ul>
					</div>
				</td>
			</tr>
		</table>

		<table width="100%" border="0" align="center" cellspacing="8">
			<td width="80%" >
				<div align="left" id="con">
					<span class="STYLE16" id="pkgName"> &nbsp;&nbsp;&nbsp;&nbsp;${pkg.name }&nbsp;&nbsp;&nbsp;&nbsp; </span>
					<input name="checkbox" id="checkAll" type="checkbox"/>
	                <label for="checkAll"><span class="STYLE13"> 全选</span></label>
					<span class="STYLE13"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;已选择&nbsp; <SPAN id="checkCount" class="STYLE14">0</SPAN>&nbsp;款, 
	           		共&nbsp;  <SPAN id="checkSize" class="STYLE14"> 0.00 </SPAN>&nbsp;MB </span>
	           	</div>	
			</td>
			<td width="20%" valign="bottom">
				<div align="center" id="con"><a href="javascript:void(0);" onclick="javascript:multiApp();" target="_self" ><img src="img_woshop/yjanz.png" /></a></div>
			</td>
		</table>
		</div>
		
        
		<div id=tagContent style="position: absolute; top: 180px; left: 50px;">
			<div class="softindex">
				<ul id="bodyul">
					<div id="shuju">
					
					</div>
				</ul>
			</div>
		</div>

		<div class="tanchubj popUp" id="tanchulei" style="position: absolute; left: 620.5px; top: 745px; display: none;"></div>
			
	</body>
</html>