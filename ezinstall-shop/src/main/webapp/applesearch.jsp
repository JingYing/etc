<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils,com.i8app.shop.dao.*,com.i8app.shop.domain.*,org.springframework.context.*"%>
<%
	ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext()); 
    AppleInfoDao appledao = context.getBean(AppleInfoDao.class);
    Appleinfo apple = appledao.getAppleInfo();
    if(apple != null) {
        out.print(apple.getId()+",");
        out.print(apple.getMac()+",");
        out.print(apple.getGuid());
    }
%>