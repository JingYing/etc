/**
 * util.js 
 * 常用javascript函数, 依赖于jquery, 需要设置项目地址根路径basePath
 */ 

var basePath = "/shop";		//${pageContext.request.contextPath}值

/**
 * 常用的body.onload事件, 
 * 1. 把所有img元素加上lazyload事件, 依赖于jquery.lazyload插件
 * 2. 把所有a和input元素加上"点击后无虚线框"效果
 * 3. 禁止拖动选择
 */
function utilInit()	{
	$("img").lazyload();
	$("a, input:button").focus(function()	{
		$(this).blur();
	});
	document.onselectstart = function()	{
		return false;
	}
}

/**
 * 单个下载, 传到后台的格式为?downInfo=70001,111,我我;
 * @param source 软件类型(沃商店游戏, i8应用等)
 * @param id 软件id
 * @param name 软件名称
 * @deprecated softname含有单双引号特殊字符时有问题
 */
function down(source, id, name)	{
	var url = basePath + "/app/down.action?downInfo=" + source + "," + id + "," + name + ";";
	window.location.href = encodeURI(url);
}

/**
 * 单个下载, 传到后台的格式为?downInfo=70001,111,我我;
 * 取button或a标签中的softname和source和appid属性, 并过滤name中的特殊字符
 * @param id
 */
function down(id)	{
	var elem = $("input[type='button'][appid=" + id +"]");
	if(elem.length == 0)	{
		elem = $("a[appid=" + id +"]");
	}
	var name = $(elem).attr("softname");
	var source = $(elem).attr("source");
	var regex = /[,;]/g;	//过滤,和;
	name = name.replace(regex, "");	
	var url = basePath + "/app/down.action?downInfo=" + source + "," + id + "," + name + ";";
	window.location.href = encodeURI(url);
}

/**
 *	勾选安装,  传到后台的格式为?downInfo=70001,111,我我;70001,111,我我;70001,111,我我;
 *  不支持IE6. 把全部勾选框(除了全选框)的三个值筛选出来并传至后台
 *	页面元素的命名规则: 全选框的id为'checkAll', 
 *  单个勾选框的value值设为"软件来源", id值设为"软件id值", name值设为"软件名称"
 *  maxlength指浏览器地址栏支持的最大长度
 */
function multidown()	{
	var maxlength = 1200;
	var info = '';
	$.each($("input:checked[id!='checkAll']"), function(index, element)	{
		if(encodeURI(info).length >= maxlength )	{	//地址栏长度超出后, 不累加info
		} else	{
			var name = element.name.replace(/[,;]/g, "");
			info += element.value +","+ element.id +","+ name +";";
		}
	});
	if(info == '')		return;
	window.location.href = encodeURI(basePath + "/app/down.action?downInfo=" + info);
}

/**
 * 全选框的click事件, 把全选框元素自身传递进来
 * @param element 全选框
 */
function checkAll(element)	{
	$(':checkbox').attr('checked', element.checked);
}
