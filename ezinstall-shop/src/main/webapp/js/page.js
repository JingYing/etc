var pageNo = 1;
var pageCount = 0;

function ld_pages(pageIndex,pageTag,url,paraStr,retForm){
	if(pageTag == "-"){
		if(pageNo<=1){
			return;
		}
		pageNo = pageNo - 1;
	}else if(pageTag == "+" ){
		if(pageNo>=pageCount){
			return;
		}
		
		pageNo = pageNo + 1;
	}else{
		pageNo = parseInt(pageIndex);
	}	
	
	url = url+"?date=" + new Date().getTime() + "&offset=" + pageNo;	
	$.post(url,paraStr,function(data) {showPage(data);initPage();},retForm);
}


function initPage(){
	
	var pageDiv = document.getElementById("pageDiv");
	var pageDivStr = "";	
	var pageCountNode = document.getElementById("pageCount");
	
	if(pageCountNode!=null){
		pageCount = parseInt(pageCountNode.value);
	}
	
	if(pageCount<1){
		//alert("当前查询条件无数据返回，请选择其他条件进行查询！");
	}
	if(pageNo > 1){
		pageDivStr = pageDivStr + '<a style="cursor:pointer" class=\"previous\" onclick=\"pages(0,\'-\');\" >上一页</a> ';
	}
	
	if(pageCount>7){
		
		if(pageNo<5){
			for(var s=1;s<=5;s++){
				if(s == pageNo){
					pageDivStr = pageDivStr + '<a style="cursor:pointer" class="curr" onclick=\"pages(' + s + ',\'\');\">' + s + '</a> ';
    			}else{
    				pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + s + ',\'\');\">' + s + '</a> ';
        		} 
			}
			
			pageDivStr = pageDivStr + ' . . . ';
			pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + pageCount + ',\'\');\">' + pageCount + '</a> ';
			
		}else if(pageNo>pageCount-4){
			pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + 1 + ',\'\');\">' + 1 + '</a> ';	
			pageDivStr = pageDivStr + ' . . . ';

			for(var s=pageCount-4;s<=pageCount;s++){
				if(s == pageNo){
					pageDivStr = pageDivStr + '<a style="cursor:pointer" class="curr" onclick=\"pages(' + s + ',\'\');\">' + s + '</a> ';
    			}else{
    				pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + s + ',\'\');\">' + s + '</a> ';
        		} 
			}		
		}else{
			
			pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + 1 + ',\'\');\">' + 1 + '</a> ';
			pageDivStr = pageDivStr + ' . . . ';
			
			for(var s=pageNo-2;s<=pageNo+2 && s>0 && s<=pageCount;s++){
				if(s == pageNo){
					pageDivStr = pageDivStr + '<a style="cursor:pointer" class="curr" onclick=\"pages(' + s + ',\'\');\">' + s + '</a> ';
    			}else{
    				pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + s + ',\'\');\">' + s + '</a> ';
        		} 
			}
		
			pageDivStr = pageDivStr + ' . . . ';
			pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + pageCount + ',\'\');\">' + pageCount + '</a> ';
		}

	}else{
		for(var i = 1; i <= pageCount; i++){
			if(i == pageNo){
				pageDivStr = pageDivStr + '<a style="cursor:pointer" class="curr" onclick=\"pages(' + i + ',\'\');\">' + i + '</a> ';
    		}else{
    			pageDivStr = pageDivStr + '<a style="cursor:pointer" onclick=\"pages(' + i + ',\'\');\">' + i + '</a> ';
        	} 
		}
	}
	
	if(pageNo < pageCount){
		pageDivStr = pageDivStr + '<a style="cursor:pointer" class=\"next\" onclick="pages(0,\'+\');\">下一页</a>'; 
	}
	
	pageDiv.innerHTML = "";
	pageDiv.innerHTML = pageDivStr;	
}	





