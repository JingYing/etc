
/*
 * imgAlter 1.0 插件
 * @description jquery的图片状态插件, 使用cookie存储图片状态.
 * 
 * HTML示例
 * <img src="xxx.jpg" imgAlterId1="111" imgAlterId2="1111"/>
 * <img src="yyy.jpg" imgAlterId1="222" imgAlterId2="2222"/>
 * 
 * 初始化示例(根据cookie初始化图片状态)
 * $(function(){
 *    $('img').imgAlter({
 * 		  map : {
			'1': '111.jpg',	//状态为1时, 对应的图片为111.jpg
			'2': '222.jpg',
			'3': '333.jpg'
		  }
 *    });
 * });
 *
 * 触发函数(改变状态,并记录入cookie)
 * $('img').imgAlterTrigger('111', '2');	//当imgAlterId1或imgAlterId2为'111'时, 改变成状态'2'对应的图片
 *
 * @author JingYing
 */
(function($) {    
	
	$.fn.imgAlter = function(option)	{
		$.extend($.fn.imgAlter.defaults, option);
		var cookies = document.cookie.split('; '); 
		var pre = $.fn.imgAlter.defaults.cookiePrefix;
		for(i in cookies)	{
			var arr = cookies[i].split('=');
			if($.trim(arr[0]).indexOf(pre) == 0)	{
				var id = arr[0].replace(pre, '');
				var target = statToImg(unescape(arr[1]));
				$(this).each(function(){
					var $this = $(this);
					for(j in $.fn.imgAlter.defaults.attribute)	{
						if($this.attr($.fn.imgAlter.defaults.attribute[j]) == id && target != null)	{
							this.src = target; 
						}
					}
				});
			}
		}
		return this;
	};
	
	$.fn.imgAlter.defaults = {
		attribute : ['imgAlterId1','imgAlterId2'],	//img的额外属性数组
		/* 配置示例:key为图片状态值, value为该状态对应的图片*/
		map : {
			'1': 'img_woshop/zzxz.png',
			'2': 'img_woshop/yxz.png',
			'3': 'img_woshop/zzaz.png',
			'4': 'img_woshop/yaz.png',
			'5': 'img_woshop/zzsc.png',
			'6': 'img_woshop/ysc.png'
		},
		cookiePrefix : 'imgAlter_'	//cookie中图片id的前缀
	};
	
	$.fn.imgAlterTrigger = function(id, status)	{
		var target = statToImg(status);
		document.cookie = $.fn.imgAlter.defaults.cookiePrefix + id +"=" + escape(status)+";path=/";
		$(this).each(function(){ 
			var $this = $(this);
			for(j in $.fn.imgAlter.defaults.attribute)	{
				if($this.attr($.fn.imgAlter.defaults.attribute[j]) == id && target != null)	{
					this.src = target;
				}
			}
		});
	};
	
	function statToImg(status)	{
		var tar = $.fn.imgAlter.defaults.map[status];
		return tar == undefined ? null : tar;
	};

})(jQuery);    