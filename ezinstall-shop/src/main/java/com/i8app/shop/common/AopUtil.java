package com.i8app.shop.common;

import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.springframework.stereotype.Component;

import com.mchange.v2.c3p0.PooledDataSource;

@Component
public class AopUtil	{
	private static Logger logger = Logger.getLogger(AopUtil.class);
	private SessionFactory sessionFactory;
	private DataSource dataSource;
	
	/**
	 * 二级缓存统计信息
	 */
	public void hibernateStatistics()	{
		Statistics st = sessionFactory.getStatistics();
		System.err.println(st);
		System.err.printf("二级缓存命中:%d 未命中:%d \n查询缓存命中:%d 未命中: %d \n",
				st.getSecondLevelCacheHitCount(), st.getSecondLevelCacheMissCount(),
				st.getQueryCacheHitCount(), st.getQueryCacheMissCount()); 
	}
	
	/**
	 * 查看c3p0连接池状态
	 * @throws SQLException 
	 */
	public void c3p0Stat() {
		if(dataSource instanceof PooledDataSource)	{
			PooledDataSource ds = (PooledDataSource)dataSource;
			try {
				logger.info(String.format("num_connections: %s \nnum_busy_connections: %s \nnum_idle_connections: %s \n",
						ds.getNumConnectionsDefaultUser(), 
						ds.getNumBusyConnectionsDefaultUser(), 
						ds.getNumIdleConnectionsDefaultUser())); 
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
	}
	
	/**
	 * 捕捉到异常后进行记录
	 * @param method
	 * @param args
	 * @param target
	 * @param ex
	 */
	public void errorLog(RuntimeException e)	{
		logger.error(e.getMessage(), e);
	}
	
	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Resource
	public void setDataSource(DataSource dataSource)	{
		this.dataSource = dataSource;
	}
}
