package com.i8app.shop.common.wsclient;

/**
 * hessian存根代码， 复制自connector工程
 * 
 * @author JingYing 2013-7-29
 */
public interface SmsSender {
	
	/**
	 * 发送6位数验证码. 每个区域码对应的验证码文字模板均不同
	 * @param areaId 区域码(areaInfo表中的areacode字段), 每个区域码对应的短信服务器均不同
	 * @param phoneNo
	 * @param verifyCode 验证码
	 */
	void sendVerifyCode(String areaId, String phoneNo, String verifyCode) throws Exception ;
	
	/**
	 * 短信软件下载地址. 每个区域码对应的地址文字模块均不同. 必须支持长短信发送
	 * @param areaId 区域码(areaInfo表中的areacode字段), 每个区域码对应的短信服务器均不同
	 * @param phoneNo 手机号
	 * @param appName
	 * @param appUrl
	 */
	void sendDownloadLink(String areaId, String phoneNo, String appName, String appUrl) throws Exception;
	
	/**
	 * 发送任意短信
	 * @param phoneNo
	 * @param msg 短信内容
	 * @throws Exception 
	 */
	void testSend(String phoneNo, String msg) throws Exception;
}
