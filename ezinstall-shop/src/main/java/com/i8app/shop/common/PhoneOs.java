package com.i8app.shop.common;

import java.io.Serializable;

/**
 * 手机操作系统分类
 * @author JingYing
 */
public enum PhoneOs implements Serializable	{
	
	/**
	 *  手机操作系统的分类iphone为正版, iphonex为破解版. 
	 *  括号内数值代表该操作系统对应的osType
	 */
	IPHONE(300), IPHONEX(300), ANDROID(200), IPAD(600), IPADX(600),
	SYMBIAN(100), WINDOWS_MOBILE(400), JAVA(500), OMS, ERROR;
	
	public static final String UNSUPPORT = "unsupport";
	
	/**
	 *  转换后的操作系统代号; 转换后的osid 
	 */
	private Integer osType, osid;
	
	/**
	 * 客户端传过来的代号
	 */
	private int inputOsType;
	
	/**
	 * 保存客户端传过来的操作系统版本号, 厂商名, 型号名
	 */
	private String version, typeName, manuName;
	
	private PhoneOs()	{	}
	
	/**
	 * 操作系统和osType对应关系, 无对应则返回null
	 * @param osType
	 */
	private PhoneOs(Integer osType)	{
		this.osType = osType;
	}
	
	/**
	 * appType和操作系统的对应规则
	 * @param appType
	 * @return
	 */
	public static PhoneOs getInstance(int inputOsType, String version, 
				String manuName, String typeName)	{
		PhoneOs p;
		switch(inputOsType)	{
		case 1 :
			p = PhoneOs.IPHONEX;  
			break;
		case 2 :
			p = PhoneOs.IPHONE;  
			break;
		case 10 :
			p = PhoneOs.SYMBIAN;
			break;
		case 20 :
			p = PhoneOs.ANDROID;
			break;
		case 40 :
			p = PhoneOs.WINDOWS_MOBILE;
			break;
		case 50 :
			p = PhoneOs.JAVA;
			break;
		case 60 :
			//TODO 待定
			p = PhoneOs.IPADX;
			break;
		case 70 :
			//TODO 待定
			p = PhoneOs.IPAD;
			break;
		default:
			p = PhoneOs.ERROR;
			break;
		}
		p.setOsid(p.toOsid(version));
		p.setInputOsType(inputOsType);
		p.setVersion(version);
		p.setManuName(manuName);
		p.setTypeName(typeName);
		return p;
	}
	
	/**
	 * 获取各个操作系统对应的软件包后缀名
	 * @return
	 */
	public String[] getSoftwareSuffix()	{
		switch(this)	{
		case IPHONE:
		case IPHONEX:
			return new String[]{".ipa"};
		case ANDROID:
			return new String[]{".apk"};
		case SYMBIAN:
			return new String[]{".sis", ".sisx"};
		case JAVA:
			return new String[]{".jar"};
		default:
			return null;
		}
	}
	
	/**
	 * 将android和iPhone的操作系统版本号字符串, 按照业务规则, 转换成int型数值, 再转成String形式
	 * @param version 例如1.2.5这种以小数点分隔的数字串
	 * @return
	 */
	private Integer toOsid(String version)	{
		if(version == null || "".equals(version))
			return null;
		else {
			int num = version.split("[.]").length;	 
			for(int i=0; i<5-num; i++)	{
				version += ".0";
			}
			String[] strings = version.split("[.]");
			int sum = 0;
			for(int i=0, j=strings.length-1; i<strings.length; i++, j--)	{
				sum += Integer.parseInt(strings[i]) * Math.pow(50, j);
			}
			switch(this)	{
				case IPHONE :
				case IPHONEX :
					return sum + 400000000;
				case ANDROID :
					return sum;
				case OMS : 
					//TODO 转换规则待定
					return sum;
				case WINDOWS_MOBILE:
					return sum + 800000000;
				case IPAD :
				case IPADX:
					return sum + 900000000;
				default : 
					return 0;
			}
		}
	}

	private void setInputOsType(Integer inputOsType) {
		this.inputOsType = inputOsType;
	}

	private void setVersion(String version) {
		this.version = version;
	}

	private void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	private void setManuName(String manuName) {
		this.manuName = manuName;
	}

	public Integer getOsType() {
		return osType;
	}

	public Integer getInputOsType() {
		return inputOsType;
	}

	public String getVersion() {
		return version;
	}

	public String getTypeName() {
		return typeName;
	}

	public String getManuName() {
		return manuName;
	}

	public  Integer getOsid() {
		return osid;
	}

	private void setOsid(Integer osid) {
		this.osid = osid;
	}
	
}
