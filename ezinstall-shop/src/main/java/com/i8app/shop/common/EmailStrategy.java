package com.i8app.shop.common;

import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.TriggeringEventEvaluator;

/**
 * 邮件发送策略
 * @author JingYing
 *
 */
public class EmailStrategy implements TriggeringEventEvaluator	{

	private static long count = 0;
	
	@Override
	public boolean isTriggeringEvent(LoggingEvent event) {
		count ++;
		if(count == 1)		//第一次要发送
			return true;
		if(count % 50 == 0)	{	//以后第?条发送一次
			return true;
		} else	{
			return false;
		}
	}

}
