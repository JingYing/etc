package com.i8app.shop.common;

/**
 * 短信通道连接时出现不可恢复的异常
 * @author jing
 *
 */
public class SmsAccessException extends RuntimeException	{

	public SmsAccessException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SmsAccessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SmsAccessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SmsAccessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
