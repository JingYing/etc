package com.i8app.shop.common;

public class EmpNotLoginException extends RuntimeException	{

	public EmpNotLoginException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmpNotLoginException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmpNotLoginException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmpNotLoginException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
