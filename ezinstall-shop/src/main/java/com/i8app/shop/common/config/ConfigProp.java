package com.i8app.shop.common.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import com.i8app.shop.common.Util;

/**
 * 从config.properties生成配置对象
 * 
 * @author JingYing 2012-12-27
 */
public class ConfigProp {

	private Properties prop;
	
	private static ConfigProp instance = null;
	
	private ConfigProp() {
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("config.properties");
		prop = new Properties();
		try {
			prop.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally	{
			if(is != null)
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	public static synchronized ConfigProp getInstance()	{
		if(instance == null)
			instance = new ConfigProp();
		return instance;
	}
	
	public String getFileServerUrl()	{
		return prop.getProperty("fileServer.url");
	}
	
	public String getFileServerDir()	{
		return prop.getProperty("fileServer.dir");
	}
	
	public String getDBUrl()	{
		return prop.getProperty("db.url");
	}
	
	public String getDBUsername()	{
		return prop.getProperty("db.username");
	}
	
	public String getDBPassword()	{
		return prop.getProperty("db.password");
	}

	public List<Integer> getExtraInstallIds()	{
		String s = prop.getProperty("extrainstall.ids");
		if(s != null)	{
			String[] ss = s.split(",");
			return Util.toIntList(ss);
		} else	{
			return null;
		}
	}
	
	public String getSmsIp1()	{
		return prop.getProperty("sms.ip1");
	}

	public String getSmsIp2()	{
		return prop.getProperty("sms.ip2");
	}
	
	public String getPageSize3()	{
		return prop.getProperty("pageSize3");
	}
	
	public String getWoJia_NATION_PKG()	{
		return prop.getProperty("woJia_NATION_PKG");
	}
	public String getWoStore_NATION_PKG()	{
		return prop.getProperty("woStore_NATION_PKG");
	}
	public String getShanXi_NATION_PKG()	{
		return prop.getProperty("shanXi_NATION_PKG");
	}
	
	public String getQz_pkgId()	{
		return prop.getProperty("qz_pkgId");
	}
}
