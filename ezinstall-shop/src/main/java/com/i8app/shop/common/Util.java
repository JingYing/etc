package com.i8app.shop.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class Util {
	
	/**
	 * 截取字符串长度, 中文占2位, 英文占1位
	 * @param str
	 * @param width
	 * @param ellipsis
	 * @return
	 */
	public static String abbreviate(String str, int width, String ellipsis) {
		if (str == null || "".equals(str)) 
			return "";
		int d = 0; // byte length
		int n = 0; // char length
		for (; n < str.length(); n++) {
			d = (int) str.charAt(n) > 256 ? d + 2 : d + 1;
			if (d > width) 	break;
		}
		if (d > width) {
			//n = n - ellipsis.length() / 2;
			return str.substring(0, n > 0 ? n : 0) + ellipsis;
		}
		return str = str.substring(0, n);
	}
	
	/**
	 * jstl自定义函数, 解决fn:replace不能替换换行符的问题
	 * @param input
	 * @param regex
	 * @param replacement
	 * @return
	 */
	 public static String replaceAll(String input, String regex, String replacement)			    {
		 return input.replaceAll(regex, replacement);
	 }
	 
	 public static String chooseStr(boolean condition, String trueResult, String falseResult)	{
		 return condition ? trueResult : falseResult;
	 }
	
	/**
	 * 判断一个URL地址是不是图片(只判断jpg, png, bmp, gif)
	 * @param s
	 * @return
	 */
	public static Boolean isPic(String s)	{
		if(s != null)	{
			String str[] = new String[]{".jpg",".png",".bmp",".gif"};
			for(int i=0; i<str.length; i++)	{
				if(s.endsWith(str[i]))
					return true;
			}
		}
		return false;
	}
	
	/**
	 * 将String数组转换为Int数组
	 * @param s
	 * @return
	 */
	public static int[] toIntArray(String[] s)	{
		int[] ints = new int[s.length];
		for(int i=0; i<s.length; i++)	{
			ints[i] = Integer.parseInt(s[i]);
		}
		return ints;
	}
	
	/**
	 * 将String数组转换成INTEGER集合
	 * @param s
	 * @return
	 */
	public static List<Integer> toIntList(String[] s)	{
		List<Integer> ints = new LinkedList<Integer>();
		for(int i=0; i<s.length; i++)	{
			ints.add(Integer.parseInt(s[i]));
		}
		return ints;
	}
	
	
	/**
	 * 游戏或软件的用户评分变化规则, 计算出用户对某款软件的新总评分
	 * @param totalPoint	原有的总评分
	 * @param valueCount	原有的评价次数
	 * @param currentPoint	新评分(用户对软件的单次评分)
	 * @return
	 */
	public static float getNewTotalValuePoint(float totalPoint, int valueCount, int currentPoint)	{
		return (totalPoint * valueCount + currentPoint)/(valueCount + 1);
	}
	
	/**
	 * 动态拼接Restrictions的Or操作
	 * @param c1
	 * @param c2
	 * @return
	 */
	public static Criterion qbcOr(Criterion c1, Criterion c2) {
		if(c1 == null)	return c2;
		if(c2 == null)	return c1;
		return Restrictions.or(c1, c2);
	}
	
	
	/**
	 * 将传入的aa,bb;cc,dd;形式的字符串转变为Map<Integer, Integer>
	 * @param str
	 * @return
	 */
	public static Map<Integer, Integer> changeToButtonMap(String str)	{
		Map<Integer, Integer> buttonMap = new HashMap<Integer, Integer>();
		String[] kvStrs = str.split("[;]");
		for(int i=0; i<kvStrs.length; i++)	{
			if(!"".equals(kvStrs[i]))	{
				String[] kv = kvStrs[i].split("[,]");
				buttonMap.put(Integer.parseInt(kv[0]), Integer.parseInt(kv[1]));
			}
		}
		return buttonMap;
	}
	
	/**
	 * 生成时间戳字符串
	 * @return
	 */
	public static String createTimestamp()	{
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(new Date());
	}
	
	/**
	 * 生成一串随机数组成的字符串
	 * @param num 随机数的位数
	 * @return
	 */
	public static String createRandomString(int num)	{
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i=0; i<num; i++)	{
			sb.append("" + rand.nextInt(10));
		}
		return sb.toString();
	}
	
	/**
	 * 转化为unicode码
	 * @param s
	 * @return
	 */
	public static String toUnicode(String s)	{
		StringBuilder sb = new StringBuilder();
		char[] chars = s.toCharArray();
		for(int i=0; i<chars.length; i++)	{
			String hex = Integer.toHexString(chars[i]);
			if(hex.length() <= 2)	{
				hex = "00" + hex;
			}
			sb.append("\\u").append(hex);
		}
		return sb.toString();
	}
	
	
	/** 
	 * MD5加密
	 * @param str 要加密的字符串 
	 * @return    加密后的字符串 
	 */  
	public static String toMD5(String str){  
	    try {  
	        MessageDigest md = MessageDigest.getInstance("MD5");  
	        md.update(str.getBytes());  
	        byte[] byteDigest = md.digest();  
	        int i;  
	        StringBuffer buf = new StringBuffer("");  
	        for (int offset = 0; offset < byteDigest.length; offset++) {  
	            i = byteDigest[offset];  
	            if (i < 0)  
	                i += 256;  
	            if (i < 16)  
	                buf.append("0");  
	            buf.append(Integer.toHexString(i));  
	        }  
	         
//	        return buf.toString().substring(8, 24);		//16位加密     
	        return buf.toString();						//32位加密    
	    } catch (NoSuchAlgorithmException e) {  
	        e.printStackTrace();  
	        return null;  
	    }
	}
	
	/**
	 * 根据起始位置和页面大小, 转换为对应的页面数
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public static int toPageNo(int offset, int pageSize)	{
		return offset/pageSize + 1;
	}
	
	/**
	 *  将pageNo转成offset
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static int toOffset(int pageNo, int pageSize)	{
		return (pageNo - 1) * pageSize;
	}
	
	/**
	 * 将字符串集合分隔拼接成一条字符串
	 * @param list 集合
	 * @param splitter 分隔符
	 * @return
	 */
	public static String join(List<String> list, String splitter)	{
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<list.size(); i++)	{
			sb.append(list.get(i));
			if(i < list.size() - 1)	
				sb.append(splitter);
		}
		return sb.toString();
	}
	
	public static String ifNull(String s)	{
		return s == null ? "" : s;
	}
	
	/**
	 * 在数组中搜索指定值, 找不到则返回-1, 找到则返回数组的下标值
	 * @param array
	 * @param target
	 * @return
	 */
	public static int searchArray(String[] array, String target)	{
		for(int i=0; i<array.length; i++)	{
			if(array[i].equals(target))
				return i;
		}
		return -1;
	}
	
	/**
	 * 解析[pro:35][city:372][area:2825][hall:1]字符串, 获取冒号后面的值
	 * @param orgIdStr [pro:35][city:372][area:2825][hall:1]
	 * @param title pro, city, area, hall四选一
	 * @return
	 */
	public static int parseOrgIdStr(String orgIdStr, String title)	{
		if(!"pro".equals(title) && !"city".equals(title) && !"area".equals(title) && !"hall".equals(title))
			throw new IllegalArgumentException("title值必须为pro, city, area, hall的一个");
		Matcher m = Pattern.compile("\\[" + title + ":\\d+\\]").matcher(orgIdStr);
		m.find();
		String s = m.group();
		s = s.replace("[" + title + ":", "").replace("]", "");
		return Integer.parseInt(s);
		
	}
	
	public static String deepToString(Map<Object, Object[]> map)	{
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for(Entry<Object, Object[]> e : map.entrySet())	{
			sb.append(e.getKey());
			sb.append("=");
			sb.append(Arrays.deepToString((Object[]) e.getValue()));
			sb.append(",");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static String getNOWTIME() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new Date());
	}
	
	public static int conversionInteger(String parameters){
		if(parameters != null) {
			return Integer.parseInt(parameters);
		}
		return -1;
	}
	
}
