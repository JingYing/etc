package com.i8app.shop.common;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.i8app.shop.service.DiskElementFactory;

/**
 * 使用合成模式体现树形关系. 并组合FILE类简化FOLDER类的实现方式.
 * @author JingYing
 *
 */
public class Folder extends DiskElement {
	
	public Folder(File file)	{
		super(file);
	}

	@Override
	public List<DiskElement> getChildren() {
		List<DiskElement> children = new ArrayList<DiskElement>();
		File[] files = file.listFiles();
		for(File f : files)	{
			if(f.isDirectory())	
				children.add(new Folder(f));	//把文件夹排在前
		}
		for(File file : files)	{
			if(!file.isDirectory())
				children.add(new FileBean(file));	//文件排在后
		}
		return children;
	}

	@Override
	public String getSuffix() {
		return "folder";
	}

}
