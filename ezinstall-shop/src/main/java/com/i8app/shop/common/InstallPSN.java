package com.i8app.shop.common;

import com.i8app.shop.domain.Machineinfo;

public class InstallPSN {

	
	public static Machineinfo DecodingPSN(String psn) {
		String tempstr = "";
		String psnstr = "";
		String temppsn = "";
		for(int i = psn.length() - 1; i >= 0; i--) {
			tempstr += psn.charAt(i); 
		}
		System.out.println(tempstr);
		
		for(int i = 0;i < tempstr.length(); i += 2) {
			if (i+2 > tempstr.length()) {i= tempstr.length()-2;}
			temppsn = tempstr.substring(i, i+2);
            int temp = emnu(temppsn.toLowerCase().trim().charAt(0))*16 + emnu(temppsn.toLowerCase().trim().charAt(1)); 
            psnstr += (char)temp;
		}
		Machineinfo mac = new Machineinfo();
		
		int a = psnstr.indexOf("=") + 1;
		int a1 = psnstr.indexOf("&");
		mac.setPsn(psnstr.substring(a, a1));
		
		int b = psnstr.indexOf("=",a+1) + 1;
		int b1 = psnstr.indexOf("&",a1+1);
		mac.setMachineCode(psnstr.substring(b, b1));
		
		int c = psnstr.indexOf("=",b+1) + 1;
		int c1 = psnstr.indexOf("&",b1+1);
		mac.setSn(psnstr.substring(c, c1));
		
        int d = psnstr.indexOf("=",c+1) + 1;
		int d1 = psnstr.indexOf("&",c1+1);
        mac.setMachineName(psnstr.substring(d, d1));
		
        int e = psnstr.indexOf("=",d+1) + 1;
		int e1 = psnstr.indexOf("&",d1+1);
        mac.setDiskSerial(psnstr.substring(e, e1));
		
		int f = psnstr.indexOf("=",e+1) + 1;
		int f1 = psnstr.indexOf("&",e1+1);
        mac.setMacAddr(psnstr.substring(f, f1));
		
		int g = psnstr.indexOf("=",f+1) + 1;
		int g1 = psnstr.indexOf("&",f1+1);
		if(g1 < 0) { g1 = psnstr.length();}
        mac.setIp(psnstr.substring(g, g1));
        
		return mac;
	} 
	
	private static int emnu(char charpsn) {
		switch (charpsn) {
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
		case 'a':
			return 10;
		case 'b':
			return 11;
		case 'c':
			return 12;
		case 'd':
			return 13;
		case 'e':
			return 14;
		case 'f':
			return 15;
		default :
			return 0;
		} 
	}
}
