package com.i8app.shop.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Mail;

@Repository
public class MailDao extends BaseDao {

	public List<Mail> list()	{
		return this.getHibernateTemplate().loadAll(Mail.class);
	}
	
}
