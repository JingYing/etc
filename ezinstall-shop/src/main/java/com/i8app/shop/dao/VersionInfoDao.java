package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.VersionInfo;

@Repository
public class VersionInfoDao extends BaseDao	{
	
	public VersionInfo findByVersions(String versions)	{
		String hql = "from VersionInfo where versions = ?";
		List<VersionInfo> list = this.getHibernateTemplate().find(hql, versions);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}

}
