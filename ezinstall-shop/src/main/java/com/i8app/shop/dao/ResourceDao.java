package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.AppDTO;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.AppMarker;
import com.i8app.shop.domain.Clientconfigdate;
import com.i8app.shop.domain.Pkg;
import com.i8app.shop.domain.ResourceType;
import com.i8app.shop.domain.UnionreadPkgView;

@SuppressWarnings("unchecked")
@Repository
public class ResourceDao extends BaseDao {

	public void updateApp(AppDTO app)	{
		this.getHibernateTemplate().update(app);
	}
	
	/**
	 * 总安装次数+1
	 * @param soft
	 */
	public void updateTotalInstallCount(String uuid)	{
		String hql = "update AppDTO set installCount = coalesce(installCount,0) + 1 where uuid = ?";
		getHibernateTemplate().bulkUpdate(hql, uuid);
	}
	
	/**
	 * 下载次数+1
	 */
	public void updateDownNum(String uuid)	{
		String hql = "update AppDTO set downCount = coalesce(downCount,0) + 1 where uuid = ?";
		getHibernateTemplate().bulkUpdate(hql, uuid);
	}
	
	public List<ResourceType> appTypeList(String typeId)	{
		String hql = " from ResourceType WHERE pid = ? ";
		return this.getHibernateTemplate().find(hql, typeId);
	}
	
	public ResourceType findAppType(String typeId)	{
		String hql = " from ResourceType WHERE id = ? ";
		List<ResourceType> list = this.getHibernateTemplate().find(hql, typeId);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
	
	public AppInstallPack findAppPkgByid(String pkguuid)	{
		return this.getHibernateTemplate().get(AppInstallPack.class, pkguuid);
	}
	
	public AppDTO findAppByid(String uuid)	{
		return this.getHibernateTemplate().get(AppDTO.class, uuid);
	}
	
	public AppInstallPack findAppPkgByAppId(String appId, String os)	{
		String hql = " from AppInstallPack WHERE app.uuid = ? and os = ? ";
		List<AppInstallPack> list = this.getHibernateTemplate().find(hql, appId, os);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
	
	public List<Clientconfigdate> findCcDate(int provId, int smallId, int bidId)	{
		String hql = " from Clientconfigdate WHERE province.id = ? and ( clientconfigtype.id >= ?  and clientconfigtype.id <= ? ) ";
		return this.getHibernateTemplate().find(hql, provId, smallId, bidId);
	}
	
	public AppInstallPack findIOSAppPackByAppUuid(String appUuid){
		String hql = " FROM AppInstallPack WHERE appId = ? AND ipaItemId IS NOT NULL AND ipaItemId <> '' ";
		List<AppInstallPack> list = this.getHibernateTemplate().find(hql, appUuid);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
	
	public AppInstallPack findIOSAppPackByPkgUuid(String pkgUuid){
		String hql = " FROM AppInstallPack WHERE UUID = ? AND ipaItemId IS NOT NULL AND ipaItemId <> '' ";
		List<AppInstallPack> list = this.getHibernateTemplate().find(hql, pkgUuid);
		
		if(list.isEmpty()){
			hql = " FROM AppInstallPack WHERE UUID = ? ";
			list = this.getHibernateTemplate().find(hql, pkgUuid);
			AppInstallPack app = list.get(0);
			if(app != null && app.getIpaBundleId() != null && app.getIpaItemId() != "" ){
				hql = " FROM AppInstallPack ap WHERE ap.app.uuid = ? and ap.ipaBundleId = ? AND ap.ipaItemId IS NOT NULL AND ap.ipaItemId <> '' ";
				list = this.getHibernateTemplate().find(hql, app.getApp().getUuid(), app.getIpaBundleId());
				
				if(list.isEmpty()){
					System.out.println("没有找到对应的数据,参数如下：pkgUuid = " + pkgUuid + "    appId = " + app.getApp().getUuid() + "  IpaBundleId = " + app.getIpaBundleId());
					return null;
				}else{
					return list.get(0);
				}
			}else {
				return null;
			}
		}else{
			return list.get(0);
		}
	}
	
	public Pkg findPkgByPkgId(Integer pkgId){
		return this.getHibernateTemplate().get(Pkg.class, pkgId);
	}
	
	public List<UnionreadPkgView> findPkgViewByProvid(Integer provId){
		String hql = " FROM UnionreadPkgView WHERE provId = ? AND isshow = 1 GROUP BY pkgId ORDER BY id ";
		return this.getHibernateTemplate().find(hql, provId);
	}
	
	public List<UnionreadPkgView> findPackUuidByPkgId(Integer provId, String pkgId){
		String hql = " FROM UnionreadPkgView WHERE  provId = ? AND pkgId = ? ORDER BY id ";
		return this.getHibernateTemplate().find(hql, provId, pkgId);
	}
	
	public String findMarkerType(Integer provId, String packUuid){
		String hql = " FROM AppMarker WHERE provId = ? AND packUuid = ? "; //provid+packuuid是唯一索引
		List<AppMarker> list = this.getHibernateTemplate().find(hql, provId, packUuid);
		if(list.isEmpty()){
			return "";
		}else{
			return list.get(0).getMarkerType();
		}
	}

	public AppInstallPack findByYLAppId(String appid)	{
		String hql = " FROM AppInstallPack WHERE cpId = 'cp03' AND originalPackId LIKE '"+appid+"_%' ";
		List<AppInstallPack> list = this.getHibernateTemplate().find(hql);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public AppInstallPack findByAXONAppId(String appid)	{
		String hql = " FROM AppInstallPack WHERE cpId = 'CP04' AND originalPackId = '"+appid+"'";
		List<AppInstallPack> list = this.getHibernateTemplate().find(hql);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public List<AppInstallPack> findAXONApp()	{
		String hql = " FROM AppInstallPack WHERE cpId = 'CP04' LIMIT 10 ";
		List<AppInstallPack> list = this.getHibernateTemplate().find(hql);
		return list.isEmpty() ? null : list;
	}
}
