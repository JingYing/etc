package com.i8app.shop.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import com.i8app.shop.common.Util;
import com.i8app.shop.domain.Appleinfo;

@Repository
public class AppleInfoDao extends BaseDao	{
	
	public int updateAppleInfoUseNumber(int appleid){
		String sql = "update appleinfo set number = number - 1 where id = ?";
		int result = this.simpleJdbcTemplate.update(sql, appleid);
		if(result == 1) {
			System.out.println(Util.getNOWTIME()+"apple使用减小成功");
		} else {
			System.out.println(Util.getNOWTIME()+"apple使用减小异常");
		}
		return result;
	}
	
	public Appleinfo getAppleInfo(){
		String sql1 = "select id,mac,guid from appleinfo where number <2 order by number asc limit 1";
		Appleinfo apple = simpleJdbcTemplate.queryForObject(sql1, new ParameterizedRowMapper<Appleinfo>() {

			@Override
			public Appleinfo mapRow(ResultSet rs, int rowNum) throws SQLException {
				Appleinfo apple = new Appleinfo();
				apple.setId(rs.getInt(1));
				apple.setMac(rs.getString(2));
				apple.setGuid(rs.getString(3));
				return apple;
			}
		});
		
		String sql2 = "update appleinfo set number = number + 1 where id = ?";
		int result = simpleJdbcTemplate.update(sql2, apple.getId());
		if(result == 1) {
			System.out.println(Util.getNOWTIME()+"apple使用增加成功");
		} else {
			System.out.println(Util.getNOWTIME()+"apple使用增加异常");
		}
		return apple;
	}

}
