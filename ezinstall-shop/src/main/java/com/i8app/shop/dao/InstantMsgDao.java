package com.i8app.shop.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.common.Pager;
import com.i8app.shop.domain.InstantMsg;

@Repository
public class InstantMsgDao extends BaseDao<InstantMsg>	{
	
	/**
	 * 
	 * @param fromTime 自1970 年 1 月 1 日 开始的秒数, 非毫秒数
	 * @param areaCode
	 * @param queryCount
	 * @return
	 */
	public List<InstantMsg> find(long fromTime, String areaCode, Integer queryCount)	{
		fromTime = fromTime * 1000;	//把秒数转换为毫秒数
		String hql = "from InstantMsg where areaCode = ? and uploadTime >= ? order by uploadTime";
		if(queryCount != null)	{
			return this.queryPager(hql, new Object[]{areaCode, new Date(fromTime)}, 0, queryCount).getList();
		} else	{
			return this.getHibernateTemplate().find(hql, areaCode, new Date(fromTime));
		}
	}
	
	public List<InstantMsg> find(long fromTime, String areaCode)	{
		return this.find(fromTime, areaCode, null);
	}
	
	public Pager<InstantMsg> find(String areaCode, int offset, int pageSize)	{
		String hql = "from InstantMsg where areaCode = ? order by uploadTime desc";
		return this.queryPager(hql, new Object[]{areaCode}, offset, pageSize);
	}
	
}
