package com.i8app.shop.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.FaqType;
import com.i8app.shop.domain.Faqs;

@Repository
public class FaqsDao extends BaseDao<Faqs>{
	
	/**
	 * 根据ID查询FAQ
	 * @param id
	 * @return
	 */
	public Faqs findById(Integer id)	{
		return this.getHibernateTemplate().get(Faqs.class, id);
	}
	
	/**
	 * 根据TYPEID查询FAQTYPE
	 * @param typeId
	 * @return
	 */
	public FaqType findTypeById(Integer typeId)	{
		return this.getHibernateTemplate().get(FaqType.class, typeId);
	}
	
	
	/**
	 * 查询所有的FAQTYPE
	 * @return
	 */
	public List<FaqType> typeList()	{
		return this.getHibernateTemplate().loadAll(FaqType.class);
	}
	
	/**
	 * 根据员工ID, 搜索该员工提问过问题
	 * @param empId
	 * @return
	 */
	public List<Faqs> findByEmpId(Integer empId)	{
		String hql = "from Faqs f where f.emp.id = ?";
		return this.getHibernateTemplate().find(hql, empId);
	}

	/**
	 * 查询相关类型下的所有问题
	 * @param typeId
	 * @return
	 */
	public List<Faqs> listByType(int typeId)	{
		String hql = "from Faqs where faqType.id = ? and checked = 1";
		return this.getHibernateTemplate().find(hql, typeId);
	}
	
	/**
	 * 根据问题,搜索已经审核过的FAQ
	 * @param question
	 * @return
	 */
	public List<Faqs> findByQuestion(String question)	{
		String hql = "from Faqs where question like ? and checked = 1";
		return this.getHibernateTemplate().find(hql, "%" + question + "%");
	}

	
	/**
	 * 获取所有FAQ
	 * @return
	 */
	public Map<FaqType, List<Faqs>> getAllMap()	{
		Map<FaqType, List<Faqs>> map = new LinkedHashMap<FaqType, List<Faqs>>();
		for(FaqType f : typeList())	{
			map.put(f, this.listByType(f.getId()));
		}
		return map;
	}
	

	
}
