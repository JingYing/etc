package com.i8app.shop.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.Channeltype;
import com.i8app.shop.domain.Collaboratechannel;
import com.i8app.shop.domain.Reifiercollaboratechannel;

@Repository
public class BusinessHallDao extends BaseDao{

	public BusinessHall findById(int id)	{
		return this.getHibernateTemplate().get(BusinessHall.class, id);
	}
	
	public Channeltype findByCtid(int ctid)	{
		return this.getHibernateTemplate().get(Channeltype.class, ctid);
	}
	
	public Collaboratechannel findByCcid(int ccid)	{
		return this.getHibernateTemplate().get(Collaboratechannel.class, ccid);
	}
	
	public Reifiercollaboratechannel findByRccid(int rccid)	{
		return this.getHibernateTemplate().get(Reifiercollaboratechannel.class, rccid);
	}

	/**
	 * @param orgId
	 */
	public void updateDeployTime(Integer orgId) {
		//为了减少更新，所以要将这个代码屏蔽掉
//		String hql = "update BusinessHall set deployTime = ? where id = ? and deployTime is null";
//		getHibernateTemplate().bulkUpdate(hql, new Date(), orgId);
	}

	public BusinessHall findByNoAndProvId(String hallno, Integer provinceId) {
		String hql = "from BusinessHall where province.id = ? and hallNo = ?";
		List<BusinessHall> list = getHibernateTemplate().find(hql, provinceId, hallno);
		return list.isEmpty() ? null : list.get(0);
	}
	
	/**
	 * 贺芳容新增厅信息修改的相关方法
	 */
	@SuppressWarnings("unchecked")
	public List<Channeltype> findCtList(){
		return this.getHibernateTemplate().find(" from Channeltype ");
	}
	
	@SuppressWarnings("unchecked")
	public List<Collaboratechannel> findCcList(Integer ctid){
		return this.getHibernateTemplate().find(" from Collaboratechannel cc where cc.ctid.id = ?",ctid);
	}
	
	@SuppressWarnings("unchecked")
	public List<Reifiercollaboratechannel> findRccByccId(Integer ccid) {
		return getHibernateTemplate().find("from Reifiercollaboratechannel rcc where rcc.ccid.id=?",ccid);
	}
	
	public void addOrUpdateHall(BusinessHall hall) {
		this.getHibernateTemplate().saveOrUpdate(hall);
	}
	
}
