package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Terminal;

@Repository
public class TerminalDao extends BaseDao	{

	public void persist(Terminal terminal)	{
		getHibernateTemplate().persist(terminal);
	}

	public boolean isExist(String psn) {
		String hql = "select count(*) from Terminal where mac = ?";
		int i = ((Long)getHibernateTemplate().find(hql, psn).get(0)).intValue();
		return i==0 ? false : true;
	}
	
}
