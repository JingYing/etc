package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.AppActive;

@Repository
public class AppActiveDao extends BaseDao	{
	
	public AppActive find(int source, int appId)	{
		String hql = "from AppActive where source = ? and appId = ?";
		List<AppActive> list = this.getHibernateTemplate().find(hql, source, appId);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
	
	public void persist(List<AppActive> list)	{
		this.getHibernateTemplate().saveOrUpdateAll(list);
	}
}
