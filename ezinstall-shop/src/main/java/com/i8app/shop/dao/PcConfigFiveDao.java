package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Pcconfigfive;

@Repository
public class PcConfigFiveDao extends BaseDao {

	public Pcconfigfive getPcConfigFive(String areacode){
		return hibernateTemplate.get(Pcconfigfive.class, areacode);
	}
}
