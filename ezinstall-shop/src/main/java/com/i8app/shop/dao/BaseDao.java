package com.i8app.shop.dao;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.util.SerializationHelper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.i8app.shop.common.Pager;

public class BaseDao <T>	{

	@Resource
	protected HibernateTemplate hibernateTemplate;
	protected SimpleJdbcTemplate simpleJdbcTemplate;
	protected JdbcTemplate jdbcTemplate;
	
	/**
	 * 存入或更新
	 * @param t
	 */
	public void saveOrUpdate(T t)	{
		hibernateTemplate.saveOrUpdate(t);
	}
	
	public T merge(T t)	{
		return hibernateTemplate.merge(t);
	}
	
	public void persist(T t)	{
		hibernateTemplate.persist(t);
		hibernateTemplate.flush();
	}
	
	/**
	 * 存入或更新全部
	 * @param list
	 */
	public void saveOrUpdateAll(Collection<T> collection)	{
		hibernateTemplate.saveOrUpdateAll(collection);
	}

//	/***
//	 * 根据Criteria进行查询
//	 * @param criteria
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public List<T> findByCriteria(DetachedCriteria criteria)	{
//		return hibernateTemplate.findByCriteria(criteria);
//	}
//	
//	/**
//	 * 根据Criteria进行查询 , 得到分页结果
//	 * criteria中不能带distinct
//	 * @param criteria
//	 * @param offset 查询的起始位置
//	 * @param pageSize 每页多少条记录
//	 * @return
//	 */
//	public Pager<T> findByCriteria(DetachedCriteria criteria, int offset, int pageSize)	{
//		
//		DetachedCriteria dc = (DetachedCriteria)SerializationHelper.clone(criteria);
//		List<T> list = hibernateTemplate.findByCriteria(dc, offset, pageSize);
//		
//		criteria.setProjection(Projections.rowCount());
//		int count = (Integer)hibernateTemplate.findByCriteria(criteria).get(0);
//		
//		return new Pager<T>(list, count);
//	}
	
	/**
	 * 使用hql方式进行分页查询, 不带查询参数
	 * hql中不能带distinct
	 * @param hql hql查询语句
	 * @param offset 查询的起始位置
	 * @param pageSize 每页多少条
	 * @return Pager
	 */
	protected Pager<T> queryPager(final String hql ,final int offset, final int pageSize)	{
		return queryPager(hql, new Object[]{}, offset, pageSize);
	}

	
	/**
	 * hql分页查询, 使用?号式查询参数
	 * hql中不能带distinct
	 * @param hql
	 * @param values ?号所对应的值
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Pager<T> queryPager(final String hql, final Object[] values, final int offset, final int pageSize)	{
		
		if(hql.contains(" distinct"))
			throw new IllegalArgumentException("hql中不能包含distinct");

		List<T> list = hibernateTemplate.executeFind(new HibernateCallback(){

			@Override
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Query query = session.createQuery(hql);
				for(int i=0; i<values.length; i++)	{
					query.setParameter(i, values[i]);
				}
				return query.setFirstResult(offset)
						.setMaxResults(pageSize)
						.list();
			}
		});
		
		String hqlChanged = hql.replaceAll(".*from ", "select count(*) from ");	//以from为标记,将hql替换成记录数查询语句
		Long count = (Long)hibernateTemplate.find(hqlChanged, values).get(0);
		
		return new Pager<T>(list, count.intValue());
	}
	
	protected HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	
	@Resource
	public void setDataSource(DataSource dataSource){
		this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
}
