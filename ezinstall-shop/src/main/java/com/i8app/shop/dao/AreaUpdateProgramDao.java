package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class AreaUpdateProgramDao extends BaseDao	{

	public List<String> softVersion(String version, String type,
			String areaCode) {
		String sql = "select address from areaUpdateProgram "
				+ " where type = ? and areaCode = ? and id >=  "
				+ " (select id from areaUpdateProgram where versionvalue > ? and areaCode = ? and type = ? order by id limit 1) "
				+ " order by id asc";
		int i = versionStrToInt(version);
		List<String> list = jdbcTemplate.queryForList(sql, String.class, type, areaCode,
				i, areaCode, type);
		return list;
	}

	private static int versionStrToInt(String version) {
		String[] spiltvalue = version.split("\\.");
		if (spiltvalue.length == 4) {
			return Integer.parseInt(spiltvalue[0]) * 255 * 255 * 255
					+ Integer.parseInt(spiltvalue[1]) * 255 * 255
					+ Integer.parseInt(spiltvalue[2]) * 255
					+ Integer.parseInt(spiltvalue[3]);
		}
		return 1000000000;
	}
}
