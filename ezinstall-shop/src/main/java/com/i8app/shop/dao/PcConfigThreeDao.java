package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Pcconfigthree;

@Repository
public class PcConfigThreeDao extends BaseDao	{
	
	public Pcconfigthree getPcConfigThree(String areacode){
		return hibernateTemplate.get(com.i8app.shop.domain.Pcconfigthree.class, areacode);
	}
}
