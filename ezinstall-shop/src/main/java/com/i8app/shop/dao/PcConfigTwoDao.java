package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Pcconfigtwo;

@Repository
public class PcConfigTwoDao extends BaseDao{

	public Pcconfigtwo getPcConfigTwo(String areacode){
		return hibernateTemplate.get(Pcconfigtwo.class, areacode);
	}
}
