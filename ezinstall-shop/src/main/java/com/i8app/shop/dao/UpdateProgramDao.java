package com.i8app.shop.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Updateprogram;

@Repository
public class UpdateProgramDao extends BaseDao{
	
	public List<Updateprogram> softVersion(String version,String type) {
		String sql ="select address from UpdateProgram where type = ? and id >= (select id from UpdateProgram where versionvalue > ? and type = ? order by id limit 1) order by id  asc";
		return simpleJdbcTemplate.query(sql, new ParameterizedRowMapper<Updateprogram>() {

			@Override
			public Updateprogram mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				Updateprogram up = new Updateprogram();
				up.setAddress(rs.getString(1));
				return up;
			}
		}, type, versionStrToInt(version), type);
	}
	
	private static int versionStrToInt(String version) {
		String[] spiltvalue = version.split("\\.");
		if(spiltvalue.length == 4) {
			return Integer.parseInt(spiltvalue[0]) * 255 * 255 * 255 + Integer.parseInt(spiltvalue[1]) * 255 * 255 + Integer.parseInt(spiltvalue[2]) * 255 + Integer.parseInt(spiltvalue[3]);	
		}
		return 1000000000;
	}
}
