package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.WorkPortAccessLog;

@Repository
public class WorkPortAccessLogDao extends BaseDao<WorkPortAccessLog>{

	public void persist(WorkPortAccessLog log)	{
		this.getHibernateTemplate().persist(log);
	}
}
