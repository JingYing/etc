package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.AppInstallPack;


@Repository
public class AppInstallPackDao extends BaseDao	{

	public AppInstallPack findById(String uuid)	{
		return (AppInstallPack)getHibernateTemplate().get(AppInstallPack.class, uuid);
	}
}
