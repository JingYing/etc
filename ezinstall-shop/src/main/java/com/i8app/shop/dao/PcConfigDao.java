package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Pcconfig;

@Repository
public class PcConfigDao extends BaseDao	{
	
	public Pcconfig getPcConfig(String areacode){
		return hibernateTemplate.get(Pcconfig.class, areacode);
	}
}
