package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Province;

@Repository
public class ProvinceDao extends BaseDao	{
	
	public Province findById(int id)	{
		return this.getHibernateTemplate().get(Province.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Province> findProvince()	{
		return this.getHibernateTemplate().find(" from Province where id>35 and id < 68 ");
	}

	/**
	 * 根据bss省code查找
	 * @param bssProvCode BSS文件中定义的省代码, 10内蒙,11北京等
	 * @return
	 */
	public Province findByBssprov(String bssProvCode) {
		String hql = "from Province where provinceCode = ?";
		List<Province> list = getHibernateTemplate().find(hql, "bss_" + bssProvCode);
		return list.isEmpty() ? null : list.get(0);
	}
	
	/**
	 * 根据bss省code查找
	 * @param areacode BSS文件中定义的省代码, 1028 上海,1041 广东等
	 * @return
	 */
	public Province findByBssAreacode(String areacode) {
		String hql = "from Province where unionreadCode = ?";
		List<Province> list = getHibernateTemplate().find(hql, areacode);
		return list.isEmpty() ? null : list.get(0);
	}
	
}
