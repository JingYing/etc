package com.i8app.shop.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TempDriverDao extends BaseDao {

	public void save(String vid, String pid, String file)	{
		String sql = "INSERT INTO TEMPDRIVERINFO (VID, PID, httpPath) VALUES(?,?,?)";
		jdbcTemplate.update(sql, vid, pid, file);
	}
}
