package com.i8app.shop.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import com.i8app.shop.common.Util;
import com.i8app.shop.domain.Updatedriver;

@Repository
public class UpdateDriverDao extends BaseDao	{

	public List<Updatedriver> driverVersion(String version) {
		System.out.println("UpdatedrivervisitTime:" +Util.getNOWTIME());
		String sql ="select address,version from driverprogram where id >= " +
				"(select id from driverprogram where versionvalue > ? order by id asc limit 1) " +
				"order by id asc";
		return simpleJdbcTemplate.query(sql, new ParameterizedRowMapper<Updatedriver>() {

			@Override
			public Updatedriver mapRow(ResultSet rs, int rowNum) throws SQLException {
				Updatedriver up = new Updatedriver();
				up.setAddress(rs.getString(1));
				up.setVersion(rs.getString(2));
				return up;
			}
		}, versionStrToInt(version));
		
	}
	private static int versionStrToInt(String version) {
		String[] spiltvalue = version.split("\\.");
		if(spiltvalue.length == 4) {
			return Integer.parseInt(spiltvalue[0]) * 255 * 255 * 255 + Integer.parseInt(spiltvalue[1]) * 255 * 255 + Integer.parseInt(spiltvalue[2]) * 255 + Integer.parseInt(spiltvalue[3]);	
		}
		return 1000000000;
	}
}
