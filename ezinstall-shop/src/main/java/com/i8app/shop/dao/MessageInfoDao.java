package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Messageinfo;

@Repository
public class MessageInfoDao extends BaseDao {
	public void saveOrUpdate(Messageinfo messageinfo){
		getHibernateTemplate().saveOrUpdate(messageinfo);
	}
	public void del(int id){
		getHibernateTemplate().delete(getHibernateTemplate().get(Messageinfo.class, id));
	}
	public Messageinfo findMessById(int id){
		return getHibernateTemplate().get(Messageinfo.class, id);
	}
	public List<Messageinfo> findMessList(){
		return getHibernateTemplate().loadAll(Messageinfo.class);
	}
	
	
}
