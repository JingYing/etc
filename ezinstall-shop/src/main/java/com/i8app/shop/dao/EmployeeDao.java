package com.i8app.shop.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.common.NoSuchRecordException;
import com.i8app.shop.domain.Employee;

@Repository
public class EmployeeDao extends BaseDao<Employee>	{
	
	/**
	 * 根据id查询对应员工
	 * @param id
	 * @return
	 */
	public Employee findById(int id)	{
		return this.getHibernateTemplate().get(Employee.class, id);
	}
	
	/**
	 * 修改员工信息
	 */
	public void saveOrUpdate(Employee emp)	{
		this.getHibernateTemplate().saveOrUpdate(emp);
	}
	
	/**
	 * 按营业厅人员的工号进行查询
	 * @param empNo
	 * @return
	 */
	public Employee findByNo(String empNo)	{
		String hql = "from Employee where empNo = ? and orgLevel = 4 and isDeleted = ?";
		List<Employee> list = this.getHibernateTemplate().find(hql, empNo, 0);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
	
	public List<Employee> findByNoAndProvId(String empNo, int[] provIds)	{
		String hql = "from Employee where empNo = :empNo and isDeleted = :isDeleted and provId in (:provIds)";
		List<Integer> list = new ArrayList<Integer>();
		for(int i : provIds)	{
			list.add(i);
		}
		return getHibernateTemplate().findByNamedParam(hql, 
				new String[]{"empNo", "isDeleted", "provIds"}, new Object[]{empNo, 0, list});
	}
	
	/**
	 * 按营业厅人员的工号进行查询, 匹配工号的大小写
	 * @param empNo
	 * @return
	 */
	public Employee findByNoCaseSensitive(String empNo)	{
		String hql = "from Employee where empNo = binary(?)";
		List<Employee> list = this.getHibernateTemplate().find(hql, empNo);
		if(list.size() == 0)
			return null;
		else
			return list.get(0);
	}
	
	/**
	 * 更新手机号
	 * @param empId
	 * @param phoneNumber
	 */
	public void update(int empId, String phoneNumber, 
								String account, String user, String bank)	{
		Employee e = findById(empId);
		if(e == null)	{
			throw new NoSuchRecordException("对应员工id没找到:" + empId);
		}
		if(phoneNumber != null && !"".equals(phoneNumber))	{
			e.setPhoneNumber(phoneNumber);
		}
		if(account != null && !"".equals(account))	{
			e.setBankAccount(account);
		}
		if(user != null && !"".equals(user))	{
			e.setBankUser(user);
		}
		if(bank != null && !"".equals(bank))	{
			e.setBank(bank);
		}
		persist(e);
	}
	
	/**
	 * 根据员工工号和省id查询员工对象
	 * @param empNo
	 * @param proId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Employee findByNoAndProId(String empNo, Integer proid)	{
		String hql = "from Employee where empNo = ? and province.id = ? and orgLevel = 4 and isDeleted = ?";
		List<Employee> list = this.getHibernateTemplate().find(hql, empNo, proid, 0);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
}
