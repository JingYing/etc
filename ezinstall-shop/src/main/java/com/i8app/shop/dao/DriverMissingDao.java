package com.i8app.shop.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.DriverMissing;

@Repository
public class DriverMissingDao extends BaseDao {

	public void persist(DriverMissing driverMissing)	{
		String sql = "INSERT INTO DRIVERMISSING(MANU, PHONETYPE, INFO1, INFO2) VALUES(?,?,?,?)";
		jdbcTemplate.update(sql, driverMissing.getManu(), driverMissing.getPhoneType(), 
								driverMissing.getInfo1(), driverMissing.getInfo2());
	}
}
