package com.i8app.shop.domain;

import javax.persistence.*;

/**
 * <<驱动信息表>>的实体类
 *  表格式: id--主键
 *  vid--表示生产厂商, 一张表里面应该只有一个VID.
 *  pid--表示手机型号,所有PID以逗号隔开放在一条记录中
 *  filePath--表示驱动包的网络地址
 *  osType--表示操作系统类型
 * 
 * @author JingYing
 * 
 */
@Entity
@Table(name="driverinfo")
public class Driver {
	private Integer id;
	private String vid, pids, filePath;
	private Integer osType;

	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

	public String getVid() {
		return vid;
	}

	public void setVid(String vid) {
		this.vid = vid;
	}

	public String getPids() {
		return pids;
	}

	public void setPids(String pids) {
		this.pids = pids;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getOsType() {
		return osType;
	}

	public void setOsType(int osType) {
		this.osType = osType;
	}


	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	

}
