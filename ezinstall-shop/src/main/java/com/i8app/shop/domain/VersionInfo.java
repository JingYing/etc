package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Versioninfo entity. @author MyEclipse Persistence Tools
 */
@Entity
public class VersionInfo implements java.io.Serializable {

	// Fields

	private Integer versionId;
	private String versions;

	// Constructors

	/** default constructor */
	public VersionInfo() {
	}

	/** full constructor */
	public VersionInfo(String versions) {
		this.versions = versions;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "versionId", unique = true, nullable = false)
	public Integer getVersionId() {
		return this.versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	@Column(name = "versions", length = 420)
	public String getVersions() {
		return this.versions;
	}

	public void setVersions(String versions) {
		this.versions = versions;
	}

}