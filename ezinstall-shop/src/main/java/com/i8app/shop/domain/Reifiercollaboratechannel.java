package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Reifiercollaboratechannel {

	
	private Integer id;
	
	private String rccName;
	
	private String rccDesc;
	
	private Collaboratechannel ccid;

	@Id
	@GeneratedValue
	@Column(name="rccId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRccName() {
		return rccName;
	}

	public void setRccName(String rccName) {
		this.rccName = rccName;
	}

	public String getRccDesc() {
		return rccDesc;
	}

	public void setRccDesc(String rccDesc) {
		this.rccDesc = rccDesc;
	}

	@ManyToOne
	@JoinColumn(name="ccId")
	public Collaboratechannel getCcid() {
		return ccid;
	}

	public void setCcid(Collaboratechannel ccid) {
		this.ccid = ccid;
	}

}
