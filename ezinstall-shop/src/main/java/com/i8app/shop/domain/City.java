package com.i8app.shop.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "cityinfo")
public class City implements Serializable{
	private Integer id;
	private String name;
	private Province province;
	private Set<County> countySet;

	@Id
	@GeneratedValue
	@Column(name="cityId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="cityName")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne
	@JoinColumn(name="provinceId")
	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	@OneToMany(mappedBy="city")
	@OrderBy(value="id")
	@JoinColumn(name="cityId")
	public Set<County> getCountySet() {
		return countySet;
	}

	public void setCountySet(Set<County> countySet) {
		this.countySet = countySet;
	}

}
