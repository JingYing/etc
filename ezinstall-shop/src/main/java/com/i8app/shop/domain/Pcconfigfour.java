package com.i8app.shop.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
// default package



/**
 * Pcconfigfour entity. @author MyEclipse Persistence Tools
 */

@Entity
public class Pcconfigfour  implements java.io.Serializable {


    // Fields    

     private String areaCode;
     private String remoteadr;
     private Integer jifen;
     private Integer wushoujibb;
     private Integer yunyings;
     private Integer guanjiflag;
     private Integer qiangzhiflag;
     private Integer isyouxiaoqi;
     private Integer youxiaoqi;
     private Integer liantongtubiao;
     private String kuozhang1;
     private String kuozhang2;
     private String kuozhang3;
     private String kuozhang4;
     private String kuozhang5;
     private String kuozhang6;
     private String kuozhang7;
     private String kuozhang8;
     private String kuozhang9;
     private String kuozhang10;
     private String kuozhang11;
     private String kuozhang12;
     private String kuozhang13;
     private String kuozhang14;
     private String kuozhang15;
     private String kuozhang16;
     private String kuozhang17;
     private String kuozhang18;
     private String kuozhang19;
     private String kuozhang20;
     private String kuozhang21;
     private String kuozhang22;
     private String kuozhang23;
     private String kuozhang24;
     private String kuozhang25;
     private String kuozhang26;
     private String kuozhang27;
     private String kuozhang28;
     private String kuozhang29;
     private String kuozhang30;
     private String kuozhang31;
     private String kuozhang32;
     private String kuozhang33;
     private String kuozhang34;
     private String kuozhang35;
     private String kuozhang36;
     private String kuozhang37;
     private String kuozhang38;
     private String kuozhang39;
     private String kuozhang40;
     private String kuozhang41;
     private String kuozhang42;
     private String kuozhang43;
     private String kuozhang44;
     private String kuozhang45;
     private String kuozhang46;
     private String kuozhang47;
     private String kuozhang48;
     private String kuozhang49;
     private String kuozhang50;
     private String kuozhang51;
     private String kuozhang52;
     private String kuozhang53;
     private String kuozhang54;
     private String kuozhang55;
     private String kuozhang56;
     private String kuozhang57;
     private String kuozhang58;
     private String kuozhang59;
     private String kuozhang60;
     private String kuozhang61;
     private String kuozhang62;
     private String kuozhang63;
     private String kuozhang64;
     private String kuozhang65;
     private String kuozhang66;
     private String kuozhang67;
     private String kuozhang68;
     private String kuozhang69;
     private String kuozhang70;
     private String kuozhang71;
     private String kuozhang72;
     private String kuozhang73;
     private String kuozhang74;
     private String kuozhang75;
     private String kuozhang76;
     private String kuozhang77;
     private String kuozhang78;
     private String kuozhang79;
     private String kuozhang80;
     private String kuozhang81;
     private String kuozhang82;
     private String kuozhang83;
     private String kuozhang84;
     private String kuozhang85;
     private String kuozhang86;
     private String kuozhang87;
     private String kuozhang88;
     private String kuozhang89;
     private String kuozhang90;
     private String kuozhang91;
     private String kuozhang92;
     private String kuozhang93;
     private String kuozhang94;
     private String kuozhang95;
     private String kuozhang96;
     private String kuozhang97;
     private String kuozhang98;
     private String kuozhang99;
     private String kuozhang100;


    // Constructors

    /** default constructor */
    public Pcconfigfour() {
    }

	/** minimal constructor */
    public Pcconfigfour(String areaCode) {
        this.areaCode = areaCode;
    }
    
    /** full constructor */
    public Pcconfigfour(String areaCode, String remoteadr, Integer jifen, Integer wushoujibb, Integer yunyings, Integer guanjiflag, Integer qiangzhiflag, Integer isyouxiaoqi, Integer youxiaoqi, Integer liantongtubiao, String kuozhang1, String kuozhang2, String kuozhang3, String kuozhang4, String kuozhang5, String kuozhang6, String kuozhang7, String kuozhang8, String kuozhang9, String kuozhang10, String kuozhang11, String kuozhang12, String kuozhang13, String kuozhang14, String kuozhang15, String kuozhang16, String kuozhang17, String kuozhang18, String kuozhang19, String kuozhang20, String kuozhang21, String kuozhang22, String kuozhang23, String kuozhang24, String kuozhang25, String kuozhang26, String kuozhang27, String kuozhang28, String kuozhang29, String kuozhang30, String kuozhang31, String kuozhang32, String kuozhang33, String kuozhang34, String kuozhang35, String kuozhang36, String kuozhang37, String kuozhang38, String kuozhang39, String kuozhang40, String kuozhang41, String kuozhang42, String kuozhang43, String kuozhang44, String kuozhang45, String kuozhang46, String kuozhang47, String kuozhang48, String kuozhang49, String kuozhang50, String kuozhang51, String kuozhang52, String kuozhang53, String kuozhang54, String kuozhang55, String kuozhang56, String kuozhang57, String kuozhang58, String kuozhang59, String kuozhang60, String kuozhang61, String kuozhang62, String kuozhang63, String kuozhang64, String kuozhang65, String kuozhang66, String kuozhang67, String kuozhang68, String kuozhang69, String kuozhang70, String kuozhang71, String kuozhang72, String kuozhang73, String kuozhang74, String kuozhang75, String kuozhang76, String kuozhang77, String kuozhang78, String kuozhang79, String kuozhang80, String kuozhang81, String kuozhang82, String kuozhang83, String kuozhang84, String kuozhang85, String kuozhang86, String kuozhang87, String kuozhang88, String kuozhang89, String kuozhang90, String kuozhang91, String kuozhang92, String kuozhang93, String kuozhang94, String kuozhang95, String kuozhang96, String kuozhang97, String kuozhang98, String kuozhang99, String kuozhang100) {
        this.areaCode = areaCode;
        this.remoteadr = remoteadr;
        this.jifen = jifen;
        this.wushoujibb = wushoujibb;
        this.yunyings = yunyings;
        this.guanjiflag = guanjiflag;
        this.qiangzhiflag = qiangzhiflag;
        this.isyouxiaoqi = isyouxiaoqi;
        this.youxiaoqi = youxiaoqi;
        this.liantongtubiao = liantongtubiao;
        this.kuozhang1 = kuozhang1;
        this.kuozhang2 = kuozhang2;
        this.kuozhang3 = kuozhang3;
        this.kuozhang4 = kuozhang4;
        this.kuozhang5 = kuozhang5;
        this.kuozhang6 = kuozhang6;
        this.kuozhang7 = kuozhang7;
        this.kuozhang8 = kuozhang8;
        this.kuozhang9 = kuozhang9;
        this.kuozhang10 = kuozhang10;
        this.kuozhang11 = kuozhang11;
        this.kuozhang12 = kuozhang12;
        this.kuozhang13 = kuozhang13;
        this.kuozhang14 = kuozhang14;
        this.kuozhang15 = kuozhang15;
        this.kuozhang16 = kuozhang16;
        this.kuozhang17 = kuozhang17;
        this.kuozhang18 = kuozhang18;
        this.kuozhang19 = kuozhang19;
        this.kuozhang20 = kuozhang20;
        this.kuozhang21 = kuozhang21;
        this.kuozhang22 = kuozhang22;
        this.kuozhang23 = kuozhang23;
        this.kuozhang24 = kuozhang24;
        this.kuozhang25 = kuozhang25;
        this.kuozhang26 = kuozhang26;
        this.kuozhang27 = kuozhang27;
        this.kuozhang28 = kuozhang28;
        this.kuozhang29 = kuozhang29;
        this.kuozhang30 = kuozhang30;
        this.kuozhang31 = kuozhang31;
        this.kuozhang32 = kuozhang32;
        this.kuozhang33 = kuozhang33;
        this.kuozhang34 = kuozhang34;
        this.kuozhang35 = kuozhang35;
        this.kuozhang36 = kuozhang36;
        this.kuozhang37 = kuozhang37;
        this.kuozhang38 = kuozhang38;
        this.kuozhang39 = kuozhang39;
        this.kuozhang40 = kuozhang40;
        this.kuozhang41 = kuozhang41;
        this.kuozhang42 = kuozhang42;
        this.kuozhang43 = kuozhang43;
        this.kuozhang44 = kuozhang44;
        this.kuozhang45 = kuozhang45;
        this.kuozhang46 = kuozhang46;
        this.kuozhang47 = kuozhang47;
        this.kuozhang48 = kuozhang48;
        this.kuozhang49 = kuozhang49;
        this.kuozhang50 = kuozhang50;
        this.kuozhang51 = kuozhang51;
        this.kuozhang52 = kuozhang52;
        this.kuozhang53 = kuozhang53;
        this.kuozhang54 = kuozhang54;
        this.kuozhang55 = kuozhang55;
        this.kuozhang56 = kuozhang56;
        this.kuozhang57 = kuozhang57;
        this.kuozhang58 = kuozhang58;
        this.kuozhang59 = kuozhang59;
        this.kuozhang60 = kuozhang60;
        this.kuozhang61 = kuozhang61;
        this.kuozhang62 = kuozhang62;
        this.kuozhang63 = kuozhang63;
        this.kuozhang64 = kuozhang64;
        this.kuozhang65 = kuozhang65;
        this.kuozhang66 = kuozhang66;
        this.kuozhang67 = kuozhang67;
        this.kuozhang68 = kuozhang68;
        this.kuozhang69 = kuozhang69;
        this.kuozhang70 = kuozhang70;
        this.kuozhang71 = kuozhang71;
        this.kuozhang72 = kuozhang72;
        this.kuozhang73 = kuozhang73;
        this.kuozhang74 = kuozhang74;
        this.kuozhang75 = kuozhang75;
        this.kuozhang76 = kuozhang76;
        this.kuozhang77 = kuozhang77;
        this.kuozhang78 = kuozhang78;
        this.kuozhang79 = kuozhang79;
        this.kuozhang80 = kuozhang80;
        this.kuozhang81 = kuozhang81;
        this.kuozhang82 = kuozhang82;
        this.kuozhang83 = kuozhang83;
        this.kuozhang84 = kuozhang84;
        this.kuozhang85 = kuozhang85;
        this.kuozhang86 = kuozhang86;
        this.kuozhang87 = kuozhang87;
        this.kuozhang88 = kuozhang88;
        this.kuozhang89 = kuozhang89;
        this.kuozhang90 = kuozhang90;
        this.kuozhang91 = kuozhang91;
        this.kuozhang92 = kuozhang92;
        this.kuozhang93 = kuozhang93;
        this.kuozhang94 = kuozhang94;
        this.kuozhang95 = kuozhang95;
        this.kuozhang96 = kuozhang96;
        this.kuozhang97 = kuozhang97;
        this.kuozhang98 = kuozhang98;
        this.kuozhang99 = kuozhang99;
        this.kuozhang100 = kuozhang100;
    }

   
    // Property accessors

    @Id
    public String getAreaCode() {
        return this.areaCode;
    }
    
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getRemoteadr() {
        return this.remoteadr;
    }
    
    public void setRemoteadr(String remoteadr) {
        this.remoteadr = remoteadr;
    }

    public Integer getJifen() {
        return this.jifen;
    }
    
    public void setJifen(Integer jifen) {
        this.jifen = jifen;
    }

    public Integer getWushoujibb() {
        return this.wushoujibb;
    }
    
    public void setWushoujibb(Integer wushoujibb) {
        this.wushoujibb = wushoujibb;
    }

    public Integer getYunyings() {
        return this.yunyings;
    }
    
    public void setYunyings(Integer yunyings) {
        this.yunyings = yunyings;
    }

    public Integer getGuanjiflag() {
        return this.guanjiflag;
    }
    
    public void setGuanjiflag(Integer guanjiflag) {
        this.guanjiflag = guanjiflag;
    }

    public Integer getQiangzhiflag() {
        return this.qiangzhiflag;
    }
    
    public void setQiangzhiflag(Integer qiangzhiflag) {
        this.qiangzhiflag = qiangzhiflag;
    }

    public Integer getIsyouxiaoqi() {
        return this.isyouxiaoqi;
    }
    
    public void setIsyouxiaoqi(Integer isyouxiaoqi) {
        this.isyouxiaoqi = isyouxiaoqi;
    }

    public Integer getYouxiaoqi() {
        return this.youxiaoqi;
    }
    
    public void setYouxiaoqi(Integer youxiaoqi) {
        this.youxiaoqi = youxiaoqi;
    }

    public Integer getLiantongtubiao() {
        return this.liantongtubiao;
    }
    
    public void setLiantongtubiao(Integer liantongtubiao) {
        this.liantongtubiao = liantongtubiao;
    }

    public String getKuozhang1() {
        return this.kuozhang1;
    }
    
    public void setKuozhang1(String kuozhang1) {
        this.kuozhang1 = kuozhang1;
    }

    public String getKuozhang2() {
        return this.kuozhang2;
    }
    
    public void setKuozhang2(String kuozhang2) {
        this.kuozhang2 = kuozhang2;
    }

    public String getKuozhang3() {
        return this.kuozhang3;
    }
    
    public void setKuozhang3(String kuozhang3) {
        this.kuozhang3 = kuozhang3;
    }

    public String getKuozhang4() {
        return this.kuozhang4;
    }
    
    public void setKuozhang4(String kuozhang4) {
        this.kuozhang4 = kuozhang4;
    }

    public String getKuozhang5() {
        return this.kuozhang5;
    }
    
    public void setKuozhang5(String kuozhang5) {
        this.kuozhang5 = kuozhang5;
    }

    public String getKuozhang6() {
        return this.kuozhang6;
    }
    
    public void setKuozhang6(String kuozhang6) {
        this.kuozhang6 = kuozhang6;
    }

    public String getKuozhang7() {
        return this.kuozhang7;
    }
    
    public void setKuozhang7(String kuozhang7) {
        this.kuozhang7 = kuozhang7;
    }

    public String getKuozhang8() {
        return this.kuozhang8;
    }
    
    public void setKuozhang8(String kuozhang8) {
        this.kuozhang8 = kuozhang8;
    }

    public String getKuozhang9() {
        return this.kuozhang9;
    }
    
    public void setKuozhang9(String kuozhang9) {
        this.kuozhang9 = kuozhang9;
    }

    public String getKuozhang10() {
        return this.kuozhang10;
    }
    
    public void setKuozhang10(String kuozhang10) {
        this.kuozhang10 = kuozhang10;
    }

    public String getKuozhang11() {
        return this.kuozhang11;
    }
    
    public void setKuozhang11(String kuozhang11) {
        this.kuozhang11 = kuozhang11;
    }

    public String getKuozhang12() {
        return this.kuozhang12;
    }
    
    public void setKuozhang12(String kuozhang12) {
        this.kuozhang12 = kuozhang12;
    }

    public String getKuozhang13() {
        return this.kuozhang13;
    }
    
    public void setKuozhang13(String kuozhang13) {
        this.kuozhang13 = kuozhang13;
    }

    public String getKuozhang14() {
        return this.kuozhang14;
    }
    
    public void setKuozhang14(String kuozhang14) {
        this.kuozhang14 = kuozhang14;
    }

    public String getKuozhang15() {
        return this.kuozhang15;
    }
    
    public void setKuozhang15(String kuozhang15) {
        this.kuozhang15 = kuozhang15;
    }

    public String getKuozhang16() {
        return this.kuozhang16;
    }
    
    public void setKuozhang16(String kuozhang16) {
        this.kuozhang16 = kuozhang16;
    }

    public String getKuozhang17() {
        return this.kuozhang17;
    }
    
    public void setKuozhang17(String kuozhang17) {
        this.kuozhang17 = kuozhang17;
    }

    public String getKuozhang18() {
        return this.kuozhang18;
    }
    
    public void setKuozhang18(String kuozhang18) {
        this.kuozhang18 = kuozhang18;
    }

    public String getKuozhang19() {
        return this.kuozhang19;
    }
    
    public void setKuozhang19(String kuozhang19) {
        this.kuozhang19 = kuozhang19;
    }

    public String getKuozhang20() {
        return this.kuozhang20;
    }
    
    public void setKuozhang20(String kuozhang20) {
        this.kuozhang20 = kuozhang20;
    }

    public String getKuozhang21() {
        return this.kuozhang21;
    }
    
    public void setKuozhang21(String kuozhang21) {
        this.kuozhang21 = kuozhang21;
    }

    public String getKuozhang22() {
        return this.kuozhang22;
    }
    
    public void setKuozhang22(String kuozhang22) {
        this.kuozhang22 = kuozhang22;
    }

    public String getKuozhang23() {
        return this.kuozhang23;
    }
    
    public void setKuozhang23(String kuozhang23) {
        this.kuozhang23 = kuozhang23;
    }

    public String getKuozhang24() {
        return this.kuozhang24;
    }
    
    public void setKuozhang24(String kuozhang24) {
        this.kuozhang24 = kuozhang24;
    }

    public String getKuozhang25() {
        return this.kuozhang25;
    }
    
    public void setKuozhang25(String kuozhang25) {
        this.kuozhang25 = kuozhang25;
    }

    public String getKuozhang26() {
        return this.kuozhang26;
    }
    
    public void setKuozhang26(String kuozhang26) {
        this.kuozhang26 = kuozhang26;
    }

    public String getKuozhang27() {
        return this.kuozhang27;
    }
    
    public void setKuozhang27(String kuozhang27) {
        this.kuozhang27 = kuozhang27;
    }

    public String getKuozhang28() {
        return this.kuozhang28;
    }
    
    public void setKuozhang28(String kuozhang28) {
        this.kuozhang28 = kuozhang28;
    }

    public String getKuozhang29() {
        return this.kuozhang29;
    }
    
    public void setKuozhang29(String kuozhang29) {
        this.kuozhang29 = kuozhang29;
    }

    public String getKuozhang30() {
        return this.kuozhang30;
    }
    
    public void setKuozhang30(String kuozhang30) {
        this.kuozhang30 = kuozhang30;
    }


    public String getKuozhang33() {
        return this.kuozhang33;
    }
    
    public void setKuozhang33(String kuozhang33) {
        this.kuozhang33 = kuozhang33;
    }

    public String getKuozhang34() {
        return this.kuozhang34;
    }
    
    public void setKuozhang34(String kuozhang34) {
        this.kuozhang34 = kuozhang34;
    }

    public String getKuozhang35() {
        return this.kuozhang35;
    }
    
    public void setKuozhang35(String kuozhang35) {
        this.kuozhang35 = kuozhang35;
    }

    public String getKuozhang36() {
        return this.kuozhang36;
    }
    
    public void setKuozhang36(String kuozhang36) {
        this.kuozhang36 = kuozhang36;
    }

    public String getKuozhang37() {
        return this.kuozhang37;
    }
    
    public void setKuozhang37(String kuozhang37) {
        this.kuozhang37 = kuozhang37;
    }

    public String getKuozhang38() {
        return this.kuozhang38;
    }
    
    public void setKuozhang38(String kuozhang38) {
        this.kuozhang38 = kuozhang38;
    }

    public String getKuozhang39() {
        return this.kuozhang39;
    }
    
    public void setKuozhang39(String kuozhang39) {
        this.kuozhang39 = kuozhang39;
    }

    public String getKuozhang40() {
        return this.kuozhang40;
    }
    
    public void setKuozhang40(String kuozhang40) {
        this.kuozhang40 = kuozhang40;
    }

    public String getKuozhang41() {
        return this.kuozhang41;
    }
    
    public void setKuozhang41(String kuozhang41) {
        this.kuozhang41 = kuozhang41;
    }

    public String getKuozhang42() {
        return this.kuozhang42;
    }
    
    public void setKuozhang42(String kuozhang42) {
        this.kuozhang42 = kuozhang42;
    }

    public String getKuozhang43() {
        return this.kuozhang43;
    }
    
    public void setKuozhang43(String kuozhang43) {
        this.kuozhang43 = kuozhang43;
    }

    public String getKuozhang44() {
        return this.kuozhang44;
    }
    
    public void setKuozhang44(String kuozhang44) {
        this.kuozhang44 = kuozhang44;
    }

    public String getKuozhang45() {
        return this.kuozhang45;
    }
    
    public void setKuozhang45(String kuozhang45) {
        this.kuozhang45 = kuozhang45;
    }

    public String getKuozhang46() {
        return this.kuozhang46;
    }
    
    public void setKuozhang46(String kuozhang46) {
        this.kuozhang46 = kuozhang46;
    }

    public String getKuozhang47() {
        return this.kuozhang47;
    }
    
    public void setKuozhang47(String kuozhang47) {
        this.kuozhang47 = kuozhang47;
    }

    public String getKuozhang48() {
        return this.kuozhang48;
    }
    
    public void setKuozhang48(String kuozhang48) {
        this.kuozhang48 = kuozhang48;
    }

    public String getKuozhang49() {
        return this.kuozhang49;
    }
    
    public void setKuozhang49(String kuozhang49) {
        this.kuozhang49 = kuozhang49;
    }

    public String getKuozhang50() {
        return this.kuozhang50;
    }
    
    public void setKuozhang50(String kuozhang50) {
        this.kuozhang50 = kuozhang50;
    }

    public String getKuozhang51() {
        return this.kuozhang51;
    }
    
    public void setKuozhang51(String kuozhang51) {
        this.kuozhang51 = kuozhang51;
    }

    public String getKuozhang52() {
        return this.kuozhang52;
    }
    
    public void setKuozhang52(String kuozhang52) {
        this.kuozhang52 = kuozhang52;
    }

    public String getKuozhang53() {
        return this.kuozhang53;
    }
    
    public void setKuozhang53(String kuozhang53) {
        this.kuozhang53 = kuozhang53;
    }

    public String getKuozhang54() {
        return this.kuozhang54;
    }
    
    public void setKuozhang54(String kuozhang54) {
        this.kuozhang54 = kuozhang54;
    }

    public String getKuozhang55() {
        return this.kuozhang55;
    }
    
    public void setKuozhang55(String kuozhang55) {
        this.kuozhang55 = kuozhang55;
    }

    public String getKuozhang56() {
        return this.kuozhang56;
    }
    
    public void setKuozhang56(String kuozhang56) {
        this.kuozhang56 = kuozhang56;
    }

    public String getKuozhang57() {
        return this.kuozhang57;
    }
    
    public void setKuozhang57(String kuozhang57) {
        this.kuozhang57 = kuozhang57;
    }

    public String getKuozhang58() {
        return this.kuozhang58;
    }
    
    public void setKuozhang58(String kuozhang58) {
        this.kuozhang58 = kuozhang58;
    }

    public String getKuozhang59() {
        return this.kuozhang59;
    }
    
    public void setKuozhang59(String kuozhang59) {
        this.kuozhang59 = kuozhang59;
    }

    public String getKuozhang60() {
        return this.kuozhang60;
    }
    
    public void setKuozhang60(String kuozhang60) {
        this.kuozhang60 = kuozhang60;
    }

    public String getKuozhang61() {
        return this.kuozhang61;
    }
    
    public void setKuozhang61(String kuozhang61) {
        this.kuozhang61 = kuozhang61;
    }

    public String getKuozhang62() {
        return this.kuozhang62;
    }
    
    public void setKuozhang62(String kuozhang62) {
        this.kuozhang62 = kuozhang62;
    }

    public String getKuozhang63() {
        return this.kuozhang63;
    }
    
    public void setKuozhang63(String kuozhang63) {
        this.kuozhang63 = kuozhang63;
    }

    public String getKuozhang64() {
        return this.kuozhang64;
    }
    
    public void setKuozhang64(String kuozhang64) {
        this.kuozhang64 = kuozhang64;
    }

    public String getKuozhang65() {
        return this.kuozhang65;
    }
    
    public void setKuozhang65(String kuozhang65) {
        this.kuozhang65 = kuozhang65;
    }

    public String getKuozhang66() {
        return this.kuozhang66;
    }
    
    public void setKuozhang66(String kuozhang66) {
        this.kuozhang66 = kuozhang66;
    }

    public String getKuozhang67() {
        return this.kuozhang67;
    }
    
    public void setKuozhang67(String kuozhang67) {
        this.kuozhang67 = kuozhang67;
    }

    public String getKuozhang68() {
        return this.kuozhang68;
    }
    
    public void setKuozhang68(String kuozhang68) {
        this.kuozhang68 = kuozhang68;
    }

    public String getKuozhang69() {
        return this.kuozhang69;
    }
    
    public void setKuozhang69(String kuozhang69) {
        this.kuozhang69 = kuozhang69;
    }

    public String getKuozhang70() {
        return this.kuozhang70;
    }
    
    public void setKuozhang70(String kuozhang70) {
        this.kuozhang70 = kuozhang70;
    }

    public String getKuozhang71() {
        return this.kuozhang71;
    }
    
    public void setKuozhang71(String kuozhang71) {
        this.kuozhang71 = kuozhang71;
    }

    public String getKuozhang72() {
        return this.kuozhang72;
    }
    
    public void setKuozhang72(String kuozhang72) {
        this.kuozhang72 = kuozhang72;
    }

    public String getKuozhang73() {
        return this.kuozhang73;
    }
    
    public void setKuozhang73(String kuozhang73) {
        this.kuozhang73 = kuozhang73;
    }

    public String getKuozhang74() {
        return this.kuozhang74;
    }
    
    public void setKuozhang74(String kuozhang74) {
        this.kuozhang74 = kuozhang74;
    }

    public String getKuozhang75() {
        return this.kuozhang75;
    }
    
    public void setKuozhang75(String kuozhang75) {
        this.kuozhang75 = kuozhang75;
    }

    public String getKuozhang76() {
        return this.kuozhang76;
    }
    
    public void setKuozhang76(String kuozhang76) {
        this.kuozhang76 = kuozhang76;
    }

    public String getKuozhang77() {
        return this.kuozhang77;
    }
    
    public void setKuozhang77(String kuozhang77) {
        this.kuozhang77 = kuozhang77;
    }

    public String getKuozhang78() {
        return this.kuozhang78;
    }
    
    public void setKuozhang78(String kuozhang78) {
        this.kuozhang78 = kuozhang78;
    }

    public String getKuozhang79() {
        return this.kuozhang79;
    }
    
    public void setKuozhang79(String kuozhang79) {
        this.kuozhang79 = kuozhang79;
    }

    public String getKuozhang80() {
        return this.kuozhang80;
    }
    
    public void setKuozhang80(String kuozhang80) {
        this.kuozhang80 = kuozhang80;
    }

    public String getKuozhang81() {
        return this.kuozhang81;
    }
    
    public void setKuozhang81(String kuozhang81) {
        this.kuozhang81 = kuozhang81;
    }

    public String getKuozhang82() {
        return this.kuozhang82;
    }
    
    public void setKuozhang82(String kuozhang82) {
        this.kuozhang82 = kuozhang82;
    }

    public String getKuozhang83() {
        return this.kuozhang83;
    }
    
    public void setKuozhang83(String kuozhang83) {
        this.kuozhang83 = kuozhang83;
    }

    public String getKuozhang84() {
        return this.kuozhang84;
    }
    
    public void setKuozhang84(String kuozhang84) {
        this.kuozhang84 = kuozhang84;
    }

    public String getKuozhang85() {
        return this.kuozhang85;
    }
    
    public void setKuozhang85(String kuozhang85) {
        this.kuozhang85 = kuozhang85;
    }

    public String getKuozhang86() {
        return this.kuozhang86;
    }
    
    public void setKuozhang86(String kuozhang86) {
        this.kuozhang86 = kuozhang86;
    }

    public String getKuozhang87() {
        return this.kuozhang87;
    }
    
    public void setKuozhang87(String kuozhang87) {
        this.kuozhang87 = kuozhang87;
    }

    public String getKuozhang88() {
        return this.kuozhang88;
    }
    
    public void setKuozhang88(String kuozhang88) {
        this.kuozhang88 = kuozhang88;
    }

    public String getKuozhang89() {
        return this.kuozhang89;
    }
    
    public void setKuozhang89(String kuozhang89) {
        this.kuozhang89 = kuozhang89;
    }

    public String getKuozhang90() {
        return this.kuozhang90;
    }
    
    public void setKuozhang90(String kuozhang90) {
        this.kuozhang90 = kuozhang90;
    }

    public String getKuozhang91() {
        return this.kuozhang91;
    }
    
    public void setKuozhang91(String kuozhang91) {
        this.kuozhang91 = kuozhang91;
    }

    public String getKuozhang92() {
        return this.kuozhang92;
    }
    
    public void setKuozhang92(String kuozhang92) {
        this.kuozhang92 = kuozhang92;
    }

    public String getKuozhang93() {
        return this.kuozhang93;
    }
    
    public void setKuozhang93(String kuozhang93) {
        this.kuozhang93 = kuozhang93;
    }

    public String getKuozhang94() {
        return this.kuozhang94;
    }
    
    public void setKuozhang94(String kuozhang94) {
        this.kuozhang94 = kuozhang94;
    }

    public String getKuozhang95() {
        return this.kuozhang95;
    }
    
    public void setKuozhang95(String kuozhang95) {
        this.kuozhang95 = kuozhang95;
    }

    public String getKuozhang96() {
        return this.kuozhang96;
    }
    
    public void setKuozhang96(String kuozhang96) {
        this.kuozhang96 = kuozhang96;
    }

    public String getKuozhang97() {
        return this.kuozhang97;
    }
    
    public void setKuozhang97(String kuozhang97) {
        this.kuozhang97 = kuozhang97;
    }

    public String getKuozhang98() {
        return this.kuozhang98;
    }
    
    public void setKuozhang98(String kuozhang98) {
        this.kuozhang98 = kuozhang98;
    }

    public String getKuozhang99() {
        return this.kuozhang99;
    }
    
    public void setKuozhang99(String kuozhang99) {
        this.kuozhang99 = kuozhang99;
    }

    public String getKuozhang100() {
        return this.kuozhang100;
    }
    
    public void setKuozhang100(String kuozhang100) {
        this.kuozhang100 = kuozhang100;
    }

	public String getKuozhang31() {
		return kuozhang31;
	}

	public void setKuozhang31(String kuozhang31) {
		this.kuozhang31 = kuozhang31;
	}

	public String getKuozhang32() {
		return kuozhang32;
	}

	public void setKuozhang32(String kuozhang32) {
		this.kuozhang32 = kuozhang32;
	}
   








}