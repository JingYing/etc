package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clientconfigdate")
public class Clientconfigdate {

	private String id;
	private Province province;
	private Clientconfigtype clientconfigtype;
	private String content;

	@Id
	@GeneratedValue
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="provinceId")
	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	@ManyToOne
	@JoinColumn(name="cctid")
	public Clientconfigtype getClientconfigtype() {
		return clientconfigtype;
	}

	public void setClientconfigtype(Clientconfigtype clientconfigtype) {
		this.clientconfigtype = clientconfigtype;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
	
}
