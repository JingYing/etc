package com.i8app.shop.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class TypeInfo {
	private Integer id;
	private String typeName;
	private ManuInfo manuInfo;
	private Date onlineTime;
	private OsInfo osInfo;
	private String beizhu;
	private String bigPic;
	private String compressPic;
	private String resolution;
	private Integer hotOrder;
	private String firstLetter;
	private Integer osTypeId;	//是OSTYPEINFO表的主键
	private String networkModel;
	private String contractPlanPic;
	
	public TypeInfo() {	}

	public TypeInfo(Integer id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	@Column(name="typeId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="manuId")
	public ManuInfo getManuInfo() {
		return manuInfo;
	}

	public void setManuInfo(ManuInfo manuInfo) {
		this.manuInfo = manuInfo;
	}

	@Temporal(TemporalType.DATE)
	public Date getOnlineTime() {
		return onlineTime;
	}

	public void setOnlineTime(Date onlineTime) {
		this.onlineTime = onlineTime;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="osId")
	public OsInfo getOsInfo() {
		return osInfo;
	}

	public void setOsInfo(OsInfo osInfo) {
		this.osInfo = osInfo;
	}

	public String getBeizhu() {
		return beizhu;
	}

	public void setBeizhu(String beizhu) {
		this.beizhu = beizhu;
	}

	@Column(length=80)
	public String getBigPic() {
		return bigPic;
	}

	public void setBigPic(String bigPic) {
		this.bigPic = bigPic;
	}

	@Column(length=80)
	public String getCompressPic() {
		return compressPic;
	}

	public void setCompressPic(String compressPic) {
		this.compressPic = compressPic;
	}

	@Column(length=80)
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public Integer getHotOrder() {
		return hotOrder;
	}

	public void setHotOrder(Integer hotOrder) {
		this.hotOrder = hotOrder;
	}

	@Column(length=50)
	public String getFirstLetter() {
		return firstLetter;
	}

	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

	public Integer getOsTypeId() {
		return osTypeId;
	}

	public void setOsTypeId(Integer osTypeId) {
		this.osTypeId = osTypeId;
	}

	@Column(length=50)
	public String getNetworkModel() {
		return networkModel;
	}

	public void setNetworkModel(String networkModel) {
		this.networkModel = networkModel;
	}

	public String getContractPlanPic() {
		return contractPlanPic;
	}

	public void setContractPlanPic(String contractPlanPic) {
		this.contractPlanPic = contractPlanPic;
	}

}
