package com.i8app.shop.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "biz_area")
public class BizArea {

	private Integer id;
	private PkgGroup pkgGroup;
	private Biz biz;
	private String deptId;
	private Integer provId, cityId;

	@Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="pkgGroupId")
	public PkgGroup getPkgGroup() {
		return pkgGroup;
	}

	public void setPkgGroup(PkgGroup pkgGroup) {
		this.pkgGroup = pkgGroup;
	}
	
	@ManyToOne
	@JoinColumn(name="bizId")
	public Biz getBiz() {
		return biz;
	}

	public void setBiz(Biz biz) {
		this.biz = biz;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public Integer getProvId() {
		return provId;
	}

	public void setProvId(Integer provId) {
		this.provId = provId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

}
