package com.i8app.shop.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Clientdeployinfo entity. @author MyEclipse Persistence Tools
 */
@Entity
public class ClientDeployInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6349367760831189235L;
	// Fields
	private Integer id;
	private String psn;
	private String activedtime;
	private Integer hallId;
	private String hallName;
	private String ldcversion;
	private String versions;

	// Constructors

	/** default constructor */
	public ClientDeployInfo() {
	}

	/** full constructor */
	public ClientDeployInfo(String psn, String activedtime, Integer hallId,
			String hallName, String ldcversion, String versions) {
		this.psn = psn;
		this.activedtime = activedtime;
		this.hallId = hallId;
		this.hallName = hallName;
		this.ldcversion = ldcversion;
		this.versions = versions;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "psn", length = 30)
	public String getPsn() {
		return this.psn;
	}

	public void setPsn(String psn) {
		this.psn = psn;
	}

	@Column(name = "activedtime", length = 30)
	public String getActivedtime() {
		return this.activedtime;
	}

	public void setActivedtime(String activedtime) {
		this.activedtime = activedtime;
	}

	@Column(name = "hallId")
	public Integer getHallId() {
		return this.hallId;
	}

	public void setHallId(Integer hallId) {
		this.hallId = hallId;
	}

	@Column(name = "hallName", length = 128)
	public String getHallName() {
		return this.hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	@Column(name = "LDCVersion", length = 20)
	public String getLdcversion() {
		return this.ldcversion;
	}

	public void setLdcversion(String ldcversion) {
		this.ldcversion = ldcversion;
	}

	@Column(name = "versions", length = 400)
	public String getVersions() {
		return this.versions;
	}

	public void setVersions(String versions) {
		this.versions = versions;
	}

}