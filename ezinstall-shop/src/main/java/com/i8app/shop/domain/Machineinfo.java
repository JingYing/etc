package com.i8app.shop.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Machineinfo entity.
 * 
 * @author MyEclipse Persistence Tools
 */

@Entity
public class Machineinfo implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String psn;
	private String machineCode;
	private String sn;
	private String machineName;
	private String diskSerial;
	private String macAddr;
	private String ip;
	private Integer installNum;

	// Constructors

	/** default constructor */
	public Machineinfo() {
	}

	/** minimal constructor */
	public Machineinfo(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public Machineinfo(Integer id, String psn, String machineCode,
			String diskSerial, String macAddr, String ip, Integer installNum) {
		this.id = id;
		this.psn = psn;
		this.machineCode = machineCode;
		this.diskSerial = diskSerial;
		this.macAddr = macAddr;
		this.ip = ip;
		this.installNum = installNum;
	}

	// Property accessors

	@Id
	@GeneratedValue
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPsn() {
		return this.psn;
	}

	public void setPsn(String psn) {
		this.psn = psn;
	}

	public String getMachineCode() {
		return this.machineCode;
	}

	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getDiskSerial() {
		return this.diskSerial;
	}

	public void setDiskSerial(String diskSerial) {
		this.diskSerial = diskSerial;
	}

	public String getMacAddr() {
		return this.macAddr;
	}

	public void setMacAddr(String macAddr) {
		this.macAddr = macAddr;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getInstallNum() {
		return this.installNum;
	}

	public void setInstallNum(Integer installNum) {
		this.installNum = installNum;
	}

}