package com.i8app.shop.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="app_marker")
public class AppMarker {
	private int id;
	private Integer provId, isReadOnly;
	private String packUuid, markerType;
	private Date autoUpdateTime;
	
	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getProvId() {
		return provId;
	}
	public void setProvId(Integer provId) {
		this.provId = provId;
	}
	public Integer getIsReadOnly() {
		return isReadOnly;
	}
	public void setIsReadOnly(Integer isReadOnly) {
		this.isReadOnly = isReadOnly;
	}
	public String getPackUuid() {
		return packUuid;
	}
	public void setPackUuid(String packUuid) {
		this.packUuid = packUuid;
	}
	public String getMarkerType() {
		return markerType;
	}
	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}
	public Date getAutoUpdateTime() {
		return autoUpdateTime;
	}
	public void setAutoUpdateTime(Date autoUpdateTime) {
		this.autoUpdateTime = autoUpdateTime;
	}
	
}
