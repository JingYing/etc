package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

/**
 * Addressbookrunlog entity. @author MyEclipse Persistence Tools
 */
@Entity
public class Addressbookrunlog implements
		java.io.Serializable {

	// Fields

	private String id;
	private Integer empid;
	private String starttime;
	private String ostype;
	private String typename;
	private Integer typeid;
	private String osversion;
	private String phonenumber;
	private Integer type;
	private Integer opt;
	private Integer result;
	private String exportid;

	// Constructors

	/** default constructor */
	public Addressbookrunlog() {
	}

	/** full constructor */
	public Addressbookrunlog(Integer empid, String starttime, String ostype,
			String typename, Integer typeid, String osversion,
			String phonenumber, Integer type, Integer opt, Integer result,
			String exportid) {
		this.empid = empid;
		this.starttime = starttime;
		this.ostype = ostype;
		this.typename = typename;
		this.typeid = typeid;
		this.osversion = osversion;
		this.phonenumber = phonenumber;
		this.type = type;
		this.opt = opt;
		this.result = result;
		this.exportid = exportid;
	}

	// Property accessors
	@Id
	@GeneratedValue(generator="my-uuid")
	@GenericGenerator(name="my-uuid", strategy="uuid")
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "Empid")
	public Integer getEmpid() {
		return this.empid;
	}

	public void setEmpid(Integer empid) {
		this.empid = empid;
	}

	@Column(name = "Starttime", length = 20)
	public String getStarttime() {
		return this.starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	@Column(name = "Ostype", length = 5)
	public String getOstype() {
		return this.ostype;
	}

	public void setOstype(String ostype) {
		this.ostype = ostype;
	}

	@Column(name = "Typename", length = 50)
	public String getTypename() {
		return this.typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	@Column(name = "Typeid")
	public Integer getTypeid() {
		return this.typeid;
	}

	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}

	@Column(name = "Osversion", length = 50)
	public String getOsversion() {
		return this.osversion;
	}

	public void setOsversion(String osversion) {
		this.osversion = osversion;
	}

	@Column(name = "Phonenumber", length = 20)
	public String getPhonenumber() {
		return this.phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	@Column(name = "TYPE")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "Opt")
	public Integer getOpt() {
		return this.opt;
	}

	public void setOpt(Integer opt) {
		this.opt = opt;
	}

	@Column(name = "Result")
	public Integer getResult() {
		return this.result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	@Column(name = "Exportid")
	public String getExportid() {
		return exportid;
	}

	public void setExportid(String exportid) {
		this.exportid = exportid;
	}
}