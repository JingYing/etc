package com.i8app.shop.domain;

/**
 * Updateprogram entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class Updateprogram implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer type;
	private String version;
	private String address;

	// Constructors

	/** default constructor */
	public Updateprogram() {
	}

	/** minimal constructor */
	public Updateprogram(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public Updateprogram(Integer id, String version, String address) {
		this.id = id;
		this.version = version;
		this.address = address;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}