package com.i8app.shop.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ClientExcept {
	private Integer id;
	private Date exceptTime;
	private String descript, mac;
	private Integer itype, exceptid;

	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getExceptTime() {
		return exceptTime;
	}

	public void setExceptTime(Date exceptTime) {
		this.exceptTime = exceptTime;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public Integer getItype() {
		return itype;
	}

	public void setItype(Integer itype) {
		this.itype = itype;
	}

	public Integer getExceptid() {
		return exceptid;
	}

	public void setExceptid(Integer exceptid) {
		this.exceptid = exceptid;
	}

}
