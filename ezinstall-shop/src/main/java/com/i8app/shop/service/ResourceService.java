package com.i8app.shop.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.dao.ResourceDao;
import com.i8app.shop.domain.AppDTO;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.Clientconfigdate;
import com.i8app.shop.domain.Pkg;
import com.i8app.shop.domain.ResourceType;
import com.i8app.shop.domain.UnionreadPkgView;

@Service
public class ResourceService {
	
	@Resource
	private ResourceDao resourceDao;

	public void updateApp(AppDTO app)	{
		resourceDao.updateApp(app);
	}
	
	public void updateTotalInstallCount(String uuid)	{
		resourceDao.updateTotalInstallCount(uuid);
	}
	
	public void updateDownNum(String uuid)	{
		resourceDao.updateDownNum(uuid);
	}
	
	public List<ResourceType> appTypeList(String typeId)	{
		return resourceDao.appTypeList(typeId);
	}
	
	public ResourceType findAppType(String typeId)	{
		return resourceDao.findAppType(typeId);
	}
	
	public AppInstallPack findAppPkgByid(String pkguuid)	{
		return resourceDao.findAppPkgByid(pkguuid);
	}
	
	public AppDTO findAppByid(String uuid)	{
		return resourceDao.findAppByid(uuid);
	}
	public AppInstallPack findAppPkgByAppId(String appId, String os)	{
		return resourceDao.findAppPkgByAppId(appId, os);	
	}
	
	public List<Clientconfigdate> findCcDate(int provId, int smallId, int bidId)	{
		return resourceDao.findCcDate(provId, smallId, bidId);
	}
	
	public AppInstallPack findIOSAppPackByAppUuid(String appUuid){
		return resourceDao.findIOSAppPackByAppUuid(appUuid);
	}
	
	public AppInstallPack findIOSAppPackByPkgUuid(String pkgUuid){
		return resourceDao.findIOSAppPackByPkgUuid(pkgUuid);
	}
	
	public Pkg findPkgByPkgId(Integer pkgId){
		return resourceDao.findPkgByPkgId(pkgId);
	}
	
	public List<UnionreadPkgView> findPkgViewByProvid(Integer provId){
		return resourceDao.findPkgViewByProvid(provId);
	}
	
	public List<UnionreadPkgView> findPackUuidByPkgId(Integer provId, String pkgId){
		return resourceDao.findPackUuidByPkgId(provId, pkgId);
	}
	
	public String findMarkerType(Integer provId, String packUuid){
		return resourceDao.findMarkerType(provId, packUuid);
	}

	public AppInstallPack findByYLAppId(String appid)	{
		return resourceDao.findByYLAppId(appid);
	}
	
	public AppInstallPack findByAXONAppId(String appid)	{
		return resourceDao.findByAXONAppId(appid);
	}
	
	public List<AppInstallPack> findAXONApp()	{
		return resourceDao.findAXONApp();
	}
}
