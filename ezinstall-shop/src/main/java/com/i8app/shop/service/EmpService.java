package com.i8app.shop.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.common.Util;
import com.i8app.shop.dao.EmployeeDao;
import com.i8app.shop.domain.Employee;

@Service
public class EmpService {

	@Resource
	private EmployeeDao empDao;
	
	/**
	 * 根据工号查询营业厅人员
	 * @param empNo
	 * @return
	 */
	public Employee findEmpByNo(String empNo)	{
		return empDao.findByNo(empNo);
	}
	
	public Employee findById(int id)	{
		return empDao.findById(id);
	}
	
	public int getProvinceIdByEmp(String empNo) throws Exception	{
		Employee e = empDao.findByNo(empNo);
		if(e == null)
			throw new Exception("未找到匹配的empno");
		return Util.parseOrgIdStr(e.getOrgIdStr(), "pro");
	}
	
	public void updatePhoneNumber(int empId, String phoneNumber, 
								String account, String user, String bank)	{
		empDao.update(empId, phoneNumber, account, user, bank);
	}
	
	public Employee findByNoAndProId(String empNo, Integer proid)	{
		return empDao.findByNoAndProId(empNo, proid);
	}
	
	public void saveOrUpdate(Employee emp)	{
		empDao.saveOrUpdate(emp);
	}
	
}
