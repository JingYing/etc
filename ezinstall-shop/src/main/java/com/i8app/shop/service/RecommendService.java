package com.i8app.shop.service;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.i8app.shop.dao.RecommendDao;

@Service
public class RecommendService {
	
	@Resource
	private RecommendDao recommendDao;

	public String recommendList(String Msisdn, String servType) throws Exception {
		return recommendDao.recommendList(Msisdn, servType);
	}
}
