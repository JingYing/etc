package com.i8app.shop.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.common.Pager;
import com.i8app.shop.dao.TypeInfoDao;
import com.i8app.shop.domain.TypeInfo;

@Service
public class TypeInfoService {
	
	@Resource
	private TypeInfoDao typeInfoDao;
	

	/**
	 * 根据ID查询TypeInfo表里的对应记录
	 * @param id
	 * @return
	 */
	public TypeInfo findById(int id)	{
		return typeInfoDao.findById(id);
	}
	
	public TypeInfo findByTemp(String manuName, String typeName, String osType)	{
		return typeInfoDao.findByTemp(manuName, typeName , osType);
	}
	
	/**
	 * 根据MANUID查询符合条件的所有TYPEINFO记录, 按TYPENAME升序排列
	 * @param manuId
	 * @return
	 */
	public List<TypeInfo> findByManuId(int manuId)	{
		return typeInfoDao.findByManuId(manuId);
	}
	
	/**
	 * 根据manuId和firstLetter查询,
	 * 条件1: compressPic不为空
	 * 条件2: 只查第一个
	 * @param initial
	 * @param manuId
	 * @return
	 */
	public TypeInfo findFirstByInitial(String initial, Integer manuId)	{
		return typeInfoDao.findFirstByInitial(initial, manuId);
	}
	
	/**
	 * 根据厂商ID和首字母查询
	 * @param firstLetter
	 * @param manuId
	 * @return
	 */
	public List<TypeInfo> findByInitial(String initial, Integer manuId){
		return typeInfoDao.findByInitial(initial, manuId);
	}

	/**
	 * 根据厂商ID和首字母查询
	 * @param firstLetter
	 * @param manuId
	 * @return
	 */
	public Pager<TypeInfo> findByInitial(String initial, Integer manuId, int offset, int pageSize){
		return typeInfoDao.findByInitial(initial, manuId, offset, pageSize);
	}
	
	public List<String> findInitial(Integer manuId)	{
		return typeInfoDao.findInitial(manuId);
	}
	
	/**
	 * 查询机型字母索引和索引对应的第一个手机的图片
	 * 在集合中String的格式为"A==/fileServer/1.pic", 首字母和图片以==隔开
	 * @param manuId
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public List<String> findInitialAndPic(int manuId, int offset, int pageSize)	{
		List<String> list = typeInfoDao.findInitial(manuId, offset, pageSize).getList();
		for(int i=0; i<list.size(); i++)	{
			TypeInfo t = typeInfoDao.findFirstByInitial(list.get(i), manuId);
			list.set(i, list.get(i) + "==" + t.getCompressPic());
		}
		return list;
	}
	
	/**
	 * 查询机型字母索引和索引对应的第一个手机的图片, 返回json数组
	 * @param manuId
	 * @param offset
	 * @param pageSize
	 * @return [{"intial":"A", "pic": "/xxxx.jpg"}, {"intial":"A", "pic": "/xxxx.jpg"}]
	 */
	public Map<String, String> findInitialPic(int manuId, int offset, int pageSize)	{
		List<String> list = typeInfoDao.findInitial(manuId, offset, pageSize).getList();
		Map<String, String> map = new LinkedHashMap<String, String>();
		for(int i=0; i<list.size(); i++)	{
			if(list.get(i) != null && !"".equals(list.get(i))){
				TypeInfo t = typeInfoDao.findFirstByInitial(list.get(i), manuId);
				if (t != null) {
					map.put(list.get(i), t.getCompressPic());
				}
			}
		}
		return map;
	}
	
	/**
	 * 手机机型列表
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<TypeInfo> list(int offset, int pageSize)	{
		return typeInfoDao.list(offset, pageSize);
	}
	
	/**
	 * 调用typeInfoDao的搜索接口
	 * @param keyword
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<TypeInfo> search(String keyword, int offset, int pageSize)	{
		return typeInfoDao.search(keyword, offset, pageSize);
	}
}
