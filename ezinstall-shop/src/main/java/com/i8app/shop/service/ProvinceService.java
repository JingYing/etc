package com.i8app.shop.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.dao.ProvinceDao;
import com.i8app.shop.domain.Province;

@Service
public class ProvinceService {

	@Resource
	private ProvinceDao provinceDao;
	
	public List<Province> findProvince() {
		return provinceDao.findProvince();
	}

	public Province findByBssAreacode(String areacode) {
		return provinceDao.findByBssAreacode(areacode);
	}
}
