package com.i8app.shop.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.i8app.shop.dao.TempDriverDao;

public class TempDriverServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		String vid = request.getParameter("vid");
		String pid = request.getParameter("pid");
		String file = request.getParameter("file");
		
		TempDriverDao dao = (TempDriverDao)applicationContext.getBean(TempDriverDao.class);
		String status = "failed";
		try	{
			dao.save(vid, pid, file);
			status="success";
		}catch(Exception e)	{
			e.printStackTrace();
		}
		
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().print(status);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
