 package com.i8app.shop.web.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants.OrderBy;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.common.config.ConfigProp;
import com.i8app.shop.common.config.Config_i8;
import com.i8app.shop.common.wsclient.SmsSender;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.Clientconfigdate;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.Pkg;
import com.i8app.shop.domain.PkgGroup;
import com.i8app.shop.domain.ResourceType;
import com.i8app.shop.service.AppService;
import com.i8app.shop.service.EmpService;
import com.i8app.shop.service.ISmsService;
import com.i8app.shop.service.InstallLogService;
import com.i8app.shop.service.ResourceService;

@Controller
@Scope("prototype")
public class ResourceAction extends BaseAction {
	@Resource
	private ResourceService resourceService;
	@Resource
	private EmpService empService;
	@Resource
	private InstallLogService installLogService;
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private ISmsService smsHessianService;
	@Resource
	private SmsSender smsSender;
	@Resource
	private Config_i8 config_i8;
	@Resource
	private AppService appService;

	private Pager<AppDTO> appPager;
	private List<ResourceType> appTypeList;
	private List<Pkg> pkgList;
	private List<AppDTO> appList;// 不分页的

	private AppDTO app;
	private Pkg pkg;
	
	private String installType = "1";
	private String packUuid, keyword, typeId, ids, version, phoneNo, imoduletype, action;
	private int empId, appOsType, pkgId, isIOS, pageIndex;
	private boolean orderBy;
	private Date cpUpdateTime;

	

	/** ============================================================ 山西一体机新接口  -- 开始 ============================================================ */
	@Override
	public String execute()	{
		if("packAppList".equals(action))
			return packAppList();
		else if("softList".equals(action))
			return this.softList();
		else if("gameList".equals(action))
			return this.gameList();
		else
			throw new IllegalArgumentException("action参数无效");
	}
	
	/**
	 * 包接口，根据业务类型和包的主键查询该包下的所有软件列表
	 *   
	 * @param appOsType
	 * @param version 
	 * @param pkgId 
	 * @param empId
	 * @param pageSize
	 * @param installType 
	 * @param isIOS 
	 * 
	 * @return appPager and appList
	 */
	
	public String packAppList() {
		
		Os os = this.adaptToOs(appOsType, version);
		
		if(pkgId > 0){
			
			Employee emp = empService.findById(empId);
			AreaParam ap = new AreaParam();
			ap.setProvId(emp.getProvince().getId());
			ap.setCityId(emp.getCity().getId());
			ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
			
			appPager = appQuerier.findInstallPackByPkg(ap, pkgId, os, offset, pageSize);
			
		}
		return SUCCESS;
	}
	
	/**
	 * 应用 宝库
	 * @param appOsType
	 * @param version
	 * @param empId 
	 * @param typeId
	 * @param pageSize 
	 * @param installType
	 * 
	 * @return  appList
	 */
	public String softList(){
		
		if(typeId == null || "".equals(typeId)){
			typeId = "101";
		}
		appTypeList = resourceService.appTypeList("101");
		
		orderBy = false;//根据更新时间排序
		
//		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.MONTH, -3);    //得到前三个月   
//		int year = calendar.get(Calendar.YEAR);   
//		int month = calendar.get(Calendar.MONTH);   //注意月份这个值如果单独拿来使用，需要加一才能是我们正常的月份
//		int today = Integer.parseInt(new SimpleDateFormat("dd").format(new Date()));
//		calendar.set(year, month, today);
//		Date lastThreeDate = calendar.getTime();
//		cpUpdateTime = lastThreeDate;
		
		appPager = this.findAppPager(appOsType, version, keyword, typeId, orderBy, cpUpdateTime, empId, false, pageSize);
		
		return SUCCESS;
	}
	
	/**
	 * 游戏 宝库
	 * @param appOsType
	 * @param version
	 * @param empId 
	 * @param typeId
	 * @param pageSize 
	 * @param installType
	 * 
	 * @return  appList
	 */
	public String gameList(){
		
		if(typeId == null){
			typeId = "102";
		}
		appTypeList = resourceService.appTypeList("102");
		
		orderBy = false;//根据更新时间排序
		
//		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.MONTH, -3);    //得到前三个月   
//		int year = calendar.get(Calendar.YEAR);   
//		int month = calendar.get(Calendar.MONTH);   //注意月份这个值如果单独拿来使用，需要加一才能是我们正常的月份
//		int today = Integer.parseInt(new SimpleDateFormat("dd").format(new Date()));
//		calendar.set(year, month, today);
//		Date lastThreeDate = calendar.getTime();
//		cpUpdateTime = lastThreeDate;
		
		appPager = this.findAppPager(appOsType, version, keyword, typeId, orderBy, cpUpdateTime, empId, false, pageSize);
		
		return SUCCESS;
	}
	
	/**
	 * 根据安装包ID, 查询单个软件的详细信息.包括截图 
	 * @param appOsType 
	 * @param version
	 * 
	 * @param imoduletype
	 * @param packUuid
	 * @param app.getUuid()
	 * 
	 * @return AppDTO
	 */
	public String app_info() {
		if(imoduletype != null && (packUuid == null || "".equals(packUuid)) 
				&& ("10".equals(imoduletype) || "9".equals(imoduletype))){//来自应用首页的软件，该处软件没有packUuid，只能根据Uuid和OS查询详情

			Os os = this.adaptToOs(appOsType, version);
			app = appQuerier.findSingle(app.getUuid(), os);
		}else{
			app = appQuerier.findSinglePack(packUuid);
		}
		return SUCCESS;
	}

	
	
	/** =================================== 下载安装 模块 ====================================== */ 
	/**
	 * 短信下载  
	 * ios短信下载改成: 发送itunes链接, 不使用短链
	 * @param empId
	 * @param installType
	 * 
	 * @param packUuid
	 * @param phoneNo
	 * @param imoduletype 
	 * @param appOsType
	 * @param version
	 * 
	 * @return
	 * @throws CpServerAccessException
	 */
	@SuppressWarnings("deprecation")
	public String duanXinDown() throws CpServerAccessException {

		Employee emp = empService.findById(empId);
		if (emp == null)
			throw new IllegalArgumentException(
					"no matching areacode of this province id on empId :" + empId);
		
		int logId = installLogService.buildSmsInstallLog(Integer.parseInt(installType), empId, packUuid, phoneNo, 
				70004, SessionAccessor.getPhoneLoginUuid().toString(), imoduletype);
		
		String areaCode = config_i8.getProvAreaMap().get(emp.getProvince().getId());
		try {
			//IOS的操作系统的短信全部发送itunes链接
			Os os = this.adaptToOs(appOsType, version);
			if(Os.IOS.equals(os) || Os.IOSX.equals(os)){
				AppInstallPack IOSAppPkg = resourceService.findIOSAppPackByPkgUuid(packUuid);//根据 packUuid 获取正版url
				String url = String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim());
				
				smsSender.sendDownloadLink(areaCode, phoneNo, IOSAppPkg.getApp().getName(), url);
				
			}else{
				smsHessianService.sendShortUrl(Integer.parseInt(installType), phoneNo, areaCode, packUuid, logId);// 该packUuid是从页面上传入的
			}
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
	/**
	 * 批量下载、强制安装、隐藏安装
	 * 
	 * 根据 操作系统、软件类别、uuid 或 packUuid 获取下载信息 (统一下载方法)
	 * @param appOsType
	 * @param version
	 * @param empId
	 * 
	 * @param ids
	 * @param pkgId
	 * @param imoduletype
	 * 
	 * @return JSONObject
	 * @throws CpServerAccessException
	 */
	@SuppressWarnings("static-access")
	public String downLoad() throws CpServerAccessException {
		
		JSONArray jarr = new JSONArray();
		Os os = this.adaptToOs(appOsType, version);

		Employee emp = empService.findById(empId);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id

		// =============================== 页面下载 ===============================
		if (ids != null && !"".equals(ids)) {
			String[] idsstr = ids.split(",");
			for (String disc : idsstr) {
				String[] disc_str = disc.split("_");
				String uuid = null;
				String packUuid = null;
				Integer discriminator = null;
				if (disc_str.length > 0) {
					uuid = disc_str[0];
				}
				if (disc_str.length > 1) {
					packUuid = disc_str[1];
				}
				if (disc_str.length > 2) {
					if (disc_str[2] != null && !"null".equals(disc_str[2]) && !"".equals(disc_str[2])) {
						discriminator = Integer.parseInt(disc_str[2]);
					}
				}
				// 下载 
				try {
					jarr = this.downMsg(jarr, ids, app, discriminator, uuid, packUuid, os, ap, pkgId, imoduletype, isIOS, installType, pageSize, emp.getProvince().getId());
					
				} catch (JSONException e) {
					e.printStackTrace();
				} 

			}
		} else {
			List<PkgGroup> wostorePkgList = new ArrayList<PkgGroup>();
			// 根据不同的业务获取不同的包组
			if ("2".equals(imoduletype)) {// ========================= 强制安装 ====================
				wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_FORCEINSTALL, ap);
			} else if ("3".equals(imoduletype)) {// ==================  隐藏安装  ================
				wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_HIDDEN_INSTALL, ap);
				os = null;
			}
			if (wostorePkgList != null && wostorePkgList.size() > 0) {
				Integer pkgGroupId = null;
				for (int i = 0; i < wostorePkgList.size(); i++) {
					pkgGroupId = wostorePkgList.get(0).getId();// 获取第一个包的主键
				}

				pkgList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包的包组
				if (pkgList != null && pkgList.size() > 0) {
					Pkg pkg = null;
					for (int i = 0; i < pkgList.size(); i++) {// 里面只有一个包
						pkg = pkgList.get(0);
					}

					if (pkg != null) {
						appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize);
						appList = appPager.getList();

						for (AppDTO app : appList) {
							// 下载
							try {
								jarr = this.downMsg(jarr, ids, app, app.getDiscriminator(), app.getUuid(), app.getPackUuid(), os, ap, pkg.getId(), imoduletype, isIOS, installType, pageSize, emp.getProvince().getId());
								
							} catch (JSONException e) {
								e.printStackTrace();
							} 
						}
					}
				}
			}
		}

		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "msginfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this.doPrint(joMap.toString());
	}

		
	
	
	
	

	/**  ========================================= 手机客户端的接口 模块  ================================ */
	/**
	 * 手机客户端--软件宝库
	 * 
	 * @param appOsType
	 * @param version
	 * @param pageIndex
	 * @param pageSize
	 * @return JsonArray
	 */
	@SuppressWarnings("deprecation")
	public String phone_findApps() {
		Os os = this.adaptToOs(appOsType, version);
		offset = (pageIndex - 1) * pageSize;
		appPager = this.findAppPager(appOsType, version, keyword, typeId, orderBy, cpUpdateTime, empId, false, pageSize);
		JSONArray jsonArr = new JSONArray();
		for (AppDTO ad : appPager.getList()) {
			String cpId = null;
			if (Os.SYMBIAN.equals(os) || Os.ANDROID.equals(os)) {
				cpId = AreaParam.CP_WOSTORE;
			} else {
				cpId = AreaParam.CP_I8APP;
			}
			String packUuid = appQuerier.confirmPack( ad.getUuid(), os, cpId);//为null时代表没有找到符合条件的安装包
			ad.setPackUuid(packUuid);
			jsonArr.put(this.jsonAppInfo(ad, packUuid));
		}
		return this.doPrint(jsonArr.toString());
	}

	public String phone_sendAuthcode() {
		Employee emp = empService.findById(empId);
		if (emp == null)
			throw new IllegalArgumentException(
					"no matching areacode of this province id on empId :" + empId);
		String areaCode = config_i8.getProvAreaMap().get( emp.getProvince().getId());

		String authcode = smsHessianService.sendAuthcode(phoneNo, areaCode);
		JSONObject json = new JSONObject();
		try {
			json.put("authcode", authcode);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return this.doPrint(json.toString());

	}

	/**
	 * 根据软件id和操作系统查询对应的单个软件信息
	 * 
	 * @param appOsType
	 * @param version
	 * @return JsonObject
	 */
	public String phone_AppInfo() {
		Os os = this.adaptToOs(appOsType, version);
		app = appQuerier.findSingle(app.getUuid(), os);
		String cpId = null;
		if (os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {
			cpId = AreaParam.CP_WOSTORE;
		} else {
			cpId = AreaParam.CP_I8APP;
		}
		String packUuid = appQuerier.confirmPack( app.getUuid(), os, cpId);//为null时代表没有找到符合条件的安装包
		return this.doPrint(this.jsonAppInfo(app, packUuid).toString());
	}

	/**
	 * 手机客户端--短信下载
	 * 
	 * @param installType
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param phoneNo
	 * @param ids 
	 * @param app.uuid
	 * @return
	 */
	public String phone_duanXinDown() {
		try {
			// 这中条件主要针对手机客户端的( PC客户端最好是通过 app.PackUuid 做短信下载 )
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			JSONObject jsnb = new JSONObject(s);
			appOsType = Integer.parseInt(jsnb.getString("appOsType"));
			version = jsnb.getString("version");
//			Os os = this.adaptToOs(appOsType, version);

			empId = Integer.parseInt(jsnb.getString("empId"));
			Employee emp = empService.findById(empId);
			if (emp == null)
				throw new IllegalArgumentException(
						"no matching areacode of this province id on empId :" + empId);
			String areaCode = config_i8.getProvAreaMap().get(emp.getProvince().getId());
			
			phoneNo = jsnb.getString("phoneNo");
			installType = jsnb.getString("installType");
			JSONArray ids = jsnb.getJSONArray("ids");
			for (int i = 0; i < ids.length(); i++) {
				String packUuid = ids.getString(i);
				int logId = installLogService.buildSmsInstallLog(Integer.parseInt(installType), empId, packUuid, phoneNo, 70004, null, "6");//6：营业人员的手机客户端
				smsHessianService.sendShortUrl(Integer.parseInt(installType), phoneNo, areaCode, packUuid, logId);
			}
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 组装单个json对象
	 * 
	 * @param app
	 * @return JsonObject
	 */
	public JSONObject jsonAppInfo(AppDTO app, String packUuid) {
		JSONObject json = new JSONObject();
		try {
			json.put("uuid", app.getUuid());
			json.put("name", app.getName());
			if(app.getIconUrl() == null || "".equals(app.getIconUrl())){
				json.put("icon", "");
			}else{
				json.put("icon", app.getIconUrl());
			}
			json.put("pinyin", app.getPinyin());
			json.put("disc", app.getDiscriminator());
			json.put("downCount", app.getDownCount());
			json.put("installCount", app.getInstallCount());
			json.put("point", app.getPoint());
			json.put("developer", app.getDeveloper());
			json.put("originalAppId", app.getOriginalAppId());

			json.put("packAppVer", app.getPackAppVer());
			json.put("packCpId", app.getPackCpId());
			json.put("packCpUpdateTime", app.getPackCpUpdateTime());
			json.put("packFileSize", app.getPackFileSize());
			json.put("packOs", app.getPackOs());
			json.put("packOsMinVer", app.getPackOsMinVer());
			json.put("packUuid", packUuid);

			json.put("cpId", app.getCpId());
			json.put("cpName", app.getCpName());
			json.put("cpUpdateTime", app.getCpUpdateTime());
			json.put("info", app.getInfo());
			json.put("infoHtml", app.getInfoHtml());
			json.put("markerType", app.getMarkerType());
			json.put("markerTypeName", app.getMarkerTypeName());

			json.put("rscTypeId", app.getRscTypeId());
			json.put("rscTypeName", app.getRscTypeName());
			json.put("rscTypePid", app.getRscTypePid());

			JSONArray pics = new JSONArray();
			if (app.getPics() != null) {
				for (String p : app.getPics()) {
					pics.put(ConfigProp.getInstance().getFileServerUrl() + p);
				}
			}
			json.put("pics", pics);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	
	/**
	 * 手机客户端--下载（只能是单个的下载）---该方法可能会存在问题，需要客户端配合调试（2013-08-29 修改过）
	 * 
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param ids
	 * @param installType
	 * @param app.uuid
	 * @return
	 */
	public String phone_DownLoad(){
		JSONObject jo = new JSONObject();
		BufferedReader br;
		try {
			br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			
			String s = br.readLine();
			JSONObject jsnb = new JSONObject(s);
			appOsType = Integer.parseInt(jsnb.getString("appOsType"));
			version = jsnb.getString("version");
			Os os = this.adaptToOs(appOsType, version);

			empId = Integer.parseInt(jsnb.getString("empId"));
			
			Employee emp = empService.findById(empId);
			AreaParam ap = new AreaParam();
			ap.setProvId(emp.getProvince().getId());
			ap.setCityId(emp.getCity().getId());
			ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
			
			String uuid = (String) jsnb.get("uuid");
			String packUuid = (String) jsnb.get("packUuid");
			
			AppInstallPack appPkg = resourceService.findAppPkgByid(appQuerier.confirmPack(uuid, os, AreaParam.CP_WOSTORE));
			
			//获取非正版的其他下载地址  这个是query查询的，所以 discriminator 的值是娶不到的，所以直接给个 null
			String discriminator = null;
			Map<String, String> urlMap = this.downLoad(null, uuid, packUuid, os, AreaParam.CP_WOSTORE);
			String pkgUrl = urlMap.get("pkgUrl");
			
			// ==================== 开始 == 是否自动打开 ==========================
			int autoOpen = 0;// 是否自动打开 1：是 ；0：否
			List<PkgGroup> wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_AUTO_OPEN, ap);// 是否自动打开
			if (wostorePkgList != null && wostorePkgList.size() > 0) {
				Integer pkgGroupId = null;
				for (int i = 0; i < wostorePkgList.size(); i++) {
					pkgGroupId = wostorePkgList.get(0).getId();// 获取第一个包的主键
				}
				pkgList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包的包组
				if (pkgList != null && pkgList.size() > 0) {
					for (int i = 0; i < pkgList.size(); i++) {// 里面只有一个包
						pkg = pkgList.get(0);
					}
					if (pkg != null) {
						/**该处是否根据山西的省id做限制，限制山西的所有项目都使用cp09的软件**/
						appPager = appQuerier.findInstallPackByPkg(ap, pkgId, os, offset, pageSize);// 查找对应必备包的软件列表
						appList = appPager.getList();
					}
					for (AppDTO ad : appList) {
						if (Integer.parseInt(discriminator)==2 && ad.getPackUuid().equals(appPkg.getUuid())) {
							autoOpen = 1;
						} else if ((discriminator == null || Integer.parseInt(discriminator) == 1 ) && ad.getUuid().equals(appPkg.getApp().getUuid())) {
							autoOpen = 1;
						}
					}
				}
			}
			// ==================== 结束 == 是否自动打开 ==========================
			
			// =================== 拼接json串 ======================
			jo.put("pkgUuid", appPkg.getUuid());
			jo.put("apkPkgName", appPkg.getApkPkgName());
			jo.put("isAutoOpen", autoOpen);
			if ("cp01".equals(appPkg.getCp().getId()) || "cp02".equals(appPkg.getCp().getId()) || "cp09".equals(appPkg.getCp().getId())) {
				jo.put("source", 0);
			} else if ("cp03".equals(appPkg.getCp().getId())) {
				jo.put("source", 2);
			} else if (("cp04".equals(appPkg.getCp().getId()) || "cp05".equals(appPkg.getCp().getId()))) {
				jo.put("source", 1);
			} else if ("cp06".equals(appPkg.getCp().getId())) {
				jo.put("source", 5);
			}
			if (appPkg.getVersion() != null || !"".equals(appPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("rscVersion", appPkg.getVersion());
			} else {
				jo.put("rscVersion", appPkg.getAppVer());
			}
			
			jo.put("url", pkgUrl.replaceAll("[,;]*",""));//这个只给android系统用
			
			// 客户端调用下载地址后, app的downCount值+1 source
			resourceService.updateDownNum(uuid);
			
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(jo.toString());
	}
	
	
	/**
	 * 手机客户端--推荐专区
	 * 
	 * @param appOsType 
	 * @param version 
	 * @param pageIndex
	 * @param pageSize 
	 * @param empId
	 * 
	 * @return JSONArray
	 */
	public String phone_homeIndex() {

		JSONArray jarr = new JSONArray();
		BufferedReader br;
		try {
			br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			
			String s = br.readLine();
			JSONObject jsnb = new JSONObject(s);
		
			appOsType = Integer.parseInt(jsnb.getString("appOsType"));
			version = jsnb.getString("version");
			Os os = this.adaptToOs(appOsType, version);
			
			pageIndex = Integer.parseInt(jsnb.getString("pageIndex"));
			pageSize = Integer.parseInt(jsnb.getString("pageSize"));
			offset = (pageIndex - 1) * pageSize;
			
			empId = Integer.parseInt(jsnb.getString("empId"));
			Employee emp = empService.findById(empId);
			AreaParam ap = new AreaParam();
			ap.setProvId(emp.getProvince().getId());
			ap.setCityId(emp.getCity().getId());
			ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
	
			// 根据不同的业务获取不同的包组
			Integer pkgGroupId = null;
			/** ================= 全国安装包 =================*/
			List<PkgGroup> wostoreNationPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_WOSTORE_NATION_PKG, ap);
			if (wostoreNationPkgList != null && wostoreNationPkgList.size() > 0) {
				for (int i = 0; i < wostoreNationPkgList.size(); i++) {
					pkgGroupId = wostoreNationPkgList.get(0).getId();// 获取第一个包组的主键
				}
				if (pkgGroupId > 0) {
					List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包组下所有的子包
					if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
						
						for (int i = 0; i < pkgDTOList.size(); i++) {//添加全国包所有子包
							Pkg p = pkgDTOList.get(i);
							System.out.println(p.getName() + "			" +pkgDTOList.size());
							JSONObject pkgJo = new JSONObject();
							pkgJo.put("groupId", p.getGroupId());
							pkgJo.put("pkgId", p.getId());
							if(p.getName() == null || "".equals(p.getName())){
								pkgJo.put("pkgName", "");
							}else{
								pkgJo.put("pkgName", p.getName());
							}
							if(p.getIcon() == null || "".equals(p.getIcon())){
								pkgJo.put("icon", "");
							}else{
								pkgJo.put("icon", p.getIcon());
							}
							if(p.getLargeIcon() == null || "".equals(p.getLargeIcon())){
								pkgJo.put("largeIcon", "");
							}else{
								pkgJo.put("largeIcon", p.getLargeIcon());
							}
							pkgJo.put("num", p.getNum());
							pkgJo.put("info", p.getInfo());
							
							JSONArray appJo = new JSONArray();
							if(p.getId() > 0){
								appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应包的软件列表
								int index = 0;
								for (AppDTO app : appPager.getList()) {
									appJo.put(this.jsonAppInfo(app, app.getPackUuid()));
									index ++;
								}
							}
							pkgJo.put("appArray", appJo);
							jarr.put(pkgJo);
							pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的全国包
						}
						
						
					}
				}
				pkgGroupId = null;
			}
	
			
			/** ================= 手机必备包 ================= **/
			List<PkgGroup> essentialPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_ESSENTIAL, ap);
			if (essentialPkgList != null && essentialPkgList.size() > 0) {
				for (int j = 0; j < essentialPkgList.size(); j++) {
					pkgGroupId = essentialPkgList.get(0).getId();// 获取第一个包组的主键
				}
				if (pkgGroupId > 0) {
					List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包组（里头包含了该包（手机必备）的所有子包）
					if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
						for (int i = 0; i < pkgDTOList.size(); i++) {
							Pkg p = pkgDTOList.get(i);
							System.out.println(p.getName() + "			" +pkgDTOList.size());
							JSONObject pkgJo = new JSONObject();
							pkgJo.put("groupId", p.getGroupId());
							pkgJo.put("pkgId", p.getId());
							if(p.getName() == null || "".equals(p.getName())){
								pkgJo.put("pkgName", "");
							}else{
								pkgJo.put("pkgName", p.getName());
							}
							if(p.getIcon() == null || "".equals(p.getIcon())){
								pkgJo.put("icon", "");
							}else{
								pkgJo.put("icon", p.getIcon());
							}
							if(p.getLargeIcon() == null || "".equals(p.getLargeIcon())){
								pkgJo.put("largeIcon", "");
							}else{
								pkgJo.put("largeIcon", p.getLargeIcon());
							}
							pkgJo.put("num", p.getNum());
							pkgJo.put("info", p.getInfo());
							
							JSONArray appJo = new JSONArray();
							
							if(p.getId() > 0){
								appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应包的软件列表
								int index = 0;
								for (AppDTO app : appPager.getList()) {
									appJo.put(this.jsonAppInfo(app, app.getPackUuid()));
									index ++;
								}
							}
							pkgJo.put("appArray", appJo);
							jarr.put(pkgJo); //添加所有手机必备包
							if(pkg == null){
								pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的包
							}
						}
					}
				}
				pkgGroupId = null;
			}
			
			
			/** ================= 地市包 ================= **/
			List<PkgGroup> cityPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_CITY_PKG, ap);
			if (cityPkgList != null && cityPkgList.size() > 0) {
				for (int j = 0; j < cityPkgList.size(); j++) {
					pkgGroupId = cityPkgList.get(0).getId();// 获取第一个包组的主键
				}
				if (pkgGroupId > 0) {
					List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包组的子包）
					if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
						for (int i = 0; i < pkgDTOList.size(); i++) {
							
							Pkg p = pkgDTOList.get(i);
							System.out.println(p.getName() + "			" +pkgDTOList.size());
							JSONObject pkgJo = new JSONObject();
							pkgJo.put("groupId", p.getGroupId());
							pkgJo.put("pkgId", p.getId());
							if(p.getName() == null || "".equals(p.getName())){
								pkgJo.put("pkgName", "");
							}else{
								pkgJo.put("pkgName", p.getName());
							}
							if(p.getIcon() == null || "".equals(p.getIcon())){
								pkgJo.put("icon", "");
							}else{
								pkgJo.put("icon", p.getIcon());
							}
							
							if(p.getLargeIcon() == null || "".equals(p.getLargeIcon())){
								pkgJo.put("largeIcon", "");
							}else{
								pkgJo.put("largeIcon", p.getLargeIcon());
							}
							pkgJo.put("num", p.getNum());
							pkgJo.put("info", p.getInfo());
							
							JSONArray appJo = new JSONArray();
							if(p.getId() > 0){
								appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应包的软件列表
								int index = 0;
								for (AppDTO app : appPager.getList()) {
									appJo.put( this.jsonAppInfo(app, app.getPackUuid()));
									index ++;
								}
							}
							pkgJo.put("appArray", appJo);
							jarr.put(pkgJo); //添加所有地市包
							if(pkg == null){
								pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的包
							}
						}
					}
				}
				pkgGroupId = null;
			}
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return this.doPrint(jarr.toString());
	}

	
	
	
	
	
	/** =================================== 内置方法 模块 ====================================== */

	/**
	 * 查询软件列表
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param pageSize
	 * @param cpUpdateTime
	 * @param keyword
	 * @param typeId
	 * @param orderBy
	 * @param isSearch
	 * 
	 * @return Pager<AppDTO>
	 * 
	 */
	public Pager<AppDTO> findAppPager(int appOsType, String version, String keyword, 
			String typeId, boolean orderBy, Date cpUpdateTime, int empId, boolean isSearch, int pageSize) {
		
		Employee emp = empService.findById(empId);
		AreaParam areap = new AreaParam();
		areap.setProvId(emp.getProvince().getId());
		areap.setCityId(emp.getCity().getId());
		areap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		AppParam appp = new AppParam();
		if("".equals(keyword)){
			keyword = null;
		}
		appp.setName(keyword);
		Os os = this.adaptToOs(appOsType, version);
		List<String> cpIdList = this.cpIdList(os, emp.getProvince().getId());
		appp.setCpIds(cpIdList);

		if (orderBy) {// 为 true 则排序
			appp.setOrderBy(OrderBy.cpUpdateTime);
			appp.setCpUpdateTime(cpUpdateTime);
		}
		if (typeId == null || "".equals(typeId)) {
			appp.setRscTypeId(AppParam.RSCTYPE_APP);
		} else {
			appp.setRscTypeId(typeId);
		}
		
		Pager<AppDTO> appPager = null;
		if(isSearch){//做关键字搜索
			appPager = appQuerier.search(areap, os, appp, offset, pageSize);// 里面的packUuid不正确
		}else {
			appPager = appQuerier.queryPack(areap, os, appp, offset, pageSize);// 里面的packUuid不正确
		}
		return appPager;
	}
	
	/**
	 * 拼接单个json字符对象
	 * 
	 * @param jarr
	 * @param ids
	 * @param app
	 * @param discriminator
	 * @param uuid
	 * @param packUuid
	 * @param os
	 * @param ap
	 * @param pkgId
	 * @param imoduletype
	 * 
	 * @return JSONArray
	 * @throws CpServerAccessException
	 * @throws JSONException
	 */
	public JSONArray downMsg(JSONArray jarr, String ids, AppDTO app, Integer discriminator, String uuid,
			String packUuid, Os os, AreaParam ap, Integer pkgId, String imoduletype, Integer isIOS, 
			String installType, Integer pageSize, Integer proId)
			throws CpServerAccessException, JSONException {
		// ============================ 开始 == 获取下载地址 ===================================

		if (app == null && uuid != null && !"".equals(uuid)) {
			app = appQuerier.findSingle(uuid, os);//这个查询只为获取名称和类别等信息，uuid和packUuid是以外面调用的时候传入的为准
		}
		String cpId = null;
		if (os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {
			cpId = AreaParam.CP_WOSTORE;
		} else { // 非android、SYMBIAN 系统使用i8资源, i8资源需要判断soft还是game
			cpId = AreaParam.CP_I8APP;
		}

		/**
		 * 	非正版的操作系统,则根据 discriminator 、packUuid 或 appUuid 、os、cpId 查询 AppInstallPack 对象
		 *  如果是正版的操作系统，并且isIOS == 1 获取正版包对象 
		 * 业务：
		 * 		1、根据 appUuid 查询 AppInstallPack, 获取 ipaItemId 不为空的对象,如果根据这个获取不到，那就是没有数据。
		 * 		2、根据 packUuid 查询 AppInstallPack, 获取 ipaItemId 不为空的对象；
		 * 			如果为空, 那么再根据 刚查到的对象的 ipaBundleId 字段查询 AppInstallPack ,获取 ipaItemId 不为空的对象 
		 */
		AppInstallPack IOSAppPkg = null;
		AppInstallPack appPkg =  null;
	
		if (Os.IOS.equals(os) && isIOS == 1) {//获取正版url
			IOSAppPkg = resourceService.findIOSAppPackByPkgUuid(packUuid);//根据 packUuid 获取正版url
			
		}else if(isIOS != 1){//不获取正版地址
			appPkg = resourceService.findAppPkgByid(packUuid);
			
		}
		
		//获取非正版的其他下载地址
		Map<String, String> urlMap = this.downLoad(discriminator, uuid, packUuid, os, cpId);
		String pkgUrl = urlMap.get("pkgUrl");
	
		// ============================ 结束 == 获取下载地址 ================================

		// ==================== 开始 == 是否自动打开 ==========================
		int autoOpen = 0;// 是否自动打开 1：是 ；0：否
		List<PkgGroup> autoOpenPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_AUTO_OPEN, ap);// 是否自动打开
		if (autoOpenPkgList != null && autoOpenPkgList.size() > 0) {
			Integer pkgGroupId = null;
			for (int i = 0; i < autoOpenPkgList.size(); i++) {
				pkgGroupId = autoOpenPkgList.get(0).getId();// 获取第一个包组的主键
			}
			List<Pkg> pkgList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包组的所有子包
			Pkg p = null;
			if (pkgList != null && pkgList.size() > 0) {
				for (int i = 0; i < pkgList.size(); i++) {// 里面只有一个包
					p = pkgList.get(0);
				}
				if (p != null) {
					appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize);// 查找对应必备包的软件列表
					appList = appPager.getList();
					for (AppDTO ad : appList) {
						if (packUuid != null && discriminator==2 && ad.getPackUuid().equals(packUuid)) {
							autoOpen = 1;
						} else if ((discriminator == null || discriminator==1) && uuid != null &&  ad.getUuid().equals(uuid)) {
							autoOpen = 1;
						}
					}
				}
			}
		}
		// ==================== 结束 == 是否自动打开 ==========================

		// ==================== 开始 == 拼接json字符串 ==========================
		JSONObject jo = new JSONObject();
		if (installType == null || "".equals(installType)) {
			installType = "1";
		}
		jo.put("imoduletype", imoduletype);
		jo.put("installType", installType);
		jo.put("autoOpen", autoOpen);// 是否自动打开 1：是 ；0：否
		if(pkgId == null){
			pkgId = 0;
		}
		jo.put("pkgId", pkgId);
		jo.put("uuid", app.getUuid());

		if (null != appPkg && !"".equals(appPkg)) {
			jo.put("pkgUUid", appPkg.getUuid());// app_install_pack(安装包唯一的id)
			jo.put("appName", app.getName());
			jo.put("sourcetype", app.getRscTypeId());
			if (appPkg.getCpUpdateTime() != null) {
				jo.put("updateTime", appPkg.getCpUpdateTime().replaceAll("[-\\s:]", ""));
			} else {
				jo.put("updateTime", "");
			}
			if ("cp01".equals(appPkg.getCp().getId()) || "cp02".equals(appPkg.getCp().getId()) || "cp09".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 0);
			} else if (null != appPkg.getCp().getId() && "cp03".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 2);
			} else if (null != appPkg.getCp().getId() && ("cp04".equals(appPkg.getCp().getId()) || "cp05".equals(appPkg .getCp().getId()))) {
				jo.put("cpid", 1);
			} else if (null != appPkg.getCp().getId() && "cp06".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 5);
			}

			if (appPkg.getVersion() != null || !"".equals(appPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("version", appPkg.getVersion());
			} else {
				jo.put("version", appPkg.getAppVer());
			}

			if (Os.IOS.equals(os) && isIOS == 1) {// IOS 的softUrl
				jo.put("url", String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim()));// appStore的地址
			} else {
				if (Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {// 沃商店 资源
					jo.put("url", pkgUrl.replaceAll("[,;]*",""));

					if (Os.ANDROID.equals(os)) {
						jo.put("version", appPkg.getVersion());
					} else if (Os.SYMBIAN.equals(os)) {
						jo.put("version", appPkg.getAppVer());
					}

				} else { // 自有资源
					String downloadUrl = pkgUrl.replaceAll("[,;]*", "");
					jo.put("url", downloadUrl);
				}
			}
			jarr.put(jo);
		}else if( null != IOSAppPkg ){
			jo.put("pkgUUid", IOSAppPkg.getUuid());// app_install_pack(安装包唯一的id)
			jo.put("appName", app.getName());
			jo.put("sourcetype", app.getRscTypeId());
			if (IOSAppPkg.getCpUpdateTime() != null) {
				jo.put("updateTime", IOSAppPkg.getCpUpdateTime().replaceAll("[-\\s:]", ""));
			} else {
				jo.put("updateTime", "");
			}
			if ("cp01".equals(IOSAppPkg.getCp().getId()) || "cp02".equals(IOSAppPkg.getCp().getId()) || "cp09".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 0);
			} else if (null != IOSAppPkg.getCp().getId() && "cp03".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 2);
			} else if (null != IOSAppPkg.getCp().getId() && ("cp04".equals(IOSAppPkg.getCp().getId()) || "cp05".equals(IOSAppPkg .getCp().getId()))) {
				jo.put("cpid", 1);
			} else if (null != IOSAppPkg.getCp().getId() && "cp06".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 5);
			}

			if (IOSAppPkg.getVersion() != null || !"".equals(IOSAppPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("version", IOSAppPkg.getVersion());
			} else {
				jo.put("version", IOSAppPkg.getAppVer());
			}

			if (Os.IOS.equals(os) && isIOS == 1) {// IOS 的softUrl
				jo.put("url", String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim()));// appStore的地址
			} else {
				if (Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {// 沃商店 资源
					jo.put("url", pkgUrl.replaceAll("[,;]*",""));

					if (Os.ANDROID.equals(os)) {
						jo.put("version", IOSAppPkg.getVersion());
					} else if (Os.SYMBIAN.equals(os)) {
						jo.put("version", IOSAppPkg.getAppVer());
					}

				} else { // 自有资源
					String downloadUrl = pkgUrl.replaceAll("[,;]*", "");
					jo.put("url", downloadUrl);
				}
			}
			jarr.put(jo);
		}
		
		// ==================== 结束 == 拼接json字符串 ==========================

		// 客户端调用下载地址后, app的downCount值+1
		resourceService.updateDownNum(app.getUuid());

		return jarr;
	}

	/**
	 * 软件下载
	 * 
	 * @param cpId
	 * @param os
	 * @param discriminator
	 * @param uuid
	 * @param packUuid
	 * 
	 * @return Map<String, Object>
	 * @throws CpServerAccessException
	 */
	public Map<String, String> downLoad(Integer discriminator,
			String uuid, String packUuid, Os os, String cpId)
			throws CpServerAccessException {
		
		Map<String, String> urlMap = new HashMap<String, String>();
		if (discriminator == null || 1 == discriminator) {// appUuid
			if (os != null) {
				urlMap.put("pkgUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, os, cpId)));
			} else {
				urlMap.put("AndroidUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.ANDROID, cpId)));
				urlMap.put("IOSUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.IOS, cpId)));
				urlMap.put("IOSXUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.IOSX, cpId)));
				urlMap.put("SymbianUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.SYMBIAN, cpId)));
				urlMap.put("JavaUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.JAVA, cpId)));
				urlMap.put("WinmobileUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.WINMOBILE, cpId)));
			}
		} else if (2 == discriminator) {// packUuid 
			urlMap.put("pkgUrl", appQuerier.getPackUrl(packUuid));
			if(os==null){
				urlMap.put("AndroidUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("IOSUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("IOSXUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("SymbianUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("JavaUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("WinmobileUrl", appQuerier.getPackUrl(packUuid));
			}
		}

		return urlMap;
	}

	/**
	 * ANDROID 、SYMBIAN 取沃商店资源，其它系统取灵动资源
	 * @param os
	 * 
	 * @return List<String>
	 */
	public List<String> cpIdList(Os os, Integer proId )	{
		List<String> cpIdList = new ArrayList<String>();
		//这个null只是为了能看详情页面，下载的时候还是要验证的手机操作系统的
		if (os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {
			cpIdList.add(AreaParam.CP_WOSTORE);
		} else {
			cpIdList.add(AreaParam.CP_I8APP);
		}
		return cpIdList;
	}
	
	
	/***************************************************************************
	 * 访问器
	 **************************************************************************/
	public AppQuerier getAppQuerierImpl() {
		return appQuerier;
	}

	public void setAppQuerierImpl(AppQuerier appQuerierImpl) {
		this.appQuerier = appQuerierImpl;
	}

	public Pager<AppDTO> getAppPager() {
		return appPager;
	}

	public void setAppPager(Pager<AppDTO> appPager) {
		this.appPager = appPager;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public int getAppOsType() {
		return appOsType;
	}

	public void setAppOsType(int appOsType) {
		this.appOsType = appOsType;
	}

	public List<ResourceType> getAppTypeList() {
		return appTypeList;
	}

	public void setAppTypeList(List<ResourceType> appTypeList) {
		this.appTypeList = appTypeList;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public AppDTO getApp() {
		return app;
	}

	public void setApp(AppDTO app) {
		this.app = app;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public Pkg getPkg() {
		return pkg;
	}

	public void setPkg(Pkg pkg) {
		this.pkg = pkg;
	}
	
	public int getPkgId() {
		return pkgId;
	}

	public void setPkgId(int pkgId) {
		this.pkgId = pkgId;
	}

	public boolean isOrderBy() {
		return orderBy;
	}

	public void setOrderBy(boolean orderBy) {
		this.orderBy = orderBy;
	}

	public List<Pkg> getPkgList() {
		return pkgList;
	}

	public void setPkgList(List<Pkg> pkgList) {
		this.pkgList = pkgList;
	}

	public List<AppDTO> getAppList() {
		return appList;
	}

	public void setAppList(List<AppDTO> appList) {
		this.appList = appList;
	}

	public String getInstallType() {
		return installType;
	}

	public void setInstallType(String installType) {
		this.installType = installType;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public int getIsIOS() {
		return isIOS;
	}

	public void setIsIOS(int isIOS) {
		this.isIOS = isIOS;
	}

	public String getImoduletype() {
		return imoduletype;
	}

	public void setImoduletype(String imoduletype) {
		this.imoduletype = imoduletype;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Date getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(Date cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}

	public String getPackUuid() {
		return packUuid;
	}

	public void setPackUuid(String packUuid) {
		this.packUuid = packUuid;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
