package com.i8app.shop.web.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants.OrderBy;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.common.config.Config_i8;
import com.i8app.shop.common.wsclient.SmsSender;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.Pkg;
import com.i8app.shop.domain.PkgGroup;
import com.i8app.shop.domain.ResourceType;
import com.i8app.shop.service.AppService;
import com.i8app.shop.service.EmpService;
import com.i8app.shop.service.ISmsService;
import com.i8app.shop.service.InstallLogService;
import com.i8app.shop.service.RecommendService;
import com.i8app.shop.service.ResourceService;

@Controller
@Scope("prototype")
public class WoplusAction extends BaseAction {
	private static final Logger logger = Logger.getLogger(WoplusAction.class);
	
	@Resource
	private ResourceService resourceService;
	@Resource
	private InstallLogService installLogService;
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private EmpService empService;
	@Resource
	private ISmsService smsHessianService;
	@Resource
	private SmsSender smsSender;
	@Resource
	private Config_i8 config_i8;
	@Resource
	private RecommendService recommendService;
	@Resource
	private AppService appService;
	
	
	private List<Pkg> pkgList;
	private List<Pkg> pkgList_City;//单独存放地市包的（广东专用）
	private List<Pkg> pkgList_Type;//分类业务集合（广东新增的业务品种）
	private Pager<AppDTO> appPager;
	private List<AppDTO> appList;// 不分页的
	private List<ResourceType> appTypeList;
	private Map<String, List<ResourceType>> appTypeMap;
	private Map<ResourceType, List<AppDTO>> typeMap = new LinkedHashMap<ResourceType, List<AppDTO>>();//应用宝库二级页面
	private Map<String, Pager<AppDTO>> appMap;//如果将来自定义包不用该变量，则给予删除
	
	
	private AppDTO app;
	private Pkg pkg;
	private ResourceType resType;
	
	private String installType = "1";
	private String version, typeId, keyword, packUuid, imoduletype, ids, phoneNo; 
	private int empId, appOsType, isIOS, pkgId, bizType;
	private String msisdn;//广东个性化推荐接口
	private int pageNumber;
	private int cacheType;
	
	
	/** =================================== 查询数据模块 ====================================== */
	
	/**
	 * 参数说明：
	 * @param empId（营业员id）
	 * @param appOsType（操作系统）
	 * @param version（操作系统版本）
	 * @param isIOS 是否为正版下载地址 (0：非正版的；1：正版下载地址 (SoftUrl值，是CP_APPSTORE 的值))
	 * @param installType 安装方式 		见 redmine 灵动一键装机系统 的 讨论区
	 * @param pageSize （每页显示的应用个数）
	 * 
	 * @param pkgId pkg 的 id(主要是包选项页做切换的时候能用到，默认拿第一个包的软件)
	 * @param bizType 
	 * @param keyword 搜索用的关键字
	 * @param typeId （app类别：null 或者 101开头的为软件；102开头的为游戏。）
	 * @param imoduletype 见 redmine 灵动一键装机系统 的 讨论区
	 * @param ids (包括：discriminator_uuid_packUuid)
	 * @param pageNumber 当前第几页，专用于分类查询（广东）弹出的窗口页面。
	 * @param cacheType 首页软件缓存类别，0：获取主页面所有业务的软件（沃商店全国包、必备包、地市包）；1：获取 猜你喜欢、分类查询（广东） 等 业务的软件
	 * 
	 * cp商对应的业务：
	 * 		沃商店: android+symbian = cp03. 其它cp09
			广东: android=cp04. 其它cp09.
			山西: 所有=cp09
	 */
	
	
	
	/** #################################### 广东wo+新版大改动  ################################### */
	
	
	/**
	 * 0、广东wo+	智能推荐接口
	 * @param msisdn
	 * @param empId
	 * @param appOsType
	 * @param version
	 * @param pageSize
	 */
	public String recommendIndex() {
		
		try {
			appList = new ArrayList<AppDTO>();
			if(msisdn != null && !"".equals(msisdn)){//刚开始没有输入手机号码，不调个性化接口
				
				//获取推荐app列表		业务编号：03
				String appStr = recommendService.recommendList(msisdn,"03");
				JSONObject aJson = new JSONObject(appStr);
				String resultCode = aJson.get("resultCode").toString();
				if("0000".equals(resultCode)){
					JSONArray aList = new JSONArray(aJson.get("offers").toString());
					for (int i = 0; i < aList.length(); i++) {
						JSONObject ajson = aList.getJSONObject(i);
						String appid = ajson.get("appId").toString();
						if(appid != null && !"".equals(appid)){
							AppInstallPack pack = resourceService.findByAXONAppId(appid);
							if(pack != null){
								com.i8app.shop.domain.AppDTO appDto = resourceService.findAppByid(pack.getApp().getUuid());
								if( appDto != null){
									
									app = new AppDTO();
									app.setUuid(appDto.getUuid());
									app.setName(appDto.getName());
									app.setRscTypeId(appDto.getResourceType().getId());
									app.setRscTypeName(appDto.getResourceType().getName());
									app.setIcon(appDto.getIcon());
									app.setInfo(appDto.getInfo());
									app.setPackUuid(pack.getUuid());
									app.setPackOs(pack.getOs());
									app.setPackCpId(pack.getCp().getId());
									app.setPackCpUpdateTime(pack.getCpUpdateTime());
									app.setPackFileSize(pack.getFileSize());
									
								}
								appList.add(app);
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//没有取到值,或抛异常时,或里面没有匹配得上的安讯资源，用我们自己的资源代替
		if(appList.isEmpty())	{
			bizType = 10;
			appList = this.findAppPager(appOsType, version, keyword, typeId, false, null, empId, false, pageSize).getList();
		}
		
		return SUCCESS;
	}
	
	/**
	 * 0-1	广东智能推荐接口2
	 * @param msisdn
	 * @param servType
	 * @param empId
	 * @param appOsType
	 * @param version
	 * @param pageSize
	 
	public String recommendContent() {
		
		try {
			pkgList = new ArrayList<PkgDTO>();
			appList = new ArrayList<AppDTO>();
			if(msisdn != null && !"".equals(msisdn)){//刚开始没有输入手机号码，不调个性化接口
				
				if("01".equals(servType)){
					//获取推荐套餐包列表		业务编号：01
					String packStr = recommendService.recommendList(msisdn,servType);
					
					JSONObject pJson = new JSONObject(packStr);
					String resultCode = pJson.get("resultCode").toString();
					if("0000".equals(resultCode)){
						JSONArray pList = new JSONArray(pJson.get("offers").toString());
						for (int i = 0; i < pList.length(); i++) {
							JSONObject pjson = pList.getJSONObject(i);
							pkg = new PkgDTO();
							pkg.setInfo(pjson.get("packageId").toString());
							pkg.setName(pjson.get("packageName").toString());
							pkgList.add(pkg);
						}
					}
					
				}else if("03".equals(servType)){
					//获取推荐app列表		业务编号：03
					String appStr = recommendService.recommendList(msisdn,"03");
					JSONObject aJson = new JSONObject(appStr);
					String resultCode = aJson.get("resultCode").toString();
					if("0000".equals(resultCode)){
						JSONArray aList = new JSONArray(aJson.get("offers").toString());
						for (int i = 0; i < aList.length(); i++) {
							JSONObject ajson = aList.getJSONObject(i);
							String appid = ajson.get("appId").toString();
							if(appid != null && !"".equals(appid)){
								AppInstallPack pack = resourceService.findByAXONAppId(appid);
								if(pack != null){
									com.i8app.shop.domain.AppDTO appDto = resourceService.findAppByid(pack.getApp().getUuid());
									if( appDto != null){
										
										app = new AppDTO();
										app.setUuid(appDto.getUuid());
										app.setName(appDto.getName());
										app.setRscTypeId(appDto.getResourceType().getId());
										app.setRscTypeName(appDto.getResourceType().getName());
										app.setIcon(appDto.getIcon().getUrl());
										app.setInfo(appDto.getInfo());
										app.setPackUuid(pack.getUuid());
										app.setPackOs(pack.getOs());
										app.setPackCpId(pack.getCp().getId());
										app.setPackCpUpdateTime(pack.getCpUpdateTime());
										app.setPackFileSize(pack.getFileSize());
										
									}
									appList.add(app);
								}
							}
						}
					}
					//里面没有匹配得上的安讯资源，用我们自己的资源代替
					if(appList.size() <= 0){
						appList = this.findAppPager(appOsType, version, keyword, typeId, false, null, empId, false, pageSize).getList();
					}
				}
			}else{
				if("03".equals(servType)){
					appList = this.findAppPager(appOsType, version, keyword, typeId, false, null, empId, false, pageSize).getList();
				}
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}*/
	
	
	/** 
	 * 1、 wo+首页	 缓存首页包数据
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType
	 * @param imoduletype
	 * @param pageSize
	 * @param cacheType
	 * 
	 * @return appPager and appList
	 */	
	public String appCaching(){
		
		JSONArray jarr = new JSONArray();
		
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		//缓存数据根据缓存类型分批获取
		int[] bizArr = new int[5];
		if(cacheType == 0){
			if("1".equals(woJia_NATION_PKG)){
				bizArr[0] = AreaParam.BIZ_WOSTORE_NATION_PKG;
				bizArr[1] = AreaParam.BIZ_ESSENTIAL;
				bizArr[2] = AreaParam.BIZ_CITY_PKG;
			}else{
				bizArr[0] = AreaParam.BIZ_ESSENTIAL;
				bizArr[1] = AreaParam.BIZ_CITY_PKG;
			}
		}else if(cacheType == 1){
			bizArr[0] = AreaParam.BIZ_HOME_RECOMMEND_PKG;//首页推荐
			bizArr[1] = AreaParam.BIZ_CLASSIFY_PKG;//分类查询（广东）
		}
		
		for (int i = 0; i < bizArr.length; i++) {
			/**11.首页全国包;
			12.首页地市包;
			13.首页手机必备包;
			18.首页推荐
			19.分类查询（广东）
			20.默认自定义包
			21.精品推荐**/
			bizType = bizArr[i];
			if(bizType == 1){
				imoduletype = "13";
			}else if(bizType == 8){
				imoduletype = "12";
			}else if(bizType == 9){
				imoduletype = "11";
			}else if(bizType == 10){
				imoduletype = "18";
			}else if(bizType == 11){
				imoduletype = "19";
			}
			
			List<PkgGroup> wostoreNationPkgList = appService.findPkgGroupByBizId(bizType, ap);
			for (PkgGroup pkgGroupDTO : wostoreNationPkgList) {
				List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupDTO.getId());
				for (int j = 0; j < pkgDTOList.size(); j++) {
					Pkg p = pkgDTOList.get(j);
					//根据子包查询该包下所有的软件
					appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应必备包的软件列表
					if(appPager != null && appPager.getList() != null && appPager.getList().size() > 0){//如果有数据则拼接数据
						for (AppDTO app : appPager.getList()) {
							// 下载
							try {
								jarr = this.downMsg(jarr, app, app.getDiscriminator(), app.getUuid(), app.getPackUuid(), os, ap, p.getId(), imoduletype, isIOS, installType, pageSize);
							} catch (JSONException e) {
								e.printStackTrace();
							} catch (CpServerAccessException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		
		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "msginfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this.doPrint(joMap.toString());
	}
	
	
	/**
	 * ###########################################		wo+首页		###########################################
	 * 
	 * 2、wo+首页	框架集
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return
	 */
	public String homeMain(){
		
		return SUCCESS;
	}
	
	/**
	 * 3、wo+ 首页	左侧框架
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return appPager and pkg
	 */
	public String homeLeft(){
		
		return SUCCESS;
	}
	
	
	/**
	 * 3、wo+ 首页 
	 * 
	 * 猜你喜欢
	 *  
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return appPager and pkg
	 */
	public String homeLeftApp(){
		
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id 
		
		List<PkgGroup> recommendPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_HOME_RECOMMEND_PKG, ap);
		if (recommendPkgList != null && recommendPkgList.size() > 0) {
			for (PkgGroup pkgGroup : recommendPkgList) {
				List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroup.getId());// 获取该包组的所有子包
				if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
					for (int j = 0; j < pkgDTOList.size(); j++) {
						pkg = pkgDTOList.get(j);
						//根据子包查询该包下所有的软件
						appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize); // 查找对应必备包的软件列表
					}
				}
			}
		}
		imoduletype="18";//下载的数据来源：首页推荐
		
		return SUCCESS;
	}
	
	/**
	 * wo+ 首页 
	 * 
	 * 广告图片
	 *  
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return appPager and pkg
	 */
	public String homeLeftPic(){
		
		Employee emp = empService.findById(empId);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		//##############################	首页广告业务包		###############################
		//各个省份的广告
		pkgList = new ArrayList<Pkg>();
		List<PkgGroup> activityPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_ADVERTISE_PKG, ap);
		if (activityPkgList != null && activityPkgList.size() > 0) {
			for (PkgGroup pkgGroup : activityPkgList) {
				pkgList = appService.findPkgByGroupId(pkgGroup.getId());
				if (pkgList != null && !pkgList.isEmpty() && pkgList.size() > 0) {
					for (int i = 0; i < pkgList.size(); i++) {
						Pkg p = pkgList.get(i);
						p.setInfo(AreaParam.BIZ_ADVERTISE_PKG+"");
					}
				}
			}
		}
		
		//全国统一的广告
		ap.setProvId(1000);
		ap.setCityId(0);
		activityPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_ADVERTISE_PKG, ap);
		if (activityPkgList != null && activityPkgList.size() > 0) {
			for (PkgGroup pkgGroup : activityPkgList) {
				List<Pkg> pkgList_qg = appService.findPkgByGroupId(pkgGroup.getId());
				if (pkgList_qg != null && !pkgList_qg.isEmpty() && pkgList_qg.size() > 0) {
					for (int i = 0; i < pkgList_qg.size(); i++) {
						Pkg p = pkgList_qg.get(i);
						p.setInfo(AreaParam.BIZ_ADVERTISE_PKG+"");
						pkgList.add(p);
					}
				}
			}
		}
		return SUCCESS;
	}
	
	
	/**
	 * 4、wo+首页  主体1
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return appPager and appList 
	 */
	public String homeRight() {
		
		Employee emp = empService.findById(empId);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id

		// 根据不同的业务获取不同的包组
		pkgList = new ArrayList<Pkg>();
		pkgList_City = new ArrayList<Pkg>();
		pkgList_Type = new ArrayList<Pkg>();
		
		//根据各省不同的要求配置不同的业务
		int[] bizArr = new int[10];
		if("1".equals(woJia_NATION_PKG)){
			bizArr[0] = AreaParam.BIZ_WOSTORE_NATION_PKG;
			bizArr[1] = AreaParam.BIZ_ESSENTIAL;
		}else{
			bizArr[0] = AreaParam.BIZ_ESSENTIAL;
		}
		
		for (int i = 0; i < bizArr.length; i++) {
			bizType = bizArr[i];
			List<PkgGroup> wostoreNationPkgList = appService.findPkgGroupByBizId(bizType, ap);
			for (PkgGroup pkgGroup : wostoreNationPkgList) {
				List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroup.getId());
				if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
					for (int n = 0; n < pkgDTOList.size(); n++) {
						Pkg p = pkgDTOList.get(n);
						p.setInfo(bizType+"");
						pkgList.add(p);
						if(pkg == null && n == 0){
							pkg = pkgDTOList.get(0);
						}
					}
				}
			}
		}
		
		/** ================= 地市包 ================= **/
		List<PkgGroup> cityPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_CITY_PKG, ap);
		if (cityPkgList != null && cityPkgList.size() > 0) {
			for (PkgGroup pkgGroup : cityPkgList) {
				List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroup.getId());// 获取该包组的所有子包
				if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
					for (int j = 0; j < pkgDTOList.size(); j++) {
						Pkg p = pkgDTOList.get(j);
						p.setInfo(AreaParam.BIZ_CITY_PKG+"");
						pkgList_City.add(p); //添加所有地市包
						if(pkg == null){
							pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的包
						}
					}
				}
			}
		}
		
		//################################  分类 业务包   ################################
		List<PkgGroup> typePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_CLASSIFY_PKG, ap); 
		if (typePkgList != null && typePkgList.size() > 0) {
			for (PkgGroup pkgGroup : typePkgList) {
				List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroup.getId());// 获取该包组的所有子包
				if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
					for (int j = 0; j < pkgDTOList.size(); j++) {
						Pkg p = pkgDTOList.get(j);
						p.setInfo(AreaParam.BIZ_CLASSIFY_PKG+"");
						pkgList_Type.add(p); //添加所有分类包
					}
				}
			}
		}

		return SUCCESS;
	}
	
	/**
	 * 5、wo+首页	主体2
	 * 
	 * 根据pkg主键 以及 对应的业务 查询对应的软件列表
	 * 
	 * @param empId 
	 * @param appOsType 
	 * @param version 
	 * @param pageSize 
	 * 
	 * @param pkgId 
	 * @param bizType 
	 * 
	 * @return appPager and appList
	 */
	public String findAppByPkg() {
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		appPager = appQuerier.findInstallPackByPkg(ap, pkgId, os, offset, pageSize); // 查找对应必备包的软件列表
		return SUCCESS;
	}
	
	/**
	 * 6、wo+首页	分类查询（广东）
	 * 
	 * 根据pkg主键 以及 对应的业务 查询对应的软件列表
	 * 
	 * @param appOsType 
	 * @param version 
	 * @param empId 
	 * @param pageSize 
	 * 
	 * @param pkgId 
	 * 
	 * @return appPager and appList
	 */
	public String classifyList() {
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		appPager = appQuerier.findInstallPackByPkg(ap, pkgId, os, offset, 20); // 查找对应必备包的软件列表
		pageNumber = pageNumber +1;
		
		//如果没有查到就从头开始查
		if(appPager.getList().size() == 0){
			appPager = appQuerier.findInstallPackByPkg(ap, pkgId, os, 0, 20); // 查找对应必备包的软件列表
			pageNumber = 0;
		}
		
		return SUCCESS;
	}
	

	
	/**
	 *  ######################################		wo+应用		#########################################
	 * 
	 * 7、wo+应用	总框架
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return
	 */
	public String appMain(){
		
		return SUCCESS;
	}
	
	/**
	 * 8、wo+应用	左侧导航框架
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return
	 */
	public String appGPS(){
		
		//查询应用和游戏的分类
		appTypeMap = new HashMap<String, List<ResourceType>>();
		appTypeMap.put("101", resourceService.appTypeList("101"));
		appTypeMap.put("102", resourceService.appTypeList("102"));
		
		return SUCCESS;
	}
	
	/**
	 * 9、wo+应用	右侧软件列表框架
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @return
	 */
	public String appIndex(){
		
		return SUCCESS;
	}
	
	/**
	 * 10、wo+应用	顶部搜索框架
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * @param imoduletype
	 * 
	 * @return
	 */
	public String appTop(){
		
		return SUCCESS;
	}
	
	/**
	 * 11、wo+应用	 主页
	 * 
	 * 根据精品推荐的业务  或者 应用的 二级类别查询对应的软件
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @param typeId
	 * 
	 * @return 
	 */
	public String appLeft(){
		
		Os os = this.adaptToOs(appOsType, version);
		Employee emp = empService.findById(empId);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		//################################  精品推荐  业务包  ################################ 
		if(typeId == null || "".equals(typeId)){
			imoduletype = "21";
			List<PkgGroup> recommendPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_BOUTIQUE_RECOMMEND_PKG, ap);
			if (recommendPkgList != null && recommendPkgList.size() > 0) {
				for (PkgGroup pkgGroup : recommendPkgList) {
					List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroup.getId());// 获取该包组的所有子包
					if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
						for (int j = 0; j < pkgDTOList.size(); j++) {
							Pkg p = pkgDTOList.get(j);
							p.setInfo(AreaParam.BIZ_BOUTIQUE_RECOMMEND_PKG+"");
							//根据子包查询该包下所有的软件
							appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应必备包的软件列表
						}
					}
				}
			}
		} else {
			imoduletype = "10";
			resType = resourceService.findAppType(typeId);
			appPager = this.findAppPager(appOsType, version, keyword, typeId, false, null, empId, false, pageSize);
		}
		
		//##############################	活动业务包 		###############################
		//各个省份的活动
		List<PkgGroup> activityPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_ACTIVITY, ap);
		if (activityPkgList != null && activityPkgList.size() > 0) {
			for (PkgGroup pkgGroup : activityPkgList) {
				pkgList = appService.findPkgByGroupId(pkgGroup.getId());
				if (pkgList != null && !pkgList.isEmpty() && pkgList.size() > 0) {
					for (int i = 0; i < pkgList.size(); i++) {
						Pkg p = pkgList.get(i);
						p.setInfo(AreaParam.BIZ_ACTIVITY+"");
					}
				}
			}
		}
		
		//全国统一的活动
		ap.setProvId(1000);
		ap.setCityId(0);
		activityPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_ACTIVITY, ap);
		if (activityPkgList != null && activityPkgList.size() > 0) {
			for (PkgGroup pkgGroup : activityPkgList) {
				List<Pkg> pkgList_qg = appService.findPkgByGroupId(pkgGroup.getId());
				if (pkgList_qg != null && !pkgList_qg.isEmpty() && pkgList_qg.size() > 0) {
					for (int i = 0; i < pkgList_qg.size(); i++) {
						Pkg p = pkgList_qg.get(i);
						p.setInfo(AreaParam.BIZ_ACTIVITY+"");
						pkgList.add(p);
					}
				}
			}
		}
		
		return SUCCESS;
	}
	
	/**
	 * wo+ 根据包ID查询广告列表
	 * 
	 * 根据pkg主键 以及 对应的业务 查询对应的软件列表
	 * 
	 * @param empId 
	 * @param appOsType 
	 * @param version 
	 * @param pageSize 
	 * 
	 * @param pkgId 
	 * @param bizType 
	 * 
	 * @return appPager and appList
	 */
	public String activityAppList() {
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version);
		
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize); // 查找对应必备包的软件列表
		
		return SUCCESS;
	}
	
	/**
	 * 12、wo+应用	 排行榜 
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @param typeId
	 * 
	 * @return appPager、pkg
	 */
	public String appRight() {
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version); 
		AreaParam ap = new AreaParam(); 
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);//运营商id
		
		List<PkgGroup> wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_RANK, ap); 
		if(wostorePkgList != null && wostorePkgList.size()>0){ 
			for (PkgGroup pkgGroup : wostorePkgList) 
			{ 
				List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroup.getId());//获取该包组的所有子包
				if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
					for (int i = 0; i < pkgDTOList.size(); i++) {
						if(typeId.startsWith("101")){//应用排行
							if(pkgDTOList.size()>=1){
								pkg = pkgDTOList.get(0);
								appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize); // 查找对应必备包的软件列表
							}
						}else if(typeId.startsWith("102")){//游戏排行
							if(pkgDTOList.size()>=2){
								pkg = pkgDTOList.get(1);
								appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize); // 查找对应必备包的软件列表
							}
						}else if(typeId == null || "".equals(typeId)){//软件总排行
							if(pkgDTOList.size()>=3){
								pkg = pkgDTOList.get(2);
								appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize); // 查找对应必备包的软件列表
							}
						}
					}
				}
			}
		}
		return SUCCESS; 
	 }
	
	/**
	 * 13、搜索页面
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @param keyword 
	 * 
	 * @param appPager
	 */
	public String search() {
		typeId = null;//主要是为了能搜出所有的软件
		appPager = this.findAppPager(appOsType, version, keyword, typeId, false, null, empId, true, pageSize);
		return SUCCESS;
	}
	
	/**
	 * 14、软件详情
	 * 
	 * 根据安装包ID, 查询单个软件的详细信息.包括截图 
	 * 
	 * @param empId
	 * @param appOsType
	 * @param version 
	 * @param isIOS 
	 * @param installType 
	 * @param pageSize
	 * 
	 * @param app.uuid
	 * @param packUuid
	 * @param imoduletype
	 * 
	 * @return AppDTO
	 */
	public String appInfo() {
		//来自应用首页的软件，该处软件没有packUuid，只能根据Uuid和OS查询详情
		if(imoduletype != null && (packUuid == null || "".equals(packUuid)) && ("10".equals(imoduletype))){
			Os os = this.adaptToOs(appOsType, version);
			app = appQuerier.findSingle(app.getUuid(), os);
		}else{
			app = appQuerier.findSinglePack(packUuid);
		}
		return SUCCESS;
	}
	
	
	/** =================================== 下载安装 模块 ====================================== */ 
	/**
	 * 短信下载  
	 * 
	 * ios短信下载改成: 发送itunes链接, 不使用短链
	 * 
	 * @param empId
	 * @param installType
	 * 
	 * @param packUuid 
	 * @param phoneNo
	 * @param imoduletype 
	 * @param appOsType
	 * @param version
	 * 
	 * @return null
	 * @throws CpServerAccessException
	 */
	public String duanXinDown() throws CpServerAccessException {

		Employee emp = empService.findById(empId);
		if (emp == null)
			throw new IllegalArgumentException(
					"no matching areacode of this province id on empId :" + empId);
		
		int logId = installLogService.buildSmsInstallLog(Integer.parseInt(installType), empId, packUuid, phoneNo, 
				70004, SessionAccessor.getPhoneLoginUuid().toString(), imoduletype);
		
		String areaCode = config_i8.getProvAreaMap().get(emp.getProvince().getId());
		try {
			//IOS的操作系统的短信全部发送itunes链接
			Os os = this.adaptToOs(appOsType, version);
			if(Os.IOS.equals(os) || Os.IOSX.equals(os)){
				AppInstallPack IOSAppPkg = resourceService.findIOSAppPackByPkgUuid(packUuid);//根据 packUuid 获取正版url
				String url = String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim());
				
				smsSender.sendDownloadLink(areaCode, phoneNo, IOSAppPkg.getApp().getName(), url);

			}else{
				smsHessianService.sendShortUrl(Integer.parseInt(installType), phoneNo, areaCode, packUuid, logId);// 该packUuid是从页面上传入的
			}
			
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
	/**
	 * 批量下载、强制安装、隐藏安装
	 * 
	 * 根据 操作系统、软件类别、uuid 或 packUuid 获取下载信息 (统一下载方法)
	 * @param empId
	 * @param appOsType
	 * @param version
	 * @param ids 
	 * @param pkgId
	 * @param imoduletype
	 * 
	 * @return JSONObject
	 * @throws CpServerAccessException
	 */
	public String downLoad() throws CpServerAccessException {
		
		JSONArray jarr = new JSONArray();
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version);

		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id

		// =============================== 页面下载 ===============================
		if (ids != null && !"".equals(ids)) {
			String[] idsstr = ids.split(",");
			for (String disc : idsstr) {
				String[] disc_str = disc.split("_");
				String uuid = null;
				String packUuid = null;
				Integer discriminator = null;
				if (disc_str.length > 0) {
					uuid = disc_str[0];
				}
				if (disc_str.length > 1) {
					packUuid = disc_str[1];
				}
				if (disc_str.length > 2) {
					if (disc_str[2] != null && !"null".equals(disc_str[2]) && !"".equals(disc_str[2])) {
						discriminator = Integer.parseInt(disc_str[2]);
					}
				}
				// 下载 
				try {
					jarr = this.downMsg(jarr, app, discriminator, uuid, packUuid, os, ap, pkgId, imoduletype, isIOS, installType, pageSize);
				} catch (JSONException e) {
					e.printStackTrace();
				} 

			}
		} else {
			List<PkgGroup> wostorePkgList = new ArrayList<PkgGroup>();
			// 根据不同的业务获取不同的包组
			if ("2".equals(imoduletype)) {// ========================= 强制安装 ====================
				wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_FORCEINSTALL, ap);
			} else if ("3".equals(imoduletype)) {// ==================  隐藏安装  ================
				wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_HIDDEN_INSTALL, ap);
				os = null;
			}
			if (wostorePkgList != null && wostorePkgList.size() > 0) {
				Integer pkgGroupId = null;
				for (int i = 0; i < wostorePkgList.size(); i++) {
					pkgGroupId = wostorePkgList.get(0).getId();// 获取第一个包的主键
				}

				pkgList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包的包组
				if (pkgList != null && pkgList.size() > 0) {
					Pkg pkg = null;
					for (int i = 0; i < pkgList.size(); i++) {// 里面只有一个包
						pkg = pkgList.get(0);
					}

					if (pkg != null) {
						appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize); 
						appList = appPager.getList();

						for (AppDTO app : appList) {
							// 下载
							try {
								jarr = this.downMsg(jarr, app, app.getDiscriminator(), app.getUuid(), app.getPackUuid(), os, ap, pkg.getId(), imoduletype, isIOS, installType, pageSize);
								
							} catch (JSONException e) {
								e.printStackTrace();
							} 
						}
					}
				}
			}
		}

		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "msginfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this.doPrint(joMap.toString());
	}
	
	
	
	
	/** =================================== 内置方法 模块 ====================================== */
	
	/**
	 * 查询软件列表
	 * 
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param pageSize
	 * @param cpUpdateTime
	 * @param keyword
	 * @param typeId
	 * @param orderBy
	 * @param isSearch
	 * 
	 * @return Pager<AppDTO>
	 */
	public Pager<AppDTO> findAppPager(int appOsType, String version, String keyword, 
			String typeId, boolean orderBy, Date cpUpdateTime, int empId, boolean isSearch, int pageSize) {
		
		AppParam appp = new AppParam();
		if("".equals(keyword)){
			keyword = null;
		}
		appp.setName(keyword);
		Os os = this.adaptToOs(appOsType, version);
		List<String> cpIdList = this.cpIdList(os);
		appp.setCpIds(cpIdList);

		if (orderBy) {// 是否根据时间排序,只有为true才会做排序，为 true 则排序
			appp.setOrderBy(OrderBy.cpUpdateTime);
			appp.setCpUpdateTime(cpUpdateTime);//cpUpdateTime 查询三个月内的数据 -- 暂时放到这,null则为无时间断限制
		}
		if (typeId == null || "".equals(typeId)) {
			appp.setRscTypeId(AppParam.RSCTYPE_APP);
		} else {
			appp.setRscTypeId(typeId);
		}
		Employee emp = empService.findById(empId);
		AreaParam areap = new AreaParam();
		areap.setProvId(emp.getProvince().getId());
		areap.setCityId(emp.getCity().getId());
		areap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		Pager<AppDTO> appPager = null;
		if(isSearch){//做关键字搜索
			appPager = appQuerier.search(areap, os, appp, offset, pageSize);// 里面的packUuid不正确
		}else {
			appPager = appQuerier.queryPack(areap, os, appp, offset, pageSize);// 里面的packUuid不正确
		}
		return appPager;
	}
	
	/**
	 * 拼接单个json字符对象
	 * 
	 * @param jarr
	 * @param app
	 * @param discriminator
	 * @param uuid
	 * @param packUuid
	 * @param os
	 * @param ap
	 * @param pkgId
	 * @param imoduletype
	 * 
	 * @return JSONArray
	 * @throws CpServerAccessException
	 * @throws JSONException
	 */
	public JSONArray downMsg(JSONArray jarr, AppDTO app, Integer discriminator, String uuid, 
			String packUuid, Os os, AreaParam ap, Integer pkgId, String imoduletype, 
			Integer isIOS, String installType, Integer pageSize)
			throws CpServerAccessException, JSONException {

		// ============================ 开始 == 获取下载地址 ===================================

		if (app == null && uuid != null && !"".equals(uuid)) {
			app = appQuerier.findSingle(uuid, os);//这个查询只为获取名称和类别等信息，uuid和packUuid是以外面调用的时候传入的为准
		}
		String cpId = null;
		if (os == null || Os.ANDROID.equals(os)) {
			cpId = AreaParam.CP_AXON;
		} else {
			cpId = AreaParam.CP_I8APP;
		}
		
		/**
		 * 	非正版的操作系统,则根据 discriminator 、packUuid 或 appUuid 、os、cpId 查询 AppInstallPack 对象
		 *  如果是正版的操作系统，并且isIOS == 1 获取正版包对象 
		 * 业务：
		 * 		1、根据 appUuid 查询 AppInstallPack, 获取 ipaItemId 不为空的对象,如果根据这个获取不到，那就是没有数据。
		 * 		2、根据 packUuid 查询 AppInstallPack, 获取 ipaItemId 不为空的对象；
		 * 			如果为空, 那么再根据 刚查到的对象的 ipaBundleId 字段查询 AppInstallPack ,获取 ipaItemId 不为空的对象 
		 */
		AppInstallPack IOSAppPkg = null;
		AppInstallPack appPkg =  null;
		if (Os.IOS.equals(os) && isIOS == 1) {//获取正版url
			IOSAppPkg = resourceService.findIOSAppPackByPkgUuid(packUuid);//根据 packUuid 获取正版url
			
		}else if(isIOS != 1){//不获取正版地址
			appPkg = resourceService.findAppPkgByid(packUuid);
			
		}
		
		//获取非正版的其他下载地址
		Map<String, String> urlMap = this.downLoad(discriminator, uuid, packUuid, os, cpId);
		String pkgUrl = urlMap.get("pkgUrl");

		// ============================ 结束 == 获取下载地址 ================================

		// ==================== 开始 == 是否自动打开 ==========================
		int autoOpen = 0;// 是否自动打开 1：是 ；0：否
		List<PkgGroup> autoOpenPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_AUTO_OPEN, ap);// 是否自动打开
		if (autoOpenPkgList != null && autoOpenPkgList.size() > 0) {
			Integer pkgGroupId = null;
			for (int i = 0; i < autoOpenPkgList.size(); i++) {
				pkgGroupId = autoOpenPkgList.get(0).getId();// 获取第一个包组的主键
			}
			List<Pkg> pkgList = appService.findPkgByGroupId(pkgGroupId);// 获取第一个包组的所有子包
			Pkg p = null;
			if (pkgList != null && pkgList.size() > 0) {
				for (int i = 0; i < pkgList.size(); i++) {// 里面只有一个包
					p = pkgList.get(0);
				}
				if (p != null) {
					appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize);// 查找对应必备包的软件列表
					appList = appPager.getList();
					for (AppDTO ad : appList) {
						if (packUuid != null && discriminator==2 && ad.getPackUuid().equals(packUuid)) {
							autoOpen = 1;
						} else if ((discriminator == null || discriminator==1) && uuid != null &&  ad.getUuid().equals(uuid)) {
							autoOpen = 1;
						}
					}
				}
			}
		}
		// ==================== 结束 == 是否自动打开 ==========================

		// ==================== 开始 == 拼接json字符串 ==========================
		JSONObject jo = new JSONObject();
		if (installType == null || "".equals(installType)) {
			installType = "1";
		}
		jo.put("imoduletype", imoduletype);
		jo.put("installType", installType);
		jo.put("autoOpen", autoOpen);// 是否自动打开 1：是 ；0：否
		if(pkgId == null){
			pkgId = 0;
		}
		jo.put("pkgId", pkgId);
		jo.put("uuid", app.getUuid());

		if (null != appPkg && !"".equals(appPkg)) {
			jo.put("pkgUUid", appPkg.getUuid());// app_install_pack(安装包唯一的id)
			jo.put("appName", app.getName());
			jo.put("originalPackId", appPkg.getOriginalPackId());
			jo.put("sourcetype", app.getRscTypeId());
			if (appPkg.getCpUpdateTime() != null) {
				jo.put("updateTime", appPkg.getCpUpdateTime().replaceAll("[-\\s:]", ""));
			} else {
				jo.put("updateTime", "");
			}
			if ("cp01".equals(appPkg.getCp().getId()) || "cp02".equals(appPkg.getCp().getId()) || "cp09".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 0);
			} else if (null != appPkg.getCp().getId() && "cp03".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 2);
			} else if (null != appPkg.getCp().getId() && ("cp04".equals(appPkg.getCp().getId()) || "cp05".equals(appPkg .getCp().getId()))) {
				jo.put("cpid", 1);
			} else if (null != appPkg.getCp().getId() && "cp06".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 5);
			}

			if (appPkg.getVersion() != null || !"".equals(appPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("version", appPkg.getVersion());
			} else {
				jo.put("version", appPkg.getAppVer());
			}

			if (Os.IOS.equals(os) && isIOS == 1) {// IOS 的softUrl
				jo.put("url", String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim()));// appStore的地址
			} else {
				if (Os.ANDROID.equals(os)) {// 安讯 资源
					jo.put("url", pkgUrl.replaceAll("[,;]*",""));

					if (Os.ANDROID.equals(os)) {
						jo.put("version", appPkg.getVersion());
					} else if (Os.SYMBIAN.equals(os)) {
						jo.put("version", appPkg.getAppVer());
					}

				} else { // 自有资源
					String downloadUrl = pkgUrl.replaceAll("[,;]*", "");
					jo.put("url", downloadUrl);
				}
			}
			jarr.put(jo);
		}else if( null != IOSAppPkg ){
			jo.put("pkgUUid", IOSAppPkg.getUuid());// app_install_pack(安装包唯一的id)
			jo.put("appName", app.getName());
			jo.put("originalPackId", IOSAppPkg.getOriginalPackId());
			jo.put("sourcetype", app.getRscTypeId());
			if (IOSAppPkg.getCpUpdateTime() != null) {
				jo.put("updateTime", IOSAppPkg.getCpUpdateTime().replaceAll("[-\\s:]", ""));
			} else {
				jo.put("updateTime", "");
			}
			if ("cp01".equals(IOSAppPkg.getCp().getId()) || "cp02".equals(IOSAppPkg.getCp().getId()) || "cp09".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 0);
			} else if (null != IOSAppPkg.getCp().getId() && "cp03".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 2);
			} else if (null != IOSAppPkg.getCp().getId() && ("cp04".equals(IOSAppPkg.getCp().getId()) || "cp05".equals(IOSAppPkg .getCp().getId()))) {
				jo.put("cpid", 1);
			} else if (null != IOSAppPkg.getCp().getId() && "cp06".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 5);
			}

			if (IOSAppPkg.getVersion() != null || !"".equals(IOSAppPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("version", IOSAppPkg.getVersion());
			} else {
				jo.put("version", IOSAppPkg.getAppVer());
			}

			if (Os.IOS.equals(os) && isIOS == 1) {// IOS 的softUrl
				jo.put("url", String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim()));// appStore的地址
			} else {
				if (Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {// 沃商店 资源
					jo.put("url", pkgUrl.replaceAll("[,;]*",""));

					if (Os.ANDROID.equals(os)) {
						jo.put("version", IOSAppPkg.getVersion());
					} else if (Os.SYMBIAN.equals(os)) {
						jo.put("version", IOSAppPkg.getAppVer());
					}

				} else { // 自有资源
					String downloadUrl = pkgUrl.replaceAll("[,;]*", "");
					jo.put("url", downloadUrl);
				}
			}
			jarr.put(jo);
		}
		
		// ==================== 结束 == 拼接json字符串 ==========================

		// 客户端调用下载地址后, app的downCount值+1
		resourceService.updateDownNum(app.getUuid());

		return jarr;
	}

	/**
	 * 软件下载
	 * 
	 * @param cpId
	 * @param os
	 * @param discriminator
	 * @param uuid
	 * @param packUuid
	 * 
	 * @return Map<String, Object>
	 * @throws CpServerAccessException
	 */
	public Map<String, String> downLoad(Integer discriminator,
			String uuid, String packUuid, Os os, String cpId)
			throws CpServerAccessException {
		
		Map<String, String> urlMap = new HashMap<String, String>();
		if (discriminator == null || 1 == discriminator) {// appUuid
			if (os != null) {
				urlMap.put("pkgUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, os, cpId)));
			} else {
				urlMap.put("AndroidUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.ANDROID, cpId)));
				urlMap.put("IOSUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.IOS, cpId)));
				urlMap.put("IOSXUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.IOSX, cpId)));
				urlMap.put("SymbianUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.SYMBIAN, cpId)));
				urlMap.put("JavaUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.JAVA, cpId)));
				urlMap.put("WinmobileUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.WINMOBILE, cpId)));
			}
		} else if (2 == discriminator) {// packUuid 
			urlMap.put("pkgUrl", appQuerier.getPackUrl(packUuid));
			if(os==null){
				urlMap.put("AndroidUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("IOSUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("IOSXUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("SymbianUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("JavaUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("WinmobileUrl", appQuerier.getPackUrl(packUuid));
			}
		}

		return urlMap;
	}
	
	/**
	 * ANDROID取安讯资源，其它系统取灵动资源
	 * @param os
	 * 
	 * @return List<String>
	 */
	public List<String> cpIdList(Os os)	{
		
		List<String> cpIdList = new ArrayList<String>();
		//这个null只是为了能看详情页面，下载的时候还是要验证的手机操作系统的
		if (os == null || Os.ANDROID.equals(os)) {
			cpIdList.add(AreaParam.CP_AXON);
		} else {
			cpIdList.add(AreaParam.CP_I8APP);
		}
		return cpIdList;
	}
	
	

	
	/***********************************************
	 * 以下是访问器
	 ************************************************/
	public AppQuerier getAppQuerierImpl() {
		return appQuerier;
	}

	public void setAppQuerierImpl(AppQuerier appQuerierImpl) {
		this.appQuerier = appQuerierImpl;
	}

	public AppDTO getApp() {
		return app;
	}

	public void setApp(AppDTO app) {
		this.app = app;
	}

	public int getAppOsType() {
		return appOsType;
	}

	public void setAppOsType(int appOsType) {
		this.appOsType = appOsType;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public List<Pkg> getPkgList() {
		return pkgList;
	}

	public void setPkgList(List<Pkg> pkgList) {
		this.pkgList = pkgList;
	}

	public int getBizType() {
		return bizType;
	}

	public void setBizType(int bizType) {
		this.bizType = bizType;
	}

	public Pager<AppDTO> getAppPager() {
		return appPager;
	}

	public void setAppPager(Pager<AppDTO> appPager) {
		this.appPager = appPager;
	}

	public int getIsIOS() {
		return isIOS;
	}

	public void setIsIOS(int isIOS) {
		this.isIOS = isIOS;
	}

	public String getInstallType() {
		return installType;
	}

	public void setInstallType(String installType) {
		this.installType = installType;
	}

	public List<AppDTO> getAppList() {
		return appList;
	}

	public void setAppList(List<AppDTO> appList) {
		this.appList = appList;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public List<ResourceType> getAppTypeList() {
		return appTypeList;
	}

	public void setAppTypeList(List<ResourceType> appTypeList) {
		this.appTypeList = appTypeList;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getPkgId() {
		return pkgId;
	}

	public void setPkgId(int pkgId) {
		this.pkgId = pkgId;
	}

	public Map<ResourceType, List<AppDTO>> getTypeMap() {
		return typeMap;
	}

	public void setTypeMap(Map<ResourceType, List<AppDTO>> typeMap) {
		this.typeMap = typeMap;
	}

	public String getPackUuid() {
		return packUuid;
	}

	public void setPackUuid(String packUuid) {
		this.packUuid = packUuid;
	}

	public String getImoduletype() {
		return imoduletype;
	}

	public void setImoduletype(String imoduletype) {
		this.imoduletype = imoduletype;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Map<String, Pager<AppDTO>> getAppMap() {
		return appMap;
	}

	public void setAppMap(Map<String, Pager<AppDTO>> appMap) {
		this.appMap = appMap;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public ResourceType getResType() {
		return resType;
	}

	public void setResType(ResourceType resType) {
		this.resType = resType;
	}

	public Pkg getPkg() {
		return pkg;
	}

	public void setPkg(Pkg pkg) {
		this.pkg = pkg;
	}

	public List<Pkg> getPkgList_City() {
		return pkgList_City;
	}

	public void setPkgList_City(List<Pkg> pkgList_City) {
		this.pkgList_City = pkgList_City;
	}
	
	public List<Pkg> getPkgList_Type() {
		return pkgList_Type;
	}


	public void setPkgList_Type(List<Pkg> pkgList_Type) {
		this.pkgList_Type = pkgList_Type;
	}

	public Map<String, List<ResourceType>> getAppTypeMap() {
		return appTypeMap;
	}

	public void setAppTypeMap(Map<String, List<ResourceType>> appTypeMap) {
		this.appTypeMap = appTypeMap;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getCacheType() {
		return cacheType;
	}

	public void setCacheType(int cacheType) {
		this.cacheType = cacheType;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
}
