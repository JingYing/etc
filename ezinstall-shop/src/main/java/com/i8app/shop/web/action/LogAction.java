package com.i8app.shop.web.action;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.servlet.http.Cookie;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.shop.common.MD5;
import com.i8app.shop.common.PhoneOs;
import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.common.Util;
import com.i8app.shop.common.config.Config_i8;
import com.i8app.shop.domain.Addressbookrunlog;
import com.i8app.shop.domain.App;
import com.i8app.shop.domain.AppErrLog;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.ClientDeployInfo;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.InstallErrorLog;
import com.i8app.shop.domain.InstallLog;
import com.i8app.shop.domain.InstallLogzy;
import com.i8app.shop.domain.Pkg;
import com.i8app.shop.domain.Province;
import com.i8app.shop.domain.TypeInfo;
import com.i8app.shop.domain.UnionreadPkgView;
import com.i8app.shop.domain.VersionInfo;
import com.i8app.shop.domain.WorkPortAccessLog;
import com.i8app.shop.service.EmpService;
import com.i8app.shop.service.HallService;
import com.i8app.shop.service.InstallLogService;
import com.i8app.shop.service.LoginSecurityService;
import com.i8app.shop.service.ProvinceService;
import com.i8app.shop.service.ResourceService;
import com.i8app.shop.service.SimpleQueryService;

/**
 * 该类主要用于C++客户端直接调用, 包括登陆注销, 日志安装记录
 * @author JingYing
 *
 */
@Controller
@Scope("prototype")
public class LogAction extends BaseAction {
	
	private static Logger logger = Logger.getLogger(LogAction.class);
	
	/**
	 * 人员ID; 运营商代号; 安装类型; "短信下载"按键开关, 0关1开; 操作系统代号 ; 营业厅ID; 游戏ID; OSTYPE;
	 * TYPEID; 资源类型ID; 强制安装类型(联通,移动,电信等); 安装资源的来源, 等等...
	 */
	private Integer wosoftId, empId, proId, operatorName, installType, smsSwitch,
				appType, hallId, osType, typeId, dtId, powerType, source, optStatus, versionId, 
				isOtherNet, isSmsVal, isGuide, isAutoOpen, clientType, tableType;
	
	private String softId, gameId, pkguuid, appName, rscVersion, imoduletype, errDesc, pkgId, uuid;//uuid离线上传使用
	
	/**
	 * 员工号; 员工密码; 区域码; 制造厂商; 手机型号; 操作系统版本号; 手机串号; 手机号; 沃商店登陆密码;软件ID串, 等等...
	 */
	private String empNo, empPwd, areaCode, manuName, typeName, version, 
					phoneIMEI, phoneNumber, pwd, name, typeNameNew, osVersion,
					psn, hallName, ldcVersion, versions, imsi, seqNo, modelName, dateStr, md5Msg;
	
	/**
	 * 通讯录日志用参数
	 */
	private String exportId;
	private int opt, result, type;
	
	/**
	 * 新疆第三方安装日志接口所用参数
	 * @param areacode (省份地区编码:
	 * 1028：上海
		1031：山西
		1033：湖南
		1039：陕西
		1041：广东
		1042：广西
		1046：山东
		1049：新疆
		1052：甘肃
		1055：浙江
		1040：福建);
	 * @param aunm (营业员工号);instype(安装方式);osversion (系统版本);appid (应用ID);
	 * @param wv(当前客户端版本号);imei(手机串号);imob (手机号) ;pkid(快捷安装包id);pkname(快捷安装包名称)
	 * @param mname(机型名称);brname(手机品牌中文名称,即厂商名称)
	 */
	private String areacode, aunm, instype, osversion, appid, wv, imei, imob, pkid, pkname, mname, brname;
	
	
	/**
	 * WOSHOP的渠道号;
	 */
	private String channelId, clientid;
	
	@Resource
	private EmpService empService;
	@Resource
	private HallService hallService;
	@Resource
	private SimpleQueryService simpleQueryService;
	@Resource
	private LoginSecurityService loginSecurityService;
	
	@Resource
	private ResourceService resourceService;
	@Resource
	private InstallLogService installLogService;
	@Resource
	private ProvinceService provinceService;
	@Resource
	private Config_i8 config_i8;
	
	@SuppressWarnings("static-access")
	public String empLogin2()	{
		int errorCode;
		String desc = "";
		JsonObject empJson = new JsonObject();
		try	{
			Employee emp = loginSecurityService.checkEmpLogin2(areaCode, empNo);
			errorCode = loginSecurityService.empErrorCode(empPwd, emp);
			switch(errorCode)	{
				case 0:	//正确
					BusinessHall hall = simpleQueryService.findBusinessHallById(emp.getOrgId());
					empJson.addProperty("id", emp.getId());
					empJson.addProperty("no", emp.getEmpNo());
					empJson.addProperty("name", emp.getName());
					empJson.addProperty("provId", emp.getProvince().getId());
					empJson.addProperty("orgId", emp.getOrgId());
					empJson.addProperty("hallName", hall.getHallName());
					SessionAccessor.setEmp(emp).setAreaCode(areaCode).setSmsSwitch(smsSwitch);
					loginSecurityService.afterLogin(emp, psn, hall.getId(), 1);		//记录日志
					break;
				case 1:	
					desc = "无此工号";
					break;
				case 2:	
					desc = "密码错误";
					break;
				case 3:	
					desc = "不是营业厅人员";
					break;
				case 4:	
					desc = "被删";
					break;
				case 5:	
					desc = "冻结";
					break;
			}
		} catch(Exception e)	{
			logger.error(e.getMessage(), e);
			errorCode = 500;
			desc = e.getMessage();
		}
		JsonObject json = new JsonObject();
		json.addProperty("errorcode", errorCode);
		json.addProperty("desc", desc);
		json.add("result", empJson);
		return doPrint(json.toString());
	}
	
	/**
	 * 营业厅人员注销接口
	 * 清除session的empMap
	 * @return
	 */
	public String empLogout()	{
		try	{
			Employee e = SessionAccessor.getEmp();
			if(e != null){
				loginSecurityService.saveOperateLog(e, 2);
			}
			SessionAccessor.removeAreaCode().removeSmsSwitch().removeEmp().removeHasFaqed();
			return doPrint(1);
		} catch(Exception e)	{
			logger.error(e.getMessage(), e);
			return doPrint(0);
		}
	}
	
	@SuppressWarnings("static-access")
	public String phoneLogin()	{
		PhoneOs phoneOs = PhoneOs.getInstance(appType, version, manuName, typeName);

		// 输出该手机对应的TYPEID
		TypeInfo typeInfo = null;
		if(typeName != null && manuName != null && phoneOs.equals(PhoneOs.SYMBIAN))	{
			typeInfo = simpleQueryService.getSymbianTypeInfo(typeName, manuName);
		}
		else if(typeName != null && manuName != null && phoneOs.equals(PhoneOs.JAVA))	{
			typeInfo = simpleQueryService.getJavaTypeInfo(typeName, manuName);
		}
		
		//将手机信息存入Session
		SessionAccessor.setPhoneOs(phoneOs).setOsVersion(phoneOs.getOsid())
						.setPhoneIMEI(phoneIMEI).setTypeInfo(typeInfo)
						.setPhoneLoginUuid(UUID.randomUUID());
		
		return this.doPrint((typeInfo == null ? 0 : typeInfo.getId())+ "");
	}
	
	/**
	 * 手机注销接口. 清除用户SESSION
	 * @return
	 */
	public String phoneLogout()	{
		SessionAccessor.removePhoneOs().removeOsVersion()
		.removePhoneIMEI().removeLastCommented().removeTypeInfo()
		.removePhoneLoginUuid().removeAuthcode();
		
		//清除以"imgAlter_"开头的cookie
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String cookieName = cookies[i].getName();
				if (cookieName.startsWith("imgAlter_")) {
					Cookie cookie = new Cookie(cookieName, null);
					cookie.setMaxAge(0);
					cookie.setPath("/");
					response.addCookie(cookie);
				}
			}
		}
		return null;
	}

	/**
	 * 验证手机号是否属于该运营商
	 * @return
	 */
	public String checkPhoneNo()	{
		if(phoneNumber.length() < 3)	{
			return this.doPrint("false");
		}
		boolean result = loginSecurityService.isPhoneMatch(phoneNumber, operatorName);
		if(result == true)
			return this.doPrint("true");
		else
			return this.doPrint("false");
	}
	
	/**
	 * 通讯录日志记录
	 * @return
	 */
	public String addressBookLog()	{
		String resultStr = "";
		try	{
			Addressbookrunlog a = new Addressbookrunlog();
			a.setEmpid(empId);
			a.setStarttime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			a.setOstype(osType + "");
			a.setTypename(typeName);
			a.setTypeid(typeId);
			a.setOsversion(version);
			a.setPhonenumber(phoneNumber);
			a.setType(type);
			a.setOpt(opt);
			a.setResult(result);
			a.setExportid(exportId);
			simpleQueryService.saveAddressbookLog(a);
			resultStr = a.getId() + ""; 
		} catch(Exception e)	{
			e.printStackTrace();
			resultStr = "failed";
		}
		return this.doPrint(resultStr);
	}
	
	/**
	 * psn对应的记录, 如果没有, 则生成activeTime,将其它4个值存进去
	 * psn对应的记录, 如果有, 则更新除activetime以外的值
	 * @return
	 */
	public String clientDeployInfo()	{
		int result = 2;
		ClientDeployInfo c = simpleQueryService.findClientDeploy(psn);
		if(c == null)	{
			result = 1;
			c = new ClientDeployInfo();
			c.setActivedtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		}
		c.setHallId(hallId);
		c.setHallName(hallName);
		c.setLdcversion(ldcVersion);
		c.setPsn(psn);
		c.setVersions(versions);
		simpleQueryService.saveClientDeploy(c);
		return this.doPrint(result + "");
	}
	
	/**
	 * 查找versions对应记录是否存在, 存在, 则返回id;
	 * 不存在, 则存入, 返回id;
	 * @return
	 */
	public String versionInfo()	{
		VersionInfo v = simpleQueryService.findVersionInfo(versions);
		if(v == null)	{
			v = new VersionInfo();
			v.setVersions(versions);
			simpleQueryService.saveVersionInfo(v);
		}
		return this.doPrint(v.getVersionId() + "");
	}
	
	
	/**
	 * 获取所有省份信息
	 */
	public String findProvs() {
		JsonArray jsonArr = new JsonArray();
		List<Province> provList = provinceService.findProvince();
		for (Province prov : provList) {
			String areaCode = config_i8.getProvAreaMap().get(prov.getId());
			if(areaCode == null ){
				areaCode = "";
			}
			JsonObject json = new JsonObject();
			json.addProperty("name", prov.getName());
			json.addProperty("areaCode", areaCode);
			jsonArr.add(json);
		}
		
		return this.doPrint(jsonArr.toString());
	}
	
	//手机客户端安装日志
	public String phone_appInstallLog(){
		JSONObject jo = new JSONObject();
		BufferedReader br;
		JSONArray array = null;
		try {
			br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			array = new JSONArray(s);
			for (int i = 0; i < array.length(); i++) {
				try {
					JSONObject object = array.getJSONObject(i);
					Integer empId = object.getInt("empId");
					String pkguuid = object.getString("pkguuid");
					Integer source = object.getInt("source");
					String rscVersion = object.getString("rscVersion");
					Integer osType = object.getInt("osType");
					String osVersion = object.getString("osVersion");
					String phoneIMEI = object.getString("phoneIMEI");
					String imsi = object.getString("imsi");
					Integer installType = object.getInt("installType");
					String manuName = object.getString("manuName");
					String modelName = object.getString("modelName");
					Integer optStatus = object.getInt("optStatus");
					String phoneNumber = object.getString("phoneNumber");
					String psn = object.getString("psn");
					Integer typeId = object.getInt("typeId");
					Integer versionId = object.getInt("versionId");
					String seqNo = object.getString("seqNo");
					Integer isAutoOpen = object.getInt("isAutoOpen");
					Integer isGuide = object.getInt("isGuide");
					Integer isOtherNet = object.getInt("isOtherNet");
					Integer isSmsVal = object.getInt("isSmsVal");
					String imoduletype = object.getString("imoduletype");
					String pkgId = null;
					Integer result = this.saveAppInstallLog(empId, pkguuid, osType, osVersion, rscVersion, imoduletype, source, versionId, phoneIMEI, imsi, 
							installType, isAutoOpen, isGuide, isOtherNet, isSmsVal, manuName, modelName, optStatus, phoneNumber, psn, typeId, seqNo, pkgId, null);
					if(result.intValue() == 1){//张思汉说的，只要有一个成功的，那么这个结果就是成功的！
						jo.put("result", result);
					}
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return this.doPrint(jo.toString());
	}
	
	//手机客户端离线安装日志
	public String phone_restInstallLog(){
		JSONObject jo = new JSONObject();
		BufferedReader br;
		JSONArray array = null;
		try {
			br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			array = new JSONArray(s);
			for (int i = 0; i < array.length(); i++) {
				try {
					JSONObject object = array.getJSONObject(i);
					Integer empId = object.getInt("empId");
					String pkguuid = object.getString("pkguuid");
					Integer source = object.getInt("source");
					String rscVersion = object.getString("rscVersion");
					Integer osType = object.getInt("osType");
					String osVersion = object.getString("osVersion");
					String phoneIMEI = object.getString("phoneIMEI");
					String imsi = object.getString("imsi");
					Integer installType = object.getInt("installType");
					String manuName = object.getString("manuName");
					String modelName = object.getString("modelName");
					Integer optStatus = object.getInt("optStatus");
					String phoneNumber = object.getString("phoneNumber");
					String psn = object.getString("psn");
					Integer typeId = object.getInt("typeId");
					Integer versionId = object.getInt("versionId");
					String seqNo = object.getString("seqNo");
					Integer isAutoOpen = object.getInt("isAutoOpen");
					Integer isGuide = object.getInt("isGuide");
					Integer isOtherNet = object.getInt("isOtherNet");
					Integer isSmsVal = object.getInt("isSmsVal");
					String imoduletype = object.getString("imoduletype");
					String pkgId = null;
					Integer result = this.saveAppInstallLog(empId, pkguuid, osType, osVersion, rscVersion, imoduletype, source, versionId, phoneIMEI, imsi, 
							installType, isAutoOpen, isGuide, isOtherNet, isSmsVal, manuName, modelName, optStatus, phoneNumber, psn, typeId, seqNo, pkgId, null);
					if(result.intValue() == 1){//张思汉说的，只要有一个成功的，那么这个结果就是成功的！
						jo.put("result", result);
					}
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return this.doPrint(jo.toString());
	}
	
	/**
	 * 营业人员软件工作量离线上传接口
	 * PC 应用离线安装日志
	 * 
	 * @return
	 */
	public String restInstallLog(){
		result = 0;
		if(pkguuid != null){
			AppInstallPack appPkg = resourceService.findAppPkgByid(pkguuid);
			if(appPkg != null){
				result = this.saveAppInstallLog(empId, pkguuid, osType, osVersion, rscVersion, imoduletype, source, 
					versionId, phoneIMEI, imsi, installType, isAutoOpen, isGuide, isOtherNet, isSmsVal, manuName, 
					modelName, optStatus, phoneNumber, psn, typeId, seqNo, pkgId, uuid );
			}
		}
		return doPrint(result);
	}
	
	/**
	 * PC 自有软件安装日志接口
	 * 做MD5加密处理，加密规则：
	 * 
	 * tableType = 1 插入到  appInstallLog ; 非1则添加至 appInstallLogzy
	 * dateStr ：yyyyMMddHHmmsss
	 * md5Msg ： phoneNumber(后7)+phoneNumber(前4)+phoneIMEI+source+installType+dateStr(倒序)+empId+pkguuid(去除-剩余的值)
	 * 
	 * @return
	 */
	public String appInstallLogzy(){
		result = 0;

		String newDateStr = new StringBuffer(dateStr).reverse().toString();//将原有的字符串反过来排序
		MD5 md5 = new MD5();
		String str = "";
		if(phoneNumber != null && !"".equals(phoneNumber)){
			str = phoneNumber.substring(4)+phoneNumber.substring(0,4)+phoneIMEI+source.toString()+installType.toString()+newDateStr+empId+pkguuid.replace("-", "");
		}else{
			str = phoneIMEI+source.toString()+installType.toString()+newDateStr+empId+pkguuid.replace("-", "");
		}
		
		String newMD5Str = md5.toMD5(str);
		//MD5验证通过，数据有效才执行保存日志
		if(newMD5Str.equals(md5Msg)){
			if(pkguuid != null){
				AppInstallPack appPkg = resourceService.findAppPkgByid(pkguuid);
				if(appPkg != null){
					if(tableType == 1){
						result = this.saveAppInstallLog(empId, pkguuid, osType, osVersion, rscVersion, imoduletype, source, 
								versionId, phoneIMEI, imsi, installType, isAutoOpen, isGuide, isOtherNet, isSmsVal, manuName,
								modelName, optStatus, phoneNumber, psn, typeId, seqNo, pkgId, null);
					}else{
						result = this.saveAppInstallLogzy(empId, pkguuid, osType, osVersion, rscVersion, imoduletype, source, 
								versionId, phoneIMEI, imsi, installType, isAutoOpen, isGuide, isOtherNet, isSmsVal, manuName,
								modelName, optStatus, phoneNumber, psn, typeId, seqNo, pkgId, null);
					}
				}
			}
		}else{
			result = 2;//MD5码不匹配
		}
		return doPrint(result);
	}

	
	
	
	//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	/**
	 * 第三方厂商上传安装日志
	 * @return 错误码: 1接收数据失败,2clientid无法识别,3解密失败,4必须参数无效(?,?,?),5结果未找到(工号,省,厅),500服务器端错误
	 */
	public String upwork()	{
		String template = "{\"errorcode\":%d,\"desc\":\"%s\"}";	//错误码模板
		
		BufferedInputStream bis;
		ByteArrayOutputStream baos = null;
		try {
			bis = new BufferedInputStream(getRequest().getInputStream());
			 baos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int length = -1;
			while((length=bis.read(buf, 0, 1024)) != -1)	{
				baos.write(buf, 0, length);
			}
			baos.flush();
		} catch (IOException e) {
			e.printStackTrace();
			return doPrint(String.format(template, 1, ""));
		}
		
		try	{
			String decryptedParam = null;
			try {
				decryptedParam = installLogService.decryptParam(baos.toByteArray(), clientid);
			} catch (IllegalArgumentException e) {
				return doPrint(String.format(template, 2, "不识别的clientid:" + clientid));
			} catch (BadPaddingException e) {
				return doPrint(String.format(template, 3, ""));
			}
			
			WorkPortAccessLog log = new WorkPortAccessLog();
			log.setAccessTime(new Date());
			log.setClientIp(getRequest().getRemoteAddr());
			log.setServerIp(getRequest().getRequestURL().toString());
			log.setParam(decryptedParam);
			installLogService.persistWorkPortAccessLog(log);
			
			InstallLog il = new InstallLog();
			for(String s : decryptedParam.split("&"))	{
				String[] kv = s.split("=");
				if("imei".equals(kv[0]))	{
					il.setImei(kv[1]);
				} else if("imsi".equals(kv[0]))	{
					il.setImsi(kv[1]);
				} else if("mac".equals(kv[0]))	{
					il.setSeqNo(kv[1]);
				} else if("modelid".equals(kv[0]))	{
					il.setTypeid(Integer.parseInt(kv[1]));	//沃商店机型ID
				} else if("os".equals(kv[0]))	{
					il.setOstype(Integer.parseInt(kv[1]));
				} else if("osver".equals(kv[0]))	{
					il.setOsVersion(kv[1]);
				} else if("phoneno".equals(kv[0]))	{
					il.setPhoneNumber(kv[1]);
				} else if("uuid".equals(kv[0]))	{
					il.setUuid(kv[1]);
				} else if("installtime".equals(kv[0]))	{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String date = sdf.format(new Date(Long.parseLong(kv[1])));
					il.setInstallTime(date);
				} else if("appname".equals(kv[0]))	{
					il.setRscName(kv[1]);
				} else if("appid".equals(kv[0]))	{
					il.setRscid(kv[1]);
				} else if("installtype".equals(kv[0]))	{
					il.setInstallType(Integer.parseInt(kv[1]));
				} else if("provid".equals(kv[0]))	{
					Province prov = simpleQueryService.findProvinceByBsscode(kv[1]);
					//TODO
					il.setProvinceId(prov.getId());
				} else if("chanid".equals(kv[0]))	{
					il.setHallno(kv[1]);
				} else if("staffnum".equals(kv[0]))	{
					il.setEmpNo(kv[1]);
				} 
			}
			
			try {
				installLogService.completeInstallLogFromWorkport(il);
			} catch (IllegalArgumentException e) {
				logger.error(e.getMessage(), e);
				return doPrint(String.format(template, 5, e.getMessage()));
			}
			return doPrint(String.format(template, 0, ""));
		} catch(Exception e)	{
			logger.error(e.getMessage(), e);
			return doPrint(String.format(template, 500));
		}
	}
	
	/**
	 * 客户端提交APP安装失败记录
	 * empId
	 * clientType
	 * packUuid
	 * appName
	 * errDesc
	 * 
	 * @return
	 */
	public String saveAppErrlog()	{
		
		try {
			//根据empId查询员工对象 
			Employee emp = empService.findById(empId);
			
			AppErrLog errLog = new AppErrLog();
			errLog.setProvId(emp.getProvince().getId());
			errLog.setCityId(emp.getCity().getId());
			errLog.setHallId(emp.getOrgId());
			errLog.setClientType(clientType);
			errLog.setPackUuid(pkguuid);
			errLog.setAppName(appName);
			errLog.setErrDesc(errDesc);
			errLog.setSubmitTime(new Date());
			
			installLogService.saveAppErrLog(errLog);
			
			return doPrint(1);
			
		} catch (Exception e) {
			/**
			 * 错误日志接口出现错误时, 发送邮件+向数据库存错误日志
			 * serverUrl : 出现错误的shop地址
			 * params: 出错的日志参数
			 */
			String params = Util.deepToString(getRequest().getParameterMap());
			InstallErrorLog installErrLog = new InstallErrorLog();
			installErrLog.setErrorTime(new Date());
			installErrLog.setServerUrl(getBasePath());
			installErrLog.setParams(params);
			installLogService.saveInstallErrorLog(installErrLog);
			
			//发邮件
			logger.fatal(String.format("%s\"--客户端提交APP安装失败记录日志\"出现错误!%s", 
					getBasePath(), Util.deepToString(getRequest().getParameterMap())), e);
			
			return doPrint(0);
		}
	}
	
	/**
	 * PC客户端向阅联提交工作量时, 有时会提交失败, PC客户端把这些错误记录提交至SHOP, 存入install_error_log
	 * errDesc
	 * 
	 * @return
	 */
	public String saveInstallErrlog()	{
		//System.out.println(errDesc.length()+"	错误");
		try {
			InstallErrorLog installErrLog = new InstallErrorLog();
			installErrLog.setErrorTime(new Date());
			installErrLog.setServerUrl("installErrlog");//这个字符串固定为 installErrlog ，仅仅是为了区分这个错误信息的来源
			installErrLog.setParams(errDesc);
			installLogService.saveInstallErrorLog(installErrLog);
			
			return doPrint(1);
			
		} catch (Exception e) {
			/**
			 * 错误日志接口出现错误时, 发送邮件+向数据库存错误日志
			 * serverUrl : 出现错误的shop地址
			 * params: 出错的日志参数
			 */
			String params = Util.deepToString(getRequest().getParameterMap());
			InstallErrorLog installErrLog = new InstallErrorLog();
			installErrLog.setErrorTime(new Date());
			installErrLog.setServerUrl(getBasePath());
			installErrLog.setParams(params);
			installLogService.saveInstallErrorLog(installErrLog);
			
			//发邮件
			logger.fatal(String.format("%s\"--App安装日志\"出现错误!%s", 
					getBasePath(), Util.deepToString(getRequest().getParameterMap())), e);
			
			return doPrint(0);
		}
	}
	
	/**
	 * 新疆第三方客户端的安装日志上传接口
	 */
	public String saveThirdPartyInstallLog(){
		JSONObject jo = new JSONObject();
		String resultcode = "";
		try {

			//根据阅联提供的省编号查询对应省
			Province prov = provinceService.findByBssAreacode(areacode);
			
			//根据empId查询员工对象
			Employee emp = empService.findByNoAndProId(aunm, prov.getId());
			if(emp == null){
				resultcode = "200002";//员工不存在
			}
			
			//根据emp.OrgId查询厅信息
			BusinessHall hall = hallService.findById(emp.getOrgId());
			
			optStatus = 1;
			if(instype.equals("21")){
				osType = 100;
				installType = 1;
			}else if(instype.equals("23")){
				osType = 200;
				installType = 1;
			}else if(instype.equals("40")){
				osType = 300;
				installType = 1;
			}else if("instype".startsWith("22")){
				osType = 400;
				installType = 1;
			}else if(instype.equals("10")){
				osType = 200;
				//optStatus 在不是短信的时候为1 ，否则为70002
				optStatus = 70002;
				installType = 16;
			}else if(instype.equals("20")){
				osType = 200;
				installType = 2;
			}else if(instype.equals("30")){
				osType = 200;
				installType = 101;
			}else if(instype.equals("60")){
				osType = 200;
				installType = 102;
			}else if(instype.equals("70")){
				osType = 200;
				installType = 103;
			}
			
			/**
			 * 
			 * cp01、cp02（灵动应用）的 source 为 0;
			 * cp03 （沃商店） 的source 为 2;
			 * cp04 （安讯） 的 source 为 3;
			 * cp05（拓维） 的 source 为 1 ;
			 * cp06 （Mobile Market） 的 source 为 5;
			 * cp09 （灵动APP） 的 source 为 0;
			 */
			//根据阅联的appid 查询 AppInstallPack 对象
			AppInstallPack appPkg = resourceService.findByYLAppId(appid);
			App app = appPkg.getApp();
			
			// 安装包的版本不为空，优先获取安装包的版本
			if (appPkg.getVersion() != null || !"".equals(appPkg.getVersion())) {
				rscVersion = appPkg.getVersion();
			} else {
				rscVersion = appPkg.getAppVer();
			}
			
			InstallLog log = new InstallLog();
			log.setProvinceId(prov.getId());
			log.setProvinceName(prov.getName());
			log.setCityId(emp.getCity().getId());
			log.setCityName(emp.getCity().getName());
			log.setCountyId(emp.getCounty().getId());
			log.setCountyName(emp.getCounty().getName());
			log.setEmpId(emp.getId().toString());
			log.setEmpName(emp.getName());
			log.setEmpNo(emp.getEmpNo());
			log.setHallId(emp.getOrgId());
			log.setHallName(hall.getHallName());
			log.setHallno(hall.getHallNo());
			log.setChanType(hall.getChanType());
			log.setChanGrade(hall.getChanGrade());
			log.setChanLevel(hall.getChanLevel());
			log.setOstype(osType);
			log.setOsVersion(osversion);
			log.setCpId(appPkg.getCp().getId());//cp商 
			log.setAppUuid(app.getUuid());//软件的UUID  
			log.setPackUuid(appPkg.getUuid());//包软件UUID
			log.setRscid(appPkg.getOriginalPackId());//软件的原始Id
			log.setRscName(app.getName());//安装软件名称
			if(rscVersion.length()>32){//安装软件版本
				log.setRscVersion(rscVersion.substring(0, 32));
			}else{
				log.setRscVersion(rscVersion);
			}
			log.setImoduletype("17");
			log.setSourceType(app.getRscType().getId());//-101***、102***之类的
			log.setSource(2);
			log.setVersionId(0);//第三方的版本不在我们数据库中,所以用0代替.
			log.setImei(imei);
			log.setImsi(imsi);
			log.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			log.setInstallType(installType);
			log.setIsAutoOpen(0);
			log.setIsGuide(0);
			log.setIsOtherNet(0);
			log.setIsSmsVal(0);
			log.setManuName(mname);
			log.setModelName(brname);
			log.setFilterrule(null);
			log.setOptStatus(optStatus);
			log.setPhoneNumber(imob);
			log.setPsn("");
			log.setBhid(hall.getBhid());
			log.setCcid(hall.getCcid());
			log.setRccid(hall.getRccid());
			log.setTypeid(0);
			log.setPkgId(Integer.parseInt(pkid));//业务包id
			log.setPkgName(pkname);//业务包名称
			log.setUuid("");
			log.setSeqNo("");
			
			installLogService.saveInstallLog(log);
			//客户端上传安装日志后, app的installCount+1
			resourceService.updateTotalInstallCount(app.getUuid());
			if(resultcode.equals("")){
				resultcode = "000000";
			}
			jo.put("resultcode", resultcode);
			return doPrint(jo.toString());
			
		} catch (Exception e) {
			/**
			 * 日志接口出现错误时, 发送邮件+向数据库存错误日志
			 * serverUrl : 出现错误的shop地址
			 * params: 出错的日志参数
			 */
			String params = Util.deepToString(getRequest().getParameterMap());
			InstallErrorLog installErrLog = new InstallErrorLog();
			installErrLog.setErrorTime(new Date());
			installErrLog.setServerUrl(getBasePath());
			installErrLog.setParams(params);
			installLogService.saveInstallErrorLog(installErrLog);
			
			//发邮件
			logger.fatal(String.format("%s\"--新疆第三方客户端的App安装日志\"出现错误!%s", 
					getBasePath(), Util.deepToString(getRequest().getParameterMap())), e);
			try {
				resultcode = "100001";
				jo.put("resultcode", resultcode);
				
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return doPrint(jo.toString());
		}
	}
	
	
	
	
	
	
	
	//####################################################	内置方法		##########################################################
	
	/**
	 * 内置方法（本项目中没有地方调用才能够删除或注释掉！）
	 * 参数说明
	 * 
	 * @param empId
	 * @param pkguuid
	 * @param osType
	 * @param osVersion
	 * @param rscVersion
	 * @param imoduletype
	 * @param source
	 * @param versionId
	 * @param phoneIMEI
	 * @param imsi
	 * @param installType
	 * @param isAutoOpen
	 * @param isGuide
	 * @param isOtherNet
	 * @param isSmsVal
	 * @param manuName
	 * @param modelName
	 * @param optStatus
	 * @param phoneNumber
	 * @param psn
	 * @param typeId
	 * @param seqNo
	 * @param pkgId
	 * @param uuid （这里传入的uuid为null，除了离线上传以外，其他安装日志的uuid均需从session中获取）
	 * 
	 * @return
	 */
	public Integer saveAppInstallLog(Integer empId, String pkguuid, Integer osType, String osVersion, String rscVersion, String imoduletype, Integer source, 
			Integer versionId, String phoneIMEI, String imsi, Integer installType, Integer isAutoOpen, Integer isGuide, Integer isOtherNet, Integer isSmsVal, 
			String manuName, String modelName, Integer optStatus, String phoneNumber, String psn, Integer typeId, String seqNo, String pkgId, String uuid){
		try {
			//根据empId查询员工对象
			Employee emp = empService.findById(empId);
			
			//根据emp.OrgId查询厅信息
			BusinessHall hall = hallService.findById(emp.getOrgId());
			
			//根据业务包的id查询业务对象，获取其名称
			String pkgName = "";
			if(pkgId != null && !"".equals(pkgId) && Integer.parseInt(pkgId)>0){
				if("14".equals(imoduletype) || "15".equals(imoduletype)){//查询阅联的
					List<UnionreadPkgView> list = resourceService.findPackUuidByPkgId(emp.getProvince().getId(), pkgId);
					for (int i = 0; i < list.size(); i++) {
						pkgName = list.get(0).getPkgName();
					}
				}else{
					Pkg p = resourceService.findPkgByPkgId(Integer.parseInt(pkgId));
					if(p != null){
						pkgName = p.getName();
					}
				}
			}else if(pkgId == null || "".equals(pkgId)){
				pkgId = "0";
			}
			
			InstallLog log = new InstallLog();
			/**
			 * cp01、cp02（灵动应用）的 source 为 0;
			 * cp03 （沃商店） 的source 为 2;
			 * cp04 （安讯） 的 source 为 3;
			 * cp05（拓维） 的 source 为 1 ;
			 * cp06 （Mobile Market） 的 source 为 5;
			 * cp09 （灵动APP） 的 source 为 0;
			 */
			//根据安装包的pkguuid 查询AppInstallPack对象
			AppInstallPack appPkg = resourceService.findAppPkgByid(pkguuid);
			if("cp01".equals(appPkg.getCp().getId()) || "cp02".equals(appPkg.getCp().getId()) || "cp09".equals(appPkg.getCp().getId())){
				source = 0;
			}else if("cp03".equals(appPkg.getCp().getId())){
				source = 2;
			}else if("cp04".equals(appPkg.getCp().getId())){
				source = 3;
			}else if("cp05".equals(appPkg.getCp().getId())){
				source = 1;
			}else if("cp06".equals(appPkg.getCp().getId())){
				source = 5;
			}
			
			log.setCpId(appPkg.getCp().getId());//cp商 
			log.setPackUuid(appPkg.getUuid());//包软件UUID
			log.setRscid(appPkg.getOriginalPackId());//软件的原始Id
			
			App app = appPkg.getApp();
			log.setAppUuid(app.getUuid());//软件的UUID  
			log.setRscName(app.getName());//安装软件名称
			log.setSourceType(app.getRscType().getId());//-101***、102***之类的

			if((phoneIMEI == null || "".equals(phoneIMEI)) && (imsi == null || "".equals(imsi)) && (phoneNumber == null || "".equals(phoneNumber))){
				phoneIMEI = new SimpleDateFormat("yyyyMMddHHmmsss").format(new Date());
			}
			log.setProvinceId(emp.getProvince().getId());
			log.setProvinceName(emp.getProvince().getName());
			log.setCityId(emp.getCity().getId());
			log.setCityName(emp.getCity().getName());
			log.setCountyId(emp.getCounty().getId());
			log.setCountyName(emp.getCounty().getName());
			log.setEmpId(emp.getId().toString());
			log.setEmpName(emp.getName());
			log.setEmpNo(emp.getEmpNo());
			log.setHallId(emp.getOrgId());
			log.setHallName(hall.getHallName());
			log.setHallno(hall.getHallNo());
			log.setChanType(hall.getChanType());
			log.setChanGrade(hall.getChanGrade());
			log.setChanLevel(hall.getChanLevel());
			log.setOstype(osType);
			log.setOsVersion(osVersion);
			if(rscVersion.length()>32){//安装软件版本
				log.setRscVersion(rscVersion.substring(0, 32));
			}else{
				log.setRscVersion(rscVersion);
			}
			log.setImoduletype(imoduletype);
			log.setSource(source);
			log.setVersionId(versionId);
			log.setImei(phoneIMEI);
			log.setImsi(imsi);
			log.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			log.setInstallType(installType);
			log.setIsAutoOpen(isAutoOpen);
			log.setIsGuide(isGuide);
			log.setIsOtherNet(isOtherNet);
			log.setIsSmsVal(isSmsVal);
			log.setManuName(manuName);
			log.setModelName(modelName);
			log.setFilterrule(null);
			log.setOptStatus(optStatus);
			log.setPhoneNumber(phoneNumber);
			log.setPsn(psn);
			log.setBhid(hall.getBhid());
			log.setCcid(hall.getCcid());
			log.setRccid(hall.getRccid());
			log.setTypeid(typeId);
			log.setPkgId(Integer.parseInt(pkgId));
			log.setPkgName(pkgName);
			if(uuid == null){//不为null才是离线上传，离线上传是无需从session中获取uuid的
				if(SessionAccessor.getPhoneLoginUuid() != null){
					log.setUuid(SessionAccessor.getPhoneLoginUuid().toString());//下载操作产生的uuid
				}else{
					log.setUuid("");
				}
			}else{
				log.setUuid(uuid);
			}
			log.setSeqNo(seqNo);
			
			installLogService.saveInstallLog(log);
			
			//客户端上传安装日志后, app的installCount+1
			if(app != null){
				resourceService.updateTotalInstallCount(app.getUuid());
			}
			
			return 1;
		} catch (Exception e) {
			/**
			 * 日志接口出现错误时, 发送邮件+向数据库存错误日志
			 * serverUrl : 出现错误的shop地址
			 * params: 出错的日志参数
			
			String params = Util.deepToString(getRequest().getParameterMap());
			InstallErrorLog installErrLog = new InstallErrorLog();
			installErrLog.setErrorTime(new Date());
			installErrLog.setServerUrl(getBasePath());
			installErrLog.setParams(params);
			installLogService.saveInstallErrorLog(installErrLog);
			
			//发邮件
			logger.fatal(String.format("%s\"--App安装日志\"出现错误!%s", 
					getBasePath(), Util.deepToString(getRequest().getParameterMap())), e); 
			*/
			return 0;
		}
	}
	
	
	/**
	 * 内置方法（本项目中没有地方调用才能够删除或注释掉！）
	 * 参数说明
	 * 
	 * @param empId
	 * @param pkguuid
	 * @param osType
	 * @param osVersion
	 * @param rscVersion
	 * @param imoduletype
	 * @param source
	 * @param versionId
	 * @param phoneIMEI
	 * @param imsi
	 * @param installType
	 * @param isAutoOpen
	 * @param isGuide
	 * @param isOtherNet
	 * @param isSmsVal
	 * @param manuName
	 * @param modelName
	 * @param optStatus
	 * @param phoneNumber
	 * @param psn
	 * @param typeId
	 * @param seqNo
	 * @param pkgId
	 * @param uuid （这里传入的uuid为null，除了离线上传以外，其他安装日志的uuid均需从session中获取）
	 * 
	 * @return
	 */
	public Integer saveAppInstallLogzy(Integer empId, String pkguuid, Integer osType, String osVersion, String rscVersion, String imoduletype, Integer source, 
			Integer versionId, String phoneIMEI, String imsi, Integer installType, Integer isAutoOpen, Integer isGuide, Integer isOtherNet, Integer isSmsVal, 
			String manuName, String modelName, Integer optStatus, String phoneNumber, String psn, Integer typeId, String seqNo, String pkgId, String uuid){
		try {
			//根据empId查询员工对象
			Employee emp = empService.findById(empId);
			
			//根据emp.OrgId查询厅信息
			BusinessHall hall = hallService.findById(emp.getOrgId());
			
			//根据业务包的id查询业务对象，获取其名称
			String pkgName = "";
			if(pkgId != null && !"".equals(pkgId) && Integer.parseInt(pkgId)>0){
				if("14".equals(imoduletype) || "15".equals(imoduletype)){//查询阅联的
					List<UnionreadPkgView> list = resourceService.findPackUuidByPkgId(emp.getProvince().getId(), pkgId);
					for (int i = 0; i < list.size(); i++) {
						pkgName = list.get(0).getPkgName();
					}
				}else{
					Pkg p = resourceService.findPkgByPkgId(Integer.parseInt(pkgId));
					if(p != null){
						pkgName = p.getName();
					}
				}
			}else if(pkgId == null || "".equals(pkgId)){
				pkgId = "0";
			}
			
			InstallLogzy logzy = new InstallLogzy();
			/**
			 * cp01、cp02（灵动应用）的 source 为 0;
			 * cp03 （沃商店） 的source 为 2;
			 * cp04 （安讯） 的 source 为 3;
			 * cp05（拓维） 的 source 为 1 ;
			 * cp06 （Mobile Market） 的 source 为 5;
			 * cp09 （灵动APP） 的 source 为 0;
			 */
			//根据安装包的pkguuid 查询AppInstallPack对象
			AppInstallPack appPkg = resourceService.findAppPkgByid(pkguuid);
			if("cp01".equals(appPkg.getCp().getId()) || "cp02".equals(appPkg.getCp().getId()) || "cp09".equals(appPkg.getCp().getId())){
				source = 0;
			}else if("cp03".equals(appPkg.getCp().getId())){
				source = 2;
			}else if("cp04".equals(appPkg.getCp().getId())){
				source = 3;
			}else if("cp05".equals(appPkg.getCp().getId())){
				source = 1;
			}else if("cp06".equals(appPkg.getCp().getId())){
				source = 5;
			}
			
			logzy.setCpId(appPkg.getCp().getId());//cp商 
			logzy.setPackUuid(appPkg.getUuid());//包软件UUID
			logzy.setRscid(appPkg.getOriginalPackId());//软件的原始Id
			
			App app = appPkg.getApp();
			logzy.setAppUuid(app.getUuid());//软件的UUID  
			logzy.setRscName(app.getName());//安装软件名称
			logzy.setSourceType(app.getRscType().getId());//-101***、102***之类的
			
			if((phoneIMEI == null || "".equals(phoneIMEI)) && (imsi == null || "".equals(imsi)) && (phoneNumber == null || "".equals(phoneNumber))){
				phoneIMEI = new SimpleDateFormat("yyyyMMddHHmmsss").format(new Date());
			}
			logzy.setProvinceId(emp.getProvince().getId());
			logzy.setProvinceName(emp.getProvince().getName());
			logzy.setCityId(emp.getCity().getId());
			logzy.setCityName(emp.getCity().getName());
			logzy.setCountyId(emp.getCounty().getId());
			logzy.setCountyName(emp.getCounty().getName());
			logzy.setEmpId(emp.getId().toString());
			logzy.setEmpName(emp.getName());
			logzy.setEmpNo(emp.getEmpNo());
			logzy.setHallId(emp.getOrgId());
			logzy.setHallName(hall.getHallName());
			logzy.setHallno(hall.getHallNo());
			logzy.setChanType(hall.getChanType());
			logzy.setChanGrade(hall.getChanGrade());
			logzy.setChanLevel(hall.getChanLevel());
			logzy.setOstype(osType);
			logzy.setOsVersion(osVersion);
			if(rscVersion.length()>32){//安装软件版本
				logzy.setRscVersion(rscVersion.substring(0, 32));
			}else{
				logzy.setRscVersion(rscVersion);
			}
			logzy.setImoduletype(imoduletype);
			logzy.setSource(source);
			logzy.setVersionId(versionId);
			logzy.setImei(phoneIMEI);
			logzy.setImsi(imsi);
			logzy.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			logzy.setInstallType(installType);
			logzy.setIsAutoOpen(isAutoOpen);
			logzy.setIsGuide(isGuide);
			logzy.setIsOtherNet(isOtherNet);
			logzy.setIsSmsVal(isSmsVal);
			logzy.setManuName(manuName);
			logzy.setModelName(modelName);
			logzy.setFilterrule(null);
			logzy.setOptStatus(optStatus);
			logzy.setPhoneNumber(phoneNumber);
			logzy.setPsn(psn);
			logzy.setBhid(hall.getBhid());
			logzy.setCcid(hall.getCcid());
			logzy.setRccid(hall.getRccid());
			logzy.setTypeid(typeId);
			logzy.setPkgId(Integer.parseInt(pkgId));
			logzy.setPkgName(pkgName);
			if(uuid == null){//不为null才是离线上传，离线上传是无需从session中获取uuid的
				if(SessionAccessor.getPhoneLoginUuid() != null){
					logzy.setUuid(SessionAccessor.getPhoneLoginUuid().toString());//下载操作产生的uuid
				}else{
					logzy.setUuid("");
				}
			}else{
				logzy.setUuid(uuid);
			}
			logzy.setSeqNo(seqNo);
			
			installLogService.saveInstallLogzy(logzy);
			
			//客户端上传安装日志后, app的installCount+1
			if(app != null){
				resourceService.updateTotalInstallCount(app.getUuid());
			}
			
			return 1;
		} catch (Exception e) {
			/**
			 * 日志接口出现错误时, 发送邮件+向数据库存错误日志
			 * serverUrl : 出现错误的shop地址
			 * params: 出错的日志参数
			
			String params = Util.deepToString(getRequest().getParameterMap());
			InstallErrorLog installErrLog = new InstallErrorLog();
			installErrLog.setErrorTime(new Date());
			installErrLog.setServerUrl(getBasePath());
			installErrLog.setParams(params);
			installLogService.saveInstallErrorLog(installErrLog);
			
			//发邮件
			logger.fatal(String.format("%s\"--App安装日志\"出现错误!%s", 
					getBasePath(), Util.deepToString(getRequest().getParameterMap())), e); 
			*/
			return 0;
		}
	}
	
	//######################################################################################################################
	
	/***************************************************************
	 * 以下是访问器
	 ****************************************************************/
	public Integer getAppType() {
		return appType;
	}

	public void setAppType(Integer appType) {
		this.appType = appType;
	}

	public String getManuName() {
		return manuName;
	}

	public void setManuName(String manuName) {
		this.manuName = manuName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getHallId() {
		return hallId;
	}

	public void setHallId(Integer hallId) {
		this.hallId = hallId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneIMEI() {
		return phoneIMEI;
	}

	public void setPhoneIMEI(String phoneIMEI) {
		this.phoneIMEI = phoneIMEI;
	}

	public Integer getOsType() {
		return osType;
	}

	public void setOsType(Integer osType) {
		this.osType = osType;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Integer getSmsSwitch() {
		return smsSwitch;
	}

	public void setSmsSwitch(Integer smsSwitch) {
		this.smsSwitch = smsSwitch;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Integer getWosoftId() {
		return wosoftId;
	}

	public void setWosoftId(Integer wosoftId) {
		this.wosoftId = wosoftId;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpPwd() {
		return empPwd;
	}

	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}

	public Integer getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(Integer operatorName) {
		this.operatorName = operatorName;
	}

	public Integer getInstallType() {
		return installType;
	}

	public void setInstallType(Integer installType) {
		this.installType = installType;
	}

	public Integer getDtId() {
		return dtId;
	}

	public void setDtId(Integer dtId) {
		this.dtId = dtId;
	}

	public Integer getPowerType() {
		return powerType;
	}

	public void setPowerType(Integer powerType) {
		this.powerType = powerType;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public int getOpt() {
		return opt;
	}

	public void setOpt(int opt) {
		this.opt = opt;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getExportId() {
		return exportId;
	}

	public void setExportId(String exportId) {
		this.exportId = exportId;
	}

	public Integer getOptStatus() {
		return optStatus;
	}

	public void setOptStatus(Integer optStatus) {
		this.optStatus = optStatus;
	}

	public String getTypeNameNew() {
		return typeNameNew;
	}

	public void setTypeNameNew(String typeNameNew) {
		this.typeNameNew = typeNameNew;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getPsn() {
		return psn;
	}

	public void setPsn(String psn) {
		this.psn = psn;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	public String getLdcVersion() {
		return ldcVersion;
	}

	public void setLdcVersion(String ldcVersion) {
		this.ldcVersion = ldcVersion;
	}

	public String getVersions() {
		return versions;
	}

	public void setVersions(String versions) {
		this.versions = versions;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getSoftId() {
		return softId;
	}

	public void setSoftId(String softId) {
		this.softId = softId;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public void setEmpService(EmpService empService) {
		this.empService = empService;
	}

	public void setSimpleQueryService(SimpleQueryService simpleQueryService) {
		this.simpleQueryService = simpleQueryService;
	}

	public void setLoginSecurityService(LoginSecurityService loginSecurityService) {
		this.loginSecurityService = loginSecurityService;
	}

	public Integer getIsOtherNet() {
		return isOtherNet;
	}

	public void setIsOtherNet(Integer isOtherNet) {
		this.isOtherNet = isOtherNet;
	}

	public Integer getIsSmsVal() {
		return isSmsVal;
	}

	public void setIsSmsVal(Integer isSmsVal) {
		this.isSmsVal = isSmsVal;
	}

	public Integer getIsGuide() {
		return isGuide;
	}

	public void setIsGuide(Integer isGuide) {
		this.isGuide = isGuide;
	}

	public Integer getIsAutoOpen() {
		return isAutoOpen;
	}

	public void setIsAutoOpen(Integer isAutoOpen) {
		this.isAutoOpen = isAutoOpen;
	}

	public String getRscVersion() {
		return rscVersion;
	}

	public void setRscVersion(String rscVersion) {
		this.rscVersion = rscVersion;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public Integer getProId() {
		return proId;
	}

	public void setProId(Integer proId) {
		this.proId = proId;
	}

	public String getPkguuid() {
		return pkguuid;
	}

	public void setPkguuid(String pkguuid) {
		this.pkguuid = pkguuid;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getImoduletype() {
		return imoduletype;
	}

	public void setImoduletype(String imoduletype) {
		this.imoduletype = imoduletype;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}
	
	public String getPkgId() {
		return pkgId;
	}

	public void setPkgId(String pkgId) {
		this.pkgId = pkgId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getClientType() {
		return clientType;
	}

	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}

	public String getErrDesc() {
		return errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	public String getAunm() {
		return aunm;
	}

	public void setAunm(String aunm) {
		this.aunm = aunm;
	}

	public String getInstype() {
		return instype;
	}

	public void setInstype(String instype) {
		this.instype = instype;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getWv() {
		return wv;
	}

	public void setWv(String wv) {
		this.wv = wv;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImob() {
		return imob;
	}

	public void setImob(String imob) {
		this.imob = imob;
	}

	public String getPkid() {
		return pkid;
	}

	public void setPkid(String pkid) {
		this.pkid = pkid;
	}

	public String getPkname() {
		return pkname;
	}

	public void setPkname(String pkname) {
		this.pkname = pkname;
	}

	public String getOsversion() {
		return osversion;
	}

	public void setOsversion(String osversion) {
		this.osversion = osversion;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getBrname() {
		return brname;
	}

	public void setBrname(String brname) {
		this.brname = brname;
	}

	public String getMd5Msg() {
		return md5Msg;
	}

	public void setMd5Msg(String md5Msg) {
		this.md5Msg = md5Msg;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public Integer getTableType() {
		return tableType;
	}

	public void setTableType(Integer tableType) {
		this.tableType = tableType;
	}
	
}
