package com.i8app.shop.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.i8app.shop.dao.AreaUpdateProgramDao;

public class UpdateServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String version = request.getParameter("version"); 
		String type = request.getParameter("type"); 
		String areaCode = request.getParameter("areaCode"); 
		
		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		AreaUpdateProgramDao dao = (AreaUpdateProgramDao)applicationContext.getBean(AreaUpdateProgramDao.class);
		List<String> list = dao.softVersion(version, type, areaCode);
		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter pw = response.getWriter();
		for(int i=0; i<list.size(); i++)	{
			pw.print(list.get(i) + ";");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
