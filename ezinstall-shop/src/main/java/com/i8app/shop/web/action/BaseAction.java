package com.i8app.shop.web.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;

import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.shop.common.PhoneOs;
import com.i8app.shop.common.config.ConfigProp;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 基本Action, 常用的Action方法 
 * @author JingYing 2011-12-7
 */
public class BaseAction extends ActionSupport 
				implements SessionAware, ServletRequestAware, 
				ServletResponseAware, ServletContextAware{
	
	protected static ConfigProp configProp = ConfigProp.getInstance(); 
	protected static final String 
	 		AJAX = "ajax",
			NOT_FOUND = "notfound", 
			FILESERVER_URL = configProp.getFileServerUrl();;	//fileServer的网络地址
	protected String pageSize3 = configProp.getPageSize3();
	protected String qz_pkgId = configProp.getQz_pkgId();//阅联的强制安装包id
	protected String woStore_NATION_PKG = configProp.getWoStore_NATION_PKG();
	protected String woJia_NATION_PKG = configProp.getWoJia_NATION_PKG();
	protected String shanXi_NATION_PKG = configProp.getShanXi_NATION_PKG();
	protected int offset, pageSize;		//pager-taglib用参数
	protected String _, timespandata;	//避免新版本STRUTS提示错误信息, 对前台经常使用的_和timespandata加getset
	
	
	public String getFileserverUrl() {
		return FILESERVER_URL;
	}
	
	/**
	 * 获取本机项目的根路径
	 * @return
	 */
	protected String getBasePath()	{
		return String.format("%s://%s:%s%s/", request.getScheme(), request.getServerName(), 
				request.getServerPort(), request.getContextPath());
	}
	
	/**
	 * 获取request方对本服务器请求的地址
	 * @return
	 */
	public String getRequestURI() {
//		return getRequest().getRequestURI();	//在JSP上调用时,显示的是JSP地址.
		ActionProxy proxy = ActionContext.getContext().getActionInvocation().getProxy();
		String uri = request.getContextPath() + proxy.getNamespace()+ "/" + proxy.getActionName() + ".action";
		return uri;
	}
	
	/**
	 * 以UTF-8格式向浏览器打印字符串
	 * @param s
	 */
	protected String doPrint(Object obj) {
		this.getResponse().setContentType("text/html;charset=utf-8");
		PrintWriter pw;
		try {
			pw = this.getResponse().getWriter();
			pw.print(obj);//不要使用println,防止客户端接收到/r/n
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected Map<String, Object> session;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected ServletContext servletContext;
	

	/**
	 * 之前在web.xml配置了sourcePath常量, 在页面上使用${application.sourcePath}调用,
	 * 去掉sourcePath常量后, 为了不修改jsp, 在servletContext加入sourcePath保持兼容性
	 */
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		this.servletContext.setAttribute("sourcePath", FILESERVER_URL);
	}

	
	/**
	 * 根据操作系统和版本转换cp商 
	 * 
	 * @param appOsType
	 * @param version
	 * @return Os
	 */
	public Os adaptToOs(Integer appOsType, String version) {
		PhoneOs phoneOs = PhoneOs.getInstance(appOsType, version, "", "");
		Os os = null;
		if (phoneOs == PhoneOs.ERROR || phoneOs == PhoneOs.ANDROID) {
			os = Os.ANDROID;// 未知的系统和android系统统一
		} else if (phoneOs == PhoneOs.IPHONE) {
			os = Os.IOS;
		} else if (phoneOs == PhoneOs.IPHONEX) {
			os = Os.IOSX;
		} else if (phoneOs == PhoneOs.SYMBIAN) {
			os = Os.SYMBIAN;
		} else if (phoneOs == PhoneOs.WINDOWS_MOBILE) {
			os = Os.WINMOBILE;
		} else if (phoneOs == PhoneOs.JAVA) {
			os = Os.JAVA;
		} 
		return os;
	}

	
	/****************************************
	 * 访问器
	 *****************************************/
	
	protected HttpServletResponse getResponse() {
		return this.response;
	}

	protected HttpServletRequest getRequest() {
		return this.request;
	}
	
	protected Map<String, Object> getSession(){
		return this.session;
	}
	
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public String get_() {
		return _;
	}

	public void set_(String _) {
		this._ = _;
	}

	public String getTimespandata() {
		return timespandata;
	}

	public void setTimespandata(String timespandata) {
		this.timespandata = timespandata;
	}
}
