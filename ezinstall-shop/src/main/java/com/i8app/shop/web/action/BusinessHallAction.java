package com.i8app.shop.web.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.shop.domain.Province;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.Channeltype;
import com.i8app.shop.domain.Collaboratechannel;
import com.i8app.shop.domain.Reifiercollaboratechannel;
import com.i8app.shop.service.HallService;

/**
 * FAQ页面Action
 * 
 */
@SuppressWarnings("serial")
@Controller
@Scope("prototype")
public class BusinessHallAction extends BaseAction {

	@Resource
	private HallService hallService;

//	@Resource
//	private EmpService empService;
	
	private BusinessHall hall;
//	private Channeltype ct;
	private Collaboratechannel cc;
	private Reifiercollaboratechannel rcc;
	
	private List<Province> proList;
	
	private List<Channeltype> channelList;
	
	private List<Collaboratechannel> ccList;
	
	private List<Reifiercollaboratechannel> rccList;

	private Integer hallId;
	private Integer ctid;
	private Integer ccid;
	
	

	/**
	 * 修改厅信息
	 */
	public String editHall()throws Exception{
		hall = hallService.findById(hallId);
		//通过 ctid 、ccid 、rccid 分别查出其对象
//		ct = hallService.findByCtid(hall.getCtid());
		cc = hallService.findByCcid(hall.getCcid());
		rcc = hallService.findByRccid(hall.getRccid());
		
		//加载所有的 渠道类型
		channelList = hallService.findCtList();
		return SUCCESS;
	}
	
	public String ccList()throws Exception {
		ccList = hallService.findCcList(ctid);
		
		StringBuilder sbJson = new StringBuilder();
		sbJson.append("[");
		for (int i=0; i<ccList.size(); i++) {
			Collaboratechannel cc = ccList.get(i);
			sbJson.append("{");
			sbJson.append("\"id\":\"");
			sbJson.append(cc.getId());
			sbJson.append("\",\"name\":\"");
			sbJson.append(cc.getCcName());
			sbJson.append("\"}");
			if (i < (ccList.size()-1)) {
				sbJson.append(",");	
			}
		}
		sbJson.append("]");
		this.doPrint(sbJson.toString());
		return AJAX;
	}
	
	public String rccList()throws Exception {
		rccList = hallService.findRccByccId(ccid);
		
		StringBuilder sbJson = new StringBuilder();
		sbJson.append("[");
		for (int i=0; i<rccList.size(); i++) {
			Reifiercollaboratechannel rcc = rccList.get(i);
			sbJson.append("{");
			sbJson.append("\"id\":\"");
			sbJson.append(rcc.getId());
			sbJson.append("\",\"name\":\"");
			sbJson.append(rcc.getRccName());
			sbJson.append("\"}");
			if (i < (rccList.size()-1)) {
				sbJson.append(",");	
			}
		}
		sbJson.append("]");
		this.doPrint(sbJson.toString());
		return AJAX;
	}
	
	public String updateHall() {
		hallService.addOrUpdateHall(hall);
		return SUCCESS;
	}



	/**************************************
	 * 以下是访问器
	 ***********************************/
	public HallService getHallService() {
		return hallService;
	}
	public void setHallService(HallService hallService) {
		this.hallService = hallService;
	}
	public List<Province> getProList() {
		return proList;
	}
	public void setProList(List<Province> proList) {
		this.proList = proList;
	}
	public List<Channeltype> getChannelList() {
		return channelList;
	}
	public void setChannelList(List<Channeltype> channelList) {
		this.channelList = channelList;
	}
	public List<Collaboratechannel> getCcList() {
		return ccList;
	}
	public void setCcList(List<Collaboratechannel> ccList) {
		this.ccList = ccList;
	}
	public List<Reifiercollaboratechannel> getRccList() {
		return rccList;
	}
	public void setRccList(List<Reifiercollaboratechannel> rccList) {
		this.rccList = rccList;
	}
	public BusinessHall getHall() {
		return hall;
	}
	public void setHall(BusinessHall hall) {
		this.hall = hall;
	}
	public Integer getHallId() {
		return hallId;
	}
	public void setHallId(Integer hallId) {
		this.hallId = hallId;
	}
	public Integer getCtid() {
		return ctid;
	}
	public void setCtid(Integer ctid) {
		this.ctid = ctid;
	}
	public Integer getCcid() {
		return ccid;
	}
	public void setCcid(Integer ccid) {
		this.ccid = ccid;
	}

//	public Channeltype getCt() {
//		return ct;
//	}
//
//	public void setCt(Channeltype ct) {
//		this.ct = ct;
//	}

	public Collaboratechannel getCc() {
		return cc;
	}

	public void setCc(Collaboratechannel cc) {
		this.cc = cc;
	}

	public Reifiercollaboratechannel getRcc() {
		return rcc;
	}

	public void setRcc(Reifiercollaboratechannel rcc) {
		this.rcc = rcc;
	}
	
	
}
