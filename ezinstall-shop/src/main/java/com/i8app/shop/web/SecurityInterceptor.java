package com.i8app.shop.web;

import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * 过滤页面上传递过来的特殊字符
 * @author JingYing
 *
 */
public class SecurityInterceptor implements Interceptor	{
	private static Logger logger = Logger.getLogger(SecurityInterceptor.class);

	/**
	 * 过滤参数中的特殊字符, 不能过滤,和; 最好不要过滤()
	 */
	String sensitiveInput = "[<>&$%@'|\\+\"]";
	
	/**
	 * 过滤URL地址中的特殊字符
	 */
	String sensitiveUrl = "[%<>]";
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		HttpServletRequest req = (HttpServletRequest) invocation.getInvocationContext().get(
				StrutsStatics.HTTP_REQUEST);
		Matcher m = Pattern.compile(sensitiveUrl).matcher(req.getRequestURL().toString());
		if(m.find())	{
			logger.error("发现URL中存在敏感字符: " + req.getRequestURL().toString());
			return Action.ERROR;
		}
		
		Map<String, Object> map = invocation.getInvocationContext().getParameters();
		for(Entry e : map.entrySet())	{
			String[] ss = (String[])e.getValue();
			for(int i=0; i<ss.length; i++)	{
				ss[i] = ss[i].replaceAll(sensitiveInput, "");
			}
		}
		invocation.getInvocationContext().setParameters(map);
		return invocation.invoke();
	}

}
