package com.i8app.shop.web;

import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.domain.Employee;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * 检查营业厅人员是否登录
 * @author jing
 */
public class EmpLoginInterceptor implements Interceptor	{

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Employee emp = SessionAccessor.getEmp();
		if(emp == null)
			return Action.LOGIN;
		else
			return invocation.invoke();
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
	}
}
