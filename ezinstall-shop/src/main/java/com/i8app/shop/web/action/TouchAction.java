package com.i8app.shop.web.action;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.shop.domain.ManuInfo;
import com.i8app.shop.domain.TypeInfo;
import com.i8app.shop.service.SimpleQueryService;
import com.i8app.shop.service.TypeInfoService;

@Controller
@Scope("prototype")
public class TouchAction extends BaseAction	{

	private int id, manuId;
	private String action, typeId, initial;
	
	private List<List<ManuInfo>> manuLists;
	private Map<String, String> initialMap;
	private List<Map<String, String>> initialLists;
	private List<List<TypeInfo>> typeInfoLists;
	private List<TypeInfo> typeInfoList;
	
	@Resource
	private SimpleQueryService simpleQueryService;
	@Resource
	private TypeInfoService typeInfoService;
	
	@Override
	public String execute()	{
		if("manuList".equals(action))
			return this.manuList();
		else if("phoneList".equals(action))
			return this.phoneList();
		else if("phoneListMain".equals(action))
			return this.phoneListMain();
		else
			throw new IllegalArgumentException("action参数无效");
	}
	
	public String manuList()	{
		pageSize = 16;
		int count = simpleQueryService.manuList().size();
		manuLists = new LinkedList<List<ManuInfo>>();
		for(int i=0; i < count; i += pageSize)	{
			List<ManuInfo> t = simpleQueryService.manuList(i, pageSize).getList();
			manuLists.add(t);
		}
		return "manuList";
	}
	
	
	public String phoneList()	{
		pageSize = 100;		//查询所有记录
		initialMap = typeInfoService.findInitialPic(manuId, 0, pageSize);
		typeInfoList = typeInfoService.findByManuId(manuId);
		return "phoneList";
	}
	
	public String phoneListMain()	{
		typeInfoList = typeInfoService.findByInitial(initial, manuId);
		return "phoneListMain";
	}
	
	
	/**********************************************************************************
	 * 以下是访问器
	 ***********************************************************************************/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<List<ManuInfo>> getManuLists() {
		return manuLists;
	}

	public void setManuLists(List<List<ManuInfo>> manuLists) {
		this.manuLists = manuLists;
	}
	public int getManuId() {
		return manuId;
	}

	public void setManuId(int manuId) {
		this.manuId = manuId;
	}

	public List<Map<String, String>> getInitialLists() {
		return initialLists;
	}

	public void setInitialLists(List<Map<String, String>> initialLists) {
		this.initialLists = initialLists;
	}

	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public List<List<TypeInfo>> getTypeInfoLists() {
		return typeInfoLists;
	}

	public void setTypeInfoLists(List<List<TypeInfo>> typeInfoLists) {
		this.typeInfoLists = typeInfoLists;
	}

	public Map<String, String> getInitialMap() {
		return initialMap;
	}

	public void setInitialMap(Map<String, String> initialMap) {
		this.initialMap = initialMap;
	}

	public List<TypeInfo> getTypeInfoList() {
		return typeInfoList;
	}

	public void setTypeInfoList(List<TypeInfo> typeInfoList) {
		this.typeInfoList = typeInfoList;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
