package com.i8app.shop.web.action;


import javax.annotation.Resource;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.service.EmpService;

/**
 * FAQ页面Action
 * 
 */
@SuppressWarnings("serial")
@Controller
@Scope("prototype")
public class EmployeeAction extends BaseAction {

	@Resource
	private EmpService empService;
	
	private Employee emp;
	
	private String empNo;
	private Integer provId;
	private String empPwd;//新密码
	
	public String editEmp() {
		emp = empService.findByNoAndProId(empNo, provId);
		JSONObject empJo = new JSONObject();
		try {
			empJo.put("id", emp.getId());
			empJo.put("empNo", emp.getEmpNo());
			empJo.put("name", emp.getName());
			empJo.put("empPwd", emp.getEmpPwd());
			empJo.put("province", emp.getProvince());
			empJo.put("city", emp.getCity());
			empJo.put("county", emp.getCounty());
			empJo.put("orgId", emp.getOrgId());
			empJo.put("orgLevel", emp.getOrgLevel());
			empJo.put("orgIdStr", emp.getOrgIdStr());
			empJo.put("orgNameStr", emp.getOrgNameStr());
			empJo.put("phoneNumber", emp.getPhoneNumber());
			empJo.put("bank", emp.getBank());
			empJo.put("bankAccount", emp.getBankAccount());
			empJo.put("bankUser", emp.getBankUser());
			empJo.put("isDeleted", emp.getIsDeleted());
			empJo.put("isFreezed", emp.getIsFreezed());
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		
		return this.doPrint(empJo.toString());
	}
	
	public String editEmpPwd() {
		emp = empService.findByNoAndProId(empNo, provId);
		emp.setEmpPwd(empPwd);
		empService.saveOrUpdate(emp);
		
		return null;
	}

	public String updateEmp(){
		empService.saveOrUpdate(emp);
		return SUCCESS;
	}
	


	/**************************************
	 * 以下是访问器
	 ***********************************/
	public EmpService getEmpService() {
		return empService;
	}
	public void setEmpService(EmpService empService) {
		this.empService = empService;
	}

	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public Integer getProvId() {
		return provId;
	}

	public void setProvId(Integer provId) {
		this.provId = provId;
	}

	public String getEmpPwd() {
		return empPwd;
	}

	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}
	
}
