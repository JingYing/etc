package com.i8app.shop.web.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.shop.common.AuthcodeException;
import com.i8app.shop.common.EmpNotLoginException;
import com.i8app.shop.common.NoSuchRecordException;
import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.common.SmsAccessException;
import com.i8app.shop.common.Util;
import com.i8app.shop.common.config.Config_i8;
import com.i8app.shop.domain.AppActive;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.CityCustomize;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.InstantMsg;
import com.i8app.shop.domain.Mail;
import com.i8app.shop.domain.ManuInfo;
import com.i8app.shop.domain.Mobile;
import com.i8app.shop.domain.TypeInfo;
import com.i8app.shop.service.EmpService;
import com.i8app.shop.service.ISmsService;
import com.i8app.shop.service.SimpleQueryService;
import com.i8app.shop.service.TypeInfoService;

/**
 * 该类用于客户端调用, 只做纯字符串显示的功能, 不做页面跳转. 不响应页面点击.
 * 
 * @author jing
 * 
 */
@Controller
@Scope("prototype")
public class PrintAction extends BaseAction {
	
	private static final Logger logger = Logger.getLogger(PrintAction.class);

	@Resource
	private ISmsService smsService;
	@Resource
	private EmpService empService;
	@Resource
	private TypeInfoService typeInfoService;
	@Resource
	private SimpleQueryService simpleQueryService;
	@Resource
	private Config_i8 config_i8;

	private int id, manuId, typeId, softId, operator, hallId, empId, count, provId;
	private String ids, phoneNumber, areaCode, typeName, manuName, authCode,
			orgIdStr, empNo, info, account, user, bank, phoneNo, deviceNo, imsi, imei, osType;
	private long from;
	private Integer detectType;

	/**
	 * 发送验证码
	 * 
	 * @return
	 */
	public String sendAuthCode() {
		int result;
		try {
			String gen = smsService.sendAuthcode(phoneNumber, SessionAccessor.getAreaCode());
			SessionAccessor.setAuthcode(gen);
			result = 0;
		} catch (EmpNotLoginException e) {
			result = 1;
		} catch (SmsAccessException e) {
			result = 2;
		} catch (NoSuchBeanDefinitionException e) {
			result = 3;
		}

		JsonObject json = new JsonObject();
		json.addProperty("errorcode", result);
		return this.doPrint(json.toString());
	}
	
	/**
	 * 新发送验证码
	 * @param phoneNumber 
	 * @param areaCode
	 * @return JsonObject
	 */
	public String sendAuthCode2() {
		int result;
		String gen = "";
		try {
			String areaCode = config_i8.getProvAreaMap().get(provId);
			gen = smsService.sendAuthcode(phoneNumber, areaCode);
			result = 0;
		} catch (EmpNotLoginException e) {
			result = 1;
		} catch (SmsAccessException e) {
			result = 2;
		} catch (NoSuchBeanDefinitionException e) {
			result = 3;
		}

		JsonObject json = new JsonObject();
		json.addProperty("errorcode", result);
		json.addProperty("num", gen);
		return this.doPrint(json.toString());
	}

	/**
	 * 检查验证码
	 * 
	 * @return
	 */
	public String checkAuthCode() {
		int result;
		String desc = "";
		try {
			String saved = SessionAccessor.getAuthcode();
			if(saved == null)	{
				throw new RuntimeException("session中未找到保存的验证码");
			} 
			if(!saved.equals(authCode))	{
				throw new AuthcodeException("输入的验证码有误:" + authCode);
			}
			result = 0;
		} catch (AuthcodeException e) {
			result = 1;
			desc = e.getMessage();
		} catch (EmpNotLoginException e) {
			result = 2;
		} catch (SmsAccessException e) {
			result = 3;
		} catch (NoSuchBeanDefinitionException e) {
			result = 4;
		} catch (Exception e)	{
			result = 500;
		}

		JsonObject json = new JsonObject();
		json.addProperty("errorcode", result);
		json.addProperty("desc", desc);
		return this.doPrint(json.toString());
	}


	/**
	 * 邮件列表
	 * 
	 * @return
	 */
	public String mailList() {
		List<Mail> mailList = simpleQueryService.mailList();
		StringBuilder sb = new StringBuilder();
		for (Mail m : mailList) {
			sb.append(String.format("%s,%s,%s;", m.getMailType(),
					m.getMailAddress(), m.getPop()));
		}
		return this.doPrint(sb.toString());
	}

	/**
	 * 从ManuInfo表里查询所有的厂商
	 * 
	 * @param 无
	 * @return 返回格式: manuInfo.manuid,manuInfo.manuname;
	 */
	public String manuInfoList() {
		List<ManuInfo> manuList = simpleQueryService.manuList();
		StringBuilder sb = new StringBuilder();
		for (ManuInfo m : manuList) {
			sb.append(String.format("%s,%s;", m.getId(), m.getManuName()));
		}
		return this.doPrint(sb.toString());
	}

	/**
	 * 根据传入的manuId, 获得对应的所有typeInfo记录
	 * 
	 * @param manuId
	 * @return 返回格式: typeInfo.id, typeInfo.typeName, typeInfo.osInfo.osType,
	 *         typeInfo.osInfo.osVersion;
	 */
	public String typeInfoList() {
		List<TypeInfo> typeInfoList = typeInfoService.findByManuId(manuId);
		StringBuilder sb = new StringBuilder();
		for (TypeInfo t : typeInfoList) {
			sb.append(String.format("%s,%s,%s,%s;", t.getId(), t.getTypeName(),
					t.getOsInfo() == null ? "" : t.getOsInfo().getOsType(),
					t.getOsInfo() == null ? "" : t.getOsInfo().getOsVersion()));
		}
		return this.doPrint(sb.toString());
	}

	/**
	 * 根据typeId查询对应的压缩图片地址
	 * 
	 * @return
	 */
	public String phonePic() {
		TypeInfo typeInfo = typeInfoService.findById(typeId);
		if ((typeInfo != null) && (typeInfo.getBigPic() != null))
			this.doPrint(typeInfo.getBigPic());
		return null;
	}
	
	/**
	 * 根据 manuName、typeName 查询对应的压缩图片地址
	 * 匹配规则：
	 * 1.厂商和机型各自进行双向匹配
	 * 2.机型对厂商及型号进行双向匹配
	 * 
	 * @return
	 */
	public String phonePic2() {
		TypeInfo typeInfo = typeInfoService.findByTemp(manuName, typeName, osType);
		if ((typeInfo != null) && (typeInfo.getBigPic() != null))
			this.doPrint(typeInfo.getBigPic());
		return null;
	}
	

	/**
	 * 根据TYPEINFO的主键得到该条记录的TYPENAME和MANUNAME;
	 * 
	 * @return
	 */
	public String phoneSel() {
		TypeInfo t = typeInfoService.findById(typeId);
		if (t != null)
			this.doPrint(t.getTypeName() + "," + t.getManuInfo().getManuName()
					+ ";");
		return null;
	}


	/**
	 * 获取SYMBIAN手机的TYPEINFO
	 * 
	 * @return
	 */
	public String getOsversion() {
		TypeInfo t = simpleQueryService.getSymbianTypeInfo(typeName, manuName);
		if (t != null)
			this.doPrint(t.getOsInfo().getOsVersion() + ","
					+ t.getOsInfo().getOsType());
		return null;
	}

	/**
	 * 获得沃商店软件的信息
	public String getWoinfo() {
		Wosoft soft = (Wosoft) wosoftService.findById(softId);
		return doPrint(soft.getWoMobileType() + "," + soft.getUrl());
	}
	 */

	/**
	 * 根据手机号判断运营商 入参: phoneNumber 返回: 运营商代号
	 * 
	 * @return
	 */
	public String getMNO() {
		String s;
		try {
			Integer result = simpleQueryService.getMNO(phoneNumber);
			s = result + "";
		} catch (IllegalArgumentException e) {
			s = e.getMessage();
		} catch(Exception e)	{
			logger.error(e);
			s = e.getMessage();
		}
		return doPrint(s);
	}

	/**
	 * 根据hallId和运营商, 查找该市的isBounce的值 入参: hallId, operator 返回: 0或1
	 * 
	 * @return
	 */
	public String checkCityCustomize() {
		BusinessHall b = simpleQueryService.findBusinessHallById(hallId);
		String result;
		if (b != null) {
			List<CityCustomize> list = simpleQueryService.findCityCustomize(
					b.getId(), operator);
			if (list.isEmpty() || list.get(0).getIsBounce())
				result = "0";
			else
				result = "1";
		} else {
			result = "-1";
		}
		return doPrint(result);
	}

	/**
	 * 获取emp信息
	 * 
	 * @return
	 */
	public String getEmp() {
		Employee emp = empService.findEmpByNo(empNo);
		if (emp != null)
			return doPrint(emp.getId() + "," + emp.getEmpNo() + ","
					+ emp.getOrgId());
		else
			return doPrint("");
	}

	public String empPhone() {
		Employee emp = empService.findById(empId);
		String result = "";
		if (emp != null)	{
			result = String.format("%s,%s,%s,%s", 
				emp.getPhoneNumber() == null ? "" : emp.getPhoneNumber(), 
				emp.getBank() == null ? "" : emp.getBank(), 
				emp.getBankAccount() == null ? "" : emp.getBankAccount(), 
				emp.getBankUser() == null ? "" : emp.getBankUser());
		} 
		return doPrint(result);
	}

	public String updateEmpPhone() {
		int errorcode;
		try {
			empService.updatePhoneNumber(empId, phoneNumber, account, user, bank);
			errorcode = 1;
		} catch(NoSuchRecordException e)	{
			errorcode = 2;
		} catch (Exception e) {
			errorcode = 0;
		}
		return doPrint(errorcode);
	}

	/**
	 * 查询appActive表, 入参:"源类型,应用id;源类型,应用id;", 出参: "源类型,应用id,状态;源类型,应用id,状态;"
	 * 
	 * @Param info=50001,1;50003,22;60001,3333;
	 * @return 50001,1,1;50003,22,0;60001,3333,1;
	 */
	public String appActive() {
		String[] infos = info.split(";");
		StringBuilder sb = new StringBuilder();
		for (String s : infos) {
			int[] ints = Util.toIntArray(s.split(","));
			AppActive a = simpleQueryService.findAppActive(ints[0], ints[1]);
			sb.append(ints[0] + "," + ints[1] + ","
					+ (a == null ? "0" : a.getIsActive()) + ";");
		}
		return doPrint(sb.toString());
	}

	/**
	 * 查询即时消息
	 * 
	 * @return
	 */
	public String instantMsg() {
		List<InstantMsg> list = simpleQueryService.findInstantMsg(from,
				areaCode, count);
		JsonObject json = new JsonObject();
		json.addProperty("name", "msginfo");
		JsonArray ja = new JsonArray();
		for (InstantMsg i : list) {
			ja.add(i.toJson());
		}
		json.add("array", ja);
		return doPrint(json.toString());
	}
	
	public String uploadMobile()	{
		int errorcode = 0;
		String desc = null;
		try	{
			Mobile m = new Mobile();
			m.setDetectType(detectType);
			m.setDeviceNo(deviceNo);
			m.setImei(imei);
			m.setImsi(imsi);
			m.setPhoneNo(phoneNo);
			simpleQueryService.persistMobile(m);
		} catch(Exception e)	{
			logger.error(e.getMessage(), e);
			errorcode = 500;
			desc = e.getMessage();
		}
		return doPrint(resultJson(errorcode, desc).toString());
	}
	
	private JsonObject resultJson(int errorcode, String desc)	{
		JsonObject json = new JsonObject();
		json.addProperty("errorcode", errorcode);
		json.addProperty("desc", desc);
		return json;
	}

	/****************************************************
	 * 访问器
	 ****************************************************/
	public int getManuId() {
		return manuId;
	}

	public void setManuId(int manuId) {
		this.manuId = manuId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getManuName() {
		return manuName;
	}

	public void setManuName(String manuName) {
		this.manuName = manuName;
	}

	public int getSoftId() {
		return softId;
	}

	public void setSoftId(int softId) {
		this.softId = softId;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getOrgIdStr() {
		return orgIdStr;
	}

	public void setOrgIdStr(String orgIdStr) {
		this.orgIdStr = orgIdStr;
	}

	public int getOperator() {
		return operator;
	}

	public void setOperator(int operator) {
		this.operator = operator;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public void setSimpleQueryService(SimpleQueryService simpleQueryService) {
		this.simpleQueryService = simpleQueryService;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public long getFrom() {
		return from;
	}

	public void setFrom(long from) {
		this.from = from;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Integer getDetectType() {
		return detectType;
	}

	public void setDetectType(Integer detectType) {
		this.detectType = detectType;
	}

	public int getProvId() {
		return provId;
	}

	public void setProvId(int provId) {
		this.provId = provId;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}
	
}
