package com.i8app.shop.dao;

import javax.annotation.Resource;

import org.junit.Test;

import com.i8app.shop.common.BaseTest;

public class EmployeeDaoTest extends BaseTest	{
	
	@Resource
	private EmployeeDao dao;
	
	@Test
	public void testFindById()	{
		System.out.println(dao.findById(1));
	}
	
	@Test
	public void testFindByNoAndProvId()	{
		dao.findByNoAndProvId("7572008145", new int[]{19, 49});
	}
	

}
