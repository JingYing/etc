package com.i8app.shop.dao;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import com.i8app.shop.common.BaseTest;
import com.i8app.shop.common.Pager;
import com.i8app.shop.domain.TypeInfo;

public class TypeInfoDaoTest extends BaseTest	{

	@Resource TypeInfoDao typeInfoDao;
	
	@Test
	public void testfindByInitial()	{
		List<TypeInfo> list = typeInfoDao.findByInitial(null, 1);
		for(TypeInfo t : list)
			System.out.println(t.getTypeName());
	}

	@Test
	public void testfindByInitial2()	{
		Pager<TypeInfo> pager = typeInfoDao.findByInitial("", 1, 0, 5);
		System.out.println("总记录数" + pager.getTotalCount());
		for(TypeInfo t : pager.getList())
			System.out.print(t.getTypeName() + "  ");
	}
	
	@Test
	public void testfindFirstByInitial()	{
		TypeInfo t = typeInfoDao.findFirstByInitial("e", 1);
	}
	
	@Test
	public void testlist4Symbian()	{
		List<Integer> list = typeInfoDao.list4Symbian("e", "nokia");
		System.out.println(list);
	}

	@Test
	public void testlist4Java()	{
		List<Integer> list = typeInfoDao.list4Java("5700", "nokia");
		System.out.println(list);
	}

	@Test
	public void testsearch()	{
		Pager<TypeInfo> list = typeInfoDao.search("5", 0, 5);
		for(TypeInfo t : list.getList())
			System.out.println(t.getTypeName());
	}
	
	@Test
	public void testfindInitial()	{
		Pager<String> pager = typeInfoDao.findInitial(1, 0, 3);
		System.out.println("总记录数==" + pager.getTotalCount());
		for(String s : pager.getList())	{
			System.out.println(s);
		}
	}
}
