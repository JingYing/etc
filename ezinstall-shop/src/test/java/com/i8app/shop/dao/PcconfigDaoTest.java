package com.i8app.shop.dao;

import javax.annotation.Resource;

import org.junit.Test;

import com.i8app.shop.common.BaseTest;

public class PcconfigDaoTest extends BaseTest{
	
	@Resource PcConfigDao dao1;
	@Resource PcConfigTwoDao dao2;
	@Resource PcConfigThreeDao dao3;
	@Resource PcConfigFourDao dao4;
	@Resource PcConfigFiveDao dao5;
	
	@Test
	public void test()	{
		System.out.println(dao1.getPcConfig("0101"));
		System.out.println(dao2.getPcConfigTwo("1201"));
		System.out.println(dao3.getPcConfigThree("1201"));
		System.out.println(dao4.getPcConfigFour("1201"));
		System.out.println(dao5.getPcConfigFive("1201"));
	}

}
