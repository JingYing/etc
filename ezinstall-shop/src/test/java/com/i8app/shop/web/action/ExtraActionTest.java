package com.i8app.shop.web.action;

import static org.easymock.EasyMock.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;

import com.i8app.shop.domain.Announcement;
import com.i8app.shop.service.SimpleQueryService;
import com.opensymphony.xwork2.ActionProxy;

public class ExtraActionTest extends StrutsSpringTestCase {
	
	@Test
	public void testAnnounceWithNoneAnnou() throws Exception {
		request.setParameter("areaCode", "6");
		ActionProxy proxy = getActionProxy("/print/announce");
		ExtraAction action = (ExtraAction) proxy.getAction();
		SimpleQueryService mock = createNiceMock(SimpleQueryService.class);
		expect(mock.findAnnounce("6")).andReturn(new ArrayList());
		replay(mock);

		action.setSimpleQueryService(mock);
		proxy.execute();
		verify(mock);
		assertEquals("", response.getContentAsString());
	}

	@Test
	public void testAnnounceWithIsNotUrl() throws Exception {
		request.setParameter("areaCode", "6");
		ActionProxy proxy = getActionProxy("/print/announce");
		ExtraAction action = (ExtraAction) proxy.getAction();
		SimpleQueryService mock = createNiceMock(SimpleQueryService.class);
		List<Announcement> list = new ArrayList<Announcement>();
		Announcement a = new Announcement("title", "content", "6",
				"2012-11-28", 1, false);
		list.add(a);
		expect(mock.findAnnounce("6")).andReturn(list);
		replay(mock);

		action.setSimpleQueryService(mock);
		proxy.execute();
		verify(mock);
		assertEquals("content", response.getContentAsString());
	}
	
	@Test
	public void testAnnounceWithIsUrl()	throws Exception	{
		request.setParameter("areaCode", "6");
		ActionProxy proxy = getActionProxy("/print/announce");
		ExtraAction action = (ExtraAction) proxy.getAction();
		SimpleQueryService mock = createNiceMock(SimpleQueryService.class);
		List<Announcement> list = new ArrayList<Announcement>();
		Announcement a = new Announcement("title", "1", "6",
				"2012-11-28", 1, true);
		list.add(a);
		expect(mock.findAnnounce("6")).andReturn(list);
		replay(mock);
		
		action.setSimpleQueryService(mock);
		assertEquals("success", proxy.execute());
		verify(mock);
		assertEquals(1, action.getId());
	}
	
	@Test
	public void testSpecAnnounce() throws Exception	{
		request.setParameter("id", "1");
		ActionProxy proxy = getActionProxy("/extra/specAnnounce");
//		ExtraAction action = (ExtraAction)proxy.getAction();
		proxy.execute();
	}
	
	@Test
	public void testUploadExcept() throws Exception	{
		request.setParameter("descript", "descdescdescdesc");
		request.setParameter("exceptid", "1");
		request.setParameter("itype", "2");
		request.setParameter("mac", "macmacmac");
		ActionProxy proxy = getActionProxy("/extra/uploadExcept");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}

}
