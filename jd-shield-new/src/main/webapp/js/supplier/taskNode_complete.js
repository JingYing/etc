//id:nodeId
function taskNode_compelete(id,url,callback){
	var taskId = $("#taskId").val();
	var u = "/supplier/taskNode_updateStatus";
	var c = "/supplier/task_view?id="+taskId;
	if(url == null || url == '')
		url = u;
	if(callback == null || callback == '')
		callback = c;
	Dialog.confirm("确认","确定完成","是","否",function(result){
        if(result){
        	if(id == "")
        		return;
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+url,
                data:{
                    id:id
                },
                success:function (data) {
                	if(typeof(data) != 'object')
                		data = eval("("+data+")");
                	if (data.operator == true){
                		Dialog.alert("成功！",data.message);
                		loadUrl(springUrl+c);
                	}else{
                    	Dialog.alert("失败！",data.message);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","系统异常");
                }
            });
        }
    });
}

//返回任务单
function back2TaskView(){
	var taskId = $("#taskId").val();
	loadUrl(springUrl+"/supplier/task_view?id="+taskId);
}