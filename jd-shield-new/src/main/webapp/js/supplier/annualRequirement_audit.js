$(function(){
	
	$("#requirement_audit_btn").click(function(){
		var param;
		if($("#approvalStatus").val()==2){
			param=validate($("#rejectPanl"));
		}else{
			param=validate($("#auditPanl"));
		}
		if(param){
			editRequirementRecord1();
		}else{
			return false;
		}
		return false;
	});
	
	
	$("#tenderStartDate").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });
}); 

function clearAddPane(){
	loadUrl("/supplier/annualReqAudit_index");
}



function editRequirementRecord1() {
	var urlpara = "/supplier/annualReqAudit_save";
	//把表单的数据进行序列化 
	var params = $("#auditRequirementForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("完成审核");
			Dialog.hide();
			loadUrl("/supplier/annualReqAudit_index");
		},
		error : function(xhr, st, err) {
			alert("审核失败!");
		}
	});
}


function changeRequired(){
	var param = document.getElementById("approvalStatus").value;
	if(param==2){
		$("#auditPanl").hide("fast");
	}
	if(param==3){
		$("#auditPanl").show("fast");
	}
}



