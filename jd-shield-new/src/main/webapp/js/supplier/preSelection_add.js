﻿var preSelectionAddTable;
$(document).ready(function () {
	bindValidate($('#preSelectionForm'));
	var columns1 =  [
{ "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
		return "<input type=\"checkbox\" value=\""+data.aData.supplierId+"\" class=\"checkbox\">";
	}},	                 
{ "mData": "id","sTitle":"id","bSortable":false,"bVisible":false},
{ "mData": "enterpriseName","sTitle":"供应商全称","bSortable":false,"bVisible":true,"sDefaultContent":""},
{ "mData": "funds","sTitle":"注册资金","bSortable":true,"sClass": "my_class","bVisible":true,"sDefaultContent":"0"},
{ "mData": "modifyTime","sTitle":"最后更新" ,"bSortable":false,"fnRender":function(data){
	return new Date(data.aData.modifyTime).format("yyyy-MM-dd hh:mm:ss");
}}
];
		
		var  btns1 = [
					{
					    "sExtends":    "text",
					    "sButtonText": "添加到预选列表",
					    "sButtonClass": "btn btn-green mr10",
					    "sToolTip": "",
					    "fnClick": function ( nButton, oConfig, oFlash ) {
					    	batchAdd();
					    }
					},
		            {
			              "sExtends":    "text",
			              "sButtonText": "新增特殊供应商",
			              "sButtonClass": "btn btn-green mr10",
			              "sToolTip": "",
			              "fnClick": function ( nButton, oConfig, oFlash ) {
			            	  addSepcial();
			              }
		            },
		            {
			              "sExtends":    "text",
			              "sButtonText": "返回",
			              "sButtonClass": "btn btn-green mr10",
			              "sToolTip": "",
			              "fnClick": function ( nButton, oConfig, oFlash ) {
			            	  back2Index();
			              }
		            }
					];

	    preSelectionAddTable = $('#preSuppSelectionAddTable').dataTable( {
		           
		        	"bProcessing": false,
		            "bServerSide":true,
		            "sPaginationType": "full_numbers",
		            "sAjaxSource":springUrl+"/supplier/preSelection_page4Enterprise",
		            "sServerMethod": "POST",
		            "bStateSave": false,
		            "sScrollY":"100%",
		            "bScrollCollapse": true,
		            "bPaginate":true,
		            "bAutoWidth":true,
		            "oLanguage": {
		                "sLengthMenu": "每页显示 _MENU_ 条记录",
		                "sZeroRecords": "抱歉， 没有找到",
		                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
		                "sInfoEmpty": "没有数据",
		                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
		                "oPaginate": {
		                    "sFirst": "首页",
		                    "sPrevious": "前页",
		                    "sNext": "后页",
		                    "sLast": "尾页"}
		            },
		            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span11'i><'span11'p>>",
		            "sPaginationType": "bootstrap",
		            "bJQueryUI": true,
		            "bFilter":false,
		            "fnServerData":function ( sSource, aoData, fnCallback) {
//						aoData.push( { "name": "userName", "value":userName },{ "name": "organizationId", "value" : nodeId },{ "name": "organizationName", "value" : organizationName });
		            	var taskId=$("input#taskId").val()==null?'':$("input#taskId").val();
		            	var supplierOwner = $("#supplierOwner").val();
		            	var enterpriseType = $("#enterpriseType").val();
//		            	var qualification = $("#qualification").val();
		            	var currency = $("#currency").val();
		            	var funds = $("#funds").val();
		            	var employeeCount = $("#employeeCount").val();
		            	var enterpriseName = $("#enterpriseName").val();
		            	var content = $("#content").val();
		    			aoData.push( { "name": "taskId", "value":taskId },{ "name": "supplierOwner", "value" : supplierOwner },{ "name": "currency", "value" : currency },
		    					{ "name": "enterpriseType", "value" : enterpriseType },
		    					{ "name": "funds", "value" : funds },{ "name": "employeeCount", "value" : employeeCount },
		    					{ "name": "enterpriseName", "value" : enterpriseName },{ "name": "content", "value" : content });
						jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    data:aoData,
		                    async: false,
		                    dataType : "json",
		                    success : function(resp) {
								//Table.currentPage=resp.current_page;
								   fnCallback(resp);
		                    },
							error : function(data){
								
							}
						});
		            },
		            "fnDrawCallback": function( oSettings ){
						/*添加回调方法*/
		            },
		            "aaSorting":[],//默认排序
		            "aoColumns":columns1,
		            "oTableTools":{
		                "aButtons":btns1
//		                "sRowSelect":"multi"
		            }
		        } );
		
		//隐藏dataTable 数据条目信息
		//jQuery(".dataTables_info").parent().hide();
		
//		document.getElementById('mainPanel').style.display = '';
//		$('#workPanel').hide();
		//搜索按钮
		$('#preSelection_btn').click(function() {
			if(!validate($('#preSelectionForm'))){
				return false;
			}
			preSelectionAddTable.fnDraw();
	    });
});
//新增

function addToPreSelection(id){
	var taskId = $("#taskId").val(); 
	var nodeId = $("#nodeId").val();
	Dialog.confirm("确认","确定添加该供应商到预选名单吗?","是","否",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/supplier/preSelection_addSave",
                data:{
                    supplierId:id,
                    taskId:taskId
                },
                success:function (data) {
                	if(typeof(data) != 'object')
                		data = eval("("+data+")");
                	if (data.operator == true){
                		Dialog.alert("成功！",data.message);
                		loadUrl(springUrl+"/supplier/preSelection_add?taskId="+taskId+"&nodeId="+nodeId);
                	}else{
                    	Dialog.alert("失败！",data.message);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","系统异常");
                }
            });
        }
    });
}

/**
 * 返回到预选列表
 */
function back2Index(){
	var nodeId = $("#nodeId").val();
	var taskId = $("#taskId").val();
	loadUrl(springUrl+"/supplier/preSelection_index?taskId="+taskId+"&nodeId="+nodeId);
}

/**
 * 批量添加
 */
function batchAdd(){
	var taskId = $("#taskId").val(); 
	var nodeId = $("#nodeId").val();
	var checkArry = $(".checkbox:checked");
	 var ids="";
	 checkArry.each(function(){
		 ids += ","+ $(this).val();
	 });
	 ids = ids.substr(1,ids.length);
    var length = ids.length;
    ids = ids.toString();
    if(length==0){
        Dialog.alert("消息提示","没有选中项","确定");
    }else{
    	Dialog.confirm("确认","确定添加该供应商到预选名单吗?","是","否",function(result){
    		if(result){
    				jQuery.ajax({
    					type:"POST",
    					cache:false,
    					url: springUrl+"/supplier/preSelection_addSave",
    					data:{
    						supplierIds:ids,
    						taskId:taskId
    					},
    					success:function (data) {
    						if(typeof(data) != 'object')
    							data = eval("("+data+")");
    						if (data.operator == true){
    							Dialog.alert("成功！",data.message);
    							loadUrl(springUrl+"/supplier/preSelection_add?taskId="+taskId+"&nodeId="+nodeId);
    						}else{
    							Dialog.alert("失败！",data.message);
    						}
    					},
    					error: function (data) {
    						Dialog.alert("失败","系统异常");
    					}
    				});
    		}
    	});
    }
}

//添加特殊供应商
function addSepcial(){
	var buttons = [
                   {
                   "label": "取消",
                   "class": "btn btn-green mr10",
                   "callback": function (modal) {
                   }
               },
               {
                   "label": "保存",
                   "class": "btn btn-green mr10",
                   "callback": function (modal) {
                	   if(!validate($('#preSelectionAddSpecialForm'))){
           				return false;
           				}
                       addSaveSepcial();
                       return false;//不让弹窗自己关闭
                   }
               }
           ];
	Dialog.openRemote('preSelectionAddSpec','新增特殊供应商', springUrl+'/supplier/preSelection_addSpecial', 600, 400, buttons);
}

//保存特殊供应商
function addSaveSepcial(){
	var name = $("#name_as").val();
	var mobile = $("#mobile_as").val();
	var contactor = $("#contactor_as").val();
	var remarks = $("#remarks_as").val();
	var taskId = $("#taskId").val();
	//$('#preSelectionAddSpecialForm').serialize(),
	jQuery.ajax({
		type:"POST",
		cache:false,
		url: springUrl+"/supplier/preSelection_addSaveSpecial",
		dataType:"json",
		data:{
			name:name,
			mobile:mobile,
			contactor:contactor,
			remarks:remarks,
			taskId:taskId
		},
		success:function (data) {
			if(typeof(data) != 'object')
				data = eval("("+data+")");
			if (data.operator == true){
//				loadUrl(springUrl+"/supplier/preSelection_add?taskId="+$("input#taskId").val());
				Dialog.hide();
				Dialog.alert("成功！",data.message);
			}else{
				Dialog.alert("失败！",data.message);
			}
		},
		error: function (data) {
			Dialog.alert("失败","系统异常");
		}
	});
}


function toggleChecks(obj)
{
    $('.checkbox').prop('checked', obj.checked);
}