$(function(){
	
	$('#requirement_save_btn').click(function() {
		editRequirementRecord1(0);
        return false;
		}
	);
	
	$('#requirement_submit_btn').click(function() {
		 if(!validate($("#editPane"))){
            return false;
        }else{
        	editRequirementRecord1(1);
        }
        return false;
		}
	);
	
	
	$("#originalContractDate,#tenderFinishDate").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });
	
});


function clearAddPane(){
	loadUrl("/supplier/annualRequirement_index");
}



function editRequirementRecord1(e) {
	var urlpara = "/supplier/annualRequirement_save?status=" + e;
	//把表单的数据进行序列化 
	var params = $("#editRequirementForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("操作成功!");
			Dialog.hide();
			loadUrl("/supplier/annualRequirement_index");
		},
		error : function(xhr, st, err) {
			alert("操作失败!");
		}
	});
}


function selectSysUser(){
	var buttons = [
	               {
	                   "label": "取消",
	                   "class": "btn-success",
	                   "callback": function (modal) {
	                   }
	               },
	               {
	                   "label": "确定",
	                   "class": "btn-success",
	                   "callback": function (modal) {
	                	   $('input[name=radio]').each(function(){
	                		   if($(this).attr("checked")=="checked"){
	                			   $("#projectManagerId").val($(this).attr("userName"));
	                			   $("#projectManager").val($(this).attr("realName"));
	                		   }
	                		});
	                   }
	               }
	           ];
	Dialog.openRemote('user','选择用户',springUrl+'/system/address_getOrgUser?source=radio',600,600,buttons);
}
