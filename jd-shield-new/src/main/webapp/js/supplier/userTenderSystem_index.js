var table;
var tableData;
$(function(){ 
//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='system' value=\""+data.aData.id+"\" fileName=\""+data.aData.fileName+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","sDefaultContent":"","bSortable": false},
	            {"mData": "fileName", "sDefaultContent":"","sTitle": "文件名称" , "bSortable": true , "fnRender": function (obj) {
					return '&nbsp;&nbsp;<a href="#" title="打开" style="font-size:12px;" onclick="showSystem(this,\''+obj.aData.fileKey+'\')" >'+obj.aData.fileName+'</a>';		        	
		        }
	            },
		        {"mData": "id", "sDefaultContent":"","sTitle": "操作", "bSortable": false,"fnRender": function (obj) {
		        	return '<a style="font-size:12px;" onclick="showSystem(this,\''+obj.aData.fileKey+'\')" title="查看" href="javascript:void(0)" >查看</a>';
		        	
		        	
		        }
		        }
	        ];


 table = $('#tenderSystemTable').dataTable( {
	 		"sDefaultContent":"requirement",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/supplier/tenderSystemPage",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
				var fileName=$("input#fileName").val();
				if(fileName==null) fileName="";
				fileName=fileName.replace("_","\\_");
				var dictTypeId=$("input#dictTypeId").val();
				if(dictTypeId==null) dictTypeId="";
				dictTypeId=dictTypeId.replace("_","\\_");
				
				aoData.push( 
						{ "name": "fileName", "value":fileName },
						{ "name": "dictTypeId", "value":dictTypeId }
						);  
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":""
                
            }
        } );


	$('#expression_search').click(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); } 
		}
	);
	

	
	$('#fileName').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );
		
	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
	bindValidate($("#supplierSearchForm"));
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}



//查看文件
function showSystem(o,fileKey){
	var fileName=$(o).parent().siblings().eq(0).find("input").attr('fileName');
	var url=springUrl+"/supplier/downFilename?filename="+fileName+"&filekey="+fileKey;
	window.location.href=url;
}




