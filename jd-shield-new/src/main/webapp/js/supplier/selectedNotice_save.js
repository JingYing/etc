

$(function() {
	

	
	 

	$("#selectedNotice_add_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			addSelectedNoticeRecord1();
		}
	});
	
	$("#selectedNotice_update_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			updateSelectedNoticeRecord();
		}
	});
	
	$("#biddate,#publishdate").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });

	//getBaseControlInfo();
	bindValidate($('#addPane'));
	findFinalSelectedByReqId();
	
});


function findFinalSelectedByReqId(){
	var requirementId=$("#requirementId option:selected").val();
	if(requirementId!=null&&requirementId!=""){
		var urlpara = "/supplier/findFinalSelectedByReqId?requirementId="+requirementId;
		//把表单的数据进行序列化 
		$.ajax({
			url : urlpara,
			type : "POST",
			//data : params,
			dataType : "json",
			success : function(data) {
				
				var table1 =$("#selectedSupplierTable");
				var firstTr=table1.find("tobdy>tr:first");
				
				var length=data.length;
				if(length!=0){
					$("#selectedSupplierTable tr").eq(0).nextAll().remove();
					for (var i=0;i<length;i++){
						var row=$("<tr></tr>");
						var td=$("<td>"+(i+1)+"</td>");
						var td1=$("<td>"+data[i].name+"</td>");
						var td2=$("<td>"+parseDate(data[i].applyCreateTime)+"</td>");
						var td3=$("<td>入围</td>");
						row.append(td);
						row.append(td1);
						row.append(td2);
						row.append(td3);
						table1.append(row);
					}
				}else{
					$("#selectedSupplierTable tr").eq(0).nextAll().remove();
					var row=$("<tr></tr>");
					var td=$("<td colspan='4'>没有记录</td>");
					row.append(td);
					table1.append(row);
				}
				
			},
			error : function(xhr, st, err) {
				alert("查询失败!");
			}
		});
	}
	
}

parseDate= function(data){
		 if(data!=""){
			    var date = new Date(data);// 或者直接new Date();
		    return date.format("yyyy-MM-dd");
		 }else return "";
	}


function addSelectedNoticeRecord1() {
	$("#requirementName").val($("#requirementId option:selected").text());
	var urlpara = "/supplier/selectedNotice_save";
	//把表单的数据进行序列化 
	var params = $("#insertSelectedNoticeForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/selectedNotice_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}


function updateSelectedNoticeRecord() {
	$("#requirementName").val($("#requirementId option:selected").text());
	var urlpara = "/supplier/selectedNotice_update";
	//把表单的数据进行序列化 
	var params = $("#updateSelectedNoticeForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/selectedNotice_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}

function selectedNotice_back_btn(){
	loadUrl("/supplier/selectedNotice_index");
}

function showSelectedNoticeYear(){
	if($("#budgetSource option:selected").text()=='计划内'){
		$("#selectedNoticeYearId").attr("style","display:block");
	}else{
		$("#selectedNoticeYearId").attr("style","display:none");
	}
}

function copyProjectName(){
	$("#projectName").val($("#selectedNoticeYearId option:selected").text());
}


Date.prototype.format = function(format) {
    var o = {
    "M+" : this.getMonth() + 1, //month
    "d+" : this.getDate(), //day
    "h+" : this.getHours(), //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth() + 3) / 3), //quarter
    "S" : this.getMilliseconds()
    //millisecond
    }
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
 
    for ( var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
        format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    }
    }
    return format;
}


