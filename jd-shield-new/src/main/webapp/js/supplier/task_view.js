
	function gotoUrl(url,nodeId,roleCode){
		var taskId = $("#taskId").val();
		var preCpUrl = "/supplier/preSelection_index_cp";
		var finalCpUrl = "/supplier/finalSelection_index_cp";
		jQuery.ajax({
	        type:"POST",
	        cache:false,
	        url: springUrl+"/supplier/taskNode_checkNodeStatus",
	        data:{
	        	nodeId:nodeId,
	        	roleCode:roleCode
	        },
	        success:function (data) {
	        	if(typeof(data)!='object')
	        		data = eval("("+data+")");
	        	if(data==9){
	        		loadUrl(springUrl+url+"?taskId="+taskId+"&nodeId="+nodeId);
	        	}else if(data==1){
	        		Dialog.alert("提示","没有访问权限！");
	                return false;
	        	}else if(data==2){
	        		Dialog.alert("提示","有前置任务未完成！");
	                return false;
	        	}else if(data==3){
	        		Dialog.alert("提示","已经完成！");
	                return false;
	        	}else if(data==4){
	        		loadUrl(springUrl+preCpUrl+"?taskId="+taskId+"&nodeId="+nodeId);
	        	}else if(data==5){
	        		loadUrl(springUrl+finalCpUrl+"?taskId="+taskId+"&nodeId="+nodeId);
	        	}
	        },
	        error: function (data) {
	            Dialog.alert("失败","数据字典数据更新失败");
	        }
	    });
	}
	
	
	$(document).ready(function(){
		$(".font-gray").click(function(){
			toggleStatus($(this));
		});
	});
	
	/**
	 * 切换tab页显示
	 * @param obj
	 */
	function toggleStatus(obj){
		obj.removeClass().addClass("font-green");
		obj.css("cursor","");
		obj.parent().siblings().find("b").removeClass().addClass("font-gray");
		obj.parent().siblings().find("b").css("cursor","pointer");
		var no = obj.parent().attr("no");
		$("#"+no).show();
		$("#"+no).siblings(".bg-gray").hide();
		$(".font-gray").click(function(){
			toggleStatus($(this));
		});
	}
	
	

