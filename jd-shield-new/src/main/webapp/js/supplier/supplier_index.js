var table;
var tableData;
	$(document).ready(function () {
		var columns = [
		    {"mDataProp": "id","sTitle":"序号","sWidth": "5%","sDefaultContent": "","bSortable": false},          
			{"mDataProp": "userCode", "sTitle": "账号", "sWidth": "10%","bSortable": false},
            {"mDataProp": "name", "sTitle": "供应商名称", "sWidth": "25%","bSortable": false},
            {"mDataProp": "mobile", "sTitle": "企业电话", "sWidth": "10%","sDefaultContent":"","bSortable": false},
		    {"mDataProp": "email","sTitle": "企业邮箱", "sWidth": "10%","sDefaultContent":"","bSortable": false},
            {"mDataProp": "supplierStatus", "sTitle":"审批状态", "sWidth": "6%","bSortable": false,
            	 "mRender": function (data) {
                     if (data == 0) {
                         return '未审核';
                     } else if(data == 1){
                         return '已审核';
                     }else{
                    	 
                    	 return '驳回';
                     }
                 }},
		    {"mDataProp": "id", "sTitle": "操作","sWidth": "14%", "bSortable": false,"fnRender": function (data, type, full) {
				var editHtml = '<a href="#"  title="编辑" onclick="editSupplier(\'' + data.aData.id + '\');"><img src="' + springUrl + '/static/admin/skin/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;';
				var delHtml = '<a href="#"  title="删除" onclick="delSupplier(\''+ data.aData.id + '\');"><img src="' + springUrl + '/static/admin/skin/img/del.png" alt="删除"/></a>&nbsp;&nbsp;';
				var examineHtml = '<a href="#"  title="审核" onclick="examineSupplier(\''+ data.aData.id + '\');"><img src="' + springUrl + '/static/admin/skin/img/enjoy.png" alt="审核"/></a>&nbsp;&nbsp;';
				var detailHtml ='<a href="#"  title="详情" onclick="detailSupplier(\''+ data.aData.id + '\');"><img src="' + springUrl + '/static/admin/skin/img/search.png" alt="详情"/></a>&nbsp;&nbsp;';
				if (data.aData.supplierStatus == 1) {
					return editHtml + detailHtml;
				}else{
					return editHtml + detailHtml + examineHtml;
				}
	        }}
        ];      
        var tableBtns= [];      
        table = $('#hello').dataTable( {
            "bProcessing": false,//是否显示正在处理的提示 
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/supplier/pageSearchSupplierList",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "sScrollX": "100%",   //表格的宽度
            "bStateSave": false,//保存状态到cookie ******很重要 ， 当搜索的时候页面一刷新会导致搜索的消失。使用这个属性设置为true就可避免了 
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,// 是否使用分页
            "oLanguage": {
            	"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",                
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            "sDom": "<'row-fluid'<'span6'i><'span6'lT>>Rt<'row-fluid'<'span12'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
            	var name=$("input#name").val();
            	var supplierRange=$("select#supplierRange").val();
            	var dataIntegrity=$("select#dataIntegrity").val();
            	var supplierStatus=$("select#supplierStatus").val();
            	var encurrency=$("select#encurrency").val();
            	var funds=$("input#funds").val();
            	var ficurrency=$("select#ficurrency").val();
            	var salesCount=$("input#salesCount").val();
            	var isCooperation=$("select#isCooperation").val();
            	var content=$("input#content").val();
            	var level=$("select#level").val();
				aoData.push( 
						{ "name": "name", "value":name },
						{ "name": "supplierRange", "value":supplierRange },
						{ "name": "dataIntegrity", "value":dataIntegrity },
						{ "name": "supplierStatus", "value":supplierStatus },
						{ "name": "encurrency", "value":encurrency },
						{ "name": "funds", "value":funds },
						{ "name": "ficurrency", "value":ficurrency },
						{ "name": "salesCount", "value":salesCount },
						{ "name": "isCooperation", "value":isCooperation },
						{ "name": "content", "value":content },
						{ "name": "level", "value":level }
						);   
                jQuery.ajax({
                    type: "POST",
					async:true,
                    url: sSource,
                    dataType: "json",
                    data: aoData,
                    success: function (resp) {
                    	tableData=resp;
                        fnCallback(resp);
                    }
                });
            },
            "fnDrawCallback": function( oSettings ){
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 0, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        });
        $('#searchBtn').click(function() {
    		if(validate($("#pageform"))){
    			table.fnDraw(); } 
    		}
    	);  
        
	});
	/**
	 * 新增供应商基本信息
	 * @param data
	 */
	function addSupplier() {
		loadUrl(springUrl+"/supplier/supplier_add");
	}
	/**
	 * 修改供应商基本信息
	 * @param 供应商ID
	 */
	function editSupplier(supplierId) {
		  loadUrl(springUrl +'/supplier/supplier_update?supplierId=' + supplierId);
	}
	/**
	 * 查看供应商所有的基本信息
	 * @param 供应商ID
	 */
	function detailSupplier(supplierId) {
		  loadUrl(springUrl +'/supplier/supplier_detail?supplierId=' + supplierId);
	}
	/**
	 * 根据供应商完成度审核信息
	 * @param 供应商ID
	 */
	function examineSupplier(supplierId) {
		  loadUrl(springUrl +'/supplier/supplier_examine?supplierId=' + supplierId);
	}
	/**
	 * 导入供应商csv文件
	 * @param 供应商ID
	 */
	function importSupplier() {
	    Dialog.openRemote('supplier_import','导入供应商',springUrl+"/supplier/supplier_import",700,600);                                                                                                        
	}
	
//	  function checkinput() 
//	    { 
//	        var strFileFormat = importform.file.value.split('.');//检查上传文件格式 
//	        filetype=strFileFormat[strFileFormat.length-1];
//	        if(filetype== "csv")
//	        { 
//	        	importform.submit();
//	        return true; 
//	        } 
//	        else 
//	        { 
//	        alert( "只能上传.csv文件"); 
//	        return false; 
//	        } 
//	        
//	    } 
	/**
	 * 删除供应商基本信息
	 * @param 供应商ID
	 */
	function delSupplier(supplierId) {
		Dialog.confirm("确认","确定删除吗?","是","否",function(result){
	        if(result){		
			 $.ajax({
		            type: "POST",
		            cache: "false",
		            url: springUrl +'/supplier/supplier_delete?supplierId=' +supplierId,
		            dataType: "json",
		            success: function (data) { 
		                if (data == 1) { 
		                    alert("删除成功");
		                    loadUrl("/supplier/supplier_index");
		                } else {
		                	alert("删除异常,请联系管理员!");
		                    return false;
		                }
		            }
		        });
	       }
	    });
	}
	/**
	 * 刷新供应商信息列表
	 */
	function refreshSupplierTable() {
		table.fnDraw();
	}