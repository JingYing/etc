var table;
var tableData;
$(function(){ 

//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='requirementAduit' value=\""+data.aData.id+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	            {"mData": "projectNum","bSortable":false,"sTitle":"项目编号"},
		        {"mData": "projectName", "sTitle": "项目名称" , "sDefaultContent":"","bSortable": true},
		        {"mData": "budgetAmount", "sTitle": "预算金额" ,"sDefaultContent":"", "bSortable": true},
		        {"mData": "applicant","bSortable":false,"sDefaultContent":"","sTitle":"申请人"},
		        {"mData": "applicantDep","bSortable":false,"sDefaultContent":"","sTitle":"申请部门"},
		        {"mData": "applicantTel","bSortable":false,"sDefaultContent":"","sTitle":"申请电话"},
		        {"mData": "createTime","bSortable":false,"sDefaultContent":"","sTitle":"申请时间","mRender":function(data){
		        	return parseDate(data);
		        }},
		        {"mData": "modifyTime","bSortable":false,"sDefaultContent":"","sTitle":"最后更新","mRender":function(data){
		        	return parseDate(data);
		        }},
		        {"mData": "reqStatus","bSortable":false,"sTitle":"需求状态","fnRender":function(data){
		        	return converReqStatus(data.aData.reqStatus);
		        }},
		        {"mData": "requirementAduit.mark","bSortable":false,"sTitle":"需求回复内容","sDefaultContent":""},
		        {"mData": "id", "sTitle": "操作", "bSortable": false,"mRender": function (data, type, full) {
		        	var id = data;
					return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#'  title='确认' onclick='editRequirementAduit(\""+id+"\");'>审核</a>";
		        	//return "&nbsp;&nbsp;<a href='#' class='btn' title='确认' onclick='saveRequirementAduitPage(\""+id+"\");'><img src='"+springUrl+"/static/admin/skin/img/s' alt='修改'></a>";
		        }}
        
        
	        ];

var  tableBtns  =[
				];


 table = $('#hello').dataTable( {
	 		"sDefaultContent":"requirementAduit",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/supplier/requirementAduit_page",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
                
				var searchProjectName=$("input#searchProjectName").val();
				if(searchProjectName==null) searchProjectName="";
				searchProjectName=searchProjectName.replace("_","\\_");
				var searchProjectNum=$("input#searchProjectNum").val();
				if(searchProjectNum==null) searchProjectNum="";
				projectNum=searchProjectNum.replace("_","\\_");
				aoData.push( 
						{ "name": "searchProjectName", "value":searchProjectName },
						{ "name": "searchProjectNum", "value":searchProjectNum }
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	//alert(oSetting);
            	
            	//alert("ddd");
				/*添加回调方法*/
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );

  	function converReqStatus(data){
  		var reqStatus="";
  		if(data=="1"){
  			reqStatus="需求提交";
  		}else if(data=="2"){
  			reqStatus="需求确认";
  		}else if(data=="3"){
  			reqStatus="驳回";
  		}else if(data=="4"){
  			reqStatus="需求确认会";
  		}
  		return reqStatus;
  	}
  	
  	parseDate= function(data){
 		 if(data!="")
	   	 return new Date(data).format('yyyy-MM-dd');
	   	 else return "";
 	}
  	
	function saveRequirementAduitPage(){
		loadUrl("/supplier/requirementAduit_savepage");
	}

	$('#requirementAduit_search').click(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); } 
		}
	);
	$('#searchProjectName').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );
	$('#searchProjectNum').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );
	$("#requirementAduit_add_btn").click(function(){
		var validateEl=$('#addPane');
		if(validate(validateEl)){
			addRequirementAduitRecord1();
		}
	});
	
	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
	bindValidate($("#supplierSearchForm"));
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}



//显示表达式对应的用户串
function showUsers(supplierId){
	var map = getUsers(supplierId);
//	$("#disp").html(map.usersName);	
//	$("#disp").show();
	if(map==null){
		Dialog.alert("提示","没有用户信息");
	}else{
		Dialog.alert("用户信息如下：",map.usersName);
	}
}

//隐藏表达式对应的用户串
function hideUsers(supplierId){
	$("#disp").hide();
}

//获得表达式对应的用户串Ajax
function getUsers(supplierId){
	var usersMap ;
    var data={
    		"supplierId":supplierId	
    };
	Dialog.post(springUrl+"/supplier/users_by_requirementAduits_search_str",data,function(result){
		usersMap=result;
	});
	return usersMap;
}

//清空添加面板的数据
function clearAddPane(){
	setRequirementAduitId("");
	setRequirementAduitName("");
	setOrganization("","不限","不限");
	setPostion("","不限","不限");
	setUserName("","任何人");
	setLevel("","","");
	setRole("");
	setGroup("");
}




//删除一条表达式
function delRequirementAduitRecord(ids){
	var data={
			"requirementAduitIds":ids
	};
	Dialog.del(springUrl+"/supplier/requirementAduit_delete",data,function(result){
		if(result!=null&&result.length>0){
			Dialog.alert("删除操作提示","【"+result[0].requirementAduitName+"】存在引用无法删除","确定");
		}else{
			Dialog.alert("删除操作提示","操作成功","确定");
			table.fnDraw();
		}

	});
}


$('#searchProjectName').keyup(function() {
	if(validate($("#requirementAduitSearchForm"))){
		table.fnDraw(); 
	} 
} );



//获取要修改操作数据的值
function editRequirementAduit(id) {
	loadUrl("/supplier/requirementAduit_editpage?id=" + id);
}



//判断两个map是否相同
function isEqualMap(map1,map2){
	var bool=true;
	for(var key in map1){
		if(map1[key]!=map2[key]){
			bool=false;
			return false;
		}
	}
	return bool;
}

