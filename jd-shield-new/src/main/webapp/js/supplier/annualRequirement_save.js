$(function() {

	$("#requirement_save_btn").click(function() {
			addRequirementRecord1(0);
	});
	
	$("#requirement_submit_btn").click(function() {
		 if(!validate($("#addPane"))){
	            return false;
	        }else{
	        	addRequirementRecord1(1);
	        }
		 		return false;
	});
	
	$("#originalContractDate,#tenderFinishDate").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });

});



function clearAddPane(){
	loadUrl("/supplier/annualRequirement_index");
}

function addRequirementRecord1(e) {
	var urlpara = "/supplier/annualRequirement_save?status=" + e;
	//把表单的数据进行序列化 
	var params = $("#insertRequirementForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/annualRequirement_index");
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}




function selectSysUser(){
	var buttons = [
	               {
	                   "label": "取消",
	                   "class": "btn-success",
	                   "callback": function (modal) {
	                   }
	               },
	               {
	                   "label": "确定",
	                   "class": "btn-success",
	                   "callback": function (modal) {
	                	   $('input[name=radio]').each(function(){
	                		   if($(this).attr("checked")=="checked"){
	                			   $("#projectManagerId").val($(this).attr("userName"));
	                			   $("#projectManager").val($(this).attr("realName"));
	                		   }
	                		});
	                   }
	               }
	           ];
	Dialog.openRemote('user','选择用户',springUrl+'/system/address_getOrgUser?source=radio',600,600,buttons);
}




function setUserInfo(userId, name) {
	   $("#projectManagerId").val(userId);
	   $("#projectManager").val(name);
	}
