var finalSelectionAddTable;

$(document).ready(function () {
	bindValidate($('#finalSelectionForm'));
	var columns =  [
{ "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
		if(data.aData.supplierId==null || data.aData.supplierId=='')
			return "";
		else
			return "<input type=\"checkbox\" value=\""+data.aData.id+"\" class=\"checkbox\">";
	}},	                 
{ "mData": "id","sTitle":"id","bSortable":false,"bVisible":false},
{ "mData": "enterpriseName","sTitle":"供应商全称","bSortable":false,"bVisible":true,"sDefaultContent":"","fnRender":function(data){
	if(data.aData.supplierId==null || data.aData.supplierId=='')
		return "<span style='color:red'>"+data.aData.enterpriseName+"</span>";
	else
		return data.aData.enterpriseName;
}},
{ "mData": "funds","sTitle":"注册资金","bSortable":true,"sClass": "my_class","bVisible":true,"sDefaultContent":"0"},
{ "mData": "modifyTime","sTitle":"最后更新" ,"bSortable":false,"fnRender":function(data){
	return new Date(data.aData.modifyTime).format("yyyy-MM-dd hh:mm:ss");
}}
];
		
		var  btns = [
		              {
	              "sExtends":    "text",
	              "sButtonText": "添加到入围列表",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
//	            	  Dialog.hide();
	            	  batchAdd();
	              		}
		            },
		            {
		              "sExtends":    "text",
		              "sButtonText": "返回",
		              "sButtonClass": "btn btn-green mr10",
		              "sToolTip": "",
		              "fnClick": function ( nButton, oConfig, oFlash ) {
		            	  back2Index();
		              }
		            }
	          ];

	    finalSelectionAddTable = $('#finalSuppSelectionAddTable').dataTable( {
		           
		        	"bProcessing": false,
		            "bServerSide":true,
		            "sPaginationType": "full_numbers",
		            "sAjaxSource":springUrl+"/supplier/preSelection_page",
		            "sServerMethod": "POST",
		            "bStateSave": false,
		            "sScrollY":"100%",
		            "bScrollCollapse": true,
		            "bPaginate":true,
		            "bAutoWidth":true,
		            "oLanguage": {
		                "sLengthMenu": "每页显示 _MENU_ 条记录",
		                "sZeroRecords": "抱歉， 没有找到",
		                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
		                "sInfoEmpty": "没有数据",
		                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
		                "oPaginate": {
		                    "sFirst": "首页",
		                    "sPrevious": "前页",
		                    "sNext": "后页",
		                    "sLast": "尾页"}
		            },
		            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span11'i><'span11'p>>",
		            "sPaginationType": "bootstrap",
		            "bJQueryUI": true,
		            "bFilter":false,
		            "fnServerData":function ( sSource, aoData, fnCallback) {
		            	var taskId=$("input#taskId").val()==null?'':$("input#taskId").val();
		            	var supplierOwner = $("#supplierOwner").val();
		            	var enterpriseType = $("#enterpriseType").val();
//		            	var qualification = $("#qualification").val();
		            	var currency = $("#currency").val();
		            	var funds = $("#funds").val();
		            	var employeeCount = $("#employeeCount").val();
		            	var enterpriseName = $("#enterpriseName").val();
		            	var content = $("#content").val();
		    			aoData.push( { "name": "taskId", "value":taskId },{ "name": "supplierOwner", "value" : supplierOwner },{ "name": "currency", "value" : currency },
		    					{ "name": "enterpriseType", "value" : enterpriseType },
		    					{ "name": "funds", "value" : funds },{ "name": "employeeCount", "value" : employeeCount },
		    					{ "name": "enterpriseName", "value" : enterpriseName },{ "name": "content", "value" : content });
						jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    data:aoData,
		                    async: false,
		                    dataType : "json",
		                    success : function(resp) {
								//Table.currentPage=resp.current_page;
								   fnCallback(resp);
		                    },
							error : function(data){
								
							}
						});
		            },
		            "fnDrawCallback": function( oSettings ){
						/*添加回调方法*/
		            },
		            "aaSorting":[],//默认排序
		            "aoColumns":columns,
		            "oTableTools":{
		                "aButtons":btns
//		                "sRowSelect":"multi"
		            }
		        } );
		
		//隐藏dataTable 数据条目信息
		//jQuery(".dataTables_info").parent().hide();
		
//		document.getElementById('mainPanel').style.display = '';
//		$('#workPanel').hide();
		//搜索按钮
		$('#finalSelection_btn').click(function() {
			if(!validate($('#finalSelectionForm'))){
					return false;
			}
			finalSelectionAddTable.fnDraw();
	    });
		
		
});


function back2Index(){
	var nodeId = $("#nodeId").val();
	var taskId = $("#taskId").val();
	loadUrl(springUrl+"/supplier/finalSelection_index?taskId="+taskId+"&nodeId="+nodeId);
}
/**
 * 添加入围列表
 */
function addToFinalSelection(id){
	var taskId = $("#taskId").val();
	var nodeId = $("#nodeId").val();
	Dialog.confirm("确认","确定添加该供应商到入围名单吗?","是","否",function(result){
		if(result){
			jQuery.ajax({
				type:"POST",
				cache:false,
				url: springUrl+"/supplier/finalSelection_addSave",
				data:{
					id:id,
					taskId:taskId
				},
				success:function (data) {
					if(typeof(data) != 'object')
						data = eval("("+data+")");
					if (data.operator == true){
						Dialog.alert("成功！",data.message);
						loadUrl(springUrl+"/supplier/finalSelection_add?taskId="+taskId+"&nodeId="+nodeId);
					}else{
						Dialog.alert("失败！",data.message);
					}
				},
				error: function (data) {
					Dialog.alert("失败","系统异常");
				}
			});
		}
	});
}


function batchAdd(){
	var taskId = $("#taskId").val();
	var nodeId = $("#nodeId").val();
	var checkArry = $(".checkbox:checked");
	 var ids="";
	 checkArry.each(function(){
		 ids += ","+ $(this).val();
	 });
	 ids = ids.substr(1,ids.length);
    var length = ids.length;
    ids = ids.toString();
    if(length==0){
        Dialog.alert("消息提示","没有选中项","确定");
    }else{
    	Dialog.confirm("确认","确定添加该供应商到入围名单吗?","是","否",function(result){
    		if(result){
    				jQuery.ajax({
    					type:"POST",
    					cache:false,
    					url: springUrl+"/supplier/finalSelection_addSave",
    					data:{
    						supplierIds:ids,
    						taskId:taskId
    					},
    					success:function (data) {
    						if(typeof(data) != 'object')
    							data = eval("("+data+")");
    						if (data.operator == true){
    							Dialog.alert("成功！",data.message);
    							loadUrl(springUrl+"/supplier/finalSelection_add?taskId="+taskId+"&nodeId="+nodeId);
    						}else{
    							Dialog.alert("失败！",data.message);
    						}
    					},
    					error: function (data) {
    						Dialog.alert("失败","系统异常");
    					}
    				});
    		}
    	});
    }
}


function toggleChecks(obj)
{
    $('.checkbox').prop('checked', obj.checked);
}