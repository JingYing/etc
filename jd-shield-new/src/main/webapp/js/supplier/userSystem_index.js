/**
 * 员工招标制度页面
 * User: hetengfei
 * Date: 14-5-23
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */
var zTree;
//根节点
var zNodes = {
    "id":"0",
    "name":"招标中心制度",
    "isParent":true,
    "iconSkin":"diy1"
};
var setting = {
    async : {
        enable : true,
        url : springUrl+"/supplier/systemUpload_load",
        autoParam : ["id"]
    },
    callback : {
        beforeClick : beforeClick
    },
    data : {
        simpleData : {
            enable : true,
            idKey : 'id',
            idPKey : 'pId',
            rootPid : null
        }
    },
    view: {
        dblClickExpand: true,
        showLine: true,
        selectedMulti: true,
        expandSpeed: "fast"
    }
};


function beforeClick(treeId, treeNode) {
	var id=treeNode.id ;
	$("#dictTypeId").val(treeNode.id);
	if(id=='0'){
		id='48';
		$("#dictTypeId").val('48');
	}
	if(treeNode.id != "null"){
        $("#box-right").load(springUrl+"/supplier/userSystemUpload_view?id="+id);
    }
    var zTree = jQuery.fn.zTree.getZTreeObj("tree");
    zTree.expandNode(treeNode);
    return true;
}



/**
 * 刷新父节点
 * @param pId 父节点ID
 */
function refreshNode(pId){
    var zTree = jQuery.fn.zTree.getZTreeObj("tree");
    if(pId == null){
        var treeNode = zTree.getNodeByTId("tree_1");
        zTree.reAsyncChildNodes(treeNode, "refresh");
    }else{
        var nodes = zTree.transformToArray(zTree.getNodes());
        var tId = "";
        for(var i=0; i<nodes.length; i++){
            if(nodes[i].id == pId){
                tId = nodes[i].tId;
                break;
            }
        }
        if(tId != ""){
            var treeNode = zTree.getNodeByTId(tId);
            zTree.reAsyncChildNodes(treeNode, "refresh");
        }
    }
}


/**
 * 初始化操作
 */
$(function() {
	  var zTree=jQuery.fn.zTree.init($("#tree"), setting, zNodes);
	  var treeNode = zTree.getNodeByTId("tree_1");
	  beforeClick("tree", treeNode);
});