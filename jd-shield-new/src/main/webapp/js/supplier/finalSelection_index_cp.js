/**
 * 数据字典-数据项列表页面JS
 * User: 
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var table;

//初始化页面
$(function() {
	bindValidate($('#finalSelectionForm'));
    
	var columns =  [
	                {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	                { "mData": "id","sTitle":"id","bSortable":false,"bVisible":false},
	                { "mData": "enterpriseName","sTitle":"供应商全称","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                { "mData": "funds","sTitle":"注册资金","bSortable":true,"sClass": "my_class","sDefaultContent":'0',"bSortable":true},
	                { "mData": "beSampleQualified","sTitle":"样品是否合格","bSortable":true,"sClass": "my_class","fnRender":function(data){
	                	return getSelected(data,'beSampleQualified');
	                }},
	                { "mData": "beInvestigationQualified","sTitle":"考察是否合格" ,"bSortable":true,"fnRender":function(data){
	                	return getSelected(data,'beInvestigationQualified');
	                }},
	                { "mData": "beWillingTo","sTitle":"是否愿意投标","bSortable":true,"bVisible":true,"fnRender":function(data){
	                	return getSelected(data,'beWillingTo');
	                }},
	                { "mData": "remarks","sTitle":"备注","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                { "mData": "beFinanceQualified","sTitle":"财务审查是否合格","bSortable":true,"bVisible":true,"fnRender":function(data){
	                	return getSelected(data,'beFinanceQualified');
	                }}
	            ];
	var btns=[
	          {
	              "sExtends":    "text",
	              "sButtonText": "返回",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  //Dialog.hide();
	            	  back2TaskView();
	              }
	          }
	          ];
	
	table=$("#finalSuppSelectionTable").dataTable({
					"bProcessing": false,
				    "bServerSide":true,
				    "sPaginationType": "full_numbers",
				    "sAjaxSource":springUrl+"/supplier/finalSelection_page",
				    "sServerMethod": "POST",
				    "bAutoWidth": false,
				    "bStateSave": false,
				    "sScrollY":"100%",
				    "bScrollCollapse": true,
				    "bPaginate":true,
				    "oLanguage": {
				        "sLengthMenu": "每页显示 _MENU_ 条记录",
				        "sZeroRecords": "抱歉， 没有找到",
				        "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
				        "sInfoEmpty": "没有数据",
				        "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
				        "oPaginate": {
				            "sFirst": "首页",
				            "sPrevious": "前页",
				            "sNext": "后页",
				            "sLast": "尾页"}
				    },
				    "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
				    "sPaginationType": "bootstrap",
				    "bJQueryUI": true,
				    "bFilter":false,
					"fnServerData":function ( sSource, aoData, fnCallback ) {
							var taskId=$("input#taskId").val()==null?'':$("input#taskId").val();
							var enterpriseName = $("#searchEnterpriseName").val();
							var funds = $("#searchFunds").val();
							aoData.push( { "name": "taskId", "value":taskId },
										 { "name": "funds", "value":funds },
										 { "name": "enterpriseName", "value":enterpriseName });
							jQuery.ajax( {
					                    type: "POST", 
					                    url:sSource, 
					                    dataType: "json",
					                    data: aoData, 
					                    success: function(json) {
					                            fnCallback(json);
					                    },
				                        error: function (data) {
				                            Dialog.alert("失败","数据字典数据查询失败");
				                        }
						    });
						},
					"fnDrawCallback": function( oSettings ){
						var that = this;
						this.$('td:first-child').each(function(i){
							that.fnUpdate( i+1, this.parentNode, 0, false, false ); 
					    });          
					},
					"aaSorting":[],//默认排序
					"aoColumns":columns,
					"oTableTools":{
					    "aButtons":btns
					}
		});
	
	
	//查询
	$('#requirement_search').click(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	});
});


function getSelected(data,o){
	var a;
	var s = 'data.aData.' + o;
	if(eval('(' +s+ ')'))
		a = '是';
	else
		a = '否';
	return a;
}


/**
 * 删除单条预选信息
 */
function deleteFinalSelection(id){
	var taskId = $("#taskId").val(); 
	var nodeId = $("#nodeId").val();
    Dialog.confirm("确认","确定删除吗?","是","否",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/supplier/finalSelection_delete",//复用供应商预选的删除
                data:{
                    id:id
                },
                success:function (data) {
                	if(typeof(data) != 'object')
                		data = eval("("+data+")");
                	if (data.operator == true){
                		Dialog.alert("成功！",data.message);
                		loadUrl(springUrl+"/supplier/finalSelection_index?taskId="+taskId+"&nodeId="+nodeId);
                	}else{
                    	Dialog.alert("失败！",data.message);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","系统异常");
                }
            });
        }
    });
}


//检查是否已做\入围
function checkFinish(){
	var taskId = $("#taskId").val();
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/finalSelection_checkFinish",
        data:{
        	taskId:taskId
        },
        success:function (data) {
        	if(typeof(data) != 'object')
        		data = eval("("+data+")");
        	if(data.count>0){
        		var nodeId = $("#nodeId").val();
        		taskNode_compelete(nodeId,"/supplier/finalSelection_complete");
        	}else{
        		Dialog.alert("提示","请添加入围数据！");
        	}
        },
        error: function (data) {
            Dialog.alert("失败","系统异常");
        }
    });
}


