/**
 * 数据字典-数据项修改、查看页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */



/**
 * 初始化操作
 */
$(function() {
	bindValidate($('#preSelectionUpdateForm'));
	var i=1;
	var filename1="";
	var filename2="";
	for(i;i<=2;i++){
		$("#uploadify"+i).uploadify({
			'uploader'       : '/static/supplier/upload/uploadify.swf',
			//'script'         : '/servlet/fileUpload',
			'script'         : '/servlet/fileUpload',
			'cancelImg'      : '/static/supplier/images/cancel.png',
			'buttonImg'      : '/static/supplier/images/btn_upload.png',
			'queueID'        : 'fileQueue'+i,
			'auto'           : false,
			'multi'          : false,
			'fileDesc'		:'*.xlsx;*.doc;',//上传文件类型说明  
			'fileExt'		:'*.xlsx;*.doc;',
			'buttonText'     : '',
			
			onComplete: function (evt, queueID, fileObj, response, data) {
				var arry;
				if(response != null && response != ''){
					arry = response.split("@@");
				}
				if(j==1){
					$("#fileName1").val(arry[0]);
					$("#investigationReportId_u").val(arry[1]);
				}else if(j==2){
					$("#fileName2").val(arry[0]);
					$("#sampleTestReportId_u").val(arry[1]);
				}
				$('#uploadifyDelete'+j).attr("style","display:block");
			},
			onError: function(a, b, c, d) {
				alert("文件上传失败");
			},
			onAllComplete : function (evt ,data){
				//alert("文件上传成功");
				$("#fileQueue"+j).hide();
				$('#uploadifyStart'+j).attr("style","display:none");
				$('#uploadifyCancel'+j).attr("style","display:none");
				$('#uploadifyDelete'+i).attr("style","display:block");
			}
		});
	}
});

var j = 1;
function uploadifyUpload(i){
	$("#fileQueue").show();
	$('#uploadify'+i).uploadifySettings('scriptData');
	$('#uploadify'+i).uploadifyUpload();
	j=i;
}

function showUpload(i){
	$('#uploadifyStart'+i).attr("style","display:block");
	$('#uploadifyCancel'+i).attr("style","display:block");
	$('#uploadifyDelete'+i).attr("style","display:none");
//	$('#uploadifyStart'+i).attr("style","display:none");
//	$('#uploadifyCancel'+i).attr("style","display:none");
	
}

function delUpload(i){
	var key;
	switch(i){
	case 1:
		key = $("#investigationReportId_u").val();
		break;
	case 2:
		key = $("#sampleTestReportId_u").val();
		break;
	}
	Dialog.confirm("操作提示","确定进行删除操作?","是","否",function(result){
		if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/supplier/preSelection_delFile",
                data:{
                    "key":key
                },
                success:function (data) {
                	if(typeof(data) != 'object')
                		data = eval("("+data+")");
                	if(data.result == 1){
//                		Dialog.alert("操作提示","删除成功");
                		switch(i){
                		case 1:
                			$("#investigationReportId_u").val("");
                			$("#fileName1").val("");
                			break;
                		case 2:
                			$("#sampleTestReportId_u").val("");
                			$("#fileName2").val("");
                			break;
                		}
                		showUpload(i);
                	}else{
                		Dialog.alert("操作提示","删除失败");
                	}
                },
                error: function (data) {
                    Dialog.alert("失败","系统异常");
                }
            });
        }
			

	});
	
}


