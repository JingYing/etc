
/**
 * 初始化tags的三级联动列表,tags参数为json格式
 * expert/index, expert/addition, extract/index三个页面用.
 */ 
function initTags(tags)	{
	var option = "<option value=''>-不限-</option>";
	
	for(i in tags)	{
		if(tags[i].parentId == undefined)	{
			$("#tag1").append("<option value=\"" + tags[i].id + "\">" + tags[i].dictDataName + "</option>");
		}
	}
	
	$("#tag1").change(function(){
        $("#tag2 option, #tag3 option").remove();
		$("#tag2,#tag3").append(option);
		for(i in tags)	{
			if(tags[i].parentId == this.value)	{
				$("#tag2").append("<option value=\"" + tags[i].id + "\">" + tags[i].dictDataName + "</option>");
			}
		}
	});
	
	$("#tag2").change(function(){
        $("#tag3 option").remove();
		$("#tag3").append(option);
		for(i in tags)	{
			if(tags[i].parentId == this.value)	{
				$("#tag3").append("<option value=\"" + tags[i].id + "\">" + tags[i].dictDataName + "</option>");
			}
		}
	});
	
}


/**
 * 联动三级下拉菜单. expert/input, expert/edit用.
 * @param tags 所有可用tag, json格式
 * @param trIndex 该组下拉菜单所在的<TR>的id
 * @param initLev1 是否生成第一级下拉菜单
 */
function linkChange(tags, trIndex, initLev1)	{
	var tr = $("#table tr[id=" + trIndex + "]");
	var option = "<option value=''>-不限-</option>";
	if(initLev1)	{
		for(i in tags)	{
			if(tags[i].parentId == undefined)	{
				tr.find("select:first").append("<option value=\"" + tags[i].id + "\">" + tags[i].dictDataName + "</option>");
			}
		}
	}
	
	tr.find("select[id^='tag']").change(function(){
		var nextAll = $(this).parent().nextAll().find("select");
		nextAll.find("option").remove();
		nextAll.append(option);
		
		var next = $(this).parent().next().find("select");
		for(i in tags)	{
			if(tags[i].parentId == this.value)	{
				next.append("<option value=\"" + tags[i].id + "\">" + tags[i].dictDataName + "</option>");
			}
		}
	});
}