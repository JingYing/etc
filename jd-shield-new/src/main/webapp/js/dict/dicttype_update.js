/**
 * 数据字典-数据类别修改、查看页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var dictTypeCode = $('#dictTypeCode').val();
/**
 * 修改文件夹-提交按钮响应函数
 */
function updateDictType(){
	var validateEl=$('#dictTypeForm');
	if(!validate(validateEl)){
		return false;
	}
	var isRepeatDictTypeName=$('#isRepeatDictTypeName').html();
    if(isRepeatDictTypeName!=''){
  	  return false;
    }
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            dictTypeName:$('#dictTypeName').val(),
            dictTypeCode:$('#dictTypeCode').val(),
            parentId:$('#parentName').val(),
            id:$('#id').val(),
            description:$('#description').val()
        },
        url:springUrl+"/dict/dictType_updateSave",
        /*beforeSend:function(){//提交前校验
            $("#dictTypeName").trigger("change.validation",{submitting: true});
            if( $("#dictTypeName").jqBootstrapValidation("hasErrors")){
                return false;
            }
        },*/
        success:function (data) {
            $("#box-right").load(springUrl+"/dict/dictType_view?dictTypeCode="+dictTypeCode);
            if($("#parentName").val() == ""){
                refreshNode("null");
            }
            else{
                refreshNode($("#parentName").val());
            }
        },
        error: function (data) {
            if(data.responseText == null){
                Dialog.alert("失败","修改数据类别失败");
            }else{
                Dialog.alert("失败",data.responseText);
            }
        }
    });
}

/**
 * 修改数据类别-取消按钮响应函数
 */
function cancelDictType(){
    $("#box-right").html("");
    refreshNode($("#parentName").val());
}

/**
 * 初始化操作
 */
$(function() {
	
	$('#dictTypeName').blur(function() {
		validateDictTypeName();
	});
	$('#dictTypeName').focus(function() {
		$("#isRepeatDictTypeName").html("");
	});
	
   /*  $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();*/
    //绑定保存按钮事件
    $('#updateDictType').click(function() {
        updateDictType();
    });
    //绑定取消按钮事件
    $('#cancelDictType').click(function() {
        cancelDictType();
    });
    
    bindValidate($('#dictTypeForm'));
});

function validateDictTypeName(){
	var dictTypeParentId=$("#parentName").val();
	var dictTypeName=$("input#dictTypeName").val();
	var dictTypeNameOld=$("input#dictTypeNameOld").val();
	if(dictTypeName!='' && dictTypeName != dictTypeNameOld){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				dictTypeName:dictTypeName,
				parentId:dictTypeParentId
	        },
	        url:springUrl + "/dict/dictType_isExistDictTypeNamePage",
	        success:function (data) {
	        	if(data){
	        		var str = "此类别名称已存在!";
	        		$("#isRepeatDictTypeName").html(str).css("color","red");
	        	}
	        }
		});
	}
}