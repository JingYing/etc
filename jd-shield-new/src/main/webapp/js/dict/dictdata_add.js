/**
 * 数据字典-数据项新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 初始化操作
 */

$(function() {
	/*数据编码重复性判断*/
	$('#dictDataCode1').blur(function() {
		validateDictDataCode();
	} );
	
	$('#dictDataCode1').focus(function() {
		$("#isRepeatDictDataCode").html("");
	});
	
	/*数据名称重复性判断*/
	$('#dictDataName1').blur(function() {
		validateDictDataName();
	} );
	
	$('#dictDataName1').focus(function() {
		$("#isRepeatDictDataName").html("");
	});
	
	bindValidate($('#dictDataForm'));
//    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
//    $("#dictDataCode").jqBootstrapValidation();
//    $("#dictDataName").jqBootstrapValidation();
//    $("#sortNo").jqBootstrapValidation();
	
	
});

function validateDictDataCode(){
	var dictDataCode=$("input#dictDataCode1").val();
	if(dictDataCode!=''){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				dictDataCode:dictDataCode
	        },
	        url:springUrl + "/dict/dictData_isExistDictDataCode",
	        success:function (data) {
	        	if(data){
	        		var str = "此数据编码已存在!";
	        		$("#isRepeatDictDataCode").html(str).css("color","red");
	        	}
	        }
		});
	}
}

function validateDictDataName(){
	var dictTypeId=$("input#dictTypeId").val();
	var dictDataName=$("input#dictDataName1").val();
	if(dictDataName!=''){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				dictDataName:dictDataName,
				dictTypeId:dictTypeId
	        },
	        url:springUrl + "/dict/dictData_isExistDictDataName",
	        success:function (data) {
	        	if(data){
	        		var str = "此数据名称已存在!";
	        		$("#isRepeatDictDataName").html(str).css("color","red");
	        	}
	        }
		});
	}
}

