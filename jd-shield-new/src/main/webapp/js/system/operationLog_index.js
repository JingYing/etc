var table;
	$(document).ready(function () {
		var columns = [
		    {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},          
			{"mDataProp": "entityName", "sTitle": "业务对象", "bSortable": false,"sWidth":70 },
            {"mDataProp": "operationType", "sTitle": "操作类型", "bSortable": false,"sWidth":60},
            {"mDataProp": "creator", "sTitle": "操作人", "bSortable": false,"sWidth":80 },
		    {"mDataProp": "operationTime","sTitle": "操作时间", "bSortable": true,"sWidth":100,"fnRender": function (obj) {
                return new Date(obj.aData.operationTime).format("yyyy-MM-dd hh:mm:ss");
            }},
            {"mDataProp": "clientIp", "sTitle": "IP", "bSortable": false,"sWidth":80 },
		    {"mDataProp": "url", "sTitle": "URL", "bSortable": false,"sWidth":100 },
		    {"mDataProp": "description", "sTitle": "描述", "bSortable": false,"sWidth":150 }
        ];
        
        var tableBtns= [];
        
        table = $('#sysoperationlog').dataTable( {
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/system/log_list",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
            	var creator = $("input#creator").val();
				var entityName = $("select#entityName").val();
				var description = $("input#description").val();
				var clientIp = $("input#clientIp").val();
				var startTime = $("input#startTime").val();
				var endTime = $("input#endTime").val();
            	
            	aoData.push({ "name": "creator", "value": creator },
					{ "name": "description", "value": description },
					{ "name": "entityName", "value": entityName },
					{ "name": "clientIp", "value": clientIp },
					{ "name": "startTime", "value": startTime },
					{ "name": "endTime", "value": endTime }
					);
 
                jQuery.ajax({
                    type: "POST",
					async:true,
                    url: sSource,
                    dataType: "json",
                    data: aoData,
                    success: function (resp) {
                        fnCallback(resp);
                    }
                });
            },
            "fnDrawCallback": function( oSettings ){
            	var that = this;                        
				//if ( oSettings.bSorted || oSettings.bFiltered ){                
				this.$('td:first-child', {"filter":"applied"}).each( function (i) {                    
					that.fnUpdate( i+1, this.parentNode, 0, false, false );                
				} );            
				//} 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );
        $('#search').click(function () {
        	table.fnDraw();
        });
        
      //页面初始化加载业务对象
        $.ajax({
            type: "POST",
            cache:"false",
            url: springUrl+"/dict/dictData_findDictDataList",
            data: {
            	dictTypeCode: "ENTITY_TYPE"
            },
            dataType: "json",
            success: function(json){
                var ary = new Array();
                var entity = '<option value="">不限</option>';
                if(json != null){
                    for(var i=0;i<json.length;i++){
                    	entity +='<option value="'+json[i].dictDataName+'">'+json[i].dictDataName+'</option>';  
                    }
                }
                $("#entityName").html(entity);
            }
        });
	});