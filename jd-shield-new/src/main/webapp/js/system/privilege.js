var privTable;
var treeObj;

var dataTablesOption = {
	oLanguage : {
        sUrl: "/static/datatables/js/dataTables.chinese.txt"
    },
    bJQueryUI: true,
    sAjaxSource: null,
	fnServerData : function ( sSource, aoData, fnCallback ) {
		var sNodes = treeObj.getSelectedNodes();
		if (sNodes.length > 0) {
			var node = sNodes[0];
			if(!node.isParent){
				$.getJSON(sSource, aoData, function (json) { 
					fnCallback(json);
				});
			}
		}
	},
	
	fnDrawCallback: function( oSettings ){
	},
	
	fnRowCallback : function(nRow, aData, iDisplayIndex){
       $("td:first", nRow).html(iDisplayIndex +1);
       return nRow;
    },
    
	sDom : "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6 text_left'i><'span6'p>>",
	oTableTools: {
		"aButtons": [{
			"sExtends": "text",
			"sButtonText": "新增",
			"fnClick": function ( nButton, oConfig, oFlash ) {
				var sNodes = treeObj.getSelectedNodes();
				if (sNodes.length > 0) {
					var node = sNodes[0];
					if(!node.isParent){
						 bootbox.dialog("",[
				            {
			     				"label": "取消",
			     				"class": "btn-success",
			     				"callback": function(el){
			     					if(el==null){
			     						bootbox.hideAll();
			     					}else{
			     						while(el.hasClass("bootbox")==false){
			     							el=el.parent();
			     						}
			     						el.modal("hide");
			     					}
			     				}
				     		},
				     		{
			     				"label": "保存",
			     				"class": "btn-success",
			     				"callback": function(){
			     					if (!validate($("#addPrivilege"))) {
			     						return false;
			     					} else {
			     						save_insert();
			     					}
			     				}
				     		}],
				        	{
				               "header": '选择功能操作',
				        	   "remote":encodeURI(baseUrl + 'system/privilege_add?resourceId=' + node.props.id + "&resourceCode=" + node.props.code + "&resourceName=" + node.name),
				        	   "width":600,
				        	   "height":400,
				        	   "backdrop":"static"
				            },undefined);
					} else {
						alert('当前选中的【' + node.name + '】非叶子节点！');
					}
				} else {
					alert('请选择一个节点');
				}
			}
		}]
	},
	aoColumns: [
        {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
        {"mData" : "operation.operationCode","sTitle":"操作编码","bSortable" : true },
        {"mData" : "operation.operationName","sTitle":"操作名称" },
        {"mData" : "privilegeCode","sTitle":"权限编码" },
        {"mData" : "privilegeName","sTitle":"权限名称" },
		{"mData" : "id","sTitle":"操作","sWidth":"100px",
			"mRender":function(data, type, full){
				return '<a href="#" class="btn" title="编辑" onclick="onEditClick(\'' + data + '\')"><img src="'+springUrl+'/static/admin/skin/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;'
					+'<a href="#" class="btn" title="删除" onclick="onDelClick(\'' + data + '\')"><img src="'+springUrl+'/static/admin/skin/img/del.png" alt="修改"/></a>';
			}
        }
    ]
};

$(function(){
	treeObj = $.fn.zTree.init($("#privTree"), {
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			onClick: function (event, treeId, treeNode) {
				if(!treeNode.isParent){
					dataTablesOption.sAjaxSource = baseUrl + "system/getOperationDataJson?rscId=" + treeNode.id;
					if(privTable != undefined)	{
						privTable.fnClearTable();
						privTable.fnDestroy();
					} 
					privTable = $("#privTable").dataTable(dataTablesOption);
				}
			}
		}
	}, treeNode);
	
	
});


/**
 * 点击编辑键时
 * @param id
 */
function onEditClick(id){
	var rscCode, rscName;
	if (treeObj.getSelectedNodes().length > 0) {
		var node = treeObj.getSelectedNodes()[0];
		rscCode = node.props.code;
		rscName = node.name;
	}
	bootbox.dialog(
    	"",[{
    		"label": "取消",
    		"class": "btn-success",
    		"callback": function(el)	{
    			if(el==null){
    				bootbox.hideAll();
    			}else{
    				while(el.hasClass("bootbox")==false){
    					el=el.parent();
    				}
    				el.modal("hide");
    			}
    		}
    	},
    	{
    		"label": "保存",
    		"class": "btn-success",
    		"callback": function(){
    			if (!validate($("#updatePrivilege"))) {
    				return false;
    			} else {
    				save_update();
    			}
    		}
    	}],
    	{
           "header": "编辑",
    	   "remote": baseUrl + 'system/privilege_edit?id=' + id + "&resourceCode=" + rscCode + "&resourceName=" + encodeURI(rscName),
    	   "width":600,
    	   "height":500,
    	   "backdrop":"static"
        },undefined
    );
}

/**
 * 点击删除键时
 * @param id
 */
function onDelClick(id){
	bootbox.confirm("确定要删除该条记录吗?","提示","否","是",function(result){
		if(result){
			$.ajax({
		        type : "POST",
		        cache : false,
		        url : baseUrl + "/system/privilege_remove?id=" + id,
		        success:function (msg) {
		        	privTable.fnClearTable();
		        	privTable.fnDestroy();
		    		privTable = $("#privTable").dataTable(dataTablesOption);
		        },
		        error : function(msg){
		        	alert("出现错误,请联系系统管理员");
		        }
		    });
		}
	});
}
