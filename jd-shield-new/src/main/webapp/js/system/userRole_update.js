/**
 * Created with IntelliJ IDEA.
 * User: yujiahe
 * Date: 13-9-2
 * Time: 下午3:19
 * To change this template use File | Settings | File Templates.
 */
function saveUserRole(){
    var roleIds=$('[name="duallistbox_demo1[]"]').val();
    var userId=$('#userId').val();
//    console.log("roleIds"+roleIds+"userID"+userId);
    $.ajax({
        type: "GET",
        cache:"false",
        url: "/system/userRole_updateSave",
        dataType: "json",
        data:"roleIds="+roleIds+"&userId="+userId,
        success: function(json){
            if(json != null){
            	Dialog.hide();
            	refreshUserTable();
                Dialog.alert("系统提示","用户角色（用户组）分配成功");
            }
        }});
}
function cancel(){
	Dialog.hide();
}
$(document).ready(function() {
    $("#updateGroups").hide();
    var demo1 = $('[name="duallistbox_demo1[]"]').bootstrapDualListbox();
    document.getElementById("userRole_update").style.visibility="visible";
    $("select[name='duallistbox_demo1[]']").hide();
});