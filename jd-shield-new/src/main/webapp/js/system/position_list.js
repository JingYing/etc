var table;
$(document).ready(function () {
	var columns =[
	              {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	              { "mData": "id","sTitle":"id","sDefaultContent": "","bSortable":false,"bVisible":false},
	              { "mData": "positionCode","sTitle":"岗位编号","sDefaultContent": "","bSortable":false,"bVisible":true},
	              { "mData": "positionName","sTitle":"岗位名称","sDefaultContent": "","bSortable":false,"bVisible":true},
	              { "mData": "parentName","sTitle":"父岗位名称","sDefaultContent": "","bSortable":false,"bVisible":true},
	              {"mData": "id","sTitle":"操作","bSortable":false,"mRender":function(data, type, full){
	                    return "<a href='#' title='查看详情' class='btn'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'/ onclick='detailData(\""+data+"\");'></a>";
	                }}
	             ];
	var btns=[];
	
	table=$("#sysposition").dataTable({
		"bProcessing": false,
		"bServerSide":true,
		"sPaginationType": "full_numbers",
		"sAjaxSource":springUrl+"/system/findPositionPage",
		"sServerMethod": "POST",
		"bAutoWidth": false,
		"bStateSave": false,
		"sScrollY":"100%",
		"bScrollCollapse": true,
		"bPaginate":true,
		"oLanguage": {
			"sLengthMenu": "每页显示 _MENU_ 条记录",
			"sZeroRecords": "抱歉， 没有找到",
			"sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
			"sInfoEmpty": "没有数据",
			"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
			"oPaginate": {
				"sFirst": "首页",
				"sPrevious": "前页",
				"sNext": "后页",
				"sLast": "尾页"}
			},
		"sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bJQueryUI": true,
		"bFilter":false,
		"fnServerData":function ( sSource, aoData, fnCallback ) {
			var orgid=$("input#orgid").val();
			var positionName=$("input#positionName").val();
			aoData.push( { "name": "orgid", "value":orgid } ,{"name":"positionName","value":positionName} );
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(json) {
	                            fnCallback(json);
	                    },
                        error: function (data) {
                            Dialog.alert("失败","岗位信息查询失败");
                        }
		    });
		},
		"fnDrawCallback": function( oSettings ){
			var that = this;
			this.$('td:first-child', {"filter":"applied"}).each( function (i) {                    
				that.fnUpdate( i+1, this.parentNode, 0, false, false );                
			} );          
		},
		"aaSorting":[],//默认排序
		"aoColumns":columns,
		"oTableTools":{
			"aButtons":btns
		}
	});
	
	$('#searchPosition').click(function() {
		table.fnDraw();
	});
	
});

function detailData(id){
	    Dialog.openRemote('position_view','查看详情',springUrl+"/system/position_view?positionId="+id,600,400,[
	        {
	            "label": "关闭",
	            "class": "btn-success",
	            "callback": function() {}
	        }]);
}