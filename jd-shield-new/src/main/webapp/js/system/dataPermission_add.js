/**
 * 数据字典-数据项新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 初始化操作
 */

$(function() {
	bindValidate($('#dataPermissionForm'));
	$.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/dict/dictData_findDictDataList",
        data: {
        	dictTypeCode: "ENTITY_TYPE"//之后改成数据字典中对应的业务对象字典类型编码
        },
        dataType: "json",
        success: function(json){
            var ary = new Array();
            var entity = '<option value="">不限</option>';
            if(json != null){
                for(var i=0;i<json.length;i++){
                	entity +='<option value="'+json[i].dictDataCode+'">'+json[i].dictDataName+'</option>';  
                }
            }
            $("#entityType").html(entity);
        }
    });
	
	$.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/dict/dictData_findDictDataList",
        data: {
        	dictTypeCode: "AUTHORIZE_SUBJECT"//之后改成数据字典中对应的可授权主体对象字典类型编码
        },
        dataType: "json",
        success: function(json){
            var ary = new Array();
            var entity = '<option value="">不限</option>';
            if(json != null){
                for(var i=0;i<json.length;i++){
                	if(json[i].dictDataCode=='Organization'){
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectOrgnaztion();return false;">'+json[i].dictDataName+'</option>';
                	}else if(json[i].dictDataCode=='AuthExpression'){
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectSysExpression();return false;">'+json[i].dictDataName+'</option>';
                	}else if(json[i].dictDataCode=='Group'){
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectSysWorkGroup();return false;">'+json[i].dictDataName+'</option>';
                	}else{
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectSys'+json[i].dictDataCode+'();return false;">'+json[i].dictDataName+'</option>';
                	}
                		  
                }
            }
            $("#subjectType").html(entity);
        }
    });

});

