var zTree;
var setting = {
	async : {
		enable : true,
		url : springUrl+ "/system/resource_load",
		autoParam : [ "id" ]
	},
	callback : {
		beforeClick : beforeClick
	},
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		addHoverDom: addHoverDom,
		removeHoverDom: removeHoverDom,
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false
	}
};

function beforeClick(treeId, treeNode) {
	if(treeNode.id!=null){
		$("#box-right").show();
        $("#box-right").load(springUrl+"/system/resource_view?id="+treeNode.id);
        return true;
    }
}

function addHoverDom(treeId, treeNode) {
	// 由于根节点是基础数据，因此只有新增资源的功能，别的节点可以同时具有新增、修改和删除的功能
	if(treeNode.level > 0){
		var sObj = $("#" + treeNode.tId + "_span");
		if (treeNode.editNameFlag || $("#removeBtn_"+treeNode.id).length>0) return;
		// 新增的临时节点，只有删除功能
		if(treeNode.id.indexOf("tmp_") == 0){
			var domStr = "<span class='button remove' id='removeBtn_" + treeNode.id
			   + "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(domStr);
			var btn = $("#removeBtn_"+treeNode.id);
			if (btn) btn.bind("click", function(){
				var zTree = jQuery.fn.zTree.getZTreeObj("tree");
				zTree.removeNode(treeNode);
			});
		}
		// 其它节点，具有新增、编辑、删除功能
		else{
			// 临时节点保存后，成为正式节点，对应的浮动按钮需要先删除，然后再加上新的按钮菜单
			$("span[id^=addBtn_]").unbind().remove();
			$("span[id^=editBtn_]").unbind().remove();
			$("span[id^=removeBtn_]").unbind().remove();
			var domStr = "<span class='button add' id='addBtn_" + treeNode.id
					   + "' title='新增' onfocus='this.blur();'></span>"
					   + "<span class='button edit' id='editBtn_" + treeNode.id
					   + "' title='修改' onfocus='this.blur();'></span>"
					   + "<span class='button remove' id='removeBtn_" + treeNode.id
					   + "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(domStr);
			//新增节点
			var btn = $("#addBtn_"+treeNode.id);
			if (btn) btn.bind("click", function(){
				var zTree = jQuery.fn.zTree.getZTreeObj("tree");
				var timestamp = new Date().getTime();
				zTree.addNodes(treeNode, {isParent:true, id:("tmp_"+timestamp), pId:treeNode.id, name:"未命名", props:{parentId:treeNode.props.id}});
				// 选中新增的节点
				zTree.selectNode(zTree.getNodeByParam("id","tmp_"+timestamp,treeNode));
				$("#box-right").show();
				$("#box-right").load(springUrl+"/system/resource_add?id="+treeNode.id);
				return false;
			});
			//编辑节点
			btn = $("#editBtn_"+treeNode.id);
			if (btn) btn.bind("click", function(){
				$("#box-right").show();
				$("#box-right").load(springUrl+"/system/resource_update?id="+treeNode.props.id);
				var zTree = jQuery.fn.zTree.getZTreeObj("tree");
				zTree.selectNode(treeNode);
				return false;
			});
			//删除节点
			btn = $("#removeBtn_"+treeNode.id);
			if (btn) btn.bind("click", function(){
				Dialog.confirm("确认","确定将此资源删除?","确定","取消",function(result){
					if(result){
						var zTree = jQuery.fn.zTree.getZTreeObj("tree");
						var param = "id="+treeNode.props.id;
//						param = param + "&parentId=" + treeNode.props.parentid;
						jQuery.ajax({
							type : "POST",
							dataType : "json",
							url : springUrl+"/system/resource_delete",
							data : param,
							success : function(data) {
								if (!data.check)
									Dialog.alert("失败",data.msg);
								else{
									zTree.removeNode(treeNode);
									$("#box-right").hide();
									Dialog.alert("成功","删除成功!");//逻辑删除
								}
							},error : function(data) {
								 Dialog.alert("失败","程序异常结束!");
							}
						});
						return false;
					}
				});
			});
		}
	}
	// 由于根节点是新增数据，因此只有新增资源的功能
	else{
		var sObj = $("#" + treeNode.tId + "_span");
		if (treeNode.editNameFlag || $("#addBtn_"+treeNode.id).length>0) return;
		var addStr = "<span class='button add' id='addBtn_" + treeNode.id
			+ "' title='新增' onfocus='this.blur();'></span>";
		sObj.after(addStr);
		var btn = $("#addBtn_"+treeNode.id);
		if (btn) btn.bind("click", function(){
			var zTree = jQuery.fn.zTree.getZTreeObj("tree");
			var timestamp = new Date().getTime();
			zTree.addNodes(treeNode, {isParent:true, id:("tmp_"+timestamp), pId:treeNode.id, name:"未命名", props:{}});
			// 选中新增的节点
			zTree.selectNode(zTree.getNodeByParam("id","tmp_"+timestamp,treeNode));
			$("#box-right").show();
			$("#box-right").load(springUrl+"/system/resource_add?id="+treeNode.pId);
			return false;
		});
	}
};

/**
 * 刷新父节点
 * @param pId 父节点ID
 */
function refreshNode(pId){
    var zTree = jQuery.fn.zTree.getZTreeObj("tree");
    if(pId == null){
        var treeNode = zTree.getNodeByTId("tree_1");
        zTree.reAsyncChildNodes(treeNode, "refresh");
    }else{
        var nodes = zTree.transformToArray(zTree.getNodes());
        var tId = "";
        for(var i=0; i<nodes.length; i++){
            if(nodes[i].id == pId){
                tId = nodes[i].tId;
                break;
            }
        }
        if(tId != ""){
            var treeNode = zTree.getNodeByTId(tId);
            zTree.reAsyncChildNodes(treeNode, "refresh");
        }
    }
}

function removeHoverDom(treeId, treeNode) {
	if(treeNode.level > 0){
		$("#addBtn_"+treeNode.id).unbind().remove();
		$("#editBtn_"+treeNode.id).unbind().remove();
		$("#removeBtn_"+treeNode.id).unbind().remove();
	}
	else{
		$("#addBtn_"+treeNode.id).unbind().remove();
	}
};

/**
 * 初始化操作
 */
$(function() {
    jQuery.fn.zTree.init($("#tree"), setting, zNodes);
});