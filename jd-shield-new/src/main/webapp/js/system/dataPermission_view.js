
/**
 * 初始化操作
 */

$(function() {
    $.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/system/dpsubject_list",
        data: {
            dataPermissionId: dataId
        },
        dataType: "json",
        success: function(json){
            tableInfo(json);
        }
    });


});



///修改详细显示主体对象的内容
function tableInfo(json){
    var userArray = new Array();
    var positionArray = new Array();
    var roleArray = new Array();
    var workGroupArray = new Array();
    var authArray = new Array();
    var orgArray = new Array();
    if(json != null){
        var labelId ='';
        var id;
        var subjectCode;
        var subjectRualName;
        var subjectParent;
        var authorizeType;

        for(var i=0;i<json.length;i++){
            labelId = json[i].subjectType;
            id=json[i].subjectId;
            subjectCode =json[i].subjectCode ;
            subjectRualName=json[i].subjectRualName;
            subjectParent=json[i].subjectParent;
            authorizeType=json[i].authorizeType;
            if(labelId=='User'){
                userArray.push({id:id,userName:subjectCode,realName:subjectRualName,orgName:subjectParent,authorizeType:authorizeType});
            }else if(labelId=='Position'){
                positionArray.push({id:id,positionCode:subjectCode,positionName:subjectRualName,parentName:subjectParent,authorizeType:authorizeType});
            }else if(labelId=='Role'){
                roleArray.push({id:id,roleCode:subjectCode,roleName:subjectRualName,authorizeType:authorizeType});
            }else if(labelId=='Group'){
                workGroupArray.push({id:id,roleCode:subjectCode,roleName:subjectRualName,authorizeType:authorizeType});
            }else if(labelId=='AuthExpression'){
                authArray.push({id:id,authName:subjectCode,mode:subjectRualName,authorizeType:authorizeType});
            }else if(labelId=='Organization'){
                orgArray.push({id:id,orgCode:subjectCode,orgName:subjectRualName,fullName:subjectParent,authorizeType:authorizeType});
            }
        }

        if(userArray.length>0){
           var html =' <table id="sysusertab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
            html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="4"  style="text-align: center"> 用户</td></tr>';
            html+=' <tr ><td >ERP</td><td >姓名</td><td >部门</td><td>授权状态</td></tr>';
            for(var i=0;i<userArray.length;i++) {
                var type = userArray[i].authorizeType;
                html+=' <tr><td >'+userArray[i].userName+'</td><td >'+userArray[i].realName+'</td><td >'+userArray[i].orgName+'</td><td >'+(type==1?'允许':'禁止')+'</td></tr>';
            }
            html+='</table>';
            $("#info").append(html);
        }
        if(positionArray.length>0){
           var  html =' <table id="syspositiontab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
            html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="4"  style="text-align: center"> 岗位</td></tr>';
            html+=' <tr ><td >岗位编号</td><td >岗位名称</td><td >父岗位名称</td><td>授权状态</td></tr>';
            for(var i=0;i<positionArray.length;i++) {
                var type = positionArray[i].authorizeType;
                html+=' <tr><td >'+positionArray[i].positionCode+'</td><td >'+positionArray[i].positionName+'</td><td >'+positionArray[i].parentName+'</td><td >'+(type==1?'允许':'禁止')+'</td></tr>';
            }
            html+='</table>';
            $("#info").append(html);
        }
        if(roleArray.length>0){
           var html =' <table id="sysroletab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
            html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="3"  style="text-align: center"> 角色</td></tr>';
            html+=' <tr ><td >角色码</td><td >角色名称</td><td>授权状态</td></tr>';
            for(var i=0;i<roleArray.length;i++) {
                var type = roleArray[i].authorizeType;
                html+=' <tr><td >'+roleArray[i].roleCode+'</td><td >'+roleArray[i].roleName+'</td><td >'+(type==1?'允许':'禁止')+'</td></tr>';
            }
            html+='</table>';
            $("#info").append(html);
        }
        if(workGroupArray.length>0){
           var html =' <table id="sysworkgrouptab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
            html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="3"  style="text-align: center"> 工作组</td></tr>';
            html+=' <tr ><td >工作组码</td><td >工作组名称</td><td>授权状态</td></tr>';
            for(var i=0;i<workGroupArray.length;i++) {
                var type = workGroupArray[i].authorizeType;
                html+=' <tr><td >'+workGroupArray[i].roleCode+'</td><td >'+workGroupArray[i].roleName+'</td><td >'+(type==1?'允许':'禁止')+'</td></tr>';
            }
            html+='</table>';
            $("#info").append(html);
        }
        if(authArray.length>0){
           var html =' <table id="sysauthexpressiontab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
            html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="3"  style="text-align: center"> 权限表达式</td></tr>';
            html+=' <tr ><td >表达式名称</td><td >表达式</td><td>授权状态</td></tr>';
            for(var i=0;i<authArray.length;i++) {
                var type = authArray[i].authorizeType;
                html+=' <tr><td >'+authArray[i].authName+'</td><td >'+authArray[i].mode+'</td><td >'+(type==1?'允许':'禁止')+'</td></tr>';
            }
            html+='</table>';
            $("#info").append(html);
        }
        if(orgArray.length>0){
           var html =' <table id="sysorganizationtab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
            html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="4"  style="text-align: center"> 组织机构</td></tr>';
            html+=' <tr ><td >组织机构码</td><td >组织机构名称</td><td>组织机构全名</td><td>授权状态</td></tr>';
            for(var i=0;i<orgArray.length;i++) {
                var type = orgArray[i].authorizeType;
                html+=' <tr><td >'+orgArray[i].orgCode+'</td><td >'+orgArray[i].orgName+'</td><td >'+orgArray[i].fullName+'</td><td >'+(type==1?'允许':'禁止')+'</td></tr>';
            }
            html+='</table>';
            $("#info").append(html);
        }

    }
}
