var setting = {
	async : {
		enable : true,
		url : getUrl
	},
	check : {
		enable : true,
		nocheckInherit : true
	},
	data : {
		simpleData : {
			enable : true
		}
	},
	view : {
		expandSpeed : "",
		addDiyDom : addDiyDom
	},
	callback: {
		onCheck : onCheck,
		onExpand : expandAll
	}
};

var zNodes = treeNode;

function getUrl(treeId, treeNode) {
	var param = "nodeId=" + treeNode.id + '&level=' + treeNode.level + "&roleId=" + roleId;
	return springUrl + "/system/rolePrivilege_getTreeNodesJson?" + param;
}

var IDMark_A = "_a";
function addDiyDom(treeId, treeNode) {
	if(typeof(treeNode.props) == 'undefined' || typeof(treeNode.props.operations) == 'undefined'){
		return;
	}
	if(treeNode.isParent){
		return;
	}
	var aObj = $("#" + treeNode.tId + IDMark_A);
	var operations = treeNode.props.operations;
	for(var i = 0 ; i < operations.length ; i++){
		var operation = operations[i];
		var className = "button chk checkbox_false_full";
		if(getChecked(treeNode,operation.id) && treeNode.checked){
			className = "button chk checkbox_true_full";
		}
		var editStr = "  <span class=\"" + className + "\" operationCode=\"" + operation.operationCode + "\" operationName=\"" + operation.operationName + "\" operationId=\"" + operation.id + "\" resourceId=\"" + treeNode.props.id + "\" url=\"" + treeNode.props.url + "\" onclick=\"checkbox(this)\"></span>" + operation.operationName;
		aObj.after(editStr);
	}
}
function getOperationCheckBox(treeNode, operationId) {
	var operations = treeNode.props.operations;
	for ( var i = 0; i < operations.length; i++) {
		var operation = operations[i];
		if (operation.id == operationId) {
			return operation;
		}
	}
	return "";
}
function getChecked(treeNode,operationId){
	var privileges = treeNode.props.privileges;
	for(var i = 0 ; i < privileges.length ; i++){
		var privilege = privileges[i];
		if(privilege.operation.id == operationId){
			return true;
		}
	}
	return false;
}
function onCheck(e, treeId, treeNode) {
	if(treeNode.checked){//选中
		if(treeNode.isParent){
			var zTree = jQuery.fn.zTree.getZTreeObj("privilegeTree");
			var childNodes = zTree.transformToArray(treeNode);  
	        for(i = 0; i < childNodes.length; i++) { 
	        	if(!childNodes[i].isParent){
		             eachTreeNodeSave(childNodes[i]);
	        	}
	        }  
		} else {
			eachTreeNodeSave(treeNode);
		}
	} else {
		var aObj_spans = $("#" + treeNode.tId + IDMark_A).parent().find('span');
		for(var i = 0 ; i < aObj_spans.length ; i++){
			var aObj_span = aObj_spans[i];
			if('button chk checkbox_true_full' == aObj_span.className){
				aObj_span.className = 'button chk checkbox_false_full';
				var operationid = jQuery(aObj_span).attr('operationid');
				if(typeof(treeNode.props) != 'undefined' && typeof(treeNode.props.id) != 'undefined'){
					var data = {
				        	resourceId : treeNode.props.id,
				        	operationId : operationid
				      };
				}
			}
		}
	}
}
function eachTreeNodeSave(treeNode){
	var aObj_spans = $("#" + treeNode.tId + IDMark_A).parent().find('span');
	for(var i = 0 ; i < aObj_spans.length ; i++){
		var aObj_span = aObj_spans[i];
		if('button chk checkbox_false_full' == aObj_span.className){
			var operationid = jQuery(aObj_span).attr('operationid');
			var operationCode = jQuery(aObj_span).attr('operationCode');
			var operationName = jQuery(aObj_span).attr('operationName');
			aObj_span.className = 'button chk checkbox_true_full';
			if(typeof(treeNode.props) != 'undefined' && typeof(treeNode.props.id) != 'undefined'){
				var data = {
			        	resourceId : treeNode.props.id,
			        	operationId : operationid,
			        	url : treeNode.props.url,
			        	name : treeNode.name + '_' + operationName,
			        	code : treeNode.props.code + '_' + operationCode
			     };
			}
		}
	}
}
function checkbox(obj){
	var resourceId = jQuery(obj).attr('resourceId');
	var url = jQuery(obj).attr('url');
	var operationId = jQuery(obj).attr('operationId');
	if(obj.className == 'button chk checkbox_false_full'){
		obj.className = 'button chk checkbox_true_full';
		var data = {
	        	resourceId : resourceId,
	        	operationId : operationId,
	        	url : url,
	        	name : '',
	        	code : ''
	     };
	} else {
		obj.className = 'button chk checkbox_false_full';
		var data = {
	        	resourceId : resourceId,
	        	operationId : operationId
	      };
	}
}
/**
 * 授权
 */
function saveAc(){
	var treeObj = $.fn.zTree.getZTreeObj("privilegeTree");
	var nodes = treeObj.getCheckedNodes(true);
	var treeNodeResourcePrivilegeArray = new Array();
	for ( var i = 0; i < nodes.length; i++) {
		var node = nodes[i];
		if(!node.isParent){
			var aObj_spans = $("#" + node.tId + IDMark_A).parent().find('span');
			var checkedResourceOperationArray = new Array();//树节点对应的选中操作
			for(var j = 0 ; j < aObj_spans.length ; j++){
				var aObj_span = aObj_spans[j];
				if('button chk checkbox_true_full' == aObj_span.className){
					var operationId = jQuery(aObj_span).attr('operationid');
					if(typeof(operationId) != 'undefined'){
						var checkedResourceOperation = {
								operationId : jQuery(aObj_span).attr('operationid'),
								operationCode : jQuery(aObj_span).attr('operationCode'),
								operationName : jQuery(aObj_span).attr('operationName')
							};
						checkedResourceOperationArray.push(checkedResourceOperation);
					}
				}
			}
			var treeNodeResourcePrivilege = {
					resourceId : node.props.id,
					name : node.name,
					code : node.props.code,
					checkedResourceOperationArray : checkedResourceOperationArray
			};
			treeNodeResourcePrivilegeArray.push(treeNodeResourcePrivilege);
		}
	}
	var data = {
		roleId : roleId,
		operationsJson : JSON.stringify(treeNodeResourcePrivilegeArray)
	};
	saveAjax(data);
}
function saveAjax(data) {
	jQuery.ajax({
		type : "POST",
		cache : false,
		url : springUrl + "/system/rolePrivilege_save",
		data : data,
		success : function(msg) {
			Dialog.hide();
		},
		error : function(msg) {

		}
	});
}
function deleteAjax(data) {
	jQuery.ajax({
		type : "POST",
		cache : false,
		url : springUrl + "/rolePrivilege/remove",
		data : data,
		success : function(msg) {

		},
		error : function(msg) {

		}
	});
}
function cancel() {
	Dialog.hide();
}
$(document).ready(function() {
	$.fn.zTree.init($("#privilegeTree"), setting, zNodes);
	setTimeout(function(){
		//expandAll();
	},5000);
});
function expandAll() {
	var zTree = $.fn.zTree.getZTreeObj("privilegeTree");
	expandNodes(zTree.getNodes());
}
function expandNodes(nodes) {
	if (!nodes) return;
	var zTree = $.fn.zTree.getZTreeObj("privilegeTree");
	for (var i = 0, l = nodes.length; i < l; i++) {
		zTree.expandNode(nodes[i], true, false, false);
		if (nodes[i].isParent) {
			expandNodes(nodes[i].children);
		}
	}
}