package com.jd.common.constants;
/**
 * 供应商常量类
 * @author wangchanghui
 *
 */
public class SupplierConstant {
	
	
	/**
	 * 币种类型
	 */
	public static final  String CURRENCY = "currency";
	/**
	 * 供应商覆盖范围
	 */
	public static final  String SUPPLIER_COVERAGE = "supplier_coverage";
	/**
	 * 供应商归属区域
	 */
	public static final  String SUPPLIER_OWNER = "supplier_owner";
	/**
	 * 供应商所属地
	 */
	public static final  String SUPPLIER_HOME = "supplier_home";
	/**
	 * 资料完整度
	 */
	public static final  String SUPPLIER_DATA_INTEGRITY = "data_integrity";
	/**
	 * 供应商组织
	 */
	public static final  String SUPPLIER_ORG = "supplier_org";
	/**
	 * 供应商类型
	 */
	public static final  String SUPPLIER_TYPE = "supplier_type";
	/**
	 * 企业性质
	 */
	public static final  String ENTERPRISE = "enterprise";
	/**
	 * 默认申报类型
	 */
	public static final  String TAX_TYPE = "tax";
	/**
	 * 认证状况
	 */
	public static final  String AUTHENTIC_TYPE = "authentic";
	/**
	 * 企业级别
	 */
	public static final  String ENTERPRISE_LEVEL = "enterprise_level";
	/**
	 * 导入文件的列数
	 */
	public static final  int IMPORT_TITLE_LENG = 61;
	
	/**
	 * 导入文件数据为空
	 */
	public static final  String IMPORT_NULL ="导入文件数据为空！";
	
	/**
	 * 导入文件非标准模板！
	 */
	public static final  String IMPORT_FILE_FALSE = "导入文件非标准模板！";
	
	/**
	 * 导入成功
	 */
	public static final  String IMPORT_RESULT = "导入成功！";
	
	/**
	 * 导入失败
	 */
	public static final  String IMPORT_RESULT_FALSE = "导入失败！";
	
	/**
	 * 失败原因,第
	 */
	public static final  String IMPORT_RESULT_FSLAE1 = "记录第 ";
	/**
	 * 失败原因,行
	 */
	public static final  String IMPORT_RESULT_FSLAE2 = " 行，";
	/**
	 * 失败原因,列
	 */
	public static final  String IMPORT_RESULT_FSLAE3 = " 列，";
	/**
	 * 失败原因,需要填写数字
	 */
	public static final  String IMPORT_RESULT_FSLAE4 = "请填写数字！";
	/**
	 * 失败原因,请输入正确邮箱格式
	 */
	public static final  String IMPORT_RESULT_FSLAE5 = "请输入正确邮箱格式！";
	/**
	 * 失败原因,请输入正确手机
	 */
	public static final  String IMPORT_RESULT_FSLAE6 = "请输入正确手机！";
	
	/**
	 * 失败原因,不能为空
	 */
	public static final  String IMPORT_RESULT_FSLAE7 = "不能为空！";
	/**
	 * 失败原因,请填写正确金额
	 */
	public static final  String IMPORT_RESULT_FSLAE8 = "请填写正确金额！";
	
	/**
	 * 失败原因,供应商类型匹配不上请核对
	 */
	public static final  String IMPORT_RESULT_FSLAE9 = "供应商类型匹配不上请核对！";
	
	/**
	 * 失败原因,内容包含逗号请核对！请将逗号(,)替换成($&$),货删除！
	 */
	public static final  String IMPORT_RESULT_FSLAE10 = "内容包含逗号请核对！请将逗号(,)替换成全角逗号(，),或删除！";
	
	/**
	 * 失败原因,内容匹配字典不正确，请核对！
	 */
	public static final  String IMPORT_RESULT_FSLAE11 = "内容匹配字典不正确，请核对！";
	
	/**
	 * 失败原因,内容长度不能超过100个字符！
	 */
	public static final  String IMPORT_RESULT_FSLAE12 = "内容长度不能超过100个字符！";
	
	/**
	 * 失败原因,内容长度不能超过1000个字符！
	 */
	public static final  String IMPORT_RESULT_FSLAE13 = "内容长度不能超过1000个字符！";
	
	/**
	 * 失败原因,内容长度不能超过50个字符！
	 */
	public static final  String IMPORT_RESULT_FSLAE14 = "内容长度不能超过50个字符！";
	
	/**
	 * 失败原因,内容长度不能超过20个字符！
	 */
	public static final  String IMPORT_RESULT_FSLAE15 = "内容长度不能超过20个字符！";
	
	/**
	 * 失败原因,内容长度不能超过500个字符！
	 */
	public static final  String IMPORT_RESULT_FSLAE16 = "内容长度不能超过500个字符！";
	
	
	

	
	
	public static final String SUPPLIER_SPECIAL = "（特殊）";
	
}
