package com.jd.common.constants;
/**
 * 字符常量类
 * @author wangchanghui
 *
 */
public class CharacterConstant {
	/**
	 * 自定义分隔符
	 */
	public static final String DELIMITER = "|+|";
	
	/**
	 * 前台展示分隔符
	 */
	public static final String DELIMITER4VIEW = "\\|\\+\\|";
}
