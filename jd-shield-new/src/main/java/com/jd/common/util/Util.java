package com.jd.common.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Util {
	
	/**
	 * 将String数组转换为Int数组
	 * @param s
	 * @return
	 */
	public static int[] toIntArray(String[] s) {
		int[] arr = new int[s.length];
		for (int i = 0; i < s.length; i++) {
			arr[i] = Integer.parseInt(s[i]);
		}
		return arr;
	}
	
	public static long[] toLongArray(String[] s) {
		long[] arr = new long[s.length];
		for (int i = 0; i < s.length; i++) {
			arr[i] = Long.parseLong(s[i]);
		}
		return arr;
	}
	
	/**
	 * 数组转字符串
	 * @param arr
	 * @param separator 分隔符
	 * @return
	 */
	public static String join(Object[] arr, String separator)	{
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<arr.length; i++)	{
			sb.append(arr[i].toString());
			if(i < arr.length-1)	{
				sb.append(separator);
			}
		}
		return sb.toString();
	}
	
	/**
	 * List转字符串
	 * @param list
	 * @param separator 分隔符
	 * @return
	 */
	public static <T> String join(List<T> list, String separator)	{
		return join(list.toArray(), separator);
	}
	
	/**
	 * 获取后缀名,含句号
	 * @param file
	 * @return
	 */
	public static String getSuffix(File file)	{
		return getSuffix(file.getAbsolutePath());
	}
	
	/**
	 * 获取后缀名,含句号
	 * @param url
	 * @return
	 */
	public static String getSuffix(String url)	{
		if(url.contains("."))
			return url.substring(url.lastIndexOf("."), url.length());
		else
			return "";
	}
	
	/**
	 * 关闭IO流
	 * @param closable
	 */
	public static void closeStream(Closeable...closable)	{
		for(Closeable c : closable)	{
			if(c != null)
				try {
					c.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * file转数组
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public static byte[] toBytes(File file) throws IOException	{
		BufferedInputStream bis = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(file));
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int len = -1;
			while((len = bis.read(buf)) != -1)	{
				bos.write(buf, 0, len);
			}
			bos.flush();
			return bos.toByteArray();
		} finally	{
			closeStream(bis);
		}
	}
	
	public static String formatDate(Date date)	{
		if(date == null)	return "";
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}
	
	
	/**
	 * 生成一串随机数组成的字符串
	 * @param num 随机数的位数
	 * @return
	 */
	public static String randStr(int num)	{
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i=0; i<num; i++)	{
			sb.append("" + rand.nextInt(10));
		}
		return sb.toString();
	}
	
	/** 
	 * MD5加密
	 * @param str 要加密的字符串 
	 * @return    加密后的字符串 
	 */  
	public static String toMD5(String str){  
	    try {  
	        MessageDigest md = MessageDigest.getInstance("MD5");  
	        md.update(str.getBytes());  
	        byte[] byteDigest = md.digest();  
	        int i;  
	        StringBuffer buf = new StringBuffer("");  
	        for (int offset = 0; offset < byteDigest.length; offset++) {  
	            i = byteDigest[offset];  
	            if (i < 0)  i += 256;  
	            if (i < 16) buf.append("0");  
	            buf.append(Integer.toHexString(i));  
	        }  
//	        return buf.toString().substring(8, 24);		//16位加密     
	        return buf.toString();						//32位加密    
	    } catch (NoSuchAlgorithmException e) {  
	        throw new RuntimeException(e); 
	    }
	}
	
	/**
	 * 生成文件的md5码
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String toMD5(File file) throws IOException {  
    	InputStream fis = null;  
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			fis = new FileInputStream(file);  
			byte[] buf = new byte[1024];  
			int len = 0;  
			while ((len = fis.read(buf)) > 0) {  
			    md.update(buf, 0, len);  
			}  
			byte[] bytes = md.digest();
			
			char[] hex = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};  
			StringBuffer sb = new StringBuffer(2 * bytes.length);  
	        for (int i = 0; i < bytes.length; i++) {  
	            char c0 = hex[(bytes[i] & 0xf0) >> 4];// 取字节中高 4 位的数字转换  
	            // 为逻辑右移，将符号位一起右移,此处未发现两种符号有何不同  
	            char c1 = hex[bytes[i] & 0xf];// 取字节中低 4 位的数字转换  
	            sb.append(c0).append(c1);  
	        }  
	        return sb.toString();  
        } catch (NoSuchAlgorithmException e1) {
        	throw new RuntimeException(e1);
		} finally	{
			try {
				if(fis != null)	fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }  
}