package com.jd.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jd.common.constants.SupplierConstant;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.supplier.model.Contacts;
import com.jd.supplier.model.Enterprise;
import com.jd.supplier.model.Equip;
import com.jd.supplier.model.Finance;
import com.jd.supplier.model.Material;
import com.jd.supplier.model.Other;
import com.jd.supplier.model.Quality;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.SupplierDto;

/**
 * 导入供应商数据
 * @author yfwangyunlong
 * 
 *
 */
public class ReadExcel {
	private Logger logger = LoggerFactory.getLogger(getClass());

	public synchronized String importExcel(InputStream is,String fileName,
			List<SupplierDto> supplierDtoList, List<String> resultString,
			DictDataService dictDataService) {
		String result = SupplierConstant.IMPORT_RESULT;

		String[] titleArray = null;
		List<String> copyArray = null;
		Set<String> set = null;

		int rowCount = 0;
		int rownumberTemp = 0;

		SupplierDto supplierDto = new SupplierDto();
		Supplier supplier = new Supplier();
		Contacts contacts = new Contacts();
		Enterprise enterprise = new Enterprise();
		Finance finace = new Finance();
		Quality quality = new Quality();
		Material material = new Material();
		List<Equip> equip = new ArrayList<Equip>();
		Equip one= new Equip();
		Equip two = new Equip();
		Other other = new Other();
		try {
			set = new HashSet<String>();
			// 表头字段数组
			titleArray = new String[SupplierConstant.IMPORT_TITLE_LENG];
			// 验证供应商名称是否重复
			copyArray = new ArrayList<String>();
			List<List<Object>> list =common(is, fileName);
			 for (int i = 0; i < list.size(); i++) {
					 String tempvalue = "&";
						String[] str = String.valueOf(list.get(i)).substring(1,String.valueOf(list.get(i)).length()-1).split(",");
						if (str != null && str.length > 0) {
							rownumberTemp = str.length;

							if (rowCount > 0) {
								if (rownumberTemp != SupplierConstant.IMPORT_TITLE_LENG) {
									result = SupplierConstant.IMPORT_RESULT_FALSE
											+ SupplierConstant.IMPORT_RESULT_FSLAE1
											+ (rowCount + 1)
											+ SupplierConstant.IMPORT_RESULT_FSLAE2
											+ SupplierConstant.IMPORT_RESULT_FSLAE10;
									resultString.add(result);
									continue;
								}
							} else {
								if (rownumberTemp != SupplierConstant.IMPORT_TITLE_LENG) {
									result = SupplierConstant.IMPORT_FILE_FALSE;
									resultString.add(result);
									titleArray = null;
									break;
								}
							}
							// 2、 先验证表头 和供应商名称是否重复、然后保存对象
							for (int k = 0; k < str.length; k++) {
								if (str[k] != null && !"".equals(str[k].trim())) {
									tempvalue = str[k];
								} else {
									tempvalue = "&";
								}
								if (rowCount == 0) {
									titleArray[k] = tempvalue.trim();
								} else {
									if (k == 10) {
										if (copyArray.contains(tempvalue.trim())) {
											result = "供应商全称 [  " + (tempvalue.trim())
													+ " ],文件中有重复数据,请校验";
											set.add(result);
										}
										copyArray.add(tempvalue.trim());
									}
									result = custSupplierDto(rowCount + 1, k,
											supplierDto, supplier, contacts,
											enterprise, finace, quality, material,
											one, two,other, tempvalue.trim(), titleArray,
											dictDataService);
									if (!result.equals(SupplierConstant.IMPORT_RESULT)) {
										resultString.add(result);
									}
								}
								
							}
							if (rowCount > 0) {
								supplierDto.setSupplier(supplier);
								supplierDto.setContacts(contacts);
								supplierDto.setEnterprise(enterprise);
								supplierDto.setFinace(finace);
								supplierDto.setQuality(quality);
								supplierDto.setMaterial(material);
								equip.add(one);
								equip.add(two);
								supplierDto.setEquip(equip);
								supplierDto.setOther(other);
								supplierDtoList.add(supplierDto);
							}
						}
						rowCount++;
			 }
			if (rowCount == 1) {
				result = SupplierConstant.IMPORT_NULL;
				resultString.add(result);
			}
			resultString.addAll(set);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {

				is.close();
			} catch (Exception e1) {
				logger.error(e1.getMessage());
			}
			result = SupplierConstant.IMPORT_RESULT_FALSE + e.getMessage();
		} finally {
			if (result.equals(SupplierConstant.IMPORT_RESULT)) {
				result += "共导入 | " + (rowCount - 1) + " |行";
			}

			try {
				is.close();
			} catch (Exception e1) {
				logger.error(e1.getMessage());
			}
		}
		return result;
	}

	public String custSupplierDto(int row, int column, SupplierDto supplierDto,
			Supplier supplier, Contacts contacts, Enterprise enterprise,
			Finance finance, Quality quality, Material material, Equip one,Equip two,
			Other other, String value, String[] titleArray,
			DictDataService dictDataService) {
		String result = SupplierConstant.IMPORT_RESULT;
		switch (column) {
		case 0:
			break;
		case 1:
			// 企业名称
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[1] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					supplier.setName(value);
				}

			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[1] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 2:
			// 企业电话
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesPhone(value)) {
					supplier.setMobile(value);
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[2] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE6;
				}
			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[2] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 3:
			// 企业邮箱
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesEMAL(value)) {
					supplier.setEmail(value);
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[3] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE5;
				}
			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[3] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 4:
			// 联系人
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[4] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					contacts.setContactor(value);
				}

			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[4] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 5:
			// 联系人职位
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[5] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					contacts.setPosition(value);
				}

			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[5] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 6:
			// 联系人办公座机
			if (value != null && !value.equals("&")) {
				if (value.length() > 15) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[6] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					contacts.setTelphone(value);
				}

			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[6] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 7:
			// 联系人手机
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesPhone(value)) {
					contacts.setMobile(value);
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[7] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE6;
				}
			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[7] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 8:
			// 联系人传真
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[8] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					contacts.setFax(value);
				}

			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[8] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 9:
			// 联系人邮箱
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesEMAL(value)) {
					contacts.setEmail(value);
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[9] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE5;
				}
			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[9] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 10:
			// 供应商全称
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[10] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setName(value);
				}

			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[10] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 11:
			// 供应商类型
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[11] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					String code = this.getDictCode(value, dictDataService,
							"supplier_type");
					enterprise.setSupplierType(code);
				}

			} else {
				result = SupplierConstant.IMPORT_RESULT_FALSE
						+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| " + row
						+ " |" + SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
						+ titleArray[11] + " |"
						+ SupplierConstant.IMPORT_RESULT_FSLAE3
						+ SupplierConstant.IMPORT_RESULT_FSLAE7;
			}
			break;
		case 12:
			// 办公地址
			if (value != null && !value.equals("&")) {
				if (value.length() > 100) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[12] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setOfficeAddress(value);
				}

			}
			break;
		case 13:
			// 注册地址
			if (value != null && !value.equals("&")) {
				if (value.length() > 100) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[13] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setRegisteredAddress(value);
				}

			}
			break;
		case 14:
			// 工厂地址
			if (value != null && !value.equals("&")) {
				if (value.length() > 100) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[14] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setFactoryAddress(value);
				}

			}
			break;
		case 15:
			// 企业性质
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[15] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					String code = this.getDictCode(value, dictDataService,
							"enterprise");
					enterprise.setEnterpriseType(code);
				}

			}
			break;
		case 16:
			// 企业法人
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[16] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setEnterpriseLaw(value);
				}

			}
			break;
		case 17:
			// 成立日期
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[17] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setEnterpriseDate(MatchesString
							.stringToDate(value));
				}

			}
			break;
		case 18:
			// 员工总人数
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					enterprise.setEmployeeCount(Integer.parseInt(value));
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[18] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 19:
			// 管理总人数
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					enterprise.setManagerCount(Integer.parseInt(value));
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[19] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 20:
			// 技术总人数
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					enterprise.setTecCount(Integer.parseInt(value));
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[20] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 21:
			// 厂地面积
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[21] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setMeasure(value);
				}

			}
			break;
		case 22:
			// 企业网站
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[22] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setWebsite(value);
				}

			}
			break;
		case 23:
			// 注册资金
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					enterprise.setFunds(Integer.parseInt(value));
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[18] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 24:
			// 营业执照号码
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[24] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setLicense(value);
				}

			}
			break;
		case 25:
			// 执照有效期
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[25] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setPeriodDate(MatchesString.stringToDate(value));
				}

			}
			break;
		case 26:
			// 经营许可证号
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[26] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setManagementNum(value);
				}

			}
			break;
		case 27:
			// 纳税人登记号
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[27] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setTaxNum(value);
				}

			}
			break;
		case 28:
			// 组织机构代码号
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[28] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setOrgCode(value);
				}

			}
			break;
		case 29:
			// 行业资质
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[28] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setQualification(value);
				}

			}
			break;
		case 30:
			// 年产量
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					enterprise.setPerAcount(Integer.parseInt(value));

				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[30] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 31:
			// 业务覆盖范围
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[31] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					String code = this.getDictCode(value, dictDataService,
							"area");
					enterprise.setQualification(code);
				}

			}
			break;
		case 32:
			// 分支机构
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[32] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					if ("有".equals(value)) {
						enterprise.setBranch("1");
					}
					enterprise.setBranch("0");
				}

			}
			break;
		case 33:
			// 经营内容
			if (value != null && !value.equals("&")) {
				if (value.length() > 200) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[33] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setContent(value);
				}

			}
			break;
		case 34:
			// 企业及产品荣誉情况
			if (value != null && !value.equals("&")) {
				if (value.length() > 200) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[34] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setHonor(value);
				}

			}
			break;
		case 35:
			// 同行业情况
			if (value != null && !value.equals("&")) {
				if (value.length() > 200) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[35] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					enterprise.setOther(value);
				}

			}
			break;
		case 36:
			// 开户银行许可证
			if (value != null && !value.equals("&")) {
				if (value.length() > 200) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[36] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					finance.setBankLicense(value);
				}

			}
			break;
		case 37:
			// 开户银行联行号
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[37] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					finance.setBankLinked(value);
				}

			}
			break;
		case 38:
			// 开户银行
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[38] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					finance.setBankName(value);
				}

			}
			break;
		case 39:
			// 银行账号
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[39] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					finance.setBankAccount(value);
				}

			}
			break;
		case 40:
			// 税率
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[40] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					finance.setBankRate(value);
				}

			}
			break;
		case 41:
			// 默认申报纳税类型
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[41] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					String code = this.getDictCode(value, dictDataService,
							"tax");
					finance.setTaxType(code);
				}

			}
			break;
		case 42:
			// 年销售总额
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					finance.setSalesCount(Integer.parseInt(value));

				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[30] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 43:
			// 企业所得税
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					finance.setEnterpriseTax(Integer.parseInt(value));

				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[30] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 44:
			// 认证状况
			if (value != null && !value.equals("&")) {
				if (value.length() > 20) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[44] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					String code = this.getDictCode(value, dictDataService,
							"authentic");
					quality.setAuthentication(code);
				}

			}
			break;
		case 45:
			// 主要生产设备名称
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					one.setName(value);
					one.setType("1");
				}

			}
			break;
		case 46:
			// 主要生产设备品牌
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					one.setBrand(value);
					one.setType("1");
				}

			}
			break;
		case 47:
			// 主要生产设备数量
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					one.setEqNumber(Integer.parseInt(value));
					one.setType("1");
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 48:
			// 主要生产设备产能
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					one.setEqProduction(value);
					one.setType("1");
				}

			}
			break;
		case 49:
			// 主要生产设备使用年限
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					one.setEqLimit(Integer.parseInt(value));
					one.setType("1");
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 50:
			// 主要检测设备名称
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					two.setName(value);
					two.setType("2");
				}

			}
			break;
		case 51:
			// 主要检测设备品牌
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					two.setBrand(value);
					two.setType("2");
				}

			}
			break;
		case 52:
			// 主要检测设备数量
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					two.setEqNumber(Integer.parseInt(value));
					two.setType("2");
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 53:
			// 主要检测设备产能
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					two.setEqProduction(value);
					two.setType("2");
				}

			}
			break;
		case 54:
			// 主要检测设备使用年限
			if (value != null && !value.equals("&")) {
				if (MatchesString.matchesMAth(value)) {
					two.setEqLimit(Integer.parseInt(value));
					two.setType("2");
				} else {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[45] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE4;
				}

			}
			break;
		case 55:
			// 主要材料名称
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[47] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					material.setName(value);
				}

			}
			break;
		case 56:
			// 供应厂家
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[47] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					material.setFactory(value);
				}

			}
			break;
		case 57:
			// 是否京东合作过
			if (value != null && !value.equals("&")) {
				if (value.length() > 10) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[48] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					if ("是".equals(value)) {
						other.setIsCooperation("1");
					}
					other.setIsCooperation("0");
				}

			}
			break;
		case 58:
			// 现合作用户
			if (value != null && !value.equals("&")) {
				if (value.length() > 45) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[49] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					other.setCooperation(value);
				}

			}
			break;
		case 59:
			// 现合作用户考察
			if (value != null && !value.equals("&")) {
				if (value.length() > 10) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[50] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					if ("是".equals(value)) {
						other.setIsVisit("1");
					}
					other.setIsVisit("0");
				}

			}
			break;
		case 60:
			// 现合作用户考察
			if (value != null && !value.equals("&")) {
				if (value.length() > 200) {
					result = SupplierConstant.IMPORT_RESULT_FALSE
							+ SupplierConstant.IMPORT_RESULT_FSLAE1 + "| "
							+ row + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE2 + "| "
							+ titleArray[51] + " |"
							+ SupplierConstant.IMPORT_RESULT_FSLAE3
							+ SupplierConstant.IMPORT_RESULT_FSLAE11;
				} else {
					other.setMark(value);
				}

			}
			break;
			
		default:
			break;

		}
		return result;

	}

	public String getDictCode(String value, DictDataService dictDataService,
			String para) {
		List<DictData> data = dictDataService.findDictDataList(para);
		for (int i = 0; i < data.size(); i++) {
			DictData res = data.get(i);
			if (value.equals(res.getDictDataName())) {
				value = String.valueOf(res.getId());
				return value;
			}
		}
		return "";
	}
	
	/**
	 * 将Excel数据合并到
	 * @param upload
	 * @param uploadFileName
	 * @return
	 */
	 public List<List<Object>> common(InputStream is, String uploadFileName) {
	        Workbook workbook = null;
	        String extensionName = FilenameUtils.getExtension(uploadFileName);
	        // 创建一个列表，用于保存EXCEL工作表的数据
	        List<List<Object>> sheetData = new ArrayList<List<Object>>();
	        List<Object> data = new ArrayList<Object>();
	        try {
	            logger.debug("------------------------------extensionName is :"
	                    + extensionName);
	            if ("XLS".equalsIgnoreCase(extensionName)) {
	                workbook = new HSSFWorkbook(is);
	            }
	            else if ("XLSX".equalsIgnoreCase(extensionName)) {
	                workbook = new XSSFWorkbook(is);
	            }
	            
	            if (workbook == null) {
	                return null;
	            }
	            NumberFormat numFt = new DecimalFormat("#0");
	            // 解析公式结果
	            FormulaEvaluator evaluator = workbook.getCreationHelper()
	                    .createFormulaEvaluator();
	            Sheet sheet = workbook.getSheetAt(0);
	            
	            // 获取第一行对象
	            Row row = sheet.getRow(0);
	            // 以第一行的列数为准
	            int col = row.getLastCellNum() - row.getFirstCellNum();
	            // 获取excel中总行数
	            int rows = sheet.getLastRowNum() - sheet.getFirstRowNum();
	            Cell cell;
	            // 以行进行循环
	            for (int i = 0; i <= rows; i++) {
	                row = sheet.getRow(i);
	                // 以列进行循环，并且列的总数以第一行列数为准
	                // 通过迭代器遍历获得行数据后遍历行数据获得单元格数据，并保存在列表sheetData中
	                data = new ArrayList<Object>();
	                for (int j = 0; j < col; j++) {
	                    try {
	                        cell = (Cell) row.getCell(j);
	                        CellValue cellValue = evaluator.evaluate(cell);
	                        if (cellValue == null) {
	                            data.add("");
	                            continue;
	                        }
	                        switch (cellValue.getCellType()) {
	                            case Cell.CELL_TYPE_BOOLEAN:
	                                data.add(cellValue.getBooleanValue());
	                                break;
	                            case Cell.CELL_TYPE_STRING:
	                                data.add(cellValue.getStringValue());
	                                break;
	                            case Cell.CELL_TYPE_NUMERIC:
	                                // 这里的日期类型会被转换为数字类型，需要判别后区分处理
	                                if (DateUtil.isCellDateFormatted(cell)) {
	                                    data.add(cell.getDateCellValue());
	                                }
	                                else {
	                                    data.add(numFt.format(cellValue
	                                            .getNumberValue()));
	                                }
	                                break;
	                            case Cell.CELL_TYPE_FORMULA:
	                                break;
	                            case Cell.CELL_TYPE_BLANK:
	                                break;
	                            case Cell.CELL_TYPE_ERROR:
	                                break;
	                            default:
	                                break;
	                        }
	                        
	                    }
	                    catch (Exception e) {
	                        e.printStackTrace();
	                    }
	                }
	                sheetData.add(data);
	            }
	        }
	        catch (IOException e) {
	            
	            logger.error("读取excel异常", e);
	        }
	        finally {
	            if (is != null) {
	                try {
	                    is.close();
	                }
	                catch (IOException e) {
	                    logger.error("读取excel关闭异常", e);
	                }
	                workbook = null;
	                data = null;
	            }
	        }
	        return sheetData;
	    }
}
