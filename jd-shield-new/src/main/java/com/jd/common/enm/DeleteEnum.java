package com.jd.common.enm;
/**
 * 删除状态枚举类
 * @author wangchanghui
 *
 */
public enum DeleteEnum {
	YES("0","删除"),NO("1","可用");
	/**
	 * 状态
	 */
	private String status;
	
	/**
	 * 名称
	 */
	private String name;
	
	DeleteEnum(String status,String name){
		this.status = status;
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
