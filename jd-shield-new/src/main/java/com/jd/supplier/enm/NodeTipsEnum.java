/**
 * 
 */
package com.jd.supplier.enm;

import org.apache.commons.lang3.StringUtils;

/**
 * 节点提示信息
 * @author wangchanghui
 *
 */
public enum NodeTipsEnum {
	
	TASK_ASSIGNMENT(),
	TASK_CLAIM(),
	REQUIREMENT_CONFIRM(),
	PRE_SELECTION(),
	FINAL_SELECTION(),
	FINAL_SELECTION_CONFRIM(),
	EXPERT_FILTER(),
	PROJECT_MEETING(),
	SUCCESS(1),
	ERROR(0);
	
	private static final String ASSIGNMENT = "指派";
	
	private static final String CLAIM = "认领";
	
	private static final String CONFIRM = "确认";
	
	private static final String successMsg = "完成";
	
	private static final String errorMsg = "失败";
	
	private Integer type;
	
	NodeTipsEnum(){
	}
	
	NodeTipsEnum(Integer type){
		this.type = type;
	}

	/**
	 * 获取错误信息
	 * @param nodeType
	 * @param nodeName
	 * @param operType
	 * @param operName
	 * @param reason
	 * @return
	 */
	public String getError(Integer nodeType,String nodeName,Integer operType,String operName,String reason) {
		StringBuffer sb =  new StringBuffer().append(getTaskNode(nodeType,nodeName,operType,operName))
				.append(errorMsg);
		if(StringUtils.isNotBlank(reason))
			sb = sb.append(reason);
		return sb.toString();
	}
	
	/**
	 * 获取错误信息
	 * @param nodeType
	 * @param nodeName
	 * @param reason
	 * @return
	 */
	public String getError(Integer nodeType,String nodeName,String reason) {
		StringBuffer sb =  new StringBuffer().append(getTaskNode(nodeType,nodeName))
				.append(errorMsg);
		if(StringUtils.isNotBlank(reason))
			sb = sb.append(reason);
		return sb.toString();
	}

	/**
	 * 获取成功信息
	 * @param nodeType
	 * @param nodeName
	 * @param operType
	 * @param operName
	 * @param reason
	 * @return
	 */
	public String getSuccess(Integer nodeType,String nodeName,Integer operType,String operName,String reason) {
		StringBuffer sb =  new StringBuffer().append(getTaskNode(nodeType,nodeName,operType,operName))
				.append(successMsg);
		if(StringUtils.isNotBlank(reason))
			sb = sb.append(reason);
		return sb.toString();
	}
	
	/**
	 * 获取成功信息
	 * @param nodeType
	 * @param nodeName
	 * @param reason
	 * @return
	 */
	public String getSuccess(Integer nodeType,String nodeName,String reason) {
		StringBuffer sb =  new StringBuffer().append(getTaskNode(nodeType,nodeName))
				.append(successMsg);
		if(StringUtils.isNotBlank(reason))
			sb = sb.append(reason);
		return sb.toString();
	}

	/**
	 * 获得操作
	 * @param type
	 * @param name
	 * @return
	 */
	private String getOperation(Integer type,String name){
		if(type == null)
			return name == null ? "" : name;
		switch(type){
		case 1:
			return OprationEnum.ADD.getName();
		case 2:
			return OprationEnum.DELETE.getName();
		case 3:
			return OprationEnum.UPDATE.getName();
		default:
			return name == null ? "" : name;
		}
	}
	
	/**
	 * 获取任务节点
	 * @param nodeType
	 * @param nodeName
	 * @param operType
	 * @param operName
	 * @return
	 */
	private StringBuffer getTaskNode(Integer nodeType,String nodeName,Integer operType,String operName){
		if(operType == null && StringUtils.isBlank(operName))
			return getTaskNode(nodeType, nodeName);
		switch(nodeType){
		case 1:
			if(operType != null)
				return new StringBuffer(TaskNodeEnum.TASK_ASSIGNMENT.getName())
					.append(getOperation(operType,operName));
			else
				return getTaskNode(nodeType,nodeName,operName);
		case 2:
			if(operType != null)
				return new StringBuffer(TaskNodeEnum.TASK_CLAIM.getName())
				.append(getOperation(operType,operName));
			else
				return getTaskNode(nodeType,nodeName,operName);
		case 3:
			return new StringBuffer(TaskNodeEnum.REQUIREMENT_CONFIRM.getName())
			.append(getOperation(operType,operName));
		case 4:
			return new StringBuffer(TaskNodeEnum.PRE_SELECTION.getName())
			.append(getOperation(operType,operName));
		case 5:
			return new StringBuffer(TaskNodeEnum.FINAL_SELECTION.getName())
			.append(getOperation(operType,operName));
		case 6:
			if(operType != null)
				return new StringBuffer(TaskNodeEnum.FINAL_SELECTION_CONFRIM.getName())
				.append(getOperation(operType,operName));
			else
				return getTaskNode(nodeType,nodeName,operName);
		default:
			return new StringBuffer(nodeName)
			.append(getOperation(operType,operName));
		}
	}
	
	/**
	 * 获取任务节点
	 * @param nodeType
	 * @param nodeName
	 * @param operName
	 * @return
	 */
	public StringBuffer getTaskNode(Integer nodeType,String nodeName,String operName){
		switch(nodeType){
		case 1:
			return new StringBuffer(TaskNodeEnum.TASK_ASSIGNMENT.getName());
		case 2:
			return new StringBuffer(TaskNodeEnum.TASK_CLAIM.getName());
		case 6:
			return new StringBuffer(TaskNodeEnum.FINAL_SELECTION_CONFRIM.getName());
			}
		return new StringBuffer(nodeName);
	}
	
	/**
	 * 获取任务节点
	 * @param nodeType
	 * @param nodeName
	 * @return
	 */
	public StringBuffer getTaskNode(Integer nodeType,String nodeName){
		switch(nodeType){
		case 1:
			return new StringBuffer(TaskNodeEnum.TASK_ASSIGNMENT.getName());
		case 2:
			return new StringBuffer(TaskNodeEnum.TASK_CLAIM.getName());
		case 3:
			return new StringBuffer(TaskNodeEnum.REQUIREMENT_CONFIRM.getName());
		case 4:
			return new StringBuffer(TaskNodeEnum.PRE_SELECTION.getName());
		case 5:
			return new StringBuffer(TaskNodeEnum.FINAL_SELECTION.getName());
		case 6:
			return new StringBuffer(TaskNodeEnum.FINAL_SELECTION_CONFRIM.getName());
		case 7:
			return new StringBuffer(TaskNodeEnum.TASK_ASSIGNMENT_Q.getName());
		case 8:
			return new StringBuffer(TaskNodeEnum.TASK_CLAIM_Q.getName());
		case 9:
			return new StringBuffer(TaskNodeEnum.EXPERT_FILTER.getName());
		case 10:
			return new StringBuffer(TaskNodeEnum.TASK_ASSIGNMENT_B.getName());
		case 11:
			return new StringBuffer(TaskNodeEnum.TASK_CLAIM_B.getName());
		case 12:
			return new StringBuffer(TaskNodeEnum.PROJECT_MEETING.getName());
			}
		return new StringBuffer(nodeName);
	}

	public Integer getType() {
		return type;
	}
}
