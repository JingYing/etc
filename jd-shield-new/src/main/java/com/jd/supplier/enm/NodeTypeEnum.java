/**
 * 
 */
package com.jd.supplier.enm;

/**
 * 节点类型枚举
 * @author wangchanghui
 *
 */
public enum NodeTypeEnum {
	ROOT("root","根节点"),PARENT("parent","父节点");
	
	/**
	 * 类型
	 */
	private final String type;
	
	/**
	 * 名称
	 */
	private String name;
	
	NodeTypeEnum(String type,String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
