package com.jd.supplier.enm;

/**
 * 供应商预选入围枚举
 * @author wangchanghui
 *
 */
public enum SupplierSelectionEnum {

	PREVIOUS(0,"预选"),FINAL(1,"入围");
	
	/**
	 * 类型
	 */
	private final int type;
	
	/**
	 * 名称
	 */
	private String name;
	
	SupplierSelectionEnum(int type,String name){	
		this.type = type;	
		this.name = name;
	}


	public String getName() {
		return name;
	}


	public int getType() {
		return type;
	}

}
