/**
 * 
 */
package com.jd.supplier.enm;

/**
 * 任务节点枚举
 * @author wangchanghui
 *
 */
public enum TaskNodeEnum {
	
	TASK_ASSIGNMENT(1,"任务指派","task_assignment"),
	TASK_CLAIM(2,"任务认领","task_claim"),
	REQUIREMENT_CONFIRM(3,"需求确认会","task_confirm"),
	PRE_SELECTION(4,"预选","task_preSelection"),
	FINAL_SELECTION(5,"入围","task_finalSelection"),
	FINAL_SELECTION_CONFRIM(6,"入围确认","task_finalSelection_confirm"),
	TASK_ASSIGNMENT_Q(7,"任务指派","task_assignment_q"),
	TASK_CLAIM_Q(8,"任务认领","task_claim_q"),
	EXPERT_FILTER(9,"专家筛选","task_expertFilter"),
	TASK_ASSIGNMENT_B(10,"任务指派","task_assignment_b"),
	TASK_CLAIM_B(11,"任务认领","task_claim_b"),
	PROJECT_MEETING(12,"立项会","project_meeting");
	
	/**
	 * 类型
	 */
	private final Integer type;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 代码
	 */
	private String code;
	
	TaskNodeEnum(Integer type,String name,String code){
		this.type = type;
		this.name = name;
		this.code = code;
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	
}
