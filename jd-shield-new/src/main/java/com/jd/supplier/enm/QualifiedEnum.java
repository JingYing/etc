package com.jd.supplier.enm;
/**
 * 质量认证枚举
 * @author wangchanghui
 *
 */
public enum QualifiedEnum {

	QUALIFIED(1,"合格"),UN_QUALIFIED(0,"不合格");
	
	/**
	 * 类型
	 */
	private final int type;
	
	/**
	 * 名称
	 */
	private String name;
	
	QualifiedEnum(int type,String name){
		this.type = type;
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}
