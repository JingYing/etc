package com.jd.supplier.model;


import com.jd.official.core.model.IdModel;

public class RequirementAffix  extends IdModel {
    
    private static final long serialVersionUID = 1659168822681619710L;

    private Long bussineId;
    
    private String bussineType;

    private String fileType;

    private String fileName;
    
    private String fileKey;

    private String status;

  

    public String getBussineType() {
        return bussineType;
    }

    public void setBussineType(String bussineType) {
        this.bussineType = bussineType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getBussineId() {
        return bussineId;
    }

    public void setBussineId(Long bussineId) {
        this.bussineId = bussineId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType == null ? null : fileType.trim();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

	public String getFileKey() {
		return fileKey;
	}

	public void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}
    
}