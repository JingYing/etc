package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

/**
 * 任务单节点
 * @author wangchanghui
 *
 */
public class TaskNode extends IdModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = -4333511057211850936L;
	
	
	public TaskNode(Long id){
		this.id = id;
	}
	
	public TaskNode(){
		super();
	}

	/**
	 * 任务表id
	 */
	private Long taskId;

	/**
	 * 节点id
	 */
    private Integer taskNodeId;

    /**
     * 节点状态
     */
    private Integer nodeStatus;

    /**
     * 父节点id
     */
    private Long parentId;
    
    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 组织机构id
     */
    private Integer organizationId;

    /**
     * 功能链接
     */
    private String url;

    /**
     * 删除状态
     */
    private String status;
    
    /**
     * 机构名称
     */
    private String organizationName;
    
    /**
     * 节点名称
     */
    private String nodeName;
    
    /**
     * 角色名称
     */
    private String roleName;
    
    /**
     * 节点样式
     */
    private String nodeStyle;
    
    /**
     * 节点类型
     */
    private Integer type;
    
    /**
     * 驳回原因
     */
    private String reason;
    
    /**
     * 执行人id
     */
    private Long userId;
    
    /**
     * 角色code
     */
    private String roleCode;


    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getNodeStatus() {
        return nodeStatus;
    }

    public void setNodeStatus(Integer nodeStatus) {
        this.nodeStatus = nodeStatus;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getTaskNodeId() {
		return taskNodeId;
	}

	public void setTaskNodeId(Integer taskNodeId) {
		this.taskNodeId = taskNodeId;
	}

	public String getNodeStyle() {
		return nodeStyle;
	}

	public void setNodeStyle(String nodeStyle) {
		this.nodeStyle = nodeStyle;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

}