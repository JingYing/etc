package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;

public class AnnualRequirementAudit  extends IdModel  {

	public AnnualRequirementAudit(){
		GregorianCalendar gc = new GregorianCalendar();
		tenderStartDate = gc.getTime();
	}
	
    private Long tJdsupplierRequirementYearId;

    private String approvalStatus;
    
    private String approvalReply;

    private String tenderType;

    private String isBudget;

    private String isTenderCenter;

    private Date tenderStartDate;

    private String tenderLevel;

    private String isTenderMethod;

    private String isPlan;

    private String mark;


    private String status;



	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long gettJdsupplierRequirementYearId() {
        return tJdsupplierRequirementYearId;
    }

    public void settJdsupplierRequirementYearId(Long tJdsupplierRequirementYearId) {
        this.tJdsupplierRequirementYearId = tJdsupplierRequirementYearId;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus == null ? null : approvalStatus.trim();
    }
    
    public String getApprovalReply() {
		return approvalReply;
	}

	public void setApprovalReply(String approvalReply) {
		this.approvalReply = approvalReply;
	}

	public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType == null ? null : tenderType.trim();
    }

    public String getIsBudget() {
        return isBudget;
    }

    public void setIsBudget(String isBudget) {
        this.isBudget = isBudget == null ? null : isBudget.trim();
    }

    public String getIsTenderCenter() {
        return isTenderCenter;
    }

    public void setIsTenderCenter(String isTenderCenter) {
        this.isTenderCenter = isTenderCenter == null ? null : isTenderCenter.trim();
    }

    public Date getTenderStartDate() {
    	if(tenderStartDate==null)
    		return null;
        return (Date)tenderStartDate.clone();
    }

    public void setTenderStartDate(Date tenderStartDate) {
    	if(tenderStartDate != null)
			this.tenderStartDate = new Date(tenderStartDate.getTime());
		else
			this.tenderStartDate = null;
    }

    public String getTenderLevel() {
        return tenderLevel;
    }

    public void setTenderLevel(String tenderLevel) {
        this.tenderLevel = tenderLevel == null ? null : tenderLevel.trim();
    }

    public String getIsTenderMethod() {
        return isTenderMethod;
    }

    public void setIsTenderMethod(String isTenderMethod) {
        this.isTenderMethod = isTenderMethod == null ? null : isTenderMethod.trim();
    }

    public String getIsPlan() {
        return isPlan;
    }

    public void setIsPlan(String isPlan) {
        this.isPlan = isPlan == null ? null : isPlan.trim();
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark == null ? null : mark.trim();
    }


}