package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

/**
 * 供应商预选入围
 * @author wangchanghui
 *
 */
public class TaskSuppSelection extends IdModel{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5862788932669199720L;

	/**
	 * 任务单id
	 */
    private Long taskId;
    
    /**
     * 需求id
     */
    private Long requirementId;
    
    /**
     * 供应商id
     */
    private Long supplierId;
    

    /**
     * 是否最终入围 1：最终入围，0：预选
     */
	private Integer finalFlag;

	/**
     * 是否样品合格 1：合格，0：不合格
     */
    private Integer beSampleQualified;

    /**
     * 是否考察合格 1：合格，0：不合格
     */
    private Integer beInvestigationQualified;

    /**
     * 是否愿意参加投标 1：愿意、0：不愿意
     */
    private Integer beWillingTo;
    
    /**
     * 是否财务检查合格 1：合格，0：不合格
     */
    private Integer beFinanceQualified;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 调查报告名称
     */
    private String investigationReportName;

    /**
     * 调查报告id
     */
    private String investigationReportId;

    /**
     * 样品测试报告名称
     */
    private String sampleTestReportName;

    /**
     * 样品测试报告id
     */
    private String sampleTestReportId;

    /**
     * 删除状态
     */
    private String status;
    
    /**
     * 附加字段
     * 只用于页面显示
     * source:enterprise.name
     * 企业名称
     */
    private String enterpriseName;
    
    /**
     * 附加字段
     * 只用于页面显示
     * 
     * 企业等级
     */
    private String grade;
    
    /**
     * 附加字段
     * 只用于页面显示
     * 
     * 注册资金
     * source:enterprise.funds
     */
    private String funds;
    
    /**
     * 企业性质
     * source:enterprise.enterpriseType
     */
    private String enterpriseType;
    
    /**
     * 地区
     * source:enterprise.area
     */
    private String supplierOwner;
    
    /**
     * 员工数量
     * source:enterprise.employeeCount
     */
    private String employeeCount;
    
    /**
     * 质量认证
     * source:enterprise.qualification
     */
    private String qualification;
    
    /**
     * 经营内容
     * source:enterprise.content
     */
    private String content;
    
    /**
     * 供应商删除状态
     */
    private String enterpriseStatus;
    


    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }



    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public String getInvestigationReportName() {
        return investigationReportName;
    }

    public void setInvestigationReportName(String investigationReportName) {
        this.investigationReportName = investigationReportName == null ? null : investigationReportName.trim();
    }

    public String getInvestigationReportId() {
        return investigationReportId;
    }

    public void setInvestigationReportId(String investigationReportId) {
        this.investigationReportId = investigationReportId == null ? null : investigationReportId.trim();
    }

    public String getSampleTestReportName() {
        return sampleTestReportName;
    }

    public void setSampleTestReportName(String sampleTestReportName) {
        this.sampleTestReportName = sampleTestReportName == null ? null : sampleTestReportName.trim();
    }

    public String getSampleTestReportId() {
        return sampleTestReportId;
    }

    public void setSampleTestReportId(String sampleTestReportId) {
        this.sampleTestReportId = sampleTestReportId == null ? null : sampleTestReportId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}


	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName == null ? null : enterpriseName.trim();
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade == null ? null : grade.trim();
	}

	public String getFunds() {
		return funds;
	}

	public void setFunds(String funds) {
		this.funds = funds == null ? null : funds.trim();
	}

	public String getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(String enterpriseType) {
		this.enterpriseType = enterpriseType == null ? null : enterpriseType.trim();
	}

	public String getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(String employeeCount) {
		this.employeeCount = employeeCount == null ? null : employeeCount.trim();
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification == null ? null : qualification.trim();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}


	public Integer getBeSampleQualified() {
		return beSampleQualified;
	}

	public void setBeSampleQualified(Integer beSampleQualified) {
		this.beSampleQualified = beSampleQualified;
	}

	public Integer getBeInvestigationQualified() {
		return beInvestigationQualified;
	}

	public void setBeInvestigationQualified(Integer beInvestigationQualified) {
		this.beInvestigationQualified = beInvestigationQualified;
	}

	public Integer getBeWillingTo() {
		return beWillingTo;
	}

	public void setBeWillingTo(Integer beWillingTo) {
		this.beWillingTo = beWillingTo;
	}

	public Integer getBeFinanceQualified() {
		return beFinanceQualified;
	}

	public void setBeFinanceQualified(Integer beFinanceQualified) {
		this.beFinanceQualified = beFinanceQualified;
	}

	public Integer getFinalFlag() {
		return finalFlag;
	}

	public void setFinalFlag(Integer finalFlag) {
		this.finalFlag = finalFlag;
	}

	public String getSupplierOwner() {
		return supplierOwner;
	}

	public void setSupplierOwner(String supplierOwner) {
		this.supplierOwner = supplierOwner == null ? null : supplierOwner.trim();
	}

	public String getEnterpriseStatus() {
		return enterpriseStatus;
	}

	public void setEnterpriseStatus(String enterpriseStatus) {
		this.enterpriseStatus = enterpriseStatus == null ? null : enterpriseStatus.trim();
	}

	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

}