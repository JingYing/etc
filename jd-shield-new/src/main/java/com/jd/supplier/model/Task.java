package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

/**
 * 任务单
 * @author wangchanghui
 *
 */
public class Task extends IdModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 5659624615295766869L;

	/**
	 * 项目id 及需求id
	 */
	private Long requirementId;
	
	/**
	 * 任务状态
	 */
    private Integer taskStatus;

    /**
     * 备注
     */
    private String remarks;


    /**
     * 删除状态
     */
    private String status;
    
    
    /**
     * 预算金额
     */
    private Long budgetAmount;
    
    /**
     * 申请编号
     */
    private String projectNum;
    
    /**
     * 申请日期
     */
    private String projectDate;
    
    
    /**
     * 项目名称
     */
    private String projectName;
    
    /**
     * 申请人（erp账号）
     */
    private String applicant;
    
    /**
     * 申请人姓名
     */
    private String applicantName;
    
    /**
     * 申请人电话
     */
    private String applicantTel;
    
    /**
     * 申请人部门
     */
    private String applicantDep;

    /**
     * 申请人部门id
     */
    private String applicantDepId;
    
    
    private String rstatus;
    
    /**
     * 状态名称
     */
    private String statusName;
    
    /**
     * 状态码
     */
    private String statusCode;


    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public Integer getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Integer taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemark(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }


	public String getProjectNum() {
		return projectNum;
	}

	public void setProjectNum(String projectNum) {
		this.projectNum = projectNum;
	}

	public String getProjectDate() {
		return projectDate;
	}

	public void setProjectDate(String projectDate) {
		this.projectDate = projectDate;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getApplicantTel() {
		return applicantTel;
	}

	public void setApplicantTel(String applicantTel) {
		this.applicantTel = applicantTel;
	}

	public String getApplicantDep() {
		return applicantDep;
	}

	public void setApplicantDep(String applicantDep) {
		this.applicantDep = applicantDep;
	}

	public String getApplicantDepId() {
		return applicantDepId;
	}

	public void setApplicantDepId(String applicantDepId) {
		this.applicantDepId = applicantDepId;
	}

	public String getRstatus() {
		return rstatus;
	}

	public void setRstatus(String rstatus) {
		this.rstatus = rstatus;
	}

	public Long getBudgetAmount() {
		return budgetAmount;
	}

	public void setBudgetAmount(Long budgetAmount) {
		this.budgetAmount = budgetAmount;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
}