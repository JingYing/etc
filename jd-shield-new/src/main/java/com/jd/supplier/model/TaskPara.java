package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

/**
 * 任务单参数
 * @author wangchanghui
 *
 */
public class TaskPara extends IdModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 8173693457961335121L;

	/**
	 * 代码
	 */
	private String code;

	/**
	 * 名称
	 */
    private String name;

    /**
     * 1:taskNodeId 2:roleId 3:organizationId
     */
    private Integer type;

    /**
     * 链接
     */
    private String url;
    
    /**
     * 类型名称
     */
    private String typeName;
    
    /**
     * 节点类型
     */
    private Integer nodeType;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}
}