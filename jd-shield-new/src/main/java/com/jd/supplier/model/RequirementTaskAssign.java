package com.jd.supplier.model;


import com.jd.official.core.model.IdModel;

public class RequirementTaskAssign extends IdModel {

    private static final long serialVersionUID = 1659168822681619710L;
    
    private Long taskId;

    private Integer roleid;

    private String status;


    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

   
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}