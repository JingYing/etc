package com.jd.supplier.model;

public class ExpertExtraction {
	
	public static final String 
		EXTRACT_MODE_系统抽取 	= "auto", 
		EXTRACT_MODE_人工补录 	= "manual",
		DICT_TYPE_拒绝原因 	= "refuseReason";	//respDesc所用字典类型
	
	public static final int
		RESP_CODE_已通知 		= 0,
		RESP_CODE_已确认参加 	= 1,
		RESP_CODE_已拒绝 		= 2,
		RESP_CODE_发送邮件失败 	= 3;

	private Integer id;
	private long extractionId;
	private Expert expert;
	private Integer respCode;	
	private String respDesc, extractMode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getExtractionId() {
		return extractionId;
	}

	public void setExtractionId(long extractionId) {
		this.extractionId = extractionId;
	}

	public Expert getExpert() {
		return expert;
	}

	public void setExpert(Expert expert) {
		this.expert = expert;
	}

	public Integer getRespCode() {
		return respCode;
	}

	public void setRespCode(Integer respCode) {
		this.respCode = respCode;
	}

	public String getRespDesc() {
		return respDesc;
	}

	public void setRespDesc(String respDesc) {
		this.respDesc = respDesc;
	}

	public String getExtractMode() {
		return extractMode;
	}

	public void setExtractMode(String extractMode) {
		this.extractMode = extractMode;
	}
}
