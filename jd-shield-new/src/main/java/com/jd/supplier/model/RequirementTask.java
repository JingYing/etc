package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

public class RequirementTask extends IdModel {
    private static final long serialVersionUID = 1659168822681619710L;

    private Long requirementId;

    private String taskStatus;

    private String remarks;

    private String status;

    public Long getRequirementId() {
	return requirementId;
    }

    public void setRequirementId(Long requirementId) {
	this.requirementId = requirementId;
    }

    public String getTaskStatus() {
	return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
	this.taskStatus = taskStatus == null ? null : taskStatus.trim();
    }

    public String getRemarks() {
	return remarks;
    }

    public void setRemarks(String remarks) {
	this.remarks = remarks;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status == null ? null : status.trim();
    }
}