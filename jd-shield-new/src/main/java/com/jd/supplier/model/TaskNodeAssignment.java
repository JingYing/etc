package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

/**
 * 任务节点指派
 * @author wangchanghui
 *
 */
public class TaskNodeAssignment extends IdModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 835613526196081481L;

	private Long nodeId;

	/**
	 * 认领人的id
	 */
    private Long userId;
    
    /**
     * 指派人的id
     */
    private Long assignedId;

    /**
     * 指派状态
     */
    private Integer assignStatus;

    private String status;
    
    /**
     * 原因
     */
    private String reason;


    public TaskNodeAssignment(){
    	super();
    }
    
    public TaskNodeAssignment(Long id,Long userId,Integer assignStatus,String reason){
    	this.id = id ;
    	this.userId = userId;
    	this.assignStatus = assignStatus;
    	this.reason = reason;
    }
    
    public TaskNodeAssignment(Long id,Integer assignStatus){
    	this.id = id;
    	this.assignStatus = assignStatus;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getAssignStatus() {
        return assignStatus;
    }

    public void setAssignStatus(Integer assignStatus) {
        this.assignStatus = assignStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

	public Long getAssignedId() {
		return assignedId;
	}

	public void setAssignedId(Long assignedId) {
		this.assignedId = assignedId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}