package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;
/**
 * 广告位
 * @author jingying
 *
 */
public class Advertising extends IdModel {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9108929089526109704L;

	public Advertising(){
		GregorianCalendar gc = new GregorianCalendar();
		expireTime = gc.getTime();
	}
	/**
	 * 广告位类型: 顶部, 底部
	 */
	public static final String 
		POSITION_TOP = "top", 	
		POSITION_BOTTOM = "bottom";

	private String advertiser, position, picUrl, picKey, linkUrl;
	private Date expireTime;
	private int sortNum, isDel;

	public String getAdvertiser() {
		return advertiser;
	}

	public void setAdvertiser(String advertiser) {
		this.advertiser = advertiser;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public Date getExpireTime() {
		if(expireTime == null)
			return null;
		return (Date)expireTime.clone();
	}

	public void setExpireTime(Date expireTime) {
		if(expireTime != null)
			this.expireTime = new Date(expireTime.getTime());
		else
			this.expireTime = null;
	}

	public int getSortNum() {
		return sortNum;
	}

	public void setSortNum(int sortNum) {
		this.sortNum = sortNum;
	}

	public String getPicKey() {
		return picKey;
	}

	public void setPicKey(String picKey) {
		this.picKey = picKey;
	}

	public int getIsDel() {
		return isDel;
	}

	public void setIsDel(int isDel) {
		this.isDel = isDel;
	}
}
