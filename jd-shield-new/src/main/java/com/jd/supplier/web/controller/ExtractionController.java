package com.jd.supplier.web.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.common.util.Util;
import com.jd.official.core.dao.Page;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.ExpertExtraction;
import com.jd.supplier.model.Extraction;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.service.ExpertService;
import com.jd.supplier.service.ExtractionService;
import com.jd.supplier.service.RequirementService;
import com.jd.supplier.web.vo.ExtractionVO;

/**
 * 抽取历史相关页面. 补录相关页面
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年4月22日
 */
@Controller
@RequestMapping(value = "/extract")
public class ExtractionController 	{
	
	@Resource
	private DictDataService dictDataService; 
	@Resource
	private ExpertService expertService;
	@Resource
	private ExtractionService extractionService;
	@Resource
	private RequirementService reqService; 
	
	private Gson gson = new GsonBuilder().serializeNulls().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	
	/**
	 * 系统抽取首页
	 * @param id requirement的ID
	 * @param taskId
	 * @param nodeId
	 * @return
	 */
	@RequestMapping(value="index")
	public ModelAndView index(long id, Long taskId, Long nodeId)	{
		Requirement req = reqService.get(id);
		List<DictData> 
			tags = expertService.findAllAvailableTag(),
			regions = dictDataService.findDictDataList(Expert.DICT_TYPE_所属区域),
			groups = dictDataService.findDictDataList(Expert.DICT_TYPE_专家组别),
			budgetScopes = dictDataService.findDictDataList(Extraction.DICT_TYPE_预算范围);
		
		ModelAndView mav = new ModelAndView("supplier/extract_index")
			.addObject("req", req)
			.addObject("taskId", taskId)
			.addObject("nodeId", nodeId)
			.addObject("tags", new Gson().toJson(tags))
			.addObject("regions", regions)
			.addObject("groups", groups)
			.addObject("budgetScopes", budgetScopes);
		
		Extraction extraction = extractionService.findByReqId(id); 
		if(extraction != null)	{
			List<ExpertExtraction> l = extractionService.findExpertExtraction(extraction.getId());
			mav.addObject("eeList", l);
		}
		return mav;
	}
	
	@RequestMapping(value="/ee/searchApi")
	@ResponseBody
	public String findExpertExtraction(long reqId)	{
		Extraction extraction = extractionService.findByReqId(reqId); 
		if(extraction != null)	{
			 List<ExpertExtraction> list = extractionService.findExpertExtraction(extraction.getId());
			 JsonElement je = gson.toJsonTree(list);
			 
			 JsonObject json = new JsonObject();
			 json.add("aaData", je);
			 return json.toString();
		} else	{
			return null;
		}
	}
	
	@RecordLog(operationType=OperationTypeValue.add, entityName="Extraction")
	@RequestMapping(value="extract")
	@ResponseBody
	public synchronized String extract(Extraction extraction)	{	//不允许并发抽取
		int err = 0;
		String desc = null;
		if(extractionService.findByReqId(extraction.getReq().getId()) == null)	{
			extraction.setCreator(ComUtils.getLoginNamePin());
			extraction.setCreateTime(new Date());
			List<Expert> extracted = extractionService.insertExtract(extraction);
			if(extracted.isEmpty())	{
				err = 1;
			}
		} else	{
			err = 2;//已抽取过,不能再次抽取
		}
		
		JsonObject j = new JsonObject();
		j.addProperty("err", err);
		j.addProperty("desc", desc);
		return j.toString();
	}
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) throws ServletException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

	
	
	@RequestMapping(value="historyIndex")
	public String historyIndex()	{
		return "supplier/extract_history_index";
	}
	
	/**
	 * 搜索
	 * @return
	 */
	@RequestMapping(value="search")
	@ResponseBody
	public String search(HttpServletRequest req, String projectName, String creator, String startTime , String endTime)	{
		PageWrapper wrapper = new PageWrapper(req);
		Page<Extraction> pager = extractionService.find(projectName, creator, startTime, endTime, 
				wrapper.getsStart(), wrapper.getsLength());
		
		List<ExtractionVO> list = new ArrayList<ExtractionVO>();
		for(Extraction e : pager.getResult())	{
			int[] arr = extractionService.calculateExpertCount(e.getId());
			String extractResult = String.format("预设抽取%d人，待回复%d人，确认参加%d人", arr[0], arr[3], arr[4]);
			if(arr[2] > 0)	{
				extractResult += "<font color='red'>，需要人工补录" + arr[2] +"人</font>";
			}
			list.add(new ExtractionVO(e, extractResult, arr[2]));
		}
		
		Page<ExtractionVO> voPage = new Page<ExtractionVO>();
		voPage.setResult(list);
		voPage.setTotalCount(pager.getTotalCount());
		return this.toDataTablesJson(voPage, wrapper.getsEcho()).toString();
	}
	
	private <T> JsonObject toDataTablesJson(Page<T> pager, String sEcho)	{
		JsonObject json = new JsonObject();
		json.addProperty("sEcho", sEcho);
		json.addProperty("iTotalRecords", pager.getTotalCount());
		json.addProperty("iTotalDisplayRecords", pager.getTotalCount());
		json.add("aaData", gson.toJsonTree(pager.getResult()));
		return json;
	}
	
	/**
	 * 进行补录
	 * @param ids
	 * @param extractionId
	 * @return err:1补录人数超限, 2补录专家已在该项目中
	 */
	@RequestMapping(value="doAddition")
	@ResponseBody
	public synchronized String doAddition(String ids, long extId)	{	//不允许并发操作
		int err = 0;
		String desc = null;
		
		long[] expertIds = Util.toLongArray(ids.split(","));
		boolean hasExtracted = false;
		for(long id : expertIds)	{
			if(extractionService.hasExtracted(extId, id))	{
				err = 2;
				desc = "专家编号:" + id;
				hasExtracted = true;
				break;
			}
		}
		
		if(!hasExtracted)	{
			int[] arr = extractionService.calculateExpertCount(extId);
			
			if(expertIds.length > arr[2])	{
				err = 1;
				desc = arr[2] + "";
			} else	{
				List<Expert> experts = expertService.findByIds(expertIds);
				extractionService.insertAddition(experts, extId, ExpertExtraction.EXTRACT_MODE_人工补录);
			}
		}
		
		JsonObject j = new JsonObject();
		j.addProperty("err", err);	
		j.addProperty("desc", desc);
		return j.toString();
	}
	
	/**
	 * 导出EXCEL
	 * @return
	 */
	@RequestMapping(value="export", produces="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	@ResponseBody
	public byte[] export(String prjName, String creator, String startTime , String endTime, HttpServletResponse resp)	{
		List<Extraction> list = extractionService.find(prjName, creator, startTime, endTime, 
				0, Integer.MAX_VALUE).getResult();
		try {
			byte[] bytes =  extractionService.toExcel(list);
			resp.addHeader("Content-Disposition", "attachment;filename=chouqujilu" + new SimpleDateFormat("MMddHHmmss").format(new Date()) + ".xlsx");
			return bytes;
		} catch (IOException e) {
			resp.setStatus(500);
			return null;
		}
	}
	
	/**
	 * 查看详情
	 * @return
	 */
	@RequestMapping(value="info")
	public ModelAndView info(int id)	{
		List<ExpertExtraction> list = extractionService.findExpertExtraction(id);
		List<DictData> groups = dictDataService.findDictDataList(Expert.DICT_TYPE_专家组别);
		int[] arr = extractionService.calculateExpertCount(id);
		
		Extraction extraction = extractionService.findById(id);
		List<Expert> avoidExperts = new ArrayList<Expert>();
		if(extraction.getAvoidExpert() != null)	{
			long[] ids = Util.toLongArray(extraction.getAvoidExpert().split(","));
			avoidExperts = expertService.findByIds(ids);
		}
		
		return new ModelAndView("supplier/extract_info")
			.addObject("extraction", extraction)
			.addObject("expertExtractions", list)
			.addObject("avoidExperts", avoidExperts)
			.addObject("groups", groups)
			.addObject("needAddition", arr[2]);
	}
	
	/**
	 * 专家接收到邮件后回复入口
	 * @param e expertId
	 * @param r requirementId
	 * @return
	 */
	@RequestMapping(value="reply")
	public ModelAndView reply(long e, long r)	{
		List<DictData> reasons = dictDataService.findDictDataList(ExpertExtraction.DICT_TYPE_拒绝原因);
		return new ModelAndView("supplier/extract_reply")
			.addObject("expertId", e)
			.addObject("reqId", r)
			.addObject("reasons", reasons);
	}
	
	/**
	 * 根据专家回复, 修改回复状态
	 * @param e
	 * @param r
	 * @param respCode
	 * @param respDesc
	 * @return
	 */
	@RequestMapping(value="doReply")
	@ResponseBody
	public String doReply(long e, long r, int respCode, String respDesc)	{
		ExpertExtraction ee = extractionService.findExpertExtraction(r, e);
		ee.setRespCode(respCode);
		ee.setRespDesc(respDesc);
		extractionService.saveExpertExtraction(ee);
		return null;
	}
	
	/**
	 * 评标报表统计首页
	 * @return
	 */
	@RequestMapping(value="rptIndex")
	public String extractReportIndex()	{
		return "supplier/extract_report_index";
	}

}
