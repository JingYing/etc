package com.jd.supplier.web.controller;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.jd.common.util.Util;
import com.jd.official.core.dao.Page;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.supplier.model.Advertising;
import com.jd.supplier.service.AdvertisingService;

/**
 * 广告位管理
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年6月4日
 */
@Controller
@RequestMapping(value = "/adv")
public class AdvertisingController {
	
	private static Logger log = Logger.getLogger(AdvertisingController.class);
	
	@Resource
	private AdvertisingService advertisingService;
	
	private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().create();
	
	/**
	 * 首页
	 * @return
	 */
	@RequestMapping(value="index")
	public ModelAndView index()  {
		Map<String, String> positions = advertisingService.findPositionValue();
		return new ModelAndView("supplier/adv_index")
			.addObject("positions", positions);
	}
	
	/**
	 * 录入页
	 * @return
	 */
	@RequestMapping(value="input")
	public ModelAndView input()  {
		Map<String, String> positions = advertisingService.findPositionValue();
		return new ModelAndView("supplier/adv_input")
			.addObject("positions", positions);
	}
	
	/**
	 * 增加
	 * @param adv
	 * @return
	 */
	@RequestMapping(value="add", method=RequestMethod.POST)
	@ResponseBody
	public String add(Advertising adv)  {
		int err = 0;
		try {
			adv.setIsDel(0);
			adv.setSortNum(Integer.MAX_VALUE);	//初始排序在最后
			adv.setCreator(ComUtils.getLoginNamePin());
			adv.setCreateTime(new Date());
			advertisingService.save(adv);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			err = 500;
		}
		return "{\"err\": " + err + "}";
	}
	
	/**
	 * 上传. uploadify在firefox/chrome下无法携带COOKIE,所以拦截器要排除
	 * @param file
	 * @return
	 */
	@RequestMapping(value="upload")
	@ResponseBody
	public String upload(MultipartFile file)	{
		int err = 0;
		String desc = null, picKey = null, picUrl = null;
		
		try {
			String[] arr = advertisingService.uploadToCloud(file);
			picKey = arr[0];
			picUrl = arr[1];
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			err = 500;
			desc = e.getMessage();
		}
		
		JsonObject json = new JsonObject();
		json.addProperty("err", err);
		json.addProperty("desc", desc);
		json.addProperty("picKey", picKey);
		json.addProperty("picUrl", picUrl);
		return json.toString();
	}
	
	/**
	 * 编辑页
	 * @param id
	 * @return
	 */
	@RequestMapping(value="edit")
	public ModelAndView edit(long id)  {
		Map<String, String> positions = advertisingService.findPositionValue();
		Advertising adv = advertisingService.findById(id);
		return new ModelAndView("supplier/adv_edit")
			.addObject("positions", positions)
			.addObject("adv", adv);
	}
	
	/**
	 * 修改
	 * @param adv
	 * @return
	 */
	@RequestMapping(value="doEdit", method=RequestMethod.POST)
	public String doEdit(Advertising adv)  {
		try {
			Advertising exist = advertisingService.findById(adv.getId());
			String existPicKey = exist.getPicKey();
			exist.setAdvertiser(adv.getAdvertiser());
			exist.setExpireTime(adv.getExpireTime());
			exist.setLinkUrl(adv.getLinkUrl());
			exist.setPicKey(adv.getPicKey());
			exist.setPicUrl(adv.getPicUrl());
			exist.setPosition(adv.getPosition());
			//exist.setSortNum(adv.getSortNum());	//不修改排序号
			exist.setModifier(ComUtils.getLoginNamePin());
			exist.setModifyTime(new Date());
			advertisingService.save(exist);
			
			if(existPicKey != null && !"".equals(existPicKey) && !existPicKey.equals(adv.getPicKey()))	{
				advertisingService.deleteFromCloud(existPicKey);	//删除旧文件
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return "redirect:/adv/index";
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequestMapping(value="del")
	@ResponseBody
	public String del(long id)  {
		int err = 0;
		try {
			advertisingService.delete(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			err = 500;
		}
		return "{\"err\": " + err + "}";
	}
	
	/**
	 * 排序页
	 * @return
	 */
	@RequestMapping(value="sort")
	public ModelAndView sort()  {
		Map<String, String> positions = advertisingService.findPositionValue();
		Map advMap = new LinkedHashMap();
		for(Entry<String, String> entry : positions.entrySet())	{
			List<Advertising> list = advertisingService.findByPosition(entry.getKey(), 0, Integer.MAX_VALUE).getResult();
			advMap.put(entry.getKey(), list);
		}
		
		return new ModelAndView("supplier/adv_sort")
				.addObject("positions", positions)
				.addObject("advMap", advMap);
	}
	
	
	/**
	 * 排序 
	 * @param req
	 * @return
	 */
	@RequestMapping(value="doSort", method=RequestMethod.POST)
	@ResponseBody
	public String doSort(HttpServletRequest	req)  {
		List<long[]> sorted = new ArrayList<long[]>();
		Map<String, String[]> map = req.getParameterMap();
		for(Entry<String, String[]> e : map.entrySet())	{
			if(!(e.getValue().length == 1 && "".equals(e.getValue()[0])))	{	//如果参数不是xxx=这样的形式
				sorted.add(Util.toLongArray(req.getParameterValues(e.getKey())));
			}
		}
		advertisingService.saveOrder(sorted);
		return null;
	}
	
	/**
	 * 搜索
	 * @param req
	 * @return
	 */
	@RequestMapping(value="search")
	@ResponseBody
	public String search(HttpServletRequest req)	{
		PageWrapper<Advertising> wrapper = new PageWrapper<Advertising>(req);
		Page<Advertising> pager = advertisingService.findLikeAdvertiser(
				this.toParamMap(req), getOrderCol(wrapper)[0], getOrderCol(wrapper)[1], 
				wrapper.getsStart(), wrapper.getsLength());
		return this.toDataTablesJson(pager, wrapper.getsEcho()).toString();
	}
	
	private String[] getOrderCol(PageWrapper wrapper)	{
		Field orderBy, order;
		try {
			orderBy = PageWrapper.class.getDeclaredField("orderBy");
			order = PageWrapper.class.getDeclaredField("order");
			orderBy.setAccessible(true); 
			order.setAccessible(true); 
			return new String[]{(String)orderBy.get(wrapper), (String)order.get(wrapper)};
		} catch (Exception e) {
			log.error("失败", e);
			return new String[]{null, null};
		}
	}
	
	private Map<String,Object> toParamMap(HttpServletRequest req)	{
		Map<String, String[]> map = req.getParameterMap();
		Map<String, Object> result = new HashMap<String, Object>();
		for(Entry<String, String[]> e : map.entrySet())	{
			result.put(e.getKey(), e.getValue()[0]);
		}
		return result;
	}
	
	private <T> JsonObject toDataTablesJson(Page<T> pager, String sEcho)	{
		JsonObject json = new JsonObject();
		json.addProperty("sEcho", sEcho);
		json.addProperty("iTotalRecords", pager.getTotalCount());
		json.addProperty("iTotalDisplayRecords", pager.getTotalCount());
		json.add("aaData", gson.toJsonTree(pager.getResult()));
		return json;
	}
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) throws ServletException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
	
	
}
