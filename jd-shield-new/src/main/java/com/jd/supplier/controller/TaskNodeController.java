package com.jd.supplier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.common.constants.TaskConstant;
import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.modules.system.model.User;
import com.jd.supplier.enm.NodeStatusEnum;
import com.jd.supplier.enm.NodeTipsEnum;
import com.jd.supplier.enm.RoleTypeEnum;
import com.jd.supplier.enm.TaskNodeEnum;
import com.jd.supplier.model.Task;
import com.jd.supplier.model.TaskNode;
import com.jd.supplier.model.TaskNodeAssignment;
import com.jd.supplier.service.TaskNodeService;
import com.jd.supplier.service.TaskService;

@Controller
@RequestMapping("/supplier")
public class TaskNodeController {
	
	private static Logger logger = Logger.getLogger(TaskNodeController.class);
	
	@Autowired
	private TaskNodeService taskNodeService;
	
	@Autowired
	private TaskService taskService;
	
	/**
	 * 任务节点详细
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/taskNode_view")
	public String view(Long id,Model model){
		TaskNode taskNode = taskNodeService.get(id);
		model.addAttribute("task", taskNode);
		return "supplier/taskNode_view";
	}
	
	/**
	 * 任务初始化
	 * @param taskId
	 * @param model
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.add, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/taskNode_init",method=RequestMethod.POST)
	public Object initNode(Long taskId,Model model){
		if(taskId != null && taskId>0)
			taskNodeService.addInitNode(taskId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("operator", true);
		map.put("message", "添加成功");
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	/**
	 * 任务指派-供应商管理部
	 * @param taskId
	 * @param nodeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_assignment",method=RequestMethod.GET)
	public String taskAssign(Long taskId,Long nodeId,Model model){
		TaskNode taskNode = taskNodeService.findInitNode(nodeId,NodeStatusEnum.COMPLETED.getType());
		List<User> userList = null;
		if(taskNode != null){
			userList = taskNodeService.findTaskAssignUsers(RoleTypeEnum.CLAIM_ROLE.getCode());
		}
		model.addAttribute("roleCode", RoleTypeEnum.CLAIM_ROLE.getCode());
		model.addAttribute("taskNode", taskNode);
		model.addAttribute("userList", userList);
		return "supplier/assignment_add";
	}
	
	/**
	 * 任务指派-质量管理部
	 * @param taskId
	 * @param nodeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_assignment_q",method=RequestMethod.GET)
	public String taskAssignQ(Long taskId,Long nodeId,Model model){
		TaskNode taskNode = taskNodeService.findInitNode(nodeId,NodeStatusEnum.COMPLETED.getType());
		List<User> userList = null;
		if(taskNode != null){
			userList = taskNodeService.findTaskAssignUsers(RoleTypeEnum.CLAIM_Q_ROLE.getCode());
		}
		model.addAttribute("roleCode", RoleTypeEnum.CLAIM_Q_ROLE.getCode());
		model.addAttribute("taskNode", taskNode);
		model.addAttribute("userList", userList);
		return "supplier/assignment_add";
	}
	
	/**
	 * 任务指派-招标部
	 * @param taskId
	 * @param nodeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_assignment_b",method=RequestMethod.GET)
	public String taskAssignB(Long taskId,Long nodeId,Model model){
		TaskNode taskNode = taskNodeService.findInitNode(nodeId,NodeStatusEnum.COMPLETED.getType());
		List<User> userList = null;
		if(taskNode != null){
			userList = taskNodeService.findTaskAssignUsers(RoleTypeEnum.CLAIM_B_ROLE.getCode());
		}
		model.addAttribute("roleCode", RoleTypeEnum.CLAIM_B_ROLE.getCode());
		model.addAttribute("taskNode", taskNode);
		model.addAttribute("userList", userList);
		return "supplier/assignment_add";
	}
	
	/**
	 * 任务认领-供应商管理部
	 * @param taskId
	 * @param nodeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_claim",method=RequestMethod.GET)
	public String taskClaim(Long taskId,Long nodeId,Model model){
		model.addAttribute("taskId", taskId);
		model.addAttribute("nodeId", nodeId);
		model.addAttribute("roleCode",RoleTypeEnum.CLAIM_ROLE.getCode());
		return "supplier/task_claim";
	}
	
	/**
	 * 任务认领-质量部
	 * @param taskId
	 * @param nodeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_claim_q",method=RequestMethod.GET)
	public String taskClaimQ(Long taskId,Long nodeId,Model model){
		model.addAttribute("taskId", taskId);
		model.addAttribute("nodeId", nodeId);
		model.addAttribute("roleCode",RoleTypeEnum.CLAIM_Q_ROLE.getCode());
		return "supplier/task_claim";
	}
	
	/**
	 * 任务认领-招标部
	 * @param taskId
	 * @param nodeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_claim_b",method=RequestMethod.GET)
	public String taskClaimB(Long taskId,Long nodeId,Model model){
		model.addAttribute("taskId", taskId);
		model.addAttribute("nodeId", nodeId);
		model.addAttribute("roleCode",RoleTypeEnum.CLAIM_B_ROLE.getCode());
		return "supplier/task_claim";
	}
	
	/**
	 * 立项会
	 * @param taskId
	 * @param nodeId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/project_meeting",method=RequestMethod.GET)
	public String projectMeeting(Long taskId,Long nodeId,Model model){
		model.addAttribute("taskId", taskId);
		model.addAttribute("nodeId", nodeId);
		return "supplier/project_meeting";
	}
	
	/**
	 * 任务驳回
	 * @param taskNode
	 * @param model
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.update, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/task_claimReject",method=RequestMethod.POST)
	public String task_claimReject(TaskNode taskNode,Model model){
		Map<String, Object> map = new HashMap<String, Object>();
		if(taskNodeService.updateClaimReject(taskNode)){
			map.put("operator", true);
			map.put("message", NodeTipsEnum.TASK_CLAIM.getSuccess(
					TaskNodeEnum.TASK_CLAIM.getType(), 
					null,
					TaskConstant.Tips.REJECT));
		}else{
			map.put("message", NodeTipsEnum.TASK_CLAIM.getError(
					TaskNodeEnum.TASK_CLAIM.getType(), 
					null,
					TaskConstant.Tips.Error.PERMISSION
					));
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	/**
	 * 入围确认驳回
	 * @param taskNode
	 * @param model
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.update, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/finalSelectionConfirm_reject",method=RequestMethod.POST)
	public String finalSelectionConfirm_reject(TaskNode taskNode,Model model){
		Map<String, Object> map = new HashMap<String, Object>();
		if(taskNodeService.updateFinalSelectionReject(taskNode)){
			map.put("operator", true);
			map.put("message", NodeTipsEnum.FINAL_SELECTION_CONFRIM.getSuccess(
					TaskNodeEnum.FINAL_SELECTION_CONFRIM.getType(), 
					null,
					TaskConstant.Tips.REJECT));
		}else{
			map.put("message", NodeTipsEnum.FINAL_SELECTION_CONFRIM.getError(
					TaskNodeEnum.FINAL_SELECTION_CONFRIM.getType(), 
					null,
					TaskConstant.Tips.Error.PERMISSION
					));
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	/**
	 * 输入驳回原因
	 * @return
	 */
	@RequestMapping(value="/inputReason",method=RequestMethod.GET)
	public String inputReason(){
		return "supplier/input_reason";
	}
	
	/**
	 * 任务指派保存
	 * @param taskNodeAssignment
	 * @param nodeId
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.add, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/task_assignmentSave",method=RequestMethod.POST)
	public String assignmentAddSave(TaskNodeAssignment taskNodeAssignment,Long taskId){
		
		Map<String, Object> map = new HashMap<String, Object>();
		if(taskNodeService.addAssignment(taskNodeAssignment)){
			map.put("operator", true);
			map.put("message", NodeTipsEnum.TASK_ASSIGNMENT.getSuccess(
					TaskNodeEnum.TASK_ASSIGNMENT.getType(), 
					null,
					null));
		}else{
			map.put("message", NodeTipsEnum.TASK_ASSIGNMENT.getError(
					TaskNodeEnum.TASK_ASSIGNMENT.getType(), 
					null,
					TaskConstant.Tips.Error.PERMISSION
					));
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	/**
	 * 检查节点状态
	 * @param nodeId
	 * @param roleCode
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/taskNode_checkNodeStatus",method=RequestMethod.POST)
	public String checkNodeStatus(Long nodeId,String roleCode){
		Integer status = taskNodeService.checkNodeStatus(nodeId,roleCode, null);
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(status);
	}
	
	/**
	 * 跳转到需求确认页面
	 * @param attributes
	 * @param taskId
	 * @param nodeId
	 * @return
	 */
	@RequestMapping(value="/task_confirm",method=RequestMethod.GET)
	public String taskConfirm(RedirectAttributes attributes,Long taskId,Long nodeId){
		Task task = taskService.get(taskId);
		attributes.addAttribute("id", task.getRequirementId());
		attributes.addAttribute("nodeId", nodeId);
		attributes.addAttribute("taskId", taskId);
		return "redirect:/supplier/reqAduitMetting_savepage";
	}
	
	/**
	 * 跳转到专家筛选页面
	 * @param attributes
	 * @param taskId
	 * @param nodeId
	 * @return
	 */
	@RequestMapping(value="/task_expertFilter",method=RequestMethod.GET)
	public String taskExpertFilter(RedirectAttributes attributes,Long taskId,Long nodeId){
		Task task = taskService.get(taskId);
		attributes.addAttribute("id", task.getRequirementId());
		attributes.addAttribute("nodeId", nodeId);
		attributes.addAttribute("taskId", taskId);
		return "redirect:/extract/index";
	}
	
	/**
	 * 更新节点状态
	 * @param taskNode
	 * @param nType
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.update, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/taskNode_updateStatus",method=RequestMethod.POST)
	public String updateNodeStatus(TaskNode taskNode,Integer nType){
		if(taskNodeService.updateNodeStatus(taskNode)){
			TaskNode node = taskNodeService.get(taskNode.getId());
			return tipsToJson(node.getType(), NodeTipsEnum.SUCCESS.getType());
		}
		else{
			TaskNode node = taskNodeService.getByParentId(taskNode.getId());
			return tipsToJson(node.getType(), NodeTipsEnum.ERROR.getType());
		}
	}
	
	/**
	 * 将返回信息转成json
	 * @param nType
	 * @param status
	 * @return
	 */
	private String tipsToJson(Integer nType,int status){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("operator", true);
		String msg;
		if(status == NodeTipsEnum.SUCCESS.getType())
			msg = getSuccessMsg(nType);
		else
			msg = geterrorMsg(nType);
		map.put("message", msg);
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	
	/**
	 * 任务认领
	 * @param taskNodeAssignment
	 * @param nodeId
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.update, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/task_claimUpdate",method=RequestMethod.POST)
	public String taskClaimUpdate(TaskNode taskNode){
		Map<String,Object> map = new HashMap<String,Object>();
		if(taskNodeService.updateTaskClaim(taskNode)){
			map.put("operator", true);
			map.put("message", NodeTipsEnum.TASK_CLAIM.getSuccess(
					TaskNodeEnum.TASK_CLAIM.getType(), 
					null,
					null
					));
		}else{
			map.put("message", NodeTipsEnum.TASK_CLAIM.getError(
					TaskNodeEnum.TASK_CLAIM.getType(), 
					null,
					TaskConstant.Tips.Error.PERMISSION));
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	/**
	 * 入围完成
	 * @param taskNodeAssignment
	 * @param nodeId
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.update, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/finalSelection_complete",method=RequestMethod.POST)
	public String finalSelection_complete(Long id){
		Map<String,Object> map = new HashMap<String,Object>();
		try{
			if(taskNodeService.addFinalSelectionComplete(id)){
				map.put("operator", true);
				map.put("message", NodeTipsEnum.FINAL_SELECTION.getSuccess(
						TaskNodeEnum.FINAL_SELECTION.getType(), 
						null,
						null
						));
			}else{
				map.put("message", NodeTipsEnum.FINAL_SELECTION.getError(
						TaskNodeEnum.FINAL_SELECTION.getType(), 
						null,
						TaskConstant.Tips.Error.PERMISSION));
			}
		}catch(Exception e){
			logger.error(e);
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	
	/**
	 * 入围确认完成
	 * @param id
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.update, entityName="TaskNode")
	@ResponseBody
	@RequestMapping(value="/finalSelection_confirmComplete",method=RequestMethod.POST)
	public String finalSelection_confirmComplete(Long id){
		Map<String,Object> map = new HashMap<String,Object>();
		try{
			if(taskNodeService.addFinalSelectionConfirmCompleted(id)){
				map.put("operator", true);
				map.put("message", NodeTipsEnum.FINAL_SELECTION_CONFRIM.getSuccess(
						TaskNodeEnum.FINAL_SELECTION_CONFRIM.getType(), 
						null,
						null
						));
			}else{
				map.put("message", NodeTipsEnum.FINAL_SELECTION_CONFRIM.getError(
						TaskNodeEnum.FINAL_SELECTION_CONFRIM.getType(), 
						null,
						TaskConstant.Tips.Error.PERMISSION));
			}
		}catch(Exception e){
			logger.error(e);
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	/**
	 * 封装成功信息
	 * @param nType
	 * @return
	 */
	private String getSuccessMsg(Integer nType){
		switch(nType){
		/**
		 * 任务认领
		 */
		case 1:
			return NodeTipsEnum.TASK_ASSIGNMENT.getSuccess(
					TaskNodeEnum.TASK_ASSIGNMENT.getType(), 
					null,
					null);
		case 2:
			return NodeTipsEnum.TASK_CLAIM.getSuccess(
					TaskNodeEnum.TASK_CLAIM.getType(), 
					null,
					null);
		case 3:
			return NodeTipsEnum.REQUIREMENT_CONFIRM.getSuccess(
					TaskNodeEnum.REQUIREMENT_CONFIRM.getType(), 
					null,
					null);
		case 4:
			return NodeTipsEnum.PRE_SELECTION.getSuccess(
					TaskNodeEnum.PRE_SELECTION.getType(), 
					null,
					null);
		case 5:
			return NodeTipsEnum.FINAL_SELECTION.getSuccess(
					TaskNodeEnum.FINAL_SELECTION.getType(), 
					null,
					null);
		case 6:
			return NodeTipsEnum.FINAL_SELECTION_CONFRIM.getSuccess(
					TaskNodeEnum.FINAL_SELECTION_CONFRIM.getType(), 
					null, 
					null);
		case 7:
			return NodeTipsEnum.TASK_ASSIGNMENT.getSuccess(
					TaskNodeEnum.TASK_ASSIGNMENT_Q.getType(), 
					null, 
					null);
		case 8:
			return NodeTipsEnum.TASK_CLAIM.getSuccess(
					TaskNodeEnum.TASK_CLAIM_Q.getType(), 
					null, 
					null);
		case 9:
			return NodeTipsEnum.EXPERT_FILTER.getSuccess(
					TaskNodeEnum.EXPERT_FILTER.getType(), 
					null, 
					null);
		case 10:
			return NodeTipsEnum.TASK_ASSIGNMENT.getSuccess(
					TaskNodeEnum.TASK_ASSIGNMENT_B.getType(), 
					null, 
					null);
		case 11:
			return NodeTipsEnum.TASK_CLAIM.getSuccess(
					TaskNodeEnum.TASK_CLAIM_B.getType(), 
					null, 
					null);
		case 12:
			return NodeTipsEnum.PROJECT_MEETING.getSuccess(
					TaskNodeEnum.PROJECT_MEETING.getType(), 
					null, 
					null);
		}
		return null ;
	}
	
	/**
	 * 封装错误信息
	 * @param nType
	 * @return
	 */
	private String geterrorMsg(Integer nType){
		switch(nType){
		case 1:
			return NodeTipsEnum.TASK_ASSIGNMENT.getError(
					TaskNodeEnum.TASK_ASSIGNMENT.getType(), 
					null, 
					null, 
					null,
					"");
		case 2:
			return NodeTipsEnum.TASK_CLAIM.getError(
					TaskNodeEnum.TASK_CLAIM.getType(), 
					null, 
					null, 
					null,
					"");
		case 3:
			return NodeTipsEnum.REQUIREMENT_CONFIRM.getError(
					TaskNodeEnum.REQUIREMENT_CONFIRM.getType(), 
					null, 
					null, 
					null,
					"");
		case 4:
			return NodeTipsEnum.PRE_SELECTION.getError(
					TaskNodeEnum.PRE_SELECTION.getType(), 
					null, 
					null, 
					null,
					"");
		case 5:
			return NodeTipsEnum.FINAL_SELECTION.getError(
					TaskNodeEnum.FINAL_SELECTION.getType(), 
					null, 
					null, 
					null,
					"");
		case 6:
			return NodeTipsEnum.FINAL_SELECTION_CONFRIM.getError(
					TaskNodeEnum.FINAL_SELECTION_CONFRIM.getType(), 
					null, 
					null, 
					null,
					"");
		}
		return null;
	}
}
