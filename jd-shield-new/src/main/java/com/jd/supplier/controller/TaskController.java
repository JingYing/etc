package com.jd.supplier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.common.constants.DictDataConstant;
import com.jd.common.enm.DeleteEnum;
import com.jd.common.util.Bean2Utils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.supplier.enm.NodeStatusEnum;
import com.jd.supplier.enm.TaskParaEnum;
import com.jd.supplier.enm.TaskStatusEnum;
import com.jd.supplier.model.Task;
import com.jd.supplier.model.TaskNode;
import com.jd.supplier.model.TaskPara;
import com.jd.supplier.service.TaskNodeService;
import com.jd.supplier.service.TaskParaService;
import com.jd.supplier.service.TaskService;

@Controller
@RequestMapping("/supplier")
public class TaskController {
	
	private static Logger logger = Logger.getLogger(TaskController.class);
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private TaskNodeService taskNodeService;
	
	@Autowired
	private TaskParaService taskParaService;
	
	@Autowired
	private DictDataService dictDataService;
	
	/**
	 * 任务单首页
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_index",method = RequestMethod.GET)
	public String index(Model model){
		List<DictData> taskStatusList = dictDataService.findDictDataList(DictDataConstant.Task.TASK_STATUS);
		model.addAttribute("taskStatusList", taskStatusList);
		return "supplier/task_index";
	}
	
	/**
	 * 任务单详情页
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/task_view")
	public String view(Long id,Model model){
		Task task = taskService.get(id);
		task.setStatusName(getStatusName(task.getTaskStatus()));
		model.addAttribute("task", task);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("type", TaskParaEnum.ORGANIZATION.getType());
		List<TaskPara> taskOrgList = taskParaService.findParaList(map);
		Map<String,List<TaskNode>> taskNodeList = new HashMap<String,List<TaskNode>>();
		//循环机构列表封装节点集合
		for(int i=0;i<taskOrgList.size();i++){
			map.put("organizationId", taskOrgList.get(i).getId());
			map.put("type", null);
			map.put("taskId", id);
			List<TaskNode> nodeList = taskNodeService.findNodeList(map);
			int size = 0;
			if(nodeList != null){
				//每种类型排除第一个元素
				if(i==0)
					nodeList.remove(nodeList.size()-1);
				size = nodeList.size();
			}
			for(int j=0;j<size;j++){
				nodeList.get(j).setNodeStyle(findStyleByNodeStatus(nodeList.get(j).getNodeStatus()));
			}
			//已map形式返回
			//部门名称作为key
			//集合作为value
			taskNodeList.put(taskOrgList.get(i).getName(), nodeList);
		}
		model.addAttribute("taskNodeList", taskNodeList);
		return "supplier/task_view";
	}
	
	/**
	 * 通过节点状态获取对应样式
	 * @param status
	 * @return
	 */
	private static String findStyleByNodeStatus(Integer status){
		switch(status){
		case 0:
			//初始状态
			return NodeStatusEnum.INITIAL.getCode();
		case 1:
			//运行状态
			return NodeStatusEnum.RUNNING.getCode();
		case 2:
			//完成状态
			return NodeStatusEnum.COMPLETED.getCode();
		}
		return "";
	}
	
	/**
	 * 任务列表分页查询
	 * @param task
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/task_page",method = RequestMethod.POST)
	public Object page(Task task,HttpServletRequest request){
		
		PageWrapper<Task> pageWrapper = new PageWrapper<Task>(request);
		pageWrapper.addResult("returnKey","returnValue");
		if(task != null){
			task.setStatus(DeleteEnum.NO.getStatus());
			task.setRstatus(DeleteEnum.NO.getStatus());
			if(!StringUtils.isBlank(task.getStatusCode()))
				task.setTaskStatus(getStatus(task.getStatusCode()));
			Map<String,Object> map = null;
			try {
				//将实体对象转成map
				map = Bean2Utils.objToHash(task);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			taskService.find(pageWrapper.getPageBean(), "findByMap", map);
		}
		//返回到页面的数据
		Gson gson = new GsonBuilder().serializeNulls().create();
        String json = gson.toJson(pageWrapper.getResult());
        logger.debug("result:" + json); 
        return json;
	}
	
	/**
	 * 获取节点状态
	 * @param statusCode
	 * @return
	 */
	private static Integer getStatus(String statusCode){
		if(StringUtils.isBlank(statusCode))
			return null;
		statusCode = statusCode.trim();
		//任务指派
		if(statusCode.equals(TaskStatusEnum.TASK_ASSIGNMENT.getCode()))
			return TaskStatusEnum.TASK_ASSIGNMENT.getType();
		//任务认领
		if(statusCode.equals(TaskStatusEnum.TASK_CLAIM.getCode()))
			return TaskStatusEnum.TASK_CLAIM.getType();
		//需求确认
		if(statusCode.equals(TaskStatusEnum.REQUIREMENT_CONFIRM.getCode()))
			return TaskStatusEnum.REQUIREMENT_CONFIRM.getType();
		//预选
		if(statusCode.equals(TaskStatusEnum.PRE_SELECTION.getCode()))
			return TaskStatusEnum.PRE_SELECTION.getType();
		//入围
		if(statusCode.equals(TaskStatusEnum.FINAL_SELECTION.getCode()))
			return TaskStatusEnum.FINAL_SELECTION.getType();
		//入围确认
		if(statusCode.equals(TaskStatusEnum.FINAL_SELECTION_CONFIRM.getCode()))
			return TaskStatusEnum.FINAL_SELECTION_CONFIRM.getType();
		//入围确认完成
		if(statusCode.equals(TaskStatusEnum.FINAL_SELECTION_CONFIRM_CP.getCode()))
			return TaskStatusEnum.FINAL_SELECTION_CONFIRM_CP.getType();
		return -1;
	}
	
	/**
	 * 获取节点名称
	 * @param status
	 * @return
	 */
	private static String getStatusName(Integer status){
		if(status == null)
			return null;
		//任务指派
		if(status == TaskStatusEnum.TASK_ASSIGNMENT.getType())
			return TaskStatusEnum.TASK_ASSIGNMENT.getName();
		//任务认领
		if(status.equals(TaskStatusEnum.TASK_CLAIM.getType()))
			return TaskStatusEnum.TASK_CLAIM.getName();
		//需求确认
		if(status.equals(TaskStatusEnum.REQUIREMENT_CONFIRM.getType()))
			return TaskStatusEnum.REQUIREMENT_CONFIRM.getName();
		//预选
		if(status.equals(TaskStatusEnum.PRE_SELECTION.getType()))
			return TaskStatusEnum.PRE_SELECTION.getName();
		//入围
		if(status.equals(TaskStatusEnum.FINAL_SELECTION.getType()))
			return TaskStatusEnum.FINAL_SELECTION.getName();
		//入围确认
		if(status.equals(TaskStatusEnum.FINAL_SELECTION_CONFIRM.getType()))
			return TaskStatusEnum.FINAL_SELECTION_CONFIRM.getName();
		//入围确认完成
		if(status.equals(TaskStatusEnum.FINAL_SELECTION_CONFIRM_CP.getType()))
			return TaskStatusEnum.FINAL_SELECTION_CONFIRM_CP.getName();
		return "";
	}
	
}
