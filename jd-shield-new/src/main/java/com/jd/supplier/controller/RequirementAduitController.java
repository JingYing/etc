package com.jd.supplier.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.LevelService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserAuthorizationService;
import com.jd.official.modules.system.service.UserService;
import com.jd.supplier.controller.vo.FileAffixVO;
import com.jd.supplier.controller.vo.RequirementAduitVO;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.model.RequirementAduit;
import com.jd.supplier.model.RequirementAffix;
import com.jd.supplier.service.RequirementAduitService;
import com.jd.supplier.service.RequirementAffixService;
import com.jd.supplier.service.RequirementService;
import com.jd.supplier.service.RequirementTaskService;
import com.jd.supplier.util.SupplierConstant;

/**
 * @author duandongdong
 * @date 2014年4月30日
 * @desc 需求审核处理类
 */
@Controller
@RequestMapping("/supplier")
public class RequirementAduitController {
    private static final Logger logger = Logger.getLogger(RequirementAduitController.class);

    @Autowired
    private UserAuthorizationService userAuthorizationService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private DictDataService dictDataService;

    @Autowired
    private RequirementAduitService requirementAduitService;

    @Autowired
    private RequirementService requirementService;

    @Autowired
    private RequirementTaskService requirementTaskService;

    @Autowired
    private RequirementAffixService requirementAffixService;

    /**
     * @param locale
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/requirementAduit_index")
    public ModelAndView requirementAduit_expression(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/requirementAduit_index");
	return mav;
    }

    /**
     * @return
     * @desc 生成编号
     */
    private String generateProjectNum() {
	String projectNum;
	String requirementAduitIdStr;
	String prefix = "";
	int length = 0;
	Calendar cal = Calendar.getInstance();
	int year = cal.get(Calendar.YEAR);
	int requirementAduitId = requirementAduitService.getMaxRequirementAduitID();
	if (requirementAduitId == 0) {
	    requirementAduitIdStr = "000001";
	} else {
	    requirementAduitIdStr = String.valueOf(requirementAduitId);
	    if (StringUtils.isNotBlank(requirementAduitIdStr)) {
	    	length = 6 - requirementAduitIdStr.length();
	    	StringBuilder sb = new StringBuilder();
	    	for (int i = 0; i < length; i++) {
	    		sb.append("0");
	    	}
	    	prefix = sb.toString();
	    }
	    requirementAduitIdStr = prefix + requirementAduitIdStr;
	}

	projectNum = String.valueOf(year) + String.valueOf(ComUtils.getCurrentLoginUser().getOrganizationId())
		+ requirementAduitIdStr;
	return projectNum;
    }

    @RequestMapping(value = "/requirementAduit_save",method = RequestMethod.POST)
    @ResponseBody
    public Object save(@ModelAttribute RequirementAduitVO requirementAduitvo) throws Exception {
	Map<String, Object> resultMap = new HashMap<String, Object>();
	// 审核(将原来驳回的记录状态置为不可用，插入一条新记录)
	RequirementAduit requirementAduit = new RequirementAduit();
	requirementAduitService.insert(requirementAduitvo, requirementAduit, SupplierConstant.REQCONFIG);
	resultMap.put("message", "保存成功！");
	return resultMap;
    }

    // 删除对应id的需求
    @RequestMapping(value = "/requirementAduit_delete",method = RequestMethod.POST)
    @ResponseBody
    public String expression_delete(@RequestParam(value = "locale", required = false) Locale locale,
	    @RequestParam(required = false) String expressionIds) {
	String[] ids = expressionIds.split(",");
	List<RequirementAduit> undeleted = requirementAduitService.deleteOnlyUnused("deleteByPrimaryKey", ids);

	return JsonUtils.toJsonByGoogle(undeleted);
    }

    /**
     * @desc 需求查询ajax
     * @requirementAduitor duandongdong
     * @param request
     * @return
     */
    @RequestMapping(value = "/requirementAduit_page",method = RequestMethod.POST)
    @ResponseBody
    public Object requirementAduit_expression_page(HttpServletRequest request,
	    @ModelAttribute RequirementAduitVO requirementAduitvo) {
	// 创建pageWrapper
	PageWrapper<Requirement> pageWrapper = new PageWrapper<Requirement>(request);

	// 添加搜索条件
	List<String> reqStatusList = new ArrayList<String>();
	reqStatusList.add(SupplierConstant.REQCOMMIT);
	reqStatusList.add(SupplierConstant.REQREFUSE);
	pageWrapper.addSearch("reqStatusList", reqStatusList);
	pageWrapper.addSearch("status", SupplierConstant.AVAILABLESTATUS);
	if (requirementAduitvo.getSearchProjectName() != null
		&& !("").equals(requirementAduitvo.getSearchProjectName()))
	    pageWrapper.addSearch("searchProjectName", requirementAduitvo.getSearchProjectName());
	if (requirementAduitvo.getSearchProjectNum() != null && !("").equals(requirementAduitvo.getSearchProjectNum()))
	    pageWrapper.addSearch("searchProjectNum", requirementAduitvo.getSearchProjectNum());
	// 后台取值
	requirementService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());

	// 返回到页面的额外数据
	pageWrapper.addResult("returnKey", "returnValue");
	// 返回到页面的数据
	// List<RequirementAduit> names =
	// userAuthorizationService.getRequirementAduitNameListByIds(requirementAduits);
	List<RequirementAduit> names = new ArrayList<RequirementAduit>();
	pageWrapper.addResult("names", names);

	String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());

	logger.debug(json);
	return json;
    }

    /**
     * 
     * @desc Edit页
     * @requirementAduitor duandongdong
     * @param locale
     * @param expressions
     * @return
     * @throws Exception
     */

    @RequestMapping(value = "/requirementAduit_editpage")
    @ResponseBody
    public ModelAndView requirementAduit_edit(@RequestParam("id") String id, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/requirementAduit_save");
	// 封装数据字段数据
	List<DictData> isYearDatalist1 = dictDataService.findDictDataList("isYear");
	List<DictData> budgetSourceDatalist1 = dictDataService.findDictDataList("budgetSource");
	List<DictData> sampleTestDatalist1 = dictDataService.findDictDataList("sampleTest");
	List<DictData> sceneTestDatalist1 = dictDataService.findDictDataList("sceneTest");
	List<DictData> recommendLevelDatalist1 = dictDataService.findDictDataList("recommendLevel");
	Requirement requirement = (Requirement) requirementService.get(Long.valueOf(id));
	// 取附件
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("bussineId", requirement.getId());
	map.put("status", SupplierConstant.AVAILABLESTATUS);
	List<RequirementAffix> requirementAffixList1 = requirementAffixService.find("findByMap", map);
	model.addAttribute("requirement", requirement);
	model.addAttribute("isYearDatalist", isYearDatalist1);
	model.addAttribute("budgetSourceDatalist", budgetSourceDatalist1);
	model.addAttribute("sampleTestDatalist", sampleTestDatalist1);
	model.addAttribute("sceneTestDatalist", sceneTestDatalist1);
	model.addAttribute("recommendLevelDatalist", recommendLevelDatalist1);
	model.addAttribute("requirementAffixList", requirementAffixList1);
	logger.debug("requirement===" + requirement);

	return mav;// + userList.size();
    }


    @RequestMapping(value = "/requirementAduit_savepage")
    public ModelAndView requirementAduit_savepage(Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/requirementAduit_save");
	String projectNum = "";
	projectNum = generateProjectNum();
	User user = ComUtils.getCurrentLoginUser();
	user = userService.get(user.getId());
	// 封装数据字段数据
	List<DictData> isYearDatalist = dictDataService.findDictDataList("isYear");
	List<DictData> budgetSourceDatalist = dictDataService.findDictDataList("budgetSource");
	List<DictData> sampleTestDatalist = dictDataService.findDictDataList("sampleTest");
	List<DictData> sceneTestDatalist = dictDataService.findDictDataList("sceneTest");
	List<DictData> recommendLevelDatalist = dictDataService.findDictDataList("recommendLevel");
	model.addAttribute("projectNum", projectNum);
	model.addAttribute("applicantDep", user.getOrganization().getOrganizationName());
	model.addAttribute("applicant", user.getUserCode());
	model.addAttribute("projectDate", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
	model.addAttribute("isYearDatalist", isYearDatalist);
	model.addAttribute("budgetSourceDatalist", budgetSourceDatalist);
	model.addAttribute("sampleTestDatalist", sampleTestDatalist);
	model.addAttribute("sceneTestDatalist", sceneTestDatalist);
	model.addAttribute("recommendLevelDatalist", recommendLevelDatalist);
	return mav;// + userList.size();
    }

    @RequestMapping(value = "/reqAduitMetting_savepage")
    @ResponseBody
    public ModelAndView reqAduitMetting_savepage(@RequestParam("id") String id, HttpServletRequest request, Model model)
	    throws Exception {
	ModelAndView mav = new ModelAndView("supplier/reqAduitMetting_save");
	// 封装数据字段数据
	List<DictData> isYearDatalist = dictDataService.findDictDataList("isYear");
	List<DictData> budgetSourceDatalist = dictDataService.findDictDataList("budgetSource");
	List<DictData> sampleTestDatalist = dictDataService.findDictDataList("sampleTest");
	List<DictData> sceneTestDatalist = dictDataService.findDictDataList("sceneTest");
	List<DictData> recommendLevelDatalist = dictDataService.findDictDataList("recommendLevel");
	Requirement requirement = (Requirement) requirementService.get(Long.valueOf(id));
	// 取附件
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("bussineId", requirement.getId());
	map.put("status", SupplierConstant.AVAILABLESTATUS);
	map.put("bussineType", SupplierConstant.BUSSINETYPE_REQCOMMIT);
	List<RequirementAffix> requirementAffixList = requirementAffixService.find("findByMap", map);

	Map<String, Object> reqAduitMettingMap = new HashMap<String, Object>();
	reqAduitMettingMap.put("bussineId", requirement.getId());
	reqAduitMettingMap.put("status", SupplierConstant.AVAILABLESTATUS);
	reqAduitMettingMap.put("bussineType", SupplierConstant.BUSSINETYPE_REQADUITMETTING);
	List<RequirementAffix> reqAduitMettingAffixList = requirementAffixService.find("findByMap", reqAduitMettingMap);
	// 取审核信息
	RequirementAduit requirementAduit = new RequirementAduit();
	Map<String, Object> aduitMap = new HashMap<String, Object>();
	aduitMap.put("requirementId", requirement.getId());
	aduitMap.put("status", SupplierConstant.AVAILABLESTATUS);
	List<RequirementAduit> requirementAduitList = requirementAduitService.find("findByMap", aduitMap);
	if (requirementAduitList != null && requirementAduitList.size() > 0) {
	    requirementAduit = requirementAduitList.get(0);
	}
	// 获取nodeId,taskId
	String nodeId=request.getParameter("nodeId");
	String taskId=request.getParameter("taskId");
	model.addAttribute("requirement", requirement);
	model.addAttribute("isYearDatalist", isYearDatalist);
	model.addAttribute("budgetSourceDatalist", budgetSourceDatalist);
	model.addAttribute("sampleTestDatalist", sampleTestDatalist);
	model.addAttribute("sceneTestDatalist", sceneTestDatalist);
	model.addAttribute("recommendLevelDatalist", recommendLevelDatalist);
	model.addAttribute("requirementAffixList", requirementAffixList);
	model.addAttribute("reqAduitMettingAffixList", reqAduitMettingAffixList);
	model.addAttribute("requirementAduit", requirementAduit);
	model.addAttribute("taskId", taskId);
	model.addAttribute("nodeId", nodeId);
	logger.debug("requirement===" + requirement);

	return mav;// + userList.size();
    }

    @RequestMapping(value = "/reqAduitMetting_save",method = RequestMethod.POST)
    @ResponseBody
    public Object saveReqAduitMetting(@ModelAttribute RequirementAduitVO requirementAduitvo,
	    @RequestParam("nodeId") String nodeId) throws Exception {
	FileAffixVO fileAffixvo = setFileAffixAttr(requirementAduitvo);
	requirementAduitService.insertReqAduitMetting(requirementAduitvo, fileAffixvo);
	return nodeId;
    }

    private FileAffixVO setFileAffixAttr(RequirementAduitVO requirementAduitvo) {
	FileAffixVO fileAffixvo = new FileAffixVO();
	fileAffixvo.setFileName1(requirementAduitvo.getFileName1());
	fileAffixvo.setFileName2(requirementAduitvo.getFileName2());
	fileAffixvo.setFileType(new String[2]);
	return fileAffixvo;
    }

    /**
     * @param locale
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/requirementReject_savePage")
    public ModelAndView reqAduitReject_savePage(@RequestParam("requirementId") String requirementId, Model model)
	    throws Exception {
	ModelAndView mv = new ModelAndView("supplier/requirementReject_save");
	model.addAttribute("requirementId", requirementId);
	return mv;
    }

    @RequestMapping(value = "/requirementReject_save",method = RequestMethod.POST)
    @ResponseBody
    public Object requirementRejectsave(@ModelAttribute RequirementAduitVO requirementAduitvo) throws Exception {
	Map<String, Object> resultMap = new HashMap<String, Object>();
	RequirementAduit requirementAduit = new RequirementAduit();
	requirementAduitService.insert(requirementAduitvo, requirementAduit, SupplierConstant.REQREFUSE);
	resultMap.put("message", "保存成功！");
	return resultMap;
    }

    @RequestMapping(value = "/requirementAduitHistory")
    public ModelAndView requirementAduitHistory(@RequestParam("requirementId") String requirementId, Model model)
	    throws Exception {
	Map<String, Object> aduitMap = new HashMap<String, Object>();
	aduitMap.put("requirementId", requirementId);
	aduitMap.put("Status", SupplierConstant.AVAILABLESTATUS);
	List<RequirementAduit> requirementAduitList = requirementAduitService.find("findByMap", aduitMap);
	ModelAndView mv = new ModelAndView("supplier/requirementAduitHistory");
	model.addAttribute("requirementAduitList", requirementAduitList);
	return mv;
    }

}
