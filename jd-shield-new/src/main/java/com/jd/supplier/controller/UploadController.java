package com.jd.supplier.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.jcloud.jss.exception.StorageClientException;
import com.jd.official.core.jss.service.JssService;
import com.jd.official.core.utils.IdUtils;

/**
 * @author duandongdong
 * @date 2014年5月4日
 * @desc 上传文件类
 */
public class UploadController extends HttpServlet {
	

    /**
	 * 
	 */
	private static final long serialVersionUID = 1175188384647683578L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	ServletContext application = getServletContext();  
        WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(application);// 获取spring的context  
        JssService jssService = (JssService) wac.getBean("jssService");
    	response.setCharacterEncoding("utf-8");
    	DiskFileItemFactory fac = new DiskFileItemFactory();
    	ServletFileUpload upload = new ServletFileUpload(fac);
    	upload.setHeaderEncoding("utf-8");
    	List<FileItem> fileList = null;
    	try {
    	    fileList = upload.parseRequest(request);
    	} catch (FileUploadException ex) {
    	    ex.printStackTrace();
    	}
    	Iterator<FileItem> it = null;
    	if(fileList != null){
    		it = fileList.iterator();
    		String name = null;
    		String extName = null;
//    	String category = "";
//    	String time = "";
    		while (it.hasNext()) {
    			FileItem item = it.next();
    			if (!item.isFormField()) {
    				name = item.getName();
    				if (name == null || name.trim().equals("")) {
    					continue;
    				}
    				if (name.lastIndexOf(".") >= 0) {
    					extName = name.substring(name.lastIndexOf("."));
    				}
    				try {
    					String bucketName = "jd.sheild.doc";
    					jssService.getJssService();
    					String uuid=IdUtils.uuid2();
    					InputStream inputStream=item.getInputStream();
    					jssService.uploadFile(bucketName,uuid,inputStream);
//    				File f= jssService.getFile(bucketName, uuid);
//    				String a=  f.getName();
    					name= new StringBuffer(name).append("@@").append(uuid).toString();
    					response.getWriter().print(name);
    				} catch (StorageClientException e) {
    					response.getWriter().print("0");
    				} catch (Exception e) {
    					response.getWriter().print("0");
    					e.printStackTrace();
    				}
    				
    			}
    		}
    	}

    }
}