package com.jd.supplier.controller.vo;

public class SpecialSupplierVO {
	/**
	 * 供应商名称
	 */
	private String name;

	/**
	 * 联系电话
	 */
    private String mobile;
    
    /**
     * 联系人
     */
    private String contactor;
    
    /**
     * 备注
     */
    private String remarks;
    
    /**
     * 任务id
     */
    private Long taskId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getContactor() {
		return contactor;
	}

	public void setContactor(String contactor) {
		this.contactor = contactor;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}
