package com.jd.supplier.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.official.core.utils.DateUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.service.LevelService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserService;
import com.jd.official.modules.system.service.UserAuthorizationService;
import com.jd.supplier.controller.vo.NoticeVO;
import com.jd.supplier.model.Notice;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.service.NoticeService;
import com.jd.supplier.service.RequirementService;
import com.jd.supplier.util.SupplierConstant;

@Controller
@RequestMapping("/supplier")
public class NoticeController {
    private static final Logger logger = Logger.getLogger(NoticeController.class);

    @Autowired
    private UserAuthorizationService userAuthorizationService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private DictDataService dictDataService;
    
    @Autowired
    private RequirementService requirementService;

  

    @RequestMapping(value = "/bidNotice_index")
    public ModelAndView bidNotice(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/bidNotice_index");
	return mav;
    }
    
    @RequestMapping(value = "/selectedNotice_index")
    public ModelAndView selectedNotice(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/selectedNotice_index");
	return mav;
    }
    
    @RequestMapping(value = "/winBidNotice_index")
    public ModelAndView winBidNotice(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/winBidNotice_index");
	return mav;
    }

    @RequestMapping(value = "/bidNotice_save",method = RequestMethod.POST)
    @ResponseBody
    public Object save(@ModelAttribute NoticeVO noticevo) throws Exception {
	return noticeSave(noticevo, "1", "保存成功！");
    }
    
    
    @RequestMapping(value = "/selectedNotice_save",method = RequestMethod.POST)
    @ResponseBody
    public Object selectedNoticeSave(@ModelAttribute NoticeVO noticevo) throws Exception {
	return noticeSave(noticevo, "2", "保存成功！");
    }
    
    
    @RequestMapping(value = "/winBidNotice_save",method = RequestMethod.POST)
    @ResponseBody
    public Object winBidNoticeSave(@ModelAttribute NoticeVO noticevo) throws Exception {
	return noticeSave(noticevo, "3", "保存成功！");
    }
    

    /**
     * author duandongdong
     * date 2014年5月19日
     * desc type:类型表示为哪种类型公告
     */
    private void setNoticeAttr(NoticeVO noticevo, Notice notice,String type) {
	if (noticevo.getId() != null && !noticevo.getId().equals("")) {
	    notice.setId(Long.valueOf(noticevo.getId()));
	} else {
	    notice.setId(null);
	}
	notice.setType(type);
	notice.setTitle(noticevo.getTitle());
	notice.setBiddate(DateUtils.convertFormatDateString_yyyy_MM_dd(noticevo.getBiddate()));
	notice.setBidprincipal(noticevo.getBidprincipal());
	notice.setEmail(noticevo.getEmail());
	notice.setEnddate(DateUtils.convertFormatDateString_yyyy_MM_dd(noticevo.getEnddate()));
	notice.setPhone(noticevo.getPhone());
	notice.setPublishdate(DateUtils.convertFormatDateString_yyyy_MM_dd(noticevo.getPublishdate()));
	if(noticevo.getRequirementId()!=null&&!noticevo.getRequirementId().equals("")){
	    notice.setRequirementId(Long.valueOf(noticevo.getRequirementId()));
	}
	notice.setTelephone(noticevo.getTelephone());
	notice.setTitle(noticevo.getTitle());
	notice.setRequirementName(noticevo.getRequirementName());
	notice.setContent(noticevo.getContent());
	if(noticevo.getSupplierId()!=null&&!noticevo.getSupplierId().equals("")){
	    notice.setSupplierId(Long.valueOf(noticevo.getSupplierId()));
	}
	
	notice.setSupplierName(noticevo.getSupplierName());
    }

   
    @RequestMapping(value = "/notice_page",method = RequestMethod.POST)
    @ResponseBody
    public Object noticePage(HttpServletRequest request, @ModelAttribute NoticeVO noticevo) {
	// 创建pageWrapper
	PageWrapper<Notice> pageWrapper = new PageWrapper<Notice>(request);

	// 添加搜索条件
	String type=request.getParameter("type");
	pageWrapper.addSearch("type", type);
	pageWrapper.addSearch("status", SupplierConstant.AVAILABLESTATUS);
	if (noticevo.getSearchTitleName() != null && !("").equals(noticevo.getSearchTitleName()))
	    pageWrapper.addSearch("searchTitleName", noticevo.getSearchTitleName());
	// 后台取值
	noticeService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());

	// 返回到页面的额外数据
	pageWrapper.addResult("returnKey", "returnValue");
	// 返回到页面的数据
//	List<Notice> notices = (List<Notice>) pageWrapper.getResult().get("aaData");
	// List<Notice> names =
	// userAuthorizationService.getNoticeNameListByIds(notices);
	List<Notice> names = new ArrayList<Notice>();
	pageWrapper.addResult("names", names);

	String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());

	logger.debug(json);
	return json;
    }

   
    @RequestMapping(value = "/bidNotice_editpage")
    @ResponseBody
    public ModelAndView bidNoticeEdit(@RequestParam("id") String id, Model model) throws Exception {
	return noticeEditPage("supplier/bidNotice_edit", model, id);
    }
    
    @RequestMapping(value = "/selectedNotice_editpage")
    @ResponseBody
    public ModelAndView selectedNoticeEdit(@RequestParam("id") String id, Model model) throws Exception {
	return noticeEditPage("supplier/selectedNotice_edit", model, id);// + userList.size();// + userList.size();
    }
    
    @RequestMapping(value = "/winBidNotice_editpage")
    @ResponseBody
    public ModelAndView winBidNoticeEdit(@RequestParam("id") String id, Model model) throws Exception {
	return noticeEditPage("supplier/winBidNotice_edit", model, id);// + userList.size();
    }
    
    
    @RequestMapping(value = "/bidNotice_update",method = RequestMethod.POST)
    @ResponseBody
    public Object bidNoticeUpdate(@ModelAttribute NoticeVO noticevo) throws Exception {
	return noticeUpdate(noticevo, "1", "保存成功！");
    }
    
    
    @RequestMapping(value = "/selectedNotice_update",method = RequestMethod.POST)
    @ResponseBody
    public Object selectedNoticeUpdate(@ModelAttribute NoticeVO noticevo) throws Exception {
	return noticeUpdate(noticevo, "2", "保存成功！");
    }
    
    
    @RequestMapping(value = "/winBidNotice_update",method = RequestMethod.POST)
    @ResponseBody
    public Object winBidNoticeUpdate(@ModelAttribute NoticeVO noticevo) throws Exception {
	return noticeUpdate(noticevo, "3", "保存成功！");
    }

    @RequestMapping(value = "/bidNotice_savepage")
    public ModelAndView bidNoticeSavePage(Model model) throws Exception {
	return noticeSavePage(model, "supplier/bidNotice_save");
    }
    
    @RequestMapping(value = "/selectedNotice_savepage")
    public ModelAndView selectedNoticeSavePage(Model model) throws Exception {
	return noticeSavePage(model, "supplier/selectedNotice_save");
    }
    
    
    @RequestMapping(value = "/winBidNotice_savepage")
    public ModelAndView winBidNoticeSavePage(Model model) throws Exception {
	return noticeSavePage(model, "supplier/winBidNotice_save");
    }
    
    @RequestMapping(value = "/notice_delete",method = RequestMethod.POST)
    @ResponseBody
    public Object deleteOne( String id,HttpServletRequest request) throws Exception {
	Map<String, Object> resultMap = new HashMap<String, Object>();
	noticeService.delete(Long.valueOf(id));
	resultMap.put("message", "已取消！");
	return resultMap;
    }
    
    /**
     * 抽象保存方法
     * @param noticevo
     * @param type
     * @param msg
     * @return
     */
    private Map<String,Object> noticeSave(NoticeVO noticevo,String type,String msg){
    	Notice notice = new Notice();
    	setNoticeAttr(noticevo, notice,type);
    	noticeService.insert(notice);
    	return getResultMap("message",msg);
    }
    
    private Map<String, Object> noticeUpdate(NoticeVO noticevo,String type,String msg){
    	Notice notice = new Notice();
    	setNoticeAttr(noticevo, notice,type);
    	noticeService.update(notice);
    	return getResultMap("message",msg);
    }
    
    private Map<String, Object> getResultMap(String key,String msg){
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	resultMap.put(key, msg);
    	return resultMap;
    }
    
    private ModelAndView noticeEditPage(String url,Model model,String id){
    	ModelAndView mav = new ModelAndView(url);
    	Notice notice =(Notice) noticeService.get(Long.valueOf(id));
    	Map<String ,Object> requirementMap=new HashMap<String ,Object>();
    	List<String> reqStatusList = new ArrayList<String>();
    	reqStatusList.add(SupplierConstant.REQCONFIG);
    	requirementMap.put("status", SupplierConstant.AVAILABLESTATUS);
    	requirementMap.put("reqStatusList",reqStatusList);
    	List<Requirement> requirementList=requirementService.find("findByMap",requirementMap);
    	model.addAttribute("requirementList", requirementList);
    	model.addAttribute("notice", notice);
    	logger.debug("notice===" + notice);
    	return mav;
    }
    
    private ModelAndView noticeSavePage(Model model,String url){
    	ModelAndView mav = new ModelAndView(url);
    	List<String> reqStatusList = new ArrayList<String>();
    	reqStatusList.add(SupplierConstant.REQCONFIG);
    	Map<String ,Object> requirementMap=new HashMap<String ,Object>();
    	requirementMap.put("status", SupplierConstant.AVAILABLESTATUS);
    	requirementMap.put("reqStatusList",reqStatusList);
    	List<Requirement> requirementList=requirementService.find("findByMap",requirementMap);
    	model.addAttribute("requirementList", requirementList);
    	return mav;
    }

}
