package com.jd.supplier.controller.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;

public abstract class BaseController {
	
	@Autowired
	private DictDataService dictDataService;

	//根据字典code获取数据字典列表
	protected List<DictData> getDictDataListByDictCode(String code){
		return dictDataService
				.findDictDataList(code);
	}
}
