package com.jd.supplier.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.common.constants.DictDataConstant;
import com.jd.common.enm.DeleteEnum;
import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.common.util.Bean2Utils;
import com.jd.common.util.StringHandler;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.supplier.enm.QualifiedEnum;
import com.jd.supplier.enm.SupplierSelectionEnum;
import com.jd.supplier.enm.WillingEnum;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.service.TaskSuppSelectionService;

@Controller
@RequestMapping("/supplier")
public class FinalSelectionController {
	
	private static Logger logger = Logger.getLogger(FinalSelectionController.class);

	@Autowired
	private TaskSuppSelectionService taskSuppSelectionService;
	
	@Autowired
	private DictDataService dictDataService;
	
	/**
	 * 入围主界面
	 * @param model
	 * @param taskId
	 * @param nodeId
	 * @return
	 */

    @RequestMapping(value = "/finalSelection_index", method = RequestMethod.GET)
    public String index(Model model,Long taskId,Long nodeId) {
    	model.addAttribute("taskId", taskId);
    	model.addAttribute("nodeId", nodeId);
        return "supplier/finalSelection_index";
    }
    
    /**
	 * 入围完成后主界面
	 * @param model
	 * @param taskId
	 * @param nodeId
	 * @return
	 */
    @RequestMapping(value = "/finalSelection_index_cp", method = RequestMethod.GET)
    public String index_cp(Model model,Long taskId,Long nodeId) {
    	model.addAttribute("taskId", taskId);
    	model.addAttribute("nodeId", nodeId);
        return "supplier/finalSelection_index_cp";
    }
    
    /**
     * 入围确认界面
     * @param model
     * @param taskId
     * @param nodeId
     * @return
     */
    @RequestMapping(value = "/finalSelection_confirm", method = RequestMethod.GET)
    public String confirm(Model model,Long taskId,Long nodeId) {
    	//暂无数据 所以传入一个固定值
    	model.addAttribute("taskId", taskId);
    	model.addAttribute("nodeId", nodeId);
        return "supplier/finalSelection_confirm";
    }

    /**
     * 入围分页列表
     * @param request
     * @param taskSuppSelection
     * @return
     */
    @RequestMapping(value = "/finalSelection_page",method = RequestMethod.POST)
    @ResponseBody
    public Object page(HttpServletRequest request, TaskSuppSelection taskSuppSelection) {

        //创建pageWrapper
        PageWrapper<TaskSuppSelection> pageWrapper = new PageWrapper<TaskSuppSelection>(request);
        //添加搜索条件
//        pageWrapper.addSearch("taskId", taskSuppSelection.getTaskId());
        if(taskSuppSelection != null){
        	taskSuppSelection.setFinalFlag(SupplierSelectionEnum.FINAL.getType());
        	taskSuppSelection.setEnterpriseName(StringHandler.handleName(taskSuppSelection.getEnterpriseName()));
        	taskSuppSelection.setContent(StringHandler.handleName(taskSuppSelection.getContent()));
        	
        	Map<String,Object> map = null;
        	try {
        		map = Bean2Utils.objToHash(taskSuppSelection);
        	} catch (IllegalArgumentException e) {
        		e.printStackTrace();
        	} catch (IllegalAccessException e) {
        		e.printStackTrace();
        	}
        	//后台取值
        	taskSuppSelectionService.find(pageWrapper.getPageBean(), "findByMap", map);
        	pageWrapper.getPageBean().getResult();
        	pageWrapper.addResult("returnKey","returnValue");
        }
        //返回到页面的数据
        String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug("result:" + json);
        return json;

    }

    /**
     * 添加入围名单界面
     * @param model
     * @param taskId
     * @param nodeId
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.add, entityName="TaskSuppSelection")
    @RequestMapping(value = "/finalSelection_add", method = RequestMethod.GET)
    public String add(Model model,Long taskId,Long nodeId) {
    	List<DictData> areaList1 = dictDataService.findDictDataList(DictDataConstant.Supplier.AREA),
    			qualificationList1 = dictDataService.findDictDataList(DictDataConstant.Supplier.QUALIFICATION),
    			currencyList1 = dictDataService.findDictDataList(DictDataConstant.Supplier.CURRENCY),
    			enterpiseTypeList1 = dictDataService.findDictDataList(DictDataConstant.Supplier.ENTERPRISE);
    	model.addAttribute("areaList",areaList1);
    	model.addAttribute("currencyList",currencyList1);
    	model.addAttribute("enterpiseTypeList",enterpiseTypeList1);
    	model.addAttribute("qualificationList",qualificationList1);
    	model.addAttribute("taskId", taskId);
    	model.addAttribute("nodeId", nodeId);
        return "supplier/finalSelection_add";
    }

    /**
     * 供应商详细信息
     * @param id
     * @param model
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.update, entityName="TaskSuppSelection")
    @RequestMapping(value = "/finalSelection_update", method = RequestMethod.GET)
    public String showSupplier(Long id, Model model) {
    	TaskSuppSelection taskSuppSelection = taskSuppSelectionService.get(id);
        model.addAttribute("taskSuppSelection", taskSuppSelection);
        model.addAttribute("qualifiedEnum",QualifiedEnum.values());
        model.addAttribute("willingEnum",WillingEnum.values());
        return "supplier/finalSelection_update";
    }
    

    /**
     * 入围新增保存
     * @param taskSuppSelection
     * @param supplierIds
     * @param request
     * @return
     */
    
    @RecordLog(operationType=OperationTypeValue.add, entityName="TaskSuppSelection")
    @RequestMapping(value = "/finalSelection_addSave",method = RequestMethod.POST)
    @ResponseBody
    public String addSave(TaskSuppSelection taskSuppSelection,String supplierIds,HttpServletRequest request) {
    	if(taskSuppSelection != null){
    		taskSuppSelectionService.saveFinalSelection(taskSuppSelection, supplierIds);
    		Map<String, Object> map = new HashMap<String, Object>();
    		map.put("operator", true);
    		map.put("message", "添加成功");
    		Gson gson = new GsonBuilder().serializeNulls().create();
    		return gson.toJson(map);
    		
    	}
    	return null;
    }
    

    /**
     * 入围修改保存
     * @param taskSuppSelection
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.update, entityName="TaskSuppSelection")
    @RequestMapping(value = "/finalSelection_updateSave", method = RequestMethod.POST)
    @ResponseBody
    public String finalSelection_updateSave(TaskSuppSelection taskSuppSelection) {
        Map<String, Object> map = new HashMap<String, Object>();
        taskSuppSelection.setStatus(DeleteEnum.NO.getStatus());
        taskSuppSelection.setFinalFlag(SupplierSelectionEnum.FINAL.getType());
        taskSuppSelectionService.update(taskSuppSelection);
        map.put("operator", true);
        map.put("message", "添加成功");
        return new Gson().toJson(map);
    }

    /**
     * 入围删除
     * @param taskSuppSelection
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.delete, entityName="TaskSuppSelection")
    @RequestMapping(value = "/finalSelection_delete",method = RequestMethod.POST)
    @ResponseBody
    public String deletetaskSuppSelection(TaskSuppSelection taskSuppSelection) {
        Map<String, Object> map = new HashMap<String, Object>();
        taskSuppSelection.setFinalFlag(SupplierSelectionEnum.PREVIOUS.getType());
        taskSuppSelectionService.update(taskSuppSelection);
        map.put("operator", true);
        map.put("message", "删除成功");
        return new Gson().toJson(map);
    }



    /**
     * 进入入围详细界面
     * @param id
     * @param model
     * @return
     */

    @RequestMapping(value = "/finalSelection_view", method = RequestMethod.GET)
    public String taskSuppSelectionView(Long id, Model model) {
        TaskSuppSelection taskSuppSelection = taskSuppSelectionService.get(id);
        model.addAttribute("taskSuppSelection", taskSuppSelection);
        return "supplier/finalSelection_view";
    }
    
    
    
    /**
     * 更加需求id查找入围列表
     * @param requirementId
     * @return
     */
    @RequestMapping(value = "/findFinalSelectedByReqId",method = RequestMethod.POST)
    @ResponseBody
    public List<Supplier> findFinalSelectedByReqId( @RequestParam("requirementId") String requirementId) {
	List <Supplier> taskSuppSelectionList=new ArrayList<Supplier>();
	if(requirementId!=null&&!requirementId.equals("")){
	    taskSuppSelectionList=taskSuppSelectionService.findTaskSuppSelectionListByReqId(Long.valueOf(requirementId));
	}
	return taskSuppSelectionList;
    }
    
    /**
     * 检查入围是否完成
     * @param taskId
     * @return
     */
    @RequestMapping(value="/finalSelection_checkFinish",method = RequestMethod.POST)
    @ResponseBody
    public String checkFinish(Long taskId){
    	Integer count = taskSuppSelectionService.findSelectionCount(taskId, SupplierSelectionEnum.FINAL.getType());
    	Map<String, Object> map = new HashMap<String, Object>();
        map.put("count", count);
        return new Gson().toJson(map);
    }
}
