package com.jd.supplier.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.official.core.utils.PageWrapper;
import com.jd.supplier.enm.TaskParaEnum;
import com.jd.supplier.model.TaskPara;
import com.jd.supplier.service.TaskParaService;
/**
 * 任务单参数配置类
 * @author wangchanghui
 *
 */
@Controller
@RequestMapping("/supplier")
public class TaskParaController {
	
	private Logger logger = Logger.getLogger(TaskController.class);
	
	@Autowired
	private TaskParaService taskParaService;
	
	/**
	 * 参数添加
	 * @param type
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/taskPara_add",method=RequestMethod.GET)
	public String add(Integer type,Model model){
		Map<String,Object> map = new HashMap<String,Object> ();
		map.put("type", type);
		model.addAttribute("taskParaEnum", TaskParaEnum.values());
		model.addAttribute("type", type);
		return "supplier/taskPara_add";
	}
	
	/**
	 * 参数添加保存
	 * @param taskPara
	 * @return
	 */
	@RequestMapping(value="/taskPara_addSave",method=RequestMethod.POST)
	@ResponseBody
	public Object addSave(TaskPara taskPara){
		taskParaService.insert(taskPara);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("operator", true);
		map.put("message", "添加成功");
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
	/**
	 * 节点集合
	 * @return
	 */
	@RequestMapping(value="/taskPara_nodeList",method=RequestMethod.GET)
	public String taskIndex(){
		Map<String,Object> map = new HashMap<String,Object> ();
		map.put("type", TaskParaEnum.TASK.getType());
		taskParaService.findParaList(map);
		return "supplier/taskPara_nodeList";
	}
	
	/**
	 * 添加节点
	 * @return
	 */
	@RequestMapping(value="/taskPara_addNode",method=RequestMethod.GET)
	public String addNode(){
		Map<String,Object> map = new HashMap<String,Object> ();
		map.put("type", TaskParaEnum.TASK.getType());
		taskParaService.findParaList(map);
		return "supplier/taskPara_add";
	}
	
	/**
	 * 添加角色
	 * @return
	 */
	@RequestMapping(value="/taskPara_addRole",method=RequestMethod.GET)
	public String addRole(){
		Map<String,Object> map = new HashMap<String,Object> ();
		map.put("type", TaskParaEnum.ROLE.getType());
		taskParaService.findParaList(map);
		return "supplier/taskPara_add";
	}
	
	/**
	 * 添加机构
	 * @return
	 */
	@RequestMapping(value="/taskPara_addOrganization",method=RequestMethod.GET)
	public String addOrganization(){
		Map<String,Object> map = new HashMap<String,Object> ();
		map.put("type", TaskParaEnum.ORGANIZATION.getType());
		taskParaService.findParaList(map);
		return "supplier/taskPara_add";
	}
	
	/**
	 * 分页
	 * @param request
	 * @param taskPara
	 * @param taskId
	 * @return
	 */
	@RequestMapping(value = "/taskPara_page",method = RequestMethod.POST)
    @ResponseBody
    public Object page(HttpServletRequest request, TaskPara taskPara,Long taskId) {

        //创建pageWrapper
        PageWrapper<TaskPara> pageWrapper = new PageWrapper<TaskPara>(request);
        //添加搜索条件
        pageWrapper.addSearch("taskId", taskId);
        pageWrapper.addSearch("name", taskPara.getName());
        if(StringUtils.isNotBlank(taskPara.getTypeName()))
        	pageWrapper.addSearch("type",getTypeId(taskPara.getTypeName().trim()));
        else
        	pageWrapper.addSearch("type",TaskParaEnum.TASK.getType());

        //后台取值
        taskParaService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());//pageWrapper.getConditionsMap()
        pageWrapper.addResult("returnKey","returnValue");
        //返回到页面的数据
        Gson gson = new GsonBuilder().serializeNulls().create();
        String json = gson.toJson(pageWrapper.getResult());
        logger.debug("result:" + json); 
        return json;

    }
	
	/**
	 * 根据类型名称查找类型id
	 * @param typeName
	 * @return
	 */
	private Integer getTypeId(String typeName){
		//任务
		if(typeName.equals(TaskParaEnum.TASK.getName())){
			return TaskParaEnum.TASK.getType();
		}
		//角色
		if(typeName.equals(TaskParaEnum.ROLE.getName())){
			return TaskParaEnum.ROLE.getType();
		}
		//部门
		if(typeName.equals(TaskParaEnum.ORGANIZATION.getName())){
			return TaskParaEnum.ORGANIZATION.getType();
		}
		return null;
	}
}
