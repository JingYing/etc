package com.jd.supplier.controller.vo;

import java.util.Arrays;

/**
 * @author duandongdong
 * @date 2014年4月29日
 * @desc 需求vo，用于自动封装表单数据
 */
public class RequirementVO {

	private String id;

	private String creator;

	private String createTime;

	private String modifier;

	private String modifyTime;

	private String requirementYearId;

	private String projectNum;
	
	private String projectDate;

	private String projectName;

	private String isYear;

	private String budgetSource;

	private String budgetAmount;

	private String outsideReason;

	private String sampleTest;

	private String sceneTest;

	private String recommendLevel;

	private String outsideFileName;

	private String outsideFileKey;

	private String yearFileName;

	private String yearFileKey;

	private String testFileName;

	private String testFileKey;

	private String tecFileName;

	private String tecFileKey;

	private String otherFileName;

	private String otherFileKey;

	private String technical;

	private String aptitude;

	private String standard;

	private String address;

	private String deliveryDate;

	private String payMethod;

	private String serviceRequire;

	private String cooperationDetail;

	private String supplementInfo;

	private String depSuggestion;

	private String applicant;

	private String applicantDep;

	private String applicantDepId;

	private String applicantTel;

	private String demandStatus;

	private String examineStatus;

	private String mark;

	private String status;
	
	private String searchProjectName;
	
	private String searchProjectNum;
	
	private String[] fileType;
	
	private String fileName1;
	
	private String fileName2;
	
	private String fileName3;
	
	private String fileName4;
	
	private String fileName5;

	private String reqStatus;


	public String getFileName3() {
	    return fileName3;
	}

	public void setFileName3(String fileName3) {
	    this.fileName3 = fileName3;
	}

	public String getFileName4() {
	    return fileName4;
	}

	public void setFileName4(String fileName4) {
	    this.fileName4 = fileName4;
	}

	public String getFileName5() {
	    return fileName5;
	}

	public void setFileName5(String fileName5) {
	    this.fileName5 = fileName5;
	}

	public String getFileName2() {
	    return fileName2;
	}

	public void setFileName2(String fileName2) {
	    this.fileName2 = fileName2;
	}

	public void setFileName1(String fileName1) {
	    this.fileName1 = fileName1;
	}

	public String getFileName1() {
	    return fileName1;
	}
	
	public String[] getFileType() {
	    return fileType == null ? null : Arrays.copyOf(fileType, fileType.length);
	}

	public void setFileType(String[] fileType) {
		if(fileType != null){
			this.fileType = Arrays.copyOf(fileType, fileType.length);
		}else
			this.fileType = null;
	}

	public String getId() {
		return id;
	}

	public String getReqStatus() {
	    return reqStatus;
	}

	public void setReqStatus(String reqStatus) {
	    this.reqStatus = reqStatus;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSearchProjectName() {
	    return searchProjectName;
	}

	public void setSearchProjectName(String searchProjectName) {
	    this.searchProjectName = searchProjectName;
	}

	public String getSearchProjectNum() {
	    return searchProjectNum;
	}

	public void setSearchProjectNum(String searchProjectNum) {
	    this.searchProjectNum = searchProjectNum;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getRequirementYearId() {
		return requirementYearId;
	}


	public void setRequirementYearId(String requirementYearId) {
		this.requirementYearId = requirementYearId;
	}

	public String getProjectNum() {
		return projectNum;
	}

	public void setProjectNum(String projectNum) {
		this.projectNum = projectNum;
	}

	public String getProjectDate() {
		return projectDate;
	}

	public void setProjectDate(String projectDate) {
		this.projectDate = projectDate;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getIsYear() {
		return isYear;
	}

	public void setIsYear(String isYear) {
		this.isYear = isYear;
	}

	public String getBudgetSource() {
		return budgetSource;
	}

	public void setBudgetSource(String budgetSource) {
		this.budgetSource = budgetSource;
	}

	public String getBudgetAmount() {
		return budgetAmount;
	}

	public void setBudgetAmount(String budgetAmount) {
		this.budgetAmount = budgetAmount;
	}

	public String getOutsideReason() {
		return outsideReason;
	}

	public void setOutsideReason(String outsideReason) {
		this.outsideReason = outsideReason;
	}

	public String getSampleTest() {
		return sampleTest;
	}

	public void setSampleTest(String sampleTest) {
		this.sampleTest = sampleTest;
	}

	public String getSceneTest() {
		return sceneTest;
	}

	public void setSceneTest(String sceneTest) {
		this.sceneTest = sceneTest;
	}

	public String getRecommendLevel() {
		return recommendLevel;
	}

	public void setRecommendLevel(String recommendLevel) {
		this.recommendLevel = recommendLevel;
	}

	public String getOutsideFileName() {
		return outsideFileName;
	}

	public void setOutsideFileName(String outsideFileName) {
		this.outsideFileName = outsideFileName;
	}

	public String getOutsideFileKey() {
		return outsideFileKey;
	}

	public void setOutsideFileKey(String outsideFileKey) {
		this.outsideFileKey = outsideFileKey;
	}

	public String getYearFileName() {
		return yearFileName;
	}

	public void setYearFileName(String yearFileName) {
		this.yearFileName = yearFileName;
	}

	public String getYearFileKey() {
		return yearFileKey;
	}

	public void setYearFileKey(String yearFileKey) {
		this.yearFileKey = yearFileKey;
	}

	public String getTestFileName() {
		return testFileName;
	}

	public void setTestFileName(String testFileName) {
		this.testFileName = testFileName;
	}

	public String getTestFileKey() {
		return testFileKey;
	}

	public void setTestFileKey(String testFileKey) {
		this.testFileKey = testFileKey;
	}

	public String getTecFileName() {
		return tecFileName;
	}

	public void setTecFileName(String tecFileName) {
		this.tecFileName = tecFileName;
	}

	public String getTecFileKey() {
		return tecFileKey;
	}

	public void setTecFileKey(String tecFileKey) {
		this.tecFileKey = tecFileKey;
	}

	public String getOtherFileName() {
		return otherFileName;
	}

	public void setOtherFileName(String otherFileName) {
		this.otherFileName = otherFileName;
	}

	public String getOtherFileKey() {
		return otherFileKey;
	}

	public void setOtherFileKey(String otherFileKey) {
		this.otherFileKey = otherFileKey;
	}

	public String getTechnical() {
		return technical;
	}

	public void setTechnical(String technical) {
		this.technical = technical;
	}

	public String getAptitude() {
		return aptitude;
	}

	public void setAptitude(String aptitude) {
		this.aptitude = aptitude;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getServiceRequire() {
		return serviceRequire;
	}

	public void setServiceRequire(String serviceRequire) {
		this.serviceRequire = serviceRequire;
	}

	public String getCooperationDetail() {
		return cooperationDetail;
	}

	public void setCooperationDetail(String cooperationDetail) {
		this.cooperationDetail = cooperationDetail;
	}

	public String getSupplementInfo() {
		return supplementInfo;
	}

	public void setSupplementInfo(String supplementInfo) {
		this.supplementInfo = supplementInfo;
	}

	public String getDepSuggestion() {
		return depSuggestion;
	}

	public void setDepSuggestion(String depSuggestion) {
		this.depSuggestion = depSuggestion;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getApplicantDep() {
		return applicantDep;
	}

	public void setApplicantDep(String applicantDep) {
		this.applicantDep = applicantDep;
	}

	public String getApplicantTel() {
		return applicantTel;
	}

	public void setApplicantTel(String applicantTel) {
		this.applicantTel = applicantTel;
	}

	public String getDemandStatus() {
		return demandStatus;
	}

	public void setDemandStatus(String demandStatus) {
		this.demandStatus = demandStatus;
	}

	public String getExamineStatus() {
		return examineStatus;
	}

	public void setExamineStatus(String examineStatus) {
		this.examineStatus = examineStatus;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApplicantDepId() {
		return applicantDepId;
	}

	public void setApplicantDepId(String applicantDepId) {
		this.applicantDepId = applicantDepId;
	}

}