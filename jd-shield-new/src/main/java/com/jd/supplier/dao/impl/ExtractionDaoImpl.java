package com.jd.supplier.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.official.core.dao.Page;
import com.jd.supplier.dao.ExtractionDao;
import com.jd.supplier.model.Extraction;

@Component("extractionDao")
public class ExtractionDaoImpl extends MyBatisDaoImpl implements ExtractionDao {
	
	public static final String NAMESPACE = Extraction.class.getName();

	@Override
	public Page<Extraction> find(String projectName, String creator,
			String startTime, String endTime, int offset, int pageSize) {
		Map map = new HashMap();
		map.put("projectName", projectName);
		map.put("creator", creator);
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		
		List<Extraction> list = getSqlSession().selectList(NAMESPACE + ".findHistory", map);
		Integer count = getSqlSession().selectOne(NAMESPACE + ".findHistory-count", map);
		
		Page<Extraction> pager = new Page<Extraction>();
		pager.setResult(list);
		pager.setTotalCount(count);
		return pager;
	}

	@Override
	public Extraction findById(long id) {
		return getSqlSession().selectOne(NAMESPACE + ".findById", id);
	}

	@Override
	public void save(Extraction extraction) {
		if(extraction.getId() == null)
			getSqlSession().insert(NAMESPACE + ".insert", extraction);
		else
			getSqlSession().update(NAMESPACE + ".update", extraction);
		
	}

	@Override
	public Extraction findByReqId(long reqId) {
		return getSqlSession().selectOne(NAMESPACE + ".findByReqId", reqId);
	}

}
