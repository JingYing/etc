package com.jd.supplier.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.TaskNodeDao;
import com.jd.supplier.model.TaskNode;

/**
 * 任务节点数据层实现
 * @author wangchanghui
 *
 */
@Repository("taskNodeDao")
public class TaskNodeDaoImpl extends MyBatisDaoImpl<TaskNode, Long> implements TaskNodeDao{


	/**
	 * 查询节点集合
	 */
	@Override
	public List<TaskNode> findNodeList(Map<String, Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskNode.find", map);
	}

	/**
	 * 查询初始节点
	 */
	@Override
	public TaskNode fintInitNode(Map<String, Object> map) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.findInitNode", map);
	}

	/**
	 * 查询未完成的父节点数量
	 */
	public Integer findUncompletedParentNodeCount(Map<String,Object> map){
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.findUncompletedParentNodeCount", map);
	}
	
	/**
	 * 检查是否为父节点
	 */
	public Integer checkIsParent(Map<String,Object> map){
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.checkIsParent", map);
	}

	/**
	 * 根据父id获取主键
	 */
	@Override
	public Long getPkByParentId(Map<String, Object> map) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.getPkByParentId", map);
	}

	/**
	 * 根据父节点获取对象
	 */
	@Override
	public TaskNode getByParentId(Long id) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.getByParentId", id);
	}

	/**
	 * 获取父节点对象
	 */
	@Override
	public TaskNode getParent(Long id) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.getParent", id);
	}

	/**
	 * 通过机构码查找节点集合
	 */
	@Override
	public List<TaskNode> findNodeListByOrgCode(Map<String,Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskNode.findNodeListByOrgCode", map);
	}

	/**
	 * 查询id集合
	 */
	@Override
	public List<Long> findIdList(Map<String, Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskNode.findIdList", map);
	}

	/**
	 * 通过状态查询节点数量
	 */
	@Override
	public Integer findNodeCountByStatus(Map<String, Object> map) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.findNodeCountByStatus", map);
	}

	/**
	 * 根据任务id查询节点数量
	 */
	@Override
	public Integer findNodeCountByTaskId(Long taskId) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNode.findNodeCountByTaskId", taskId);
	}
}
