package com.jd.supplier.dao;

import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
/**
 * 预选入围数据层接口
 * @author wangchanghui
 *
 */
public interface TaskSuppSelectionDao extends BaseDao<TaskSuppSelection, Long>{
	
	/**
	 * 查询size
	 * @param map
	 * @return
	 */
	Integer findTaskSuppSelectionSize(Map<String,Object> map);
	
	/**
	 * 查询入围供应商信息
	 * @param map
	 * @return
	 */
	List<Supplier> findFinalSupplierList(Map<String,Object> map);
	
	/**
	 * 根据需求id查询对象集合
	 * @param requirementId
	 * @return
	 */
	List<Supplier> findTaskSuppSelectionListByReqId(Long requirementId);
	
	/**
	 * 查找预选或入围数量
	 * @param map
	 * @return
	 */
	Integer findSelectionCount(Map<String,Object> map);
}