package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Contacts;

public interface IContactsDao extends BaseDao<Contacts, Long>{

}
