package com.jd.supplier.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.TaskSuppSelectionDao;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
/**
 * 预选入围数据层实现
 * @author wangchanghui
 *
 */
@Repository("taskSuppSelectionDao")
public class TaskSuppSelectionDaoImpl extends MyBatisDaoImpl<TaskSuppSelection, Long> implements TaskSuppSelectionDao {


	/**
	 * 查询对象数量
	 */
	@Override
	public Integer findTaskSuppSelectionSize(Map<String, Object> map) {
		String sql = new StringBuffer().append(this.sqlMapNamespace).append(".").append(map.get("sql")).toString();
		return (Integer)this.getSqlSession().selectOne(sql, map);
	}
	
	/**
	 * 查询入围集合
	 */
	@Override
	public List<Supplier> findFinalSupplierList(Map<String,Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskSuppSelection.findFinalSupplierList", map);
	}
	
	/**
	 * 根据需求id查询对象集合
	 */
	@Override
	public List<Supplier> findTaskSuppSelectionListByReqId(Long requirementId){
	    Map<String ,Object> reqMap=new HashMap<String ,Object>();
	    reqMap.put("requirementId", requirementId);
	    return this.getSqlSession().selectList("com.jd.supplier.model.TaskSuppSelection.findFinalSupplierListByReqId", reqMap);
	}

	/**
	 * 查询数量
	 */
	@Override
	public Integer findSelectionCount(Map<String, Object> map) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskSuppSelection.findSelectionCount", map);
	}


}
