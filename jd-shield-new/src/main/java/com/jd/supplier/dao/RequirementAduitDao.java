package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.official.modules.system.model.AuthExpression;
import com.jd.supplier.model.RequirementAduit;

public interface RequirementAduitDao extends BaseDao<RequirementAduit, Long> {

}
