package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Enterprise;

public interface IEnterpriseDao extends BaseDao<Enterprise, Long>{
	
	public String getMaxSupId();

}
