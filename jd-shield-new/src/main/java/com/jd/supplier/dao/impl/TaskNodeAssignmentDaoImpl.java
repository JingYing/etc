/**
 * 
 */
package com.jd.supplier.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.official.modules.system.model.User;
import com.jd.supplier.dao.TaskNodeAssignmentDao;
import com.jd.supplier.model.TaskNodeAssignment;

/**
 * 任务节点指派数据层实现
 * @author wangchanghui
 *
 */
@Repository("taskNodeAssignmentDao")
public class TaskNodeAssignmentDaoImpl extends MyBatisDaoImpl<TaskNodeAssignment, Long> implements
		TaskNodeAssignmentDao {

	/**
	 * 查找任务指派用户集合
	 */
	@Override
	public List<User> findTaskAssignUsers(Map<String, Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskNodeAssignment.findTaskAssignUsers", map);
	}

	/**
	 * 获取认领数量
	 */
	@Override
	public Integer getClaimCount(Map<String, Object> map) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNodeAssignment.getClaimCount", map);
	}

	/**
	 * 获取指派数量
	 */
	@Override
	public Integer getAssignedCount(Map<String, Object> map) {
		return null;
	}

	/**
	 * 根据节点id获取指派对象
	 */
	@Override
	public TaskNodeAssignment getAssignmentByNodeId(Long nodeId) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.TaskNodeAssignment.getAssignmentByNodeId", nodeId);
	}
}
