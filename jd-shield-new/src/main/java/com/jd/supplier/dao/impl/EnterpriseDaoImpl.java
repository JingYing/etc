package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.IEnterpriseDao;
import com.jd.supplier.model.Enterprise;

@Component("enterpriseDao")
public class EnterpriseDaoImpl extends MyBatisDaoImpl<Enterprise, Long> implements IEnterpriseDao{


	
	@Override
	public String getMaxSupId() {
		String str=this.getSqlSession().selectOne("com.jd.supplier.model.Enterprise.getMaxSupId");
		return str;
	}

}
