package com.jd.supplier.dao;

import java.util.Map;

import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Advertising;

public interface AdvertisingDao {

	Page<Advertising> findLikeAdvertiser(Map<String, Object> condition, 
			String orderBy, String order, int offset, int pageSize);

	Advertising findById(long id);

	void insert(Advertising adv);

	void update(Advertising adv);

	Page<Advertising> findByPosition(String position, int offset, int pageSize);

	void updateSortNum(long id, int sortNum);

}
