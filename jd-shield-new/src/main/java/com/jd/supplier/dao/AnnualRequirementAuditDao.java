package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.AnnualRequirementAudit;

public interface AnnualRequirementAuditDao extends BaseDao<AnnualRequirementAudit, Long> {

}
