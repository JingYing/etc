package com.jd.supplier.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.official.core.dao.Page;
import com.jd.official.core.exception.BusinessException;
import com.jd.supplier.dao.AdvertisingDao;
import com.jd.supplier.model.Advertising;

@Component("advertisingDao")
public class AdvertisingDaoImpl extends MyBatisDaoImpl implements AdvertisingDao	{
	
	public static final String NAMESPACE = Advertising.class.getName();

	@Override
	public Page<Advertising> findLikeAdvertiser(Map<String, Object> condition,
			String orderBy, String order, int offset, int pageSize) {
		if(condition.containsKey("offset") || condition.containsKey("pageSize"))	{
			throw new BusinessException("condition中不能包含offset或pageSize");
		}
		condition.put("offset", offset);
		condition.put("pageSize", pageSize);
		condition.put("orderBy", orderBy);
		condition.put("order", order);
		List<Advertising> list = getSqlSession().selectList(NAMESPACE + ".findLikeAdvertiser", condition);
		long count =(Long)getSqlSession().selectOne(NAMESPACE + ".findLikeAdvertiser-count", condition);
		
		Page<Advertising> pager = new Page<Advertising>();
		pager.setResult(list);
		pager.setTotalCount(count);
		return pager;
	}

	@Override
	public Advertising findById(long id) {
		return getSqlSession().selectOne(NAMESPACE + ".findById", id);
	}

	@Override
	public void insert(Advertising adv) {
		getSqlSession().insert(NAMESPACE + ".insert", adv);
	}

	@Override
	public void update(Advertising adv) {
		getSqlSession().update(NAMESPACE + ".update", adv);
	}

	@Override
	public Page<Advertising> findByPosition(String position, int offset,
			int pageSize) {
		Map map = new HashMap();
		map.put("position", position);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Advertising> list = getSqlSession().selectList(NAMESPACE + ".findByPosition", map);
		long count = (Long)getSqlSession().selectOne(NAMESPACE + ".findByPosition-count", map);
		
		Page<Advertising> pager = new Page<Advertising>();
		pager.setResult(list);
		pager.setTotalCount(count);
		return pager;
	}

	@Override
	public void updateSortNum(long id, int sortNum) {
		Map map = new HashMap();
		map.put("id", id);
		map.put("sortNum", sortNum);
		getSqlSession().update(NAMESPACE + ".updateSortNum", map);
	}

}
