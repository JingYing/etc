package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.RequirementAduitDao;
import com.jd.supplier.model.RequirementAduit;

@Component("requirementAduitDao")
public class RequirementAduitDaoImpl extends MyBatisDaoImpl<RequirementAduit, Long> implements RequirementAduitDao{

}
