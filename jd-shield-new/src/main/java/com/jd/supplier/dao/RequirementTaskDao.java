package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.RequirementTask;

public interface RequirementTaskDao extends BaseDao<RequirementTask, Long> {

}
