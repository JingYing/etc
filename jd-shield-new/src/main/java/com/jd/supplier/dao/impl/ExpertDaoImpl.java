package com.jd.supplier.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.official.core.dao.Page;
import com.jd.supplier.dao.ExpertDao;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.Extraction;

@Component("expertDao")
public class ExpertDaoImpl extends MyBatisDaoImpl implements ExpertDao 	{

	private static final String NAMESPACE = Expert.class.getName();
	@Override
	public Page<Expert> findAll(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Expert> list = getSqlSession().selectList(Expert.class.getName() + ".findAll", map);
		Integer total = getSqlSession().selectOne(NAMESPACE + ".findAll-count", map);
		
		Page<Expert> pager = new Page<Expert>();
		pager.setResult(list);
		pager.setTotalCount(total);
		return pager;
	}

	@Override
	public List<Expert> findLikeRealName(String name) {
		return getSqlSession().selectList(NAMESPACE + ".findLikeRealName", name);
	}
	
	@Override
	public Expert findByUserName(String userName) {
		return getSqlSession().selectOne(NAMESPACE + ".findByUserName", userName);
	}

	@Override
	public Page<Expert> find(Map<String, Object> condition, int offset,
			int pageSize) {
		condition.put("offset", offset);
		condition.put("pageSize", pageSize);
		List<Expert> list = getSqlSession().selectList(NAMESPACE + ".find", condition);
		Integer total = getSqlSession().selectOne(NAMESPACE + ".find-count", condition);

		Page<Expert> pager = new Page<Expert>();
		pager.setResult(list);
		pager.setTotalCount(total);
		return pager;
	}

	@Override
	public Long save(Expert expert) {
		if(expert.getId() == null)	{
			getSqlSession().insert(NAMESPACE + ".insert", expert);
		} else	{
			getSqlSession().update(NAMESPACE + ".update", expert);
		}
		return expert.getId();
	}

	@Override
	public Expert findById(long id) {
		return getSqlSession().selectOne(NAMESPACE + ".findById", id);
	}
	
	@Override
	public List<Expert> findByIds(long[] ids) {
		return getSqlSession().selectList(NAMESPACE + ".findByIds", ids);
	}

	@Override
	public Page<Expert> extract(Extraction extraction, String groupId, List<Long> excludedUserIds,
			int offset, int pageSize) {
		Map map = new HashMap();
		map.put("extraction", extraction);
		map.put("groupId", groupId);
		map.put("excludedUserIds", excludedUserIds);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Expert> list = getSqlSession().selectList(NAMESPACE + ".extract", map);
		Integer total = getSqlSession().selectOne(NAMESPACE + ".extract-count", map);
		
		Page<Expert> pager = new Page<Expert>();
		pager.setResult(list);
		pager.setTotalCount(total);
		return pager;
	}

}
