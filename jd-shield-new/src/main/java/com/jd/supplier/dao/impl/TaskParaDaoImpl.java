package com.jd.supplier.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.TaskParaDao;
import com.jd.supplier.model.TaskPara;

/**
 * 任务参数数据层实现
 * @author wangchanghui
 *
 */
@Repository("taskParaDao")
public class TaskParaDaoImpl extends MyBatisDaoImpl<TaskPara, Long> implements
		TaskParaDao {


	/**
	 * 查询参数集合
	 */
	@Override
	public List<TaskPara> findParaList(Map<String, Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskPara.findParaList", map);
	}

	/**
	 * 查询需要绑定的集合
	 */
	@Override
	public List<TaskPara> findToBeBindedList(Map<String, Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskPara.", map);
	}

}
