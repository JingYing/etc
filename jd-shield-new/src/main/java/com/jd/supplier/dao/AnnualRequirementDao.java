package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.AnnualRequirement;

public interface AnnualRequirementDao extends BaseDao<AnnualRequirement, Long> {

}
