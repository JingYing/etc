package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.RequirementAffixDao;
import com.jd.supplier.model.RequirementAffix;

@Component("requirementAffixDao")
public class RequirementAffixDaoImpl extends MyBatisDaoImpl<RequirementAffix, Long> implements RequirementAffixDao{

	@Override
	public RequirementAffix findByPara(RequirementAffix requirementAffix) {
		return getSqlSession().selectOne("com.jd.supplier.model.RequirementAffix.findByPara", requirementAffix);
	}

}
