package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Task;

/**
 * 任务单数据层接口
 * @author wangchanghui
 *
 */
public interface TaskDao extends BaseDao<Task, Long>{
}