package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.IEquipDao;
import com.jd.supplier.model.Equip;
@Component("equipDao")
public class EquipDaoImpl extends MyBatisDaoImpl<Equip, Long> implements IEquipDao {


}
