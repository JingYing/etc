package com.jd.supplier.service;

import java.util.List;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.RequirementTaskAssign;

public interface RequirementTaskAssignService extends BaseService<RequirementTaskAssign, Long> {
    List<RequirementTaskAssign> deleteOnlyUnused(String ql, Object... values);

    List<RequirementTaskAssign> findRequirementTaskAssignListByIds(final String... values);

    List<RequirementTaskAssign> findRequirementTaskAssignListByIds(final RequirementTaskAssign... values);

    List<RequirementTaskAssign> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId);

    Integer getMaxRequirementTaskAssignID();
}
