package com.jd.supplier.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.Page;
import com.jd.official.modules.dict.model.DictData;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.ExpertTag;

public interface ExpertService {

	Page<Expert> findAll(int offset, int pageSize);
	
	List<DictData> findAllAvailableTag();

	List<Expert> findLikeRealName(String name);
	
	Page<Expert> find(Map<String, Object> condition, int offset, int pageSize);
	
	void save(Expert expert);

	Expert findById(long id);

	List<Expert> findByIds(long[] ids);
	
	void saveExpertAndTags(Expert expert, List<ExpertTag> tags, int[] delTagId);

	List<ExpertTag> findExpertTag(long expertId);

	/**
	 * 导入EXCEL后关闭流
	 * @param is
	 * @return 将反馈excel写入redis后的key
	 * @throws IOException
	 */
	String insertFromExcel(InputStream is, String fileName) throws IOException;
	
	


}
