package com.jd.supplier.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.RequirementTaskDao;
import com.jd.supplier.model.RequirementTask;
import com.jd.supplier.service.RequirementTaskService;

@Service("requirementTaskService")
@Transactional
public class RequirementTaskServiceImpl extends BaseServiceImpl<RequirementTask, Long> implements RequirementTaskService {
    @Autowired
    private RequirementTaskDao requirementTaskDao;

    public RequirementTaskDao getDao() {
	return requirementTaskDao;
    }

    @Override
    public List<RequirementTask> deleteOnlyUnused(String ql, Object... values) {
	List<RequirementTask> listUnDelete = new ArrayList<RequirementTask>();
	if (values != null) {
	    for (int i = 0; i < values.length; i++) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("expressionid", values[i]);

		List<RequirementTask> list = requirementTaskDao.find("findByExpressionId", map);
		if (list != null && list.size() > 0) {
			listUnDelete.add((RequirementTask) get(values[i].toString()));
		} else{
			delete(ql, new Object[] { values[i] });
		}

	    }
	}
	return listUnDelete;
    }

    @Override
    public Integer getMaxRequirementTaskID() {
	return (Integer) requirementTaskDao.get("getMaxRequirementTaskID", null);
    }

    @Override
    public List<RequirementTask> findRequirementTaskListByIds(String... values) {
	List<RequirementTask> list = new ArrayList<RequirementTask>();
	for (int i = 0; i < values.length; i++) {
	    RequirementTask authExpression = (RequirementTask) get(Long.valueOf(values[i]));
	    if (authExpression != null)
		list.add(authExpression);
	}
	return list;

    }

    @Override
    public List<RequirementTask> findRequirementTaskListByIds(RequirementTask... values) {
	List<RequirementTask> list = new ArrayList<RequirementTask>();
	for (int i = 0; i < values.length; i++)
	    list.add((RequirementTask) get(values[i].getId()));
	return list;
    }

    @Override
    public List<RequirementTask> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId) {
	Map<String, String> map = new HashMap<String, String>();
	map.put("businessType", businessType);
	map.put("businessId", businessId);
	List<RequirementTask> list = requirementTaskDao.find("findExpressionByBusinessIdAndBusinessType", map);

	return list;
    }

}
