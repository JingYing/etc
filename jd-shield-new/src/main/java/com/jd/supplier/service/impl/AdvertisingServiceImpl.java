package com.jd.supplier.service.impl;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.jcloud.jss.service.ObjectService;
import com.jd.common.util.Util;
import com.jd.official.core.dao.Page;
import com.jd.official.core.jss.service.JssService;
import com.jd.supplier.dao.AdvertisingDao;
import com.jd.supplier.model.Advertising;
import com.jd.supplier.service.AdvertisingService;

















@Component("advertisingServicerrrr")
public class AdvertisingServiceImpl implements AdvertisingService	{
	
	@Resource
	private AdvertisingDao advertisingDao;
	
	@Resource
	private JssService jssService;
	
	@Value("${jss.bucket}")
	private String bucket;
	
	@Override
	public Page<Advertising> findLikeAdvertiser(Map<String, Object> condition,
			String orderBy, String order, int offset, int pageSize) {
		return advertisingDao.findLikeAdvertiser(condition, orderBy, order, offset, pageSize);
	}
	
	/**
	 * 上传文件到云存储, 并生成url, 有效期68年. 不知道怎么生成永久性链接.
	 * @param is
	 * @param fileName
	 * @return
	 * @throws IOException 
	 */
	@Override
	public String[] uploadToCloud(MultipartFile file) throws IOException	{
		String key = UUID.randomUUID().toString() + Util.getSuffix(file.getOriginalFilename()); 
		
		ObjectService os = jssService.getJssService().bucket(bucket).object(key);
		os.entity(file.getSize(), file.getInputStream()).put();
		String url = os.generatePresignedUrl(Integer.MAX_VALUE).toString();
		
		return new String[]{key, url};
	}
	
	@Override
	public void deleteFromCloud(String key) {
		jssService.deleteObject(bucket, key);
	}

	@Override
	public void save(Advertising adv) {
		if(adv.getId() == null)
			advertisingDao.insert(adv);
		else
			advertisingDao.update(adv);
	}

	@Override
	public Advertising findById(long id) {
		return advertisingDao.findById(id);
	}

	@Override
	public void delete(long id) {
		Advertising adv = findById(id);
		adv.setIsDel(1);
		advertisingDao.update(adv);
	}

	@Override
	public Map<String, String> findPositionValue() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put(Advertising.POSITION_TOP, "顶部广告");
		map.put(Advertising.POSITION_BOTTOM, "底部广告");
		return map;
	}

	@Override
	public Page<Advertising> findByPosition(String position, int offset,
			int pageSize) {
		return advertisingDao.findByPosition(position, offset, pageSize);
	}

	@Override
	public void saveOrder(List<long[]> sorted) {
		for(long[] arr : sorted)	{
			for(int i=0; i<arr.length; i++)	{
				advertisingDao.updateSortNum(arr[i], i);
			}
		}
		
	}



}
