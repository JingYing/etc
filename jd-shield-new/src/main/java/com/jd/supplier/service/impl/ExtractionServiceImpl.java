package com.jd.supplier.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.common.util.Util;
import com.jd.official.core.dao.Page;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.mail.MailSendService;
import com.jd.official.modules.dict.dao.DictDataDao;
import com.jd.supplier.dao.ExpertDao;
import com.jd.supplier.dao.ExpertExtractionDao;
import com.jd.supplier.dao.ExtractionDao;
import com.jd.supplier.dao.TaskSuppSelectionDao;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.ExpertExtraction;
import com.jd.supplier.model.Extraction;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.service.ExtractionService;
import com.jd.supplier.service.NotEnoughExpertException;

@Component("extractionService")
public class ExtractionServiceImpl implements ExtractionService	{
	
	static Logger log = Logger.getLogger(ExtractionServiceImpl.class);
	
	@Resource
	private ExtractionDao extractionDao;
	@Resource
	private ExpertExtractionDao expertExtractionDao;
	@Resource
	private ExpertDao expertDao;
	@Resource
	private MailSendService mailSendService;
	@Resource
	private DictDataDao dictDataDao;
	@Resource
	private TaskSuppSelectionDao taskSuppSelectionDao;

	/**
	 * 1.专家数是否够用
	 * 2.抽取
	 * 3.保存抽取操作
	 * 4.发邮件
	 * 5.保存expertextraction
	 * @throws NotEnoughExpertException 
	 */
	@Override
	public List<Expert> insertExtract(Extraction extraction)  {
		List<Long> referrer = this.findSupplierReferrer(extraction.getReq().getId());
		JsonObject json = new JsonParser().parse(extraction.getReqExpert()).getAsJsonObject();
		
		List<Expert> extracted = new ArrayList<Expert>();
		for(Entry<String,JsonElement> e : json.entrySet())	{
			if(!"all".equals(e.getKey()))	{
				
				List<Expert> list = expertDao.extract(extraction, e.getKey(), 
						referrer.isEmpty() ? null : referrer, 	//无值时传null
						0, Integer.MAX_VALUE).getResult();			//抽取所有符合条件的专家出来,再进行随机排列
				
				Collections.shuffle(list);
				if(list.size() > e.getValue().getAsInt())	{
					list = list.subList(0, e.getValue().getAsInt());
				} 
				extracted.addAll(list);
			}
		}
	
		if(extracted.isEmpty())	{
			return new ArrayList<Expert>();	//一个专家都没抽取到时, 返回空集合
		} else	{
			extractionDao.save(extraction);
			insertAddition(extracted, extraction.getId(), ExpertExtraction.EXTRACT_MODE_系统抽取);
			return extracted;
		}
	}
	
	/**
	 * 查询该项目的入围供应商的推荐员工ID
	 * @param reqId
	 * @return
	 */
	private List<Long> findSupplierReferrer(long reqId)	{
		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("requirementId", reqId);
			List<Supplier> list = taskSuppSelectionDao.findFinalSupplierList(map);
			
			List<Long> referer = new ArrayList<Long>();
			for(Supplier s : list)	{
				referer.add(s.getRecommended());
			}
			return referer;
		} catch (Exception e) {
			throw new BusinessException("调用taskSuppSelectionDao.findFinalSupplierList()出现异常", e);
		}
	}
	
	@Override
	public void insertAddition(List<Expert> experts, long extractionId, String extractMode) {
		Extraction ext = extractionDao.findById(extractionId);
		for(Expert e : experts)	{
			ExpertExtraction ee = new ExpertExtraction();
			ee.setExpert(e);
			ee.setExtractionId(ext.getId());
			ee.setExtractMode(extractMode);
			
			boolean hasSendEmail = false;
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("expert", e);
			map.put("extraction", ext);

			try {
				mailSendService.sendTemplateMail(new String[]{e.getUser().getEmail()}, 
						"招投标项目评审通知-"+ext.getReq().getProjectName(), 
						"mail/afterExtraction.vm", map, new File[]{});
				ee.setRespCode(ExpertExtraction.RESP_CODE_已通知);
				hasSendEmail = true;
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
				ee.setRespCode(ExpertExtraction.RESP_CODE_发送邮件失败);
			}
			expertExtractionDao.save(ee);
			if(hasSendEmail)	{
				schedulerChangeResp(ee);
			}
		}
	}

	/**
	 * 24小时后如果未回复,则修改通知状态
	 */
	private void schedulerChangeResp(final ExpertExtraction ee)	{
		new Timer(true).schedule(new TimerTask() {
			@Override
			public void run() {
				if(ee.getRespCode() == ExpertExtraction.RESP_CODE_已通知)	{
					ee.setRespCode(ExpertExtraction.RESP_CODE_已拒绝);
					ee.setRespDesc("24h内未回复");
				}
				expertExtractionDao.save(ee);
			}
		}, 1000 * 3600 * 24);	
	}
	
	@Override
	public Page<Extraction> find(String prjName, String creator,
			String startTime, String endTime, int offset, int pageSize) {
		return extractionDao.find(prjName, creator, startTime, endTime, offset, pageSize);
	}

	@Override
	public byte[] toExcel(List<Extraction> list) throws IOException {
		Workbook wb = new XSSFWorkbook();
		CellStyle style = wb.createCellStyle();
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
		
		
		for(Extraction ext : list)	{
			Sheet sheet = wb.createSheet(ext.getReq().getProjectName() + "(" + ext.getReq().getId() + ")");
			int rowNum = 0;
			
			sheet.addMergedRegion(new CellRangeAddress(
					rowNum, //first row (0-based)
					rowNum, //last row  (0-based)
					0, //first column (0-based)
					10  //last column  (0-based)
					));
			sheet.createRow(rowNum++).createCell(0).setCellValue("京盾招投标系统评标专家抽取结果");
			
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 10));
			Row prj = sheet.createRow(rowNum++);
			prj.createCell(0).setCellValue("项目名称:");
			prj.createCell(1).setCellValue(ext.getReq().getProjectName());
			
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 4));
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 6, 10));
			Row time = sheet.createRow(rowNum++);
			time.createCell(0).setCellValue("评标开始时间:");
			time.createCell(1).setCellValue(Util.formatDate(ext.getEvalStartTime()));
			time.createCell(5).setCellValue("评标结束时间:");
			time.createCell(6).setCellValue(Util.formatDate(ext.getEvalEndTime()));
			
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 4));
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 6, 10));
			Row oper = sheet.createRow(rowNum++);
			oper.createCell(0).setCellValue("操作人:");
			oper.createCell(1).setCellValue(ext.getCreator());
			oper.createCell(5).setCellValue("操作时间:");
			oper.createCell(6).setCellValue(Util.formatDate(ext.getCreateTime()));
			
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 10));
			Row result = sheet.createRow(rowNum++);
			result.createCell(0).setCellValue("抽取结果:");
			int[] arr = calculateExpertCount(ext.getId());
			String extractResult = String.format("预设抽取%d人，待回复%d人，确认参加%d人", arr[0], arr[3], arr[4]);
			if(arr[2] > 0)	{
				extractResult += "需要人工补录" + arr[2] +"人";
			}
			result.createCell(1).setCellValue(extractResult);
			
			sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 10));
			sheet.createRow(rowNum++).createCell(0).setCellValue("抽取详情:");
			
			List<ExpertExtraction> eelist = expertExtractionDao.findByExtrId(ext.getId());
			Row rowTitle = sheet.createRow(rowNum ++);
			String[] titles = new String[]{"序号","专家编号","姓名","一级机构","二级机构","企业邮箱","手机","专家组别","通知结果","原因","抽取方式"};
			for(int i=0; i<titles.length; i++)	{
				rowTitle.createCell(i).setCellValue(titles[i]);
			}
			
			int sequence = 1;
			for(ExpertExtraction ee : eelist)	{
				Row row = sheet.createRow(rowNum ++);
				row.createCell(0).setCellValue(sequence++);
				row.createCell(1).setCellValue(ee.getExpert().getId());
				row.createCell(2).setCellValue(ee.getExpert().getUser().getRealName());
				row.createCell(3).setCellValue(ee.getExpert().getUser().getOrganization().getOrganizationName());
				row.createCell(4).setCellValue("");		//暂不支持二级机构
				row.createCell(5).setCellValue(ee.getExpert().getUser().getEmail());
				row.createCell(6).setCellValue(ee.getExpert().getUser().getMobile());
				row.createCell(7).setCellValue(ee.getExpert().getGroup().getDictDataName());
				
				String resp = "";
				switch(ee.getRespCode())	{
				case ExpertExtraction.RESP_CODE_已通知 : 
					resp = "已通知";break;
				case ExpertExtraction.RESP_CODE_已确认参加 : 
					resp = "确认回复参加";break;
				case ExpertExtraction.RESP_CODE_已拒绝 : 
					resp = "确认回复拒绝";break;
				case ExpertExtraction.RESP_CODE_发送邮件失败 : 
					resp = "邮件发送失败";break;
				default :
					break;
				}
				row.createCell(8).setCellValue(resp);
				row.createCell(9).setCellValue(ee.getRespDesc());
				
				String extractMode = "";
				if(ExpertExtraction.EXTRACT_MODE_人工补录.equals(ee.getExtractMode()))	{
					extractMode = "人工补录";
				} else if(ExpertExtraction.EXTRACT_MODE_系统抽取.equals(ee.getExtractMode()))	{
					extractMode = "系统抽取";
				}
				row.createCell(10).setCellValue(extractMode);
				
			}
		}
		
		if(list.isEmpty())	{
			wb.createSheet("无数据");
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		wb.write(baos);
		return baos.toByteArray();
	}
	
	/*TODO
	public byte[] toReportXls(String startDate, String endDate)	{
		List<Expert> hasAttend = expertExtractionDao.findByTime(startDate, endDate);//筛选该时间段内所有参加过项目的专家
		int[][] arr = new int[hasAttend.size()][];
		String[] experts = new String[hasAttend.size()];
		String[] reqs = null;
		
		for(int i=0; i<hasAttend.size(); i++)	{
			Map<String,Object> map = expertExtractionDao.find(hasAttend.get(i).getId(), startDate, endDate);
			int[] ints = new int[map.size()];
			arr[i] = ints;
		}
		
		return toXls(reqs, experts, arr);
	}
	*/
	
	
	private byte[] toXls(String[] vertical, String[] horizon, int[][] arr) throws IOException	{
		Workbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet();
		
		//初始化每列的列头
		Row expertRow = sheet.createRow(0);
		for(int i=0; i<vertical.length; i++)	{
			expertRow.createCell(i+1).setCellValue(vertical[i]);		//从第2列开始
		}
		
		//初始化每行的行头
		for(int i=0; i<horizon.length; i++)	{
			Row reqRow = sheet.createRow(i+1);		//从第2行开始
			reqRow.createCell(0).setCellValue(horizon[i]);
		}
		
		for(int i=0; i<arr.length; i++)	{
			for(int j=0; j<arr[i].length; j++)	{
				Row row = sheet.getRow(j);
				if(row == null)
					row = sheet.createRow(j);
				
				Cell cell = row.getCell(i);
				if(cell == null)
					cell = row.createCell(i);
				
				cell.setCellValue(arr[i][j]);
			}
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		wb.write(baos);
		return baos.toByteArray();
	}
	
	

	@Override
	public Extraction findById(long id) {
		return extractionDao.findById(id);
	}

	@Override
	public int[] calculateExpertCount(long extractionId) {
		Extraction e = extractionDao.findById(extractionId);
		JsonObject json = new JsonParser().parse(e.getReqExpert()).getAsJsonObject();
		int reqTotal = json.get("all").getAsInt(), 
			extractedTotal = 0, unreply = 0, confirm = 0, refuse = 0;
		
		List<ExpertExtraction> eeList = expertExtractionDao.findByExtrId(e.getId());
		extractedTotal = eeList.size();
		for(ExpertExtraction ee : eeList)	{
			if(ee.getRespCode() != null)	{
				if(ee.getRespCode() == 0)	{
					unreply ++;
				} else if(ee.getRespCode() == 1)	{
					confirm ++;
				} else if(ee.getRespCode() == 2){
					refuse ++;
				} 
			}
		}
		return new int[]{reqTotal, extractedTotal, reqTotal-extractedTotal, unreply, confirm, refuse};
	}

	@Override
	public List<ExpertExtraction> findExpertExtraction(long extractionId) {
		return expertExtractionDao.findByExtrId(extractionId);
	}
	
	@Override
	public ExpertExtraction findExpertExtraction(long reqId, long expertId) {
		return expertExtractionDao.findUnique(reqId, expertId);
	}

	@Override
	public Extraction findByReqId(long reqId) {
		return extractionDao.findByReqId(reqId);
	}

	@Override
	public void saveExpertExtraction(ExpertExtraction ee) {
		expertExtractionDao.save(ee);
	}

	@Override
	public boolean hasExtracted(long extractionId, long expertId) {
		List<ExpertExtraction> eelist = expertExtractionDao.find(extractionId, expertId);
		return eelist.isEmpty() ? false : true;
	}
}
