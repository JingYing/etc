package com.jd.supplier.service;

import java.util.List;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.controller.vo.SpecialSupplierVO;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.SupplierDto;

public interface ISupplierService extends BaseService<Supplier, Long>{

	
	/**
	 * 导入供应商
	 * @param recordList
	 * @param resultString
	 * @param fileName
	 * @param results
	 * @return
	 */
	public String insertImportSupplier(List<SupplierDto> recordList,List<String> resultString,String fileName,String results) ;
	/**
	 * 插入供应商基本信息
	 * @param supplier
	 * @return
	 */
	public  int  insertData(Supplier supplier);
	/**
	 * 修改供应商基本信息
	 * @param supplier
	 * @return
	 */
	public  int  updateData(Supplier supplier);
	
	/**
	 * 添加特殊供应商
	 * @param supplier
	 * @param contacts
	 * @return
	 */
	public void saveSpecialSupplier(SpecialSupplierVO specialSupplierVO);
	
	/**
	 * 通知内荐供应商
	 * @param supplier
	 * @return
	 */
	public  int  updateNotice(Supplier supplier);
	/**
	 * 插入内荐供应商
	 * @param supplier
	 * @return
	 */
	public  int  insertRecommend(Supplier supplier);
	
	/**
	 * 审核供应商，并且更新推荐供应商表
	 * @param supplier
	 * @return
	 */
	public int updateStatus(Supplier supplier);
	
	/**
	 * 驳回
	 * @param supplier
	 * @return
	 */
	public int updateReject(Supplier supplier);
}
