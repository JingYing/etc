package com.jd.supplier.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.common.constants.EmailConstant;
import com.jd.common.constants.TaskConstant;
import com.jd.common.enm.DeleteEnum;
import com.jd.official.core.mail.MailSendService;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.modules.system.dao.RoleDao;
import com.jd.official.modules.system.dao.UserDao;
import com.jd.official.modules.system.model.Role;
import com.jd.official.modules.system.model.User;
import com.jd.supplier.dao.RequirementDao;
import com.jd.supplier.dao.TaskDao;
import com.jd.supplier.dao.TaskNodeAssignmentDao;
import com.jd.supplier.dao.TaskNodeDao;
import com.jd.supplier.dao.TaskParaDao;
import com.jd.supplier.dao.TaskParaRelaDao;
import com.jd.supplier.enm.AssignmentStatusEnum;
import com.jd.supplier.enm.CheckNodeStatusEnum;
import com.jd.supplier.enm.NodeStatusEnum;
import com.jd.supplier.enm.RoleTypeEnum;
import com.jd.supplier.enm.TaskNodeEnum;
import com.jd.supplier.enm.TaskParaEnum;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.model.Task;
import com.jd.supplier.model.TaskNode;
import com.jd.supplier.model.TaskNodeAssignment;
import com.jd.supplier.model.TaskPara;
import com.jd.supplier.model.TaskParaRela;
import com.jd.supplier.service.TaskNodeService;
@Service("taskNodeService")
public class TaskNodeServiceImpl extends BaseServiceImpl<TaskNode, Long>
		implements TaskNodeService {
	
	private static Logger logger = Logger.getLogger(TaskNodeServiceImpl.class);

	/**
	 * 任务节点
	 */
	@Autowired
	private TaskNodeDao taskNodeDao;
	
	/**
	 * 任务参数关系
	 */
	@Autowired
	private TaskParaRelaDao taskParaRelaDao;
	
	/**
	 * 任务参数
	 */
	@Autowired
	private TaskParaDao taskParaDao;
	
	/**
	 * 任务节点指派
	 */
	@Autowired
	private TaskNodeAssignmentDao taskNodeAssignmentDao;
	
	/**
	 * 邮件发送服务
	 */
	@Autowired
	private MailSendService mailSendService;
	
	/**
	 * 用户
	 */
	@Autowired
	private UserDao userDao;
	
	/**
	 * 需求
	 */
	@Autowired
	private RequirementDao requirementDao;
	
	/**
	 * 任务单
	 */
	@Autowired
	private TaskDao taskDao;
	
	/**
	 * 角色
	 */
	@Autowired
	private RoleDao roleDao;
	
	private static HashSet<Integer> typeSet;
	
	private static String sep = System.getProperty("line.separator");

	public TaskNodeDao getDao() {
		
		return taskNodeDao;
	}

	/**
	 * 查询节点集合
	 */
	@Override
	public List<TaskNode> findNodeList(Map<String, Object> map) {
		return taskNodeDao.findNodeList(map);
	}

	/**
	 * 任务节点初始化
	 */
	@Override
	public synchronized Boolean addInitNode(Long taskId) {
		if(taskId != null && taskId>0){
			Integer count = taskNodeDao.findNodeCountByTaskId(taskId);
			if(count>0)
				return true;
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("type", TaskParaEnum.ORGANIZATION.getType());
			List<TaskPara> organizationList = taskParaDao.findParaList(map);
			for(TaskPara para:organizationList){
				Long id = null;
				map.put("type", TaskParaEnum.TASK.getType());
				map.put("tempalteType", TaskConstant.TaskInit.TEMPLATE_DEFAULT);
				map.put("organizationId", para.getId());
				List<TaskParaRela> relaList = taskParaRelaDao.findList(map);
				for(int i=0;i<relaList.size();i++){
					if(relaList.get(i) != null){
						TaskNode taskNode = new TaskNode();
						if(id != null){
							taskNode.setParentId(id);
							taskNode.setNodeStatus(NodeStatusEnum.INITIAL.getType());
						}else{
							taskNode.setNodeStatus(NodeStatusEnum.RUNNING.getType());
						}
						taskNode.setTaskNodeId(relaList.get(i).getTaskNodeId());
						taskNode.setRoleId(relaList.get(i).getRoleId());
						taskNode.setOrganizationId(Integer.valueOf(String.valueOf(para.getId())));
						taskNode.setNodeName(relaList.get(i).getName());
						taskNode.setStatus(DeleteEnum.NO.getStatus());
						taskNode.setTaskId(taskId);
						taskNode.setUrl(relaList.get(i).getUrl());
						taskNode.setType(relaList.get(i).getNodeType());
						id = getDao().insert(taskNode);
					}
				}
			}
		}
		return true;
	}

	/**
	 * 查找初始节点集合
	 */
	@Override
	public TaskNode findInitNode(Long nodeId,Integer nodeStatus) {
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("id", nodeId);
		map.put("nodeStatus", nodeStatus);
		return getDao().fintInitNode(map);
	}

	/**
	 * 根据角色id查找关联用户
	 */
	@Override
	public List<User> findTaskAssignUsers(String roleCode) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("roleCode", roleCode);
		return taskNodeAssignmentDao.findTaskAssignUsers(map);
	}

	/**
	 * 添加任务指派
	 */
	@Override
	public Boolean addSaveAssignment(TaskNodeAssignment taskNodeAssignment,String nodeName) {
		Long nodeId = taskNodeAssignment.getNodeId();
		//根据当前节点取相应的任务指派记录
		TaskNodeAssignment assignment = taskNodeAssignmentDao.getAssignmentByNodeId(nodeId);
		taskNodeAssignment.setAssignStatus(AssignmentStatusEnum.ASSIGNED.getType());
		if(assignment == null){
			taskNodeAssignment.setAssignedId(ComUtils.getCurrentLoginUser().getId());
			taskNodeAssignmentDao.insert(taskNodeAssignment);
		}else{
			taskNodeAssignment.setId(assignment.getId());
			taskNodeAssignment.setNodeId(null);
			taskNodeAssignment.setReason("");
			taskNodeAssignmentDao.update(taskNodeAssignment);
		}
		//
		TaskNode taskNode = taskNodeDao.getByParentId(nodeId);
		TaskNode node = new TaskNode(taskNode.getId());
		node.setUserId(taskNodeAssignment.getUserId());
		taskNodeDao.update(node);
		
		//更新节点状态
		updateNodeStatus(new TaskNode(taskNode.getParentId()));
		
		//发邮件通知被指派的人
		taskNode.setNodeName(nodeName);
		EmailClaimer(taskNodeAssignment,taskNode);
		return true;
	}
	
	

	/**
	 * 检查节点状态
	 */
	@Override
	public Integer checkNodeStatus(Long nodeId,String roleCode,Integer nodeStatus) {
		if(nodeStatus == null)
			nodeStatus = NodeStatusEnum.COMPLETED.getType();
		Map<String,Object> map = new HashMap<String,Object>();
		Long userId = ComUtils.getCurrentLoginUser().getId();
		map.put("id", nodeId);
		map.put("nodeStatus", nodeStatus);
		map.put("userId", userId);
		List<Role> roleList = roleDao.findByUserId(userId);
		for(Role role:roleList){
			if(roleCode.equalsIgnoreCase(role.getRoleCode())){
				//检查userId
				if(!roleCode.equalsIgnoreCase(RoleTypeEnum.FINAL_SELECTON_CONFIRM.getCode())){
					map.put("condition", 1);
					if(getDao().findNodeCountByStatus(map) == 0)
						return CheckNodeStatusEnum.NO_PRIVILEGE.getType();
				}
				//检查当前节点是否已完成
				map.put("condition", 2);
				if(getDao().findNodeCountByStatus(map) == 0){
					TaskNode node = getDao().get(nodeId);
					if(node!=null){
						if(node.getType()==4)
							return CheckNodeStatusEnum.PRE_SELE_COMPLETED.getType();
						if(node.getType()==5)
							return CheckNodeStatusEnum.FINAL_SELE_COMPLETED.getType();
					}
					return CheckNodeStatusEnum.PRE_SELE_COMPLETED.getType();
				}
				//检查当前节点是否有未完成的前置节点
				map.put("condition", 3);
				if(getDao().findNodeCountByStatus(map) == 0)
					return CheckNodeStatusEnum.NO_ACCESS.getType();
				//返回通过
				return CheckNodeStatusEnum.SUCCESS.getType();
			}
		}
		return CheckNodeStatusEnum.NO_PRIVILEGE.getType();
	}
	
	/**
	 * 更新节点状态
	 */
	@Override
	public Boolean updateNodeStatus(TaskNode taskNode) {
		if(taskNode != null&&taskNode.getId()!=null){
			taskNode.setNodeStatus(NodeStatusEnum.COMPLETED.getType());
			taskNodeDao.update(taskNode);
			TaskNode node = taskNodeDao.getByParentId(taskNode.getId());
			if(node != null ){
				//更新子节点状态
				taskNode = new TaskNode();
				taskNode.setId(node.getId());
				taskNode.setNodeStatus(NodeStatusEnum.RUNNING.getType());
				taskNodeDao.update(taskNode);
			}else{
				node = taskNodeDao.get(taskNode.getId());
			}
			
			//更新主任务的状态为下一节点的状态
			if(initType(node).contains(node.getType()))
				updateTaskStatus(node);
		}
		return true;
	}
	
	/**
	 * 获取节点集合
	 * @param node
	 * @return
	 */
	private HashSet<Integer> getTypeSet(TaskNode node){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("orgCode", TaskConstant.ParaCode.Org.SUPPLIER_CODE);
		if(node != null)
			map.put("taskId", node.getTaskId());
		List<TaskNode> nodeList = taskNodeDao.findNodeListByOrgCode(map);
		HashSet<Integer> set = new HashSet<Integer>();
		for(TaskNode t:nodeList){
			set.add(t.getType());
		}
		return set;
	}
	
//	private HashSet<Integer> initType(TaskNode taskNode){
//		if(typeSet == null)
//			typeSet = new HashSet<Integer>();
//		synchronized (typeSet) {
//			if(typeSet.size()==0){
//				typeSet = getTypeSet(taskNode);
//			}
//		}
//		return typeSet;
//	}
	
	/**
	 * 初始化节点配置
	 * @param taskNode
	 * @return
	 */
	private synchronized HashSet<Integer> initType(TaskNode taskNode){
		if(typeSet == null || typeSet.size()==0){
			typeSet = getTypeSet(taskNode);
		}
		return typeSet;
	}
	
	/**
	 * 更新任务状态
	 * @param node
	 */
	private void updateTaskStatus(TaskNode node){
		Task task = new Task();
		task.setId(node.getTaskId());
		task.setTaskStatus(node.getType());
		taskDao.update(task);
	}

	/**
	 * 添加任务认领
	 */
	@Override
	public Boolean updateTaskClaim(TaskNode taskNode) {
//		TaskNode parent = taskNodeDao.getParent(taskNode.getId());
//		TaskNodeAssignment assignment = taskNodeAssignmentDao.getAssignmentByNodeId(parent.getId());
//		taskNodeAssignmentDao.update(new TaskNodeAssignment(assignment.getId(), AssignmentStatusEnum.CLAIMED.getType()));
//		updateNodeStatus(new TaskNode(taskNode.getId()));
//		return true;
		String roleCode = taskNode.getRoleCode();
		taskNode = taskNodeDao.get(taskNode.getId());
		Long userId = ComUtils.getCurrentLoginUser().getId();
		if(checkLegalUser(userId,roleCode)){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("taskId", taskNode.getTaskId());
			map.put("roleId", taskNode.getRoleId());
			List<Long> idList = taskNodeDao.findIdList(map);
			TaskNode node = null;
			for(int i=0;i<idList.size();i++){
				node = new TaskNode(idList.get(i));
				node.setUserId(userId);
				taskNodeDao.update(node);
			}
		}
//		TaskNode parent = taskNodeDao.getParent(taskNode.getId());
//		TaskNodeAssignment assignment = taskNodeAssignmentDao.getAssignmentByNodeId(parent.getId());
//		taskNodeAssignmentDao.update(new TaskNodeAssignment(assignment.getId(), AssignmentStatusEnum.CLAIMED.getType()));
		updateNodeStatus(new TaskNode(taskNode.getId()));
		return true;
	}
	
	/**
	 * 检查是否为合法用户
	 * @param userId
	 * @param roleCode
	 * @return
	 */
	private Boolean checkLegalUser(Long userId,String roleCode){
		List<Role> roleList = roleDao.findByUserId(userId);
		for(Role role:roleList){
			if(roleCode.equalsIgnoreCase(role.getRoleCode()))
				return true;
		}
		return false;
	}

	/**
	 * 判断是否存在符合条件的认领
	 */
	@Override
	public Boolean ifExistsClaim(Long nodeId,Long userId) {
		Map<String, Object> map = new HashMap<String,Object>();
		TaskNode parent = taskNodeDao.getParent(nodeId);
		if(parent != null)
			map.put("nodeId", parent.getId());
		else
			map.put("nodeId", nodeId);
		map.put("userId", userId);
		if(taskNodeAssignmentDao.getClaimCount(map)>0)
			return true;
		return false;
	}

	@Override
	public TaskNode getByParentId(Long id) {
		return getDao().getByParentId(id);
	}

	
	/**
	 * 更新任务驳回
	 */
	@Override
	public Boolean updateTaskReject(TaskNode taskNode) {
		if(taskNode != null){
			Long id = taskNode.getId();
			TaskNode parent = taskNodeDao.getParent(id);
			if(parent != null){
				TaskNodeAssignment assignment = taskNodeAssignmentDao.getAssignmentByNodeId(parent.getId());
				if(assignment != null){
					emailAssignedPerson(assignment, taskNode);
					taskNodeAssignmentDao.update(new TaskNodeAssignment(assignment.getId(),
							0L,AssignmentStatusEnum.UNASSIGNED.getType(),taskNode.getReason()));
					updateRejectNodeStatus(new TaskNode(taskNode.getId()));
				}
				//驳回后更新指派状态为未指派
//			assignment.setAssignStatus(AssignmentStatusEnum.UNASSIGNED.getType());
				
			}
		}
		return true;
	}
	
	/**
	 * 发邮件给分配任务的人
	 * @param assignment
	 * @param taskNode
	 */
	private void emailAssignedPerson(TaskNodeAssignment assignment,TaskNode taskNode){
		User assignedPerson = userDao.get(assignment.getAssignedId());
		String rece[] = {assignedPerson.getEmail()};
		Requirement requirement = requirementDao.getByNodeId(assignment.getNodeId());
		User user = ComUtils.getCurrentLoginUser();
		email2Assigned(rece, requirement.getProjectName(),assignedPerson.getRealName(),user.getRealName(),taskNode.getReason(),taskNode.getNodeName());
	}
	
	/**
	 * 发送邮件通知被指派人
	 * @param userId
	 * @param nodeId
	 */
	private void EmailClaimer(TaskNodeAssignment assignment,TaskNode taskNode){
//		User user = ComUtils.getCurrentLoginUser();
		User claimer = userDao.get(assignment.getUserId());
		String rece[] = {claimer.getEmail()};
		Requirement requirement = requirementDao.getByNodeId(taskNode.getId());
		email2Claimed(rece, requirement.getProjectName(), claimer.getRealName(),taskNode.getNodeName());
	}
	
	/**
	 * 发给任务分配人
	 * @param rece
	 * @param projectName
	 * @param userName
	 * @param reason
	 */
	private void email2Assigned(String rece[],String projectName,String assigner,String rejector,String reason,String nodeName){
		String subject = new StringBuffer(nodeName)
		.append(TaskConstant.Tips.REJECT).append(TaskConstant.Tips.NOTICE).toString();
		String content = new StringBuffer(assigner).append(EmailConstant.Task.HELLO)
						.append(EmailConstant.Task.REJECT_ASSIGNMENT)
						.append(rejector).append(EmailConstant.Task.REJECT)
						.append(EmailConstant.Task.CHECK)
						.append(sep)
						.append(TaskConstant.Noun.REASON).append(TaskConstant.Symbol.COLON)
						.append(reason).toString();
		String sign = getSign();
		
		try{
			mailSendService.sendTemplateMail(
					rece, subject,
					"mail/AssignmentEmail.vm", getMailMap(content,sign), new File[] {});
//			mailSendService.sendSimpleMail(rece, subject, content);
		}catch(Exception e){
			logger.error("发邮件失败："+e);
		}
	}
	
	/**
	 * 任务指派通知
	 * @param rece
	 * @param projectName
	 * @param userName
	 * @param nodeName
	 */
	private void email2Claimed(String rece[],String projectName,String userName,String nodeName){
		String subject = new StringBuffer(nodeName)
		.append(TaskConstant.Tips.NOTICE).toString();
		String content = new StringBuffer(userName).append(EmailConstant.Task.HELLO)
						.append(EmailConstant.Task.ASSIGNMENT)
						.append(EmailConstant.Task.CHECK).toString();
		String sign = getSign();
		try{
			mailSendService.sendTemplateMail(
					rece, subject,
					"mail/AssignmentEmail.vm", getMailMap(content,sign), new File[] {});
//			mailSendService.sendSimpleMail(rece, subject, content);
		}catch(Exception e){
			logger.error("发邮件失败："+e);
		}
	}

	/**
	 * 更新驳回节点状态
	 */
	@Override
	public Boolean updateRejectNodeStatus(TaskNode taskNode) {
		TaskNode parent= null;
		if(taskNode != null){
			taskNode.setNodeStatus(NodeStatusEnum.INITIAL.getType());
			parent = taskNodeDao.getParent(taskNode.getId());
		}
		taskNodeDao.update(taskNode);
		taskNode = new TaskNode();
		if(parent != null)
			taskNode.setId(parent.getId());
		else
			taskNode.setId(taskNode.getId());
		taskNode.setNodeStatus(NodeStatusEnum.RUNNING.getType());
		taskNodeDao.update(taskNode);
		//更新主任务的状态为下一节点的状态
		
		if(parent != null && initType(parent).contains(parent.getType()))
			updateTaskStatus(parent);
		return true;
	}

	/**
	 * 添加入围信息完成
	 */
	@Override
	public Boolean addFinalSelectionComplete(Long id) {
		//插入一条入围分配信息
		//通过指定角色来查询相应的user
		TaskNode taskNode = taskNodeDao.get(id);
		TaskNodeAssignment ssignment = new TaskNodeAssignment();
		ssignment.setNodeId(taskNode.getId());
		List<User> userList = findTaskAssignUsers(RoleTypeEnum.FINAL_SELECTON_CONFIRM.getCode());
		User u = null;
		if(userList != null && userList.size()>0){
			u = userList.get(0);
			ssignment.setUserId(u.getId());
		}
		addSaveAssignment(ssignment,TaskNodeEnum.FINAL_SELECTION.getName());
		//更新状态
		updateNodeStatus(new TaskNode(taskNode.getId()));
		return true;
	}

	/**
	 * 更新入围确认驳回
	 */
	@Override
	public Boolean updateFinalSelectionReject(TaskNode taskNode) {
		if(taskNode != null){
			taskNode.setNodeName(TaskNodeEnum.FINAL_SELECTION_CONFRIM.getName());
			return updateTaskReject(taskNode);
		}
		return true;
	}

	/**
	 * 更新认领驳回
	 */
	@Override
	public Boolean updateClaimReject(TaskNode taskNode) {
		if(taskNode != null){
			taskNode.setNodeName(TaskNodeEnum.TASK_ASSIGNMENT.getName());
			return updateTaskReject(taskNode);
		}
		return true;
	}

	@Override
	public Boolean addAssignment(TaskNodeAssignment taskNodeAssignment) {
		return addSaveAssignment(taskNodeAssignment, TaskNodeEnum.TASK_ASSIGNMENT.getName());
	}

	/**
	 * 入围确认完成
	 */
	@Override
	public Boolean addFinalSelectionConfirmCompleted(Long id) {
		TaskNode node = new TaskNode(id);
		updateTaskClaim(node);
		updateNodeStatus(node);
		return true;
	}
	
	/**
	 * 获取签名
	 * @return
	 */
	private String getSign(){
		return EmailConstant.Task.SIGN_NAME;
	}
	
	/**
	 * 获取发邮件日期
	 * @return
	 */
	private String getMailDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	
	/**
	 * 获取邮件信息
	 * @param content
	 * @param sign
	 * @return
	 */
	private Map<String,Object> getMailMap(String content,String sign){
		Map<String,Object> map = new ConcurrentHashMap<String, Object>();
		map.put("content", content);
		map.put("autoSend", EmailConstant.Task.AUTO_SEND);
		map.put("sign", sign);
		map.put("mailDate", getMailDate());
		return map;
	}

}
