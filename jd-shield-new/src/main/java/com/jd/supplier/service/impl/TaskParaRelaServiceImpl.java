/**
 * 
 */
package com.jd.supplier.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.common.constants.TaskConstant;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.TaskParaRelaDao;
import com.jd.supplier.model.TaskParaRela;
import com.jd.supplier.service.TaskParaRelaService;

/**
 * 任务参数关系服务
 * @author wangchanghui
 *
 */
@Service("taskParaRelaService")
public class TaskParaRelaServiceImpl extends
		BaseServiceImpl<TaskParaRela, Long> implements TaskParaRelaService {


	@Autowired
	private TaskParaRelaDao taskParaRelaDao;
	
	public TaskParaRelaDao getDao() {
		return taskParaRelaDao;
	}

	/**
	 * 批量添加关系
	 */
	@Override
	public Boolean addBatch(TaskParaRela taskParaRela, String ids) {
		if(StringUtils.isNotBlank(ids)){
			String idArry[] = ids.split(",");
			Long id = null;
			for(int i=0;i<idArry.length;i++){
				if(id != null)
					taskParaRela.setParentId(id);
				taskParaRela.setTaskNodeId(Integer.valueOf(idArry[i]));
				taskParaRela.setTemplateType(TaskConstant.TaskInit.TEMPLATE_DEFAULT);
				id = getDao().insert(taskParaRela);
			}
		}
		return true;
	}
}
