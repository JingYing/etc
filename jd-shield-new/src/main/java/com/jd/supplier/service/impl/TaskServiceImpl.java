package com.jd.supplier.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.TaskDao;
import com.jd.supplier.model.Task;
import com.jd.supplier.service.TaskService;
/**
 * 任务单服务实现
 * @author wangchanghui
 *
 */
@Service("taskService")
public class TaskServiceImpl extends BaseServiceImpl<Task, Long> implements TaskService{

	@Autowired
	private TaskDao taskDao;
	
	public TaskDao getDao() {
		return taskDao;
	}

	/**
	 * 新建任务做初始化工作
	 */
	@Override
	public Boolean init(Map<String, Object> map) {
		//选模板
		//导入关系
		//初始化节点
		//供应商管理部 00008410 质量管理部 00008413 招标一部00008411 招标二部 00008412
//		Map<String,Object> map = new HashMap<String,Object> ();
//		map.put("type", TaskParaEnum.ROLE.getType());
//		List<TaskPara> roleList = taskParaService.findParaList(map);
//		map.put("type", TaskParaEnum.ORGANIZATION.getType());
//		List<TaskPara> organizationList = taskParaService.findParaList(map);
		return true;
	}

}
