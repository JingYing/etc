package com.jd.supplier.service;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.Notice;

public interface NoticeService extends BaseService<Notice, Long> {
}
