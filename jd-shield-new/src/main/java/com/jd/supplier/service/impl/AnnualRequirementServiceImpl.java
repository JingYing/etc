package com.jd.supplier.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.official.core.dao.BaseDao;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.official.modules.system.service.UserService;
import com.jd.supplier.dao.AnnualRequirementDao;
import com.jd.supplier.model.AnnualRequirement;
import com.jd.supplier.service.AnnualRequirementAuditService;
import com.jd.supplier.service.AnnualRequirementService;
@Service("annualRequirementService")
public class AnnualRequirementServiceImpl extends
		BaseServiceImpl<AnnualRequirement, Long> implements
		AnnualRequirementService {
	@Autowired
	private AnnualRequirementDao annualRequirementDao;
	@Autowired
	private UserService userService;
	@Autowired
	private AnnualRequirementAuditService annualRequirementAuditService;
	
	private final static String      XLS    = "xls";
	private final static String      XLSX   = "xlsx";
	

	@Override
	public Long insert(AnnualRequirement entity) {
		// TODO Auto-generated method stub
		annualRequirementDao.insert(entity);
		return null;
	}

	@Override
	public Integer getMaxRequirementID() {
		return (Integer) annualRequirementDao.get("getMaxRequirementID", null);
	}

	@Override
	public BaseDao<AnnualRequirement, Long> getDao() {
		// TODO Auto-generated method stub
		return annualRequirementDao;
	}

	
	
	
	
}
