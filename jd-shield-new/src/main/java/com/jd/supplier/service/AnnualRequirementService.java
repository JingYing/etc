package com.jd.supplier.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.AnnualRequirement;

public interface AnnualRequirementService extends
		BaseService<AnnualRequirement, Long> {

	Integer getMaxRequirementID();
	
	
	
}
