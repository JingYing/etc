package com.jd.supplier.service;

import java.util.List;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.RequirementTask;

public interface RequirementTaskService extends BaseService<RequirementTask, Long> {
    List<RequirementTask> deleteOnlyUnused(String ql, Object... values);

    List<RequirementTask> findRequirementTaskListByIds(final String... values);

    List<RequirementTask> findRequirementTaskListByIds(final RequirementTask... values);

    List<RequirementTask> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId);

    Integer getMaxRequirementTaskID();
}
