package com.jd.supplier.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.NoticeDao;
import com.jd.supplier.model.Notice;
import com.jd.supplier.service.NoticeService;

@Service("noticeService")
@Transactional
public class NoticeServiceImpl extends BaseServiceImpl<Notice, Long> implements NoticeService {
    @Autowired
    private NoticeDao noticeDao;
    
    public NoticeDao getDao() {
	return noticeDao;
    }
    
  

}
