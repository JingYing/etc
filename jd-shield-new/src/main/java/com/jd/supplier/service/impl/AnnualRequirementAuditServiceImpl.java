package com.jd.supplier.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.AnnualRequirementAuditDao;
import com.jd.supplier.model.AnnualRequirementAudit;
import com.jd.supplier.service.AnnualRequirementAuditService;
@Service("annualRequirementAuditService")
public class AnnualRequirementAuditServiceImpl extends BaseServiceImpl<AnnualRequirementAudit, Long>
		implements AnnualRequirementAuditService {
	@Autowired
	private AnnualRequirementAuditDao annualRequirementAuditDao;
	
	public AnnualRequirementAuditDao getDao() {
		// TODO Auto-generated method stub
		return annualRequirementAuditDao;
	}
	
	@Override
	public void updateRejectAllReqs(String ids, String approvalReply) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		String[] idStrings=ids.split(",");
		List<String> idsList=Arrays.asList(idStrings);
		map.put("idsList", idsList);
		map.put("approvalReply", approvalReply);
		annualRequirementAuditDao.update("rejectAllReqs", map);
		
	}

	@Override
	public void updateAuditAllReqs(String reqNumber,AnnualRequirementAudit annualRequirementAudit) {
		// TODO Auto-generated method stub
		Map<String, Object> map=new HashMap<String, Object>();
		String[] reqNumberString =reqNumber.split(",");
		List<String> reqNumberList=Arrays.asList(reqNumberString);
		map.put("reqNumberList", reqNumberList);
		map.put("annualRequirementAudit", annualRequirementAudit);
		annualRequirementAuditDao.update("auditAllReqs", map);
	}

}
