/**
 * 
 */
package com.jd.supplier.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.TaskParaDao;
import com.jd.supplier.model.TaskPara;
import com.jd.supplier.service.TaskParaService;

/**
 * 任务参数服务
 * @author wangchanghui
 *
 */
@Service("taskParaService")
public class TaskParaServiceImpl extends BaseServiceImpl<TaskPara, Long>
		implements TaskParaService {
	
	@Autowired
	private TaskParaDao taskParaDao;
	
	public TaskParaDao getDao() {
		return taskParaDao;
	}

	/**
	 * 查询参数集合
	 */
	@Override
	public List<TaskPara> findParaList(Map<String, Object> map) {
		return getDao().findParaList(map);
	}

	/**
	 * 查询需要绑定的参数集合
	 */
	@Override
	public List<TaskPara> findToBeBindedList(Map<String, Object> map) {
		return null;
	}


}
