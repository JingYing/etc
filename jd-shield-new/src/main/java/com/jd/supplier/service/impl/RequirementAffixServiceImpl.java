package com.jd.supplier.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.controller.vo.FileAffixVO;
import com.jd.supplier.controller.vo.RequirementVO;
import com.jd.supplier.dao.RequirementAffixDao;
import com.jd.supplier.model.RequirementAffix;
import com.jd.supplier.service.RequirementAffixService;
import com.jd.supplier.util.SupplierConstant;

@Service("this")
@Transactional
public class RequirementAffixServiceImpl extends BaseServiceImpl<RequirementAffix, Long> implements
	RequirementAffixService {
    @Autowired
    private RequirementAffixDao requirementAffixDao;
    

    public RequirementAffixDao getDao() {
	return requirementAffixDao;
    }
    
    /*  author  duandongdong
     *  desc 插入或者更新附件表
     *  fileAffixVO要封装filename属性和type属性，否则不能插入附件值
     *  bussineId 具体的业务的id
     *  bussineType 具体的业务类型，可参见supplierContant类
     */
    @Override
    public  void insert(FileAffixVO fileAffixVO, long bussineId,String bussineType) {
	// 存附件
	RequirementAffix requirementAffix = new RequirementAffix();
	//if (fileAffixVO.getFileType() != null && !fileAffixVO.getFileType().equals("")) {
	    String filename1 = fileAffixVO.getFileName1();
	    if (filename1 != null && !filename1.equals("")) {
	    String[] filename1Str = filename1.split(",");
		if (filename1Str != null && filename1Str.length > 0) {
		    // 是否已上传附件
		    Map<String, Object> map = new HashMap<String, Object>();
		    map.put("bussineType", bussineType);
		    map.put("bussineId", bussineId);
		    map.put("fileType", "1");
		    map.put("status", SupplierConstant.AVAILABLESTATUS);
		    List<RequirementAffix> requirementAffixList = this.find("findByMap", map);
		    if (requirementAffixList != null && requirementAffixList.size() > 0) {
			for (int j = 0; j < requirementAffixList.size(); j++) {
			    RequirementAffix requirementAffixTmp = (RequirementAffix) requirementAffixList.get(j);
			    this.delete(requirementAffixTmp.getId());
			}
		    }
		    for (int j = 0; j < filename1Str.length; j++) {
			String[] filename1StrArr=filename1Str[j].split("@@");
			if(filename1StrArr!=null){
			    	requirementAffix.setBussineType(bussineType);
				requirementAffix.setFileType("1");
				requirementAffix.setFileName(filename1StrArr[0]);
				requirementAffix.setFileKey(filename1StrArr[1]);
				requirementAffix.setBussineId(bussineId);
				this.insert(requirementAffix);
			}
		    }
		}
	    }

	    String filename2 = fileAffixVO.getFileName2();
	    if (filename2 != null && !filename2.equals("")) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bussineType", bussineType);
		map.put("bussineId", bussineId);
		map.put("fileType", "2");
		map.put("status", SupplierConstant.AVAILABLESTATUS);
		List<RequirementAffix> requirementAffixList = this.find("findByMap", map);
		if (requirementAffixList != null && requirementAffixList.size() > 0) {
		    for (int j = 0; j < requirementAffixList.size(); j++) {
			RequirementAffix requirementAffixTmp = (RequirementAffix) requirementAffixList.get(j);
			this.delete(requirementAffixTmp.getId());
		    }
		}
		String[] filename2Str = filename2.split(",");
		for (int j = 0; j < filename2Str.length; j++) {
		    String[] filename2StrArr=filename2Str[j].split("@@");
			if(filename2StrArr!=null){
			    requirementAffix.setBussineType(bussineType);
			    requirementAffix.setFileType("2");
			    requirementAffix.setFileName(filename2StrArr[0]);
			    requirementAffix.setFileKey(filename2StrArr[1]);
			    requirementAffix.setBussineId(bussineId);
			    this.insert(requirementAffix);
			}
		}
	    }

	    String filename3 = fileAffixVO.getFileName3();
	    if (filename3 != null && !filename3.equals("")) {
	    String[] filename3Str = filename3.split(",");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bussineType", bussineType);
		map.put("bussineId", bussineId);
		map.put("fileType", "3");
		map.put("status", SupplierConstant.AVAILABLESTATUS);
		List<RequirementAffix> requirementAffixList = this.find("findByMap", map);
		if (requirementAffixList != null && requirementAffixList.size() > 0) {
		    for (int j = 0; j < requirementAffixList.size(); j++) {
			RequirementAffix requirementAffixTmp = (RequirementAffix) requirementAffixList.get(j);
			this.delete(requirementAffixTmp.getId());
		    }
		}
		for (int j = 0; j < filename3Str.length; j++) {
		    String[] filename3StrArr=filename3Str[j].split("@@");
			if(filename3StrArr!=null){
			    requirementAffix.setBussineType(bussineType);
			    requirementAffix.setFileType("3");
			    requirementAffix.setFileName(filename3StrArr[0]);
			    requirementAffix.setFileKey(filename3StrArr[1]);
			    requirementAffix.setBussineId(bussineId);
			    this.insert(requirementAffix);
			}
		}
	    }

	    String filename4 = fileAffixVO.getFileName4();
	    if (filename4 != null && !filename4.equals("")) {
	    String[] filename4Str = filename4.split(",");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bussineType", bussineType);
		map.put("bussineId", bussineId);
		map.put("fileType", "4");
		map.put("status", SupplierConstant.AVAILABLESTATUS);
		List<RequirementAffix> requirementAffixList = this.find("findByMap", map);
		if (requirementAffixList != null && requirementAffixList.size() > 0) {
		    for (int j = 0; j < requirementAffixList.size(); j++) {
			RequirementAffix requirementAffixTmp = (RequirementAffix) requirementAffixList.get(j);
			this.delete(requirementAffixTmp.getId());
		    }
		}
		if (filename4Str != null && filename4Str.length > 0) {
		    for (int j = 0; j < filename4Str.length; j++) {
			String[] filename4StrArr=filename4Str[j].split("@@");
			if(filename4StrArr!=null){
			    requirementAffix.setBussineType(bussineType);
			    requirementAffix.setFileType("4");
			    requirementAffix.setFileName(filename4StrArr[0]);
			    requirementAffix.setFileKey(filename4StrArr[1]);
			    requirementAffix.setBussineId(bussineId);
			    this.insert(requirementAffix);
			}
			
		    }
		}
	    }

	    String filename5 = fileAffixVO.getFileName5();
	    if (filename5 != null && !filename5.equals("")) {
	    String[] filename5Str = filename5.split(",");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bussineType", bussineType);
		map.put("bussineId", bussineId);
		map.put("fileType", "5");
		map.put("status", SupplierConstant.AVAILABLESTATUS);
		List<RequirementAffix> requirementAffixList = this.find("findByMap", map);
		if (requirementAffixList != null && requirementAffixList.size() > 0) {
		    for (int j = 0; j < requirementAffixList.size(); j++) {
			RequirementAffix requirementAffixTmp = (RequirementAffix) requirementAffixList.get(j);
			this.delete(requirementAffixTmp.getId());
		    }
		}
		if (filename5Str != null && filename5Str.length > 0) {
		    for (int j = 0; j < filename5Str.length; j++) {
			String[] filename5StrArr=filename5Str[j].split("@@");
			if(filename5StrArr!=null){
			    requirementAffix.setBussineType(bussineType);
    			    requirementAffix.setFileType("5");
    			    requirementAffix.setFileName(filename5StrArr[0]);
    			    requirementAffix.setFileKey(filename5StrArr[1]);
    			    requirementAffix.setBussineId(bussineId);
    			    this.insert(requirementAffix);
			}
		    }
		}
	    }

	//}
    }

    @Override
    public  List<RequirementAffix> findRequirementAffixListByReqId(Long reqId){
	return  requirementAffixDao.find("findRequirementAffixListByReqId", reqId);
    }
    

    @Override
    public Integer getMaxRequirementAffixID() {
	return (Integer) requirementAffixDao.get("getMaxRequirementAffixID", null);
    }

    @Override
    public List<RequirementAffix> findRequirementAffixListByIds(String... values) {
	List<RequirementAffix> list = new ArrayList<RequirementAffix>();
	for (int i = 0; i < values.length; i++) {
	    RequirementAffix authExpression = (RequirementAffix) get(Long.valueOf(values[i]));
	    if (authExpression != null)
		list.add(authExpression);
	}
	return list;

    }

    @Override
    public List<RequirementAffix> findRequirementAffixListByIds(RequirementAffix... values) {
	List<RequirementAffix> list = new ArrayList<RequirementAffix>();
	for (int i = 0; i < values.length; i++)
	    list.add((RequirementAffix) get(values[i].getId()));
	return list;
    }

    /**
     * 
     * @desc 获得指定业务数据对应的需求附件列表
     * @author ddd
     * @param businessType
     * @param businessId
     * @return
     */
    @Override
    public List<RequirementAffix> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId) {
	Map<String, String> map = new HashMap<String, String>();
	map.put("businessType", businessType);
	map.put("businessId", businessId);
	List<RequirementAffix> list = requirementAffixDao.find("findExpressionByBusinessIdAndBusinessType", map);
	return list;
    }
    /**
     * 
     * @desc 逻辑删除文件
     * @author hetengfei
     * @param ids
     */

	@Override
	public void deleteFromIds(String ids) {
				Map<String, Object> map = new HashMap<String, Object>();
				String[] idStrings=ids.split(",");
				List<String> idsList=Arrays.asList(idStrings);
				map.put("idsList", idsList);
				requirementAffixDao.update("deleteFromIds", map);
	}

	/**
     * 
     * @desc 获取文件对象
     * @author hetengfei
     * @param businessType
     * @param businessId
     * @param fileType
     * @return List<RequirementAffix>
     */
	@Override
	public List<RequirementAffix> findExpressionByParams(String businessType,
			String businessId, String fileType) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("businessType", businessType);
		map.put("businessId", businessId);
		map.put("businessId", fileType);
		List<RequirementAffix> list = requirementAffixDao.find("findExpressionByParams", map);
		return list;
	}

}
