package com.jd.supplier.service;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.AnnualRequirementAudit;

public interface AnnualRequirementAuditService extends BaseService<AnnualRequirementAudit, Long> {
	
	void updateRejectAllReqs(String ids,String approvalReply);
	void updateAuditAllReqs(String reqNumber,AnnualRequirementAudit annualRequirementAudit);

}
