package com.jd.supplier.service;

import java.util.Map;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.Task;
/**
 * 任务单服务接口
 * @author wangchanghui
 *
 */
public interface TaskService extends BaseService<Task, Long>{
	/**
	 * 节点初始化
	 * @param map
	 * @return
	 */
	Boolean init(Map<String,Object> map);
}
