package com.jd.supplier.service.impl;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.common.constants.SupplierConstant;
import com.jd.common.util.Util;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.mail.MailSendService;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.controller.vo.SpecialSupplierVO;
import com.jd.supplier.dao.IContactsDao;
import com.jd.supplier.dao.IEnterpriseDao;
import com.jd.supplier.dao.IEquipDao;
import com.jd.supplier.dao.IFinanceDao;
import com.jd.supplier.dao.IMaterialDao;
import com.jd.supplier.dao.IOtherDao;
import com.jd.supplier.dao.IQualityDao;
import com.jd.supplier.dao.ISupplierDao;
import com.jd.supplier.model.Contacts;
import com.jd.supplier.model.Enterprise;
import com.jd.supplier.model.Equip;
import com.jd.supplier.model.Finance;
import com.jd.supplier.model.Material;
import com.jd.supplier.model.Other;
import com.jd.supplier.model.Quality;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.SupplierDto;
import com.jd.supplier.service.ISupplierService;

@Service("supplierService")
@Transactional
public class SupplierServiceImpl extends BaseServiceImpl<Supplier, Long>
		implements ISupplierService {
	static Logger log = Logger.getLogger(SupplierServiceImpl.class);
	@Autowired
	private ISupplierDao supplierDao;

	public ISupplierDao getDao() {
		return supplierDao;
	}

	@Autowired
	private IContactsDao contactsDao;
	@Autowired
	private IEnterpriseDao enterpriseDao;
	@Autowired
	private IFinanceDao financeDao;
	@Autowired
	private IOtherDao otherDao;
	@Autowired
	private IQualityDao qulityDao;
	@Autowired
	private IMaterialDao materialDao;
	@Autowired
	private IEquipDao equipDao;
	@Resource
	private MailSendService mailSendService;

	@Override
	public String insertImportSupplier(List<SupplierDto> recordList,
			List<String> resultString, String fileName, String results) {
		String result = SupplierConstant.IMPORT_RESULT;
		int tempcount = 0;
		for (SupplierDto supplierDto : recordList) {
			Supplier temp = supplierDto.getSupplier();
			List<Supplier> supCount = supplierDao.find(temp);
			if (supCount.size() > 0) {
				resultString.add("供应商全称[ " + temp.getName()
						+ " ],在数据库中已存在，重复请查证");
				tempcount++;
			}
		}
		if (tempcount == 0) {
			for (SupplierDto supplierDto : recordList) {
				Supplier supplier = supplierDto.getSupplier();
				// 首先插入基本信息表
				supplier.setDataStatus("1");// 数据默认状态
				supplier.setSupplierStatus("0");// 数据审批状态
				supplier.setDataIntegrity("0");// 资料完成度
				String pwd = Util.toMD5("12345");
				supplier.setPassword(pwd);
				supplierDao.insert(supplier);
				supplier.setUserCode("jd" + supplier.getId());// 更新账号
				supplierDao.update(supplier);
				// 插入联系人表
				Contacts con = supplierDto.getContacts();
				con.setSupplierId(supplier.getId());
				contactsDao.insert(con);
				// 插入企业基本信息表
				Enterprise en = supplierDto.getEnterprise();
				// 根据企业类型code获取供应商编码
				en.setCode(this.getCode(en.getSupplierType()));// 供应商编码
				en.setSupplierId(supplier.getId());
				enterpriseDao.insert(en);
				// 插入财务信息基本表
				Finance fi = supplierDto.getFinace();
				if (fi != null) {
					fi.setSupplierId(supplier.getId());
					financeDao.insert(fi);
				}
				// 插入技术状况及质量控制表
				Quality quality = supplierDto.getQuality();
				if (quality != null) {
					quality.setSupplierId(supplier.getId());
					qulityDao.insert(quality);
					// 检测设备表
					List<Equip> eq = supplierDto.getEquip();
					if (eq != null) {
						for (Equip e : eq) {
							e.setQualityId(quality.getId());
							equipDao.insert(e);
						}
					}
					// 原材料表
					Material m = supplierDto.getMaterial();
					if (m != null) {
						m.setQualityId(quality.getId());
						materialDao.insert(m);
					}
				}
				// 插入其它信息表
				Other o = supplierDto.getOther();
				if (o != null) {
					o.setSupplierId(supplier.getId());
					otherDao.insert(o);
				}

			}
		}
		return result;
	}

	/**
	 * 跟据供应商类型获取序列号
	 */
	public synchronized String getCode(String code) {
		if (code == null) {
			throw new RuntimeException("供应商类型不能为空");
		}

		StringBuffer temp = new StringBuffer();
		temp.append(code);
		temp.append("-");
		// 年份
		Calendar cal = Calendar.getInstance();
		int yearTemp = 0;
		long synctemp = 0000;
		DecimalFormat df = new DecimalFormat("0000");
		String var = enterpriseDao.getMaxSupId();
		if (var != null && !var.equals("")) {
			String[] tempArray = var.split("-");
			yearTemp = Integer.parseInt(tempArray[0]);
			synctemp = Long.parseLong(tempArray[1]);
			if (cal.get(Calendar.YEAR) > yearTemp) {
				synctemp = 0001;
				temp.append(cal.get(Calendar.YEAR));
				temp.append("-");

				temp.append(df.format(synctemp));
			} else {
				synctemp++;

				temp.append(yearTemp);
				temp.append("-");
				temp.append(df.format(synctemp));
			}
		} else {
			synctemp = 0001;
			synctemp = cal.get(Calendar.YEAR);
			temp.append(cal.get(Calendar.YEAR));
			temp.append("-");
			temp.append(df.format(synctemp));
		}
		return temp.toString();

	}

	@Override
	public int insertData(Supplier supplier) {
		// 首先插入基本信息表
		int i = supplier.hashCode();
		supplier.setUserCode("jd" + i);// 账号
		supplier.setDataStatus("1");// 数据默认状态
		supplier.setSupplierStatus("0");// 数据审批状态
		String pwd = Util.toMD5("12345");
		supplier.setPassword(pwd);
		// supplier.setDataIntegrity("ten");// 资料完成度
		supplierDao.insert(supplier);
		// 插入联系人表
		for (Contacts con : supplier.getContacts()) {

			con.setSupplierId(supplier.getId());
			contactsDao.insert(con);
		}
		// 插入企业基本信息表
		Enterprise en = supplier.getEnterprise();
		// 根据企业类型code获取供应商编码
		en.setCode(this.getCode(en.getSupplierType()));// 供应商编码
		en.setSupplierId(supplier.getId());
		enterpriseDao.insert(en);
		// 插入财务信息基本表
		Finance fi = supplier.getFinance();
		fi.setSupplierId(supplier.getId());
		financeDao.insert(fi);
		// 其它信息表
		Other o = supplier.getOther();
		o.setSupplierId(supplier.getId());
		otherDao.insert(o);

		return 0;
	}

	@Override
	public int updateData(Supplier supplier) {
		// 修改供应商的基本信息
		supplier.setSupplierStatus("0");// 将审核状态改成未审核
		supplierDao.update(supplier);
		// 修改联系人的基本信息
		for (Contacts con : supplier.getContacts()) {
			if (con.getId() != null && con.getId() > 0) {
				contactsDao.update(con);
			} else {
				con.setSupplierId(supplier.getId());
				contactsDao.insert(con);
			}
		}
		// 修改企业基本信息表
		Enterprise en = supplier.getEnterprise();
		enterpriseDao.update(en);
		// 修改财务信息基本表
		Finance fi = supplier.getFinance();
		financeDao.update(fi);
		// 修改其它信息表
		Other o = supplier.getOther();
		if (o.getId() != null && o.getId() > 0) {
			otherDao.update(o);
		} else {
			o.setSupplierId(supplier.getId());
			otherDao.insert(o);
		}
		return 1;
	}

	@Override
	public void saveSpecialSupplier(SpecialSupplierVO specialSupplierVO) {
		if(specialSupplierVO != null){
			Supplier supplier = new Supplier();
			supplier.setName(specialSupplierVO.getName());
			Long id = supplierDao.insert(supplier);
			Contacts contacts = new Contacts();
			if (id != null && id > 0)
				contacts.setSupplierId(id);
			contacts.setMobile(specialSupplierVO.getMobile());
			contactsDao.insert(contacts);
		}
	}

	@Override
	public int updateStatus(Supplier supplier) {
		try {
			supplier.setSupplierStatus("1");
			supplierDao.update(supplier);
			Enterprise en = supplier.getEnterprise();
			if (en != null) {
				// 根据企业类型code获取供应商编码,供应商前台注册的没有编码,这里要添加
				if (en.getCode() == null || "".equals(en.getCode())) {
					en.setCode(this.getCode(en.getSupplierType()));// 供应商编码
				}
				enterpriseDao.update(supplier.getEnterprise());
			}
			// 审核通过以后,如果改供应商是员工推荐的,要变更状态为推荐成功
			if (supplier.getRecommended() != null) {
				Supplier s = new Supplier();
				s.setId(supplier.getId());
				s.setRecommendStatus("2");
				supplierDao.update(s);
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	public int updateReject(Supplier supplier) {
		try {
			supplier.setSupplierStatus("2");
			supplierDao.update(supplier);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	public int updateNotice(Supplier supplier) {
		supplier.setRecommendStatus("1");
		int i = supplierDao.update(supplier);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("supplierName", supplier.getName());
		map.put("realName", supplier.getRealName());
		map.put("userCode", supplier.getUserCode());
		map.put("mailDate", getMailDate());
		map.put("userCode", supplier.getUserCode());
		// 发送邮件
		if (i > 0) {

			try {
				mailSendService.sendTemplateMail(
						new String[] { supplier.getEmail() }, "京东供应商注册邀请通知",
						"mail/supplierNotice.vm", map, new File[] {});

				return 2;
			} catch (BusinessException ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		return 0;
	}

	@Override
	public int insertRecommend(Supplier supplier) {
		try {
			supplier.setRecommendStatus("0");// 默认推荐中
			supplier.setSupplierStatus("0");
			String pwd = Util.toMD5("1qa2ws");
			supplier.setPassword(pwd);
			supplierDao.insert(supplier);
			supplier.setUserCode("jd" + supplier.getId());// 更新账号
			supplierDao.update(supplier);
			return 1;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return 0;
		}

	}

	private String getMailDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
}
