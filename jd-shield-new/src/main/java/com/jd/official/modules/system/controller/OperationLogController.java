/**
 * 
 */
package com.jd.official.modules.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.OperationLog;
import com.jd.official.modules.system.service.OperationLogService;

/**
 * 
 * @author qiuyang
 *
 */
@Controller
@RequestMapping(value="/system")
public class OperationLogController {
	private static final Logger logger = Logger.getLogger(OperationLogController.class);
	
	@Autowired
	private OperationLogService operationLogService;
	
	@RequestMapping(value = "/log_index", method = RequestMethod.GET)
	public String index() throws Exception {
		return "system/log_index";
	}
	
	 @RequestMapping(value = "/log_list")
	 @ResponseBody
	 public Object list(HttpServletRequest request,@RequestParam String  creator,
			 @RequestParam String  description,@RequestParam String  startTime,
			 @RequestParam String  endTime,@RequestParam String  entityName,
			 @RequestParam String  clientIp) throws Exception {
		 PageWrapper<OperationLog> pageWrapper = new PageWrapper<OperationLog>(request);
		 if (null != creator && !creator.equals("")) {
             pageWrapper.addSearch("creator", creator);
         }
		 if (null != description && !description.equals("")) {
             pageWrapper.addSearch("description", description);
         }
		 if (null != entityName && !entityName.equals("")) {
             pageWrapper.addSearch("entityName", entityName);
         }
		 if (null != clientIp && !clientIp.equals("")) {
             pageWrapper.addSearch("clientIp", clientIp);
         }
		 if (null != startTime && !startTime.equals("")) {
             pageWrapper.addSearch("startTime", startTime);
         }
         if (null != endTime && !endTime.equals("")) {
             pageWrapper.addSearch("endTime", endTime);
         }
		 operationLogService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		 pageWrapper.addResult("returnKey","returnValue");
		 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	     logger.debug("result:"+json);
	     return json;
	 }
}
