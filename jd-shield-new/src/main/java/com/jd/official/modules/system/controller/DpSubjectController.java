/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.internal.matchers.SubstringMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.official.core.utils.JsonUtils;
import com.jd.official.modules.system.model.DpSubject;
import com.jd.official.modules.system.service.DpSubjectService;

/**
 * @author qiuyang
 *
 */
@Controller
@RequestMapping(value="/system")
public class DpSubjectController {

	private static final Logger logger = Logger.getLogger(DpSubjectController.class);
	
	@Autowired
	private DpSubjectService dpSubjectService;
	
	@RequestMapping(value = "/dpsubject_list",method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object list(DpSubject dpSubject) throws Exception {
		List<DpSubject> fullDpSubjects = getFullDpSubjects(dpSubject);
		String json = JsonUtils.toJsonByGoogle(fullDpSubjects);
		return json;
	}
	
	private List<DpSubject> getFullDpSubjects(DpSubject dpSubject){
		List<DpSubject> fullDpSubjects = new ArrayList<DpSubject>();
		
		dpSubject.setSubjectType("User");
		List<DpSubject> dpSubjectByUsers = dpSubjectService.selectDpSubjectBySubjectType(dpSubject);
		if(null != dpSubjectByUsers && !dpSubjectByUsers.isEmpty()){
			fullDpSubjects.addAll(dpSubjectByUsers);
		}
		
		dpSubject.setSubjectType("Organization");
		List<DpSubject> dpSubjectByOrgs = dpSubjectService.selectDpSubjectBySubjectType(dpSubject);
		if(null != dpSubjectByOrgs && !dpSubjectByOrgs.isEmpty()){
			fullDpSubjects.addAll(dpSubjectByOrgs);
		}
		
		dpSubject.setSubjectType("Position");
		List<DpSubject> dpSubjectByPositions = dpSubjectService.selectDpSubjectBySubjectType(dpSubject);
		if(null != dpSubjectByPositions && !dpSubjectByPositions.isEmpty()){
			fullDpSubjects.addAll(dpSubjectByPositions);
		}
		
		dpSubject.setSubjectType("Role");
		List<DpSubject> dpSubjectByRoles = dpSubjectService.selectDpSubjectBySubjectType(dpSubject);
		if(null != dpSubjectByRoles && !dpSubjectByRoles.isEmpty()){
			fullDpSubjects.addAll(dpSubjectByRoles);
		}
		
		dpSubject.setSubjectType("Group");
		List<DpSubject> dpSubjectByGroups = dpSubjectService.selectDpSubjectBySubjectType(dpSubject);
		if(null != dpSubjectByGroups && !dpSubjectByGroups.isEmpty()){
			fullDpSubjects.addAll(dpSubjectByGroups);
		}
		
		dpSubject.setSubjectType("AuthExpression");
		List<DpSubject> dpSubjectByAuths = dpSubjectService.selectDpSubjectBySubjectType(dpSubject);
		if(null != dpSubjectByAuths && !dpSubjectByAuths.isEmpty()){
			List<DpSubject> fullDpSubjectByAuths = new ArrayList<DpSubject>();
			for(DpSubject dpsubject: dpSubjectByAuths){
				StringBuffer sb = new StringBuffer();
				sb.append("[部门：");
				sb.append(dpsubject.getAuthExpresion().getOrganizationName()==null?
						"不限":dpsubject.getAuthExpresion().getOrganizationName());
				sb.append("]+[职位：");
				sb.append(dpsubject.getAuthExpresion().getPositionName()==null?
						"不限":dpsubject.getAuthExpresion().getPositionName());
				sb.append("]+[职级：");
				String leveUpName = dpsubject.getAuthExpresion().getLevelUpName();
				String leveDownName = dpsubject.getAuthExpresion().getLevelDownName();
				if(null != leveUpName && null == leveDownName){
					sb.append(leveUpName+"以上]");
				}else if(null == leveUpName && null != leveDownName){
					sb.append(leveDownName+"以下]");
				}else if(null != leveUpName && null != leveDownName){
					if(leveUpName.equals(leveDownName)){
						sb.append(leveUpName+"]");
					}else{
						sb.append(leveUpName+"-"+leveDownName+"]");
					}
				}else{
					sb.append("不限]");
				}
				/*if(null != dpsubject.getAuthExpresion().getLevelAbove() && 
						dpsubject.getAuthExpresion().getLevelAbove().equals("")){
					sb.append(dpsubject.getAuthExpresion().getLevelAbove()+"以上]");
				}else{
					sb.append(dpsubject.getAuthExpresion().getLevelUp()==null?
							"不限":dpsubject.getAuthExpresion().getLevelUp());
				}*/
				
				sb.append("]+[用户：");
				sb.append(dpsubject.getAuthExpresion().getUserName()==null?
						"不限":dpsubject.getAuthExpresion().getUserName());
				sb.append("]+[角色：");
				sb.append(dpsubject.getAuthExpresion().getRoleName()==null?
						"不限":dpsubject.getAuthExpresion().getRoleName());
				sb.append("]+[用户组：");
				sb.append(dpsubject.getAuthExpresion().getGroupName()==null?
						"不限":dpsubject.getAuthExpresion().getGroupName());
				sb.append("]");
				dpsubject.setSubjectRualName(sb.toString());
				fullDpSubjectByAuths.add(dpsubject);
			}
			fullDpSubjects.addAll(fullDpSubjectByAuths);
		}
		
		return fullDpSubjects;
	}
	
	
	
	
}
