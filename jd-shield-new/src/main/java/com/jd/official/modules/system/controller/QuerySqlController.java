/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.official.core.utils.JsonUtils;

/**
 *@Description: 系统功能 Controller
 *@author liub
 *
 */
@Controller
@RequestMapping(value="/system")
public class QuerySqlController {
	
	 private static final Logger logger = Logger.getLogger(QuerySqlController.class);
	
	 
	 @RequestMapping(value = "/querySql", method = RequestMethod.GET)
	 public String index() throws Exception {
	     return "system/querySql";
	 }
	 
	 @RequestMapping(value = "/querySql_list")
	 @ResponseBody
	 public Object list(HttpServletRequest request,@RequestParam String  sql,@RequestParam String  type) throws Exception {
		 /*PageWrapper<Operation> pageWrapper = new PageWrapper<Operation>(request);
		 operationService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		 pageWrapper.addResult("returnKey","returnValue");
		 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	     logger.debug("result:"+json);*/
		 Map<String,Object> result = new HashMap<String,Object>();
		 if(result.get("operate").equals("select")){
			 String json = JsonUtils.toJsonByGoogle(result.get("data"));
		     return json;
		 } else {
			 return result.get("data");
		 }
	 }
}
