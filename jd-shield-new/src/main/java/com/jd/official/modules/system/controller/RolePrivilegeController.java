/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import redis.clients.jedis.Jedis;

import com.jd.cachecloud.driver.jedis.JedisOperation;
import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.redis.redis.RedisCacheSerializer;
import com.jd.official.core.tree.TreeNode;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.modules.system.model.Operation;
import com.jd.official.modules.system.model.Privilege;
import com.jd.official.modules.system.model.Resource;
import com.jd.official.modules.system.model.RolePrivilege;
import com.jd.official.modules.system.service.OperationService;
import com.jd.official.modules.system.service.PrivilegeService;
import com.jd.official.modules.system.service.ResourceService;
import com.jd.official.modules.system.service.RolePrivilegeService;
import com.jd.official.modules.system.service.UserService;

/**
 * 角色功能权限 controller
 * 该类提供授权的查询,添加,修改,删除操作
 * @author liub
 *
 */
@Controller
@RequestMapping(value="/system")
public class RolePrivilegeController {
	private static final Logger logger = Logger.getLogger(RolePrivilegeController.class);
	
	 @Autowired(required=true)
	 private RolePrivilegeService rolePrivilegeService;
	 
	 @Autowired(required=true)
	 private PrivilegeService privilegeService;
	 
	 @Autowired
	 private ResourceService resourceService;
	 
	 @Autowired
	 private OperationService operationService;
	 
	 private static List<Operation> operations = null;
	 
	 @Autowired
	 private UserService userService;
	 
	 @Autowired
	 private ShardedXCommands redisClient;
	 
	 @RequestMapping(value = "/rolePrivilege_index", method = RequestMethod.GET)
	 public ModelAndView list(String roleId) throws Exception {
		ModelAndView mav = new ModelAndView("system/rolePrivilege");
		TreeNode rootNode = new TreeNode();
		rootNode.setId("0");
		rootNode.setName("京东-办公自动化");
		rootNode.setIsParent(Boolean.TRUE);
		rootNode.setIconClose("../static/zTree/css/zTreeStyle/img/diy/1_close.png");
		rootNode.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/1_open.png");
			
		mav.addObject("roleId", roleId);
		mav.addObject("treeNode", JsonUtils.toJsonByGoogle(rootNode));
		return mav;
	 }
	 /**
	  * 
	  * @param request
	  * @param level
	  * @param nodeId
	  * @param response
	  */
	 @RequestMapping(value = "/rolePrivilege_getTreeNodesJson", method = RequestMethod.POST)
	 @ResponseBody
	 public void resourceLoad(HttpServletRequest request,
			 @RequestParam(value = "roleId", required = true) Long roleId,
			@RequestParam(value = "level", required = true) int level,
			@RequestParam(value = "nodeId", required = true) String nodeId,
			HttpServletResponse response) {
			 try {
				if (!nodeId.equals("undefined")) {
					nodeId = StringUtils.substring(nodeId,StringUtils.indexOf(nodeId, "_") + 1);
					List<Resource> resources = null;
					if(nodeId.equals("0")){
						resources = resourceService.findByParentId(null);
					}else{
						resources = resourceService.findByParentId(Long.parseLong(nodeId));
					}
					
					response.getWriter().write(JsonUtils.toJsonByGoogle(getTreeNodes(resources,roleId)));
				}
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
	}
	private List<TreeNode> getTreeNodes(List<Resource> resources,Long roleId) {
		synchronized(Object.class){
			if (operations == null) {
				operations = operationService.find(new Operation());
			}
		}
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (Resource resource : resources) {
			TreeNode treeNode = new TreeNode();
			treeNode.setIsParent(true);
			treeNode.setId(Resource.class.getSimpleName() + "_"  + resource.getId());
			treeNode.setpId(resource.getParentId() != null ? Resource.class.getSimpleName() + "_" + resource.getParentId() : "Application_1");
			treeNode.setName(resource.getResourceName());

			Map<String, Object> props = new HashMap<String, Object>();
			props.put("id", resource.getId());
			props.put("url", resource.getResourceUrl());
			props.put("code", resource.getResourceCode());
			
			Privilege entity = new Privilege();
			entity.setResourceId(resource.getId());
			List<Privilege> privileges = privilegeService.find(entity);
			props.put("privileges", privileges);
			
			props.put("operations", getResourceForOperation(privileges));
			//设置是否选中
			if(isTreeNodeChecked(privileges, roleId)){
				treeNode.setChecked(true);
			}
			treeNode.setProps(props);
			
			treeNode.setIconClose("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
			treeNode.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
			
			treeNode.setProps(props);
				
			if(resource.getChildrenNum() > 0){
				treeNode.setIsParent(true);
			} else {
				treeNode.setIsParent(false);
			}
				
			treeNodes.add(treeNode);
			}
			return treeNodes;
	}
	private List<Operation> getResourceForOperation(List<Privilege> privileges){
		List<Operation> oper = new ArrayList<Operation>();
		for(int i = 0 ; i < privileges.size() ; i++){
			Privilege privilege = privileges.get(i);
			if(privilege != null){
				for(int j = 0 ; j < operations.size() ; j++){
					Operation operation = operations.get(j);
					if(operation != null){
						if(privilege.getOperation().getId().equals(operation.getId())){
							oper.add(operation);
							break;
						}
					}
				}
			}
		}
		return oper;
	}
	/**
	 * 判断treeNode是否选中
	 * @param privileges
	 * @param roleId
	 * @return
	 */
	private boolean isTreeNodeChecked(List<Privilege> privileges,Long roleId){
		 RolePrivilege entity = new RolePrivilege();
		 entity.setRoleId(roleId);
		 List<RolePrivilege> records = rolePrivilegeService.find(entity);
		 
		 for(int i = 0 ; i < privileges.size() ; i++){
			 Privilege privilege = privileges.get(i);
			 if(privilege != null){
				 for(int j = 0 ; j < records.size() ; j++){
					 RolePrivilege rolePrivilege = records.get(j);
					 if(rolePrivilege != null){
						 if(privilege.getId().equals(rolePrivilege.getPrivilegeId())){
							 return true;
						 }
					 }
				 }
			 }
		 }
		 return false;
	}
	 
	 /**
	  * 对授权信息进行保存
	  * @return
	  */
	 @RequestMapping(value="/rolePrivilege_save",method=RequestMethod.POST)
	 @ResponseBody
	 private String save(Long roleId,String operationsJson) throws BusinessException,IOException{
		 //redis删除缓存
		 removeCache("*");
		 RolePrivilege deletEntity = new RolePrivilege();
		 deletEntity.setRoleId(roleId);
		 rolePrivilegeService.delete(deletEntity,true);
		 JSONArray jsonArray = JSONArray.fromObject(operationsJson);
		 for(int i = 0 ; i < jsonArray.size() ; i++){
			 JSONArray jsonArray1 = JSONArray.fromObject(jsonArray.get(i));
			 for(int j = 0 ; j < jsonArray1.size() ; j++){
				 JSONObject jsonObject = jsonArray1.getJSONObject(j);  
				 Long resourceId = Long.parseLong(String.valueOf(jsonObject.get("resourceId")));//资源ID
				 JSONArray jsonArray2 = JSONArray.fromObject(jsonObject.get("checkedResourceOperationArray"));
				 for(int k = 0 ; k < jsonArray2.size() ; k++){
					 JSONObject jsonObject2 = jsonArray2.getJSONObject(k);
					 //操作数据
					 Long operationId = Long.parseLong(String.valueOf(jsonObject2.get("operationId")));
					 Privilege privilege = new Privilege();
					 privilege.setResourceId(resourceId);
					 
					 privilege.setOperation(operationService.get(operationId));
					 
					 
					 List<Privilege> records = privilegeService.find(privilege);
					 if(records != null && records.size() > 0){
						 for(int ii=0 ; ii<records.size() ; ii++){
							 if(records.get(ii).getOperation().getId().equals(operationId)){
								 Long privilegeId = records.get(ii).getId();
								 RolePrivilege rolePrivilege = new RolePrivilege();
								 rolePrivilege.setPrivilegeId(privilegeId);
								 rolePrivilege.setRoleId(roleId);
								 rolePrivilegeService.insert(rolePrivilege);//写入功能权限表
							 }
						 }
					 }
					
				 }
			 }
    	 }
		 Map<String,Object> map = new HashMap<String,Object>();
		 map.put("operator", true);
		 map.put("message", "授权成功");
		 return JSONObject.fromObject(map).toString();
	 }
	 
	 private synchronized void removeCache(final String key){
			final Set<byte[]> bytesKey = new HashSet<byte[]>();
			//不删除数据字典缓存
			final String[] safeKeys = {"JD-OFFICIAL/DictData/findDictDataListByDictCode",
					"JD-OFFICIAL/DictData/findDictDataListByPid"};
			redisClient.execute(new JedisOperation<Void>() {
	            @Override
	            public Void call(Jedis jedis) throws Throwable {
	                bytesKey.addAll(jedis.keys(key.getBytes("utf-8")));
	                return null;
	            }
	        });
	        for (byte[] s : bytesKey) {
	        	try {
	        		String allKeys = (String) RedisCacheSerializer.deSerialize(s);
	        		if(allKeys.indexOf(safeKeys[0]) == -1
	        				&&
	        		   allKeys.indexOf(safeKeys[1]) == -1)
	        			redisClient.del(s);
	        	} catch (IOException e) {
	        		e.printStackTrace();
	        	} catch (ClassNotFoundException e) {
	        		e.printStackTrace();
	        	}
	        }
	        
		}
}
