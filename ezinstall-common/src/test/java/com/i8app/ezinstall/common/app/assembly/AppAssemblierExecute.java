package com.i8app.ezinstall.common.app.assembly;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.ClientParamHolder;

public class AppAssemblierExecute extends BaseTest	{
	
	@Resource
	AppAssemblierImpl aa;
	@Resource
	AppAdaptee i8appAdapteeImpl, wostoreAdapteeImpl, axonAdapteeImpl;
	
	private String[] tables = new String[]{"app","app_install_pack","assembly_log"};
	
	@BeforeClass
	public static void setUp()	{
		ClientParamHolder.setFileServerDir("F:/workspace/.metadata/.me_tcat/webapps/fileServer/");
		ClientParamHolder.setFileServerUrl("http://sx.source.i8app.com/fileServer/");
		Map<String,String> map = new LinkedHashMap<String,String>();
		map.put("cp09", "local");
		ClientParamHolder.setCpMapToAssembly(map);
	}
	
	/**
	 * 因为汇集程序在一个service下开始执行, 属于一个事务内, 不方便查看导入进度.
	 * 执行前修改表引擎为myisam,方便随时查看导入情况
	 */
	@Before
	public void setup()	{
//		for(String s : tables)	{
//			simpleJdbcTemplate.update("ALTER TABLE " + s + " ENGINE=MyISAM");
//		}
	}
	
	/**
	 * 改回到innodb
	 */
	@After
	public void tearDown()	{
		for(String s : tables)	{
//			simpleJdbcTemplate.update("ALTER TABLE " + s + " ENGINE=INNODB");
		}
	}
	
	@Test
	@Rollback(false)	//不回滚
	public void testStartAssembly()	{
		aa.startAssembly();
	}

	@Test
	//@Rollback(false)	//不回滚
	public void testImportToApp()	{
		aa.importToApp(i8appAdapteeImpl);
	}

}
