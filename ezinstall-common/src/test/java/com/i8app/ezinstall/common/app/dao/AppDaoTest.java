package com.i8app.ezinstall.common.app.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.Constants.OrderBy;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;

public class AppDaoTest extends BaseTest	{
	
	@Resource AppDao dao;
	
	@BeforeClass
	public static void beforeClass()	{
		ClientParamHolder.setFileServerUrl("http://192.168.1.123:8080/fileServer");
	}
	
	@Before
	public void setUp()	{
		//deleteFromTables("pkg_info","app", "app_install_pack");
	}
	
	@Test
	public void testFindByPkg1(){
		List<String> list = new ArrayList<String>();
		list.add(Constants.CP_I8SOFT);
		list.add(Constants.CP_I8GAME);
		
		Pager<AppDTO> pager = dao.findByPkg(Os.ANDROID, 1, list, 0, 10);
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
		}
		System.out.println(pager.getTotalCount());
		System.out.println(pager.getList());
	}

	
	@Test
	public void testFindByPkg2(){
		List<String> list = new ArrayList<String>();
		list.add(Constants.CP_I8SOFT);
		list.add(Constants.CP_I8GAME);

		Os os = Os.ANDROID;
		os.setOsVersion("1.2.3");
		Pager<AppDTO> pager = dao.findByPkg(os, 1, list, 0, 10);
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
		}
		System.out.println(pager.getTotalCount());
		System.out.println(pager.getList());
	}

	@Test
	public void testFindInstallPackByPkg(){
		List<String> list = new ArrayList<String>();
		list.add(Constants.CP_I8SOFT);
		list.add(Constants.CP_I8GAME);
		
		Os os = Os.ANDROID;
		os.setOsVersion("1.2.3");
		Pager<AppDTO> pager = dao.findInstallPackByPkg(null, 1, os, new String[]{Constants.CP_I8SOFT, Constants.CP_I8GAME}, 0, 10);
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
		}
		System.out.println(pager.getTotalCount());
		System.out.println(pager.getList());
	}
	
	
	@Test
	public void testQuery()	{
		AreaParam area = new AreaParam("01", 1, 2);
		AppParam app = new AppParam();
		app.setCpIds(Arrays.asList(AppParam.CP_AXON, AppParam.CP_I8SOFT));
		app.setRscTypeId("101");
		//app.setOrderBy(OrderBy.cpUpdateTime);
		Os os = Os.ANDROID;
		long s = System.currentTimeMillis();
		Pager<AppDTO> pager = dao.query(area, os, app, 0, 200);
		long e = System.currentTimeMillis();
		System.err.println((e-s)/1000);
		pager = dao.query(area, os, app, 200, 200);
//		pager = dao.query(area, os, app, 400, 200);
//		pager = dao.query(area, os, app, 600, 200);
//		pager = dao.query(area, os, app, 800, 200);
		System.out.println(pager.getTotalCount());
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
			//System.out.println(a.getPackCpUpdateTime());
		}
	}
	
	
	@Test
	public void testQuery2()	{
		AreaParam area = new AreaParam("01", 1, 2);
		AppParam app = new AppParam();
		//app.setRscTypeId("102");
		//app.setName("qq");
		Calendar now = Calendar.getInstance();
		//now.add(Calendar.DAY_OF_YEAR, -90);
		//app.setCpUpdateTime(now.getTime());
		app.setOrderBy(OrderBy.cpUpdateTime);
		Os os = Os.ANDROID;
		long s = System.currentTimeMillis();
		Pager<AppDTO> pager = dao.query(area, os, app, 0, 100);
		long e = System.currentTimeMillis();
		System.err.println((e-s)/1000);
		System.out.println(pager.getTotalCount());
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
			//System.out.println(a.getPackCpUpdateTime());
		}
	}
	
	@Test
	public void testSearch()	{
		AreaParam area = new AreaParam("01", 1, 2);
		AppParam app = new AppParam();
		app.setCpIds(Arrays.asList(AppParam.CP_AXON, AppParam.CP_I8SOFT));
		app.setRscTypeId("101");
		//app.setOrderBy(OrderBy.cpUpdateTime);
		Os os = Os.ANDROID;
		
		long s = System.currentTimeMillis();
		Pager<AppDTO> pager = dao.search(area, os, app, 0, 100);
		long e = System.currentTimeMillis();
		System.err.println((e-s)/1000);
		
		pager = dao.search(area, os, app, 200, 200);
		pager = dao.search(area, os, app, 400, 200);
		pager = dao.search(area, os, app, 600, 200);
		pager = dao.search(area, os, app, 800, 200);
		System.out.println(pager.getTotalCount());
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
			//System.out.println(a.getPackCpUpdateTime());
		}
	}

	@Test
	public void testSearch2()	{
		AreaParam area = new AreaParam("01", 1, 2);
		AppParam app = new AppParam();
		//app.setRscTypeId("102");
		app.setName("qq");
		Calendar now = Calendar.getInstance();
		//now.add(Calendar.DAY_OF_YEAR, -90);
		//app.setCpUpdateTime(now.getTime());
		//app.setOrderBy(OrderBy.cpUpdateTime);
		Os os = Os.ANDROID;
		long s = System.currentTimeMillis();
		Pager<AppDTO> pager = dao.search(area, os, app, 0, 100);
		long e = System.currentTimeMillis();
		System.err.println((e-s)/1000);
		System.out.println(pager.getTotalCount());
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
			//System.out.println(a.getPackCpUpdateTime());
		}
	}

	@Test
	public void testFindSingle()	{
		AppDTO a = dao.findSingle("1b499df4-bff3-4c67-841d-11ac984a8bbb", Os.ANDROID);
		assertNotNull(a.getUuid());
		System.out.println(a);
		System.out.println(a.getIcon());
	}
	
	@Test
	public void testFindByCpidAndOriginalappid()	{
		App a = dao.find("cp01", "426");
		System.out.println(a);
	}
	
	@Test
	public void testQueryPack()	{
		AreaParam area = new AreaParam("01", 1, 2);
		AppParam app = new AppParam();
		app.setCpIds(Arrays.asList(AppParam.CP_AXON, AppParam.CP_I8SOFT));
		app.setRscTypeId("101");
		//app.setOrderBy(OrderBy.cpUpdateTime);
		Os os = Os.ANDROID;
		
		long s = System.currentTimeMillis();
		Pager<AppDTO> pager = dao.queryPack(area, os, app, 0, 100);
		long e = System.currentTimeMillis();
		System.err.println((e-s)/1000);
		
		System.out.println(pager.getTotalCount());
		for(AppDTO a : pager.getList())	{
			assertNotNull(a.getUuid());
			//System.out.println(a.getPackCpUpdateTime());
		}
	}
	
	@Test
	public void testPersist()	{
		App a = new App();
		a.setUuid("dddddddddd");
		dao.persist(a);
	}

}
