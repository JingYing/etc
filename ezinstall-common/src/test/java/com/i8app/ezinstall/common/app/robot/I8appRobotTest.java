package com.i8app.ezinstall.common.app.robot;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.BeforeClass;
import org.junit.Test;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.ClientParamHolder;

public class I8appRobotTest extends BaseTest{
	
	@Resource	I8appApkRobot robot;
	
	@BeforeClass
	public static void setUp1()	{
		ClientParamHolder.setFileServerDir("F:/workspace/.metadata/.me_tcat/webapps/fileServer/");
		ClientParamHolder.setFileServerUrl("http://3gdl2.wostore.cn/fileServer/");
		Map<String, String> map = new HashMap<String, String>();
		map.put("cp09", "local");
		ClientParamHolder.setCpMapToAssembly(map);
	}
	
	@Test
	public void test1()	{
		robot.startFromInstallPack();
//		robot.insertMissingPack();
	}

	@Test
	public void test2()	{
		robot.updateFromInstallPack();
	}

	@Test
	public void test3()	{
		robot.updateFromWdj();
	}

}
