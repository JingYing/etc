package com.i8app.ezinstall.common.app.assembly;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.app.assembly.cp.AxonAdapteeImpl;
import com.i8app.ezinstall.common.app.assembly.cp.WostoreAdapteeImpl;
import com.i8app.ezinstall.common.app.dao.AppDao;
import com.i8app.ezinstall.common.app.dao.AppInstallPackDao;
import com.i8app.ezinstall.common.app.dao.I8GameDao;
import com.i8app.ezinstall.common.app.dao.I8SoftDao;
import com.i8app.ezinstall.common.app.dao.WostoreDao;
import com.i8app.ezinstall.common.app.dao.WostorePackDao;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;

public class AppAssemblierTest extends BaseTest {
	
	@Resource AppAssemblierImpl aa;
	@Resource WostoreAdapteeImpl wostoreAdaptee;
	@Resource AxonAdapteeImpl axonAdaptee;
	@Resource I8SoftDao i8SoftDao;
	@Resource I8GameDao i8GameDao;
	@Resource WostoreDao wostoreDao;
	@Resource WostorePackDao wostorePackDao;
	@Resource AppDao appDao;
	@Resource AppInstallPackDao packDao;
	
	@BeforeClass
	public static void setUp()	{
		ClientParamHolder.setFileServerDir("c:/users/jing/desktop");
		ClientParamHolder.setFileServerUrl("http://124.248.34.134:8080/fileServer/");
	}
	
	@Test
	public void testAxonAdaptee()	{
		aa.importToApp(axonAdaptee);
	}
	
	@Test
	public void testbindSource()	{
		aa.switchSource("23346d2e-82ed-45dc-98b9-d211a1e1fea5", "cp04", "1078205");
	}
	
	/**
	 * 检查现有的appInstallPack表中数据的有效性
	 */
	@Test
	public void testCurAppInstallPack()	{
		//安卓系统,但没有apkpkgname或apkversioncode或version
		int i1 = simpleJdbcTemplate.queryForInt("SELECT COUNT(*) FROM app_install_pack WHERE OS = 'OS1' " +
				"AND (apkPkgName IS NULL OR APKVERSIONCODE IS NULL OR VERSION IS NULL)");
		assertEquals(0, i1);
		
		//IOS或IOSX, 但没有ipabundleid
		int i2 = simpleJdbcTemplate.queryForInt("SELECT COUNT(*) FROM app_install_pack WHERE OS IN('OS2','OS3') AND IPABUNDLEID IS NULL");
		assertEquals(0, i2);
		
		//android系统中,apkpkgname相同, 但appid不同
		List list1 = simpleJdbcTemplate.queryForList("SELECT COUNT(*) C, UUID FROM(" +
				"select UUID, APPID, APKPKGNAME from app_install_pack where os = 'os1' group by appid,apkpkgname)X " +
				"GROUP BY APKPKGNAME HAVING C > 1");
		assertEquals(0, list1.size());
		
		//ios或iosx, ipabundleid相同, 但appid不同
		List list2 = simpleJdbcTemplate.queryForList("SELECT COUNT(*) C , ipabundleid FROM(" +
				"select appid,ipabundleid from app_install_pack where os IN('os2','os3') group by appid,ipabundleid)X " +
				"GROUP BY ipabundleid HAVING C > 1");
		assertEquals(0, list2.size());
		
		
	}
	
	/**
	 * 两个CP商提供同一款ANDROID软件
	 * 测试目标:一个APP, 两个PACK 
	 */
	@Test
	public void test1()	{
		deleteFromTables("app", "app_install_pack", "softinfo", "gameinfo");
		simpleJdbcTemplate.update("INSERT INTO SOFTINFO(SOFTID,SOFTNAME,SOFTVERSION,SOFTTYPE,OSTYPE,OSID,SOFTFILENAME) VALUES(1,'Quickoffice','5.7.327',10,200,12500000,'soft/files/20130220/20130220133351401.apk')");
		simpleJdbcTemplate.update("INSERT INTO GAMEINFO(GID,GAMENAME,GAMEVERSION,GAMETYPE,OSTYPE,OSID,GAMEFILENAME) VALUES(10,'Quickoffice2','5.7.9',10,200,12500000,'soft/files/20130220/20130220133351401.apk')");
		List<App> appList = appDao.findAll(0, 1).getList();
		List<AppInstallPack> packList = packDao.findAll(0,2).getList();
		assertEquals(1, appList.size());
		assertEquals(2, packList.size());
		assertEquals(packList.get(0).getApkPkgName(), packList.get(1).getApkPkgName());
	}
	
	/**
	 * 一款app已入库后, 如果cp软件的名称,资源类型等做了更新, app是否自动更新
	 */
	@Test
	public void test2()	{
		deleteFromTables("app", "app_install_pack", "softinfo", "gameinfo", "rsctype_mapper");
		
		simpleJdbcTemplate.update("INSERT INTO APP(UUID,NAME,ICONID,RSCTYPEID,DOWNCOUNT,UPDATETIME,CPID,ORIGINALAPPID,ORIGINALTYPEID) " +
				"VALUES('xxx', 'quickoffice', 1, '1010302', 154, '2013-05-10 02:54:45', 'cp01', '1', '01')");
		
		simpleJdbcTemplate.update("INSERT INTO APP_INSTALL_PACK(UUID,APPID,APPVER,OS,CPID,APKPKGNAME,APKVERSIONCODE,ORIGINALPACKID,CPUPDATETIME,UPDATETIME) " +
				"VALUES('yyy','xxx','5.7.327','os1','cp01', 'com.qo.android.am3', 1,'1','2012-01-01','2013-05-10 02:54:45')");
		
		simpleJdbcTemplate.update("INSERT INTO SOFTINFO(SOFTID,SOFTNAME,SOFTVERSION,SOFTTYPE,OSTYPE,OSID,SOFTFILENAME) " +
				"VALUES(1,'Quickofficeeeeee','5.7.327',2,200,12500000,'soft/files/20130220/20130220133351401.apk')");
		
		simpleJdbcTemplate.update("INSERT INTO RSCTYPE_MAPPER(CPID,CPAPPTYPEID,RSCTYPEID) " +
				"VALUES('CP01','1','1010302'),('CP01','2','1010401')");
		
		List<App> appList = appDao.findAll(0, 1).getList();
		List<AppInstallPack> packList = packDao.findAll(0,2).getList();
		System.out.println(appList);
		System.out.println(packList);
		assertEquals("1010401", appList.get(0).getRscTypeId());
		assertEquals("Quickofficeeeeee", appList.get(0).getName());
	}
	
	
	
	/**
	 * 当ANDROID的版本更新后, 是否能正常update
	 */
	@Test
	public void test3()	{
		deleteFromTables("app", "app_install_pack", "softinfo");

		simpleJdbcTemplate.update("INSERT INTO APP(UUID,NAME,ICONID,RSCTYPEID,DOWNCOUNT,UPDATETIME,CPID,ORIGINALAPPID,ORIGINALTYPEID) " +
				"VALUES('xxx', 'quickoffice', 1, '1010302', 154, '2013-05-10 02:54:45', 'cp01', '1', '1')");
		
		simpleJdbcTemplate.update("INSERT INTO APP_INSTALL_PACK(UUID,APPID,APPVER,OS,CPID,APKPKGNAME,APKVERSIONCODE,ORIGINALPACKID,CPUPDATETIME,UPDATETIME) " +
				"VALUES('yyy','xxx','5.7.327','os1','cp01', 'com.qo.android.am3', 1,'1','2012-01-01','2013-05-10 02:54:45')");
		
		simpleJdbcTemplate.update("INSERT INTO SOFTINFO(SOFTID,SOFTNAME,SOFTVERSION,SHANGJIADATE,SOFTTYPE,OSTYPE,OSID,SOFTFILENAME) " +
				"VALUES(1,'Quickoffice','5.7.3275555','2056-12-25',1,200,12500000,'soft/files/20130220/20130220133351401.apk')");
		
		List<App> appList = appDao.findAll(0, 1).getList();
		List<AppInstallPack> packList = packDao.findAll(0,2).getList();
		System.out.println(appList);
		System.out.println(packList);
	}
	
	/**
	 * 如果CP商提供的同一款ANDROID软件, 在版本更新后发现包名变化了, 是否能删除原PACK记录, 新加一条PACK记录, 并视情况新增APP
	 */
	@Test
	public void test4()	{
		deleteFromTables("app", "app_install_pack", "softinfo");
		
		simpleJdbcTemplate.update("INSERT INTO APP(UUID,NAME,ICONID,RSCTYPEID,DOWNCOUNT,UPDATETIME,CPID,ORIGINALAPPID,ORIGINALTYPEID) " +
		"VALUES('xxx', 'quickoffice', 1, '1010302', 154, '2013-05-10 02:54:45', 'cp01', '1', '1')");
		
		simpleJdbcTemplate.update("INSERT INTO APP_INSTALL_PACK(UUID,APPID,APPVER,OS,CPID,APKPKGNAME,APKVERSIONCODE,ORIGINALPACKID,CPUPDATETIME,UPDATETIME) " +
		"VALUES('yyy','xxx','5.7.327','os1','cp01', 'com.qo.android.am3', 1,'1','2012-01-01','2013-05-10 02:54:45')");
		
		simpleJdbcTemplate.update("INSERT INTO SOFTINFO(SOFTID,SOFTNAME,SOFTVERSION,SHANGJIADATE,SOFTTYPE,OSTYPE,OSID,SOFTFILENAME) " +
		"VALUES(1,'Quickoffice2(kuwo)','6.0.0','2056-12-25',1,200,12500000,'soft/files/20130314/20130314143012636.apk')");
		
//		aa.importToApp(i8SoftAdaptee);
		List<App> appList = appDao.findAll(0, 10).getList();
		List<AppInstallPack> packList = packDao.findAll(0,10).getList();
		System.out.println(appList);
		System.out.println(packList);
		assertEquals(2, appList.size());
		assertEquals(1, packList.size());
		assertFalse(packList.get(0).getUuid().equals("yyy"));
	}
	
	

	/**
	 * 当IOS正版离线包的更新时间改变后, 是否能正常update
	 * <依赖于软件地址是否能正常下载>
	 */
	@Test
	public void test5()	{
		deleteFromTables("app", "app_install_pack", "softinfo");
		simpleJdbcTemplate.update("INSERT INTO APP(UUID,NAME,ICONID,RSCTYPEID,DOWNCOUNT,UPDATETIME,CPID,ORIGINALAPPID,ORIGINALTYPEID) " +
		"VALUES('xxx', '掌上百度', 1, '1010302', 154, '2013-05-10 02:54:45', 'cp01', '1', '409')");
		
		simpleJdbcTemplate.update("INSERT INTO APP_INSTALL_PACK(UUID,APPID,APPVER,OS,CPID,IPABUNDLEID,VERSION,ORIGINALPACKID,CPUPDATETIME,UPDATETIME) " +
		"VALUES('yyy','xxx','4.8','os1','cp01', 'com.baidu.BaiduMobile', 1,'409','2012-01-01','2013-05-10 02:54:45')");
		
		simpleJdbcTemplate.update("INSERT INTO SOFTINFO(SOFTID,SOFTNAME,SOFTVERSION,SHANGJIADATE,SOFTTYPE,OSTYPE,OSID,LEGALFILENAME) " +
		"VALUES(409,'掌上百度','4.81','2013-05-15',2,300,12500000,'soft/files/20130421/20130421212517949.ipa')");
		
//		aa.importToApp(i8SoftAdaptee);
		List<App> appList = appDao.findAll(0, 1).getList();
		List<AppInstallPack> packList = packDao.findAll(0,2).getList();
		System.out.println(appList);
		System.out.println(packList);
		assertEquals("4.81", packList.get(0).getAppVer());
	}
	
	/**
	 * 自有软件资源和自有游戏资源, 如果一条IPHONE软件, 既提供的IOS又提供了IOSX, 这两个PACK的cpid和ORGINALPACKID相同.
	 * 是否能正常INSERT和UPDATE?
	 * <依赖于软件地址是否能正常下载>
	 */
	@Test
	public void test6()	{
		deleteFromTables("app", "app_install_pack", "softinfo");
		
		simpleJdbcTemplate.update("INSERT INTO SOFTINFO(SOFTID,SOFTNAME,SOFTVERSION,SHANGJIADATE,SOFTTYPE,OSTYPE,OSID,SOFTFILENAME,LEGALFILENAME,SOFTURL) " +
			"VALUES(407,'微信','4.5.0.11','2013-05-15',2,300,12500000,'soft/files/20130321/20130321163924325.ipa','soft/files/20130412/20130412092831667.ipa','https://itunes.apple.com/cn/app/wei-xin/id414478124?mt=8')");
		
//		aa.importToApp(i8SoftAdaptee);
		List<App> appList = appDao.findAll(0, 1).getList();
		List<AppInstallPack> packList = packDao.findAll(0,3).getList();
		System.out.println(appList);
		System.out.println(packList);
		assertEquals(3, packList.size());
	}
	
	
	@Test
	public void testSort()	{
		List<AppInstallPack> list = new ArrayList<AppInstallPack>();
		AppInstallPack 
			p1 = new AppInstallPack(),
			p2 = new AppInstallPack();
		p1.setUuid("1");
		p2.setUuid("2");

		p1.setOs("os2");
		p1.setCpId("cp01");
		p2.setOs("os2");
		p2.setCpId("cp07");
		list.add(p1);
		list.add(p2);

		Assert.assertEquals("2", list.get(1).getUuid());
	}
	
	@Test
	public void testFixMissingIcon()	{
		aa.fixMissingIconAndPic();
	}
}


