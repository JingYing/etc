package com.i8app.ezinstall.common.app.query;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.Constants.OrderBy;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;

public class AppQuerierImplTest extends BaseTest {

	@Resource
	AppQuerierImpl appQuerierImpl;
	
	@BeforeClass
	public static void setUp1()	{
		ClientParamHolder.setFileServerDir("c:/users/jing/desktop");
		ClientParamHolder.setFileServerUrl("http://124.248.34.134:8080/fileServer/");
	}
	
	@Before
	public void setUp()	{
		//deleteFromTables("resource_type", "pkg", "pkg_info", "pkg_group", "biz", "biz_area", "softinfo");
	}
	
//	@Test
//	public void testfindRscTypeList() {
//		simpleJdbcTemplate.update("INSERT INTO RESOURCE_TYPE(ID,PID,ISLEAF) VALUES('A', null, 0),('101', 'A', 0),('102', 'A',0)");
//		List<RscTypeDTO> list = appQuerierImpl.findRscTypeList("A");
//		assertEquals(2, list.size());
//		assertEquals("101", list.get(0).getId());
//		assertEquals("102", list.get(1).getId());
//	}
	
	/**
	 * 查询必备,榜单,强制安装业务对应的pkgGroup
	 * TODO
	 */
//	@Test
//	public void testfindPkgGroupAboutOtherBiz() {
//		List<PkgGroupDTO> list = appQuerierImpl.findPkgGroup(
//				new AreaParam("01", 19, 49), 1);
//		System.out.println(list);
//	}
//	
//	@Test
//	public void testfindPkgList() {
//		//simpleJdbcTemplate.update("INSERT INTO PKG(ID,NAME,INFO,GROUPID) VALUES(1,'','',1),(2,'','',1),(3,'','',1)");
//		List<PkgDTO> list = appQuerierImpl.findPkgList(1);
//		System.out.println(list.size());
////		assertEquals(3, list.size());
//		assertNotNull(list.get(0).getId());
//	}
	
//	@Test
//	public void testfindAppByPkg() {
//		//simpleJdbcTemplate.update("INSERT INTO PKG_INFO(PKGID,DISCRIMINATOR,APPID,NUM) VALUES(1,1,'123',1)");
//		List<String> list = new ArrayList<String>();
//		list.add(Constants.CP_WOSTORE);
//		
//		Pager<AppDTO> pager = appQuerierImpl.findAppByPkg(Os.ANDROID, 1, list, 0, 10);
//		System.out.println(pager.getTotalCount());
//		System.out.println(pager.getList().get(0).getIcon());
////		assertNotNull(pager.getList().get(0).getUuid());
////		System.out.println(pager.getList().get(0).getIcon());
//	}

	@Test
	public void testfindInstallPackByPkgWithoutAreaParam() {
		//simpleJdbcTemplate.update("INSERT INTO PKG_INFO(PKGID,DISCRIMINATOR,APPID,NUM) VALUES(1,1,'123',1)");
		String[] cpIds = new String[]{Constants.CP_I8SOFT, Constants.CP_I8GAME};
		Pager<AppDTO> pager = appQuerierImpl.findInstallPackByPkg(1, Os.ANDROID, 0, 10);
		System.out.println(pager.getTotalCount());
		System.out.println(pager.getList().get(0).getIcon());
//		assertNotNull(pager.getList().get(0).getUuid());
//		System.out.println(pager.getList().get(0).getIcon());
	}
	
	@Test
	public void testfindInstallPackByPkgWithAreaParam() {
		//simpleJdbcTemplate.update("INSERT INTO PKG_INFO(PKGID,DISCRIMINATOR,APPID,NUM) VALUES(1,1,'123',1)");
		AreaParam ap = new AreaParam();
		ap.setDeptId(Constants.DEPTID_LIANTONG);
		ap.setProvId(1);
		Pager<AppDTO> pager = appQuerierImpl.findInstallPackByPkg(ap, 1, Os.ANDROID, 0, 10);
		System.out.println(pager.getTotalCount());
		System.out.println(pager.getList().get(0).getIcon());
//		assertNotNull(pager.getList().get(0).getUuid());
//		System.out.println(pager.getList().get(0).getIcon());
	}
	
	@Test
	public void testQuery()	{
		AreaParam area = new AreaParam("01", 0, 0);
		AppParam ap = new AppParam();
		Pager<AppDTO> pager = appQuerierImpl.query(area, Os.ANDROID, ap, 0, 10);
		System.out.println(pager.getTotalCount());
		assertNotNull(pager.getList().get(0).getUuid());
	}

	@Test
	public void testQueryPack()	{
		AreaParam area = new AreaParam("01", 0, 0);
		AppParam ap = new AppParam(null,null,null,null,null, OrderBy.downCount);
		Pager<AppDTO> pager = appQuerierImpl.queryPack(area, Os.ANDROID, ap, 0, 10);
		System.out.println(pager.getTotalCount());
		assertNotNull(pager.getList().get(0).getUuid());
	}

	@Test
	public void testSearch()	{
		AreaParam area = new AreaParam("01", 0, 0);
		AppParam ap = new AppParam();
		Pager<AppDTO> pager = appQuerierImpl.search(area, Os.ANDROID, ap, 0, 10);
		System.out.println(pager.getTotalCount());
		assertNotNull(pager.getList().get(0).getUuid());
	}
	
//	@Test
//	public void testqueryAndFindUrl() throws CpServerAccessException	{
//		AreaParam area = new AreaParam("01", 0, 0);
//		AppParam ap = new AppParam();
//		Pager<AppDTO> pager = appQuerierImpl.query(area, Os.ANDROID, ap, 0, 10);
//		for(AppDTO a : pager.getList())	{
//			System.out.println(a.getUuid());
//			assertNotNull(pager.getList().get(0).getUuid());
//			assertNotNull(appQuerierImpl.getDownloadUrl(a.getUuid(), Os.ANDROID, null));
//		}
//	}
	
	@Test
	public void testfindSingle() {
		AppDTO app = appQuerierImpl.findSingle("056c6841-825c-4268-b998-ddb627ba5f72", Os.ANDROID);
		System.out.println(app);
	}

	@Test
	public void testfindSinglePack() {
		AppDTO app = appQuerierImpl.findSinglePack("c7c694ef-e50f-4aa6-83ea-5ca0fa7ea24f");
		System.out.println(app);
	}
	
	@Test
	public void testConfirmPack()	{
		String packId = appQuerierImpl.confirmPack("056c6841-825c-4268-b998-ddb627ba5f72", Os.ANDROID, Constants.CP_I8APP);
		System.out.println(packId);
	}

	@Test
	public void testGetPackUrl() throws CpServerAccessException	{
		String url = appQuerierImpl.getPackUrl("c7c694ef-e50f-4aa6-83ea-5ca0fa7ea24f");
		System.out.println(url);
		assertNotNull(url);
	}
	
//	@Test
//	public void testFindAppInstallPack1()	{
//		AppInstallPackDTO dto = appQuerierImpl.findAppInstallPack(
//				"00070cd5-a145-4bac-adc6-af96f537ac71", Os.IOS, null);
//		System.out.println(dto);
//	}
//	
//	@Test
//	public void testfindAppInstallPack2() {
//		List<String> appIds = new ArrayList<String>();
//		appIds.add("00070cd5-a145-4bac-adc6-af96f537ac71");
//		List<AppInstallPackDTO> list = appQuerierImpl.findAppInstallPack(
//				appIds, Os.IOS, "cp01");
//	}
//	
//	@Test
//	public void testGetDownloadUrl1() throws CpServerAccessException	{
//		AppInstallPackDTO dto = appQuerierImpl.getDownloadUrl("6bab3dcc-1ccc-4b41-8340-39721d5044f2");
//		System.out.println(dto);
//	}
//
//	@Test
//	public void testGetDownloadUrl2() throws CpServerAccessException	{
//		AppInstallPackDTO dto = appQuerierImpl.getDownloadUrl("6ae83d2a-af95-48b5-966b-9d47d871bbe1", Os.ANDROID, null);
//		System.out.println(dto);
//	}
	
	@Test
	public void testfindIpaItemId1()	{
		System.out.println(appQuerierImpl.findItunesId1("18352986-24c1-4159-9ec7-23c46c3e4603"));
	}

	@Test
	public void testfindIpaItemId2()	{
		System.out.println(appQuerierImpl.findItunesId2("f0921e84-b261-4fe3-b2d9-feed63464ebb"));
	}

}
