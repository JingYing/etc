package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class PkgInfoDao extends BaseDao {

	public List<Map> findOldPack()	{
		return sqlMapClientTemplate.queryForList("PkgInfo.findOldPack");
	}

	public List<Map> findOldApp() {
		return sqlMapClientTemplate.queryForList("PkgInfo.findOldApp");
	}

	public void updateAppId(int id, String uuid) {
		Map map = new HashMap(2);
		map.put("id", id);
		map.put("uuid", uuid);
		sqlMapClientTemplate.update("PkgInfo.updateAppId", map);
	}

	public List<Map> findOldI8Soft() {
		return sqlMapClientTemplate.queryForList("PkgInfo.findOldI8Soft");
	}

	public List<Map> findOldI8Game() {
		return sqlMapClientTemplate.queryForList("PkgInfo.findOldI8Game");
	}

}
