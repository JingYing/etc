package com.i8app.ezinstall.common.app.robot;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.I8appAcquire;
import com.i8app.ezinstall.common.app.dao.RscTypeMapperDao;
import com.i8app.ezinstall.common.app.domain.RscTypeMapper;
import com.i8app.ezinstall.common.wsclient.WdjJsonParser;

@Component
public class I8appAcquireOnInsert4Wdj implements I8appAcquire, Constants{
	
	@Resource
	private RscTypeMapperDao rscTypeMapperDao;
	
	private WdjJsonParser wdj;
	
	public WdjJsonParser getWdj() {
		return wdj;
	}

	public void setWdj(WdjJsonParser wdj) {
		this.wdj = wdj;
	}

	@Override
	public List<InputStream> getPics() {
		List<InputStream> list = new ArrayList<InputStream>();
		for(String url : wdj.getScreenshots())	{
			try {
				list.add(new URL(url).openStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public InputStream getBanner() {
		if(wdj.getBanner() != null)	{
			try {
				return new URL(wdj.getBanner()).openStream();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getOs() {
		return ANDROID;
	}

	@Override
	public String getInfo() {
		return wdj.getDescription();
	}

	@Override
	public String getDeveloper() {
		return wdj.getDeveloper();
	}

	@Override
	public String getChangeLog() {
		return wdj.getChangelog();
	}

	@Override
	public String getRscTypeId() {
		RscTypeMapper rtm = rscTypeMapperDao.findUnique(CP_WANDOUJIA, wdj.getCategory0());
		return rtm == null ? RSCTYPE_SOFT : rtm.getRscTypeId();	//没有匹配时, 默认101
	}

	@Override
	public int getIsAutoUpdate() {
		return 1;
	}

}
