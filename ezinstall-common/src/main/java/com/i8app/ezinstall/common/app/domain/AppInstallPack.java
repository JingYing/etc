package com.i8app.ezinstall.common.app.domain;

import java.util.Date;

public class AppInstallPack {
	private String uuid;
	private String appId, cpId, originalPackId, originalName, appVer, osMinVer, os,
			apkPkgName, ipaBundleId, cpUpdateTime, version, ipaItemId, appleId, changeLog;
	private Float fileSize;
	private Long appVerValue;
	private long osMinVerValue;
	private Date updateTime;
	private Integer apkVersionCode, ipaVersionCode;
	private int status;
	private App app;	//同时使用appId和app多对一对象
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getApkPkgName() {
		return apkPkgName;
	}

	public void setApkPkgName(String apkPkgName) {
		this.apkPkgName = apkPkgName;
	}

	public Long getAppVerValue() {
		return appVerValue;
	}
	
	public void setAppVerValue(Long appVerValue) {
		this.appVerValue = appVerValue;
	}
	
	public Float getFileSize() {
		return fileSize;
	}

	public void setFileSize(Float fileSize) {
		this.fileSize = fileSize;
	}

	public long getOsMinVerValue() {
		return osMinVerValue;
	}

	public void setOsMinVerValue(long osMinVerValue) {
		this.osMinVerValue = osMinVerValue;
	}

	public String getAppVer() {
		return appVer;
	}

	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}

	public String getOsMinVer() {
		return osMinVer;
	}

	public void setOsMinVer(String osMinVer) {
		this.osMinVer = osMinVer;
	}

	public String getOriginalPackId() {
		return originalPackId;
	}

	public void setOriginalPackId(String originalPackId) {
		this.originalPackId = originalPackId;
	}

	public String getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(String cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getIpaBundleId() {
		return ipaBundleId;
	}

	public void setIpaBundleId(String ipaBundleId) {
		this.ipaBundleId = ipaBundleId;
	}

	public Integer getApkVersionCode() {
		return apkVersionCode;
	}

	public void setApkVersionCode(Integer apkVersionCode) {
		this.apkVersionCode = apkVersionCode;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getIpaVersionCode() {
		return ipaVersionCode;
	}

	public void setIpaVersionCode(Integer ipaVersionCode) {
		this.ipaVersionCode = ipaVersionCode;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	
	public String getIpaItemId() {
		return ipaItemId;
	}

	public void setIpaItemId(String ipaItemId) {
		this.ipaItemId = ipaItemId;
	}
	
	public String getAppleId() {
		return appleId;
	}

	public void setAppleId(String appleId) {
		this.appleId = appleId;
	}
	
	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}
	
	public String getChangeLog() {
		return changeLog;
	}

	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}

}
