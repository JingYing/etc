package com.i8app.ezinstall.common.app.assembly;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.assembly.cp.AxonAdapteeImpl;
import com.i8app.ezinstall.common.app.assembly.cp.I8appAdapteeImpl;
import com.i8app.ezinstall.common.app.assembly.cp.WostoreAdapteeImpl;
import com.i8app.ezinstall.common.app.query.I8GameCpQueryImpl;
import com.i8app.ezinstall.common.app.query.I8SoftCpQueryImpl;

@Component
public class AppAdapteeFactory implements Constants{
	@Resource
	protected AxonAdapteeImpl axonAdapteeImpl;
	@Resource
	protected WostoreAdapteeImpl wostoreAdapteeImpl;
	@Resource
	protected I8appAdapteeImpl i8appAdapteeImpl;
	
	/**
	 * 使用assembly包下appAdaptee的实现, 或query包下的ICpQuery实现
	 * @param cpId
	 * @return
	 */
	public AppAdaptee factory(String cpId)	{
		Map<String, String> map = ClientParamHolder.getCpMapToAssembly();
		if(CP_AXON.equalsIgnoreCase(cpId))	{
			axonAdapteeImpl.setPackAccessMode(map.get(cpId));
			return axonAdapteeImpl;
		} else if(CP_WOSTORE.equalsIgnoreCase(cpId))	{
			wostoreAdapteeImpl.setPackAccessMode(map.get(cpId));
			return wostoreAdapteeImpl;
		} else if(CP_I8APP.equalsIgnoreCase(cpId))	{
			i8appAdapteeImpl.setPackAccessMode(map.get(cpId));
			return i8appAdapteeImpl;
		}
		throw new IllegalArgumentException("不支持的cpId:" + cpId);
	}

}
