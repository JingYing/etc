package com.i8app.ezinstall.common.app.query;

import java.util.ArrayList;
import java.util.List;

import com.i8app.ezinstall.common.Util;

/**
 * 手机操作系统的枚举类.<br>
 * 生成实例的方式:<br>
 * 1.使用枚举:Os.ANDROID, Os.IOS, Os.IOSX ....默认版本号为最新版本<br>
 * 2.使用静态工厂方法:Os os = Os.getInstance("os1"), 默认版本号为最新版本<br>
 * 		如果需要手动设置版本号, 再调用os.setOsVersion(String osVer) <br>
 * 3.使用带版本号的静态工厂方法: Os os = Os.getInstance("os1", "1.2.3")<br> 
 * <br>
 * 如果需要检测输入的版本号是否有效, 使用Os.validateOsVer(String osVer)
 * 
 * @author JingYing 2013-5-17
 */
public enum Os {

	ANDROID("os1"), IOS("os2"), IOSX("os3"), 
	SYMBIAN("os4"), JAVA("os5"), WINMOBILE("os6");
	
	/**
	 * 获取所有OS列表(无版本适配功能)
	 * @return
	 */
	public static List<Os> getAllOs()	{
		List<Os> list = new ArrayList<Os>();
		list.add(Os.ANDROID);
		list.add(Os.IOS);
		list.add(Os.IOSX);
		list.add(Os.SYMBIAN);
		list.add(Os.JAVA);
		list.add(Os.WINMOBILE);
		return list;
	}
	
	/**
	 * 根据osStr, 获取一个os实例. 该实例中的操作系统版本号为最新.
	 * @param osStr 取值	ANDROID("os1"), IOS("os2"), IOSX("os3"), 
	 * 				SYMBIAN("os4"), JAVA("os5"), WINMOBILE("os6")
	 * @throws IllegalArgumentException osStr值无效
	 * @return
	 */
	public static Os getInstance(String osStr)	{
		return getInstance(osStr, null);
	}
	
	/**
	 * 根据osStr,osVersion, 获取一个os实例. 该实例中的操作系统版本号根据osVersion进行转换. 如果转换失败, 则版本号默认最新
	 * @param osStr 取值	ANDROID("os1"), IOS("os2"), IOSX("os3"), 
	 * 				SYMBIAN("os4"), JAVA("os5"), WINMOBILE("os6")
	 * @param osVersion
	 * @throws IllegalArgumentException osStr值无效
	 * @return
	 */
	public static Os getInstance(String osStr, String osVersion)	{
		Os os;
		if("os1".equals(osStr))	{
			os = Os.ANDROID;
			if(osVersion != null)	os.setOsVersion(osVersion);
		} else if("os2".equals(osStr))	{
			os = Os.IOS;
			if(osVersion != null)	os.setOsVersion(osVersion);
		} else if("os3".equals(osStr))	{
			os = Os.IOSX;
			if(osVersion != null)	os.setOsVersion(osVersion);
		} else if("os4".equals(osStr))	{
			os = Os.SYMBIAN;
		} else if("os5".equals(osStr))	{
			os = Os.JAVA;
		} else if("os6".equals(osStr))	{
			os = Os.WINMOBILE;
			if(osVersion != null)	os.setOsVersion(osVersion);
		} else	{
			throw new IllegalArgumentException("无效的osStr值:" + osStr);
		}
		return os;
	}
	
	private Long VerValue = Long.MAX_VALUE;	//默认为最高值, 即该操作系统版本最新
	private String osStr, osVer;
	private Os()	{	}
	
	private Os(String osStr)	{
		this.osStr = osStr;
	}
	
	public Long getVerValue() {
		return VerValue;
	}
	
	public String getOsStr()	{
		return osStr;
	}
	
	public String getOsVer()	{
		return osVer;
	}
	
	/**
	 * 测试版本号的有效性, 成功则返回解析后的字符串. 失败则抛出InvalidOsException.<br>
	 * 可以用该函数事先校验版本号.<br>
	 * @param osVer
	 * @return
	 * @throws InvalidOsVersionException
	 */
	public static String validateOsVer(String osVer) throws InvalidOsVersionException	{
		String version = Util.filterNumAndPoint(osVer);
		if(version != null && !"".equals(version))	{	//如果version不符合规则
			int num = version.split("[.]").length;	 
			for(int i=0; i<5-num; i++)	{	//补足5位
				version += ".0";
			}
			if(version.matches("^\\d+[.]\\d+[.]\\d+[.]\\d+[.]\\d+$"))	{
				return version;
			} else	{
				throw new InvalidOsVersionException("无效的版本号.转换前:" + osVer + ";转换后:" + version);
			}
		} else	{
			throw new InvalidOsVersionException("无效的版本号:" + osVer);
		}
	}
	
	/**
	 * 将操作系统版本号字符串, 过滤掉非数字, 补足4个小数点, 转换成long值.<br>
	 * 如果osVer无法转换, 则操作系统的版本号long值为默认值Long.maxValue, 即默认操作系统为最新版本.<br>
	 * @param osVer 例如1.2.5这种以小数点分隔的数字串
	 * @return
	 */
	public void setOsVersion(String osVer) {
		this.osVer = osVer;
		if(osVer == null)
			throw new IllegalArgumentException("无效的版本号:" + osVer);
		
		String version = Util.filterNumAndPoint(osVer);
		if(version != null && !"".equals(version))	{	//如果version不符合规则
			int num = version.split("[.]").length;	 
			for(int i=0; i<5-num; i++)	{	//补足5位
				version += ".0";
			}
			if(version.matches("^\\d+[.]\\d+[.]\\d+[.]\\d+[.]\\d+$"))	{
				String[] strings = version.split("[.]");
				long sum = 0;
				for(int i=0, j=strings.length-1; i<strings.length; i++, j--)	{
					sum += Integer.parseInt(strings[i]) * Math.pow(50, j);
				}
				switch(this)	{
					case IOS :
					case IOSX:
						sum += 400000000;
						break;
					case ANDROID:
						break;
					case WINMOBILE:
						sum += 800000000;
						break;
					default : 
						sum = 0;
						break;
				}
				this.VerValue = sum;
			}
		}
	}
}
