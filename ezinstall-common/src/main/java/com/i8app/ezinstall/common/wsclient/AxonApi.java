package com.i8app.ezinstall.common.wsclient;

/**
 * 发布的hessian接口. 客户端使用hessian调用此接口
 * 
 * @author JingYing 2013-7-18
 */
public interface AxonApi {

	
	/**
	 * 更新所有的安讯资源
	 */
	void updateFromAxonServer();
	
	
	/**
	 * 获取下载地址
	 * @param id
	 * @return
	 */
	String getDownloadUrl(String id) throws Exception;

}
