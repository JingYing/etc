package com.i8app.ezinstall.common.app;

import java.io.InputStream;
import java.util.List;

/**
 * 
 * I8appAcquire的抽象处理类, 当更新安装包时, 已实现的字段不需要处理.
 * @author JingYing 2014-1-3
 */
public abstract class AbstractI8appAcquireOnUpdate implements I8appAcquire	{

	@Override
	public List<InputStream> getPics() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getOs() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getRscTypeId() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getIsAutoUpdate() {
		throw new UnsupportedOperationException();
	}

}
