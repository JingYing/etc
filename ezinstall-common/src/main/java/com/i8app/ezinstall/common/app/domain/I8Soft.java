package com.i8app.ezinstall.common.app.domain;

public class I8Soft {
	protected Integer id;
	protected String name;
	protected String manu;
	protected String version;
	protected Integer fileSize;
	protected Integer fee;
	protected String appType;
	protected String date;
	protected String info;
	protected Integer osType;
	private OsInfo osInfo;
	protected String icon;
	protected String pic1, pic2, pic3;
	protected String file;
	protected Integer hotRecmd, flag, userRemarkNum;
	protected Float userRemark;
	protected Integer downNum, installCount, weekInstallCount,
			monthInstallCount;
	protected String url, legalFileName, tmcImage, legalTmcImage, mnemonicCode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManu() {
		return manu;
	}

	public void setManu(String manu) {
		this.manu = manu;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getFileSize() {
		return fileSize;
	}

	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}

	public Integer getFee() {
		return fee;
	}

	public void setFee(Integer fee) {
		this.fee = fee;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Integer getOsType() {
		return osType;
	}

	public void setOsType(Integer osType) {
		this.osType = osType;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPic1() {
		return pic1;
	}

	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}

	public String getPic2() {
		return pic2;
	}

	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}

	public String getPic3() {
		return pic3;
	}

	public void setPic3(String pic3) {
		this.pic3 = pic3;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Integer getHotRecmd() {
		return hotRecmd;
	}

	public void setHotRecmd(Integer hotRecmd) {
		this.hotRecmd = hotRecmd;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Integer getUserRemarkNum() {
		return userRemarkNum;
	}

	public void setUserRemarkNum(Integer userRemarkNum) {
		this.userRemarkNum = userRemarkNum;
	}

	public Float getUserRemark() {
		return userRemark;
	}

	public void setUserRemark(Float userRemark) {
		this.userRemark = userRemark;
	}

	public Integer getDownNum() {
		return downNum;
	}

	public void setDownNum(Integer downNum) {
		this.downNum = downNum;
	}

	public Integer getInstallCount() {
		return installCount;
	}

	public void setInstallCount(Integer installCount) {
		this.installCount = installCount;
	}

	public Integer getWeekInstallCount() {
		return weekInstallCount;
	}

	public void setWeekInstallCount(Integer weekInstallCount) {
		this.weekInstallCount = weekInstallCount;
	}

	public Integer getMonthInstallCount() {
		return monthInstallCount;
	}

	public void setMonthInstallCount(Integer monthInstallCount) {
		this.monthInstallCount = monthInstallCount;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLegalFileName() {
		return legalFileName;
	}

	public void setLegalFileName(String legalFileName) {
		this.legalFileName = legalFileName;
	}

	public String getTmcImage() {
		return tmcImage;
	}

	public void setTmcImage(String tmcImage) {
		this.tmcImage = tmcImage;
	}

	public String getLegalTmcImage() {
		return legalTmcImage;
	}

	public void setLegalTmcImage(String legalTmcImage) {
		this.legalTmcImage = legalTmcImage;
	}

	public String getMnemonicCode() {
		return mnemonicCode;
	}

	public void setMnemonicCode(String mnemonicCode) {
		this.mnemonicCode = mnemonicCode;
	}

	public OsInfo getOsInfo() {
		return osInfo;
	}

	public void setOsInfo(OsInfo osInfo) {
		this.osInfo = osInfo;
	}

	@Override
	public String toString() {
		return "I8Soft [id=" + id + ", name=" + name + ", manu=" + manu
				+ ", version=" + version + ", fileSize=" + fileSize + ", fee="
				+ fee + ", appType=" + appType + ", date=" + date + ", info="
				+ info + ", osType=" + osType + ", osInfo=" + osInfo
				+ ", icon=" + icon + ", pic1=" + pic1 + ", pic2=" + pic2
				+ ", pic3=" + pic3 + ", file=" + file + ", hotRecmd="
				+ hotRecmd + ", flag=" + flag + ", userRemarkNum="
				+ userRemarkNum + ", userRemark=" + userRemark + ", downNum="
				+ downNum + ", installCount=" + installCount
				+ ", weekInstallCount=" + weekInstallCount
				+ ", monthInstallCount=" + monthInstallCount + ", url=" + url
				+ ", legalFileName=" + legalFileName + ", tmcImage=" + tmcImage
				+ ", legalTmcImage=" + legalTmcImage + ", mnemonicCode="
				+ mnemonicCode + "]";
	}
	
}
