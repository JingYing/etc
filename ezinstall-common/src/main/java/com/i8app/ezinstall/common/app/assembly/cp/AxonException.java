package com.i8app.ezinstall.common.app.assembly.cp;

import com.i8app.ezinstall.common.app.CpServerAccessException;

/**
 * 连接安讯服务器时出现的异常
 * 
 * @author JingYing 2013-5-6
 */
public class AxonException extends CpServerAccessException	{

	public AxonException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AxonException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AxonException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AxonException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
