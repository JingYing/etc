
package com.i8app.ezinstall.common.app.assembly.process;

import static com.i8app.ezinstall.common.app.Constants.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.AppService;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppMeta;
import com.i8app.ezinstall.common.app.assembly.AssemblyReport;
import com.i8app.ezinstall.common.app.dao.AppInstallPackDao;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.packreader.PackReadException;

/**
 * java/symbian/wm用
 * 
 * @author JingYing 2014-1-13
 * @deprecated 不再导入ios/android以外的资源
 */
@Component
public class OtherOsProcessorImpl implements OsProcessor{
	
	private static Logger log = Logger.getLogger(OtherOsProcessorImpl.class);
	
	@Resource
	protected AppInstallPackDao packDao;
	@Resource
	private AppService appService;

	@Override
	public AssemblyReport process(AppMeta appMeta, AppAdaptee adaptee) {
		AssemblyReport rpt = new AssemblyReport();
		rpt.setCpId(appMeta.getCpId());
		rpt.setOriginalPackId(appMeta.getOriginalPackId());
		rpt.setOs(appMeta.getOs());
		rpt.setAssemblyStatus(ERRCODE_NORMAL);
		
		AppInstallPack existPack = packDao.find(appMeta.getCpId(), appMeta.getOriginalPackId(), appMeta.getOs());
		
		if(existPack != null)	{	//packid & originalpackid & os 已存在, 直接更新pack的version字段和cpUpdateTime	
			this.whenNotUpdate(appMeta, existPack);
			return rpt;
		} else	{
			try {
				Long filesize = (Long)adaptee.readPack(appMeta.getOriginalPackId(), appMeta.getOs());//只用来校验文件是否存在
				App a = appService.find(appMeta.getCpId(), appMeta.getOriginalAppId());
				if(a == null)	{	//cpid & originalAppid在app表中不存在
					a = insertNewApp(appMeta, filesize);
				} else	{	//cpid & originalAppid在app表中已存在
					updateExistApp(a, appMeta);
				}
				whenPackNotIn(a.getUuid(), appMeta, filesize);
			} catch (CpServerAccessException e) {
				log.error(e.getMessage());
				rpt.setAssemblyStatus(ERRCODE_DOWNLOAD_FAIL);
			} catch (FileNotFoundException e) {
				log.error(e.getMessage());
				rpt.setAssemblyStatus(ERRCODE_NO_PACK_ON_DISK);
			} catch (PackReadException e) {	//目前不会出现这种异常
				e.printStackTrace();
			}
		}
		return rpt;
	}

	private void updateExistApp(App existApp, AppMeta appMeta) {
//		existApp.set TODO
		existApp.setUpdateTime(new Date());
		appService.persist(existApp);
	}

	private void whenPackNotIn(String existAppId, AppMeta appMeta, Long filesize) {
		AppInstallPack p = new AppInstallPack();
		p.setApkPkgName(null);
		p.setApkVersionCode(null);
		p.setAppId(existAppId);
		p.setAppleId(null);
		p.setAppVer(appMeta.getOriginalVersion());
		p.setAppVerValue(null);	//不需要对比
		p.setCpId(appMeta.getCpId());
		p.setCpUpdateTime(appMeta.getCpUpdateTime());
		p.setFileSize(new Float(filesize/1024));
		p.setIpaBundleId(null);
		p.setIpaItemId(null);
		p.setIpaVersionCode(null);
		p.setOriginalName(appMeta.getOriginalName());
		p.setOriginalPackId(appMeta.getOriginalPackId());
		p.setOs(appMeta.getOs());
		p.setOsMinVer(appMeta.getOsMinVer());
		p.setOsMinVerValue(toOsMinVerValue(p.getOsMinVer()));
		p.setStatus(appMeta.getStatus());
		p.setUpdateTime(new Date());
		p.setUuid(UUID.randomUUID().toString());
		p.setVersion(null);
		p.setChangeLog(appMeta.getChangeLog());
		p.setUpdateTime(new Date());
		packDao.insert(p);
	}

	private App insertNewApp(AppMeta appMeta, Long fileSize) {
		App a = new App();
		if(appMeta.getBannerUri() != null){
			String banner;
			try {
				banner = appService.saveToPicDir(appMeta.getBannerUri().toURL().openStream(), ".jpg");
				a.setBanner(banner);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		a.setBundleId(null);
		a.setCpId(appMeta.getCpId());
		a.setDeveloper(appMeta.getDeveloper());
		a.setDownCount(0);	//初始0
		a.setFileSize(new Long(fileSize/1024).floatValue());
		a.setInfo(appMeta.getInfo());
		a.setInstallCount(0);	
		a.setIpaItemId(null);
		a.setIsAreaCustom(0);
		a.setIsDel(0);
		a.setName(appMeta.getOriginalName());
		a.setOriginalAppId(appMeta.getOriginalAppId());
		a.setOriginalTypeId(appMeta.getOriginalTypeId());
		a.setOs(appMeta.getOs());
		a.setPinyin(Util.getPinyinInitial(a.getName()));
		a.setPoint(null);
		a.setRscTypeId(appMeta.getRscTypeId());
		a.setUpdateTime(new Date());
		a.setUuid(UUID.randomUUID().toString());
		if(appMeta.getIconUri() != null)	{
			try {
				String iconPath = appService.saveToIconDir(appMeta.getIconUri().toURL().openStream(), ".jpg");
				a.setIcon(iconPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//存截图
		int index = 1;
		for(URI uri : appMeta.getScreenshotsUri())	{
			try {
				String halfPath = appService.saveToPicDir(uri.toURL().openStream(), ".jpg");	//统一jpg
				a.getClass().getMethod("setPic" + index, String.class).invoke(a, halfPath);
			} catch (Exception e) {
				e.printStackTrace();
			}
			index ++;
		}
		appService.persist(a);
		return a;
	}

	private void whenNotUpdate(AppMeta appMeta, AppInstallPack existPack) {
		//TODO
	}
	
	/**
	 * 转换osMinverValue. 不识别时转换成0
	 * @param minOsVer
	 * @return
	 */
	private long toOsMinVerValue(String minOsVer)	{
		try {
			int num = minOsVer.split("[.]").length;
			for (int i = 0; i < 5 - num; i++) {
				minOsVer += ".0";
			}
			String[] strings = minOsVer.split("[.]");
			int sum = 0;
			for (int i = 0, j = strings.length - 1; i < strings.length; i++, j--) {
				sum += Integer.parseInt(strings[i]) * Math.pow(50, j);
			}
			return sum + 400000000;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
