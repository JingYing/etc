package com.i8app.ezinstall.common.app.query;

import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.SpringIOC;

/**
 * 根据applicationContext.xml,构建IOC容器, 并获取appQuerierImpl
 * 
 * @author JingYing 2013-5-16
 */
public class AppQuerierFactory implements FactoryBean<AppQuerier>{
	
	@Override
	public AppQuerier getObject() throws Exception {
		
		if(ClientParamHolder.getFileServerUrl() ==  null 
				|| ClientParamHolder.getDataSource() == null)	{
			throw new IllegalArgumentException("appQuerierFactory需要设置fileServerUrl, dataSource");
		}
		return SpringIOC.getIOC().getBean(AppQuerier.class);	//调用appQuerierImpl
	}

	@Override
	public Class<?> getObjectType() {
		return AppQuerier.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
	
	/**
	 * app查询器查询的图片和软件包等下载路径, 所使用的路径头地址
	 * @param fileServerUrl
	 */
	public void setFileServerUrl(String fileServerUrl)	{
		ClientParamHolder.setFileServerUrl(fileServerUrl);
	}
	
	/**
	 * app查询器操作的数据库
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource) {
		ClientParamHolder.setDataSource(dataSource);
	}
	
	public void setConnectorUrl(String connectorUrl)	{
		ClientParamHolder.setConnectorUrl(connectorUrl);
	}
	
	public void setRemoteType(int remoteType)	{
		ClientParamHolder.setRemoteType(remoteType);
	}
}
