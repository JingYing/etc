package com.i8app.ezinstall.common.app.query;

import java.util.List;

import com.i8app.ezinstall.common.app.CpServerAccessException;

/**
 * 查询CP商的软件地址或截图地址
 * @author JingYing 2013-6-13
 */
public interface CpQuery {
	
	/**
	 * 获取安装包下载地址
	 * @param pack
	 * @return
	 * @throws CpServerAccessException 
	 */
	String findPackUrl(String originalPackId, String os) throws CpServerAccessException;
	
	/**
	 * 查找CP商某软件的截图列表
	 * @param originalAppId 原始id
	 * @return
	 */
	List<String> findScreenshots(String originalAppId);
	
	/**
	 * 查询ICON的URL
	 * @param originalAppId
	 * @return
	 */
	String findIconUrl(String originalAppId);
	
//	/**
//	 * 根据CP软件ID, 查询CP软件, 不是查询CP安装包
//	 * @param originalAppId
//	 * @return
//	 */
//	Object findAppByAppId(String originalAppId);
//	
//	/**
//	 * 根据ID查找
//	 * @param originalAppIds cp软件(不是安装包)的原始id
//	 * @return
//	 */
//	List findAppByAppIds(List<String> originalAppIds);
	
	List findByOriginalPackIds(List<String> originalPackIds);

}
