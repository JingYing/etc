package com.i8app.ezinstall.common.app.robot;

import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.SpringIOC;

public class I8appRobotFactory implements FactoryBean<I8appApkRobot>{

	@Override
	public I8appApkRobot getObject() throws Exception {
		if(ClientParamHolder.getFileServerUrl() ==  null 
				|| ClientParamHolder.getDataSource() == null 
				|| ClientParamHolder.getFileServerDir() == null 
				)	{
			throw new IllegalArgumentException("appQuerierFactory需要设置fileServerDir, fileServerUrl, dataSource");
		}
		return SpringIOC.getIOC().getBean(I8appApkRobot.class);
	}

	@Override
	public Class<?> getObjectType() {
		return I8appApkRobot.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
	
	public void setDataSource(DataSource dataSource) {
		ClientParamHolder.setDataSource(dataSource);
	}
	
	public void setFileServerDir(String fileServerDir)	{
		ClientParamHolder.setFileServerDir(fileServerDir);
	}

	public void setFileServerUrl(String fileServerUrl)	{
		ClientParamHolder.setFileServerUrl(fileServerUrl);
	}

}
