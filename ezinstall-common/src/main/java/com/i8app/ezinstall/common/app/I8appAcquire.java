package com.i8app.ezinstall.common.app;

import java.io.InputStream;
import java.util.List;

/**
 * 新增或更新i8app安装包时, 附带的安装包信息
 * 
 * @author JingYing 2013-12-31
 */
public interface I8appAcquire {
	
	/**
	 * 截图
	 * @return
	 */
	List<InputStream> getPics();
	
	/**
	 * banner图片
	 * @return
	 */
	InputStream getBanner();
	
	/**
	 * 操作系统
	 * @return
	 */
	String getOs();
	
	/**
	 * 详情
	 * @return
	 */
	String getInfo();
	
	/**
	 * 开发商
	 * @return
	 */
	String getDeveloper();
	
	/**
	 * 更新日志
	 * @return
	 */
	String getChangeLog();
	
	/**
	 * 类别ID
	 * @return
	 */
	String getRscTypeId();

	/**
	 * 是否自动更新
	 * @return
	 */
	int getIsAutoUpdate();

}
