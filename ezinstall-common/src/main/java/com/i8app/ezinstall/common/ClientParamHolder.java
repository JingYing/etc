package com.i8app.ezinstall.common;

import java.net.MalformedURLException;
import java.util.Map;

import javax.sql.DataSource;

import com.caucho.hessian.client.HessianProxyFactory;
import com.i8app.ezinstall.common.wsclient.AxonApi;

/**
 * 统一存放调用端传递过来的各种参数
 * @author JingYing 2013-5-25
 *
 */
public class ClientParamHolder {

	private static DataSource dataSource;
	private static AxonApi axonApi;
	private static String fileServerUrl, fileServerDir, connectorUrl;
	private static Map<String,String> cpMapToAssembly;
	private static int remoteType = 1;	//1:使用本机直连外部系统, 2:使用接口服务器连接外部系统

	public static DataSource getDataSource() {
		return dataSource;
	}

	public static void setDataSource(DataSource dataSource) {
		ClientParamHolder.dataSource = dataSource;
	}

	public static String getFileServerDir() {
		return fileServerDir;
	}

	public static void setFileServerDir(String fileServerDir) {
		ClientParamHolder.fileServerDir = fileServerDir;
	}

	public static String getFileServerUrl() {
		return fileServerUrl;
	}

	public static void setFileServerUrl(String fileServerUrl) {
		ClientParamHolder.fileServerUrl = fileServerUrl;
	}

	public static void setCpMapToAssembly(Map<String,String> map) {
		ClientParamHolder.cpMapToAssembly = map;
	}

	public static Map<String,String> getCpMapToAssembly() {
		return cpMapToAssembly;
	}

	public static AxonApi getAxonApi() {
		return axonApi;
	}

	public static String getConnectorUrl() {
		return connectorUrl;
	}

	public static void setConnectorUrl(String connectorUrl) {
		ClientParamHolder.connectorUrl = connectorUrl;
		String url = "http://" + connectorUrl + "/hessian/axon"; 
		HessianProxyFactory factory = new HessianProxyFactory(); 
		factory.setConnectTimeout(15000);	//15秒超时
		factory.setReadTimeout(15000);		//15秒超时
		try { 
			axonApi =(AxonApi)factory.create(AxonApi.class,url); 
		} catch (MalformedURLException e) { 
			e.printStackTrace(); 
		} 
	}

	public static int getRemoteType() {
		return remoteType;
	}

	public static void setRemoteType(int remoteType) {
		ClientParamHolder.remoteType = remoteType;
	}
}
