package com.i8app.ezinstall.common.app;

/**
 * 访问CP商服务器时出现的异常
 * 
 * @author JingYing 2013-5-6
 */
public class CpServerAccessException extends Exception	{

	public CpServerAccessException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CpServerAccessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CpServerAccessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CpServerAccessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	
}
