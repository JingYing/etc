package com.i8app.ezinstall.common.app.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.domain.PkgGroup;
import com.i8app.ezinstall.common.app.query.AreaParam;

@Component
public class PkgGroupDao extends BaseDao{

	/**
	 * @param areaParam
	 * @param bizId
	 * @return
	 */
	public List<PkgGroup> find(AreaParam areaParam, int bizId) {
		Map<String, Object> map = areaParam.toParamMap();
		map.put("bizId", bizId);
		return sqlMapClientTemplate.queryForList("PkgGroup.byBizId", map);
	}
}
