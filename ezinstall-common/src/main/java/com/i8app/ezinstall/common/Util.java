package com.i8app.ezinstall.common;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class Util {

	/**
	 * 获取拼音首字母, 忽略标点符号, 英文不做转换
	 * 
	 * @param s
	 * @return
	 */
	public static String getPinyinInitial(String s) {
		StringBuffer pybf = new StringBuffer();
		char[] arr = s.toCharArray();
		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		try {
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] > 128) {
					String[] temp = PinyinHelper.toHanyuPinyinStringArray(
							arr[i], format);
					if (temp != null) {
						pybf.append(temp[0].charAt(0));
					}
				} else {
					pybf.append(arr[i]);
				}
			}
		} catch (BadHanyuPinyinOutputFormatCombination e) {
			e.printStackTrace();
		}
		String result = pybf.toString().replaceAll("\\W", "").trim();
		return result;
	}

	/**
	 * 过滤字符串中的小数点和数字以外的字符
	 * 
	 * @param s
	 * @return
	 */
	public static String filterNumAndPoint(String s) {
		StringBuilder sb = new StringBuilder();
		char[] cs = s.toCharArray();
		for (char c : cs) {
			if ((c > 47 && c < 58) || c == 46) {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * 截取字符串中, 最后一个句号后的字符串, 在url中, 它一般代表文件的后缀名
	 * 
	 * @param url
	 * @return
	 */
	public static String getSuffix(String url) {
		return url.substring(url.lastIndexOf("."));
	}
	
	/**
	 * MD5加密
	 * 
	 * @param str
	 *            要加密的字符串
	 * @return 加密后的字符串
	 */
	public static String toMD5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte[] byteDigest = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < byteDigest.length; offset++) {
				i = byteDigest[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}

			// return buf.toString().substring(8, 24); //16位加密
			return buf.toString(); // 32位加密
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * unicode码转换
	 * @param unicode
	 * @return
	 */
	public static String unicodeToUtf8(String unicode) {
		if(unicode == null)	return null;
		char aChar = 'c';
		int len = unicode.length();
		StringBuffer outBuffer = new StringBuffer(len);
		for (int x = 0; x < len;) {
			aChar = unicode.charAt(x++);
			if (aChar == '\\') {
			try {
				aChar = unicode.charAt(x++);
			} catch (Exception e) {
				e.printStackTrace();
			}
				if (aChar == 'u') {
					// Read the xxxx
					int value = 0;
					for (int i = 0; i < 4; i++) {
						aChar = unicode.charAt(x++);
						switch (aChar) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							value = (value << 4) + aChar - '0';
							break;
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
							value = (value << 4) + 10 + aChar - 'a';
							break;
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
							value = (value << 4) + 10 + aChar - 'A';
							break;
						default:
							value = 1;
							break;
						}
					}
					outBuffer.append((char) value);
				} else {
					outBuffer.append("\\");
					outBuffer.append(aChar);
				}
			} else
				outBuffer.append(aChar);
		}
		return outBuffer.toString();
	}
	
	/**
	 * 关闭多个流
	 * @param closeable
	 */
	public static void closeStream(Closeable...closeable)	{
		for(Closeable c : closeable)	{
			try {
				if(c != null)	c.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * encode中文路径
	 * @param url
	 * @return
	 */
	public static String encodeChUrl(String url)	{
		String regex = "http://[^/]+/";
		Matcher m = Pattern.compile(regex).matcher(url);
		m.find();
		String s1 = url.substring(0, m.end());
		String s2;
		try {
			s2 = URLEncoder.encode(url.substring(m.end()), "utf-8");
			return s1 + s2;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 判断是否有中文
	 * @param s
	 * @return
	 */
	public static boolean containChinese(String s)	{
		if(s == null)	return false;
		return s.getBytes().length == new String(s).length() ? false: true; 
	}
	
	/**
	 * 生成一串随机数组成的字符串
	 * @param num 随机数的位数
	 * @return
	 */
	public static String randStr(int num)	{
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i=0; i<num; i++)	{
			sb.append("" + rand.nextInt(10));
		}
		return sb.toString();
	}
	
	/**
	 * 对比版本号
	 * @param ver1
	 * @param ver2
	 * @return
	 * @throws Exception
	 */
	public static int compareVersion(String ver1, String ver2) throws Exception{
		String[] v1 = filterNumAndPoint(ver1).split("[.]");
		String[] v2 = filterNumAndPoint(ver2).split("[.]");
		for(int i=0; i<v1.length; i++)	{
			if(Integer.parseInt(v1[i]) < Integer.parseInt(v2[i]))	{
				return -1;
			} else if(Integer.parseInt(v1[i]) > Integer.parseInt(v2[i]))	{
				return 1;
			}
		}
		return 0;
		
	}
	
	/**
	 * 将FileRead指向的文件写入到fileWrite指向的文件中
	 * 如果父路径不存在则自动创建
	 * @param fileRead
	 * @param fileWrite
	 * @throws IOException 
	 */
	public static void copyFile(File fileRead, File fileWrite) throws IOException	{
		if(!fileWrite.getParentFile().exists())	{
			fileWrite.getParentFile().mkdirs();
		}
		FileInputStream fis = null;
		FileOutputStream fos = null;
		FileChannel fcIn = null, fcOut = null;
		try {
			fis = new FileInputStream(fileRead);
			fos = new FileOutputStream(fileWrite);
			fcIn = fis.getChannel();
			fcOut = fos.getChannel();
			fcIn.transferTo(0, fcIn.size(), fcOut);
		} finally {
			Util.closeStream(fcIn, fcOut);
		}
	}
	
}
