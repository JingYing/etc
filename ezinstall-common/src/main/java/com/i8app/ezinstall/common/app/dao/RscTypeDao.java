package com.i8app.ezinstall.common.app.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.domain.RscType;

@Component
public class RscTypeDao extends BaseDao{

	public List<RscType> getChildren(String parentId) {
		if(parentId == null)	{
			return sqlMapClientTemplate.queryForList("RscType.getRoot");
		} else	{
			return sqlMapClientTemplate.queryForList("RscType.getChildren", parentId);
		}
	}

}
