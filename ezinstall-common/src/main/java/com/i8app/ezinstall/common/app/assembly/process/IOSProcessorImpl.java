package com.i8app.ezinstall.common.app.assembly.process;

import static com.i8app.ezinstall.common.app.Constants.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.AppService;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppMeta;
import com.i8app.ezinstall.common.app.assembly.AssemblyReport;
import com.i8app.ezinstall.common.app.dao.AppInstallPackDao;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.packreader.PackReadException;
import com.i8app.ezinstall.packreader.ipa.ItunesMetadata;

@Component("iOSProcessorImpl")
public class IOSProcessorImpl implements OsProcessor {
	
	private static Logger log = Logger.getLogger(IOSProcessorImpl.class);
	
	/**
	 * APP表的name,icon的更新策略
	 * 取值为1时, 跟随CP商的名称变化. 取值为2时, 跟随安装包的名称变化
	 */
	public static int STRATEGY_CHOOSENAME = 2;

	@Resource
	protected AppInstallPackDao packDao;
	@Resource
	private AppService appService;

	@Override
	public AssemblyReport process(AppMeta appMeta, AppAdaptee adaptee) {
		AssemblyReport rpt = new AssemblyReport();
		rpt.setCpId(appMeta.getCpId());
		rpt.setOriginalPackId(appMeta.getOriginalPackId());
		rpt.setOs(appMeta.getOs());
		rpt.setAssemblyStatus(ERRCODE_NORMAL);
		
		AppInstallPack existPack = packDao.find(appMeta.getCpId(), appMeta.getOriginalPackId(), appMeta.getOs());
		//第一个关键流程节点: 当该安装包在数据库中已存在,且未更新时
		if(existPack != null 
				&& !adaptee.isPackUpdated(appMeta, existPack))	{	//如果pack没有更新且数据库中没有相关pack	
			this.whenNotUpdate(appMeta, existPack);
			return rpt;
		}
		
		try {
			//第二个关键流程节点:读取安装包信息,可能会抛出各种异常
			ItunesMetadata ios = (ItunesMetadata)adaptee.readPack(appMeta.getOriginalPackId(), appMeta.getOs());//抛出异常后,以下流程都不再执行
			//第三个关键流程节点:安装包是否在数据库中已存在
			if(existPack == null)	{	//没有对应的pack
				App existApp = findExistOrInsertNewApp(ios.getBundleId(), appMeta, ios);
				whenPackNotIn(existApp.getUuid(), appMeta, ios);
			} else	{//已有对应的pack
				//第四个关键流程节点:包名是否发生变化
				if(existPack.getIpaBundleId().equals(ios.getBundleId()))	{//包名没有变化
					whenPkgNameNotChange(existPack, appMeta, ios);
				} else	{	//包名发生变化了
					App existApp = findExistOrInsertNewApp(ios.getBundleId(), appMeta, ios);
					whenPkgNameChanged(existApp.getUuid(), existPack, appMeta, ios);
				}
			}
		} catch (CpServerAccessException e) {
			log.error(String.format("访问cp商服务器失败,id:%s, 信息:%s", appMeta.getOriginalPackId(), e.getMessage()), e);
			rpt.setAssemblyStatus(ERRCODE_DOWNLOAD_FAIL);
		} catch (FileNotFoundException e) {
			log.error(String.format("磁盘文件没找到,id:%s, 信息:%s", appMeta.getOriginalPackId(), e.getMessage()), e);
			rpt.setAssemblyStatus(ERRCODE_NO_PACK_ON_DISK);
		} catch (PackReadException e) {
			log.error(String.format("安装包解析失败,id:%s, 信息:%s", appMeta.getOriginalPackId(), e.getMessage()), e);
			rpt.setAssemblyStatus(ERRCODE_PACKREAD_FAIL);
		}
		return rpt;
	}
	
	/**
	 * 版本没有更新时
	 * @param appMeta
	 * @param existPack
	 */
	private void whenNotUpdate(AppMeta appMeta, AppInstallPack existPack) {
		App a = appService.find(appMeta.getCpId(), appMeta.getOriginalAppId());
		if(a != null)	{
			a.setDeveloper(appMeta.getDeveloper());
			a.setInfo(appMeta.getInfo());
			if(STRATEGY_CHOOSENAME == 1)	{
				a.setName(appMeta.getOriginalName());	//使用安装包内置名称
			}
			a.setPinyin(Util.getPinyinInitial(a.getName()));
			a.setRscTypeId(appMeta.getRscTypeId());
			a.setUpdateTime(new Date());
			
			//存icon
			if(a.getIcon() == null && appMeta.getIconUri() != null)	{
				try {
					InputStream fromCp = appMeta.getIconUri().toURL().openStream();
					String iconPath = appService.saveToIconDir(fromCp, ".jpg");
					a.setIcon(iconPath);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			//存banner
			if(a.getBanner() == null && appMeta.getBannerUri() != null)	{
				try {
					a.setBanner(appService.saveToPicDir(appMeta.getBannerUri().toURL().openStream(), ".jpg"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			//存截图
			if(a.getPic1() == null)	{
				int index = 1;
				for(URI uri : appMeta.getScreenshotsUri())	{
					try {
						String halfPath = appService.saveToPicDir(uri.toURL().openStream(), ".jpg");	//统一jpg
						a.getClass().getMethod("setPic" + index, String.class).invoke(a, halfPath);
						index ++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			appService.persist(a);
		}
		
		existPack.setStatus(appMeta.getStatus());
		existPack.setUpdateTime(new Date());
		packDao.update(existPack);
	}

	/**
	 * 当安装包版本已更新,且包名发生变化
	 * @param existAppId
	 * @param existPack
	 * @param apkInfo
	 */
	private void whenPkgNameChanged(String existAppId, AppInstallPack existPack, AppMeta appMeta, ItunesMetadata ios) {
		existPack.setIpaBundleId(ios.getBundleId());
		existPack.setVersion(ios.getBundleVersion() != null ? ios.getBundleVersion() : appMeta.getOriginalVersion());
		existPack.setIpaVersionCode(ios.getSoftwareVersionExternalIdentifier());
		if(existPack.getIpaVersionCode() != null)	{
			existPack.setAppVerValue(new Long(existPack.getIpaVersionCode()));	//使用ipaVersionCode
		}
		existPack.setIpaItemId(ios.getItemId());
		existPack.setOsMinVer(ios.getMinOsVersion() != null ? ios.getMinOsVersion() : appMeta.getOsMinVer());
		existPack.setOsMinVerValue(toOsMinVerValue(ios.getMinOsVersion()));
		
		existPack.setAppVer(appMeta.getOriginalVersion());
		existPack.setAppId(existAppId);
		existPack.setCpUpdateTime(appMeta.getCpUpdateTime());
		existPack.setFileSize(new Float(ios.getFileLength()/1024));
		existPack.setOriginalName(appMeta.getOriginalName());
		existPack.setUpdateTime(new Date());
		existPack.setStatus(appMeta.getStatus());
		existPack.setChangeLog(appMeta.getChangeLog());
		existPack.setUpdateTime(new Date());
		packDao.update(existPack);
	}

	/**
	 * 当包名已变化 
	 * @param existPack
	 * @param appMeta
	 * @param ios
	 */
	private void whenPkgNameNotChange(AppInstallPack existPack, AppMeta appMeta, ItunesMetadata ios) {
		existPack.setAppVer(appMeta.getOriginalVersion());//使用原始version
		existPack.setVersion(ios.getBundleVersion());
		existPack.setIpaVersionCode(ios.getSoftwareVersionExternalIdentifier());
		if(existPack.getIpaVersionCode() != null)	{
			existPack.setAppVerValue(new Long(existPack.getIpaVersionCode()));	//使用ipaVersionCode
		}
		existPack.setFileSize(new Float(ios.getFileLength()/1024));
		existPack.setStatus(appMeta.getStatus());
		existPack.setChangeLog(appMeta.getChangeLog());
		existPack.setUpdateTime(new Date());
		packDao.update(existPack);	
	}

	/**
	 * 安装包第一次入库
	 * @param existAppId
	 * @param appMeta
	 * @param apkInfo
	 */
	private void whenPackNotIn(String existAppId, AppMeta appMeta, ItunesMetadata ios) {
		AppInstallPack p = new AppInstallPack();
		p.setAppleId(ios.getAppleId());
		
		p.setIpaBundleId(ios.getBundleId());
		p.setIpaVersionCode(ios.getSoftwareVersionExternalIdentifier());
		if(p.getIpaVersionCode() != null)	{
			p.setAppVerValue(new Long(p.getIpaVersionCode()));	//使用ipaVersionCode
		}
		p.setVersion(ios.chooseVersion() != null ? ios.chooseVersion() : appMeta.getOriginalVersion());
		p.setOsMinVer(ios.getMinOsVersion() != null ? ios.getMinOsVersion() : appMeta.getOsMinVer());
		if(p.getOsMinVer() != null)	{
			p.setOsMinVerValue(toOsMinVerValue(p.getOsMinVer()));
		}
		p.setIpaItemId(ios.getItemId());
		
		p.setAppId(existAppId);
		p.setAppVer(appMeta.getOriginalVersion());
		p.setCpId(appMeta.getCpId());
		p.setCpUpdateTime(appMeta.getCpUpdateTime());
		p.setFileSize(new Float(ios.getFileLength()/1024));
		p.setOriginalName(appMeta.getOriginalName());
		p.setOriginalPackId(appMeta.getOriginalPackId());
		p.setOs(appMeta.getOs());
		p.setStatus(appMeta.getStatus());
		p.setUpdateTime(new Date());
		p.setChangeLog(appMeta.getChangeLog());
		p.setUuid(UUID.randomUUID().toString());
		p.setUpdateTime(new Date());
		packDao.insert(p);
		
	}

	/**
	 * 查找已有的APP, 查不到就新增一个
	 * @param bundleId
	 * @param appMeta
	 * @param ios
	 * @return
	 */
	private App findExistOrInsertNewApp(String bundleId, AppMeta appMeta, ItunesMetadata ios) {
		List<AppInstallPack> packs = packDao.findByIpaBundleId(bundleId);
		if(packs.isEmpty())	{
			return insertNewApp(appMeta, ios);
		} else	{
			for(AppInstallPack p : packs)	{
				if(p.getApp() == null)	{
					log.error("appInstallPack数据出现异常,pack.appid在app表中不存在,需要手动清理.pack.uuid=" + p.getUuid());
				} else	{
					return p.getApp();	//取有app的一条记录
				}
			}
			
			//如果所有相关pack都没有对应的app, 则抛出异常停止运行
			throw new RuntimeException("appInstallPack数据出现异常,pack.appid在app表中不存在,需要手动清理.pack.uuid=" + packs.get(0).getUuid());
		}
	}

	/**
	 * 新增一个app
	 * @param appMeta
	 * @param apkInfo
	 * @return
	 */
	private App insertNewApp(AppMeta appMeta, ItunesMetadata ios) {
		App a = new App();
		a.setCpId(appMeta.getCpId());
		if(ios.getArtistName() != null)	{
			a.setDeveloper(ios.getArtistName());
		} else	{
			a.setDeveloper(appMeta.getDeveloper());
		}
		a.setInfo(appMeta.getInfo());
		a.setName(this.chooseName(appMeta.getOriginalName(), ios.chooseName()));
		a.setPinyin(Util.getPinyinInitial(a.getName()));
		a.setOriginalAppId(appMeta.getOriginalAppId());
		a.setOriginalTypeId(appMeta.getOriginalTypeId());
		
		//存icon
		try {
			String iconPath = appService.saveToIconDir(this.chooseIcon(appMeta.getIconUri(), ios.getIconUrl()), ".jpg");
			a.setIcon(iconPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//存截图
		int index = 1;
		for(URI uri : appMeta.getScreenshotsUri())	{
			try {
				String halfPath = appService.saveToPicDir(uri.toURL().openStream(), ".jpg");	//统一jpg
				a.getClass().getMethod("setPic" + index, String.class).invoke(a, halfPath);
			} catch (Exception e) {
				e.printStackTrace();
			}
			index ++;
		}
		a.setRscTypeId(appMeta.getRscTypeId());
		
		//以下为appInstallpack的冗余字段,可以不设值, 最后统一更新冗余字段
		a.setBundleId(ios.getBundleId());
		a.setOs(appMeta.getOs());
		a.setIpaItemId(null);	//ios专用字段,无需处理
		a.setFileSize(new Long(ios.getFileLength()).floatValue());
		
		//以下给默认值
		a.setPoint(null);	
		a.setIsDel(0);
		a.setInstallCount(0);
		a.setDownCount(0);
		a.setIsAreaCustom(0);
		a.setUpdateTime(new Date());
		a.setUuid(UUID.randomUUID().toString());
		appService.persist(a);
		return a;
	}

	/**
	 * 转换osMinverValue. 不识别时转换成0
	 * @param minOsVer
	 * @return
	 */
	private long toOsMinVerValue(String minOsVer)	{
		try {
			int num = minOsVer.split("[.]").length;
			for (int i = 0; i < 5 - num; i++) {
				minOsVer += ".0";
			}
			String[] strings = minOsVer.split("[.]");
			int sum = 0;
			for (int i = 0, j = strings.length - 1; i < strings.length; i++, j--) {
				sum += Integer.parseInt(strings[i]) * Math.pow(50, j);
			}
			return sum + 400000000;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * 选择名称
	 * @param nameFromCp
	 * @param nameFromPack
	 * @return
	 */
	private String chooseName(String nameFromCp, String nameFromPack)	{
		if(nameFromCp == null)		return nameFromPack;
		if(nameFromPack == null)	return nameFromCp;
		
		if(STRATEGY_CHOOSENAME == 1)	{
			return nameFromCp;
		} else if(STRATEGY_CHOOSENAME == 2)	{
			return nameFromPack;
		} else	{
			throw new IllegalArgumentException("参数有误:" + STRATEGY_CHOOSENAME);
		}
	}
	
	/**
	 * 选择ICON, 除非其中有的为空
	 * @param iconFromCp
	 * @param iconFromPack
	 * @return
	 */
	private InputStream chooseIcon(URI iconFromCp, String iconFromPack)	{
		if(iconFromCp == null)	{
			try {
				return new URL(iconFromPack).openStream();
			} catch (IOException e1) {
				return null;
			}
		}
		if(iconFromPack == null)	{
			try {
				return iconFromCp.toURL().openStream();
			} catch (IOException e1) {
				return null;
			}
		}
		
		if(STRATEGY_CHOOSENAME == 1)	{
			try {
				return iconFromCp.toURL().openStream();
			} catch (IOException e) {
				return null;
			}
		} else if(STRATEGY_CHOOSENAME == 2)	{
			try {
				return new URL(iconFromPack).openStream();
			} catch (IOException e) {
				return null;
			}
		} else	{
			throw new IllegalArgumentException("参数有误:" + STRATEGY_CHOOSENAME);
		}
	}

}
