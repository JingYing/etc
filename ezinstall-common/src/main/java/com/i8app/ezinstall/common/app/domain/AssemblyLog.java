package com.i8app.ezinstall.common.app.domain;

import java.util.Date;

public class AssemblyLog {
	private Integer id;
	private Date operTime;
	private String cpId, originalPackId, os;
	private int status;
	
	public AssemblyLog()	{}
	
	public AssemblyLog(Date operTime, String cpId, String originalPackId,
			String os, int status) {
		super();
		this.operTime = operTime;
		this.cpId = cpId;
		this.originalPackId = originalPackId;
		this.os = os;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getOperTime() {
		return operTime;
	}

	public void setOperTime(Date operTime) {
		this.operTime = operTime;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getOriginalPackId() {
		return originalPackId;
	}

	public void setOriginalPackId(String originalPackId) {
		this.originalPackId = originalPackId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
