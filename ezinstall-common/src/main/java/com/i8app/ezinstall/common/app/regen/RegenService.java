package com.i8app.ezinstall.common.app.regen;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.AppService;
import com.i8app.ezinstall.common.app.assembly.AppAssemblier;
import com.i8app.ezinstall.common.app.dao.AppAreaDao;
import com.i8app.ezinstall.common.app.dao.AppCommentDao;
import com.i8app.ezinstall.common.app.dao.AppDao;
import com.i8app.ezinstall.common.app.dao.AppInstallPackDao;
import com.i8app.ezinstall.common.app.dao.BaseDao;
import com.i8app.ezinstall.common.app.dao.PkgInfoDao;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.query.CpQueryFactory;
import com.i8app.ezinstall.common.app.query.CpQuery;

/**
 * 如果app重新生成,UUID发生变化,导致pkg_info表中所有appid失效, 
 * 使用此服务配合app_new,app_install_pack_new表进行修复
 * 
 * @author JingYing 2013-8-15
 */
@Component
public class RegenService extends BaseDao {
	private static Logger logger = Logger.getLogger(RegenService.class);
	@Resource
	private AppAssemblier appAssembiler;
	@Resource
	private PkgInfoDao pkgInfoDao;
	@Resource
	private AppInstallPackDao appInstallPackDao;
	@Resource
	private AppDao appDao;
	@Resource
	private AppAreaDao appAreaDao;
	@Resource
	private AppCommentDao appCommentDao;
	@Resource
	private AppService appService;
	@Resource
	private CpQueryFactory cpQueryFactory;
	/**
	 * 当app,app_install_pack重新生成后, 更新pkg_info,app_area,app_comment三张表的APPID.
	 */
	public void afterRegen()	{
		logger.error("开始根据app_install_pack_old, app_old, app_install_pack, app四张表更新pkg_info, app_area,app_comment");
		updatePkgInfo();
		updateAppArea();
		updateAppComment();
		renameTable();
		appAssembiler.fixMissingIconAndPic();
		logger.error("更新APPID完成");
	}
	
	public void updatePkgInfo()	{
		List<Map> list = pkgInfoDao.findOldPack();
		for(Map map : list)	{
			AppInstallPack pack = appInstallPackDao.findInNewTable((String)map.get("cpId"), (String)map.get("originalPackId"), (String)map.get("os"));
			if(pack != null)	{
				pkgInfoDao.updateAppId((Integer)map.get("id"), pack.getUuid());
			}
		}
		
		List<Map> list1 = pkgInfoDao.findOldApp();
		for(Map map : list1)	{
			App a = appDao.findInNewTable((String)map.get("cpId"), (String)map.get("originalAppId"));
			if(a != null)	{
				pkgInfoDao.updateAppId((Integer)map.get("id"), a.getUuid());
			}
		}
	}
	
	public void updateAppArea()	{
		List list = appAreaDao.findOldApp();
		for(Object o : list)	{
			Map map = (Map)o;
			App a = appDao.findInNewTable((String)map.get("cpId"), (String)map.get("originalAppId"));
			if(a != null)	{
				appAreaDao.updateAppId(map.get("id"), a.getUuid());
			}
		}
	}
	
	public void updateAppComment()	{
		List list = appCommentDao.findOldApp();
		for(Object o : list)	{
			Map map = (Map)o;
			App a = appDao.findInNewTable((String)map.get("cpId"), (String)map.get("originalAppId"));
			if(a != null)	{
				appCommentDao.updateAppId(map.get("id"), a.getUuid());
			}
		}
	}
	
	public void renameTable()	{
		jdbcTemplate.update("ALTER TABLE `app_install_pack` RENAME `app_install_pack_old`"); 
		jdbcTemplate.update("ALTER TABLE `app` RENAME `app_old`");
		jdbcTemplate.update("ALTER TABLE `app_new` RENAME `app`"); 
		jdbcTemplate.update("ALTER TABLE `app_install_pack_new` RENAME `app_install_pack`"); 
	}
	
	/**
	 * 2013-10-30,app表中新加了icon,pic1,pic2,pic3,pic4, os, fileSize, ipaItemId字段.
	 * 只补齐缺失PIC的数据. 不补ICON, 因为汇集程序自动补齐ICON
	 */
	public void fixPicAfterAddColumn()	{
		//jdbcTemplate.update("UPDATE APP A, PIC P SET A.ICON = P.URL WHERE A.ICONID = P.ID");
//		appDao.updateRedundantField();
//
//		int total = appDao.findMissingPic(0, 1).getTotalCount();
//		for(int offset=0; offset<=total; offset+=100)	{
//			List<App> list = appDao.findMissingPic(offset, 100).getList();
//			for(App app : list)	{
//				ICpQuery adaptee = cpQueryFactory.factory(app.getCpId());	//根据app.cpid决定去哪个表中查询图片
//				
//				List<String> pics = adaptee.findScreenshots(app.getOriginalAppId());
//				logger.info(String.format("正在下载图片...id:%s,名称:%s", app.getUuid(), app.getName()));
//				List<String> savedPaths = appService.savePicFromUrl(pics);	//下载截图
//				for(int i=0; i<savedPaths.size(); i++)	{
//					try {
//						app.getClass().getMethod("setPic" + (i+1), String.class)
//							.invoke(app, savedPaths.get(i));
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				jdbcTemplate.update("UPDATE APP SET PIC1 = ?, PIC2 = ?, PIC3 = ?, PIC4 = ? WHERE UUID = ?", 
//						app.getPic1(), app.getPic2(), app.getPic3(), app.getPic4(), app.getUuid());
//			}
//		}
	}
	
	
}
