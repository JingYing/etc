package com.i8app.ezinstall.common.app;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.ImgComparator;
import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.dao.I8appDao;
import com.i8app.ezinstall.common.app.domain.I8app;
import com.i8app.ezinstall.packreader.PackReadException;
import com.i8app.ezinstall.packreader.apk2.ApkInfo;
import com.i8app.ezinstall.packreader.apk2.ApkReader2;
import com.i8app.ezinstall.packreader.ipa.IpaReader;

@Component
public class I8appService {
	
	private static Logger logger = Logger.getLogger(I8appService.class);
	private static String FILESERVER_DIR = ClientParamHolder.getFileServerDir();
	
	@Resource
	private I8appDao i8appDao;
	private ApkReader2 apkReader2 = new ApkReader2();
	private IpaReader ipaReader = new IpaReader();
	
	public void insertIpa(File ipa)	{
		
	}
	
	/**
	 * 新增安装包.将apk复制入fileServer, 并删除tmpApk
	 * @param tmpApk 操作后会被删除
	 * @throws IOException 
	 * @throws PackReadException 
	 */
	public void insertApk(File tmpApk, I8appAcquire collector) throws IOException, PackReadException	{
		File savedFile = null;
		try {
			String savedPath = saveToFileDir(new FileInputStream(tmpApk), tmpApk.getName());
			savedFile = new File(FILESERVER_DIR + savedPath);
			ApkInfo apkInfo = apkReader2.read(savedFile);	//复制后再校验,防止复制出错
			int fileSize = new Float(savedFile.length()/1024.0).intValue();
			
			I8app i = new I8app();
			i.setPackUrl(savedPath);
			i.setFileSize(fileSize);
			i.setName(apkInfo.chooseChineseLabel());
			i.setVersion(apkInfo.getVersionName());
			i.setPkgName(apkInfo.getPackageName());
			i.setVersionCode(apkInfo.getVersionCode());
			byte[] iconInApk = apkReader2.readFile(savedFile, apkInfo.chooseLargestIcon());
			if(i.getIcon() == null || "".equals(i.getIcon()))	{	//如果以前没有icon, 直接更新
				String halfPath = saveToIconDir(new ByteArrayInputStream(iconInApk), apkInfo.chooseLargestIcon());
				i.setIcon(halfPath);
			}
			
			//存截图
			int index = 1;
			for(InputStream is : collector.getPics())	{
				String halfPath = this.saveToPicDir(is, ".jpg");	//统一jpg
				try {
					i.getClass().getMethod("setPic" + index, String.class).invoke(i, halfPath);
				} catch (Exception e) {
					e.printStackTrace();
				}
				index ++;
			}
			
			//下载banner
			try {
				if(collector.getBanner() != null)	{	//我们没有,且调用者传递了banner, 则更新
					String halfPath = saveToPicDir(collector.getBanner(), ".jpg");
					i.setBanner(halfPath);
				} 
			} catch (IOException e2) {
				e2.printStackTrace();
			}

			i.setDeveloper(collector.getDeveloper());
			i.setInfo(collector.getInfo());
			i.setOs(collector.getOs());
			i.setRscTypeId(collector.getRscTypeId());
			i.setIsAutoUpdate(collector.getIsAutoUpdate());
			
			i.setIsDel(0);
			i.setAddTime(new Date());
			i.setUpdateTime(new Date());	//使用系统时间做更新时间
			
			i8appDao.insert(i);
		} catch (IOException e) {
			if(savedFile != null && savedFile.isFile())
				savedFile.delete();
			throw e;
		} catch (PackReadException e) {
			if(savedFile != null && savedFile.isFile())
				savedFile.delete();
			throw e;
		} finally	{
			if(tmpApk.isFile())	tmpApk.delete();	//成功或出现任何异常,都要删除tmpApk
		}
	}
	
	/**
	 * 更新安装包
	 * @param tmpApk
	 * @param i8appId
	 * @param collector 只处理以下信息:info,developer,changelog,banner
	 * @throws PackReadException 无法解析pack
	 * @throws IOException 将pack存入fileserver时IO异常
	 */
	public void updateApk(File tmpApk, int i8appId, I8appAcquire collector) throws IOException, PackReadException 	{
		File savedFile = null;
		try {
			String savedPath = saveToFileDir(new FileInputStream(tmpApk), Util.getSuffix(tmpApk.getName()));
			savedFile = new File(FILESERVER_DIR + savedPath);
			ApkInfo apkInfo = apkReader2.read(savedFile);	//复制后进行安装包校验
			I8app exist = i8appDao.findById(i8appId);
			
			if(apkInfo.getPackageName().equals(exist.getPkgName()) 
					&& apkInfo.getVersionCode() > exist.getVersionCode())	{	//防止传入的包版本或包名不对
				
				String oldPack = exist.getPackUrl();
				
				exist.setFileSize(new Double(savedFile.length()/1024.0).intValue());
				exist.setName(apkInfo.chooseChineseLabel());	//强制更名
				exist.setPackUrl(savedPath);
				exist.setVersion(apkInfo.getVersionName());
				exist.setVersionCode(apkInfo.getVersionCode());
				
				exist.setInfo(collector.getInfo());
				exist.setChangeLog(collector.getChangeLog());
				exist.setDeveloper(collector.getDeveloper());
				exist.setUpdateTime(new Date());
				
				// 获取包的里小图标, 和本地图片对比, 有变化则更新
				try {
					byte[] iconInApk = apkReader2.readFile(savedFile, apkInfo.chooseLargestIcon());
					if(exist.getIcon() == null || "".equals(exist.getIcon()))	{	//如果以前没有icon, 直接更新
						String halfPath = saveToIconDir(new ByteArrayInputStream(iconInApk), apkInfo.chooseLargestIcon());
						exist.setIcon(halfPath);
					} else	{	//如果以前有icon, 则对比更新
						File existedIcon = new File(FILESERVER_DIR + exist.getIcon());
						int diff = new ImgComparator().compare(iconInApk, existedIcon);
						if(diff > 5)	{	//差别较大, 需要更新
							String halfPath = saveToIconDir(new ByteArrayInputStream(iconInApk), apkInfo.chooseLargestIcon());
							exist.setIcon(halfPath);
							if(existedIcon.isFile())
								existedIcon.delete();
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				//处理banner. 如果没有则下载banner. 如果有, 不对比更新
				try {
					File existBanner = new File(FILESERVER_DIR + exist.getBanner());
					if(!existBanner.exists() && collector.getBanner() != null)	{	//我们没有,且调用者传递了banner, 则更新
						String halfPath = saveToPicDir(collector.getBanner(), ".jpg");
						exist.setBanner(halfPath);
					} 
				} catch (IOException e2) {
					e2.printStackTrace();
				}
				
				i8appDao.update(exist);
				
				File oldFile = new File(FILESERVER_DIR + oldPack);
				if(oldFile.isFile())	oldFile.delete();	//防止数据库数据异常时错删目录
			} else	{
				logger.warn(String.format("%s的包名或版本与数据库已有记录不符合\n.传入安装包:%s.版本%s.\n已有记录:%s.版本:%s", 
						savedFile, apkInfo.getPackageName(), apkInfo.getVersionCode(), exist.getPkgName(), exist.getVersionCode()
				));
				if(savedFile.isFile())	
					savedFile.delete();
			}
		} catch (IOException e) {	//异常时删除已复制文件
			if(savedFile != null && savedFile.isFile())
				savedFile.delete();
			throw e;
		} catch (PackReadException e) {
			if(savedFile != null && savedFile.isFile())
				savedFile.delete();
			throw e;
		} finally	{
			if(tmpApk.isFile())	tmpApk.delete();	//成功或出现任何异常,都要删除tmpApk
		}
	}
	
	/**
	 * 按照时间规则, 将文件保存到icon目录下
	 * @param is 用后会关闭
	 * @param suffix 后缀名, 例:".apk", ".ipa"
	 * @return
	 * @throws IOException
	 */
	public String saveToIconDir(InputStream is, String suffix) throws IOException	{
		return saveFile(is, Util.getSuffix(suffix), "icon");
	}
	
	/**
	 * 按照时间规则, 将文件保存到pic目录下
	 * @param is 用后会关闭
	 * @param suffix 后缀名, 例:".apk", ".ipa"
	 * @return
	 * @throws IOException
	 */
	public String saveToPicDir(InputStream is, String suffix) throws IOException	{
		return saveFile(is, Util.getSuffix(suffix), "pic");
	}
	
	/**
	 * 按照时间规则, 将文件保存到file目录下
	 * @param is 用后会关闭
	 * @param suffix 后缀名, 例:".apk", ".ipa"
	 * @return
	 * @throws IOException
	 */
	public String saveToFileDir(InputStream is, String suffix) throws IOException	{
		return saveFile(is, Util.getSuffix(suffix), "file");
	}
	
	/**
	 * 保存文件至fileServer/i8app目录
	 * @param is
	 * @param fileSuffix 后缀名 .ipa, .png, .jpg...
	 * @param dir icon/file/pic
	 * @return 半截地址 	比如i8app/icon/xxxx/xxx.jpg
	 * @throws IOException
	 */
	private String saveFile(InputStream is, String fileSuffix, String dir) throws IOException	{
		String path2 = "i8app/" + dir + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "/";
		String fileName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + fileSuffix.toLowerCase();
		File target = new File(FILESERVER_DIR + path2 + fileName);
		if(!target.getParentFile().exists())
			target.getParentFile().mkdirs();

		ReadableByteChannel inChannel = Channels.newChannel(is);
		FileChannel outChannel = null;
		try {
			outChannel = new FileOutputStream(target).getChannel(); 
			ByteBuffer buf = ByteBuffer.allocate(1024);
			while(true)	{
				int len = inChannel.read(buf);
				if(len == -1)	break;
				buf.flip();
				outChannel.write(buf);
				buf.clear();
			}
		} finally	{
			Util.closeStream(inChannel, outChannel);
		}
		return path2 + fileName;
	}
	
}
