package com.i8app.ezinstall.common.app.domain;

public class Axon {
	private String contentId, contentName, version, size, suitableType, typeId,
			typeName, createTime, updateTime, downloadNum, providerTags,
			description, providerName, platform, price, free, language,
			developers, fileId, format, logosUrl, screenshots1, screenshots2,
			screenshots3, screenshots4, status;
	private int isDel, isInvalid, priority;
	
	
	public int getIsDel() {
		return isDel;
	}

	public void setIsDel(int isDel) {
		this.isDel = isDel;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getContentName() {
		return contentName;
	}

	public void setContentName(String contentName) {
		this.contentName = contentName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSuitableType() {
		return suitableType;
	}

	public void setSuitableType(String suitableType) {
		this.suitableType = suitableType;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getDownloadNum() {
		return downloadNum;
	}

	public void setDownloadNum(String downloadNum) {
		this.downloadNum = downloadNum;
	}

	public String getProviderTags() {
		return providerTags;
	}

	public void setProviderTags(String providerTags) {
		this.providerTags = providerTags;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFree() {
		return free;
	}

	public void setFree(String free) {
		this.free = free;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDevelopers() {
		return developers;
	}

	public void setDevelopers(String developers) {
		this.developers = developers;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getLogosUrl() {
		return logosUrl;
	}

	public void setLogosUrl(String logosUrl) {
		this.logosUrl = logosUrl;
	}

	public String getScreenshots1() {
		return screenshots1;
	}

	public void setScreenshots1(String screenshots1) {
		this.screenshots1 = screenshots1;
	}

	public String getScreenshots2() {
		return screenshots2;
	}

	public void setScreenshots2(String screenshots2) {
		this.screenshots2 = screenshots2;
	}

	public String getScreenshots3() {
		return screenshots3;
	}

	public void setScreenshots3(String screenshots3) {
		this.screenshots3 = screenshots3;
	}

	public String getScreenshots4() {
		return screenshots4;
	}

	public void setScreenshots4(String screenshots4) {
		this.screenshots4 = screenshots4;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getIsInvalid() {
		return isInvalid;
	}

	public void setIsInvalid(int isInvalid) {
		this.isInvalid = isInvalid;
	}
}
