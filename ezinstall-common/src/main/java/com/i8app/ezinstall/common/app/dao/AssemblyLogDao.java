package com.i8app.ezinstall.common.app.dao;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.domain.AssemblyLog;

@Component
public class AssemblyLogDao extends BaseDao	{

	public void insert(AssemblyLog log)	{
		sqlMapClientTemplate.insert("AssemblyLog.insert", log);
	}
}
