package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.I8Soft;

@Component
public class I8SoftDao extends BaseDao	{

	public Pager<I8Soft> find(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<I8Soft> list = sqlMapClientTemplate.queryForList("I8Soft.findAll", map);
		int total = (Integer)sqlMapClientTemplate.queryForObject("I8Soft.findAll-count");
		return new Pager<I8Soft>(list, total);
	}

	public I8Soft findById(int id) {
		return (I8Soft)sqlMapClientTemplate.queryForObject("I8Soft.byId", id);
	}

	public List findByIds(List<String> originalPackIds) {
		Map map = new HashMap();
		map.put("ids", originalPackIds);
		return sqlMapClientTemplate.queryForList("I8Soft.byIds", map);
	}

	public List<I8Soft> findByOs(int osType) {
		return sqlMapClientTemplate.queryForList("I8Soft.byOs", osType);
	}
	
}
