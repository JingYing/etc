package com.i8app.ezinstall.common.app.query;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.dao.I8SoftDao;
import com.i8app.ezinstall.common.app.domain.I8Soft;

@Component
public class I8SoftCpQueryImpl implements Constants, CpQuery	{
	private static final Logger logger = Logger.getLogger(I8SoftCpQueryImpl.class);
	
	public static final String 
		FILESERVER_URL 			= ClientParamHolder.getFileServerUrl(),
		FILESERVER_DIR 			= ClientParamHolder.getFileServerDir(); 
	
	@Resource
	private I8SoftDao i8SoftDao;

	@Override
	public String findPackUrl(String originalPackId, String os) throws CpServerAccessException {
		I8Soft s = i8SoftDao.findById(Integer.parseInt(originalPackId));	//s为空时需要向外抛异常
		if(s == null)	throw new RuntimeException("missing soft id:" + originalPackId);
		String halfPath = IOS.equals(os) ? s.getLegalFileName() : s.getFile();
		if(halfPath != null && !"".equals(halfPath))	{
			return FILESERVER_URL + halfPath;
		} else	{
			return null;
		}
	}

	@Override
	public List<String> findScreenshots(String orginalAppId ) {
		I8Soft s = i8SoftDao.findById(Integer.parseInt(orginalAppId));
		if(s == null){
//			throw new RuntimeException("missing soft id:" + orginalAppId);
			logger.error("missing soft id:" + orginalAppId);
		}
		List<String> list = new ArrayList<String>();
		if(s != null)	{
			String s1 = s.getPic1();
			if(s1 != null && s1.length()>0)
				list.add(FILESERVER_URL + s1);
	
			String s2 = s.getPic2();
			if(s2 != null && s2.length()>0)
				list.add(FILESERVER_URL + s2);
			
			String s3 = s.getPic3();
			if(s3 != null && s3.length()>0)
				list.add(FILESERVER_URL + s3);
		}
		return list;
	}

	@Override
	public String findIconUrl(String originalAppId) {
		I8Soft s = i8SoftDao.findById(Integer.parseInt(originalAppId));
		if(s != null && s.getIcon() != null && s.getIcon().length() > 0)	{
			return FILESERVER_URL + s.getIcon(); 
		} 
		return null;
	}

	@Override
	public List findByOriginalPackIds(List<String> originalPackIds) {
		return i8SoftDao.findByIds(originalPackIds);
	}

}
