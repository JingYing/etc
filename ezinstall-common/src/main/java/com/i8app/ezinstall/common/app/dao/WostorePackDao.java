package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.Wostore;
import com.i8app.ezinstall.common.app.domain.WostorePack;

@Component
public class WostorePackDao extends BaseDao	{

	public List<WostorePack> findByAppId(String appId) {
		return sqlMapClientTemplate.queryForList("WostorePack.byAppId", appId);
	}

	public WostorePack findById(String id) {
		return (WostorePack)sqlMapClientTemplate.queryForObject("WostorePack.byId", id);
	}

	public List findByIds(List<String> originalPackIds) {
		Map map = new HashMap();
		map.put("ids", originalPackIds);
		return sqlMapClientTemplate.queryForList("WostorePack.byIds", map);
	}

	public List<WostorePack> findDeletedApp() {
		return sqlMapClientTemplate.queryForList("WostorePack.findDeletedApp");
	}

	public Pager<WostorePack> findValidApp(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<WostorePack> list = sqlMapClientTemplate.queryForList("WostorePack.findValidApp", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("WostorePack.findValidApp-count");
		return new Pager<WostorePack>(list, count);
	}

}
