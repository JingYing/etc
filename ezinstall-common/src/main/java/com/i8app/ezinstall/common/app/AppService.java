package com.i8app.ezinstall.common.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.dao.AppDao;
import com.i8app.ezinstall.common.app.domain.App;

@Component
public class AppService {
	
	private static Logger logger = Logger.getLogger(AppService.class);
	private static String FILESERVER_DIR = ClientParamHolder.getFileServerDir();
	
	@Resource
	private AppDao appDao;
	
	public App find(String cpId, String originalAppId)	{
		return appDao.find(cpId, originalAppId);
	}
	
	public App findById(String id) {
		return appDao.findById(id);
	}
	
	public void persist(App app)	{
		appDao.persist(app);
	}
	
	public void updateRedundantField()	{
		appDao.updateRedundantField();
	}
	
	public List<App> findMissingIcon()	{
		return appDao.findMissingIcon();
	}

	public List<App> findMissingPic()	{
		return appDao.findMissingPic();
	}
	
	/**
	 * 按照时间规则, 将文件保存到icon目录下
	 * @param is 用后会关闭
	 * @param suffix 后缀名, 例:".apk", ".ipa"
	 * @return
	 * @throws IOException
	 */
	public String saveToIconDir(InputStream is, String suffix) throws IOException	{
		return saveFile(is, Util.getSuffix(suffix), "icon");
	}
	
	/**
	 * 按照时间规则, 将文件保存到pic目录下
	 * @param is 用后会关闭
	 * @param suffix 后缀名, 例:".apk", ".ipa"
	 * @return
	 * @throws IOException
	 */
	public String saveToPicDir(InputStream is, String suffix) throws IOException	{
		return saveFile(is, Util.getSuffix(suffix), "pic");
	}
	
	/**
	 * 保存文件至fileServer/i8app目录
	 * @param is
	 * @param fileSuffix 后缀名 .ipa, .png, .jpg...
	 * @param dir icon/file/pic
	 * @return 半截地址 	比如i8app/icon/xxxx/xxx.jpg
	 * @throws IOException
	 */
	private String saveFile(InputStream is, String fileSuffix, String dir) throws IOException	{
		String path2 = "app/" + dir + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "/";
		String fileName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + fileSuffix.toLowerCase();
		File target = new File(FILESERVER_DIR + path2 + fileName);
		if(!target.getParentFile().exists())
			target.getParentFile().mkdirs();

		ReadableByteChannel inChannel = Channels.newChannel(is);
		FileChannel outChannel = null;
		try {
			outChannel = new FileOutputStream(target).getChannel(); 
			ByteBuffer buf = ByteBuffer.allocate(1024);
			while(true)	{
				int len = inChannel.read(buf);
				if(len == -1)	break;
				buf.flip();
				outChannel.write(buf);
				buf.clear();
			}
		} finally	{
			Util.closeStream(inChannel, outChannel);
		}
		return path2 + fileName;
	}

}
