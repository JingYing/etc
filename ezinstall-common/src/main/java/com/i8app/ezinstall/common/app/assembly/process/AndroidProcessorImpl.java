package com.i8app.ezinstall.common.app.assembly.process;

import static com.i8app.ezinstall.common.app.Constants.*;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.AppService;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppMeta;
import com.i8app.ezinstall.common.app.assembly.AssemblyReport;
import com.i8app.ezinstall.common.app.dao.AppInstallPackDao;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.wsclient.WdjConnection;
import com.i8app.ezinstall.common.wsclient.WdjJsonParser;
import com.i8app.ezinstall.packreader.PackReadException;
import com.i8app.ezinstall.packreader.apk2.ApkInfo;


/**
 * 定义android流程
 * 
 * @author JingYing 2014-1-10
 */
@Component
public class AndroidProcessorImpl implements OsProcessor{
	private static Logger log = Logger.getLogger(AndroidProcessorImpl.class);
	
	/**
	 * APP表的name,icon的更新策略
	 * 取值为1时, 跟随CP商的名称变化. 取值为2时, 跟随安装包的名称变化
	 */
	public static int STRATEGY_CHOOSENAME = 2;
		 
	@Resource
	private AppService appService;
	@Resource
	private AppInstallPackDao packDao;
	
	/**
	 * 定义流程骨架, 返回报告. 由各个私有方法进行具体操作
	 */
	@Override
	public AssemblyReport process(AppMeta appMeta, AppAdaptee adaptee)	{
		AssemblyReport rpt = new AssemblyReport();
		rpt.setCpId(appMeta.getCpId());
		rpt.setOriginalPackId(appMeta.getOriginalPackId());
		rpt.setOs(appMeta.getOs());
		rpt.setAssemblyStatus(ERRCODE_NORMAL);
		
		AppInstallPack existPack = packDao.find(appMeta.getCpId(), appMeta.getOriginalPackId(), appMeta.getOs());
		//第一个关键流程节点: 当该安装包在数据库中已存在,且未更新时
		if(existPack != null 
				&& !adaptee.isPackUpdated(appMeta, existPack))	{	//如果pack没有更新且数据库中没有相关pack	
			this.whenNotUpdate(appMeta, existPack);
			return rpt;
		}
		
		try {
			//第二个关键流程节点:读取安装包信息,可能会抛出各种异常
			ApkInfo apkInfo = (ApkInfo)adaptee.readPack(appMeta.getOriginalPackId(), appMeta.getOs());//抛出异常后,以下流程都不再执行
			//第三个关键流程节点:安装包是否在数据库中已存在
			if(existPack == null)	{	//没有对应的pack
				App existApp = findExistOrInsertNewApp(apkInfo.getPackageName(), appMeta, apkInfo);
				whenPackNotIn(existApp.getUuid(), appMeta, apkInfo);
			} else	{//已有对应的pack
				//第四个关键流程节点:包名是否发生变化
				if(existPack.getApkPkgName().equals(apkInfo.getPackageName()))	{//包名没有变化
					whenPkgNameNotChange(existPack, appMeta, apkInfo);
				} else	{	//包名发生变化了
					App existApp = findExistOrInsertNewApp(apkInfo.getPackageName(), appMeta, apkInfo);
					whenPkgNameChanged(existApp.getUuid(), existPack, appMeta, apkInfo);
				}
			}
		} catch (CpServerAccessException e) {
			log.error(String.format("访问cp商服务器失败,id:%s, 信息:%s", appMeta.getOriginalPackId(), e.getMessage()), e);
			rpt.setAssemblyStatus(ERRCODE_DOWNLOAD_FAIL);
		} catch (FileNotFoundException e) {
			log.error(String.format("磁盘文件没找到,id:%s, 信息:%s", appMeta.getOriginalPackId(), e.getMessage()));//不记录堆栈,避免因为i8app-apk的资源而生成大量日志
			rpt.setAssemblyStatus(ERRCODE_NO_PACK_ON_DISK);
		} catch (PackReadException e) {
			log.error(String.format("安装包解析失败,id:%s, 信息:%s", appMeta.getOriginalPackId(), e.getMessage()), e);
			rpt.setAssemblyStatus(ERRCODE_PACKREAD_FAIL);
		}
		return rpt;
	}
	
	/**
	 * 查询已有的app, 如果没有, 则新增一个
	 * @param apkPkgName
	 * @param appMeta
	 * @param apkInfo
	 * @return
	 */
	private App findExistOrInsertNewApp(String apkPkgName, AppMeta appMeta, ApkInfo apkInfo)	{
		List<AppInstallPack> packs = packDao.findByApkPkgName(apkPkgName);
		if(packs.isEmpty())	{
			return insertNewApp(appMeta, apkInfo);
		} else	{
			for(AppInstallPack p : packs)	{
				if(p.getApp() == null)	{
					log.error("appInstallPack数据出现异常,pack.appid在app表中不存在,需要手动清理.pack.uuid=" + p.getUuid());
				} else	{
					return p.getApp();	//取有app的一条记录
				}
			}
			
			//如果所有相关pack都没有对应的app, 则抛出异常停止运行
			throw new RuntimeException("appInstallPack数据出现异常,pack.appid在app表中不存在,需要手动清理.pack.uuid=" + packs.get(0).getUuid());
		}
	}
	
	/**
	 * 安装包已在数据库中存在, 且版本未更新.
	 * 不修改名称, 因为假设app名称在第一次入库时是从安装包读入的.
	 */
	private void whenNotUpdate(AppMeta appMeta, AppInstallPack existPack) {
		App a = appService.find(appMeta.getCpId(), appMeta.getOriginalAppId());
		if(a != null)	{
			a.setDeveloper(appMeta.getDeveloper());
			a.setInfo(appMeta.getInfo());	//当CP商的类别变化时,跟随CP商而变化
			if(STRATEGY_CHOOSENAME == 1)	{	
				a.setName(appMeta.getOriginalName());	//如果选择策略1,则每次汇集时都会把app.name修改成cp商的软件名
			}
			a.setPinyin(Util.getPinyinInitial(a.getName()));
			a.setRscTypeId(appMeta.getRscTypeId());	//当cp商的类别变化时,跟随cp商而变化
			a.setUpdateTime(new Date());
			
			//存icon
			if(a.getIcon() == null && appMeta.getIconUri() != null)	{
				try {
					InputStream fromCp = appMeta.getIconUri().toURL().openStream();
					String iconPath = appService.saveToIconDir(fromCp, ".jpg");
					a.setIcon(iconPath);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			//存banner
			if(a.getBanner() == null && appMeta.getBannerUri() != null)	{
				try {
					a.setBanner(appService.saveToPicDir(appMeta.getBannerUri().toURL().openStream(), ".jpg"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			//存截图
			if(a.getPic1() == null)	{
				int index = 1;
				for(URI uri : appMeta.getScreenshotsUri())	{
					try {
						String halfPath = appService.saveToPicDir(uri.toURL().openStream(), ".jpg");	//统一jpg
						a.getClass().getMethod("setPic" + index, String.class).invoke(a, halfPath);
						index ++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			appService.persist(a);
		}
		
		existPack.setStatus(appMeta.getStatus());	//每次都要修改状态值
		existPack.setUpdateTime(new Date());
		packDao.update(existPack);
	}
	
	/**
	 * 当安装包版本更新,且包名未变化
	 * @param existPack
	 * @param apkInfo
	 */
	private void whenPkgNameNotChange(AppInstallPack existPack, AppMeta appMeta, ApkInfo apkInfo) {
		existPack.setAppVer(appMeta.getOriginalVersion());//使用原始version
		existPack.setVersion(apkInfo.getVersionName());
		existPack.setApkVersionCode(apkInfo.getVersionCode());
		existPack.setFileSize(new Float(apkInfo.getFileLength()/1024));
		existPack.setStatus(appMeta.getStatus());
		existPack.setChangeLog(appMeta.getChangeLog());
		existPack.setUpdateTime(new Date());
		packDao.update(existPack);	//当版本升级后, 同时将该pack置为可用包,因为cp商可能将一款软件反复上下架
	}

	/**
	 * 当安装包版本已更新,且包名发生变化
	 * @param existAppId
	 * @param existPack
	 * @param apkInfo
	 */
	private void whenPkgNameChanged(String existAppId, AppInstallPack existPack, AppMeta appMeta, ApkInfo apkInfo) {
		existPack.setApkPkgName(apkInfo.getPackageName());
		existPack.setVersion(apkInfo.getVersionName());
		existPack.setApkVersionCode(apkInfo.getVersionCode());
		existPack.setAppVer(appMeta.getOriginalVersion());
		existPack.setAppId(existAppId);
		existPack.setCpUpdateTime(appMeta.getCpUpdateTime());
		existPack.setFileSize(new Float(apkInfo.getFileLength()/1024));
		existPack.setOriginalName(appMeta.getOriginalName());
		existPack.setUpdateTime(new Date());
		existPack.setStatus(appMeta.getStatus());
		existPack.setChangeLog(appMeta.getChangeLog());
		existPack.setUpdateTime(new Date());
		packDao.update(existPack);
	}
	
	
	/**
	 * 安装包第一次入库
	 * @param existAppId
	 * @param appMeta
	 * @param apkInfo
	 */
	private void whenPackNotIn(String existAppId, AppMeta appMeta, ApkInfo apkInfo) {
		AppInstallPack p = new AppInstallPack();
		p.setApkPkgName(apkInfo.getPackageName());
		p.setApkVersionCode(apkInfo.getVersionCode());
		p.setAppId(existAppId);
		p.setAppVer(appMeta.getOriginalVersion());
		p.setCpId(appMeta.getCpId());
		p.setCpUpdateTime(appMeta.getCpUpdateTime());
		p.setFileSize(new Float(apkInfo.getFileLength()/1024));
		p.setOriginalName(appMeta.getOriginalName());
		p.setOriginalPackId(appMeta.getOriginalPackId());
		p.setOs(appMeta.getOs());
		p.setOsMinVerValue(0L);	//默认为0
		p.setStatus(appMeta.getStatus());
		p.setUpdateTime(new Date());
		p.setUuid(UUID.randomUUID().toString());
		p.setVersion(apkInfo.getVersionName());
		p.setChangeLog(appMeta.getChangeLog());
		p.setUpdateTime(new Date());
		packDao.insert(p);
	}
	
	
	/**
	 * 新增一个app
	 * @param appMeta
	 * @param apkInfo
	 * @return
	 */
	private App insertNewApp(AppMeta appMeta, ApkInfo apkInfo) {
		App a = new App();
		a.setCpId(appMeta.getCpId());
		a.setDeveloper(appMeta.getDeveloper());
		a.setInfo(appMeta.getInfo());
		a.setName(this.chooseName(appMeta.getOriginalName(), apkInfo.chooseChineseLabel()));
		a.setPinyin(Util.getPinyinInitial(a.getName()));
		a.setOriginalAppId(appMeta.getOriginalAppId());
		a.setOriginalTypeId(appMeta.getOriginalTypeId());
		
		//存icon
		InputStream fromCp = null, fromPack = null;
		fromPack = new ByteArrayInputStream(apkInfo.getIcon());
		try {
			fromCp = appMeta.getIconUri().toURL().openStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			String iconPath = appService.saveToIconDir(this.chooseIcon(fromCp, fromPack), ".jpg");
			a.setIcon(iconPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//存banner
		try {
			InputStream banner = null;
			if(appMeta.getBannerUri() != null)	{
				banner = appMeta.getBannerUri().toURL().openStream();
			} else	{
				String wdjBanner = new WdjJsonParser(new WdjConnection().connect(apkInfo.getPackageName())).getBanner();
				if(wdjBanner != null)	{
					banner = new URL(wdjBanner).openStream();
				}
				Thread.sleep(15000);	//休眠,防止频繁访问豌豆荚.
			}
			if(banner != null)	{
				a.setBanner(appService.saveToPicDir(banner, ".jpg"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//存截图
		int index = 1;
		for(URI uri : appMeta.getScreenshotsUri())	{
			try {
				String halfPath = appService.saveToPicDir(uri.toURL().openStream(), ".jpg");	//统一jpg
				a.getClass().getMethod("setPic" + index, String.class).invoke(a, halfPath);
				index ++;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		a.setRscTypeId(appMeta.getRscTypeId());
		
		//以下为appInstallpack的冗余字段,可以不设值, 最后统一更新冗余字段
		a.setBundleId(apkInfo.getPackageName());
		a.setOs(Constants.ANDROID);
		a.setIpaItemId(null);	//ios专用字段,无需处理
		a.setFileSize(new Long(apkInfo.getFileLength()).floatValue());
		
		//以下给默认值
		a.setPoint(null);	
		a.setIsDel(0);
		a.setInstallCount(0);
		a.setDownCount(0);
		a.setIsAreaCustom(0);
		a.setUpdateTime(new Date());
		a.setUuid(UUID.randomUUID().toString());
		appService.persist(a);
		return a;
	}
	
	private String chooseName(String nameFromCp, String nameFromPack)	{
		if(STRATEGY_CHOOSENAME == 1)	{
			return nameFromCp;
		} else if(STRATEGY_CHOOSENAME == 2)	{
			return nameFromPack;
		} else	{
			throw new IllegalArgumentException("参数有误:" + STRATEGY_CHOOSENAME);
		}
	}
	
	private InputStream chooseIcon(InputStream iconFromCp, InputStream iconFromPack)	{
		if(STRATEGY_CHOOSENAME == 1)	{
			return iconFromCp;
		} else if(STRATEGY_CHOOSENAME == 2)	{
			return iconFromPack;
		} else	{
			throw new IllegalArgumentException("参数有误:" + STRATEGY_CHOOSENAME);
		}
	}
}
