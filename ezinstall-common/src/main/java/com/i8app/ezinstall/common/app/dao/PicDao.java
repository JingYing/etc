package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class PicDao extends BaseDao{

	public Long save(String savedPath) {
		Map map = new HashMap();
		map.put("savedPath", savedPath);
		return (Long)sqlMapClientTemplate.insert("Pic.insert", map);
	}

}
