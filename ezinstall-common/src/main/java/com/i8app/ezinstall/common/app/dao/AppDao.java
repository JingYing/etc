package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;


@Component
public class AppDao extends BaseDao	{
	private static Logger logger = Logger.getLogger(AppDao.class);
	
	public App findById(String uuid)	{
		return (App)sqlMapClientTemplate.queryForObject("App.byId", uuid);
	}
	
	public Pager<App> findAll(int offset, int pageSize)	{
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<App> list = sqlMapClientTemplate.queryForList("App.findAll", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("App.findAll-count", map);
		return new Pager<App>(list, count);
	}
	
	/**
	 * 分页查询包内的安装包和软件
	 * @param os
	 * @param pkgId
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<AppDTO> findByPkg(Os os, int pkgId, List<String> cpIds, int offset, int pageSize)	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("os", os);
		map.put("pkgId", pkgId);
		map.put("cpIds", cpIds);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		int count = (Integer)sqlMapClientTemplate.queryForObject("App.byPkg-count", map);
		List<AppDTO> list = sqlMapClientTemplate.queryForList("App.byPkg", map);
		return new Pager<AppDTO>(list, count);
	}
	
	/**
	 * 分页查询包内的安装包和软件
	 * @param os
	 * @param pkgId
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<AppDTO> findInstallPackByPkg(AreaParam areaParam, int pkgId, Os os, String[] cpIds, int offset, int pageSize)	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("os", os);
		map.put("pkgId", pkgId);
		map.put("cpIds", cpIds);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		map.put("areaParam", areaParam == null ? null : areaParam.toParamMap());
		int count = (Integer)sqlMapClientTemplate.queryForObject("App.findInstallPackByPkg-count", map);
		List<AppDTO> list = sqlMapClientTemplate.queryForList("App.findInstallPackByPkg", map);
		return new Pager<AppDTO>(list, count);
	}
	
	/**
	 * 全量查询
	 * @param areaParam
	 * @param os
	 * @param appParam
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<AppDTO> query(AreaParam areaParam, Os os, AppParam appParam, int offset, int pageSize) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("areaParam", areaParam.toParamMap());
		map.put("appParam", appParam.toParamMap());
		map.put("os", os);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		long t1 = System.currentTimeMillis();
		int count = (Integer)sqlMapClientTemplate.queryForObject("App.query-count", map);
		long t2 = System.currentTimeMillis();
		logger.debug(String.format("query-count时间:%.1f秒", (t2-t1)/1000.0));
		List<AppDTO> list = sqlMapClientTemplate.queryForList("App.query", map);
		long t3 = System.currentTimeMillis();
		logger.debug(String.format("query时间:%.1f秒", (t3-t2)/1000.0));
		
		return new Pager<AppDTO>(list, count);
	}

	/**
	 * 查询安装包. 不根据app去重
	 * @param areaParam
	 * @param os
	 * @param appParam
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<AppDTO> queryPack(AreaParam areaParam, Os os, AppParam appParam, int offset, int pageSize) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("areaParam", areaParam.toParamMap());
		map.put("appParam", appParam.toParamMap());
		map.put("os", os);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		long t1 = System.currentTimeMillis();
		int count = (Integer)sqlMapClientTemplate.queryForObject("App.queryPack-count", map);
		long t2 = System.currentTimeMillis();
		logger.debug(String.format("queryPack-count时间:%.1f秒", (t2-t1)/1000.0));
		@SuppressWarnings("unchecked")
		List<AppDTO> list = sqlMapClientTemplate.queryForList("App.queryPack", map);
		long t3 = System.currentTimeMillis();
		logger.debug(String.format("queryPack时间:%.1f秒", (t3-t2)/1000.0));
		return new Pager<AppDTO>(list, count);
	}

	/**
	 * 全量查询
	 * @param areaParam
	 * @param os
	 * @param appParam
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<AppDTO> search(AreaParam areaParam, Os os, AppParam appParam, int offset, int pageSize) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("areaParam", areaParam.toParamMap());
		map.put("appParam", appParam.toParamMap());
		map.put("os", os);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		long t1 = System.currentTimeMillis();
		int count = (Integer)sqlMapClientTemplate.queryForObject("App.search-count", map);
		long t2 = System.currentTimeMillis();
		logger.debug(String.format("search-count时间:%.1f秒", (t2-t1)/1000.0));
		List<AppDTO> list = sqlMapClientTemplate.queryForList("App.search", map);
		long t3 = System.currentTimeMillis();
		logger.debug(String.format("search时间:%.1f秒", (t3-t2)/1000.0));
		return new Pager<AppDTO>(list, count);
	}
	

	/**
	 * 详情查询
	 * @param appId
	 * @param os 不可以为空
	 * @return
	 */
	public AppDTO findSingle(String appId, Os os) {
		Map map = new HashMap();
		map.put("uuid", appId);
		map.put("os", os);
		AppDTO app = (AppDTO)sqlMapClientTemplate.queryForObject("App.findSingle", map);
		return app;
	}
	
	public AppDTO findSinglePack(String packUuid)	{
		return (AppDTO)sqlMapClientTemplate.queryForObject("App.findSinglePack", packUuid);
	}

	
	/**
	 * 插入或更新
	 * @param app
	 */
	public void persist(App app) {
		App a = this.findById(app.getUuid());
		if(a == null)
			sqlMapClientTemplate.insert("App.insert", app);
		else
			sqlMapClientTemplate.update("App.update", app);
	}

	/**
	 * 更新app表的信息, 不包含图标和截图
	 * @param app
	 */
	public void updateInfo(App app)	{
		sqlMapClientTemplate.update("App.updateInfo", app);
	}


	/**
	 * app的isDel设为1
	 * @param cpId
	 * @param appId
	 */
	public void updateIsDel(String cpId, String originalAppId) {
		Map map = new HashMap();
		map.put("cpId", cpId);
		map.put("originalAppId", originalAppId);
		sqlMapClientTemplate.update("App.updateIsDel", map);
	}
	
	/**
	 * 更新iconid
	 * @param appUuid
	 * @param iconId
	 */
	public void updateIcon(String appUuid, String icon)	{
		Map map = new HashMap();
		map.put("uuid", appUuid);
		map.put("icon", icon);
		sqlMapClientTemplate.update("App.updateIcon", map);
	}
	
	/**
	 * 切换APP绑定的软件源. 除了修改字段值,也要修改icon和pic字段值
	 * @param appId
	 * @param appParam
	 */
	public void updateBind(String appId, App appParam)	{
		Map map = new HashMap();
		map.put("uuid", appId);
		map.put("app", appParam);
		sqlMapClientTemplate.update("App.updateBind", map);
	}


	/**
	 * 
	 * @param cpId
	 * @param originalAppId
	 * @return appId
	 */
	public App find(String cpId, String originalAppId) {
		Map map = new HashMap();
		map.put("cpId", cpId);
		map.put("originalAppId", originalAppId);
		return (App)sqlMapClientTemplate.queryForObject("App.byCpIdAndOriginalAppId", map);
	}
	
	public App findInNewTable(String cpId, String originalAppId) {
		Map map = new HashMap();
		map.put("cpId", cpId);
		map.put("originalAppId", originalAppId);
		return (App)sqlMapClientTemplate.queryForObject("App.confirmInNewTable", map);
	}

	public List<App> findMissingIcon() {
		return sqlMapClientTemplate.queryForList("App.findMissingIcon");
	}
	
	public List<App> findMissingPic()	{
		return sqlMapClientTemplate.queryForList("App.findMissingPic");
	}

	/**
	 * 更新app的冗余字段: os, filesize, ipaItemId, bundleId
	 * @param persistedAppId
	 */
	public void updateRedundantField() {
		sqlMapClientTemplate.update("App.updateRedundantField");
	}
}
