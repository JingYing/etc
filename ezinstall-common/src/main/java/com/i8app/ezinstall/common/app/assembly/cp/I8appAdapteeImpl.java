package com.i8app.ezinstall.common.app.assembly.cp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.HttpClientUtil;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppMeta;
import com.i8app.ezinstall.common.app.dao.AssemblyLogDao;
import com.i8app.ezinstall.common.app.dao.I8appDao;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.domain.I8app;
import com.i8app.ezinstall.packreader.PackReadException;
import com.i8app.ezinstall.packreader.apk2.ApkReader2;
import com.i8app.ezinstall.packreader.ipa.IpaReader;

@Component
public class I8appAdapteeImpl implements AppAdaptee<I8app> {
	
	private static final Logger logger = Logger.getLogger(I8appAdapteeImpl.class);
	
	@Resource
	private I8appDao i8appDao;
	@Resource
	private AssemblyLogDao assemblyLogDao;
	private ApkReader2 apkReader2 = new ApkReader2();
	private IpaReader ipaReader = new IpaReader();
	private String packAccessMode;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public String findPackUrl(String originalPackId, String os)
			throws CpServerAccessException {
		I8app s = i8appDao.findById(Integer.parseInt(originalPackId));	//s为空时需要向外抛异常
		if(s == null)	throw new RuntimeException("missing i8 id:" + originalPackId);
		String halfPath = s.getPackUrl();
		if(halfPath != null && !"".equals(halfPath))	{
			return FILESERVER_URL + halfPath;
		} else	{
			return null;
		}
	}

	@Override
	public List<String> findScreenshots(String originalAppId) {
		I8app s = i8appDao.findById(Integer.parseInt(originalAppId));
		if(s == null){
			logger.error("missing i8 id:" + originalAppId);
		}	//throw new RuntimeException("missing i8 id:" + originalAppId);
		List<String> list = new ArrayList<String>();
		if(s != null)	{
			String s1 = s.getPic1();
			if(s1 != null && s1.length()>0)
				list.add(FILESERVER_URL + s1);
	
			String s2 = s.getPic2();
			if(s2 != null && s2.length()>0)
				list.add(FILESERVER_URL + s2);
			
			String s3 = s.getPic3();
			if(s3 != null && s3.length()>0)
				list.add(FILESERVER_URL + s3);

			String s4 = s.getPic4();
			if(s4 != null && s4.length()>0)
				list.add(FILESERVER_URL + s4);
		}
		return list;
	}

	@Override
	public String findIconUrl(String originalAppId) {
		I8app s = i8appDao.findById(Integer.parseInt(originalAppId));
		if(s!= null && s.getIcon() != null)	{
			return FILESERVER_URL + s.getIcon(); 
		} 
		return null;
	}
	
	@Override
	public List findByOriginalPackIds(List<String> originalPackIds) {
		return i8appDao.findByIds(originalPackIds);
	}

	@Override
	public Pager<I8app> findValidApp(int offset, int pageSize) {
		return i8appDao.findToImport(offset, pageSize);
	}
	
	@Override
	public AppMeta toAppMeta(I8app i)	{
		AppMeta meta = new AppMeta();
		
		if(MODE_LOCAL.equals(packAccessMode))	{	//本地访问模式下, 取fileserverdir
			if(i.getBanner() != null)	{
				meta.setBannerUri(new File(FILESERVER_DIR + i.getBanner()).toURI());
			}
			if(i.getIcon() != null)	{
				meta.setIconUri(new File(FILESERVER_DIR + i.getIcon()).toURI());
			}
			
			List<URI> ssUris = new ArrayList<URI>();
			if(i.getPic1() != null && i.getPic1().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + i.getPic1()).toURI());
			if(i.getPic2() != null && i.getPic2().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + i.getPic2()).toURI());
			if(i.getPic3() != null && i.getPic3().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + i.getPic3()).toURI());
			if(i.getPic4() != null && i.getPic4().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + i.getPic4()).toURI());
			meta.setScreenshotsUri(ssUris);
		} else if(MODE_REMOTE.equals(packAccessMode))	{	//远程下载模式下,取fileserverurl
			if(i.getBanner() != null)	{
				try {
					meta.setBannerUri(new URI(FILESERVER_URL + i.getBanner()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
			if(i.getIcon() != null)	{
				try {
					meta.setIconUri(new URI(FILESERVER_URL + i.getIcon()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
			
			List<URI> ssUris = new ArrayList<URI>();
			if(i.getPic1() != null && i.getPic1().length() > 0)
				try {
					ssUris.add(new URI(FILESERVER_URL + i.getPic1()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			if(i.getPic2() != null && i.getPic2().length() > 0)
				try {
					ssUris.add(new URI(FILESERVER_URL + i.getPic2()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			if(i.getPic3() != null && i.getPic3().length() > 0)
				try {
					ssUris.add(new URI(FILESERVER_URL + i.getPic3()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			if(i.getPic4() != null && i.getPic4().length() > 0)
				try {
					ssUris.add(new URI(FILESERVER_URL + i.getPic4()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			meta.setScreenshotsUri(ssUris);
		} else 	{
			throw new UnsupportedOperationException("不支持的访问模式:" + packAccessMode);
		}

		meta.setRscTypeId(i.getRscTypeId());
		meta.setChangeLog(i.getChangeLog());
		meta.setCpId(CP_I8APP);
		meta.setDeveloper(i.getDeveloper());
		meta.setInfo(i.getInfo());
		meta.setOriginalAppId(i.getId() + "");
		meta.setOriginalName(i.getName());
		meta.setOriginalPackId(i.getId() + "");
		meta.setOriginalTypeId(i.getRscTypeId());
		meta.setOriginalVersion(i.getVersion());
		meta.setOs(i.getOs());
		meta.setCpUpdateTime(sdf.format(i.getUpdateTime()));
		meta.setStatus(i.getIsDel());
		return meta;
	}

	@Override
	public boolean isPackUpdated(AppMeta appMeta, AppInstallPack existPack) {
		return appMeta.getCpUpdateTime().equals(existPack.getCpUpdateTime()) 
						? false : true;
	}

	@Override
	public void setRscTypeMap(Map<String, String> rscTypeMap) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getCpId() {
		return CP_I8APP;
	}

	@Override
	public Object readPack(String originalPackId, String os) 
		throws CpServerAccessException, PackReadException, FileNotFoundException	{
		I8app i = i8appDao.findById(Integer.parseInt(originalPackId));
		if(MODE_LOCAL.equals(getPackAccessMode()))	{	//本地磁盘模式
			File f = new File(FILESERVER_DIR + i.getPackUrl());
			if(f.isFile() && f.exists())	{
				if(ANDROID.equalsIgnoreCase(os)){
					return apkReader2.read(f, true);	//把icon也读进去
				} else if(IOS.equalsIgnoreCase(os) || IOSX.equalsIgnoreCase(os))	{
					return ipaReader.read(f);
				} else	{
					return f.length();
				}
			} else	{
				throw new FileNotFoundException("未找到相关磁盘文件:" + originalPackId + ":" + f.getAbsolutePath());
			}
		} else if(MODE_REMOTE.equals(getPackAccessMode()))	{	//远程下载模式
			String url = FILESERVER_URL + i.getPackUrl();
			File tmp = null;
			if(ANDROID.equalsIgnoreCase(os)){
				try {
					logger.info("正在下载i8appAndroid软件, id=" + i.getId() + ",url=" + url);
					tmp = HttpClientUtil.downloadToTemp(url, ".apk", 60 * 5);
					return apkReader2.read(tmp, true);
				} catch (IOException e) {	
					throw new CpServerAccessException(e.getMessage(), e);
				} finally {	
					if(tmp != null)	{
						tmp.delete();
					}
				}
			} else if(IOS.equalsIgnoreCase(os) || IOSX.equalsIgnoreCase(os))	{
				try {
					logger.info("正在下载i8appIOS软件, id=" + i.getId() + ",url=" + url);
					tmp = HttpClientUtil.downloadToTemp(url, ".ipa", 60 * 5);
					return ipaReader.read(tmp);
				} catch (IOException e) {
					throw new CpServerAccessException(e.getMessage(), e);
				} finally {	//下载后,如果解析失败,先删除临时文件再向外抛异常
					if(tmp != null && tmp.isFile())	{
						tmp.delete();
					}
				}
			} else	{
				return i.getFileSize() * 1024;	//直接从数据中获取大小即可, 不需要远程下载, i8app.filesize的单位为KB
			}
		} else	{
			throw new UnsupportedOperationException("不支持的访问模式:" + getPackAccessMode());
		}
	}
	
	public String getPackAccessMode() {
		return packAccessMode;
	}
	
	public void setPackAccessMode(String packAccessMode) {
		this.packAccessMode = packAccessMode;
	}



}
