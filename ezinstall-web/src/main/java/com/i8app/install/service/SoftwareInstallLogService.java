package com.i8app.install.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.SoftwareInstallLogDao;
import com.i8app.install.domain.AppFlowLog;
import com.i8app.install.domain.SoftwareInstallLog;
import com.i8app.install.domain.UserFlowLog;

@Component()
public class SoftwareInstallLogService {
	@Resource
	private SoftwareInstallLogDao softwareInstallLogDao;
	public SoftwareInstallLogDao getSoftwareInstallLogDao() {
		return softwareInstallLogDao;
	}
	public void setSoftwareInstallLogDao(SoftwareInstallLogDao softwareInstallLogDao) {
		this.softwareInstallLogDao = softwareInstallLogDao;
	}
	public void saveOrUpdateSoftwareInstallLog(SoftwareInstallLog p)	{
		softwareInstallLogDao.saveOrUpdateSoftwareInstallLog(p);
	}
	public void saveOrUpdateUserFlowLog(UserFlowLog p){
		softwareInstallLogDao.saveOrUpdateUserFlowLog(p);
	}
	public void saveOrUpdateAppFlowLog(AppFlowLog p){
		softwareInstallLogDao.saveOrUpdateAppFlowLog(p);
	}
	public int findSoftwareInstallLog(String peopleId, String pname)	{
		return softwareInstallLogDao.findSoftwareInstallLog(peopleId, pname);
	}
}
