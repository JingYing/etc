package com.i8app.install.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.install.Config;
import com.i8app.install.dao.VideoInfoDao;
import com.i8app.install.domain.VideoInfo;

@Component("videoInfoService")
public class VideoInfoService{
	
	private VideoInfoDao videoInfoDao;
	
	private String HTTP_PATH;
	
	public void addOrUpdate(VideoInfo videoInfo) {
		videoInfoDao.addOrUpdate(videoInfo);
	}
	
	/**
	 * 将List<VideoInfo>展示为Json格式
	 * @param VideoInfoList VideoInfo类的List
	 * @return JSON字符串
	 * @author jingying
	 */
	public String changeToJson(List<VideoInfo> list)	{
		JsonArray jsonArray = new JsonArray();
		for(VideoInfo v : list)	{
			JsonObject json = new JsonObject();
			json.addProperty("id", v.getId() + "");
			json.addProperty("videoName", v.getVideoName() + "");
			json.addProperty("videoSize", v.getVideoSize() + "");
			json.addProperty("videoLen", v.getVideoLen() + "");
			json.addProperty("videoType", v.getVideoType() == null ? "" : v.getVideoType().getTypeName() + "");
			json.addProperty("suffix", v.getSuffix() == null ? "" : v.getSuffix().getSuffixName() + "");
			
			String s1 = v.getVideoFile();
			if(s1 == null || "".equals(s1.trim()))
				json.addProperty("videoFile", "");
			else
				json.addProperty("videoFile", HTTP_PATH + s1.replace("\\", "/"));

			json.addProperty("desc", v.getDesc());

			String s2 = v.getVideoImgFile();
			if(s2 == null || "".equals(s2.trim()))
				json.addProperty("videoImgFile", "");
			else
				json.addProperty("videoImgFile", HTTP_PATH + s2.replace("\\", "/"));
			
			json.addProperty("dateAdded", v.getDateAdded() + "");
			json.addProperty("totalDownloadNum", v.getTotalDownloadNum() + "");
			json.addProperty("weekDownloadNum", v.getWeekDownloadNum() + "");
			json.addProperty("monthDownloadNum", v.getMonthDownloadNum() + "");
			json.addProperty("hotDegree", v.getHotDegree() + "");
			json.addProperty("userRemarkNum", v.getUserRemarkNum() + "");
			json.addProperty("userRemark", v.getUserRemark() + "");
			json.addProperty("memo", v.getMemo() + "");
			jsonArray.add(json);
		}
		return jsonArray.toString().replace("null", "");
	}
	 
	public int count()	{
		return videoInfoDao.count();
	}
 
	public VideoInfo findById(int id) {
		return videoInfoDao.findById(id);
	}

	public List<VideoInfo> list() {
		return videoInfoDao.list();
	}

	public List<VideoInfo> list(int pageNo, int pageSize) {
		return videoInfoDao.list(pageNo, pageSize);
	}

	public List<VideoInfo> list(String videoName, int videoTypeId)	{
		return videoInfoDao.list(videoName, videoTypeId);
	}

	public String listJson()	{
		return this.changeToJson(this.list());
	}

	public String listJson(int pageNo, int pageSize)	{
		return this.changeToJson(this.list(pageNo, pageSize));
	}

	@Resource
	public void setVideoInfoDao(VideoInfoDao videoInfoDao) {
		this.videoInfoDao = videoInfoDao;
	}
	
	@Resource
	public void setConfig(Config config)	{
		this.HTTP_PATH = config.getFileServerUrl();
	}
 
	 
	 
}
