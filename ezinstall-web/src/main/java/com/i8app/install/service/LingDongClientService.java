package com.i8app.install.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.install.InvalidOsVersionException;
import com.i8app.install.Util;
import com.i8app.install.dao.LingDongClientDao;
import com.i8app.install.domain.I8client;
import com.i8app.install.domain.LingDongClient;
import com.i8app.install.domain.Tongyong;

@Service
public class LingDongClientService {

	@Resource
	private LingDongClientDao dao;
	
	public List<LingDongClient> find(int softId, int osType, int subType, int osid)	{
		return dao.find(softId, osType, subType, osid);
	}
	
	/**
	 * 对比集合中哪个SoftVersion的版本号最大
	 * @throws InvalidOsVersionException 
	 */
	public LingDongClient latestVersion(List<LingDongClient> list) throws InvalidOsVersionException	{
		if(list == null || list.isEmpty())	
			return null;
		int flag = 0;
		long result = Util.softVerToInt(list.get(0).getSoftVersion());
		System.out.println("选择最大版本开始");
		for(int i=0; i<list.size(); i++)	{
			long intVal = Util.softVerToInt(list.get(i).getSoftVersion());	
			if( intVal > result)	{
				flag = i;
				result = intVal;
			}
		}
		System.out.println("选择最大版本完毕");
		return list.get(flag);
	}
	
	
	public Tongyong latestVersionsoft(List<Tongyong> list) throws InvalidOsVersionException	{
		if(list == null || list.isEmpty())	
			return null;
		int flag = 0;
		long result = Util.softVerToInt(list.get(0).getTongyong4());
		for(int i=0; i<list.size(); i++)	{
			long intVal = Util.softVerToInt(list.get(i).getTongyong4());	
			if( intVal > result)	{
				flag = i;
				result = intVal;
			}
		}
		return list.get(flag);
	}
	
	public I8client findI8client(String bundleId,String os){
		return dao.findI8client(bundleId, os);
	}
}
