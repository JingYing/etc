package com.i8app.install.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.BookTypeDao;
import com.i8app.install.domain.BookType;
import com.i8app.install.service.BookTypeService;
@Component("bookTypeService")
public class BookTypeService{
	private BookTypeDao bookTypeDao;
	
	public void setBookTypeDao(BookTypeDao bookTypeDao) {
		this.bookTypeDao = bookTypeDao;
	}

	public void addOrUpdate(BookType bookType) {
		bookTypeDao.addOrUpdate(bookType);
	}

	public List<BookType> bookTypeList() {
		return bookTypeDao.list();
	}

	public void del(int id) {
		bookTypeDao.del(id);
	}

	public BookType findbookTypeById(int id) {
		return bookTypeDao.findbookById(id);
	}

}
