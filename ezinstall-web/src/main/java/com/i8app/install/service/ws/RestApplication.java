package com.i8app.install.service.ws;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

public class RestApplication extends ResourceConfig {

	/**
	 * Register JAX-RS application components.
	 */
	public RestApplication() {
		register(RequestContextFilter.class);
		register(AppRestService.class);
	}
}
