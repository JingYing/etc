package com.i8app.install.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.i8app.install.Config;
import com.i8app.install.Pager;
import com.i8app.install.Util;
import com.i8app.install.dao.MobileClickDao;
import com.i8app.install.dao.PeopleDao;
import com.i8app.install.domain.EnterpriseInfo;
import com.i8app.install.domain.Messagelog;
import com.i8app.install.domain.PeopleInfo;
import com.i8app.install.domain.Subscription;
import com.i8app.install.domain.Tag;
import com.i8app.install.jpush.IOSExtra;
import com.i8app.install.jpush.JPushClient;
import com.i8app.install.jpush.MessageResult;

@Service
public class MobileClickService {
	
    //在极光注册上传应用的 appKey 和 masterSecret
	private static final String appKey ="a259be4e66111c65262a0432";////必填，例如466f7032ac604e02fb7bda89
	private static final String masterSecret = "96060bc41bc867a7929be1a0";//必填，每个应用都对应一个masterSecret//04809ed319cd34d8
	private static JPushClient jpush = null;
	/*
	 * 保存离线的时长。秒为单位。最多支持10天（864000秒）。
	 * 0 表示该消息不保存离线。即：用户在线马上发出，当前不在线用户将不会收到此消息。
	 * 此参数不设置则表示默认，默认为保存1天的离线消息（86400秒
	 */
	private static long timeToLive =  60 * 60 * 24;  
    public static final int MAX = Integer.MAX_VALUE;
	public static final int MIN = (int) MAX/2;
	
	@Resource
	public MobileClickDao mobileClickDao;
	@Resource
	private PeopleDao peopleDao;
	@Resource
	public Config config;
	public Pager<EnterpriseInfo> pagerlist(String name,int pageNo,int pageSize) {
		return mobileClickDao.pagerlist(name,"0", pageNo, pageSize);
	}
	public Pager<EnterpriseInfo> pagerAlllist(String name,int pageNo,int pageSize) {
		return mobileClickDao.pagerlist(name,null, pageNo, pageSize);
	}
	public Pager<Tag> pagerTaglist(int pageNo,int pageSize) {
		return mobileClickDao.pagerTaglist(null,pageNo, pageSize);
	}
	public Pager<Tag> pagerTaglist(String name,int pageNo,int pageSize) {
		return mobileClickDao.pagerTaglist(name,pageNo, pageSize);
	}
	public Pager<Subscription> pagerSubscribedlist(String pid,int pageNo,int pageSize) {
		return mobileClickDao.pagerSubscribedlist(pid, pageNo, pageSize);
	}
	
	public Pager<Messagelog> pagerMessageloglist(int sid,int aid,int mid,int isdelete,String starttime,String endtime,int pageNo,int pageSize) {
		return mobileClickDao.pagerMessageloglist(sid, aid, mid, isdelete, starttime, endtime, pageNo, pageSize);
	}
	public void addOrUpdatePrise(EnterpriseInfo ent) {
		mobileClickDao.addOrUpdatePrise(ent);
	}
	public void addOrUpdateSub(Subscription sub) {
		mobileClickDao.addOrUpdateSub(sub);
	}
	public void addOrUpdateTag(Tag tag) {
		mobileClickDao.addOrUpdateTag(tag);
	}
	public void addOrUpdateLog(Messagelog tag) {
		mobileClickDao.addOrUpdateLog(tag);
	}
	public void delSubscription(int id) {
		mobileClickDao.delSubscription(id);
	}
	public void delEnterpriseInfo(int id) {
		mobileClickDao.delEnterpriseInfo(id);
	}
	public void delTag(int id) {
		mobileClickDao.delTag(id);
	}
	public void buldelEnterpriseInfo(int[] enIds) {
		for (int enId : enIds) {
			mobileClickDao.delEnterpriseInfo(enId);
		}
	}
	public void buldelTag(String[] tagIds) {
		for (String tagId : tagIds) {
			mobileClickDao.delTag(Integer.parseInt(tagId));
		}
	}
	public int findByPersion(int eid,String pid) {
		return mobileClickDao.findByPersion(eid, pid);
	}
	public EnterpriseInfo findEnterpriseInfoById(int id) {
		return mobileClickDao.findEnterpriseInfoById(id);
	}
	public Subscription findSubscriptionById(int id) {
		return mobileClickDao.findSubscriptionById(id);
	}
	public Tag findTagById(int id) {
		return mobileClickDao.findTagById(id);
	}
	public Subscription findByPidAndEid(final int eid,final String pid) {
		return mobileClickDao.findByPidAndEid(eid, pid);
	}
	public  MessageResult sendAllMessage(String pid,String title,Messagelog sms)throws Exception{
		List<Messagelog> smslist = new ArrayList<Messagelog>();
 		jpush = new JPushClient(masterSecret, appKey, timeToLive);
		//jpush.setEnableSSL(true);
 		sms.setSendtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
 		
 		Map<String, Object> extra = new HashMap<String, Object>();
		IOSExtra iosExtra = new IOSExtra(1, "WindowsLogonSound.wav");
		extra.put("ios", iosExtra);
		EnterpriseInfo enter = new EnterpriseInfo();
		if(sms.getSendtype()==1){
			enter = findEnterpriseInfoById(Integer.parseInt(sms.getSenderid()));
			sms.setSender(enter.getEnterpriseName());
		}
		smslist.add(sms);
		String jsonStr = null;
		MessageResult result = null;
		// 在实际业务中，建议 sendNo 是一个你自己的业务可以处理的一个自增数字。
	    // 除非需要覆盖，请确保不要重复使用。详情请参考 API 文档相关说明。
	    Integer num= getRandomSendNo();
	    String sendNo=num+"";
	    if(pid !=null && !"".equals(pid)){
	    	PeopleInfo p = peopleDao.findByPeopleID(pid);
	    	jsonStr = objectTojson(enter,p,smslist);
	    	System.out.println(jsonStr.length());
	    	result = jpush.sendCustomMessageWithAlias(sendNo, pid, title, jsonStr, sms.getMessagetype()+"", extra);
	    }else{
			jsonStr = objectTojson(enter,null,smslist);
			result = jpush.sendCustomMessageWithTag(sendNo, String.valueOf(sms.getAcceptedid()), title, jsonStr, sms.getMessagetype()+"", extra);
		}
		smslist.clear();
 		addOrUpdateLog(sms);
 		System.out.println(result);
 		return result;
	}
	
	public MessageResult sendSysTemMess(String title,Messagelog sms) throws Exception{
		List<Messagelog> smslist = new ArrayList<Messagelog>();
		jpush = new JPushClient(masterSecret, appKey, timeToLive);
		sms.setSendtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
 		Map<String, Object> extra = new HashMap<String, Object>();
		IOSExtra iosExtra = new IOSExtra(1, "WindowsLogonSound.wav");
		extra.put("ios", iosExtra);
		MessageResult result = null;
		// 在实际业务中，建议 sendNo 是一个你自己的业务可以处理的一个自增数字。
	    // 除非需要覆盖，请确保不要重复使用。详情请参考 API 文档相关说明。
	    Integer num= getRandomSendNo();
	    String sendNo=num+"";
	    String jsonStr = null;
	    EnterpriseInfo enter = findEnterpriseInfoById(1);
	    sms.setSender(enter.getEnterpriseName());
	    sms.setSenderid(enter.getId()+"");
	    smslist.add(sms);
	    jsonStr = objectTojson(enter,null,smslist);
	    result  = jpush.sendCustomMessageWithAppKey(sendNo, title, jsonStr, sms.getMessagetype()+"", extra);
		smslist.clear();
 		addOrUpdateLog(sms);
	    return result;
	}
	/**
	 * 保持 sendNo 的唯一性是有必要的
	 * It is very important to keep sendNo unique.
	 * @return sendNo
	 */
	public static int getRandomSendNo() {
	    return (int) (MIN + Math.random() * (MAX - MIN));
	}
	
	public String writeFile(String fileName, File fileRead)	{
		String childPath = null;
		childPath = "img" + "/" + "file" + 
		"/"+ new SimpleDateFormat("yyyyMMdd").format(new Date()) + 
			"/" + Util.createTimestamp() + fileName.substring(fileName.lastIndexOf("."));
		File fileWrite = new File(config.getFileServerDir() + childPath);
		this.writeToDisk(fileRead, fileWrite);
		return  childPath;
	}
	private void writeToDisk(File fileRead, File fileWrite)	{
		if(!fileWrite.getParentFile().exists())	{
			fileWrite.getParentFile().mkdirs();
		}
		FileInputStream fis = null;
		FileOutputStream fos = null;
		FileChannel fcIn = null, fcOut = null;
		try {
			fis = new FileInputStream(fileRead);
			fos = new FileOutputStream(fileWrite);
			fcIn = fis.getChannel();
			fcOut = fos.getChannel();
			fcIn.transferTo(0, fcIn.size(), fcOut);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(fcIn != null)
					fcIn.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if(fcOut != null)
					fcOut.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				if (fos != null) 
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public String objectTojson(EnterpriseInfo enter,PeopleInfo p ,List<Messagelog> smsList) throws JSONException{
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		for(Messagelog sms:smsList){
			JSONObject json1 = new JSONObject();
			
			json1.put("sender_type", sms.getSendtype());
			json1.put("sender_id", enter !=null? enter.getId():"");
			json1.put("sender_name",enter !=null?  enter.getEnterpriseName():"");
			json1.put("sender_nick", enter !=null? enter.getNickname():"");
			json1.put("sender_img", enter !=null? config.getFileServerUrl()+enter.getImg():"");
			if(p !=null){
				json1.put("receiver_type", sms.getAcceptedtype());
				json1.put("receiver_id",p.getId());
				json1.put("receiver_name", p.getUserNumber());
				json1.put("receiver_img", config.getFileServerUrl() + p.getImg());
			}else{
				json1.put("receiver_type", sms.getAcceptedtype());
				json1.put("receiver_id","");
				json1.put("receiver_name", "");
				json1.put("receiver_img", "");
			}
			
			json1.put("message_type", sms.getMessagetype());
			json1.put("content", sms.getMessagecontent());
			json1.put("file_path",(sms.getFile_url() !=null && !"".equals(sms.getFile_url()))?config.getFileServerUrl()+sms.getFile_url():"");
			json1.put("jump_url", sms.getJump_url());
			
			
			json1.put("send_time", sms.getSendtime());
			json1.put("status", sms.getIsdelete());
			
			array.put(json1);
		}
		json.put("data", array);
		return json.toString();
	}
	
	public List<EnterpriseInfo> findEnterpriseList(){
		return mobileClickDao.findEnterpriseList();
	}
	public List<Tag> findTagList(int pid){
		return mobileClickDao.findTagList(pid);
	}
	public static void main(String[] args) {
//		Bbb b = new Bbb();
//		b.aa();
	}
}
