package com.i8app.install.service.ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.Constants.OrderBy;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.install.Config;
import com.i8app.install.dao.RestSourceDao;
import com.i8app.install.domain.App;
import com.i8app.install.domain.Pkg;
import com.i8app.install.domain.Resource_type;
import com.i8app.install.service.WapService;

/**
 * 必传参数 : chanid:渠道id, 灵动PC客户端1, 灵动手机客户端2,其它待分配. os: os1(安卓),os2(ios正),
 * os3(ios越)
 * 
 * 
 * @author JingYing 2013-9-8
 * 
 */
@Path("/app")
@Component
@Scope("request")
@Singleton
public class AppRestService {
	private static Logger logger = Logger.getLogger(AppRestService.class);
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private RestSourceDao restSourceDao;
	@Resource
	private WapService wapService;
	@Resource
	private Config config;
	/**
	 * 软件分类
	 * url示例: http://localhost:8080/wap/api/app/type?chan=xx
	 * 利用@XmlRootElement自动生成app的json串
	 * @param chan 渠道
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@GET
	@Path("type")
	@Produces({ MediaType.APPLICATION_JSON })
	public String findSourceType(@QueryParam("chan") String chan) {
		JsonObject json1 = new JsonObject();
		try{
			List<Resource_type> resourceList = restSourceDao.findResource_type();
			if(resourceList !=null && resourceList.size()>0){
				json1.addProperty("success", "20000");
				JsonArray js = new JsonArray();
				for(int i=0;i<resourceList.size();i++){
					JsonObject json = new JsonObject();
					json.addProperty("id", resourceList.get(i).getId() + "");
					json.addProperty("name", resourceList.get(i).getName() + "");
					json.addProperty("isLeaf", resourceList.get(i).getIsLeaf() + "");
					json.addProperty("depth", resourceList.get(i).getDepth() + "");
					json.addProperty("pid", resourceList.get(i).getPid() + "");
					json.addProperty("iconId", resourceList.get(i).getIconId() + "");
					js.add(json);
				}
				json1.add("data", js);
			}else{
				json1.addProperty("success", "20001");
			}
		}catch(Exception e){
			logger.info(e);
			json1.addProperty("success", "20400");
		}
		doPrint(json1.toString());
		return null;
	}
	
	
	/**
	 * 根据安装包id查询软件详情
	 * url示例: http://localhost:8080/wap/api/app/pack/1870477c-cdc0-4b0a-b302-3f6824eb1551?chan=1
	 * 利用@XmlRootElement自动生成app的json串
	 * @param uuid           app_install_pack的uuid
	 * @param chan 渠道 
	 * @return
	 */
	@GET
	@Path("pack/{uuid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String findByPackUuId(
					@PathParam("uuid") String packUuid,
					@QueryParam("chan") String chan) {
		JsonObject json1 = new JsonObject();
		if(packUuid !=null && !"".equals(packUuid)){
			try{
				AppDTO appdto = appQuerier.findSinglePack(packUuid);
				JsonObject js = new JsonObject();
				if(appdto !=null){
					json1.addProperty("success", 20000);
					js.addProperty("cpid", appdto.getCpId()== null ? "" :appdto.getCpId());
					js.addProperty("cpname", appdto.getCpName()== null ? "" :appdto.getCpName());
					js.addProperty("cpUpdateTime", appdto.getCpUpdateTime()== null ? "" :appdto.getCpUpdateTime());
					js.addProperty("developer", appdto.getDeveloper()== null ? "" :appdto.getDeveloper());
					js.addProperty("icon", appdto.getIconUrl()== null ? "" :appdto.getIconUrl());
					js.addProperty("info", appdto.getInfo()== null ? "" :appdto.getInfo());
			//		js.addProperty("infohtml", appdto.getInfoHtml()== null ? "" :appdto.getInfoHtml());
					js.addProperty("markerType", appdto.getMarkerType()== null ? "" :appdto.getMarkerType());
					js.addProperty("markerTypeName", appdto.getMarkerTypeName()== null ? "" :appdto.getMarkerTypeName());
					js.addProperty("name", appdto.getName()== null ? "" :appdto.getName());
					js.addProperty("originalAppId", appdto.getOriginalAppId()== null ? "" :appdto.getOriginalAppId());
					js.addProperty("originalPackId", appdto.getOriginalPackId()== null ? "" :appdto.getOriginalPackId());
					js.addProperty("packAppVer", appdto.getPackAppVer()== null ? "" :appdto.getPackAppVer());
					js.addProperty("packCpId", appdto.getPackCpId()== null ? "" :appdto.getPackCpId());
					js.addProperty("packCpUpdateTime", appdto.getPackCpUpdateTime()== null ? "" :appdto.getPackCpUpdateTime());
					js.addProperty("packFileSize", appdto.getPackFileSize()== null ? "" :appdto.getPackFileSize());
					js.addProperty("packOs", appdto.getPackOs()== null ? "" :appdto.getPackOs());
					js.addProperty("packOsMinVer", appdto.getPackOsMinVer()== null ? "" :appdto.getPackOsMinVer());
					js.addProperty("packUuid", appdto.getPackUuid()== null ? "" :appdto.getPackUuid());
					js.addProperty("pinyin", appdto.getPinyin()== null ? "" :appdto.getPinyin());
					js.addProperty("point", appdto.getPoint());
					JsonObject json2 = new JsonObject();
					for(int i=0;i<appdto.getPicsUrl().size();i++){
						json2.addProperty("pic"+i, appdto.getPicsUrl().get(i));
					}
					js.add("pics", json2);
					js.addProperty("rscTypeId", appdto.getRscTypeId()== null ? "" :appdto.getRscTypeId());
					js.addProperty("rscTypeName", appdto.getRscTypeName()== null ? "" :appdto.getRscTypeName());
					js.addProperty("rscTypePid", appdto.getRscTypePid()== null ? "" :appdto.getRscTypePid());
					js.addProperty("uuid", appdto.getUuid()== null ? "" :appdto.getUuid());
					js.addProperty("discriminator", appdto.getDiscriminator());
					js.addProperty("downCount", appdto.getDownCount()+ "");
					js.addProperty("installCount", appdto.getInstallCount()+ "");
					json1.add("data", js);
				}else{
					json1.addProperty("success", 20001);
				}
			}catch(Exception e){
				logger.info(e);
				json1.addProperty("success", 20400);
			}
		}else{
			json1.addProperty("success", 20002);
		}
		doPrint(json1.toString());
		return null;
	}

	
	/**
	 * 根据APP软件appId和操作系统os及cp商cpId查询软件详情
	 * url示例: http://localhost:8080/wap/api/app/pack?appId=b009a604-693a-4acb-a706-2c691945b7b3&os=os1&chan=1&cpId=cp09
	 * 利用@XmlRootElement自动生成app的json串
	 * @param appId         app的uuid
	 * @param os  操作系统
	 * @param chan 渠道
	 * @param cpId  cp商，多个cp商可以用,隔开
	 * @return
	 */
	@GET
	@Path("pack")
	@Produces({ MediaType.APPLICATION_JSON })
	public String findByAppIdandOs(
			@QueryParam("appId") String appId,
			@QueryParam("os") String os,
			@QueryParam("chan") String chan,
			@QueryParam("cpId") String cpId) {
		JsonObject json1 = new JsonObject();

		if(appId !=null && !"".equals(appId) && os !=null && !"".equals(os)){
			Pattern p = Pattern.compile("^os[1,2,3,4,5,6]{1}$"); 
			Matcher m = p.matcher(os); 
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
			if(cpId !=null && !"".equals(cpId)){
				Pattern p2 = Pattern.compile("^cp0[1,2,3,4,5,6,7,8,9]{1}$"); 
				Matcher m2 = p2.matcher(cpId); 
				if(!m2.matches()){
					json1.addProperty("success", 20002);
					doPrint(json1.toString());
					return null;
				}
			}
			try{
				Os ostype = Os.ANDROID;
				if(os !=null && !"".equals(os)){
					if("os1".equals(os)){
						ostype = Os.ANDROID;
					}else if("os2".equals(os)){
						ostype = Os.IOS;
					}else if("os3".equals(os)){
						ostype = Os.IOSX;
					}else if("os4".equals(os)){
						ostype = Os.SYMBIAN;
					}else if("os5".equals(os)){
						ostype = Os.JAVA;
					}else if("os6".equals(os)){
						ostype = Os.WINMOBILE;
					}else{
						throw new IllegalArgumentException("无效的操作系统os:" + os);
					}
				}else{
					ostype = Os.ANDROID;
				}
				AppDTO appdto = appQuerier.findSingle(appId, ostype);
				JsonObject js = new JsonObject();
				if(appdto !=null){
					json1.addProperty("success", 20000);
					js.addProperty("cpid", appdto.getCpId()== null ? "" :appdto.getCpId());
					js.addProperty("cpname", appdto.getCpName()== null ? "" :appdto.getCpName());
					js.addProperty("cpUpdateTime", appdto.getCpUpdateTime()== null ? "" :appdto.getCpUpdateTime());
					js.addProperty("developer", appdto.getDeveloper()== null ? "" :appdto.getDeveloper());
					js.addProperty("icon", appdto.getIconUrl()== null ? "" :appdto.getIconUrl());
					js.addProperty("info", appdto.getInfo()== null ? "" :appdto.getInfo());
			//		js.addProperty("infohtml", appdto.getInfoHtml()== null ? "" :appdto.getInfoHtml());
					js.addProperty("markerType", appdto.getMarkerType()== null ? "" :appdto.getMarkerType());
					js.addProperty("markerTypeName", appdto.getMarkerTypeName()== null ? "" :appdto.getMarkerTypeName());
					js.addProperty("name", appdto.getName()== null ? "" :appdto.getName());
					js.addProperty("originalAppId", appdto.getOriginalAppId()== null ? "" :appdto.getOriginalAppId());
					js.addProperty("originalPackId", appdto.getOriginalPackId()== null ? "" :appdto.getOriginalPackId());
					js.addProperty("packAppVer", appdto.getPackAppVer()== null ? "" :appdto.getPackAppVer());
					js.addProperty("packCpId", appdto.getPackCpId()== null ? "" :appdto.getPackCpId());
					js.addProperty("packCpUpdateTime", appdto.getPackCpUpdateTime()== null ? "" :appdto.getPackCpUpdateTime());
					js.addProperty("packFileSize", appdto.getPackFileSize()== null ? "" :appdto.getPackFileSize());
					js.addProperty("packOs", appdto.getPackOs()== null ? "" :appdto.getPackOs());
					js.addProperty("packOsMinVer", appdto.getPackOsMinVer()== null ? "" :appdto.getPackOsMinVer());
					js.addProperty("packUuid", appdto.getPackUuid()== null ? "" :appdto.getPackUuid());
					js.addProperty("pinyin", appdto.getPinyin()== null ? "" :appdto.getPinyin());
					js.addProperty("point", appdto.getPoint());
			//		js.addProperty("pics", appdto.getPics());
					JsonObject json2 = new JsonObject();
					for(int i=0;i<appdto.getPicsUrl().size();i++){
						json2.addProperty("pic"+i, appdto.getPicsUrl().get(i));
					}
					js.add("pics", json2);
					js.addProperty("rscTypeId", appdto.getRscTypeId()== null ? "" :appdto.getRscTypeId());
					js.addProperty("rscTypeName", appdto.getRscTypeName()== null ? "" :appdto.getRscTypeName());
					js.addProperty("rscTypePid", appdto.getRscTypePid()== null ? "" :appdto.getRscTypePid());
					js.addProperty("uuid", appdto.getUuid()== null ? "" :appdto.getUuid());
					js.addProperty("discriminator", appdto.getDiscriminator());
					js.addProperty("downCount", appdto.getDownCount()+ "");
					js.addProperty("installCount", appdto.getInstallCount()+ "");
					json1.add("data", js);
				}else{
					json1.addProperty("success", 20001);
				}
			}catch(Exception e){
				logger.info(e);
				json1.addProperty("success", 20400);
			}
		}else{
			json1.addProperty("success", 20002);
		}
		doPrint(json1.toString());
		return null;
	}

	/**
	 * 搜索
	 * url示例: http://192.168.1.77:8080/wap-phone/api/app/search?os=os1&rsctypeid=1&chan=1&deptId=01&cpIds=cp09&orderBy=01&provid=44&cityid=9000451&index=0&size=10
	 * @param os 操作系统
	 * @param rscTypeId 软件类型
	 * @param keyword 关键字
	 * @param deptId 运营商
	 * @param pinyin 拼音
	 * @param chan 渠道
	 * @param cpIds cp商
	 * @param cpUpdateTime  cp商更新时间
	 * @param orderBy  排序
	 * @param provid 省id
	 * @param cityid 市id
	 * @param index 分页
	 * @param size 分页大小
	 * @return   手动将复杂对象转换为json
	 */
	@GET
	@Path("search")
	@Produces({ MediaType.APPLICATION_JSON })
	public String query(
					@QueryParam("os") String os,
					@QueryParam("rsctypeid") String rscTypeId,
					@QueryParam("keyword") String keyword,
					@QueryParam("deptid") String deptId,
					@QueryParam("pinyin") String pinyin,
					@QueryParam("chan") String chan,
					@QueryParam("cpIds") String cpIds,
					@QueryParam("cpUpdateTime") String cpUpdateTime,
					@QueryParam("orderBy") String orderBy,
					@QueryParam("provid") int provid,
					@QueryParam("cityid") int cityid,
					@QueryParam("index") int index,
					@QueryParam("size") int size) {
		
		JsonObject json1 = new JsonObject();

		if(os !=null && !"".equals(os)){
			Pattern p = Pattern.compile("^os[1,2,3,4,5,6]{1}$"); 
			Matcher m = p.matcher(os); 
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}

		if(deptId !=null && !"".equals(deptId)){
			Pattern p = Pattern.compile("^0[1,2]{1}$");
			Matcher m = p.matcher(deptId); 
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}
		
		List<String> cpList = new ArrayList<String>();
		if(cpIds !=null &&!"".equals(cpIds)){
			String[] cpId = cpIds.split(",");
			for(int i=0;i<cpId.length;i++){
				Pattern p2 = Pattern.compile("^cp0[1,2,3,4,5,6,7,8,9]{1}$"); 
				Matcher m2 = p2.matcher(cpId[i]); 
				if(!m2.matches()){
					json1.addProperty("success", 20002);
					doPrint(json1.toString());
					return null;
				}else{
					cpList.add(cpId[i]);
				}
			}
		}

		if(pinyin !=null && !"".equals(pinyin)){
			Pattern p = Pattern.compile("^[a-zA-Z]*$");
			Matcher m = p.matcher(pinyin);
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}
		
		if(rscTypeId !=null && !"".equals(rscTypeId)){
			Pattern p = Pattern.compile("^[0-9]*$");
			Matcher m = p.matcher(rscTypeId);
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}
		if(orderBy !=null && !"".equals(orderBy)){
			Pattern p = Pattern.compile("^0[1,2]{1}$");
			Matcher m = p.matcher(orderBy); 
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}
			try{
				Date date =  null;
				if(cpUpdateTime !=null && !"".equals(cpUpdateTime)){
					try {
						date = new SimpleDateFormat("yyyy-MM-dd").parse(cpUpdateTime);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				if(deptId != null && !"".equals(deptId)){
					if("01".equals(deptId)){
						deptId = Constants.DEPTID_LIANTONG;
					}else if("02".equals(deptId)){
						deptId = Constants.DEPTID_YIDONG;
					}else{
						throw new IllegalArgumentException("无效的 deptId:" + deptId);
					}
				}else{
					deptId = Constants.DEPTID_LIANTONG;
				}
				Os ostype = Os.ANDROID;
				if(os !=null && !"".equals(os)){
					if("os1".equals(os)){
						ostype = Os.ANDROID;
					}else if("os2".equals(os)){
						ostype = Os.IOS;
					}else if("os3".equals(os)){
						ostype = Os.IOSX;
					}else if("os4".equals(os)){
						ostype = Os.SYMBIAN;
					}else if("os5".equals(os)){
						ostype = Os.JAVA;
					}else if("os6".equals(os)){
						ostype = Os.WINMOBILE;
					}else{
						throw new IllegalArgumentException("无效的操作系统os:" + os);
					}
				}else{
					ostype = Os.ANDROID;
				}
				OrderBy order = OrderBy.cpUpdateTime;
				if(orderBy !=null && !"".equals(orderBy)){
					if("01".equals(orderBy)){
						order = OrderBy.cpUpdateTime;
					}else if("02".equals(orderBy)){
						order = OrderBy.downCount;
					}
				}
				AreaParam area = new AreaParam(deptId,provid,cityid);
				AppParam  appParam = new AppParam(rscTypeId,keyword,pinyin,cpList,date, order);
//				Pager<AppDTO> list = appQuerier.search(area, ostype, appParam, index, size);
				Pager<AppDTO> list = appQuerier.queryPack(area, ostype, appParam, index, size);
				JsonObject json = new JsonObject();
				if(list.getTotalCount() ==0){
					json1.addProperty("success", 20001);
					doPrint(json1.toString());
					return null;
				}else{
					json1.addProperty("success", 20000);
				}
				json.addProperty("totalCount", list.getTotalCount() + "");
				
				JsonArray jsonArray = new JsonArray();
				for(int i=0;i<list.getList().size();i++){
					JsonObject js = new JsonObject();
					js.addProperty("cpid", list.getList().get(i).getCpId()== null ? "" :list.getList().get(i).getCpId());
					js.addProperty("cpname", list.getList().get(i).getCpName()== null ? "" :list.getList().get(i).getCpName());
					js.addProperty("cpUpdateTime", list.getList().get(i).getCpUpdateTime()== null ? "" :list.getList().get(i).getCpUpdateTime());
					js.addProperty("developer", list.getList().get(i).getDeveloper()== null ? "" :list.getList().get(i).getDeveloper());
					js.addProperty("icon", list.getList().get(i).getIconUrl()== null ? "" :list.getList().get(i).getIconUrl());
		//			js.addProperty("info", list.getList().get(i).getInfo()== null ? "" :list.getList().get(i).getInfo());
		//			js.addProperty("infohtml", list.getList().get(i).getInfoHtml()== null ? "" :list.getList().get(i).getInfoHtml());
					js.addProperty("markerType", list.getList().get(i).getMarkerType()== null ? "" :list.getList().get(i).getMarkerType());
					js.addProperty("markerTypeName", list.getList().get(i).getMarkerTypeName()== null ? "" :list.getList().get(i).getMarkerTypeName());
					js.addProperty("name", list.getList().get(i).getName()== null ? "" :list.getList().get(i).getName());
					js.addProperty("originalAppId", list.getList().get(i).getOriginalAppId()== null ? "" :list.getList().get(i).getOriginalAppId());
					js.addProperty("originalPackId", list.getList().get(i).getOriginalPackId()== null ? "" :list.getList().get(i).getOriginalPackId());
					js.addProperty("packAppVer", list.getList().get(i).getPackAppVer()== null ? "" :list.getList().get(i).getPackAppVer());
					js.addProperty("packCpId", list.getList().get(i).getPackCpId()== null ? "" :list.getList().get(i).getPackCpId());
					js.addProperty("packCpUpdateTime", list.getList().get(i).getPackCpUpdateTime()== null ? "" :list.getList().get(i).getPackCpUpdateTime());
					js.addProperty("packFileSize", list.getList().get(i).getPackFileSize()== null ? "" :list.getList().get(i).getPackFileSize());
					js.addProperty("packOs", list.getList().get(i).getPackOs()== null ? "" :list.getList().get(i).getPackOs());
					js.addProperty("packOsMinVer", list.getList().get(i).getPackOsMinVer()== null ? "" :list.getList().get(i).getPackOsMinVer());
					js.addProperty("packUuid", list.getList().get(i).getPackUuid()== null ? "" :list.getList().get(i).getPackUuid());
					js.addProperty("pinyin", list.getList().get(i).getPinyin()== null ? "" :list.getList().get(i).getPinyin());
					js.addProperty("point", list.getList().get(i).getPoint());
		//			js.addProperty("pics", list.getList().get(i).getPics());
					js.addProperty("rscTypeId", list.getList().get(i).getRscTypeId()== null ? "" :list.getList().get(i).getRscTypeId());
					js.addProperty("rscTypeName", list.getList().get(i).getRscTypeName()== null ? "" :list.getList().get(i).getRscTypeName());
					js.addProperty("rscTypePid", list.getList().get(i).getRscTypePid()== null ? "" :list.getList().get(i).getRscTypePid());
					js.addProperty("uuid", list.getList().get(i).getUuid()== null ? "" :list.getList().get(i).getUuid());
					String downUrl = "";
					if(list.getList().get(i).getUuid()!= null && os !=null && !"".equals(os) && cpIds !=null){
						String[] cpId = cpIds.split(",");
						String packuuid = appQuerier.confirmPack(list.getList().get(i).getUuid(), ostype,cpId[0]);
						if(packuuid !=null){
							downUrl = appQuerier.getPackUrl(packuuid);
						}
					}
					js.addProperty("downUrl", downUrl);
					js.addProperty("discriminator", list.getList().get(i).getDiscriminator());
					js.addProperty("downCount", list.getList().get(i).getDownCount()+ "");
					js.addProperty("installCount", list.getList().get(i).getInstallCount()+ "");
					jsonArray.add(js);
				}
				json.add("list", jsonArray);
				json1.add("data", json);
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
				json1.addProperty("success", 20400);
			}
		doPrint(json1.toString());
		return null;
//		return new Gson().toJson(pager);
	}

	/**
	 * 包查询(过滤操作系统)
	 * url示例: http://localhost:8080/ezinstall-web/api/app/pkgApp/42?os=os1&chan=1&index=0&size=10
	 * @param pkgId  软件包uuid
	 * @param os 操作系统
	 * @param chan 渠道
	 * @param index 分页
	 * @param size 分页大小
	 * @return
	 */
	@GET
	@Path("pkgApp/{pkgId}")
	@Produces({MediaType.APPLICATION_JSON })
	public String findByPkg(
						@PathParam("pkgId") int pkgId,
						@QueryParam("os") String os,
						@QueryParam("chan") String chan,
						@QueryParam("index") int index,
						@QueryParam("size") int size) {
		JsonObject json1 = new JsonObject();
		if(os !=null && !"".equals(os)){
			Pattern p = Pattern.compile("^os[1,2,3,4,5,6]{1}$"); 
			Matcher m = p.matcher(os); 
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}
		Os ostype = Os.ANDROID;
		if(os !=null && !"".equals(os)){
			if("os1".equals(os)){
				ostype = Os.ANDROID;
			}else if("os2".equals(os)){
				ostype = Os.IOS;
			}else if("os3".equals(os)){
				ostype = Os.IOSX;
			}else if("os4".equals(os)){
				ostype = Os.SYMBIAN;
			}else if("os5".equals(os)){
				ostype = Os.JAVA;
			}else if("os6".equals(os)){
				ostype = Os.WINMOBILE;
			}else{
				throw new IllegalArgumentException("无效的操作系统os:" + os);
			}
		}else{
			ostype = Os.ANDROID;
		}
		
		try{
			Pager<AppDTO> list= appQuerier.findInstallPackByPkg(pkgId,ostype,index,size);
			
			JsonObject json = new JsonObject();
			if(list.getTotalCount() ==0){
				json1.addProperty("success", 20001);
				doPrint(json1.toString());
				return null;
			}else{
				json1.addProperty("success", 20000);
			}
			json.addProperty("totalCount", list.getTotalCount() + "");
			JsonArray jsonArray = new JsonArray();
			for(int i=0;i<list.getList().size();i++){
				JsonObject js = new JsonObject();
				js.addProperty("cpid", list.getList().get(i).getCpId()== null ? "" :list.getList().get(i).getCpId());
				js.addProperty("cpname", list.getList().get(i).getCpName()== null ? "" :list.getList().get(i).getCpName());
				js.addProperty("cpUpdateTime", list.getList().get(i).getCpUpdateTime()== null ? "" :list.getList().get(i).getCpUpdateTime());
				js.addProperty("developer", list.getList().get(i).getDeveloper()== null ? "" :list.getList().get(i).getDeveloper());
				js.addProperty("icon", list.getList().get(i).getIconUrl()== null ? "" :list.getList().get(i).getIconUrl());
	//			js.addProperty("info", list.getList().get(i).getInfo()== null ? "" :list.getList().get(i).getInfo());
	//			js.addProperty("infohtml", list.getList().get(i).getInfoHtml()== null ? "" :list.getList().get(i).getInfoHtml());
				js.addProperty("markerType", list.getList().get(i).getMarkerType()== null ? "" :list.getList().get(i).getMarkerType());
				js.addProperty("markerTypeName", list.getList().get(i).getMarkerTypeName()== null ? "" :list.getList().get(i).getMarkerTypeName());
				js.addProperty("name", list.getList().get(i).getName()== null ? "" :list.getList().get(i).getName());
				js.addProperty("originalAppId", list.getList().get(i).getOriginalAppId()== null ? "" :list.getList().get(i).getOriginalAppId());
				js.addProperty("originalPackId", list.getList().get(i).getOriginalPackId()== null ? "" :list.getList().get(i).getOriginalPackId());
				js.addProperty("packAppVer", list.getList().get(i).getPackAppVer()== null ? "" :list.getList().get(i).getPackAppVer());
				js.addProperty("packCpId", list.getList().get(i).getPackCpId()== null ? "" :list.getList().get(i).getPackCpId());
				js.addProperty("packCpUpdateTime", list.getList().get(i).getPackCpUpdateTime()== null ? "" :list.getList().get(i).getPackCpUpdateTime());
				js.addProperty("packFileSize", list.getList().get(i).getPackFileSize()== null ? "" :list.getList().get(i).getPackFileSize());
				js.addProperty("packOs", list.getList().get(i).getPackOs()== null ? "" :list.getList().get(i).getPackOs());
				js.addProperty("packOsMinVer", list.getList().get(i).getPackOsMinVer()== null ? "" :list.getList().get(i).getPackOsMinVer());
				js.addProperty("packUuid", list.getList().get(i).getPackUuid()== null ? "" :list.getList().get(i).getPackUuid());
				String downUrl = "";
				if(list.getList().get(i).getPackUuid()!= null ){
					downUrl = appQuerier.getPackUrl(list.getList().get(i).getPackUuid());
				}
				js.addProperty("downUrl", downUrl);
				js.addProperty("pinyin", list.getList().get(i).getPinyin()== null ? "" :list.getList().get(i).getPinyin());
				js.addProperty("point", list.getList().get(i).getPoint());
	//			js.addProperty("pics", list.getList().get(i).getPics());
				js.addProperty("rscTypeId", list.getList().get(i).getRscTypeId()== null ? "" :list.getList().get(i).getRscTypeId());
				js.addProperty("rscTypeName", list.getList().get(i).getRscTypeName()== null ? "" :list.getList().get(i).getRscTypeName());
				js.addProperty("rscTypePid", list.getList().get(i).getRscTypePid()== null ? "" :list.getList().get(i).getRscTypePid());
				js.addProperty("uuid", list.getList().get(i).getUuid()== null ? "" :list.getList().get(i).getUuid());
				js.addProperty("discriminator", list.getList().get(i).getDiscriminator());
				js.addProperty("downCount", list.getList().get(i).getDownCount()+ "");
				js.addProperty("installCount", list.getList().get(i).getInstallCount()+ "");
				js.addProperty("banner", list.getList().get(i).getBannerUrl()+ "");
				jsonArray.add(js);
			}
			json.add("list", jsonArray);
			json1.add("data", json);
		}catch(Exception e){
			logger.error(e);
			json1.addProperty("success", 20400);
		}
		doPrint(json1.toString());
		return null;
	}
	
	
	
	/**
	 * 包组合查询(过滤操作系统)
	 * url示例: http://localhost:8080/wap-phone/api/app/pkgGroup/42
	 * @param pkgId  软件包uuid
	 * @param os 操作系统
	 * @param chan 渠道
	 * @param index 分页
	 * @param size 分页大小
	 * @return
	 */
	@GET
	@Path("pkgGroup/{pkgGroupId}")
	@Produces({MediaType.APPLICATION_JSON })
	public String findByPkgGroup(
						@PathParam("pkgGroupId") int pkgId) {
		JsonObject json = new JsonObject();
		try{
			List<Pkg> list= wapService.findPkgList(pkgId);
			JsonArray jsonArray = new JsonArray();
			for(int i=0;i<list.size();i++){
				JsonObject js = new JsonObject();
				js.addProperty("id", list.get(i).getId()== null ? "" :list.get(i).getId().toString());
				js.addProperty("name", list.get(i).getName()== null ? "" :list.get(i).getName());
				js.addProperty("info", list.get(i).getInfo()== null ? "" :list.get(i).getInfo());
				js.addProperty("groupId", list.get(i).getGroupId());
				js.addProperty("num", list.get(i).getNum());
				js.addProperty("icon",   list.get(i).getIcon()== null ? "" :config.getFileServerUrl() +list.get(i).getIcon());
				js.addProperty("largeIcon", list.get(i).getLargeIcon()== null ? "" :config.getFileServerUrl() +list.get(i).getLargeIcon());
				jsonArray.add(js);
			}
			json.addProperty("success", 20000);
			json.addProperty("desc", "成功");
			json.add("data", jsonArray);
		}catch(Exception e){
			logger.info(e);
			json.addProperty("success", 20400);
		}
		doPrint(json.toString());
		return null;
	}
	
	/**
	 * 根据安装包uuid获取下载地址
	 * url示例: http://localhost:8080/wap/api/app/pack/dl/1870477c-cdc0-4b0a-b302-3f6824eb1551?chan=1
	 * @param uuid 安装包uuid
	 * @param chan 渠道
	 * @return
	 */
	@GET
	@Path("pack/dl/{uuid}")
	@Produces({MediaType.APPLICATION_JSON })
	public String findDownByPkg(
						@PathParam("uuid") String uuid,
						@QueryParam("chan") String chan) {
		JsonObject json1 = new JsonObject();
		String downUrl ="";
		try {
			App count = restSourceDao.finddowncount(uuid);
			int c = count.getDownCount();
			c++;
			restSourceDao.saveOrUpdatedowncount(c, count.getUuid());
			downUrl = appQuerier.getPackUrl(uuid);
			if(downUrl !=null && !"".equals(downUrl)){
				JsonObject json = new JsonObject();
				json.addProperty("url", downUrl);
				json1.addProperty("success", 20000);
				json1.add("data", json);
			}else{
				json1.addProperty("success", 20001);
			}
		} catch (CpServerAccessException e) {
			logger.info(e);
			json1.addProperty("success", 20400);
		}
		doPrint(json1.toString());
		return null;
	}
	
	/**
	 * 根据APP软件appId、cp商的cpIds 、操作系统os获取安装包uuid
	 * url示例: http://localhost:8080/wap/api/app/pack/dl?appId=11621da6-7c9e-4b05-9e9f-996d8a982b0a&chan=1&os=os1&cpIds=cp03
	 * @param appId APP软件id
	 * @param cpIds cp商
	 * @param os 操作系统
	 * @param chan 渠道
	 * @return
	 */
	@GET
	@Path("pack/dl")
	@Produces({MediaType.APPLICATION_JSON })
	public String findDownByAppIdAndOs(
						@QueryParam("appId") String appId,
						@QueryParam("cpIds") String cpIds,
						@QueryParam("os") String os,
						@QueryParam("chan") String chan) {
		JsonObject json1 = new JsonObject();
		if(os !=null && !"".equals(os)){
			Pattern p = Pattern.compile("^os[1,2,3,4,5,6]{1}$"); 
			Matcher m = p.matcher(os); 
			if(!m.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}
		
		if(cpIds !=null &&!"".equals(cpIds)){
			Pattern p2 = Pattern.compile("^cp0[1,2,3,4,5,6,7,8,9]{1}$"); 
			Matcher m2 = p2.matcher(cpIds); 
			if(!m2.matches()){
				json1.addProperty("success", 20002);
				doPrint(json1.toString());
				return null;
			}
		}
		Os ostype = Os.ANDROID;
		if(os !=null && !"".equals(os)){
			if("os1".equals(os)){
				ostype = Os.ANDROID;
			}else if("os2".equals(os)){
				ostype = Os.IOS;
			}else if("os3".equals(os)){
				ostype = Os.IOSX;
			}else if("os4".equals(os)){
				ostype = Os.SYMBIAN;
			}else if("os5".equals(os)){
				ostype = Os.JAVA;
			}else if("os6".equals(os)){
				ostype = Os.WINMOBILE;
			}else{
				throw new IllegalArgumentException("无效的操作系统os:" + os);
			}
		}else{
			ostype = Os.ANDROID;
		}
		String downUrl = appQuerier.confirmPack(appId, ostype,cpIds);
//		return new Gson().toJson(downUrl);
		if(downUrl !=null && !"".equals(downUrl)){
			json1.addProperty("success", 20000);
			JsonObject json = new JsonObject();
			json.addProperty("uuid", downUrl);
			json1.add("data", json);
		}else{
			json1.addProperty("success", 20001);
		}

		doPrint(json1.toString());
		return null;
	}
	
	
	/**
	 * 对浏览器的请求返回字符串
	 * @param s 要输出的字符串
	 * @throws IOException
	 */
	public String doPrint(Object obj)	{
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		PrintWriter pw;
		try {
			pw =ServletActionContext.getResponse().getWriter();
			pw.println(obj.toString());
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
