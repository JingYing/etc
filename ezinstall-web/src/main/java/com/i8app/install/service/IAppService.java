package com.i8app.install.service;

import java.util.List;

import com.i8app.install.Pager;
import com.i8app.install.PhoneOs;

/**
 * 游戏表和应用表共同使用的接口
 * 
 * @author JingYing 2012-11-14
 */
public interface IAppService {
	
	/**
	 * 更新下载数
	 * @param <T>
	 * @param app
	 */
	<T> void updateDownnum(T app);
	
	/**
	 * 根据app生成该软件的一条下载记录日志
	 * @param <T>
	 * @param app
	 */
	<T> void persistInstallLog(T app);
	
	/**
	 * 根据ID查找
	 * @param <T>
	 * @param id
	 * @return
	 */
	<T> T findById(int id);
	
	/**
	 * 适配查找
	 * @param <T>
	 * @param phoneOs
	 * @return
	 */
	<T> List<T> find(PhoneOs phoneOs);
	
	/**
	 * 适配分页查找
	 * @param <T>
	 * @param phoneOs
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	<T> List<T> find(PhoneOs phoneOs, int pageno, int pagesize);
	
	/**
	 * 适配搜索
	 * @param <T>
	 * @param phoneOs
	 * @param keyword
	 * @return
	 */
	<T> List<T> search(PhoneOs phoneOs, String keyword);

	/**
	 * 适配分页搜索
	 * @param <T>
	 * @param phoneOs
	 * @param keyword
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	<T> List<T> search(PhoneOs phoneOs, String keyword, int pageno, int pagesize);
	

	/**
	 * 应用和游戏公用
	 * @param <T>
	 * @param phoneOs
	 * @param keyword
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
//	<T> List<T> searchappandgame(PhoneOs phoneOs, String keyword, int pageno, int pagesize);
	
	
	/**
	 * 根据typeid分页搜索
	 * @param <T>
	 * @param phoneOs
	 * @param appTypeid
	 * @param offset
	 * @param pagesize
	 * @return
	 */
	<T> Pager<T> byTypeid(PhoneOs phoneOs, int appTypeid, int offset, int pagesize);
	
	/**
	 * 类型列表
	 * @param <T>
	 * @return
	 */
	<T> List<T> typeList();

}
