package com.i8app.install.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.install.dao.BizAreaDao;
import com.i8app.install.dao.PkgDao;
import com.i8app.install.domain.Pkg;
import com.i8app.install.domain.PkgGroup;

@Component
public class WapService{
	
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private BizAreaDao bizAreaDao;
	@Resource
	private PkgDao pkgDao;
	/**
	 * 全量查询, 包括以下条件,(版本适配, 资源类型,软件名关键字,拼音首字母, 过滤区域定制, 取特定CP商)
	 * 根据返回的appDTO.uuid, 再调用getDownloadUrl(String appUuid, Os os, String cpId)获取下载地址
	 * @param areaParam 必须带有区域信息, 防止出现其它区域专属软件
	 * @param os 为null时查所有操作系统的软件
	 * @param appParam app的查询参数
	 * @param offset 分页参数, 例( LIMIT X, Y), 则offset为X
	 * @param pageSize 分页参数, 例( LIMIT X, Y), 则pageSize为Y
	 * @return 
	 */
	public Pager<AppDTO> query(AreaParam areaParam,Os os,AppParam appParam,int offset,int pageSize){
		return appQuerier.query(areaParam, os, appParam, offset, pageSize);
	}
	/**
	 * 查看单个软件的详细信息. 以app信息为主.
	 * @param appId
	 * @param Os 不能为空, 防止出现不适配的软件包的信息(包大小,系统,更新日期,cp商)
	 * @return 多了"截图"列表
	 */
	public AppDTO findSingle(String appId, Os os){
		return appQuerier.findSingle(appId, os);
	}
	/**
	 * 查询当前区域所参加的活动, 手机必备组合, 榜单组合, 强制安装组合等业务<br>
	 * 如果查询该区域下的活动时, PKGGROUP可能有多个, 代表该区域同时参加多个活动<br>
	 * 查询活动以外的业务时, 一般只有一个PKGGROUP
	 * @param areaParam
	 * @param bizId 见AppQuerier.Biz_****的取值
	 * @return 
	 */
	public List<PkgGroup> findPkgGroup(int bizId, AreaParam areaParam){
		return bizAreaDao.findByBizId(bizId, areaParam);
	}
	/**
	 * 根据软件包组合ID, 查询组合下面的包列表
	 * 
	 * @return 
	 */
	public List<Pkg> findPkgList(int pkgGroupId){
		return pkgDao.findByGroupId(pkgGroupId);
	}
	/**
	 * 查询包下的软件列表, 分页
	 * 由于包下面既能指定APP, 又能指定PACK, 所以在APPDTO中,需要判断discriminator:为1时取appDTO.uuid, 
	 * 再根据getDownloadUrl(String appUuid, Os os, String cpId)取下载地址, 
	 * 为2时取appDTO.packUuid,再根据getDownloadUrl(String packUuid)取下载地址 
	 * 
	 * @param os 为null时查所有操作系统的软件
	 * @param pkgId 软件包的id
	 * @param offset 分页参数, 例"SELECT * FROM TABLE LIMIT X, Y", 则offset为X
	 * @param pageSize 分页参数, 例"SELECT * FROM TABLE LIMIT X, Y", 则pageSize为Y
	 * @return  
	 */
//	public Pager<AppDTO> findAppByPkg(Os os, int pkgId, List<String> cpIds, int offset, int pageSize){
//		return appQuerier.findAppByPkg(os, pkgId,cpIds, offset, pageSize);
//	}

	public Pager<AppDTO> findInstallPackByPkg(AreaParam areaParam, int pkgId, Os os, int offset, int pageSize){
		return appQuerier.findInstallPackByPkg(areaParam, pkgId, os, offset, pageSize);
	}
	
	public AppDTO findSinglePack(String packUuid){
		return appQuerier.findSinglePack(packUuid);
	}

	/**
	 * 根据APPID, OS, CPID, 确定唯一安装包
	 * @param appId APP的ID
	 * @param os 操作系统, 不能为NULL
	 * @param cpId CP商ID, 可以为NULL
	 * @return 唯一安装包的ID, 为null时代表未找到符合条件的安装包
	 */
	public String confirmPack(String appId, Os os, String cpId){
		return appQuerier.confirmPack(appId, os, cpId);
	}
}
