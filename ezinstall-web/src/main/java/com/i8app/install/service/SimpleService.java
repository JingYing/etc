package com.i8app.install.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.install.dao.ShortUrlDao;
import com.i8app.install.domain.ShortUrl;

/**
 * 简单未归类的查询
 * 
 * @author JingYing 2013-6-7
 */
@Service
public class SimpleService {

	@Resource
	private ShortUrlDao shortUrlDao;
	
	public ShortUrl findShortUrl(String shortUrlId) {
		return shortUrlDao.findById(shortUrlId);
	}
	
 
}
