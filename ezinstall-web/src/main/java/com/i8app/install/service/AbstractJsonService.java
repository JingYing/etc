package com.i8app.install.service;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.i8app.install.JsonSerializable;
import com.i8app.install.Pager;
import com.i8app.install.domain.GameInfo;
import com.i8app.install.domain.SoftInfo;

/**
 * json拼接类. 主要生成JSON主树干, 具体JSON属性由子类实现
 * 
 * @author JingYing 2012-11-14
 */
public abstract class AbstractJsonService {
	
	/**
	 * 
	 * @param <T>
	 * @param pager
	 * @return
	 */
	public <T> JsonObject template1(Pager<T> pager)	{
		JsonObject resultVal = new JsonObject();
		resultVal.addProperty("errorcode", 0);
		resultVal.addProperty("desc", "");
		resultVal.add("list", this.appListToJson(pager.getList()));
		resultVal.addProperty("total", pager.getTotal());
		
		JsonObject result = new JsonObject();
		result.add("result", resultVal);
		return result;
	}
	
	/**
	 * 生成如下字符串
	 * {"result":{"errorcode":???,"desc":"?????"}} 
	 * @param msg
	 * @return
	 */
	public JsonObject template1_fail(int errorcode, String msg)	{
		JsonObject resultVal = new JsonObject();
		resultVal.addProperty("errorcode", false);
		resultVal.addProperty("desc", msg);
		
		JsonObject result = new JsonObject();
		result.add("result", resultVal);
		return result;
	}
	
	
	/**
	 * 生成如下字符串
	 * {"errorcode":0,"desc":"","result":???} 
	 * @param <T>
	 * @param list
	 * @return
	 */
	public <T extends JsonSerializable> JsonObject template2(List<T> list)	{
		JsonObject resultVal = new JsonObject();
		resultVal.addProperty("errorcode", 0);
		resultVal.addProperty("desc", "");
		resultVal.add("result", toJsonSeriList(list));
		
		JsonObject result = new JsonObject();
		result.add("result", resultVal);
		return result;
	}
	
	/**
	 * 生成如下字符串
	 * {"errorcode":???,"desc":"???"} 
	 * @param msg
	 * @return
	 */
	public JsonObject template2_fail(int errorcode, String msg)	{
		JsonObject resultVal = new JsonObject();
		resultVal.addProperty("errorcode", false);
		resultVal.addProperty("desc", msg);
		return resultVal;
	}
	
	public <T extends JsonSerializable> JsonElement toJsonSeriList(List<T> list)	{
		JsonArray array = new JsonArray();
		for(T t : list)	{
			JsonSerializable j = (JsonSerializable)t;
			array.add(j.toJsonObject());
		}
		return array;
	}
	
	public <T> JsonElement appListToJson(List<T> appList)	{
		JsonArray array = new JsonArray();
		for(T t : appList)	{
			if(!(t instanceof SoftInfo) && !(t instanceof GameInfo))
				throw new IllegalArgumentException();
			array.add(appToJson(t));
		}
		return array;
	}
	
	protected abstract <T> JsonObject appToJson(T app);
	

}
