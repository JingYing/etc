package com.i8app.install.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.TempDriverDao;
import com.i8app.install.domain.TempDriverInfo;

@Component("tempDriverService")
public class TempDriverService {

	private TempDriverDao tempDriverDao;
	
	// 将上传的文件保存到项目路径下哪个文件夹里
	private String fileChildPath = "driver";

	public void addOrUpdate(TempDriverInfo tempDriverInfo) {
		tempDriverDao.addOrUpdate(tempDriverInfo);
	}

	/**
	 * 写入磁盘并存入数据库
	 * @param vid
	 * @param pid
	 * @param httpRootPath
	 * @param realRootPath
	 * @param is
	 * @return
	 */
	public String saveAndWrite(String vid, String pid, String httpRootPath,
			String realRootPath, InputStream is) {
		// 文件名的自动生成规则 : vid+pid+时间戳+.zip
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String timestamp = sdf.format(new Date());
		String fileName = String.format("%s_%s_%s.zip", vid, pid, timestamp);
		String realfileFullName = String.format("%s%s\\%s", realRootPath,
				fileChildPath, fileName);
		String httpFileFullName = String.format("%s%s/%s", httpRootPath,
				fileChildPath, fileName);
		File file = new File(realfileFullName);
		if (!file.getParentFile().exists())
			file.getParentFile().mkdirs();

		String message = null;
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		int i;
		bis = new BufferedInputStream(is);

		try {
			bos = new BufferedOutputStream(new FileOutputStream(
					realfileFullName));
			while ((i = bis.read()) != -1) {
				bos.write(i);
			}

			// 存入数据库
			TempDriverInfo ur = new TempDriverInfo();
			ur.setPid(pid);
			ur.setVid(vid);
			ur.setFileName(httpFileFullName);

			// 调用 Dao
			// uploadRecordDao.save(ur);

			message = "{\"status\":\"success\"}";
		} catch (Exception e) {
			message = "{\"status\":\"failed\"}";
			e.printStackTrace();
		} finally {
			if (bis != null) {
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return message;
	}

	/**
	 * 
	 * @param httpRootPath
	 * @param realRootPath
	 * @param is
	 * @return
	 */
	public String writeToDisk(String realRootPath, InputStream is) {

		// 文件名的自动生成规则 : 时间戳.zip
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String timestamp = sdf.format(new Date());
		String fileName = String.format("%s.zip", timestamp);

		String realfileFullName = String.format("%s%s\\%s", realRootPath,
				fileChildPath, fileName);
		File file = new File(realfileFullName);
		if (!file.getParentFile().exists())
			file.getParentFile().mkdirs();

		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		int i;
		bis = new BufferedInputStream(is);

		try {
			bos = new BufferedOutputStream(new FileOutputStream(
					realfileFullName));
			while ((i = bis.read()) != -1) {
				bos.write(i);
			}
			return fileName;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * 传入项目的HTTP路径和文件名, 返回该文件的网络地址全名
	 * 
	 * @param httpRootPath
	 * @param fileName
	 * @return
	 */
	public String changeToFullPath(String httpRootPath, String fileName) {
		String httpFileFullName = String.format("%s%s/%s.zip", httpRootPath,
				fileChildPath, fileName);
		return httpFileFullName;
	}

	@Resource
	public void setTempDriverDao(TempDriverDao tempDriverDao) {
		this.tempDriverDao = tempDriverDao;
	}
	
	
	
}
