package com.i8app.install.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.SoftInfoDao;
import com.i8app.install.domain.Tongyong;
@Component
public class SoftService {
	@Resource
	private SoftInfoDao softInfoDao;
	/**
	 * 山西移动手机客户端 精彩推荐接口
	 * @param <T>
	 * @param areacode 区域ID
	 * @param dtId 数据ID
	 * @param pageNo 页码
	 * @param pageSize 页面大小
	 * @return
	 */
	public List<Tongyong> listSoftByShortName(String areacode, Integer dtId,Integer osType,Integer osId,int pageNo,int pageSize){
		return softInfoDao.listSoftByShortName(areacode, dtId,osType ,osId,pageNo, pageSize);
	}
	public Tongyong listSoftByShortNamecount(String areacode, Integer dtId,Integer osType,Integer osId){
		return softInfoDao.listSoftByShortNamecount(areacode, dtId,osType,osId);
	}
	public List<Tongyong> listgameByShortName(String areacode, Integer dtId,Integer osType,Integer osId,int pageNo,int pageSize){
		return softInfoDao.listgameByShortName(areacode, dtId,osType ,osId,pageNo, pageSize);
	}
	public Tongyong listgameByShortNamecount(String areacode, Integer dtId,Integer osType,Integer osId){
		return softInfoDao.listgameByShortNamecount(areacode, dtId,osType,osId);
	}
	//应用最新top100
	public List<Tongyong> listSoftByNew(Integer osType,Integer osId,int pageNo,int pageSize){
		return softInfoDao.listSoftByNew(osType, osId, pageNo, pageSize);
	}
	//游戏最新top100
	public List<Tongyong> listgameByNew(Integer osType,Integer osId,int pageNo,int pageSize){
		return softInfoDao.listgameByNew(osType, osId, pageNo, pageSize);
	}
	//应用更新提示
	public List<Tongyong> listSoftByUpdate(Integer osType,Integer osId,String name){
		return softInfoDao.listSoftByUpdate(osType, osId, name);
	}
	//游戏更新提示
	public List<Tongyong> listgameByUpdate(Integer osType,Integer osId,String name){
		return softInfoDao.listgameByUpdate(osType, osId, name);
	}
	
	public List<Tongyong> listgameandapp(Integer osType,Integer osId,String name,int pageNo,int pageSize){
		return softInfoDao.listgameandapp(osType, osId, name, pageNo, pageSize);
	}
	
}
