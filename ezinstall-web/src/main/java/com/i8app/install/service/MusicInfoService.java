package com.i8app.install.service;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.install.Config;
import com.i8app.install.dao.MusicInfoDao;
import com.i8app.install.domain.MusicInfo;

@Component("musicInfoService")
public class MusicInfoService{
	
	/**
	 *枚举变量, 定义要上传的文件类型是什么
	 */
	public static enum FileType{音乐, 截图};
	
	/**
	 * 日期格式转换器, 将日期转换为yyyyMMdd格式
	 */
	public static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyyMMdd");

	
	private MusicInfoDao musicInfoDao;
	
	private String HTTP_PATH;
	
	public MusicInfo findById(int id) {
		return musicInfoDao.findById(id);
	}

	public List<MusicInfo> list() {
		return musicInfoDao.list();
	}
	
	public List<MusicInfo> list(int pageNo, int pageSize) {
		return musicInfoDao.list(pageNo, pageSize);
	}
	
	public String listJson()	{
		return this.changeToJson(this.list());
	}
	
	public String listJson(int pageNo, int pageSize)	{
		return this.changeToJson(this.list(pageNo, pageSize));
	}
	
	public int count()	{
		return musicInfoDao.count();
	}
	
	/**
	 * 将List<MusicInfo>展示为Json格式
	 * @param musicInfoList MusicInfo类的List
	 * @return JSON字符串
	 * @author jingying
	 */
	public String changeToJson(List<MusicInfo> list)	{
		JsonArray jsonArray = new JsonArray();
		for(MusicInfo m : list)	{
			JsonObject json = new JsonObject();
			json.addProperty("id", m.getId() + "");
			json.addProperty("musicName", m.getMusicName() + "");
			json.addProperty("musicFileSize", m.getMusicFileSize() + "");
			json.addProperty("musicLen", m.getMusicLen() + "");
			json.addProperty("singerName", m.getSingerName() + "");
			json.addProperty("specialName", m.getSpecialName() + "");
			json.addProperty("musicType", m.getMusicType() == null ? "" : m.getMusicType().getTypeName() + "");
			json.addProperty("suffix", m.getSuffix() == null ? "" : m.getSuffix().getSuffixName() + "");
			
			String s1 = m.getMusicFile();
			if(s1 == null || "".equals(s1.trim()))
				json.addProperty("musicFile", "");
			else
				json.addProperty("musicFile", HTTP_PATH + s1.replace("\\", "/"));
			
			json.addProperty("desc", m.getDesc() + "");

			String s2 = m.getMusicImgFile();
			if(s2 == null || "".equals(s2.trim()))
				json.addProperty("musicImgFile", "");
			else
				json.addProperty("musicImgFile", HTTP_PATH + s2.replace("\\", "/"));
			
			json.addProperty("dateAdded", m.getDateAdded() + "");
			json.addProperty("totalDownloadNum", m.getTotalDownloadNum() + "");
			json.addProperty("WeekDownloadNum", m.getWeekDownloadNum() + "");
			json.addProperty("MonthDownloadNum", m.getMonthDownloadNum() + "");
			json.addProperty("HotDegree", m.getHotDegree() + "");
			json.addProperty("UserRemarkNum", m.getUserRemarkNum() + "");
			json.addProperty("UserRemark", m.getUserRemark() + "");
			json.addProperty("memo", m.getMemo() + "");
			jsonArray.add(json);
		}
		return jsonArray.toString().replace("null", "");
	}
	
	@Resource
	public void setMusicInfoDao(MusicInfoDao musicInfoDao) {
		this.musicInfoDao = musicInfoDao;
	}
	
	@Resource
	public void setConfig(Config config)	{
		this.HTTP_PATH = config.getFileServerUrl();
	}

}
