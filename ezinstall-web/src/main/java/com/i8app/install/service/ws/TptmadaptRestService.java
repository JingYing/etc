package com.i8app.install.service.ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.Constants.OrderBy;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.install.domain.App;
import com.i8app.install.domain.Pkg;
import com.i8app.install.domain.Resource_type;
import com.i8app.install.service.TptmadaptService;

/**
 * @author LXY 2014-04-08
 * 
 */
@Path("/tpt")
@Component
@Scope("request")
@Singleton
public class TptmadaptRestService {
	private static Logger logger = Logger.getLogger(TptmadaptRestService.class);
	@Resource
	private TptmadaptService tptmadaptService;
	/**
	 * 软件分类
	 * url示例: http://localhost:8080/wap/api/app/type?chan=xx
	 * 利用@XmlRootElement自动生成app的json串
	 * @param chan 渠道
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@GET
	@Path("type")
	@Produces({ MediaType.APPLICATION_JSON })
	public String findSourceType(@QueryParam("chan") String chan) {
		JsonObject json1 = new JsonObject();
		try{
		}catch(Exception e){
			logger.info(e);
			json1.addProperty("success", "20400");
		}
		doPrint(json1.toString());
		return null;
	}
	/**
	 * 对浏览器的请求返回字符串
	 * @param s 要输出的字符串
	 * @throws IOException
	 */
	public String doPrint(Object obj)	{
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		PrintWriter pw;
		try {
			pw =ServletActionContext.getResponse().getWriter();
			pw.println(obj.toString());
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
