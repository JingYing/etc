package com.i8app.install.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.install.Pager;
import com.i8app.install.Util;
import com.i8app.install.dao.PaymentDao;
import com.i8app.install.domain.CommisionBalance;
import com.i8app.install.domain.CommisionWithdrawal;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.Requisition;

/**
 * @author LiXiangyang 2014-3-5
 */
@Service
public class PaymentService {

	@Resource
	private PaymentDao paymentDao;
	
	public List<Employee> findByEmpno(String empNo) {
		return paymentDao.findByEmpno(empNo);
	}
	public Employee findByEmpno(String empNo,int pro) {
		return paymentDao.findByEmpno(empNo, pro);
	}
	public Employee findByEmp(String empNo) {
		return paymentDao.findByEmp(empNo);
	}
	public Pager<CommisionWithdrawal> pagerlist(int empId,String year,String month,int pageNo,int pageSize) {
		return paymentDao.pagerlist(empId, year, month, pageNo, pageSize);
	}
/*
	public Pager<CommisionAccount> pagerlist(int empId,String year,String month,int pageNo,int pageSize) {
		return paymentDao.pagerlist(empId, year, month, pageNo, pageSize);
	}
 */
	
	public Requisition findByPerson(int empid) {
		return paymentDao.findByPersion(empid);
	}
	public Requisition findByPersonnum(final int empid) {
		return paymentDao.findByPersionnum(empid);
	}
	public CommisionBalance findBalanceByEmpId(Integer i) {
		return paymentDao.findBalanceByEmpId(i);
	}
	public int saveOrUpdate(Requisition requisition) {
		return paymentDao.saveOrUpdate(requisition);
	}
	public void updatePassword(Requisition requisition) {
		paymentDao.updateNewPassword(requisition);;
	}
	public int insertWithdrawal(Employee emp1, CommisionWithdrawal cw) {
		if(!cw.getApproveNote().equalsIgnoreCase(getCheck(cw))){
			return 3;
		}
		CommisionWithdrawal w = getNewCommisionWithdrawal(cw);
		if (w == null) {
			return 3;
		}
		w.setId(Util.createTimes("yyyyMMddHHmmss")+emp1.getEmpID());
		w.setEmpid(emp1.getEmpID());
		w.setApply_amount(cw.getApply_amount().setScale(2, BigDecimal.ROUND_DOWN));
		w.setApplyTime(new Timestamp(System.currentTimeMillis()));
		
		w.setApproveStatus(0);//0为未批复状态
		w.setApproveNote("正在审核中。");
		w.setBookkeepingStatus(0);//0为未记账状态
		paymentDao.insertWithdrawal(w);
		return 1;
	}
	private CommisionWithdrawal getNewCommisionWithdrawal(CommisionWithdrawal cw) {
		CommisionWithdrawal c = new CommisionWithdrawal();
		c.setApply_amount(cw.getApply_amount().setScale(2, BigDecimal.ROUND_DOWN));
		c.setApply_amount_fee(getFee(c.getApply_amount()));
		c.setApply_amount_tax(getTax(cw.getApply_amount()));
		if((c.getApply_amount().compareTo(cw.getApply_amount())!=0)){
			return null;
		}
		if((c.getApply_amount_fee().compareTo(cw.getApply_amount_fee())!=0)){
			return null;
		}
		if((c.getApply_amount_tax().compareTo(cw.getApply_amount_tax())!=0)){
			return null;
		}
		return c;
	}
	private BigDecimal getFee(BigDecimal apply_amount) {
		BigDecimal fee = apply_amount.multiply(new BigDecimal("0.005")).setScale(2, BigDecimal.ROUND_HALF_UP);
		if(fee.compareTo(new BigDecimal("1.00"))<=0){
			fee = new BigDecimal("1.00");
		}else if(fee.compareTo(new BigDecimal("25.00"))>=0){
			fee = new BigDecimal("25.00");
		}
		return fee;
	}
	private BigDecimal getTax(BigDecimal apply_amount) {
		BigDecimal tax;
		if(apply_amount.compareTo(new BigDecimal("800.00"))<=0){
			tax = new BigDecimal("0.00");
		}else if(apply_amount.compareTo(new BigDecimal("4000.00"))<=0){
			tax = apply_amount.multiply(new BigDecimal("0.20")).setScale(2, BigDecimal.ROUND_HALF_UP);
		}else{
			tax = apply_amount.multiply(new BigDecimal("0.40")).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		return tax;
	}
	/**
	 * hex_md5(''+apply_amount+tax+fee+allMoney+_);
	 * @param cw
	 * @return
	 */
	private String getCheck(CommisionWithdrawal cw) {
		StringBuilder sb = new StringBuilder("");
		sb.append(cw.getApply_amount())
		.append(cw.getApply_amount_tax())
		.append(cw.getApply_amount_fee())
		.append(cw.getBookkeepingRemaining())
		.append(cw.getEmpid());
		return Util.toMD5(sb.toString());
	}
	public CommisionWithdrawal checkWithdrawal(Employee emp1, String money) {
		return paymentDao.findWithdrawalThisMonthByEmpId(emp1.getEmpID());
	}
}
