package com.i8app.install.web.action;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.ezinstall.packreader.apk1.ApkMetaData;
import com.i8app.ezinstall.packreader.apk1.ApkReader;
import com.i8app.install.PhoneOs;
import com.i8app.install.Util;
import com.i8app.install.domain.Tongyong;
import com.i8app.install.service.SoftService;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class SoftAction extends BaseAction{
	private int daId,osType,typeId,pageNo,pageSize;
	private String ver,name,packageName;
	private SoftService softService;
	@Resource
	public void setSoftService(SoftService softService) {
		this.softService = softService;
	}

	//用于山西移动手机客户端精彩推荐接口//
	/*
	 * 数据类型daId,规则1、应用 2、游戏 3、音乐 4、视频 
	 * 软件类型typeId,
	 * 操作系统osType, 1. Symbian 2. android 31. iphone破 33. iPhone正 4. windowsMobile  5. java
	 * 操作系统版本ver  (针对iphone和ANDORID系统的版本号)适配
	 * 页码pageNo,
	 * 页面大小pageSize
	 */
	public String excitingRecommended()throws Exception{
		int osId=0;
		if(pageNo<1){
			pageNo=1;
		}
		if(daId==1){
			daId=50001; //软件
		}else if(daId==2){
			daId=50003;//游戏
		}else if(daId==3){
			daId=50002;//音乐
		}else if(daId==4){
			daId=50004;//视频
		}else if(daId==0){
			daId=50001;	//默认软件
		}else{
			daId=50001;
		}
		
		
		if(osType==2){
			osId=PhoneOs.getInstance(osType, ver).getOsid();
		}else if(osType==3){
			osId=PhoneOs.getInstance(osType, ver).getOsid();
		}
		
		
		if(osType==1){
			osType=100; //Symbian
		}else if(osType==2){
			osType=200;//android
		}else if(osType==3){
			osType=300;//ios
		}else if(osType==4){
			osType=400;//windows
		}else if(osType==5){
			osType=500;	//java
		}else{
			osType=100;
		}

		int flag = 0;
		String message = "";
		List<Tongyong> recomm = null;
		Tongyong ty = null;
		try{
			if(daId==50001){
				recomm = softService.listSoftByShortName(this.AREA_CODE, daId,osType,osId, pageNo, pageSize);
				ty = softService.listSoftByShortNamecount(this.AREA_CODE, daId,osType,osId);
			}else if(daId==50002){
			}else if(daId==50003){
				recomm = softService.listgameByShortName(this.AREA_CODE, daId,osType,osId, pageNo, pageSize);
				ty = softService.listgameByShortNamecount(this.AREA_CODE, daId,osType,osId);
			}else if(daId==50004){
			}else if(daId==50002){
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "数据出错";
			flag = 1;
		}
		
		JsonObject hotjson = new JsonObject();
		hotjson.addProperty("errorcode", flag);
		hotjson.addProperty("desc", message);
		JsonArray recJson = new JsonArray();
		for(int i=0;i<recomm.size();i++){
			JsonObject json = new JsonObject();
			json.addProperty("softid", toolStr(recomm.get(i).getTongyong1()));
			json.addProperty("softname", toolStr(recomm.get(i).getTongyong2()));
			json.addProperty("kaifamanu", toolStr(recomm.get(i).getTongyong3()));
			json.addProperty("softVersion", toolStr(recomm.get(i).getTongyong4()));
			json.addProperty("fileSize", toolStr(recomm.get(i).getTongyong5()));
			json.addProperty("softFee", toolStr(recomm.get(i).getTongyong6()));
			json.addProperty("softtype", toolStr(recomm.get(i).getTongyong7()));
			json.addProperty("shangjiadate", toolStr(recomm.get(i).getTongyong8()));
			json.addProperty("softinfo", toolStr(recomm.get(i).getTongyong9()));
			json.addProperty("ostype", toolStr(recomm.get(i).getTongyong10()));
			json.addProperty("osid", toolStr(recomm.get(i).getTongyong11()));
			json.addProperty("suffix", toolStr(recomm.get(i).getTongyong12()));
			json.addProperty("iconfilename", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong13()));
			json.addProperty("picfilename1", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong14()));
			json.addProperty("picfilename2", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong15()));
			json.addProperty("picfilename3", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong16()));
			json.addProperty("softfilename", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong17()));
			json.addProperty("legalfilename", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong18()));
			json.addProperty("softurl", toolStr(recomm.get(i).getTongyong19()));
			json.addProperty("hotrecmd", toolStr(recomm.get(i).getTongyong20()));
			json.addProperty("downnum", toolStr(recomm.get(i).getTongyong21()));
			json.addProperty("installCount", toolStr(recomm.get(i).getTongyong22()));
			json.addProperty("userRemarkNum", toolStr(recomm.get(i).getTongyong23()));
			json.addProperty("userRemark", toolStr(recomm.get(i).getTongyong24()));
			recJson.add(json);
		}
		hotjson.add("list", recJson);
		hotjson.addProperty("total", ty.getTongyong1());
		JsonObject hotendJson = new JsonObject();
		hotendJson.add("result", hotjson);
		this.doPrint(hotendJson.toString());
		
//
//		if(recomm !=null && recomm.size()==0){
//			flag = 0;
//			message = "没有相关数据！";
//		}
//		Document document = DocumentHelper.createDocument(); 
//		//创建根节点
//		Element root = document.addElement("root");
//		root.addElement("flag").setText(flag + "");
//		root.addElement("message").setText(message);
//
//		Element result = root.addElement("result");
//		for(Tongyong t:recomm){
//			Element product = result.addElement("soft");
//			product.addElement("id").setText(t.getTongyong1());
//			product.addElement("softname").setText(t.getTongyong2());
//			product.addElement("softVersion").setText(t.getTongyong3());
//			product.addElement("fileSize").setText(t.getTongyong4());
//			product.addElement("iconfilename").setText(this.HTTP_PATH + t.getTongyong5());
//			product.addElement("softfilename").setText(this.HTTP_PATH + t.getTongyong6());
//			product.addElement("downnum").setText(t.getTongyong7());
//			product.addElement("userRemark").setText(t.getTongyong8());
//			product.addElement("oSType").setText(t.getTongyong9());
//			product.addElement("dtid").setText(t.getTongyong10());
//			product.addElement("areacode").setText(t.getTongyong11());
//			product.addElement("hotOrder").setText(t.getTongyong12());
//		}
//		ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		try {
//			new XMLWriter(bos).write(document);
////			return bos.toString("utf-8");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//			throw new ApplicationException(e);
//		} catch (IOException e) {
//			e.printStackTrace();
//			throw new ApplicationException(e);
//		}
//		System.out.println(bos.toString("utf-8"));
//		this.doPrint(bos.toString("utf-8"));
		return null;
	}
	
	public String toolStr(String str){
		if(str==null){
			str="";
		}else{
			str = str.replace("\\", "");
		}
		return str;
	}
	
	public String newtopappandgame()throws Exception{
		int osId=0;
		if(pageNo<1){
			pageNo=1;
		}
		
		
		if(daId==1){
			daId=50001; //软件
		}else if(daId==2){
			daId=50003;//游戏
		}else if(daId==3){
			daId=50002;//音乐
		}else if(daId==4){
			daId=50004;//视频
		}else if(daId==0){
			daId=50001;	//默认软件
		}else{
			daId=50001;
		}
		
		
		if(osType==2){
			osId=PhoneOs.getInstance(osType, ver).getOsid();
		}else if(osType==3){
			osId=PhoneOs.getInstance(osType, ver).getOsid();
		}
		if(osType==1){
			osType=100; //Symbian
		}else if(osType==2){
			osType=200;//android
		}else if(osType==3){
			osType=300;//ios
		}else if(osType==4){
			osType=400;//windows
		}else if(osType==5){
			osType=500;	//java
		}else{
			osType=100;
		}

		int flag = 0;
		String message = "";
		List<Tongyong> recomm = null;
//		Tongyong ty = null;
		try{
			if(daId==50001){
				recomm = softService.listSoftByNew(osType, osId, pageNo, pageSize);
			}else if(daId==50002){
			}else if(daId==50003){
				recomm = softService.listgameByNew(osType, osId, pageNo, pageSize);
			}else if(daId==50004){
			}else if(daId==50002){
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "数据出错";
			flag = 1;
		}
		
		JsonObject hotjson = new JsonObject();
		hotjson.addProperty("errorcode", flag);
		hotjson.addProperty("desc", message);
		JsonArray recJson = new JsonArray();
		for(int i=0;i<recomm.size();i++){
			JsonObject json = new JsonObject();
			json.addProperty("softid", toolStr(recomm.get(i).getTongyong1()));
			json.addProperty("softname", toolStr(recomm.get(i).getTongyong2()));
			json.addProperty("kaifamanu", toolStr(recomm.get(i).getTongyong3()));
			json.addProperty("softVersion", toolStr(recomm.get(i).getTongyong4()));
			json.addProperty("fileSize", toolStr(recomm.get(i).getTongyong5()));
			json.addProperty("softFee", toolStr(recomm.get(i).getTongyong6()));
			json.addProperty("softtype", toolStr(recomm.get(i).getTongyong7()));
			json.addProperty("shangjiadate", toolStr(recomm.get(i).getTongyong8()));
			json.addProperty("softinfo", toolStr(recomm.get(i).getTongyong9()));
			json.addProperty("ostype", toolStr(recomm.get(i).getTongyong10()));
			json.addProperty("osid", toolStr(recomm.get(i).getTongyong11()));
			json.addProperty("suffix", toolStr(recomm.get(i).getTongyong12()));
			json.addProperty("iconfilename", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong13()));
			json.addProperty("picfilename1", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong14()));
			json.addProperty("picfilename2", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong15()));
			json.addProperty("picfilename3", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong16()));
			json.addProperty("softfilename", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong17()));
			json.addProperty("legalfilename", this.HTTP_PATH + toolStr(recomm.get(i).getTongyong18()));
			json.addProperty("softurl", toolStr(recomm.get(i).getTongyong19()));
			json.addProperty("hotrecmd", toolStr(recomm.get(i).getTongyong20()));
			json.addProperty("downnum", toolStr(recomm.get(i).getTongyong21()));
			json.addProperty("installCount", toolStr(recomm.get(i).getTongyong22()));
			json.addProperty("userRemarkNum", toolStr(recomm.get(i).getTongyong23()));
			json.addProperty("userRemark", toolStr(recomm.get(i).getTongyong24()));
			recJson.add(json);
		}
		hotjson.add("list", recJson);
		hotjson.addProperty("total", 100);
		JsonObject hotendJson = new JsonObject();
		hotendJson.add("result", hotjson);
		this.doPrint(hotendJson.toString());
		return null;
	}
	//资源在线更新参数为：daId:数据类型，osType:操作系统,ver操作系统版本，name:软件名字,package:包名
	public String update()throws Exception{
		int osId=0;
		if(daId==1){
			daId=50001; //软件
		}else if(daId==2){
			daId=50003;//游戏
		}else if(daId==3){
			daId=50002;//音乐
		}else if(daId==4){
			daId=50004;//视频
		}else if(daId==0){
			daId=50001;	//默认软件
		}else{
			daId=50001;
		}
		if(osType==2){
			osId=PhoneOs.getInstance(osType, ver).getOsid();
		}else if(osType==3){
			osId=PhoneOs.getInstance(osType, ver).getOsid();
		}
		if(osType==1){
			osType=100; //Symbian
		}else if(osType==2){
			osType=200;//android
		}else if(osType==3){
			osType=300;//ios
		}else if(osType==4){
			osType=400;//windows
		}else if(osType==5){
			osType=500;	//java
		}else{
			osType=100;
		}

		int flag = 0;
		String message = "";
		List<Tongyong> recomm = null;
//		Tongyong ty = null;
		Tongyong updatesoft = null;
		try{
			if(daId==50001){
				recomm = softService.listSoftByUpdate(osType, osId, name);
				List<Tongyong> samePackage = new ArrayList<Tongyong>();
				//1.创建一个工厂
				DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
				//2.得到解析器
				DocumentBuilder db=dbf.newDocumentBuilder();
				for(int i=0;i<recomm.size();i++){
					String apkPath = this.DISK_PATH+ toolStr(recomm.get(i).getTongyong17());
//					String path = "D:/20120903163336/game/files/20120904/20120904143510484.apk";
					System.out.println(apkPath);
					try	{
						ApkMetaData meta = new ApkReader().read(new File(apkPath));
						if(meta.getPackageName().equals(packageName))	
							samePackage.add(recomm.get(i));
					}catch(Exception e){
						flag=1;
						message="找不到文件，解析apk有误";
						e.printStackTrace();
					}
				}
				updatesoft = Util.latestVersionsoft(samePackage);
				
			}else if(daId==50002){
			}else if(daId==50003){
				recomm = softService.listgameByUpdate(osType, osId, name);
				List<Tongyong> samePackage = new ArrayList<Tongyong>();
				//1.创建一个工厂
				DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
				//2.得到解析器
				DocumentBuilder db=dbf.newDocumentBuilder();
				for(int i=0;i<recomm.size();i++){
					String apkPath = this.DISK_PATH+ toolStr(recomm.get(i).getTongyong17());
//					String path = "D:/20120903163336/game/files/20120904/20120904143510484.apk";
					System.out.println(apkPath);
					try{
						ApkMetaData meta = new ApkReader().read(new File(apkPath));
						if(meta.getPackageName().equals(packageName))	
							samePackage.add(recomm.get(i));
					}catch(Exception e){
						flag=1;
						message="找不到文件，解析apk有误";
						e.printStackTrace();
					}
				}
				updatesoft = Util.latestVersionsoft(samePackage);
				
			}else if(daId==50004){
			}else if(daId==50002){
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "数据出错";
			flag = 1;
		}

		JsonObject hotjson = new JsonObject();
		hotjson.addProperty("errorcode", flag);
		hotjson.addProperty("desc", message);
		JsonArray recJson = new JsonArray();
//		for(int i=0;i<recomm.size();i++){
		if(updatesoft !=null){
			JsonObject json = new JsonObject();
			json.addProperty("softid", toolStr(updatesoft.getTongyong1()));
			json.addProperty("softname", toolStr(updatesoft.getTongyong2()));
			json.addProperty("kaifamanu", toolStr(updatesoft.getTongyong3()));
			json.addProperty("softVersion", toolStr(updatesoft.getTongyong4()));
			json.addProperty("fileSize", toolStr(updatesoft.getTongyong5()));
			json.addProperty("softFee", toolStr(updatesoft.getTongyong6()));
			json.addProperty("softtype", toolStr(updatesoft.getTongyong7()));
			json.addProperty("shangjiadate", toolStr(updatesoft.getTongyong8()));
			json.addProperty("softinfo", toolStr(updatesoft.getTongyong9()));
			json.addProperty("ostype", toolStr(updatesoft.getTongyong10()));
			json.addProperty("osid", toolStr(updatesoft.getTongyong11()));
			json.addProperty("suffix", toolStr(updatesoft.getTongyong12()));
			json.addProperty("iconfilename", this.HTTP_PATH + toolStr(updatesoft.getTongyong13()));
			json.addProperty("picfilename1", this.HTTP_PATH + toolStr(updatesoft.getTongyong14()));
			json.addProperty("picfilename2", this.HTTP_PATH + toolStr(updatesoft.getTongyong15()));
			json.addProperty("picfilename3", this.HTTP_PATH + toolStr(updatesoft.getTongyong16()));
			json.addProperty("softfilename", this.HTTP_PATH + toolStr(updatesoft.getTongyong17()));
			json.addProperty("legalfilename", this.HTTP_PATH + toolStr(updatesoft.getTongyong18()));
			json.addProperty("softurl", toolStr(updatesoft.getTongyong19()));
			json.addProperty("hotrecmd", toolStr(updatesoft.getTongyong20()));
			json.addProperty("downnum", toolStr(updatesoft.getTongyong21()));
			json.addProperty("installCount", toolStr(updatesoft.getTongyong22()));
			json.addProperty("userRemarkNum", toolStr(updatesoft.getTongyong23()));
			json.addProperty("userRemark", toolStr(updatesoft.getTongyong24()));
			recJson.add(json);
		}
//		}
		hotjson.add("list", recJson);
		hotjson.addProperty("total", 1);
		JsonObject hotendJson = new JsonObject();
		hotendJson.add("result", hotjson);
		this.doPrint(hotendJson.toString());
		return null;
	} 
	
	public int getOsType() {
		return osType;
	}
	public void setOsType(int osType) {
		this.osType = osType;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getDaId() {
		return daId;
	}
	public void setDaId(int daId) {
		this.daId = daId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
//	public static void main(String[] args) {
//		String testp = "D:/20120903163336/game/files/20120904/20120904143510484.apk";
//		ApkInfo apkInfo=null;
//		try {
//			apkInfo = GetApkInfo.getApkInfoByFilePath(testp);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
////		ApkInfo apkInfo = GetApkInfo.getApkInfoByFilePath(apkPath);
//		System.out.println(apkInfo);
//	}
}
