package com.i8app.install.web.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.i8app.install.domain.TempDriverInfo;
import com.i8app.install.service.TempDriverService;

/**
 * 临时驱动上传对应的Action
 * 
 * @author JingYing
 * 
 */
@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class TempDriverAction extends BaseAction {

	private TempDriverService tempDriverService;
	private String vid, pid, file;

	public String save() {
		String status = "failed";
		if ((vid != null) && (pid != null) && (file != null)) {
			TempDriverInfo td = new TempDriverInfo();
			td.setVid(vid);
			td.setPid(pid);
			td.setFileName(file);
			try {
				tempDriverService.addOrUpdate(td);
				status = "success";
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		this.doPrint(status);
		return null;
	}

	/***************************************************************************
	 * 以下是访问器
	 **************************************************************************/

	@Resource
	public void setTempDriverService(TempDriverService tempDriverService) {
		this.tempDriverService = tempDriverService;
	}

	public String getVid() {
		return vid;
	}

	public void setVid(String vid) {
		this.vid = vid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

}
