package com.i8app.install.web.action;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.i8app.install.domain.BusinessHall;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.InstallLog;
import com.i8app.install.service.EmployeeService;
import com.i8app.install.service.LogService;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class LogAction extends BaseAction{
	private LogService logService;
	private EmployeeService empService;
	@Resource
	public void setLogService(LogService logService) {
		this.logService = logService;
	}
	@Resource
	public void setEmpService(EmployeeService empService) {
		this.empService = empService;
	}
	public String phoneLog(){
		try{
			BufferedReader br = new BufferedReader(
					new InputStreamReader(
							ServletActionContext.getRequest().getInputStream(),"UTF-8"
							));
			String s= br.readLine();
			JSONObject jsnb = new JSONObject(s);
			Employee emp = empService.findByEmpnoAndProvidLogin(jsnb.getString("empno"), Integer.parseInt(jsnb.getString("provid")));
			if(emp == null)	{
				throw new IllegalArgumentException("无效的empno:" + jsnb.getString("empno"));
			}
			InstallLog log = new InstallLog();
			log.setProvinceId(emp.getProvince().getProvinceID());
			log.setProvinceName(emp.getProvince().getProvinceName());
			log.setCityId(emp.getCity().getCityID());
			log.setCityName(emp.getCity().getCityName());
			log.setCountyId(emp.getCounty().getCountyID());
			log.setCountyName(emp.getCounty().getCountyName());
			log.setEmpId(emp.getEmpID()+"");
			log.setEmpName(emp.getEmpName());
			log.setEmpNo(emp.getEmpno());
			BusinessHall hall = emp.getHall();
			if(hall != null){
				log.setBhid(hall.getBhid());	//?
				log.setCcid(hall.getCcid());
				log.setChanGrade(hall.getChanGrade());
				log.setChanLevel(hall.getChanLevel());
				log.setChanType(hall.getChanType());
				log.setCtid(hall.getCtid());
				log.setHallId(hall.getHallID());
				log.setHallName(hall.getHallName());
				log.setHallno(hall.getHallNo());
				log.setRccid(hall.getRccid());
			}
//			log.setFilterrule(filterrule);
			log.setImei("");
			log.setImsi("");
			log.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			log.setInstallType(Integer.parseInt(jsnb.getString("installType")));
			log.setIsAutoOpen(0);
//			log.setIsGuide(isGuide);
			log.setIsOtherNet(0);
			log.setIsSmsVal(0);
//			log.setManuName(manuName);
//			log.setModelName(modelName);
			log.setOptStatus(1);	
//			log.setOsVersion(osVersion);
			log.setPhoneNumber(jsnb.getString("no"));
			log.setSeqNo("");
//			log.setTypeid(typeid);
			log.setUuid(UUID.randomUUID().toString());
//			log.setVersionId(versionId);
//			log.setPsn(psn);
			
			logService.buildPhoneInstallLog(log,jsnb.getString("uuid"));
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
