package com.i8app.install.web.action;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.i8app.install.Config;
import com.i8app.install.InvalidOsVersionException;
import com.i8app.install.PhoneOs;
import com.i8app.install.Util;
import com.i8app.install.domain.DriverInfo;
import com.i8app.install.domain.I8client;
import com.i8app.install.domain.LingDongClient;
import com.i8app.install.service.DriverService;
import com.i8app.install.service.LingDongClientService;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class DriverAction extends BaseAction {

	private DriverService driverService;
	private DriverInfo driver;
	private String vid, pid,osVersion,os,bundleId;
	private int id, osType,softId;
	private List<DriverInfo> driverList;
	@Resource
	private LingDongClientService lingDongClientService;
	/**
	 * 接受下载请求, 根据Vid, pid获取数据库表中对应的网络地址
	 * 
	 * @return
	 */
	public String download() {
		String message = "failed";
		DriverInfo d = driverService.getUnique(vid, pid, osType);
		if (d != null)
			message = d.getFilePath();
		this.doPrint(message);
		return null;
	}

	//升级接口地址
	public String upgrade(){
		//把osType转换成Phone.getInstance()所需的参数
		int paramOsType;
		try {
			if(osType == 1)	{
				paramOsType = 31;
			} else if(osType == 2)	{
				paramOsType = 2;
			} else{
				throw new InvalidOsVersionException("osType值:1iphone,2android");
			}
			
			PhoneOs os = PhoneOs.getInstance(paramOsType, osVersion);
			List<LingDongClient> list = lingDongClientService.find(softId, 0, 0, os.getOsid());
			LingDongClient l = lingDongClientService.latestVersion(list);
			if(l == null)	{
				doPrint("null");
			} else	{
				JsonObject json = new JsonObject();
				json.addProperty("softVersion", l.getSoftVersion());
				json.addProperty("url", l.getUrl());
				json.addProperty("memo", l.getMemo());
				System.out.println("打印版本号");
				json.addProperty("versionCode",l.getVersionCode());
				doPrint(json.toString());
			}
		} catch (InvalidOsVersionException e) {
			e.printStackTrace();
			doPrint(e.getMessage());
		}
		return null;
	}
	
	
	public String index() {
		driverList = driverService.list(vid, pid);
		return SUCCESS;
	}
	
	public String i8client(){
		JsonObject json = new JsonObject();
		try {
			if(bundleId !=null && !"".equals(bundleId) && os !=null && !"".equals(os)){
				Pattern p = Pattern.compile("^os[1,2,3,4,5,6]{1}$"); 
				Matcher m = p.matcher(os); 
				if(!m.matches()){
					json.addProperty("success", 20002);
					json.addProperty("desc", "操作系统参数错误");
					doPrint(json.toString());
					return null;
				}
				
				Pattern p1 = Pattern.compile("^[0-9]*$");
				Matcher m1 = p1.matcher(bundleId);
				if(!m1.matches()){
					json.addProperty("success", 20002);
					json.addProperty("desc", "安装包标示参数错误");
					doPrint(json.toString());
					return null;
				}
				
			}else{
				json.addProperty("success", 20002);
				json.addProperty("desc", "操作系统或安装包标示参数不能为空");
				doPrint(json.toString());
				return null;
			}
			I8client l = lingDongClientService.findI8client(bundleId, os);
//			Util.softVerToInt(osVersion);
//			Util.softVerToInt(osVersion);
			
			if(l == null)	{
				json.addProperty("success", 20001);
				json.addProperty("desc", "没有符合条件的最新软件");
				doPrint(json.toString());
			} else	{
//				if(versionCode<=l.getVersionCode()){
//					json.addProperty("softVersion", l.getVersion());
//					json.addProperty("versionCode", l.getVersionCode());
//					json.addProperty("fileSize", l.getFileSize());
//					json.addProperty("info",l.getInfo());
//					json.addProperty("memo",l.getReleaseNotes());
//					json.addProperty("url",this.config.getFileServerUrl() + l.getUrl());
//					json.addProperty("updateTime",l.getUpdateTime());
//					doPrint(json.toString());
//				}else{
//					json.addProperty("success", 20001);
//					json.addProperty("desc", "已是最新软件");
//					doPrint(json.toString());
//				}
				
				json.addProperty("softVersion", l.getVersion());
				json.addProperty("versionCode", l.getVersionCode());
				json.addProperty("fileSize", l.getFileSize());
				json.addProperty("info",l.getInfo());
				json.addProperty("memo",l.getReleaseNotes());
				json.addProperty("url",this.config.getFileServerUrl() + l.getUrl());
				json.addProperty("updateTime",l.getUpdateTime());
				doPrint(json.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			json.addProperty("success", 20400);
			json.addProperty("desc", "系统错误");
			doPrint(json.toString());
		}
		return null;
	}

	public String input() {
		return SUCCESS;
	}

	public String modify() {
		driver = driverService.findById(id);
		return SUCCESS;
	}

	public String del() {
		driverService.del(id);
		return null;
	}

	public String bulkDell() {
		return null;
	}

	public String add() {
		driverService.addOrUpdate(driver);
		return null;
	}

	public String update() {
		driverService.addOrUpdate(driver);
		return null;
	}

	/***************************************************************************
	 * 以下是访问器
	 **************************************************************************/
	@Resource
	public void setDriverService(DriverService driverService) {
		this.driverService = driverService;
	}

	public DriverInfo getDriver() {
		return driver;
	}

	public void setDriver(DriverInfo driver) {
		this.driver = driver;
	}

	public String getVid() {
		return vid;
	}

	public void setVid(String vid) {
		this.vid = vid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public List<DriverInfo> getDriverList() {
		return driverList;
	}

	public void setDriverList(List<DriverInfo> driverList) {
		this.driverList = driverList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOsType() {
		return osType;
	}

	public void setOsType(int osType) {
		this.osType = osType;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public int getSoftId() {
		return softId;
	}
	public void setSoftId(int softId) {
		this.softId = softId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public LingDongClientService getLingDongClientService() {
		return lingDongClientService;
	}

	public void setLingDongClientService(LingDongClientService lingDongClientService) {
		this.lingDongClientService = lingDongClientService;
	}

	public DriverService getDriverService() {
		return driverService;
	}
}
