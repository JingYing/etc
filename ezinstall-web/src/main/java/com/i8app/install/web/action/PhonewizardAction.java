package com.i8app.install.web.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.Mobile;
import com.i8app.install.domain.MobileActivateLog;
import com.i8app.install.domain.Packageinfo;
import com.i8app.install.domain.Phonewizard;
import com.i8app.install.service.LogService;
import com.i8app.install.service.PhonewizardService;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class PhonewizardAction extends BaseAction {
	private static Logger logger = Logger.getLogger(PhonewizardAction.class);

	@Resource
	private PhonewizardService phonewizardService;
	@Resource
	private LogService logService;
	public String phonewizardlog(){
		try{
			BufferedReader br = new BufferedReader(
					new InputStreamReader(
							ServletActionContext.getRequest().getInputStream(),"UTF-8"
							));
			String s= br.readLine();
//			String s = "{\"imei\":\"2q3423412\",\"imsi\":\"1342341234134\",\"phonenumber\":\"15901031592\",\"seqno\":\"adfa2345234\",\"manuName\":\"HTC\",\"ostype\":\"1\",\"modelName\":\"a6390\",\"osversion\":\"2.1.4\",\"empnumber\":\"aaNC0c\",\"provinceid\":\"64\",\"appinfo\":[{\"appid\":11,\"appname\":\"QQ\",\"versionname\":\"3.3.4.2\",\"versioncode\":2,\"pkgname\":\"sadads\",\"flow\":313,\"mobileflow\":313,\"wififlow\":0},{\"appid\":\"\",\"appname\":\"renren\",\"versionname\":\"3.3.3.2\",\"versioncode\":5,\"pkgname\":\"sada2323ds\",\"flow\":34523,\"mobileflow\":34523,\"wififlow\":0}]}";//没有问题
			if(s!=null){
				JSONObject jsnb =new JSONObject(s);
				if(!jsnb.isNull("appinfo")){
					String arraysoft = jsnb.getString("appinfo");
					JSONArray jsar = new JSONArray(arraysoft);
					for(int i=0;i<jsar.length();i++){
						Phonewizard phonewizard = new Phonewizard();
						if(!jsnb.isNull("imsi")){
							String imsi = jsnb.getString("imsi");
							phonewizard.setImsi(imsi);
						}
						if(!jsnb.isNull("phonenumber")){
							String phonenumber = jsnb.getString("phonenumber");
							phonewizard.setPhoneNo(phonenumber);
						}
						if(!jsnb.isNull("empnumber")){
							String empno = jsnb.getString("empnumber");
							if(empno !=null && !"".equals(empno)){
								phonewizard.setEmpno(empno);
							}
						}
						if(!jsnb.isNull("imei")){
							String imei = jsnb.getString("imei");
							phonewizard.setImei(imei);
						}
						if(!jsnb.isNull("seqno")){
							String seqn = jsnb.getString("seqno");
							phonewizard.setDeviceNo(seqn);
							if(seqn !=null &&!"".equals(seqn)){
								Mobile mobile = phonewizardService.findMobileByDevice(seqn);
								if(mobile ==null ){
									Mobile mo = new Mobile();
									mo.setDetectType(1);
									mo.setDeviceNo(seqn);
									if(!jsnb.isNull("modelname")){
										String modelName = jsnb.getString("modelname");
										if(modelName !=null){
											if(modelName.length()>32){
												modelName = modelName.substring(0, 32);
											}
										}else{
											modelName ="";
										}
										mo.setModelName(modelName);
									}
									
									if(!jsnb.isNull("osversion")){
										String osversion = jsnb.getString("osversion");
										mo.setOsVersion(osversion);
									}
									if(!jsnb.isNull("imsi")){
										String imsi = jsnb.getString("imsi");
										mo.setImsi(imsi);
									}
									if(!jsnb.isNull("phonenumber")){
										String phonenumber = jsnb.getString("phonenumber");
										mo.setPhoneNo(phonenumber);
									}
									if(!jsnb.isNull("imei")){
										String imei = jsnb.getString("imei");
										mo.setImei(imei);
									}
									if(!jsnb.isNull("manuname")){
										String manuName = jsnb.getString("manuname");
										mo.setManuName(manuName);
									}
									
									if(!jsnb.isNull("ostype")){
										String ostype = jsnb.getString("ostype");
										if(Integer.parseInt(ostype)==1){
											mo.setOsType(200);
										}
									}
									mo.setSourcetype(1);
									phonewizardService.addOrUpdate(mo);
								}else{
									mobile.setDetectType(1);
									mobile.setDeviceNo(seqn);
									if(!jsnb.isNull("modelname")){
										String modelName = jsnb.getString("modelname");
										if(modelName !=null){
											if(modelName.length()>32){
												modelName = modelName.substring(0, 32);
											}
										}else{
											modelName ="";
										}
										mobile.setModelName(modelName);
									}
									
									if(!jsnb.isNull("osversion")){
										String osversion = jsnb.getString("osversion");
										mobile.setOsVersion(osversion);
									}
									if(!jsnb.isNull("imsi")){
										String imsi = jsnb.getString("imsi");
										mobile.setImsi(imsi);
									}
									if(!jsnb.isNull("phonenumber")){
										String phonenumber = jsnb.getString("phonenumber");
										mobile.setPhoneNo(phonenumber);
									}
									if(!jsnb.isNull("imei")){
										String imei = jsnb.getString("imei");
										mobile.setImei(imei);
									}
									if(!jsnb.isNull("manuname")){
										String manuName = jsnb.getString("manuname");
										mobile.setManuName(manuName);
									}
									
									if(!jsnb.isNull("ostype")){
										String ostype = jsnb.getString("ostype");
										if(Integer.parseInt(ostype)==1){
											mobile.setOsType(200);
										}
									}
									mobile.setSourcetype(1);
									phonewizardService.addOrUpdate(mobile);
								}
							}
						}
						if(!jsnb.isNull("manuname")){
							String manuName = jsnb.getString("manuname");
							phonewizard.setManuName(manuName);
						}
						if(!jsnb.isNull("ostype")){
							String ostype = jsnb.getString("ostype");
							if(Integer.parseInt(ostype)==1){
								phonewizard.setOstype(200+"");
							}
						}
						if(!jsnb.isNull("provinceid")){
							String provinceid = jsnb.getString("provinceid");
							if(provinceid !=null && !"".equals(provinceid)){
								phonewizard.setProvid(Integer.parseInt(provinceid));
							}
						}
						if(!jsar.getJSONObject(i).isNull("appid") && !"".equals(jsar.getJSONObject(i).getString("appid"))){
							phonewizard.setRscid(jsar.getJSONObject(i).getString("appid"));
						}
						if(!jsar.getJSONObject(i).isNull("appname")){
							phonewizard.setRscname(jsar.getJSONObject(i).getString("appname"));
						}
						if(!jsar.getJSONObject(i).isNull("versionname")){
							phonewizard.setVersionName(jsar.getJSONObject(i).getString("versionname"));
						}
						if(!jsar.getJSONObject(i).isNull("versioncode")){
							phonewizard.setVersionCode(Integer.parseInt(jsar.getJSONObject(i).getString("versioncode")));
						
						if(!jsar.getJSONObject(i).isNull("pkgname")){
							phonewizard.setPackageName(jsar.getJSONObject(i).getString("pkgname"));
							if(!jsnb.isNull("deviceid") && !jsar.getJSONObject(i).isNull("pkgname")){
								if(jsnb.getString("deviceid") !=null &&jsar.getJSONObject(i).getString("pkgname") !=null && !"".equals(jsar.getJSONObject(i).getString("pkgname"))){
									Packageinfo packageinfo = phonewizardService.findByPackage(jsnb.getString("deviceid"), jsar.getJSONObject(i).getString("pkgname"));
									if(packageinfo ==null){
										Packageinfo packagi = new Packageinfo();
										packagi.setDeviceNo(jsnb.getString("deviceid"));
										packagi.setApkPkgName(jsar.getJSONObject(i).getString("pkgname"));
										packagi.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										phonewizardService.addOrUpdate(packagi);
									}
								}
							}
						}
						if(!jsar.getJSONObject(i).isNull("flow")){
							if(jsar.getJSONObject(i).getString("flow") != null && !"".equals(jsar.getJSONObject(i).getString("flow"))){
								phonewizard.setFlowSize(Integer.parseInt(jsar.getJSONObject(i).getString("flow")));
							}
						}
						if(!jsar.getJSONObject(i).isNull("mobileflow")){
							if(jsar.getJSONObject(i).getString("mobileflow") != null && !"".equals(jsar.getJSONObject(i).getString("mobileflow"))){
								phonewizard.setGflow(Integer.parseInt(jsar.getJSONObject(i).getString("mobileflow")));
							}else{
								phonewizard.setGflow(0);
							}
						}
						if(!jsar.getJSONObject(i).isNull("wififlow")){
							if(jsar.getJSONObject(i).getString("wififlow") != null && !"".equals(jsar.getJSONObject(i).getString("wififlow"))){
								phonewizard.setWifiFlow(Integer.parseInt(jsar.getJSONObject(i).getString("wififlow")));
							}else{
								phonewizard.setWifiFlow(0);
							}
						}
						
						if(jsar.getJSONObject(i).getString("mobileflow") != null && jsar.getJSONObject(i).getString("wififlow") != null ){
							if(!"".equals(jsar.getJSONObject(i).getString("mobileflow")) && !"".equals(jsar.getJSONObject(i).getString("wififlow"))){
								if(Integer.parseInt(jsar.getJSONObject(i).getString("mobileflow")) ==0&& Integer.parseInt(jsar.getJSONObject(i).getString("wififlow")) ==0){
									phonewizard.setFlowType(0);
								}else if(Integer.parseInt(jsar.getJSONObject(i).getString("mobileflow")) !=0&& Integer.parseInt(jsar.getJSONObject(i).getString("wififlow")) ==0){
									phonewizard.setFlowType(1);
								}else if(Integer.parseInt(jsar.getJSONObject(i).getString("mobileflow")) ==0&& Integer.parseInt(jsar.getJSONObject(i).getString("wififlow")) !=0){
									phonewizard.setFlowType(2);
								}else{
									phonewizard.setFlowType(3);
								}
							}else{
								phonewizard.setFlowType(0);
							}
						}else{
							phonewizard.setFlowType(0);
						}
						if(jsnb.getString("provinceid") !=null && jsnb.getString("empnumber") !=null){
							if(!"".equals(jsnb.getString("empnumber")) && !"".equals(jsnb.getString("provinceid"))){
								Employee emp = phonewizardService.findByIdandOstype(jsnb.getString("empnumber"), Integer.parseInt(jsnb.getString("provinceid")));
								if(emp !=null && emp.getHall() !=null){
									if(emp.getHall().getProvince() !=null){
										phonewizard.setProvid(emp.getHall().getProvince().getProvinceID());
										phonewizard.setProvname(emp.getHall().getProvince().getProvinceName());
									}
									if(emp.getHall().getCity() !=null){
										phonewizard.setCityid(emp.getHall().getCity().getCityID());
										phonewizard.setCityname(emp.getHall().getCity().getCityName());
									}
									if(emp.getHall().getCounty() !=null){
										phonewizard.setCountyid(emp.getHall().getCounty().getCountyID());
										phonewizard.setCountyname(emp.getHall().getCounty().getCountyName());
									}
									phonewizard.setHallid(emp.getHall().getHallID());
									phonewizard.setHallname(emp.getHall().getHallName());
									phonewizard.setCtid(emp.getHall().getCtid());
									phonewizard.setCcid(emp.getHall().getCcid());
									phonewizard.setRccid(emp.getHall().getRccid());
									phonewizard.setEmpname(emp.getEmpName());
								}
							}
						}
						
						phonewizard.setDetectType(1);//idetect:1为自动获取 2短信验证 3为输入的
						phonewizard.setNewtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						phonewizardService.addOrUpdate(phonewizard);
					}
					}
					doPrint("{\"success\":0}");
				}else{
					doPrint("{\"success\":2}");
				}
			}
		}catch(Exception e){
			doPrint("{\"success\":1}");
		}
		return null;
	}
	
	public String packageList(){
		try{
			BufferedReader br = new BufferedReader(
					new InputStreamReader(
							ServletActionContext.getRequest().getInputStream(),"UTF-8"
							));
			String s= br.readLine();
//			String s = "{\"deviceid\":\"353617050262363ffffffff-f2fa-0ed5-5ac2-96c231af1900\"}";//没有问题
			if(s!=null){
				JSONObject jsnb =new JSONObject(s);
				if(!jsnb.isNull("deviceid")){
					String empno = jsnb.getString("deviceid");
					List<Packageinfo> packageList = phonewizardService.findByDevice(empno) ;
					if(packageList !=null && packageList.size()>0){
						JsonObject jsObject = new JsonObject();
						jsObject.addProperty("success", 0);
						JsonArray json = new JsonArray();
						for(int i=0;i<packageList.size();i++){
							JsonObject topsoftJson = new JsonObject();
							topsoftJson.addProperty("pkgname", packageList.get(i).getApkPkgName());
							topsoftJson.addProperty("installtime", packageList.get(i).getInstallTime());
							json.add(topsoftJson);
						}
						jsObject.add("pkgnms", json);
						doPrint(jsObject.toString());
					}else{
						doPrint("{\"success\":\"-1\"}");
					}

				}
			}else{
				doPrint("{\"success\":2}");
			}
		}catch(Exception e){
			doPrint("{\"success\":1}");
		}
		return null;
	}
	
	
	/**
	 * 软件激活日志
	 * @return
	 * @throws JSONException 
	 */
	public String activelog() 	{
		int result = 0;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(getRequest().getInputStream(),"UTF-8"));
			String s= br.readLine();
//			String s =  "{\"imei\":\"862741011156243\",\"imsi\":\"460016326500462\",\"provinceid\":\"44\",\"empnumber\":\"218020\",\"pkgname\":\"sdfsdfa.apk\",\"softid\":\"67\"}";//没有问题
			if(s !=null){
				JSONObject json =new JSONObject(s);
				
				MobileActivateLog log = new MobileActivateLog();
				log.setImei(json.getString("imei"));
	//			log.setSeqNo(json.getString("seqno"));
				log.setImsi(json.getString("imsi"));
				log.setProvid(Integer.parseInt(json.getString("provinceid")));
				log.setEmpno(json.getString("empnumber"));
				log.setPackageName(json.getString("pkgname"));
				if(json.isNull("ostype")){
					log.setOstype("200");
				}else{
					log.setOstype(json.getString("ostype"));
				}
	//			log.setIsOpen(json.getInt("isopen"));	//手机端传入int值
				log.setRscid(json.getString("softid"));
				logger.error("logService : "+logService+" ,log : " + log);
				logger.error("empno : " + json.getString("empnumber")+" , provinceid : "+Integer.parseInt(json.getString("provinceid")));
				logService.completeMobileActivateLog(log);
			
			}else{
				result = 3;
			}
		} catch (IOException e) {
			result = 2;
		} catch (JSONException e) {
			result = 1;
		} catch (Exception e)	{
			result = 500;
		} 
		return doPrint("{\"success\":" + result + "}");
	}
}
