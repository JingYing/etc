package com.i8app.install.web.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.i8app.install.domain.AppleUser;
import com.i8app.install.domain.Applelog;
import com.i8app.install.service.AppleService;
import com.opensymphony.xwork2.ActionContext;
/**
 * @author Lixiangyang 2014-05-08
 */
@SuppressWarnings("serial")
//@Controller
@Component("appleAction")
@Scope("prototype")
public class AppleAction extends BaseAction	{
	private static Logger logger = Logger.getLogger(AppleAction.class);
	@Resource
	public AppleService appleService;
	private Integer user_id,app_id,isOk,isActivate,logid;
	private String openudid,sign,mac,idfa,version,adudid,rscid,mobile_client_version; 
	/**
	 * 葫芦苹果树用户信息保存入桓源数据库
	 * @return
	 */
	public String appleuser(){
		JsonObject errorjson = new JsonObject();
		String msg = "ok";
		try{
			AppleUser user = new AppleUser();
			if(idfa !=null && !"".equals(idfa) && mac !=null && !"".equals(mac)){
				mac = mac.toLowerCase();
//				Pattern macPattern =  Pattern.compile("([0-9A-F]{2})(:[0-9A-F]{2}){5}");
//				Matcher macMatcher = macPattern.matcher(mac);
//				if(macMatcher.matches()){
					user.setApp_id(app_id);
					user.setUser_id(user_id);
					user.setIdfa(idfa);
					user.setMac(mac);
					user.setOpenudid(openudid);
					user.setSign(sign);
					user.setVersion(mobile_client_version);
					user.setUpdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					appleService.saveOrUpdateAppleUser(user);
					String namespace = ActionContext.getContext().getActionInvocation().getProxy().getNamespace().replace("/", "");
					String callback = String.format("%s%s/callback.action?", getBasePath(),namespace);
//					appleService.checkActivate(callback);
					appleService.asynccheckActivate(callback,mac);
			}else{
				msg="mac地址和idfa不能为空";
			}
		}catch(Exception e){
			logger.error("用户信息保存失败");
			msg ="保存失败";
		}
		errorjson.addProperty("msg", msg);
		return this.doPrint(errorjson.toString());
	}
	/**
	 * PC端苹果树下载安装日志
	 * @return
	 */
	public String applelog(){
		JsonObject errorjson = new JsonObject();
		int error = 0;
		try{
			if(rscid !=null && !"".equals(rscid) && mac !=null && !"".equals(mac)){
				mac = mac.toLowerCase();
				String adud = appleService.findAdudid(rscid);
				if(adud !=null && !"".equals(adud)){
					Applelog log = new Applelog();
					log.setMac(mac);
					log.setRscid(rscid);
					log.setPhoneversion(version);
					log.setAdudid(adud);
					log.setIsOk(0);
					log.setInserttime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					appleService.saveOrUpdateApplelog(log);
					String namespace = ActionContext.getContext().getActionInvocation().getProxy().getNamespace().replace("/", "");
					String callback = String.format("%s%s/callback.action?", getBasePath(),namespace);
					appleService.asynccheckActivate(callback,mac);
				}
			}else{
				error=2;
			}
		}catch(Exception e){
			logger.error("苹果树软件下载安装日志保存失败");
			error =1;
		}
		errorjson.addProperty("error", error);
		return this.doPrint(errorjson.toString());
	}
	/**
	 * 回调接口
	 * @return
	 */
	public String callback(){
		JsonObject errorjson = new JsonObject();
		int error = 0;
		try{
			if(logid !=null){
				Applelog log =appleService.findApplelogbyid(logid);
				if(log !=null){
					log.setIsActivate(1);
					appleService.saveOrUpdateApplelog(log);
				}
			}else{
				error=2;
			}
		}catch(Exception e){
			logger.error("苹果树软件激活状态保存失败");
			error =1;
		}
		errorjson.addProperty("error", error);
		return this.doPrint(errorjson.toString());
	}
	public String getOpenudid() {
		return openudid;
	}
	public void setOpenudid(String openudid) {
		this.openudid = openudid;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getIdfa() {
		return idfa;
	}
	public void setIdfa(String idfa) {
		this.idfa = idfa;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public AppleService getAppleService() {
		return appleService;
	}
	public void setAppleService(AppleService appleService) {
		this.appleService = appleService;
	}
	public Integer getIsOk() {
		return isOk;
	}
	public void setIsOk(Integer isOk) {
		this.isOk = isOk;
	}
	public Integer getIsActivate() {
		return isActivate;
	}
	public void setIsActivate(Integer isActivate) {
		this.isActivate = isActivate;
	}
	public String getAdudid() {
		return adudid;
	}
	public void setAdudid(String adudid) {
		this.adudid = adudid;
	}
	public Integer getLogid() {
		return logid;
	}
	public void setLogid(Integer logid) {
		this.logid = logid;
	}
	public String getRscid() {
		return rscid;
	}
	public void setRscid(String rscid) {
		this.rscid = rscid;
	}
	public String getMobile_client_version() {
		return mobile_client_version;
	}
	public void setMobile_client_version(String mobile_client_version) {
		this.mobile_client_version = mobile_client_version;
	}
}
