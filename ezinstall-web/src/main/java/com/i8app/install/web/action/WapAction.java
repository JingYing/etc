package com.i8app.install.web.action;

import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.install.Config;
import com.i8app.install.Globals;
import com.i8app.install.MD5;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.Pkg;
import com.i8app.install.domain.PkgGroup;
import com.i8app.install.service.EmployeeService;
import com.i8app.install.service.SmsHessianService;
import com.i8app.install.service.WapService;
import com.i8app.install.wsclient.EcsFacade;
import com.i8app.install.wsclient.EcsQuery;
@SuppressWarnings("serial")
@Controller
@Scope("prototype")
public class WapAction extends BaseAction {

	private EmployeeService empService;
	private WapService wapService;
	private SmsHessianService smsHessianService;
	private String empNo,pass,checkName,packUuid;
	private int provId,checkNameflag,pageNo,packageid,ostype,dis;
	private Pager<AppDTO> pager;
	private AppDTO appDto;
	private String phoneNumber,verificationcode,uuid,packuuid;
	private String softName,checkPage,unikey,netType;
	private List<Pkg> pkgDTOList;
	@Resource
	private Config config;
	@Resource
	private EcsFacade ecsFacade;
	@Resource
	private EcsQuery ecsQuery;
	private Integer flag;
	public String loginPage()throws Exception{
		Cookie cookies[]=this.request.getCookies();
		if(cookies !=null){
			for(Cookie cookie1:cookies){    //用一个循环语句遍历刚才建立的Cookie对象数组 
				if(cookie1.getName().equals("username")){
					empNo=cookie1.getValue(); //取得这个Cookie的名字 
				}
				if(cookie1.getName().equals("password")){
					pass=cookie1.getValue(); //取得这个Cookie的内容 
				}
				if(cookie1.getName().equals("provId")){
					if(cookie1.getValue() !=null){
						provId=Integer.parseInt(cookie1.getValue()); //取得这个Cookie的内容
					}else{
						provId=0; //取得这个Cookie的内容 
					}
				}
				if(cookie1.getName().equals("checkName")){
					if(cookie1.getValue() !=null){
						checkNameflag=1;
					}else{
						checkNameflag=0;
					}
				}
			}
		}
		return SUCCESS;
	}
	public String login()throws Exception{
		try{
		if(provId!=0){
			if(empNo !=null && !"".equals(empNo) && pass !=null && !"".equals(pass)){
				Employee emp = empService.findByEmpnoAndProvidLogin(empNo, provId);
				if(emp !=null){
					if(emp.getEmpPwd().equalsIgnoreCase(new MD5().toMD5(pass))){
						ServletActionContext.getRequest().getSession().setAttribute(Globals.CURRENT_USER, emp);
						ServletActionContext.getRequest().getSession().setAttribute("checkPage", config.getProList());
						ServletActionContext.getRequest().getSession().setAttribute("ostype", 0);
						String check = this.getCheckName();
						if(check !=null){
							Cookie cookie1=new Cookie("username", emp.getEmpno());
							Cookie cookie2=new Cookie("password", pass);
							Cookie cookie3=new Cookie("provId", provId+"");
							Cookie cookie4=new Cookie("checkName", check);
							cookie1.setMaxAge(60*60*24*7);   //存活期为一周
							cookie2.setMaxAge(60*60*24*7);   //存活期为一周
							cookie3.setMaxAge(60*60*24*7);   //存活期为一周
							cookie4.setMaxAge(60*60*24*7);
							response.addCookie(cookie1);
							response.addCookie(cookie2);
							response.addCookie(cookie3);
							response.addCookie(cookie4);
						}else{
							Cookie cookie1=new Cookie("username", emp.getEmpno());
							Cookie cookie2=new Cookie("password", pass);
							Cookie cookie3=new Cookie("provId", provId+"");
							Cookie cookie4=new Cookie("checkName", check);
							cookie1.setMaxAge(0);   
							cookie2.setMaxAge(0);  
							cookie3.setMaxAge(0);   
							cookie4.setMaxAge(0);
							response.addCookie(cookie1);
							response.addCookie(cookie2);
							response.addCookie(cookie3);
							response.addCookie(cookie4);
						}
//						return "index_First";
						if(config.getProList().contains(emp.getProvince().getProvinceID()+"")){
							this.setMessage("{\"success\":6}");
						}else{
							this.setMessage("{\"success\":5}");
						}
					}else{
						this.setMessage("{\"success\":3}");
					}
				}else{
					this.setMessage("{\"success\":2}");
				}
			}else{
				this.setMessage("{\"success\":1}");
			}
		}else{
			this.setMessage("{\"success\":0}");
		}
		}catch(Exception e){
			this.setMessage("{\"success\":4}");
		}
		return AJAX;
	}
	public String findDesignationAppByPage() throws Exception{
		ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
		Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
		AreaParam area = new AreaParam(Constants.DEPTID_LIANTONG,emp.getProvince().getProvinceID(),emp.getCity().getCityID());
		List<PkgGroup> groupList = wapService.findPkgGroup(Constants.BIZ_ESSENTIAL, area);
		if(groupList.size()==0){
			pkgDTOList = new ArrayList<Pkg>();
		}else{
			pkgDTOList = wapService.findPkgList(groupList.get(0).getId());
		}
		return SUCCESS;
	}
	
	public String sessionOstype()throws Exception{
		ServletActionContext.getRequest().getSession().setAttribute("ostype", ostype);
		this.setMessage("{\"success\":1}");
		return AJAX;
	}
	public String findDesignationAppByPackage()throws Exception{
//		String mobtype = getRequest().getHeader("User-Agent");
//		Os os = AdaptWap.adaptWapUc(mobtype) ;
		Os os = toOs();
		if(pageNo >0){
			this.offset=(pageNo-1)*20;
		}else{
			pageNo=1;
		}
		pager = wapService.findInstallPackByPkg(null, packageid, os, this.offset, 20);
//		pager = wapService.findAppByPkg(os, packageid, cpList, this.offset, 20);
		return SUCCESS;
	}
	//最新排行
	public String findAllSoftInfoByPage() throws Exception{
		Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
		ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
		Os os = toOs();
		AreaParam area = new AreaParam(Constants.DEPTID_LIANTONG,emp.getProvince().getProvinceID(),emp.getCity().getCityID());
		List<String> cpList = new ArrayList<String>();
		if(ostype !=0 && ostype !=1 && ostype!=4){
			cpList.add(Constants.CP_I8APP);
		}else{
			cpList.add(Constants.CP_WOSTORE);
		}
		AppParam app = new AppParam(null,null,null,cpList,null,null);
		if(pageNo >0){
			this.offset=(pageNo-1)*20;
		}else{
			pageNo=1;
		}
		pager = wapService.query(area,os,app,this.offset,20);
		return SUCCESS;
	}
	//软件单条信息查询
	public String findsoftInfo() throws Exception{
		Os os = toOs();
		checkPage = config.getProList();
		appDto = wapService.findSingle(packUuid, os);
		return SUCCESS;
	}
	
	//软件单条信息查询
	public String findsoftInfopack() throws Exception{
		checkPage = config.getProList();
		appDto = wapService.findSinglePack(packUuid);
		return SUCCESS;
	}
	public String checkPhone()throws Exception{
			Pattern p = Pattern.compile("^1[3,5,4,8]{1}\\d{9}$"); 
			if(phoneNumber !=null && !"".equals(phoneNumber)){
				Matcher m = p.matcher(phoneNumber.trim()); 
				if(m.matches()){
					String checkCode = smsHessianService.sendAuthcode(null, phoneNumber.trim());
					if(checkCode !=null){
						ServletActionContext.getRequest().getSession().setAttribute("checkCode", checkCode);
						this.setMessage("{\"success\":2}");
					}else{
						this.setMessage("{\"success\":1}");
					}
				}else{
					this.setMessage("{\"success\":3}");
				}
			}else{
				this.setMessage("{\"success\":4}");
			}
		return AJAX;
	}
	public String smsPhone()throws Exception{
			String checkCode = (String)ServletActionContext.getRequest().getSession().getAttribute("checkCode");
			if(verificationcode !=null){
				if(checkCode !=null){
					if(checkCode.equals(verificationcode)){
						Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
						Os os = toOs();
						if(phoneNumber !=null && !"".equals(phoneNumber)){
//							String packUuid = appQuerier.confirmPack(uuid, os, "cp09");
//							smsHessianService.sendShortUrl(installType, phoneNo, empId, installPackUuid)
//							AppInstallPackDTO appInstallPackDto = wapService.findAppInstallPack(uuid, os ,null);
							ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
							String packUuid =null;
							if(ostype !=0 && ostype !=1 && ostype!=4){
								packUuid =  wapService.confirmPack(uuid, os, Constants.CP_I8APP);
							}else{
								packUuid =  wapService.confirmPack(uuid, os, Constants.CP_WOSTORE);
							}
							if(packUuid !=null){
								ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
								if(ostype==2){
									smsHessianService.sendIOSShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  packUuid);
								}else{
									smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  packUuid);
								}
							}
							this.setMessage("{\"success\":4}");
						}else{
							this.setMessage("{\"success\":5}");
						}
					}else{
						this.setMessage("{\"success\":3}");
					}
				}else{
					this.setMessage("{\"success\":2}");
				}
			}else{
				this.setMessage("{\"success\":1}");
			}
		return AJAX;
	}
	public String smsbulPhone()throws Exception{
			String checkCode = (String)ServletActionContext.getRequest().getSession().getAttribute("checkCode");
			Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
			Os os = toOs();
			if(verificationcode !=null){
				if(checkCode !=null){
					if(checkCode.equals(verificationcode)){
						@SuppressWarnings("unchecked")
						List<String> uuidList = (List<String>)ServletActionContext.getRequest().getSession().getAttribute("allDown");
						if(uuidList !=null && uuidList.size()>0){
							if(phoneNumber !=null && !"".equals(phoneNumber)){
								for(int i=0;i<uuidList.size();i++){
//									AppInstallPackDTO appInstallPackDto = wapService.findAppInstallPack(uuidList.get(i), os ,null);
//									String packUuid =  wapService.confirmPack(uuidList.get(i), os, Constants.CP_I8APP);
									
									
									ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
									String packUuid =null;
									if(ostype !=0 && ostype !=1 && ostype!=4){
										packUuid =  wapService.confirmPack(uuidList.get(i), os, Constants.CP_I8APP);
									}else{
										packUuid =  wapService.confirmPack(uuidList.get(i), os, Constants.CP_WOSTORE);
									}
									
//									smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(),appInstallPackDto.getUuid());
									if(ostype==2){
										smsHessianService.sendIOSShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  packUuid);
									}else{
										smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(), packUuid);
									}
								}
								uuidList.clear();
								this.setMessage("{\"success\":4}");
							}else{
								this.setMessage("{\"success\":6}");
							}
						}else{ 
							this.setMessage("{\"success\":5}");
						}
					}else{
						this.setMessage("{\"success\":3}");
					}
				}else{
					this.setMessage("{\"success\":2}");
				}
			}else{
				this.setMessage("{\"success\":1}");
			}
		return AJAX;
	}
	public String smsPhoneNOCheck()throws Exception{
		Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
		Os os = toOs();
		if(phoneNumber !=null && !"".equals(phoneNumber)){
//			AppInstallPackDTO appInstallPackDto = wapService.findAppInstallPack(uuid, os ,null);
			if(dis !=2){
//				String packUuid1 =  wapService.confirmPack(uuid, os, Constants.CP_I8APP);
				ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
				String packUuid1 =null;
				if(ostype !=0 && ostype !=1 && ostype!=4){
					packUuid1 =  wapService.confirmPack(uuid, os, Constants.CP_I8APP);
				}else{
					packUuid1 =  wapService.confirmPack(uuid, os, Constants.CP_WOSTORE);
				}
				if(packUuid1 !=null){
	//				smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  appInstallPackDto.getUuid());
					
					if(ostype==2){
						smsHessianService.sendIOSShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  packUuid1);
					}else{
						smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(), packUuid1);
					}
				}
			}else{
				ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
				if(ostype==2){
					smsHessianService.sendIOSShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  packUuid);
				}else{
					smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(), packUuid);
				}
			}
			this.setMessage("{\"success\":1}");
		}else{
			this.setMessage("{\"success\":2}");
		}
		return AJAX;
	}
	public String smsbulPhoneNOCheck()throws Exception{
		Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
		Os os = toOs();
		@SuppressWarnings("unchecked")
		List<String> uuidList = (List<String>)ServletActionContext.getRequest().getSession().getAttribute("allDown");
		if(uuidList !=null && uuidList.size()>0){
			if(phoneNumber !=null && !"".equals(phoneNumber)){
				for(int i=0;i<uuidList.size();i++){
//					AppInstallPackDTO appInstallPackDto = wapService.findAppInstallPack(uuidList.get(i), os ,null);
//					String packUuid =  wapService.confirmPack(uuidList.get(i), os, Constants.CP_I8APP);
//					smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(),appInstallPackDto.getUuid());
					ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
					String packUuid =null;
					if(ostype !=0 && ostype !=1 && ostype!=4){
						packUuid =  wapService.confirmPack(uuidList.get(i), os, Constants.CP_I8APP);
					}else{
						packUuid =  wapService.confirmPack(uuidList.get(i), os, Constants.CP_WOSTORE);
					}
					if(ostype==2){
						smsHessianService.sendIOSShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  packUuid);
					}else{
						smsHessianService.sendShortUrl(8, phoneNumber.trim(), emp.getEmpID(),  packUuid);
					}
				}
				uuidList.clear();
				this.setMessage("{\"success\":1}");
			}else{
				this.setMessage("{\"success\":2}");
			}
		}else{ 
			this.setMessage("{\"success\":3}");
		}
		return AJAX;
	}
	//搜索功能：根据名称查找软件
	public String findsoftByName() {
		try	{
			if(softName !=null){
//				softName = URLEncoder.encode(softName, "UTF-8");
				softName = URLDecoder.decode(softName, "UTF-8");
			}
			Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
			Os os = toOs();
			AreaParam area = new AreaParam(Constants.DEPTID_LIANTONG,emp.getProvince().getProvinceID(),emp.getCity().getCityID());
			List<String> cpList = new ArrayList<String>();
			ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
			if(ostype !=0 && ostype !=1 && ostype!=4){
				cpList.add(Constants.CP_I8APP);
			}else{
				cpList.add(Constants.CP_WOSTORE);
			}
			
			
			AppParam app = new AppParam(null,softName,null,cpList,null,null);
			if(pageNo >0){
				this.offset=(pageNo-1)*20;
			}else{
				pageNo=1;
			}
			pager = wapService.query(area,os,app,this.offset,20);
		} catch(Exception e)	{
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String allAppDownZhiding()throws Exception{
		ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
		checkPage = config.getProList();
		return SUCCESS;
	}
	public String buldown()throws Exception{
		try{
			if(uuid !=null){
				@SuppressWarnings("unchecked")
				List<String> uuidList = (List<String>)ServletActionContext.getRequest().getSession().getAttribute("allDown");
				if(uuidList !=null){
					if(!uuidList.contains(uuid)){
						uuidList.add(uuid);
					}
					ServletActionContext.getRequest().getSession().setAttribute("allDown", uuidList);
				}else{
					uuidList = new ArrayList<String>();
					uuidList.add(uuid);
					ServletActionContext.getRequest().getSession().setAttribute("allDown", uuidList);
				}
			}else{
				this.setMessage("{\"success\":1}");
			}
		}catch(Exception e){
			this.setMessage("{\"success\":0}");
		}
		return AJAX;
	}
	public String allAppDown()throws Exception{
		checkPage = config.getProList();
		return SUCCESS;
	}
	public String allAppDownJSON()throws Exception{
		@SuppressWarnings("unchecked")
		List<String> uuidList = (List<String>)ServletActionContext.getRequest().getSession().getAttribute("allDown");
		StringBuffer sb = new StringBuffer();
		int i=0;
		if(uuidList !=null && uuidList.size()>0){
			sb.append("{\"id\":[");
			for(String str:uuidList){
				sb.append("\"");
				sb.append(str);
				sb.append("\"");
				if (i < (uuidList.size() - 1)) {
					sb.append(",");
				}
				i++;
			}
			sb.append("]}");
		}else{
			sb.append("{\"id\":0}");
		}
		this.setMessage(sb.toString());
		return AJAX;
	}
	public String allAppDownAJAX()throws Exception{
		@SuppressWarnings("unchecked")
		List<String> uuidList = (List<String>)ServletActionContext.getRequest().getSession().getAttribute("allDown");
		List<AppDTO> appList = new ArrayList<AppDTO>();
		Os os = toOs();
		StringBuffer sb = new StringBuffer();
		if(uuidList !=null && uuidList.size()>0){
			if(uuid !=null){
				if(uuidList.contains(uuid)){
					uuidList.remove(uuid);
				}
			}
			for(String uuid:uuidList){
				AppDTO app = wapService.findSingle(uuid, os);
				appList.add(app);
			}
			for(int i=0;i<appList.size();i++){
				sb.append("<div class=\"tui2_kid\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td width=\"60\" rowspan=\"2\">");
				sb.append("<div class=\"tui2_kid_icon\">");
				sb.append("<a href=\"../wap/findsoftInfo.action?packUuid="+appList.get(i).getUuid()+"\"><img src=\""+config.getFileServerUrl()+ appList.get(i).getIcon()+"\" /></a>");
				sb.append("</div>");
				sb.append("</td>");
				sb.append("<td>");
				sb.append("<div class=\"tui2_kid_name\">"+appList.get(i).getName()+"</div>");
				sb.append("</td>");
				sb.append("<td>");
				sb.append("<div class=\"tui2_kid_star\">");
//				for(int j=0;j<5;j++){
//					sb.append("<img src=\"../img/star-on.png\" width=\"16\" height=\"16\" />");
//				}
				sb.append("</div>");
				sb.append("</td>");
				sb.append("<td>");
				sb.append("<a href=\"javascript:void(0);\" onclick=\"delteDown('"+appList.get(i).getUuid()+"');\"><img src=\"../images/cross_circle.png\" alt=\"删除\" title=\"删除\"/></a>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("</table>");
				sb.append("</div>");
			}
		}else{
			sb.append("<div class=\"tui2_kid\">");
			sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("您还未选择下载的软件");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</div>");			
		}
		this.setMessage(sb.toString());
		return AJAX;
	}
	public String deldown()throws Exception{
		try{
			if(uuid !=null){
				@SuppressWarnings("unchecked")
				List<String> uuidList = (List<String>)ServletActionContext.getRequest().getSession().getAttribute("allDown");
				if(uuidList !=null && uuidList.size()>0){
					if(uuidList.contains(uuid)){
						uuidList.remove(uuid);
					}
					ServletActionContext.getRequest().getSession().setAttribute("allDown", uuidList);
				}
			}else{
				this.setMessage("{\"success\":1}");
			}
		}catch(Exception e){
			this.setMessage("{\"success\":0}");
		}
		return AJAX;
	}
	//必备
	public String findspecifiedInfoByPage() throws Exception{
		Employee emp =(Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
		AreaParam area = new AreaParam(Constants.DEPTID_LIANTONG,emp.getProvince().getProvinceID(),emp.getCity().getCityID());
		List<String> cpList = new ArrayList<String>();
//		cpList.add(Constants.CP_I8APP);
		
		ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
		if(ostype !=0 && ostype !=1 && ostype!=4){
			cpList.add(Constants.CP_I8APP);
		}else{
			cpList.add(Constants.CP_WOSTORE);
		}
		
		List<PkgGroup> pkgGroupDTO = wapService.findPkgGroup(Constants.BIZ_ESSENTIAL, area);
		if(pkgGroupDTO !=null && pkgGroupDTO.size()>0){
			for(int i=0;i<pkgGroupDTO.size();i++){
				wapService.findPkgList(pkgGroupDTO.get(i).getId());
			}
		}
		return SUCCESS;
	}
	
	public Os toOs(){
		ostype = (Integer)ServletActionContext.getRequest().getSession().getAttribute("ostype");
		Os os = Os.ANDROID;
		switch(ostype){
		case 1:
			os = Os.ANDROID;
			break;
		case 2:
			os = Os.IOS;
			break;
		case 3:
			os=Os.IOSX;
			break;
		case 4:
			os=Os.SYMBIAN;
			break;
		case 5:
			os=Os.WINMOBILE;
			break;
		case 6:
			os=Os.JAVA;
			break;
		default:
			os=Os.ANDROID;
		}
		return os;
	}
	public  String getStrUtf8(String s) throws Exception{
		String ret = null;
		try {
			ret = java.net.URLDecoder.decode(s, "utf-8");
		} catch (Exception ex) {
		}
		return ret;
	}

	public String catchMethod() throws Exception{
		return "error";
	}

	
	
	public String tmcdl()	{
//		String mobtype = getRequest().getHeader("User-Agent");
//		StringTokenizer st = new StringTokenizer(mobtype,";");
//		String userlook = st.nextToken();
//		String userbrowser = st.nextToken();//得到用户的浏览器名及版本
//		String useros = st.nextToken();//得到用户的操作系统名及版本
//		String typeinfo = AdaptWap.adaptWapUc(mobtype) ;//操作系统100代表symbian 200代表android，300代表ios
//		Properties props=System.getProperties(); //获得系统属性集    
//		String osName = props.getProperty("os.name"); //操作系统名称    
//		String osArch = props.getProperty("os.arch"); //操作系统构架    
//		String osVersion = props.getProperty("os.version"); //操作系s统版本
//		int tyInt=10;
//		if(typeinfo == null||"".equals(typeinfo)){
//			tyInt=10;
//		}else{
//			tyInt=Integer.parseInt(typeinfo);
//		}
		//http://q.17186.cn/dl.to?dw=2&ac=4&vs=0415&id=809&dt=1&ot=2&pn=15901031592
		//web,tdc,sms,wap四种下载方式公用接口     		
		//	1：dt数据类型， 1:应用和2:游戏
		//	2：dw下载类型，1代表web,2代表tdc,3代表sms,4代表wap四种下载方式公用接口    
		//	3：id数据id，
		//	4：phonenum手机号  用pn代替phonenum
		//  5: ot 手机操作系统 1 塞班,2 安卓,31 iphone越狱,33未越狱,4 window,5 java
		//			ot：手机操作系统类型，1：Symbian，2：Android，31：越狱iPhone，33：正版iPhone，4：Windows，5：JAVA;
		//  6:ac areacode 区域id;
		//  7:vs 0415 二维码生成批次
//		Os os = AdaptWap.adaptWapUc(mobtype) ;
		return null;
	}
	
	public InputStream getInputStream(){
		InputStream inputStream = null;
			return inputStream;
	}
	
	public String findsoftByOsType()throws Exception{
//		list1=hotTypeService.findAllSoftInfoByPage(osTYpe);
		return SUCCESS;
	}
	
	
	/**
	 * 查询3G套餐/流量数据接口
	 * @return
	 * @throws Exception
	 */
	public String query()throws Exception{
		String result = null;
		if(flag==0){
			result = ecsFacade.prodFindReq529(phoneNumber);
		}else if(flag==1){
			result = ecsFacade.flowSearchReq(phoneNumber);
		}else if(flag==2){
			result = ecsFacade.prodFindReq528(phoneNumber);
		}
		return doPrint(result);
	} 

	/**
	 * 联通NET取号接口
	 * @return
	 * @throws Exception
	 */
	public String queryMNumber()throws Exception{
		String result = null;
		System.out.println(this.getRequest().getRemoteAddr());
		System.err.println(this.getRequest().getHeader("X-real-ip"));
		result = ecsQuery.queryMobileNumber(unikey, this.getRequest().getHeader("X-real-ip"));
//		this.setMessage(result);
		System.out.println(result);
//		return AJAX;
		doPrint(result);
		return null;
	} 

	public EmployeeService getEmpService() {
		return empService;
	}
	@Resource
	public void setEmpService(EmployeeService empService) {
		this.empService = empService;
	}
	@Resource
	public void setWapService(WapService wapService) {
		this.wapService = wapService;
	}
	@Resource
	public void setSmsHessianService(SmsHessianService smsHessianService) {
		this.smsHessianService = smsHessianService;
	}
	public String getEmpNo() {
		return empNo;
	}
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public int getProvId() {
		return provId;
	}
	public void setProvId(int provId) {
		this.provId = provId;
	}
	public String getCheckName() {
		return checkName;
	}
	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}
	public int getCheckNameflag() {
		return checkNameflag;
	}
	public void setCheckNameflag(int checkNameflag) {
		this.checkNameflag = checkNameflag;
	}
	public Pager<AppDTO> getPager() {
		return pager;
	}
	public void setPager(Pager<AppDTO> pager) {
		this.pager = pager;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public String getPackUuid() {
		return packUuid;
	}
	public void setPackUuid(String packUuid) {
		this.packUuid = packUuid;
	}
	public AppDTO getAppDto() {
		return appDto;
	}
	public void setAppDto(AppDTO appDto) {
		this.appDto = appDto;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getVerificationcode() {
		return verificationcode;
	}
	public void setVerificationcode(String verificationcode) {
		this.verificationcode = verificationcode;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getSoftName() {
		return softName;
	}
	public void setSoftName(String softName) {
		this.softName = softName;
	}
	public String getCheckPage() {
		return checkPage;
	}
	public void setCheckPage(String checkPage) {
		this.checkPage = checkPage;
	}
	public List<Pkg> getPkgDTOList() {
		return pkgDTOList;
	}
	public void setPkgDTOList(List<Pkg> pkgDTOList) {
		this.pkgDTOList = pkgDTOList;
	}
	public int getPackageid() {
		return packageid;
	}
	public void setPackageid(int packageid) {
		this.packageid = packageid;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public int getOstype() {
		return ostype;
	}
	public void setOstype(int ostype) {
		this.ostype = ostype;
	}
	public String getUnikey() {
		return unikey;
	}
	public void setUnikey(String unikey) {
		this.unikey = unikey;
	}
	public int getDis() {
		return dis;
	}
	public void setDis(int dis) {
		this.dis = dis;
	}
	public String getPackuuid() {
		return packuuid;
	}
	public void setPackuuid(String packuuid) {
		this.packuuid = packuuid;
	}
	public String getNetType() {
		return netType;
	}
	public void setNetType(String netType) {
		this.netType = netType;
	}
	public void setEcsFacade(EcsFacade ecsFacade) {
		this.ecsFacade = ecsFacade;
	}
	public void setEcsQuery(EcsQuery ecsQuery) {
		this.ecsQuery = ecsQuery;
	}
}
