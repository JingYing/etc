package com.i8app.install.web.action;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.install.InvalidOsVersionException;
import com.i8app.install.Pager;
import com.i8app.install.PhoneOs;
import com.i8app.install.domain.GameInfo;
import com.i8app.install.domain.GameType;
import com.i8app.install.domain.MusicInfo;
import com.i8app.install.domain.SoftInfo;
import com.i8app.install.domain.SoftType;
import com.i8app.install.domain.Tongyong;
import com.i8app.install.domain.VideoInfo;
import com.i8app.install.service.AbstractJsonService;
import com.i8app.install.service.BookInfoService;
import com.i8app.install.service.IAppService;
import com.i8app.install.service.MusicInfoService;
import com.i8app.install.service.SoftService;
import com.i8app.install.service.VideoInfoService;

/**
 * 对手机端提供的一些访问接口
 */
@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class MobilePhoneAction extends BaseAction {
	private static Logger logger = Logger.getLogger(MobilePhoneAction.class);
	
	@Resource
	private MusicInfoService musicInfoService;
	@Resource
	private VideoInfoService videoInfoService;
	@Resource
	private BookInfoService bookInfoService;
	
	@Resource(name="softInfoService")
	private AbstractJsonService softJsonService;
	@Resource(name="gameInfoService")
	private AbstractJsonService gameJsonService;
	@Resource(name="softInfoService")
	private IAppService softService;
	@Resource(name="gameInfoService")
	private IAppService gameService;
	@Resource
	private SoftService softServicexiangyang;
	
	/**
	 * 向response写入字符串
	 */
	private String result;
	
	/**
	 * 请求端所请求的资源id
	 */
	private int id;
	/**
	 * 手机二维码下载时所传入的参数
	 * 根据dt的值, 判断请求端是以下哪几种
	 * 1. 软件  2. 游戏  3. 音乐  4. 视频  5. 图片
	 */
	private int dt;
	
	/**
	 * 手机二维码下载时所传入的参数: 
	 * 1. Symbian 2. android 31. iphone破 33. iPhone正 4. windowsMobile  5. java
	 */
	private int ot;
	
	/**
	 * 接收手机端传递过来的要搜索的软件名
	 */
	private String keyword;
	
	
	/**
	 * 判断请求端传入的操作系统类型代号
	 * 1. Symbian 2. android 31. iphone破 33. iPhone正 4. windowsMobile  5. java
	 */
	private int os, osType;
	
	/**
	 * offset:起始查询位置;
	 * pageNo: 第几页, 默认第1页
	 */
	private int offset = 0, pageNo = 1;

	/**
	 * 每页有多少条记录, 默认10
	 */
	private int size = 10, pageSize = 10;
	
	/**
	 * 接收手机端传递过来的搜索请求, 1代表请求搜索软件, 2代表请求搜索游戏
	 */
	private int searchType;
	
	/**
	 * 1应用, 2游戏
	 */
	private int type;
	
	/**
	 * 接收手机端传来的版本号(针对iphone和ANDORID系统的版本号)
	 */
	private String ver, version;
	
	/**
	 * softtype的id
	 */
	private int typeid;
	
	/**
	 * 二维码下载用, 用于传递文件名
	 */
	private String fileName;
	
	private String contentLength;
	
	/*
	 * 接口: 手机通过二维码扫描进入到此接口进行下载
	 * 根据Struts.xml的配置,该方法将调用getInputStream()提供下载
	 */
	public String tmcdl()	{
		return SUCCESS;
	}
	
	/**
	 * 根据手机端传来的参数, 返回对应的资源OutputStream
	 * @return 001 : 数据库中未找到对应ID
	 * 002 : 访问资源中心错误
	 * dt: 资源类型
	 */
	public InputStream getInputStream(){
		InputStream is = null;
		URL url = null;
		URLConnection conn = null;
		switch(dt)	{
			//软件, 需要分辨iphone正版盗版
			case 1 :
				SoftInfo s = softService.findById(id);
				if(s != null)	{
					this.fileName = new File(s.getSoftFileName()).getName();
					try {
						url = new URL(this.HTTP_PATH + s.getSoftFileName());
						conn = url.openConnection();
						this.contentLength = conn.getContentLength() + "";
						is = conn.getInputStream();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
						this.doPrint("网络连接错误");
						return null;
					} 
				} else {
					this.doPrint("您访问的资源已被删除");
					return null;
				}
				softService.persistInstallLog(s);
				return is;
			
			//游戏 , 需要区分iphone正版盗版
			case 2 :
				GameInfo g = gameService.findById(id);
				if(g != null)	{
					this.fileName = new File(g.getSoftFileName()).getName();
					try {
						url = new URL(this.HTTP_PATH + g.getSoftFileName());
						conn = url.openConnection();
						this.contentLength = conn.getContentLength() + "";
						is = conn.getInputStream();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
						this.doPrint("网络连接错误");
						return null;
					}
				} else {
					this.doPrint("您访问的资源已被删除");
					return null;
				}
				gameService.persistInstallLog(g);
				return is;

			//音乐
			case 3 :
				MusicInfo m = musicInfoService.findById(id);
				if(m != null)	{
					this.fileName = new File(m.getMusicFile()).getName();
					try {
						url = new URL(this.HTTP_PATH + m.getMusicFile());
						conn = url.openConnection();
						this.contentLength = conn.getContentLength() + "";
						is = conn.getInputStream();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
						this.doPrint("网络连接错误");
						return null;
					}
				} else {
					this.doPrint("您访问的资源已被删除");
					return null;
				}
			
			//视频
			case 4 :
				VideoInfo v = videoInfoService.findById(id);
				if(v != null)	{
					this.fileName = new File(v.getVideoFile()).getName();
					try {
						url = new URL(this.HTTP_PATH + v.getVideoFile());
						conn = url.openConnection();
						this.contentLength = conn.getContentLength() + "";
						is = conn.getInputStream();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
						this.doPrint("网络连接错误");
						return null;
					}
				} else {
					this.doPrint("您访问的资源已被删除");
					return null;
				}
				return is;
				
			//图片
			case 5 :
				return is;

			default:
				return is;
		}
	}

	/**
	 * ANDROID和IPHONE适配游戏记录条数
	 * @return
	 */
	public String gameCount() {
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			List<GameInfo> list = gameService.find(p);
			result = String.format("{\"count\":\"%s\"}", list.size()); 
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}
	
	/**
	 * ANDROID和IPHONE适配游戏列表
	 * @return
	 */
	public String gameList()	{
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			List<GameInfo> list = gameService.find(p);
			result = gameJsonService.appListToJson(list).toString();
		} catch (InvalidOsVersionException e) {
			e.printStackTrace();
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * ANDROID和IPHONE适配游戏分页列表, 需要传参数PageNo,pageSize, 
	 * @return
	 */
	public String gameListByPage() {
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			List<GameInfo> list = gameService.find(p, pageNo, pageSize);
			result = gameJsonService.appListToJson(list).toString();
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * 音乐记录条数
	 * @return
	 */
	public String musicCount() {
		this.doPrint(String.format("{\"count\":\"%s\"}", 
									musicInfoService.count()));
		return null;
		
	}
	
	/**
	 * 音乐全部列表, 不分页
	 * @return
	 */
	public String musicList() {
		this.doPrint(musicInfoService.listJson());
		return null;
	}
	
	/**
	 * 音乐分页列表, 需要传参pageNo, pageSize
	 * @return
	 */
	public String musicListByPage() {
		this.doPrint(musicInfoService.listJson(pageNo, pageSize));
		return null;
	}
	
	/**
	 * 根据软件或游戏的名称进行搜索
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String search() {
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			switch(searchType)	{
			case 1 :
				List<SoftInfo> list = softService.search(p, keyword);
				result = softJsonService.appListToJson(list).toString();
				break;
			case 2 :
				List<GameInfo> list1 = gameService.search(p, keyword);
				result = gameJsonService.appListToJson(list1).toString();
				break;
			default:
				result = "searchType值有误";
				break;
			}
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * 根据软件或游戏的名称进行搜索, 分页
	 * @return
	 */
	public String searchByPage()	{
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			switch(searchType)	{
			case 0 :
				int phoneos=200;
				switch(osType)	{
				case 1 :
					phoneos = 100;
					break;
				case 2 :
					phoneos = 200;
					break;
				case 31:
					phoneos = 300;
					break;
				case 33:
					phoneos = 300;
					break;
				case 4 :
					phoneos = 400;
					break;
				case 5 :
					phoneos = 500;
					break;
				default:
					throw new InvalidOsVersionException("ostype值有误:[1,2,31,33,4,5]");
			}
				
				int osId= p.getOsid();
				List<Tongyong> list0= softServicexiangyang.listgameandapp(phoneos, osId, keyword, pageNo, pageSize);
				JsonArray array = new JsonArray();
				if(list0!=null){
					for(int i=0;i<list0.size();i++){
						JsonObject json = new JsonObject();
						json.addProperty("softid", list0.get(i).getTongyong1() + "");
						json.addProperty("softname", list0.get(i).getTongyong2());
						json.addProperty("kaifamanu", list0.get(i).getTongyong3() == null ? "" : list0.get(i).getTongyong3());
						json.addProperty("softVersion", list0.get(i).getTongyong4() == null ? "" : list0.get(i).getTongyong4());
						json.addProperty("fileSize", list0.get(i).getTongyong5() == null ? "" : list0.get(i).getTongyong5() + "");
						json.addProperty("softFee", list0.get(i).getTongyong6() == null ? "" : list0.get(i).getTongyong6() + "");
						json.addProperty("softtype",list0.get(i).getTongyong7() == null ? "" :list0.get(i).getTongyong7() + "");
						json.addProperty("shangjiadate", list0.get(i).getTongyong8() == null ? "" :list0.get(i).getTongyong8());
						json.addProperty("softinfo", list0.get(i).getTongyong9() == null ? "" : list0.get(i).getTongyong9());
						json.addProperty("ostype",list0.get(i).getTongyong10() + "");
						json.addProperty("osid", list0.get(i).getTongyong11() == null ? "" : list0.get(i).getTongyong11() + "");
						json.addProperty("suffix",list0.get(i).getTongyong12() == null ? "" : list0.get(i).getTongyong12());
						
						String s1 =list0.get(i).getTongyong13();
						if(s1 == null || "".equals(s1.trim()))
							json.addProperty("iconfilename", "");
						else
							json.addProperty("iconfilename", HTTP_PATH + s1.replace("\\", "/"));
						
						String s2 = list0.get(i).getTongyong14();
						if(s2 == null || "".equals(s2.trim()))
							json.addProperty("picfilename1", "");
						else
							json.addProperty("picfilename1", HTTP_PATH + s2.replace("\\", "/"));
						
						String s3 = list0.get(i).getTongyong15();
						if(s3 == null || "".equals(s3.trim()))
							json.addProperty("picfilename2", "");
						else
							json.addProperty("picfilename2", HTTP_PATH + s3.replace("\\", "/"));
						
						String s4 = list0.get(i).getTongyong16();
						if(s4 == null || "".equals(s4.trim()))
							json.addProperty("picfilename3", "");
						else
							json.addProperty("picfilename3", HTTP_PATH +s4.replace("\\", "/"));
						
						String s5 = list0.get(i).getTongyong17();
						if(s5 == null || "".equals(s5.trim()))
							json.addProperty("softfilename", "");
						else
							json.addProperty("softfilename", HTTP_PATH + s5.replace("\\", "/"));
	
						String s6 =list0.get(i).getTongyong18();
						if(s6 == null || "".equals(s6.trim()))
							json.addProperty("legalfilename", "");
						else
							json.addProperty("legalfilename", HTTP_PATH + s6.replace("\\", "/"));
						
						json.addProperty("softurl", list0.get(i).getTongyong19() == null ? "" : list0.get(i).getTongyong1());
						json.addProperty("hotrecmd", list0.get(i).getTongyong20() == null ? "" : list0.get(i).getTongyong20() + "");
						json.addProperty("downnum", list0.get(i).getTongyong21() == null ? "" :list0.get(i).getTongyong21() + "");
						json.addProperty("installCount", list0.get(i).getTongyong22() == null ? "" : list0.get(i).getTongyong22() + "");
						json.addProperty("userRemarkNum",list0.get(i).getTongyong23() == null ? "" : list0.get(i).getTongyong23()+"");
						json.addProperty("userRemark", list0.get(i).getTongyong24() + "");
						array.add(json);
					}
				}
				
				
				
				result = array.toString();
				
				
				break;
			case 1 :
				List<SoftInfo> list = softService.search(p, keyword, pageNo, pageSize);
				result = softJsonService.appListToJson(list).toString();
				break;
			case 2 :
				List<GameInfo> list1 = gameService.search(p, keyword, pageNo, pageSize);
				result = gameJsonService.appListToJson(list1).toString();
				break;
			default:
				result = "searchType值有误";
				break;
			}
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * 根据软件或游戏的名称进行搜索, 返回找到的结果数
	 * @return
	 */
	public String searchCount()	{
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			@SuppressWarnings("rawtypes")
			List list = null;
			switch(searchType)	{
			case 1 :
				list = softService.search(p, keyword);
				result = String.format("{\"count\":\"%s\"}",list.size());
				break;
			case 2 :
				list = gameService.search(p, keyword);
				result = String.format("{\"count\":\"%s\"}",list.size());
				break;
			default:
				result = "searchType值有误";
				break;
			}
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * 软件记录条数
	 * @return
	 */
	public String softCount() {
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			List<SoftInfo> list = softService.find(p);
			result = String.format("{\"count\":\"%s\"}", list.size());
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * ANDROID和IPHONE适配软件列表
	 * @return
	 */
	public String softList() {
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			List<SoftInfo> list = softService.find(p);
			result = softJsonService.appListToJson(list).toString();
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * ANDROID和IPHONE适配软件分页列表, 需要传参数PageNo,pageSize, 
	 * @return
	 */
	public String softListByPage() {
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(osType, version);
			List<SoftInfo> list = softService.find(p, pageNo, pageSize);
			result = softJsonService.appListToJson(list).toString();
		} catch (InvalidOsVersionException e) {
			result = e.getMessage();
		}
		return doPrint(result);
	}

	/**
	 * 视频记录条数
	 * @return
	 */
	public String videoCount() {
		this.doPrint(String.format("{\"count\":\"%s\"}", videoInfoService.count()));
		return null;
	}

	/**
	 * 视频全部列表, 不分页
	 * @return
	 */
	public String videoList() {
		this.doPrint(videoInfoService.listJson());
		return null;
	}

	/**
	 * 视频分页列表, 需要传参pageNo, pageSize
	 * @return
	 */
	public String videoListByPage() {
		this.doPrint(videoInfoService.listJson(pageNo, pageSize));
		return null;
	}
	
	/**
	 * 图书全部列表, 不分页
	 * @return
	 */
	public String bookList() {
		this.doPrint(bookInfoService.listJson());
		return null;
	}

	/**
	 * 图书分页列表, 需要传参pageNo, pageSize
	 * @return
	 */
	public String bookListByPage() {
		this.doPrint(bookInfoService.listJson(pageNo, pageSize));
		return null;
	}
	
	/**
	 * 软件类型列表
	 * @return
	 */
	public String typeList()	{
		JsonObject json;
		try	{
			switch (type) {
			case 1:
				List<SoftType> list = softService.typeList();
				json = softJsonService.template2(list);
				break;
			case 2:
				List<GameType> glist = gameService.typeList();
				json = softJsonService.template2(glist);
				break;
			default:
				json = softJsonService.template1_fail(1, "type值应为1应用或2游戏");
				break;
			}
		} catch(Exception e)	{
			logger.error(e);
			json = softJsonService.template2_fail(2, "服务器内部错误");
		}
		return doPrint(json.toString());
	}
	
	/**
	 * 根据typeid分页查询
	 * @return
	 */
	public String bytypeid()	{
		JsonObject json;
		PhoneOs p;
		try {
			p = PhoneOs.getInstance(os, ver);
			switch(type)	{
			case 1:
				Pager<SoftInfo> pager = softService.byTypeid(p, typeid, offset, size);
				json = softJsonService.template1(pager);
				break;
			case 2:
				Pager<GameInfo> gpager = gameService.byTypeid(p, typeid, offset, size);
				json = gameJsonService.template1(gpager);
				break;
			default:
				json = softJsonService.template1_fail(1, "type值有误,1或2");
				break;
			}
		} catch (InvalidOsVersionException e) {
			e.printStackTrace();
			json = softJsonService.template1_fail(2, e.getMessage());
		} catch(Exception e)	{
			logger.error(e.getMessage(), e);
			json = softJsonService.template1_fail(3, "服务器内部错误,未知异常");
		}
		return doPrint(json.toString());
	}
	
	
	/***************************************************************************
	 * 以下是访问器
	 **************************************************************************/
	public void setOsType(int osType) {
		this.osType = osType;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}

	public void setDataId(int dataId) {
		this.id = dataId;
	}
	
	public void setDataType(int dataType) {
		this.dt = dataType;
	}
	
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getDataId() {
		return id;
	}

	public int getDataType() {
		return dt;
	}

	public String getKeyword() {
		return keyword;
	}

	public int getOsType() {
		return osType;
	}

	public int getPageNo() {
		return pageNo;
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDt() {
		return dt;
	}

	public void setDt(int dt) {
		this.dt = dt;
	}

	public int getOt() {
		return ot;
	}

	public void setOt(int ot) {
		this.ot = ot;
	}

	public int getSearchType() {
		return searchType;
	}
	
	public String getVersion() {
		return version;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentLength() {
		return contentLength;
	}

	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getOs() {
		return os;
	}

	public void setOs(int os) {
		this.os = os;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}
}
