package com.i8app.install.web.action;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.install.Util;
import com.i8app.install.service.LogService;
import com.opensymphony.xwork2.ActionContext;

/**
 * 短信和二维码的统一下载入口
 * 
 * @author JingYing 2013-6-6
 */
@Controller
@Scope("prototype")
public class DlAction extends BaseAction	{
	private static Logger logger = Logger.getLogger(DlAction.class);
	
	@Resource
	private LogService logService;
	
	/**
	 * url资源静态下载地址; id安装包的UUID; location把浏览器重定向到哪个地址; no手机号;
	 */
	private String url, id, location, no;
	
	
	/**
	 * type下载类型(见installlog表的installType)1:pc端直连;2:蓝牙下载;3:USB连接;
	 * 4:pc客户端短信下载;5:二维码下载;6:web短信下载;7web直接下载;8:wap短信下载;9:wap直接下载;12手机客户端短信下载;13wap手机验证码;14wap短信下发量
	 * lid:installLog的id; empId员工id;proxy(0不代理,1代理)
	 */
	private int type, lid;
	private Integer empId, proxy;
	
	/**
	 * 记录下载日志. 之后根据下载类型, 把短信连接跳转到代理下载地址, 记录是否下载成功.
	 * 把二维码直接重定向到资源静态地址;
	 * @param type installType
	 * @param empId 员工ID
	 * @param lid 日志ID
	 * @param url 最终下载地址
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @deprecated 和installType耦合度过高,被log2代替
	 */
	public String log() throws UnsupportedEncodingException	{
		switch(type)	{	//4和8和12, 且empId有值时才记日志,且使用代理下载模式
			case 4:
			case 8:
			case 12:
				if(empId != null)	{
					logService.updateInstallLogOptStat(lid, 70001);
					String namespace = ActionContext.getContext().getActionInvocation()
										.getProxy().getNamespace().replace("/", "");
					location = String.format("%s%s/proxy.action?lid=%s&url=%s", 
							getBasePath(), //TODO 暂且redirect到本机, 如果以后短信下载量大, proxy要放在一个单独的TOMCAT下, 并将此处修改成配置文件
							namespace, lid, URLEncoder.encode(url, "utf-8"));
					break;
				} 
			default:	//其它情况下不记日志,直接重定向到资源静态地址
				location = url;
		}
		return SUCCESS;
	}
	
	/**
	 * 日志action. 不再生成日志,而是使用前端传递的日志ID
	 * @param lid 日志id
	 * @param url 最终下载地址
	 * @param proxy 是否使用代理下载模式. 0不使用,1使用
	 * @throws UnsupportedEncodingException
	 */
	public String log2() throws UnsupportedEncodingException	{
		if(proxy.intValue() == 1)	{	//使用代理模式
			logService.updateInstallLogOptStat(lid, 70001);
			String namespace = ActionContext.getContext().getActionInvocation()
								.getProxy().getNamespace().replace("/", "");
			location = String.format("%s%s/proxy.action?lid=%s&url=%s", 
					getBasePath(), //TODO 暂且redirect到本机, 如果以后短信下载量大, proxy要放在一个单独的TOMCAT下, 并将此处修改成配置文件
					namespace, lid, URLEncoder.encode(url, "utf-8"));
			return SUCCESS;
			
		} else if(proxy.intValue() == 0)	{//不使用代理模式
			location = url;
			return SUCCESS;
		
		} else	{
			return doPrint("");
		}
	}
	
	/**
	 * 从静态地址中读取流, 返回给手机浏览器
	 * @return
	 */
	public String proxy()	{
		ReadableByteChannel inChan = null;
		WritableByteChannel outChan = null;
		String contentType, contentDisposition;
		int contentLength;

		try {	//同远程地址建立输入流
			HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
			contentType = conn.getContentType();
			contentLength = conn.getContentLength();
			contentDisposition = conn.getHeaderField("Content-Disposition");
			if(contentDisposition == null)	{	//从最终下载地址中截串获取文件名
				String finalUrl = conn.getURL().toString();
				if(finalUrl.contains("/"))	{
					contentDisposition = String.format("attachment;filename=\"%s\"", 
							finalUrl.substring(finalUrl.lastIndexOf("/")+1));
				} else	{
					contentDisposition = "attachment;filename=\"unknown.zip\"";
				}
			}
			
			inChan = Channels.newChannel(conn.getInputStream());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return "404-wap";	//如果远程地址连接失败, 向手机端报告404
		}

		try {//向用户写出流
			HttpServletResponse resp = getResponse();
			resp.setContentType(contentType);
			resp.setContentLength(contentLength);
			resp.setHeader("Content-Disposition", contentDisposition);		//使用最终地址的content-Disposition

			outChan = Channels.newChannel(getResponse().getOutputStream());
			ByteBuffer buf = ByteBuffer.allocate(1024);
			while(inChan.read(buf) != -1)	{
				buf.flip();
				outChan.write(buf);		//出现异常时一般说明用户中断了下载
				buf.clear();
			}
			
			logService.updateInstallLogOptStat(lid, 70002);	//更新日志
		} catch (IOException e) {
			e.printStackTrace();	//不向外抛异常
		} finally	{
			Util.closeStream(inChan, outChan);
		}
		return null;
	}

	
	/**************************************************************************************
	 * 
	 ************************************************************************************/

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLid() {
		return lid;
	}

	public void setLid(int lid) {
		this.lid = lid;
	}

	public void setLogService(LogService logService) {
		this.logService = logService;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public Integer getProxy() {
		return proxy;
	}

	public void setProxy(Integer proxy) {
		this.proxy = proxy;
	}
}
