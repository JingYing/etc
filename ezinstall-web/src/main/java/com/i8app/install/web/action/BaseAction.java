package com.i8app.install.web.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;

import com.i8app.install.Config;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class BaseAction extends ActionSupport 
	implements SessionAware, ServletRequestAware, ServletResponseAware, ServletContextAware {
	public static final String AJAX = "ajax";
	protected String message;
	
	protected String HTTP_PATH, DISK_PATH, AREA_CODE, fileserverUrl;
	
	protected Config config;

	protected int offset;	//offset默认0
	protected Map<String, Object> session;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected ServletContext servletContext;
	
	public String getBasePath()	{
		return String.format("%s://%s:%s%s/",
				getRequest().getScheme(),
				getRequest().getServerName(),
				getRequest().getServerPort(),
				getRequest().getContextPath());
	}
	
	/**
	 * 获取本项目在本地磁盘上的地址
	 * @return
	 */
	public String getRealRootPath()	{
		return getServletContext().getRealPath("/");
	}
	
	/**
	 * 对浏览器的请求返回字符串
	 * @param s 要输出的字符串
	 * @throws IOException
	 */
	public String doPrint(Object obj)	{
		this.getResponse().setContentType("text/html;charset=utf-8");
		PrintWriter pw;
		try {
			pw = this.getResponse().getWriter();
			pw.println(obj.toString());
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param s
	 */
	public void doPrintXml(Object obj)	{
		this.getResponse().setContentType("text/xml;charset=utf-8");
		PrintWriter pw;
		try {
			pw = this.getResponse().getWriter();
			pw.println(obj.toString());
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 将一个本地文件写出至输出流
	 * @param outputStream
	 * @param file
	 */
	public void sendOutputStream(OutputStream outputStream, File file)	{
		BufferedOutputStream bos = null;
		BufferedInputStream bis = null;
		try {
			bos = new BufferedOutputStream(this.getResponse().getOutputStream());
			bis = new BufferedInputStream(new FileInputStream(file));
			int i;
			while((i=bis.read()) != -1)
				bos.write(i);
		} catch (IOException e) {
			e.printStackTrace();
		} finally	{
			try {
				bis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * 将一个本地文件写出至输出流
	 * @param outputStream
	 * @param file
	 */
	public void sendOutputStream(OutputStream outputStream, String filePath)	{
		BufferedOutputStream bos = null;
		BufferedInputStream bis = null;
		try {
			bos = new BufferedOutputStream(this.getResponse().getOutputStream());
			bis = new BufferedInputStream(new FileInputStream(filePath));
			int i;
			while((i=bis.read()) != -1)
				bos.write(i);
		} catch (IOException e) {
			e.printStackTrace();
		} finally	{
			try {
				bis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
//	public int getPageSize(){
//		return Integer.parseInt(getServletContext().getInitParameter("pageSize"));
//	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Resource
	public void setConfig(Config config)	{
		this.config = config;
		this.HTTP_PATH = config.getFileServerUrl();
		this.fileserverUrl = config.getFileServerUrl();
		this.AREA_CODE = config.getAreaCode();
		this.DISK_PATH = config.getFileServerDir();// 添加人：贺芳容， 该字段目前没有地方赋值，所以我先拿这个字段用于存放文件上传的磁盘路径
	}
	
	public String getFileserverUrl() {
		return fileserverUrl;
	}

	public HttpServletResponse getResponse()	{
		return response;
	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
		
	}
	
	public HttpServletRequest getRequest()	{
		return request;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	
	public ServletContext getServletContext()	{
		return servletContext;
	}

	@Override
	public void setServletContext(ServletContext context) {
		this.servletContext = context;
	}
	public String _;
	public String get_() {
		return _;
	}
	public void set_(String _) {
		this._ = _;
	}
	protected HttpSession getSession() {
		return ServletActionContext.getRequest().getSession();
	}
}
