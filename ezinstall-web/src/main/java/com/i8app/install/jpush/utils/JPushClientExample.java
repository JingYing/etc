package com.i8app.install.jpush.utils;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.i8app.install.domain.Tag;
import com.i8app.install.jpush.ErrorCodeEnum;
import com.i8app.install.jpush.IOSExtra;
import com.i8app.install.jpush.JPushClient;
import com.i8app.install.jpush.MessageResult;
/**
 * 参考：http://docs.jpush.cn/display/dev/Java+v2 
 * @author Administrator
 *
 */
public class JPushClientExample {
    //在极光注册上传应用的 appKey 和 masterSecret
	private static final String appKey ="a259be4e66111c65262a0432";////必填，例如466f7032ac604e02fb7bda89
	
	private static final String masterSecret = "96060bc41bc867a7929be1a0";//必填，每个应用都对应一个masterSecret//04809ed319cd34d8
	
	private static JPushClient jpush = null;

	/*
	 * 保存离线的时长。秒为单位。最多支持10天（864000秒）。
	 * 0 表示该消息不保存离线。即：用户在线马上发出，当前不在线用户将不会收到此消息。
	 * 此参数不设置则表示默认，默认为保存1天的离线消息（86400秒
	 */
	private static long timeToLive =  60 * 60 * 24;  

	/**
	 * 		  参数名称	        参数类型	   选项  	      	内容说明
 			masterSecret    String	  必须 	Portal上注册应用时生成的 masterSecret
			appKey			String	必须		Portal上注册应用时生成的 appKey
			timeToLive		long	可选		保存离线消息的时长。秒为单位。最多支持10天（864000秒）。
											0 表示该消息不保存离线。即：用户在线马上发出，当前不在线用户将不会收到此消息。
											此参数不设置则表示默认，默认为保存1天的离线消息（86400秒）。
			DeviceEnum		Enum	可选		指定的设备。
											可选值：DeviceEnum.Android, DeviceEnum.IOS。
											不填或者null值为同时支持 Android 与 iOS。
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Example1: 初始化,默认发送给android和ios，同时设置离线消息存活时间
		 * jpush = new JPushClient(masterSecret, appKey, timeToLive);
		 * 
		 * Example2: 只发送给android
		 * jpush = new JPushClient(masterSecret, appKey, DeviceEnum.Android);
		 * 
		 * Example3: 只发送给IOS
		 * jpush = new JPushClient(masterSecret, appKey, DeviceEnum.IOS);
		 * 
		 * Example4: 只发送给android,同时设置离线消息存活时间
		 * jpush = new JPushClient(masterSecret, appKey, timeToLive, DeviceEnum.Android);
		 */
 		jpush = new JPushClient(masterSecret, appKey, timeToLive);
		
//		jpush = new JPushClient(masterSecret, appKey, timeToLive, DeviceEnum.Android);
		
		/* 
		 * 是否启用ssl安全连接, 可选
		 * 参数：启用true， 禁用false，默认为非ssl连接
		 */
		//jpush.setEnableSSL(true);

		//测试发送消息或者通知
		testSend();
	}

	public static void testSend() {
	    // 在实际业务中，建议 sendNo 是一个你自己的业务可以处理的一个自增数字。
	    // 除非需要覆盖，请确保不要重复使用。详情请参考 API 文档相关说明。
	    Integer num= getRandomSendNo();
	    String sendNo=num+"";
//	    String imei = "A0000040F0DB7F";
		String msgTitle = "你有新的消息";
		String msgContent = "{\"heardname\":\"让狂风暴雨来的更猛烈些吧！\",\"heardurl\":\"http://www.baidu.com\",\"heardicon\":\"http://www.baidu.com\",\"data\":[{\"name\":\"让狂风暴雨来的更猛烈些吧！\",\"url\":\"http://www.baidu.com\",\"icon\":\"http://www.baidu.com\"},{\"name\":\"让狂风暴雨来的更猛烈些吧！\",\"url\":\"http://www.baidu.com\",\"icon\":\"http://www.baidu.com\"},{\"name\":\"让狂风暴雨来的更猛烈些吧！\",\"url\":\"http://www.baidu.com\",\"icon\":\"http://www.baidu.com\"}]}";
		System.out.println(msgContent);
		/*
		 * IOS设备扩展参数,
		 * 设置badge，设置声音
		 */

		Map<String, Object> extra = new HashMap<String, Object>();
		IOSExtra iosExtra = new IOSExtra(1, "WindowsLogonSound.wav");
		extra.put("ios", iosExtra);
		//IOS和安卓一起,所有安装山西联通一点通的用户
//		MessageResult msgResult = jpush.sendNotificationWithAppKey(sendNo, msgTitle, msgContent, 0, extra);
		//安装山西联通一点通的指定用户1：金龙：355136053808211;玉龙：A0000040F0DB7F
//		MessageResult msgResult = jpush.sendCustomMessageWithImei(sendNo, imei, msgTitle, msgContent);

		//alias字符限制40字节
		String alias= "king";
		
		
		//按照别名
//		MessageResult msgResult = jpush.sendNotificationWithAlias(sendNo,alias, msgTitle, msgContent);//发送带Alias的通知
		
//		MessageResult msgResult = jpush.sendNotificationWithAlias(sendNo,alias, msgTitle, msgContent, 0, extra);//自定义通知栏(没有则填写0)	以及传递附属信息
		
//		MessageResult msgResult = jpush.sendCustomMessageWithAlias(sendNo,alias, msgTitle, msgContent);//发送带Alias的消息
		
		MessageResult msgResult = jpush.sendCustomMessageWithAlias(sendNo,"HY012013111900051", msgTitle, msgContent, "msgContentType", extra);//用户自定义消息类型，以及传递附属信息
		
		
		
		
		//按照标签
		
//		String tag="remeng";
		try {
			Tag tag = new Tag();
			tag.setTagname("山西");
			System.out.println("太原");
			System.out.println(new String("太原".getBytes(),"UTF-8"));//18635154082
//		MessageResult msgResult = jpush.sendCustomMessageWithTag(sendNo,"HY012013111900051", "sssdfasdf", "adfasdf", "文本", null);//用户自定义消息类型，以及传递附属信息
		
		//对所有用户发送通知, 更多方法请参考文档
	//	MessageResult msgResult = jpush.sendCustomMessageWithAppKey(sendNo,msgTitle, msgContent);
		
		if (null != msgResult) {
			System.out.println("服务器返回数据: " + msgResult.toString());
			if (msgResult.getErrcode() == ErrorCodeEnum.NOERROR.value()) {
				System.out.println("发送成功， sendNo=" + msgResult.getSendno());
			} else {
				System.out.println("发送失败， 错误代码=" + msgResult.getErrcode() + ", 错误消息=" + msgResult.getErrmsg());
			}
		} else {
			System.out.println("无法获取数据");
		}
		
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
    public static final int MAX = Integer.MAX_VALUE;
	public static final int MIN = (int) MAX/2;
	
	/**
	 * 保持 sendNo 的唯一性是有必要的
	 * It is very important to keep sendNo unique.
	 * @return sendNo
	 */
	public static int getRandomSendNo() {
	    return (int) (MIN + Math.random() * (MAX - MIN));
	}
	
}
