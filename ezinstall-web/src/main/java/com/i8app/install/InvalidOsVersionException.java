package com.i8app.install;

@SuppressWarnings("serial")
public class InvalidOsVersionException extends Exception	{

	public InvalidOsVersionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidOsVersionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidOsVersionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidOsVersionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
