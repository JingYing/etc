package com.i8app.install;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.i8app.install.domain.GameInfo;
import com.i8app.install.domain.SoftInfo;


public enum PhoneOs {

	/**
	 * 手机操作系统的分类, iphone为正版, iphonex为破解版
	 */
	IPHONE(300), IPHONEX(300), ANDROID(200), SYMBIAN(100), WINDOWS_MOBILE(400), JAVA(500);
	
	private String version;

	/**
	 * osid:version和操作系统换算出来的osid
	 * osType: 数据库中定义的操作系统代号
	 */
	private int osid, osType;
	
	/**
	 * 默认构造器
	 */
	private PhoneOs()	{}
	
	/**
	 * 构造器, 向osType赋值
	 * @param osType
	 */
	private PhoneOs(int osType)	{
		this.osType = osType;
	}
	
	/**
	 * 1. Symbian 2. android 31或33. iPhone 4. windowsMobile  5. java
	 * @param osType
	 * @return
	 * @throws InvalidOsVersionException 
	 */
	public static PhoneOs getInstance(int paramOstype, String version) 
							throws InvalidOsVersionException	{
		PhoneOs p;
		switch(paramOstype)	{
			case 1 :
				p = PhoneOs.SYMBIAN;
				break;
			case 2 :
				p = PhoneOs.ANDROID;
				break;
			case 31:
				p = PhoneOs.IPHONEX;
				break;
			case 33:
				p = PhoneOs.IPHONE;
				break;
			case 4 :
				p = PhoneOs.WINDOWS_MOBILE;
				break;
			case 5 :
				p = PhoneOs.JAVA;
				break;
			default:
				throw new InvalidOsVersionException("ostype值有误:[1,2,31,33,4,5]");
		}
		if(version != null)	{
			p.setVersion(version);
			p.setOsid(p.convertToOsid(version));
		}
		return p;
	}
	
	
	/**
	 * 用于IPHONE和ANDROID适配时的版本号转换
	 * @param version 操作系统版本号
	 * @param PhoneOs 操作系统类型
	 * @return
	 */
	private int convertToOsid(String version) throws InvalidOsVersionException	{
		int num = version.split("[.]").length;
		for (int i = 0; i < 5 - num; i++) {
			version += ".0";
		}
		String[] strings = version.split("[.]");
		int sum = 0;
		try	{
			for (int i = 0, j = strings.length - 1; i < strings.length; i++, j--) {
				sum += Integer.parseInt(strings[i]) * Math.pow(50, j);
			}
		} catch(NumberFormatException e)	{
			e.printStackTrace();
			throw new InvalidOsVersionException();
		}
		switch (this) {
			case IPHONE:
			case IPHONEX:
				return sum + 400000000;
			case ANDROID:
				return sum;
			case WINDOWS_MOBILE:
				// TODO 转换规则待定
				return sum + 800000000;
			default:
				throw new UnsupportedOperationException("该操作系统不支持转换为osid:" + this);
		}
	}

	/**
	 * 各个操作系统生成对应的detachedCriteria
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	public <T> DetachedCriteria createCriteria(Class<T> clazz)	{
		if(clazz != SoftInfo.class && clazz != GameInfo.class)
			throw new IllegalArgumentException("clazz取值有误");
		
		DetachedCriteria dc = DetachedCriteria.forClass(clazz)
		.createAlias("osInfo", "oi")
		.add(Restrictions.lt("oi.id", osid))
		.addOrder(Order.desc("id"));//按ID降序排

		switch(this)	{
			case IPHONE : // 正版
				dc.add(Restrictions.eq("osType", osType));
				dc.add(Restrictions.isNotNull("legalFileName"));
				break;
			case IPHONEX: // 破解
				dc.add(Restrictions.eq("osType", osType));
				dc.add(Restrictions.isNotNull("softFileName"));
				break;
			case ANDROID:
				dc.add(Restrictions.eq("osType", osType));
				break;
			default:
				break;
		}
		return dc;
	}
	
	
	
	public <T> DetachedCriteria createCriteriagongyong(Class<T> clazz)	{
		if(clazz != SoftInfo.class && clazz != GameInfo.class)
			throw new IllegalArgumentException("clazz取值有误");
		
		DetachedCriteria dc = DetachedCriteria.forClass(clazz)
		.createAlias("osInfo", "oi")
		.add(Restrictions.lt("oi.id", osid));

		switch(this)	{
			case IPHONE : // 正版
				dc.add(Restrictions.eq("osType", osType));
				dc.add(Restrictions.isNotNull("legalFileName"));
				break;
			case IPHONEX: // 破解
				dc.add(Restrictions.eq("osType", osType));
				dc.add(Restrictions.isNotNull("softFileName"));
				break;
			case ANDROID:
				dc.add(Restrictions.eq("osType", osType));
				break;
			default:
				break;
		}
		return dc;
	}
	
	
	public String getVersion() {
		return version;
	}

	private void setVersion(String version) {
		this.version = version;
	}

	public int getOsid() {
		return osid;
	}
	
	private void setOsid(int osid) {
		this.osid = osid;
	}

	public int getOsType() {
		return osType;
	}
}
