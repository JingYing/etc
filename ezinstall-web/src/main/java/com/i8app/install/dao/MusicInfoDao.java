package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

import com.i8app.install.dao.MusicInfoDao;
import com.i8app.install.domain.MusicInfo;
/**
 * MusicInfo的Dao类
 * @author JingYing
 *
 */
@SuppressWarnings("rawtypes")
@Component("musicInfoDao")
public class MusicInfoDao extends BaseDao{

	public void addOrUpdate(MusicInfo musicInfo) {
		this.getHibernateTemplate().saveOrUpdate(musicInfo);
	}

	public void del(int id) {
		this.getHibernateTemplate().delete(this.findById(id));
	}

	public MusicInfo findById(int id) {
		return (MusicInfo)this.getHibernateTemplate().get(MusicInfo.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<MusicInfo> list() {
		return this.getHibernateTemplate().find("from MusicInfo");
	}
	
	@SuppressWarnings("unchecked")
	public List<MusicInfo> list(final String musicName, final String singerName,
									final String specialName, final int musicTypeId )	{
		return (List<MusicInfo>)this.getHibernateTemplate().execute(new HibernateCallback(){
			
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				StringBuilder sb = new StringBuilder();
				sb.append("from MusicInfo m where 1=1 ");
				if(musicName != null && !"".equals(musicName))
					sb.append(String.format("and m.musicName like '%%%s%%'", musicName));
				if(singerName != null && !"".equals(singerName))
					sb.append(String.format(" and m.singerName like '%%%s%%'", singerName));
				if(specialName != null && !"".equals(specialName))
					sb.append(String.format(" and m.specialName like '%%%s%%'", specialName));
				if(musicTypeId != 0)
					sb.append(String.format(" and m.musicType.musicTypeId = '%s'",musicTypeId));
				return session.createQuery(sb.toString()).list();
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<MusicInfo> list(int pageNo, final int pageSize) {
		final int begin = (pageNo - 1) * pageSize;
		return (List<MusicInfo>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						return session.createQuery("from MusicInfo")
								.setFirstResult(begin).setMaxResults(pageSize)
								.list();
					}
				});
	}
 
	public int count() {
		return list().size();
	}

}
