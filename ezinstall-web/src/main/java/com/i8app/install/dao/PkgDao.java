package com.i8app.install.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.install.domain.Pkg;


@Repository
public class PkgDao extends BaseDao	{
	
	public List<Pkg> findByGroupId(int groupId)	{
		String hql = "from Pkg where groupId = ? and isDeleted = 0 order by num desc";
		return getHibernateTemplate().find(hql, groupId);
	}
	

}
