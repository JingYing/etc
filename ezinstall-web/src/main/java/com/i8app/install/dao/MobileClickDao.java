package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.i8app.install.Pager;
import com.i8app.install.domain.EnterpriseInfo;
import com.i8app.install.domain.Messagelog;
import com.i8app.install.domain.Subscription;
import com.i8app.install.domain.Tag;
/**
 * @author LiXiangyang 2014-3-5
 */
@SuppressWarnings("rawtypes")
@Repository
public class MobileClickDao extends BaseDao {
	/**
	 * 查询企业服务号
	 * @param name
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Pager<EnterpriseInfo> pagerlist(final String name,final String status,final int pageNo,final int pageSize) {
		final StringBuffer sql = new StringBuffer();
		sql.append("from EnterpriseInfo w where 1=1");
		final List<String> paramList = new ArrayList<String>();
		if(status != null && !"".equals(status)){
			sql.append(" and status=0 and id !=1 ");
		}
		if(name != null && !"".equals(name)){
			sql.append(" and (w.nickname like ? or w.enterpriseName like ?) ");
//			paramList.add(name);
			paramList.add("%"+name+"%");
			paramList.add("%"+name+"%");
		}
		sql.append(" order by w.id ASC");
		List<EnterpriseInfo> softList = getHibernateTemplate().executeFind(new HibernateCallback(){
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "select w " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.setFirstResult(pageNo).setMaxResults(pageSize).list();
			}
		});
		Long total = (Long)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException, SQLException{
				String hql = "select count(*) " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.uniqueResult();
			}
		});
		Pager<EnterpriseInfo> pager = new Pager<EnterpriseInfo>();
		pager.setList(softList);
		pager.setTotal(total.intValue());
		return pager;
	}
	
	
	/**
	 * 查询用户标签
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Pager<Tag> pagerTaglist(final String name,final int pageNo,final int pageSize) {
		final StringBuffer sql = new StringBuffer();
		sql.append("from Tag w ");
		final List<String> paramList = new ArrayList<String>();
		if(name != null && !"".equals(name)){
			sql.append("  where w.tagname like ? ");
			paramList.add("%"+name+"%");
		}
		sql.append(" order by w.id ASC");
		List<Tag> softList = getHibernateTemplate().executeFind(new HibernateCallback(){
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "select w " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.setFirstResult(pageNo).setMaxResults(pageSize).list();
			}
		});
		Long total = (Long)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException, SQLException{
				String hql = "select count(*) " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.uniqueResult();
			}
		});
		Pager<Tag> pager = new Pager<Tag>();
		pager.setList(softList);
		pager.setTotal(total.intValue());
		return pager;
	}
	
	/**
	 * 查询已订阅服务号
	 * @param name
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Pager<Subscription> pagerSubscribedlist(final String pid,final int pageNo,final int pageSize) {
		final StringBuffer sql = new StringBuffer();
		sql.append("from Subscription w where w.eid.id !=1 and w.peopleID.id=?");
		final List<String> paramList = new ArrayList<String>();
			paramList.add(pid);
		List<Subscription> softList = getHibernateTemplate().executeFind(new HibernateCallback(){
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "select w " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.setFirstResult(pageNo).setMaxResults(pageSize).list();
			}
		});
		Long total = (Long)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException, SQLException{
				String hql = "select count(*) " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.uniqueResult();
			}
		});
		Pager<Subscription> pager = new Pager<Subscription>();
		pager.setList(softList);
		pager.setTotal(total.intValue());
		return pager;
	}
	
	@SuppressWarnings("unchecked")
	public Pager<Messagelog> pagerMessageloglist(final int sid,final int aid,final int mid,final int isdelete,final String starttime,final String endtime,final int pageNo,final int pageSize) {
		final StringBuffer sql = new StringBuffer();
		sql.append("from Messagelog m where 1=1 ");
		final List<Object> paramList = new ArrayList<Object>();
		if(sid !=0){
			sql.append("  AND m.sendtype=? ");
			paramList.add(sid);
		}
		if(aid !=0){
			sql.append("  AND m.acceptedtype=? ");
			paramList.add(aid);
		}
		if(mid !=0){
			sql.append("  AND m.messagetype=? ");
			paramList.add(mid);
		}
		if(isdelete !=0){
			sql.append("  AND m.isdelete=? ");
			paramList.add(isdelete);
		}
		if(starttime !=null && !"".equals(starttime)){
			sql.append("  AND m.sendtime>=? ");
			paramList.add(starttime);
		}
		if(endtime !=null && !"".equals(endtime)){
			sql.append("  AND m.sendtime<=? ");
			paramList.add(endtime);
		}
		sql.append("   ORDER BY m.sendtime desc");
		List<Messagelog> softList = getHibernateTemplate().executeFind(new HibernateCallback(){
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "select m " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.setFirstResult(pageNo).setMaxResults(pageSize).list();
			}
		});
		Long total = (Long)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException, SQLException{
				String hql = "select count(*) " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.uniqueResult();
			}
		});
		Pager<Messagelog> pager = new Pager<Messagelog>();
		pager.setList(softList);
		pager.setTotal(total.intValue());
		return pager;
	}
	
	/**
	 * 添加企业服务号
	 * @param sub
	 */
	public void addOrUpdatePrise(EnterpriseInfo ent) {
		this.getHibernateTemplate().saveOrUpdate(ent);
	}
	
	/**
	 * 添加服务号订阅信息
	 * @param sub
	 */
	public void addOrUpdateSub(Subscription sub) {
		this.getHibernateTemplate().saveOrUpdate(sub);
	}

	/**
	 * 添加服务标签
	 * @param sub
	 */
	public void addOrUpdateTag(Tag tag) {
		this.getHibernateTemplate().saveOrUpdate(tag);
	}
	/**
	 * 添加推送日志
	 * @param tag
	 */
	public void addOrUpdateLog(Messagelog tag) {
		this.getHibernateTemplate().saveOrUpdate(tag);
	}

	/**
	 * 删除订阅
	 * @param id
	 */
	public void delSubscription(int id) {
		this.getHibernateTemplate().delete(findSubscriptionById(id));
	}
	public void delEnterpriseInfo(int id) {
		this.getHibernateTemplate().delete(findEnterpriseInfoById(id));
	}
	public void delTag(int id) {
		this.getHibernateTemplate().delete(findTagById(id));
	}
	/**
	 * 查询订阅详情
	 * @param id
	 * @return
	 */
	public Subscription findSubscriptionById(int id) {
		return (Subscription)this.getHibernateTemplate().get(Subscription.class, id);
	}
	/**
	 * 查询企业服务号详情
	 * @param id
	 * @return
	 */
	public EnterpriseInfo findEnterpriseInfoById(int id) {
		return (EnterpriseInfo)this.getHibernateTemplate().get(EnterpriseInfo.class, id);
	}
	
	public Tag findTagById(int id) {
		return (Tag)this.getHibernateTemplate().get(Tag.class, id);
	}
	
	/**
	 * 查询个人是否订阅过该企业服务号
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int findByPersion(final int eid,final String pid) {
		Long count =  (Long)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("select count(*) from Subscription where eid.id = ? and peopleID.id=?")
				.setInteger(0, eid).setString(1, pid).uniqueResult();
			}
		});
		return count.intValue();
	}
	
	public Subscription findByPidAndEid(final int eid,final String pid) {
		Subscription count =  getHibernateTemplate().execute(new HibernateCallback<Subscription>(){
			public Subscription doInHibernate(Session session)throws HibernateException,SQLException{
				return (Subscription) session.createQuery("from Subscription where eid.id = ? and peopleID.id=?")
				.setInteger(0, eid).setString(1, pid).uniqueResult();
			}
		});
		return count;
	}
	/**]
	 * 临时使用
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<EnterpriseInfo> findEnterpriseList(){
		return this.getHibernateTemplate().find("from EnterpriseInfo where status=0 and id!=1");
	}
	/**]
	 * 临时使用
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Tag> findTagList(int pid){
		return this.getHibernateTemplate().find("from Tag t where t.pid=?",pid);
	}
}
