package com.i8app.install.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
//import org.hibernate.criterion.Projections;
//import org.hibernate.util.SerializationHelper;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

//import com.i8app.install.Pager;
import com.i8app.install.domain.SoftInfo;
import com.i8app.install.domain.Tongyong;
/**
 * SoftInfo的Dao类
 * @author JingYing
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
@Component("softInfoDao")
public class SoftInfoDao extends BaseDao{

	public void addOrUpdate(SoftInfo softInfo) {
		this.getHibernateTemplate().saveOrUpdate(softInfo);
	}

	public int count() {
		return list().size();
	}

	public void del(int id) {
		this.getHibernateTemplate().delete(findById(id));
	}

	public SoftInfo findById(int id) {
		return (SoftInfo)this.getHibernateTemplate().get(SoftInfo.class, id);
	}

	public List<SoftInfo> list() {
		return this.getHibernateTemplate().find("from SoftInfo");
	}
	
	public List<SoftInfo> list(int pageNo, final int pageSize) {
		final int begin = (pageNo-1) * pageSize;
		return (List<SoftInfo>) this.getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				return session.createQuery("from SoftInfo").setFirstResult(begin).setMaxResults(pageSize).list();
			}
		});
	}

	public List<SoftInfo> findByCriteria(DetachedCriteria dc) {
		return this.getHibernateTemplate().findByCriteria(dc);
	}
	
	public List<SoftInfo> findByCriteria(DetachedCriteria dc, int pageNo, int pageSize)	{
		int begin = (pageNo - 1) * pageSize;
		return this.getHibernateTemplate().findByCriteria(dc, begin, pageSize); //起始查询数为0, 不是1
	}
	
	
	/**
	 * 下载次数+1
	 */
	public void updateDownNum(SoftInfo soft)	{
		Integer num = soft.getDownNum();
		if(num == null)		num = 0;
		soft.setDownNum(++ num);
		this.getHibernateTemplate().saveOrUpdate(soft);
	}

	
	//山西移动手机客户端精彩推荐接口
	@SuppressWarnings("deprecation")
	public List<Tongyong> listSoftByShortName(String areacode, Integer dtId,Integer osType,Integer osId,int pageNo,int pageSize) {
		List<Tongyong> softNameList = new ArrayList<Tongyong>();
		if(osType!=200 && osType!=300){
			String sql = "SELECT " +
			"s.softid," +
			"s.softname," +
			"s.kaifamanu," +
			"s.softVersion," +
			"s.fileSize," +
			"s.softFee," +
			"s.softtype," +
			"s.shangjiadate," +
			"s.softinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.softfilename," +
			"s.legalfilename," +
			"s.softurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM tophotappinfo t JOIN softinfo s ON (t.softid=s.softid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=? ORDER BY t.hotOrder DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				pstat.setInt(4, (pageNo-1)*pageSize);
				pstat.setInt(5, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			"s.softid," +
			"s.softname," +
			"s.kaifamanu," +
			"s.softVersion," +
			"s.fileSize," +
			"s.softFee," +
			"s.softtype," +
			"s.shangjiadate," +
			"s.softinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.softfilename," +
			"s.legalfilename," +
			"s.softurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM tophotappinfo t JOIN softinfo s ON (t.softid=s.softid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=? and s.osid <= ? ORDER BY t.hotOrder DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				pstat.setInt(4, osId);
				pstat.setInt(5, (pageNo-1)*pageSize);
				pstat.setInt(6, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return softNameList;
	}
	
	@SuppressWarnings("deprecation")
	public Tongyong listSoftByShortNamecount(String areacode, Integer dtId,Integer osType,Integer osId) {
		Tongyong ty = new Tongyong();
		if(osType!=200 && osType!=300){
			String sql = "SELECT " +
			" count(*) " +
			" FROM tophotappinfo t JOIN softinfo s ON (t.softid=s.softid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				res = pstat.executeQuery();
				while (res.next()) {
					ty.setTongyong1(res.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			" count(*) " +
			" FROM tophotappinfo t JOIN softinfo s ON (t.softid=s.softid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=?  and s.osid <= ? ";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				pstat.setInt(4, osId);
				res = pstat.executeQuery();
				while (res.next()) {
					ty.setTongyong1(res.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return ty;
	}
	@SuppressWarnings("deprecation")
	public List<Tongyong> listgameByShortName(String areacode, Integer dtId,Integer osType,Integer osId,int pageNo,int pageSize) {
		List<Tongyong> softNameList = new ArrayList<Tongyong>();
		if(osType !=200 && osType!=300){
			String sql = "SELECT " +
			"s.GID," +
			"s.gamename," +
			"s.kaifamanu," +
			"s.gameVersion," +
			"s.fileSize," +
			"s.gameFee," +
			"s.gametype," +
			"s.shangjiadate," +
			"s.gameinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.gamefilename," +
			"s.legalfilename," +
			"s.gameurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM tophotappinfo t JOIN gameinfo s ON (t.softid=s.gid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=? ORDER BY t.hotOrder DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				pstat.setInt(4, (pageNo-1)*pageSize);
				pstat.setInt(5, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			"s.GID," +
			"s.gamename," +
			"s.kaifamanu," +
			"s.gameVersion," +
			"s.fileSize," +
			"s.gameFee," +
			"s.gametype," +
			"s.shangjiadate," +
			"s.gameinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.gamefilename," +
			"s.legalfilename," +
			"s.gameurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM tophotappinfo t JOIN gameinfo s ON (t.softid=s.gid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=?  and s.osid <= ? ORDER BY t.hotOrder DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				pstat.setInt(4, osId);
				pstat.setInt(5, (pageNo-1)*pageSize);
				pstat.setInt(6, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return softNameList;
	}
	@SuppressWarnings("deprecation")
	public Tongyong listgameByShortNamecount(String areacode, Integer dtId,Integer osType,Integer osId) {
		Tongyong ty = new Tongyong();
		if(osType !=200 && osType !=300){
			String sql = "SELECT " +
			" count(*) " +
			" FROM tophotappinfo t JOIN gameinfo s ON (t.softid=s.gid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=? ";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				res = pstat.executeQuery();
				while (res.next()) {
					ty.setTongyong1(res.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			" count(*) " +
			" FROM tophotappinfo t JOIN gameinfo s ON (t.softid=s.gid) " +
			"WHERE t.areacode=? AND t.dtid=? and s.ostype=?  and s.osid <= ? ";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, areacode);
				pstat.setInt(2, dtId);
				pstat.setInt(3, osType);
				pstat.setInt(4, osId);
				res = pstat.executeQuery();
				while (res.next()) {
					ty.setTongyong1(res.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return ty;
	}
	
	//软件更新
	@SuppressWarnings("deprecation")
	public List<Tongyong> listSoftByNew(Integer osType,Integer osId,int pageNo,int pageSize) {
		List<Tongyong> softNameList = new ArrayList<Tongyong>();
		if(osType!=200 && osType!=300){
			String sql = "SELECT " +
			"s.softid," +
			"s.softname," +
			"s.kaifamanu," +
			"s.softVersion," +
			"s.fileSize," +
			"s.softFee," +
			"s.softtype," +
			"s.shangjiadate," +
			"s.softinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.softfilename," +
			"s.legalfilename," +
			"s.softurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM  softinfo s " +
			"WHERE s.ostype=? ORDER BY s.shangjiadate DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, (pageNo-1)*pageSize);
				pstat.setInt(3, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			"s.softid," +
			"s.softname," +
			"s.kaifamanu," +
			"s.softVersion," +
			"s.fileSize," +
			"s.softFee," +
			"s.softtype," +
			"s.shangjiadate," +
			"s.softinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.softfilename," +
			"s.legalfilename," +
			"s.softurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM  softinfo s " +
			"WHERE  s.ostype=? and s.osid <= ? ORDER BY s.shangjiadate DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, osId);
				pstat.setInt(3, (pageNo-1)*pageSize);
				pstat.setInt(4, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return softNameList;
	}
	//游戏更新
	@SuppressWarnings("deprecation")
	public List<Tongyong> listgameByNew(Integer osType,Integer osId,int pageNo,int pageSize) {
		List<Tongyong> softNameList = new ArrayList<Tongyong>();
		if(osType !=200 && osType!=300){
			String sql = "SELECT " +
			"s.GID," +
			"s.gamename," +
			"s.kaifamanu," +
			"s.gameVersion," +
			"s.fileSize," +
			"s.gameFee," +
			"s.gametype," +
			"s.shangjiadate," +
			"s.gameinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.gamefilename," +
			"s.legalfilename," +
			"s.gameurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM gameinfo s " +
			"WHERE  s.ostype=? ORDER BY s.shangjiadate DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, (pageNo-1)*pageSize);
				pstat.setInt(3, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			"s.GID," +
			"s.gamename," +
			"s.kaifamanu," +
			"s.gameVersion," +
			"s.fileSize," +
			"s.gameFee," +
			"s.gametype," +
			"s.shangjiadate," +
			"s.gameinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.gamefilename," +
			"s.legalfilename," +
			"s.gameurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM  gameinfo s " +
			"WHERE s.ostype=?  and s.osid <= ? ORDER BY s.shangjiadate DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, osId);
				pstat.setInt(3, (pageNo-1)*pageSize);
				pstat.setInt(4, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return softNameList;
	}
	
	
	
	
	@SuppressWarnings("deprecation")
	public List<Tongyong> listSoftByUpdate(Integer osType,Integer osId,String name) {
		List<Tongyong> softNameList = new ArrayList<Tongyong>();
		if(osType!=200 && osType!=300){
			String sql = "SELECT " +
			"s.softid," +
			"s.softname," +
			"s.kaifamanu," +
			"s.softVersion," +
			"s.fileSize," +
			"s.softFee," +
			"s.softtype," +
			"s.shangjiadate," +
			"s.softinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.softfilename," +
			"s.legalfilename," +
			"s.softurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM  softinfo s " +
			"WHERE s.ostype=? and s.softname like ? ORDER BY s.shangjiadate DESC";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setString(2, '%'+name+'%');
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			"s.softid," +
			"s.softname," +
			"s.kaifamanu," +
			"s.softVersion," +
			"s.fileSize," +
			"s.softFee," +
			"s.softtype," +
			"s.shangjiadate," +
			"s.softinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.softfilename," +
			"s.legalfilename," +
			"s.softurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM  softinfo s " +
			"WHERE  s.ostype=? and s.osid <= ?  and s.softname like ? ORDER BY s.shangjiadate DESC";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, osId);
				pstat.setString(3, '%'+name+'%');
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return softNameList;
	}
	
	
	//游戏更新提示
	@SuppressWarnings("deprecation")
	public List<Tongyong> listgameByUpdate(Integer osType,Integer osId,String name) {
		List<Tongyong> softNameList = new ArrayList<Tongyong>();
		if(osType !=200 && osType!=300){
			String sql = "SELECT " +
			"s.GID," +
			"s.gamename," +
			"s.kaifamanu," +
			"s.gameVersion," +
			"s.fileSize," +
			"s.gameFee," +
			"s.gametype," +
			"s.shangjiadate," +
			"s.gameinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.gamefilename," +
			"s.legalfilename," +
			"s.gameurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM gameinfo s " +
			"WHERE  s.ostype=?  and s.gamename like ? ORDER BY s.shangjiadate DESC";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setString(2, '%'+name +'%');
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT " +
			"s.GID," +
			"s.gamename," +
			"s.kaifamanu," +
			"s.gameVersion," +
			"s.fileSize," +
			"s.gameFee," +
			"s.gametype," +
			"s.shangjiadate," +
			"s.gameinfo," +
			"s.ostype," +
			"s.osid," +
			"s.suffixid," +
			"s.iconfilename," +
			"s.picfilename1," +
			"s.picfilename2," +
			"s.picfilename3," +
			"s.gamefilename," +
			"s.legalfilename," +
			"s.gameurl," +
			"s.hotrecmd," +
			"s.downnum," +
			"s.installCount," +
			"s.userRemarkNum," +
			"s.userRemark " +
			" FROM  gameinfo s " +
			"WHERE s.ostype=?  and s.osid <= ?  and s.gamename like ? ORDER BY s.shangjiadate DESC";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, osId);
				pstat.setString(3, '%'+name+'%');
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return softNameList;
	}
	
	
	
	public List<Tongyong> listgameandapp(Integer osType,Integer osId,String name,int pageNo,int pageSize) {
		List<Tongyong> softNameList = new ArrayList<Tongyong>();
		if(osType !=200 && osType!=300){
			String sql = "SELECT allsoft.softid,allsoft.softname,allsoft.kaifamanu,allsoft.softVersion,allsoft.fileSize,allsoft.softFee,allsoft.softtype,allsoft.shangjiadate,allsoft.softinfo," +
					"allsoft.ostype,allsoft.osid,allsoft.suffixid,allsoft.iconfilename,allsoft.picfilename1,allsoft.picfilename2,allsoft.picfilename3,allsoft.softfilename,allsoft.legalfilename," +
					"allsoft.softurl,allsoft.hotrecmd,allsoft.downnum,allsoft.installCount,allsoft.userRemarkNum,allsoft.userRemark FROM (" +
					"SELECT s.softid,s.softname,s.kaifamanu,s.softVersion,s.fileSize,s.softFee,s.softtype,s.shangjiadate,s.softinfo," +
					"s.ostype,s.osid,s.suffixid,s.iconfilename,s.picfilename1,s.picfilename2,s.picfilename3,s.softfilename,s.legalfilename," +
					"s.softurl,s.hotrecmd,s.downnum,s.installCount,s.userRemarkNum,s.userRemark  FROM  softinfo s " +
					"WHERE  s.ostype=? " +
					"UNION " +
					"SELECT s.GID,s.gamename,s.kaifamanu,s.gameVersion,s.fileSize,s.gameFee,s.gametype,s.shangjiadate,s.gameinfo,s.ostype,s.osid,s.suffixid,s.iconfilename,s.picfilename1,s.picfilename2,s.picfilename3," +
					"s.gamefilename,s.legalfilename,s.gameurl,s.hotrecmd,s.downnum,s.installCount,s.userRemarkNum,s.userRemark FROM gameinfo s " +
					"WHERE  s.ostype=? " +
					") AS allsoft WHERE allsoft.softname LIKE ? " +
					"ORDER BY allsoft.shangjiadate DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			@SuppressWarnings("deprecation")
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, osType);
				pstat.setString(3, '%'+name +'%');
				pstat.setInt(4, (pageNo-1)*pageSize);
				pstat.setInt(5, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			String sql = "SELECT allsoft.softid,allsoft.softname,allsoft.kaifamanu,allsoft.softVersion,allsoft.fileSize,allsoft.softFee,allsoft.softtype,allsoft.shangjiadate,allsoft.softinfo," +
					"allsoft.ostype,allsoft.osid,allsoft.suffixid,allsoft.iconfilename,allsoft.picfilename1,allsoft.picfilename2,allsoft.picfilename3,allsoft.softfilename,allsoft.legalfilename," +
					"allsoft.softurl,allsoft.hotrecmd,allsoft.downnum,allsoft.installCount,allsoft.userRemarkNum,allsoft.userRemark FROM (" +
					"SELECT s.softid,s.softname,s.kaifamanu,s.softVersion,s.fileSize,s.softFee,s.softtype,s.shangjiadate,s.softinfo," +
					"s.ostype,s.osid,s.suffixid,s.iconfilename,s.picfilename1,s.picfilename2,s.picfilename3,s.softfilename,s.legalfilename," +
					"s.softurl,s.hotrecmd,s.downnum,s.installCount,s.userRemarkNum,s.userRemark  FROM  softinfo s " +
					"WHERE  s.ostype=? AND s.osid <= ? " +
					"UNION " +
					"SELECT s.GID,s.gamename,s.kaifamanu,s.gameVersion,s.fileSize,s.gameFee,s.gametype,s.shangjiadate,s.gameinfo,s.ostype,s.osid,s.suffixid,s.iconfilename,s.picfilename1,s.picfilename2,s.picfilename3," +
					"s.gamefilename,s.legalfilename,s.gameurl,s.hotrecmd,s.downnum,s.installCount,s.userRemarkNum,s.userRemark FROM gameinfo s " +
					"WHERE  s.ostype=?  AND s.osid <= ? " +
					") AS allsoft WHERE allsoft.softname LIKE ? " +
					"ORDER BY allsoft.shangjiadate DESC LIMIT ?,?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			@SuppressWarnings("deprecation")
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setInt(1, osType);
				pstat.setInt(2, osId);
				pstat.setInt(3, osType);
				pstat.setInt(4, osId);
				pstat.setString(5, '%'+name+'%');
				pstat.setInt(6, (pageNo-1)*pageSize);
				pstat.setInt(7, pageSize);
				res = pstat.executeQuery();
				while (res.next()) {
					Tongyong ty = new Tongyong();
					ty.setTongyong1(res.getString(1));
					ty.setTongyong2(res.getString(2));
					ty.setTongyong3(res.getString(3));
					ty.setTongyong4(res.getString(4));
					ty.setTongyong5(res.getString(5));
					ty.setTongyong6(res.getString(6));
					ty.setTongyong7(res.getString(7));
					ty.setTongyong8(res.getString(8));
					ty.setTongyong9(res.getString(9));
					ty.setTongyong10(res.getString(10));
					ty.setTongyong11(res.getString(11));
					ty.setTongyong12(res.getString(12));
					ty.setTongyong13(res.getString(13));
					ty.setTongyong14(res.getString(14));
					ty.setTongyong15(res.getString(15));
					ty.setTongyong16(res.getString(16));
					ty.setTongyong17(res.getString(17));
					ty.setTongyong18(res.getString(18));
					ty.setTongyong19(res.getString(19));
					ty.setTongyong22(res.getString(20));
					ty.setTongyong21(res.getString(21));
					ty.setTongyong22(res.getString(22));
					ty.setTongyong23(res.getString(23));
					ty.setTongyong24(res.getString(24));
					softNameList.add(ty);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return softNameList;
	}
	
	
	
	@SuppressWarnings("deprecation")
	public String findSoftByuuid(String uuid) {
		String ty = "";
			String sql = "SELECT b.name FROM app_install_pack a LEFT JOIN app b ON a.appId=b.uuid WHERE a.uuid=?";
			Session s = this.getHibernateTemplate().getSessionFactory()
			.getCurrentSession();
			Connection conn = s.connection();
			PreparedStatement pstat = null;
			ResultSet res = null;
			try {
				pstat = conn.prepareStatement(sql);
				pstat.setString(1, uuid);
				res = pstat.executeQuery();
				while (res.next()) {
					ty = res.getString(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (res != null)
						res.close();
					if (pstat != null)
						pstat.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		return ty;
	}
}
