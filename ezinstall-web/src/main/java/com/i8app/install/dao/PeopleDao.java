package com.i8app.install.dao;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.i8app.install.domain.App;
import com.i8app.install.domain.App_Comment;
import com.i8app.install.domain.ClientActivityLog;
import com.i8app.install.domain.ClientConfigInfo;
import com.i8app.install.domain.ContactBakData;
import com.i8app.install.domain.FeedbackInfo;
import com.i8app.install.domain.Guessyoulike;
import com.i8app.install.domain.Mobile_installed_list;
import com.i8app.install.domain.PeopleInfo;
import com.i8app.install.domain.Signalqualitydata;
import com.i8app.install.domain.Signalrequest;
import com.i8app.install.domain.Sms_backup;
import com.i8app.install.domain.Smsspam;
import com.i8app.install.domain.Threads;
import com.i8app.install.domain.User_uninstall_log;

@Repository
public class PeopleDao extends BaseDao {

	public void saveOrUpdatePeople(PeopleInfo p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateFeeback(FeedbackInfo p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateUninstall(User_uninstall_log p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public void saveOrUpdateClientConfigInfo(ClientConfigInfo p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateSignalrequest(Signalrequest p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateSignalqualitydata(Signalqualitydata p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public void saveOrUpdateApp(App p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public void saveOrUpdateContactBakData(ContactBakData p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void delteleMobile_installed_list(Mobile_installed_list mobile){
		getHibernateTemplate().delete(mobile);
	}
	public void saveOrUpdateSms(Sms_backup p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateThreads(Threads p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateApp_Comment(App_Comment p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public void saveOrUpdateSmsspam(Smsspam p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public Threads findBythreadid(int id) {
		return getHibernateTemplate().get(Threads.class, id);
	}
	public void saveOrUpdateMobile_installed_list(Mobile_installed_list p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public App findByuuid(String id) {
		return getHibernateTemplate().get(App.class, id);
	}
	
	public Threads findBythrid(int id) {
		String hql = "from Threads where thread_id=?";
		List<Threads> thrList = this.getHibernateTemplate().find(hql,id);
		return thrList.isEmpty()?null:thrList.get(0);
	}
	
	public List<Threads> findBypeopleId(String id) {
		String hql = "from Threads where peopleId=? order by thread_date desc";
		return (List<Threads>)this.getHibernateTemplate().find(hql,id);
	}
	
	public void delSms(Sms_backup p)	{
		getHibernateTemplate().delete(p);
	}
	
	public void delThreads(Threads p)	{
		getHibernateTemplate().delete(p);
	}
	
	public List<Sms_backup> findBythread_id(int id) {
		String hql = "from Sms_backup where threadId=?";
		return (List<Sms_backup>)this.getHibernateTemplate().find(hql,id);
	}

	public void saveOrUpdateContactBakData1(String peo,String memo,String name,String cardVer,String time,File cardfile,int count) {
//		String sql = "INSERT INTO contactbakdata(peopleID,username,vCardFile,vcfVersion,vcfTime,Memo,testfile) VALUES (?,?,?,?,?,?,?);";
		String sql = "INSERT INTO contactbakdata(peopleID,username,vCardFile,vcfVersion,vcfTime,Memo,count) VALUES (?,?,?,?,?,?,?);";
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			//获取Reader对象  
//	        Reader reader = new BufferedReader(new FileReader(cardfile));
			InputStream inputstream = new BufferedInputStream(new FileInputStream(cardfile)); 
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, peo);
			pstat.setString(2, name);
			 //设置Statement对象的参数  
//			pstat.setCharacterStream(3, reader, (int)cardfile.length());
			pstat.setBinaryStream(3, inputstream,(int)cardfile.length());
			pstat.setString(4, cardVer);
			pstat.setString(5, time);
			pstat.setString(6, memo);
			pstat.setInt(7, count);
			pstat.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public PeopleInfo findByUserNumber(String userNumber) {
		String hql = " from PeopleInfo where userNumber = ? ";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, userNumber);
		return list.isEmpty() ? null : list.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public PeopleInfo findByUserNumberAndPassword(String userNumber, String userPassword) {
		String hql = " from PeopleInfo where userNumber = ? and userPassword = ? ";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, userNumber, userPassword);
		return list.isEmpty() ? null : list.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public Mobile_installed_list findByDeviceNoAndApkName(String deviceNo, String apkName) {
		String hql = " from Mobile_installed_list where deviceNo = ? and apkPkgName = ? ";
		List<Mobile_installed_list> list = getHibernateTemplate().find(hql, deviceNo, apkName);
		return list.isEmpty() ? null : list.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public Smsspam findByPeopleIdAndsms(String peopleId, String content,String time) {
		String hql = " from Smsspam where peopleId = ? and content = ? and receivetime= ?";
		List<Smsspam> list = getHibernateTemplate().find(hql, peopleId, content,time);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public Integer saveClientActivityLog(ClientActivityLog caLog)	{
		return (Integer) getHibernateTemplate().save(caLog);
	}
	public PeopleInfo findByUserNu(String userNumber) {
		String hql = " from PeopleInfo where userNumber = ? ";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, userNumber);
		return list.isEmpty() ? null : list.get(0);
	}
	/**
	 * 根据手机号获取用户信息
	 * @author lixiangyang
	 * @param phoneNumber
	 * @return
	 */
	public PeopleInfo findByPhoneNumber(String phoneNumber) {
		String hql = " from PeopleInfo where phoneNumber = ? ";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, phoneNumber);
		return list.isEmpty() ? null : list.get(0);
	}
	/**
	 * 根据imsi号获取用户信息
	 * @author lixiangyang
	 * @param phoneNumber
	 * @return
	 */
	public PeopleInfo findByPhoneIMSI(String imsi) {
		String hql = " from PeopleInfo where phoneIMSI = ? ";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, imsi);
		return list.isEmpty() ? null : list.get(0);
	}
	/**
	 * 根据IMEI号获取用户信息
	 * @author lixiangyang
	 * @param imei
	 * @return
	 */
	public PeopleInfo findByPhoneIMEI(String imei) {
		String hql = " from PeopleInfo where phoneIMEI = ? ";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, imei);
		return list.isEmpty() ? null : list.get(0);
	}
	/**
	 * 根据用户标识id获取用户信息
	 * @author lixiangyang
	 * @param id
	 * @return
	 */
	public PeopleInfo findByPeopleID(String id) {
		String hql = " from PeopleInfo where id = ? ";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, id);
		return list.isEmpty() ? null : list.get(0);
	}
	/**
	 * 根据手机号、imei、imsi获取用户信息
	 * @author lixiangyang
	 * @param phoneNumber
	 * @param imei
	 * @param imsi
	 * @return
	 */
	public PeopleInfo findByPhonenoandimeiandimsi(String phoneNumber,String imei,String imsi) {
		String hql = " from PeopleInfo where phoneNumber = ? and phoneIMEI = ? and phoneIMSI = ?";
		List<PeopleInfo> list = getHibernateTemplate().find(hql, phoneNumber,imei,imsi);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public String findMaxPeopleID() {
		String sql = "SELECT RIGHT(MAX(peopleID),5) FROM peopleinfo";
		String ty = "";
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			res = pstat.executeQuery();
			while (res.next()) {
				ty = res.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return ty;
	}
	
	public int provinceID(String provName) {
		String sql = "SELECT id FROM province WHERE NAME=?";
		int ty = 0 ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, provName);
			res = pstat.executeQuery();
			while (res.next()) {
				ty = res.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ty;
	}
	
	public int cityID(String cityName) {
		String sql = "SELECT id FROM city WHERE NAME=?";
		int ty = 0 ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, cityName);
			res = pstat.executeQuery();
			while (res.next()) {
				ty = res.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ty;
	}
	
	public ClientConfigInfo findClientConfigInfo() {
		String hql = " from ClientConfigInfo";
		List<ClientConfigInfo> list = getHibernateTemplate().find(hql);
		return list.isEmpty() ? null : list.get(0);
	}
	
	
	
	public List<ContactBakData> ContactBakDataList(String peopleID) {
		String sql = "SELECT id, username,vcfVersion,vcfTime,Memo,count FROM contactbakdata  WHERE peopleID= ? ORDER BY vcfTime DESC LIMIT 5";
		List<ContactBakData> cList  =new ArrayList<ContactBakData>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, peopleID);
			res = pstat.executeQuery();
			while (res.next()) {
				ContactBakData c = new ContactBakData();
				c.setId(res.getInt("id"));
				c.setUsername(res.getString("username"));
				c.setVcfVersion(res.getString("vcfVersion"));
				c.setVcfTime(res.getString("vcfTime"));
				c.setMemo(res.getString("Memo"));
				c.setCount(res.getInt("count"));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
	
	public InputStream findvcardFile(String id) {
		String sql = "SELECT vCardFile FROM contactbakdata a WHERE a.id=?";
		InputStream input = null;
		StringBuilder sb = new StringBuilder();   
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, id);
			res = pstat.executeQuery();
			while (res.next()) {
				 
				Blob image = res.getBlob("vCardFile");
				input = image.getBinaryStream();
//				BufferedReader reader = new BufferedReader(new InputStreamReader(input));   
//				
//		        String line = null;   
//		            while ((line = reader.readLine()) != null) {   
//		                sb.append(line + "/n");   
//		            }   
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return input;
	}
	
	
	public List<String> findnewVersion(String apkPkgName) {
		List<String> strList = new ArrayList<String>();
		String sql = "SELECT a.uuid,a.version,a.apkPkgName,a.apkVersionCode FROM app_install_pack a WHERE a.apkPkgName=? and a.cpid='cp09' and a.os='os1' ORDER BY a.apkVersionCode DESC LIMIT 1";
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, apkPkgName);
			res = pstat.executeQuery();
			while (res.next()) {
				strList.add(res.getString(1));
				strList.add(res.getString(2));
				strList.add(res.getString(3));
				strList.add(res.getString(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return strList;
	}
	
	
	public List<Sms_backup> sms_backupList(String uid,int threadId,int count) {
		String sql = "SELECT s.id,s.peopleId,s.thread_id,s.phoneno,s.address,s.backup_date,s.body,s.isread,s.locked,s.back_type,s.thread_date,s.service_center,s.error_code,s.person,s.reply_path_present,s.protocol,s.seen,s.sms_id,s.back_status,s.back_subject,s.long_date FROM sms_backup s WHERE s.peopleId=? AND s.thread_id=? ORDER BY s.sms_id";
		List<Sms_backup> cList  =new ArrayList<Sms_backup>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, uid);
			pstat.setInt(2, threadId);
			res = pstat.executeQuery();
			while (res.next()) {
				Sms_backup c = new Sms_backup();
				c.setId(res.getInt("id"));
				c.setPeopleId(res.getString("peopleId"));
				c.setThreadId(res.getInt("thread_id"));
				c.setPhoneno(res.getString("phoneno"));
				c.setAddress(res.getString("address"));
				c.setBackup_date(res.getString("backup_date"));
				c.setSms_body(res.getString("body"));
				c.setIsread(res.getInt("isread"));
				c.setLocked(res.getInt("locked"));
				c.setSms_type(res.getInt("back_type"));
				c.setThread_date(res.getString("thread_date"));
				c.setColumncenter(res.getString("service_center"));
				c.setError_code(res.getInt("error_code"));
				c.setPerson(res.getString("person"));
				c.setPresnet(res.getInt("reply_path_present"));
				c.setProtocol(res.getInt("protocol"));
				c.setSeen(res.getInt("seen"));
				c.setSms_id(res.getInt("sms_id"));
				c.setSms_status(res.getInt("back_status"));
				c.setSubject(res.getString("back_subject"));
				c.setLong_date(res.getString("long_date"));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
	
	public int findByuidandsmsid(String uid,int smsId,int threadId) {
		String hql = "select count(*) as num from sms_backup where peopleId=? and sms_id=? and thread_id=?";
		int i =0;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, uid);
			pstat.setInt(2, smsId);
			pstat.setInt(3, threadId);
			res = pstat.executeQuery();
			while (res.next()) {
				i= res.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return i;
	}
	public List<App_Comment> findByPidAndUuid(String uid,int index,int size) {
		String hql = "SELECT a.id,a.app_uuid,a.comment_date,a.content,a.nickname,a.peopleId,a.phoneno,a.rating FROM app_comment a WHERE a.app_uuid=? ORDER BY a.comment_date DESC LIMIT ?,?;";
		List<App_Comment> cList  =new ArrayList<App_Comment>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, uid);
			pstat.setInt(2, index);
			pstat.setInt(3, size);
			res = pstat.executeQuery();
			while (res.next()) {
				App_Comment c = new App_Comment();
				c.setId(res.getInt("id"));
				c.setPeopleId(res.getString("peopleId"));
				c.setApp_uuid(res.getString("app_uuid"));
				c.setComment_date(res.getString("comment_date"));
				c.setContent(res.getString("content"));
				c.setNickname(res.getString("nickname"));
				c.setPhoneno(res.getString("phoneno"));
				c.setRating(Float.parseFloat(res.getString("rating")));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
	
	public int findByPidAndUuid(String uid) {
		String hql = "SELECT count(*) FROM app_comment a WHERE a.app_uuid=?";
		int count =0;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, uid);
			res = pstat.executeQuery();
			while (res.next()) {
				count= res.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return count;
	}
	
	public int findByAppUuid(String uid) {
		String hql = "SELECT a.point FROM app a WHERE a.uuid=?";
		int count =0;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, uid);
			res = pstat.executeQuery();
			while (res.next()) {
				count= res.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return count;
	}
	
	public String findvcard(String id) {
		String sql = "SELECT vCardFile FROM contactbakdata a WHERE a.id=?";
		String input = null;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, id);
			res = pstat.executeQuery();
			while (res.next()) {
				Blob image = res.getBlob("vCardFile");
				input = image.toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return input;
	}
	
	
	public List<Guessyoulike> findByPid(String uid,int index,int size) {
		String hql = "SELECT g.id,g.peopleId,g.phoneno,g.app_uuid,g.appname,g.icon,g.file_size,g.friends_count,g.versions FROM guessyoulike g WHERE g.phoneno=? ORDER BY g.friends_count DESC LIMIT ?,?";
		List<Guessyoulike> cList  =new ArrayList<Guessyoulike>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, uid);
			pstat.setInt(2, index);
			pstat.setInt(3, size);
			res = pstat.executeQuery();
			while (res.next()) {
				Guessyoulike c = new Guessyoulike();
				c.setId(res.getInt("id"));
				c.setApp_uuid(res.getString("app_uuid"));
				c.setPhoneno(res.getString("phoneno"));
				c.setAppname(res.getString("appname"));
				c.setIcon(res.getString("icon"));
				c.setPeopleId(res.getString("peopleId"));
				c.setVersions(res.getString("versions"));
				c.setFriends_count(Integer.parseInt(res.getString("friends_count")));
				c.setFile_size(Float.parseFloat(res.getString("file_size")));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
	
	public List<Guessyoulike> findByPidNoBackup(int index,int size) {
		String hql = "SELECT g.id,g.peopleId,g.phoneno,g.app_uuid,g.appname,g.icon,g.file_size,g.friends_count,g.versions FROM guessyoulike g WHERE g.phoneno is null ORDER BY g.friends_count DESC LIMIT ?,?";
		List<Guessyoulike> cList  =new ArrayList<Guessyoulike>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setInt(1, index);
			pstat.setInt(2, size);
			res = pstat.executeQuery();
			while (res.next()) {
				Guessyoulike c = new Guessyoulike();
				c.setId(res.getInt("id"));
				c.setApp_uuid(res.getString("app_uuid"));
				c.setPhoneno(res.getString("phoneno"));
				c.setAppname(res.getString("appname"));
				c.setIcon(res.getString("icon"));
				c.setPeopleId(res.getString("peopleId"));
				c.setVersions(res.getString("versions"));
				c.setFriends_count(Integer.parseInt(res.getString("friends_count")));
				c.setFile_size(Float.parseFloat(res.getString("file_size")));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
}
