package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.util.SerializationHelper;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.i8app.install.Pager;

public class BaseDao<T> {

	private HibernateTemplate hibernateTemplate;

	@Resource
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	public HibernateTemplate getHibernateTemplate()	{
		return hibernateTemplate;
	}
	
	/**
	 * 使用hql方式进行分页查询
	 * @param hql hql查询语句
	 * @param offset 查询的起始位置
	 * @param pageSize 每页多少条
	 * @return Pager
	 */
	@SuppressWarnings("unchecked")
	protected Pager<T> queryPager(final String hql ,final int offset, final int pageSize)	{
		@SuppressWarnings("rawtypes")
		List<T> list = hibernateTemplate.executeFind(new HibernateCallback(){

			@Override
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				return session.createQuery(hql)
						.setFirstResult(offset)
						.setMaxResults(pageSize)
						.list();
			}
		});
		
		String str = hql.replaceAll(".*from ", "select count(*) from ");	//以from为标记,将hql替换成记录数查询语句
		Long count = (Long)hibernateTemplate.find(str).get(0);
		
		return new Pager<T>(list, count.intValue());
	}
	
	/**
	 * 使用criteria方式做分页查询
	 * @param criteria
	 * @param offset
	 * @param pageSize
	 * @return Pager
	 */
	@SuppressWarnings("unchecked")
	public Pager<T> queryPager(DetachedCriteria criteria, int pageNo, int pageSize)	{
		if(pageNo<1){
			pageNo=1;
		}
		int offset = (pageNo-1)*pageSize;
		DetachedCriteria clonedCriteria = (DetachedCriteria)SerializationHelper.clone(criteria);	//复制一份criteria
		List<T> list = hibernateTemplate.findByCriteria(clonedCriteria, offset, pageSize);	
		
		criteria.setProjection(Projections.rowCount());
		int count = (Integer)hibernateTemplate.findByCriteria(criteria).get(0);	//查询总记录数
		
		Pager<T> pager = new Pager<T>(list, count);
		return pager;
	}
	
}
