package com.i8app.install.dao;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.i8app.install.domain.Tptmadapt;

@SuppressWarnings("rawtypes")
@Repository
public class TptmadaptDao extends BaseDao {
	public void addOrUpdateTptmadapt(Tptmadapt tpt) {
		this.getHibernateTemplate().saveOrUpdate(tpt);
	}
	public Tptmadapt findByPEP(final int pid,final String empno,final String phone) {
		Tptmadapt count =  getHibernateTemplate().execute(new HibernateCallback<Tptmadapt>(){
			public Tptmadapt doInHibernate(Session session)throws HibernateException,SQLException{
				return (Tptmadapt) session.createQuery("from Tptmadapt where provinceid = ? and empno=? and phonenumber=?")
				.setInteger(0, pid).setString(1, empno).setString(2, phone).uniqueResult();
			}
		});
		return count;
	}
}
