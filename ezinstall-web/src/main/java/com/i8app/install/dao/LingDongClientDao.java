package com.i8app.install.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.i8app.install.domain.I8client;
import com.i8app.install.domain.LingDongClient;

@SuppressWarnings("rawtypes")
@Repository
public class LingDongClientDao extends BaseDao	{
	
//	String sql = "SELECT L.SOFTVERSION, L.URL, L.MEMO FROM LINGDONGCLIENT L WHERE L.SOFTID = ?" +
//	" AND L.OSTYPE = ? AND L.SUBTYPE = ? AND L.OSID <= ?";

//	public List<LingDongClient> find(int softId, int osType, int subType, int osid)	{
//		String hql = "from LingDongClient where softId = ? and osType = ? and subType = ? and osid <= ?";
//		return this.getHibernateTemplate().find(hql, softId, osType, subType, osid);
//	}
	
	@SuppressWarnings("unchecked")
	public List<LingDongClient> find(int softId, int osType, int subType, int osid)	{
		String hql = "from LingDongClient where softId = ?  and osid <= ?";
		return this.getHibernateTemplate().find(hql, softId,osid);
	}
	
	public I8client findI8client(String bundleId,String os){
		
//		String sql = "FROM I8client WHERE bundleId=? AND os=? AND isPublish =1 ORDER BY versionCode DESC LIMIT 1";
//		return (I8client)this.getHibernateTemplate().find(sql, bundleId,os).get(0);
		
		I8client i = null;
		String sql = "SELECT id,bundleId,VERSION,versionCode,fileSize,info,os,releaseNotes,url,updateTime,isPublish FROM I8client WHERE bundleId=? AND os=? AND isPublish =1 ORDER BY versionCode DESC LIMIT 1";
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, bundleId);
			pstat.setString(2, os);
			res = pstat.executeQuery();
			while (res.next()) {
				i = new I8client();
				i.setId(res.getInt(1));
				i.setBundleId(res.getString(2));
				i.setVersion(res.getString(3));
				i.setVersionCode(res.getLong(4));
				i.setFileSize(res.getFloat(5));
				i.setInfo(res.getString(6));
				i.setOs(res.getString(7));
				i.setReleaseNotes(res.getString(8));
				i.setUrl(res.getString(9));
				i.setUpdateTime(res.getString(10));
				i.setIsPublish(res.getInt(11));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return i;
	}
	
}
