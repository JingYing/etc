package com.i8app.install.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.install.domain.SoftType;

@SuppressWarnings("rawtypes")
@Component
public class SoftTypeDao extends BaseDao {
	
	@SuppressWarnings("unchecked")
	public List<SoftType> findAll()	{
		String hql = "from SoftType order by num, id";
		return this.getHibernateTemplate().find(hql);
	}
}
