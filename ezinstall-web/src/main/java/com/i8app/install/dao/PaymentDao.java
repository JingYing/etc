package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.i8app.install.Pager;
import com.i8app.install.Util;
import com.i8app.install.domain.CommisionBalance;
import com.i8app.install.domain.CommisionWithdrawal;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.Requisition;
/**
 * @author LiXiangyang 2014-3-5
 */
@SuppressWarnings("rawtypes")
@Repository
public class PaymentDao extends BaseDao {

	@SuppressWarnings("unchecked")
	public List<Employee> findByEmpno(String empNo) {
		String hql = "from Employee where empNo = ?";
		return (List<Employee>)getHibernateTemplate().find(hql, empNo);
	}
	@SuppressWarnings("unchecked")
	public Employee findByEmpno(final String empNo,final int pro) {
//		String hql = "from Employee where empNo = ? and province.provinceID =?";
//		return (Employee)getHibernateTemplate().find(hql, empNo,pro);
		return (Employee)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("from Employee where empNo = ?  and province.provinceID =?")
				.setString(0, empNo).setInteger(1, pro).uniqueResult();
			}
		});
	}
	@SuppressWarnings("unchecked")
	public Employee findByEmp(final String empNo) {
		return (Employee)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("from Employee where empNo = ?")
				.setString(0, empNo).uniqueResult();
			}
		});
	}
	@SuppressWarnings("unchecked")
	public Pager<CommisionWithdrawal> pagerlist(final int empId,final String year,final String month,final int pageNo,final int pageSize) {
		final StringBuffer sql = new StringBuffer();
		sql.append("from CommisionWithdrawal w where w.empid=?");
		final List<Integer> paramList = new ArrayList<Integer>();
		paramList.add(empId);
		if(year != null && !"".equals(year)){
			sql.append(" and YEAR(w.applyTime)=?");
			paramList.add(Integer.parseInt(year));
		}
		if(month != null && !"".equals(month)){
			sql.append(" and MONTH(w.applyTime)=?");
			paramList.add(Integer.parseInt(month));
		}
		sql.append(" order by w.applyTime DESC");
		List<CommisionWithdrawal> softList = getHibernateTemplate().executeFind(new HibernateCallback(){
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "select w " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.setFirstResult((pageNo-1)*pageSize).setMaxResults(pageSize).list();
			}
		});
		Long total = (Long)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException, SQLException{
				String hql = "select count(*) " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.uniqueResult();
			}
		});
		Pager<CommisionWithdrawal> pager = new Pager<CommisionWithdrawal>();
		pager.setList(softList);
		pager.setTotal(total.intValue());
		return pager;
	}
	
/*	@SuppressWarnings("unchecked")
	public Pager<CommisionAccount> pagerlist(final int empId,final String year,final String month,final int pageNo,final int pageSize) {
		final StringBuffer sql = new StringBuffer();
		sql.append("from CommisionAccount w where w.empId=?");
		final List<Integer> paramList = new ArrayList<Integer>();
		paramList.add(empId);
		if(year != null && !"".equals(year)){
			sql.append(" and YEAR(w.financialTime)=?");
			paramList.add(Integer.parseInt(year));
		}
		if(month != null && !"".equals(month)){
			sql.append(" and MONTH(w.financialTime)=?");
			paramList.add(Integer.parseInt(month));
		}
		sql.append(" order by w.financialTime DESC");
		List<CommisionAccount> softList = getHibernateTemplate().executeFind(new HibernateCallback(){
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "select w " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.setFirstResult((pageNo-1)*pageSize).setMaxResults(pageSize).list();
			}
		});
		Long total = (Long)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException, SQLException{
				String hql = "select count(*) " + sql.toString();
				Query query = session.createQuery(hql);
				for (int i=0; i<paramList.size(); i++) {
					query.setParameter(i, paramList.get(i));
				}
				return query.uniqueResult();
			}
		});
		Pager<CommisionAccount> pager = new Pager<CommisionAccount>();
		pager.setList(softList);
		pager.setTotal(total.intValue());
		return pager;
	}
*/	@SuppressWarnings("unchecked")
	public Requisition findByPersion(final int empid) {
		return (Requisition)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("from Requisition where empid.empID = ?")
				.setInteger(0, empid).uniqueResult();
			}
		});
	}
	
	public Requisition findByPersionnum(final int empid) {
		return getHibernateTemplate().execute(new HibernateCallback<Requisition>(){
			public Requisition doInHibernate(Session session)throws HibernateException,SQLException{
				return (Requisition)session.createQuery("from Requisition where empid.empID = ?")
				.setInteger(0, empid).uniqueResult();
			}
		});
	}
	public CommisionBalance findBalanceByEmpId(final Integer i) {
		return getHibernateTemplate().execute(new HibernateCallback<CommisionBalance>() {

			@Override
			public CommisionBalance doInHibernate(Session session)
					throws HibernateException, SQLException {
				return (CommisionBalance) session.createQuery("from CommisionBalance where empid = ?")
				.setInteger(0, i).uniqueResult();
			}
		});
	}
	public int saveOrUpdate(Requisition requisition) {
		getHibernateTemplate().saveOrUpdate(requisition);
		return 1; 
	}
	public void updateNewPassword(Requisition requisition) {
		getHibernateTemplate().update(requisition);
	}
	public void insertWithdrawal(CommisionWithdrawal w) {
		getHibernateTemplate().save(w);
	}
	public CommisionWithdrawal findWithdrawalThisMonthByEmpId(final Integer empID) {
		return getHibernateTemplate().execute(new HibernateCallback<CommisionWithdrawal>() {

			@Override
			public CommisionWithdrawal doInHibernate(Session session)
					throws HibernateException, SQLException {
				return (CommisionWithdrawal) session.createQuery("from CommisionWithdrawal where empid = ? and id like ?")
				.setString(1, Util.createTimes("yyyyMM")+"%")
				.setInteger(0, empID).uniqueResult();
			}
		});
	}
}
