package com.i8app.install.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.install.domain.MusicType;
/**
 * MusicType的Dao类
 * @author JingYing
 *
 */
@SuppressWarnings("rawtypes")
@Component("musicTypeDao")
public class MusicTypeDao extends BaseDao{

	public void addOrUpdate(MusicType musicType) {
		this.getHibernateTemplate().saveOrUpdate(musicType);
	}

	public void del(int id) {
		this.getHibernateTemplate().delete(findById(id));
	}

	public MusicType findById(int id) {
		return (MusicType) this.getHibernateTemplate().get(MusicType.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<MusicType> list() {
		return this.getHibernateTemplate().find("from MusicType m order by m.num");
	}

}
