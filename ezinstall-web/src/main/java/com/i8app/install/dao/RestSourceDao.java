package com.i8app.install.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.i8app.install.domain.App;
import com.i8app.install.domain.Resource_type;

@SuppressWarnings("rawtypes")
//@Component
@Repository
public class RestSourceDao extends BaseDao {
	@SuppressWarnings("unchecked")
//	public List<Resource_type> findResource_type()		{
//		String hql = "from Resource_type";
//		return this.getHibernateTemplate().find(hql);
//	}
	public List<Resource_type> findResource_type(){
		return (List<Resource_type>)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("from Resource_type").list();
			}
		});
	}
	
	public App finddowncount(String packuuid) {
		int i =0;
		App a = new App();
		String sql = "SELECT a.uuid as uuid,a.downCount as count FROM app a JOIN app_install_pack ap ON a.uuid=ap.appId WHERE ap.uuid=?";
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, packuuid);
			res = pstat.executeQuery();
			while (res.next()) {
				a.setUuid(res.getString("uuid"));
				a.setDownCount(res.getInt("count"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return a;
	}
	
	public void saveOrUpdatedowncount(int count,String uuid) {
		int i =0;
		String sql = "UPDATE app SET downCount=? WHERE UUID=?";
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setInt(1, count);
			pstat.setString(2, uuid);
			pstat.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
