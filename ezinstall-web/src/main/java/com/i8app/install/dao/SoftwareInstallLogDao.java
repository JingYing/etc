package com.i8app.install.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.i8app.install.domain.AppFlowLog;
import com.i8app.install.domain.ClientActivityLog;
import com.i8app.install.domain.Guessyoulike;
import com.i8app.install.domain.SoftwareInstallLog;
import com.i8app.install.domain.UserFlowLog;

@SuppressWarnings("rawtypes")
@Repository
public class SoftwareInstallLogDao extends BaseDao {

	public void saveOrUpdateSoftwareInstallLog(SoftwareInstallLog p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateUserFlowLog(UserFlowLog p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public void saveOrUpdateAppFlowLog(AppFlowLog p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	
	public Integer saveClientActivityLog(ClientActivityLog caLog)	{
		return (Integer) getHibernateTemplate().save(caLog);
	}
	public int findSoftwareInstallLog(String peopleId, String pname)	{
		String hql = "SELECT COUNT(*) FROM SoftwareInstallLog s WHERE s.peopleId=? AND s.packUuid=?";
		int count = 0 ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, peopleId);
			pstat.setString(2, pname);
			res = pstat.executeQuery();
			while (res.next()) {
				count = res.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return count;
	}
}
