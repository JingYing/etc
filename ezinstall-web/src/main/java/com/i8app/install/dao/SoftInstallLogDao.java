package com.i8app.install.dao;

import org.springframework.stereotype.Repository;

import com.i8app.install.domain.SoftInstallLog;

@SuppressWarnings("rawtypes")
@Repository("softInstallLogDao")
public class SoftInstallLogDao extends BaseDao {
	
	public void saveOrUpdate(SoftInstallLog log)	{
		this.getHibernateTemplate().saveOrUpdate(log);
	}

}
