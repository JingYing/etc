package com.i8app.install.dao;

import org.springframework.stereotype.Component;

import com.i8app.install.domain.AppInstallPack;

@SuppressWarnings("rawtypes")
//@Repository
@Component
public class AppInstallPackDao extends BaseDao {

	public AppInstallPack findById(String id)		{
		return (AppInstallPack)getHibernateTemplate().get(AppInstallPack.class, id);
	}

}
