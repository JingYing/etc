package com.i8app.install.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.i8app.install.domain.AppInstallPack;
import com.i8app.install.domain.App_backup;
import com.i8app.install.domain.Image_backup;
import com.i8app.install.domain.Sms_backup;

@SuppressWarnings("rawtypes")
@Component
public class App_backupDao extends BaseDao {

	public App_backup findById(int id){
		return (App_backup)getHibernateTemplate().get(App_backup.class, id);
	}
	public void saveOrUpdateApp_backup(App_backup p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public void delApp_backup(App_backup p)	{
		getHibernateTemplate().delete(p);
	}
	public App_backup findAppByUPV(String peopleId,String pname,String version){
		String hql = "from App_backup where peopleId=? and packname=?"; 
		List<App_backup> appList = this.getHibernateTemplate().find(hql,peopleId,pname);
		return appList.isEmpty()?null:appList.get(0);
	}
	
	public AppInstallPack findAppByPnameAndVer(String pname,int version){
		String hql = "from AppInstallPack where apkPkgName=? and apkVersionCode=? and cpId='cp09' and os='os1'"; 
		List<AppInstallPack> appList = this.getHibernateTemplate().find(hql,pname,version);
		return appList.isEmpty()?null:appList.get(0);
	}
	
	public AppInstallPack findMaxAppByPname(String pname){
		String hql = "from AppInstallPack where apkPkgName=? and cpId='cp09' and os='os1' order by apkVersionCode desc"; 
		List<AppInstallPack> appList = this.getHibernateTemplate().find(hql,pname);
		return appList.isEmpty()?null:appList.get(0);
	}
	
	public List<App_backup> findAppByPeopleId(String peopleId){
//		String hql = "from App_backup where peopleId=?"; 
//		return this.getHibernateTemplate().find(hql,peopleId);
		
		String hql = "SELECT a.id,a.peopleId,a.phoneno,a.appname,a.packname,a.versioncode,a.backup_date,a.app_version,a.size,a.file_url,a.icon_url FROM app_backup a WHERE a.peopleId=? GROUP BY a.packname";
		List<App_backup> cList  =new ArrayList<App_backup>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, peopleId);
			res = pstat.executeQuery();
			while (res.next()) {
				App_backup c = new App_backup();
				c.setId(res.getInt("id"));
				c.setPhoneno(res.getString("phoneno"));
				c.setAppname(res.getString("appname"));
				c.setPeopleId(res.getString("peopleId"));
				c.setApp_version(res.getString("app_version"));
				c.setBackup_date(res.getString("backup_date"));
				c.setFile_url(res.getString("file_url"));
				c.setIcon_url(res.getString("icon_url"));
				c.setPackname(res.getString("packname"));
				c.setSize(res.getInt("size"));
				c.setVersioncode(res.getInt("versioncode"));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
	
	
	public App_backup findAppByPe(String peopleId,String pname){
		String hql = "SELECT a.id,a.peopleId,a.phoneno,a.appname,a.packname,a.versioncode,a.backup_date,a.app_version,a.size,a.file_url,a.icon_url FROM app_backup a WHERE a.peopleId=? AND a.packname=? AND a.file_url IS NOT NULL ORDER BY a.versioncode DESC LIMIT 1;";
		App_backup c = new App_backup();
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, peopleId);
			pstat.setString(2, pname);
			res = pstat.executeQuery();
			while (res.next()) {
				c.setId(res.getInt("id"));
				c.setPhoneno(res.getString("phoneno"));
				c.setAppname(res.getString("appname"));
				c.setPeopleId(res.getString("peopleId"));
				c.setApp_version(res.getString("app_version"));
				c.setBackup_date(res.getString("backup_date"));
				c.setFile_url(res.getString("file_url"));
				c.setIcon_url(res.getString("icon_url"));
				c.setPackname(res.getString("packname"));
				c.setSize(res.getInt("size"));
				c.setVersioncode(res.getInt("versioncode"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return c;
	}
	
	@SuppressWarnings("unchecked")
	public List<Image_backup> findImageByPeopleId(String peopleId){
		String hql = "from Image_backup where peopleId=? order by backup_date desc"; 
		return this.getHibernateTemplate().find(hql,peopleId);
	}
	
	@SuppressWarnings("unchecked")
	public Image_backup findImageByPidAndName(String peopleId,String name){
		String hql = "from Image_backup where peopleId=? and imagename=? order by backup_date desc"; 
		List<Image_backup> list = this.getHibernateTemplate().find(hql,peopleId,name);
		return list.isEmpty() ? null : list.get(0); 
	}
	
	public String app_backupIcon(String pname) {
		String sql = "SELECT a.ICON FROM app_install_pack pa LEFT JOIN app a ON a.uuid=pa.appId WHERE pa.apkPkgName=? ORDER BY pa.apkVersionCode DESC";
		String icon_url ="";
		List<Sms_backup> cList  =new ArrayList<Sms_backup>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(sql);
			pstat.setString(1, pname);
			res = pstat.executeQuery();
			while (res.next()) {
				icon_url = res.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return icon_url;
	}
	
	public App_backup findapp_backup(String uid,String pname,String version){
		String hql = " from App_backup where peopleId =? and packname=? and app_version=?";
		@SuppressWarnings("unchecked")
		List<App_backup> list = getHibernateTemplate().find(hql, uid,pname,version);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public Image_backup findimageById(int id,String uid){
//		return (Image_backup)getHibernateTemplate().get(Image_backup.class, id);
		String hql = " from Image_backup where peopleId =? and id=?";
		@SuppressWarnings("unchecked")
		List<Image_backup> list = getHibernateTemplate().find(hql, uid,id);
		return list.isEmpty() ? null : list.get(0);
	}
	public void saveOrUpdateApp_backup(Image_backup p)	{
		getHibernateTemplate().saveOrUpdate(p);
	}
	public void delImage_backup(Image_backup p)	{
		getHibernateTemplate().delete(p);
	}
}
