package com.i8app.install.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.BookTypeDao;
import com.i8app.install.domain.BookType;
@SuppressWarnings("rawtypes")
@Component("bookTypeDao")
public class BookTypeDao extends BaseDao	{

	public void addOrUpdate(BookType bookType) {
		this.getHibernateTemplate().saveOrUpdate(bookType);
	}

	public void del(int id) {
		this.getHibernateTemplate().delete(findbookById(id));
	}

	public BookType findbookById(int id) {
		return (BookType)this.getHibernateTemplate().load(BookType.class,id);
	}

	@SuppressWarnings("unchecked")
	public List<BookType> list() {
		return this.getHibernateTemplate().find("from BookType b order by b.id");
	}

}
