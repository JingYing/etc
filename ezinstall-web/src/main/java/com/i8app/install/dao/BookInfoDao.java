package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

import com.i8app.install.domain.BookInfo;
@SuppressWarnings("rawtypes")
@Component("bookInfoDao")
public class BookInfoDao extends BaseDao{

	public void addOrUpdate(BookInfo bookInfo) {
		this.getHibernateTemplate().saveOrUpdate(bookInfo);
	}

	public void del(int id) {
		this.getHibernateTemplate().delete(findBookById(id));
	}

	public BookInfo findBookById(int id) {
		return (BookInfo)this.getHibernateTemplate().load(BookInfo.class,id);
	}

	@SuppressWarnings("unchecked")
	public List<BookInfo> list() {
		return this.getHibernateTemplate().find("from BookInfo");
	}

	@SuppressWarnings("unchecked")
	public List<BookInfo> list(final int pageNo,final int pageSize) {
		final int begin = (pageNo - 1)*pageSize; 
		return (List<BookInfo>)this.getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session) throws HibernateException,SQLException{
				return session.createQuery("from BookInfo").setFirstResult(begin).setMaxResults(pageSize).list();
			}
		});
	}

	@SuppressWarnings({ "unchecked" })
	public List<BookInfo> list(final String title,final String author,final int bookTypeId) {
		return (List<BookInfo>)this.getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				StringBuilder sb = new StringBuilder();
				sb.append("from BookInfo b where 1=1");
				if(title != null && !"".equals(title)){
					sb.append(String.format(" and b.title like '%%s%%'", title));
				}
				if(author != null && !"".equals(author)){
					sb.append(String.format(" and b.author like '%%s%%'", author));
				}
				if(bookTypeId != 0){
					sb.append(String.format(" and b.bookType.bookTypeId = '%s'", bookTypeId));
				}
				return session.createQuery(sb.toString()).list();
			}
		});
	}

	public int count(){
		return list().size();
	}

}
