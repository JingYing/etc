package com.i8app.install.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.install.domain.VideoType;
/**
 * VideoType的Dao类
 * @author JingYing
 *
 */
@SuppressWarnings("rawtypes")
@Component("videoTypeDao")
public class VideoTypeDao extends BaseDao {

	public void addOrUpdate(VideoType videoType) {
		this.getHibernateTemplate().saveOrUpdate(videoType);
	}

	public void del(int id) {
		this.getHibernateTemplate().delete(findById(id));
	}

	public VideoType findById(int id) {
		return (VideoType) this.getHibernateTemplate().get(VideoType.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<VideoType> list() {
		return this.getHibernateTemplate().find("from VideoType v order by v.num");
	}

}
