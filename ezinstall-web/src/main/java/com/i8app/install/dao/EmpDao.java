package com.i8app.install.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.install.domain.Employee;
import com.i8app.install.domain.GroupPrivilege;
import com.i8app.install.domain.UserGroupInfo;
import com.i8app.install.domain.Userdatarestrict;

@Repository
public class EmpDao extends BaseDao {

	public Employee findById(int id)	{
		return (Employee)getHibernateTemplate().get(Employee.class, id);
	}

	public Employee findByEmpnoAndProvid(String empNo, Integer provId) {
		String hql = "from Employee where empNo = ? and provId = ?";
		List<Employee> list = getHibernateTemplate().find(hql, empNo, provId);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public Employee findByEmpnoAndProvidLogin(String empNo, Integer provId) {
		String hql = "from Employee where empNo = ? and provId = ? and isDeleted=0 and isFreezed=0";
		List<Employee> list = getHibernateTemplate().find(hql, empNo, provId);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public List<Employee> findEmpListByEmpnoAndProvidLogin(String empNo, Integer provId) {
		List<Employee> list = new ArrayList<Employee>();
		String hql = "";
		if(empNo !=null && !"manager".equals(empNo)){
			hql = "from Employee where empNo = ? and provId = ?";
			list = getHibernateTemplate().find(hql, empNo, provId);
		}else{
			hql = "from Employee where empNo = ?";
			list = getHibernateTemplate().find(hql, empNo);
		}
		return list.isEmpty() ? null : list;
	}
	
	public List<Userdatarestrict> findUserdatarestrictByEmpId(Integer empId) {
		String hql = "from Userdatarestrict where userId = ?";
		List<Userdatarestrict> list = getHibernateTemplate().find(hql,empId);
		return list.isEmpty() ? null : list;
	}
	public List<UserGroupInfo> findUserGroupInfoByEmpId(Integer empId) {
		String hql = "from UserGroupInfo where userId = ?";
		List<UserGroupInfo> list = getHibernateTemplate().find(hql,empId);
		return list.isEmpty() ? null : list;
	}
	
	public List<GroupPrivilege> findGroupPrivilegeBygroupId(Integer groupId) {
		String hql = "from GroupPrivilege where groupId.groupId = ?";
		List<GroupPrivilege> list = getHibernateTemplate().find(hql,groupId);
		return list;
	}
}
