package com.i8app.install.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.SoftSuffixDao;
import com.i8app.install.domain.SoftSuffix;
@SuppressWarnings("rawtypes")
@Component("softSuffixDao")
public class SoftSuffixDao 	extends BaseDao{

	public SoftSuffix findById(int id) {
		return (SoftSuffix)getHibernateTemplate().load(SoftSuffix.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<SoftSuffix> list() {
		return getHibernateTemplate().find("from SoftSuffix");
	}	
	

}
