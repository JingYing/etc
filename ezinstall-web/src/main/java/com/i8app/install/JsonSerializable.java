package com.i8app.install;

import com.google.gson.JsonObject;

public interface JsonSerializable {
	
	public JsonObject toJsonObject();

}
