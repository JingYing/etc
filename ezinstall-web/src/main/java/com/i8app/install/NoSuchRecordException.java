package com.i8app.install;

public class NoSuchRecordException extends RuntimeException {

	public NoSuchRecordException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoSuchRecordException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoSuchRecordException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NoSuchRecordException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
