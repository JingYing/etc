package com.i8app.install.domain;

import javax.persistence.*;

/**
 * <<驱动信息表>>的实体类
 *  表格式: id--主键
 *  vid--表示生产厂商, 一张表里面应该只有一个VID.
 *  pid--表示手机型号,所有PID以逗号隔开放在一条记录中
 *  filePath--表示驱动包的网络地址
 *  osType--表示操作系统类型
 * 
 * @author JingYing
 * 
 */
@Entity
public class Mobile_installed_list {
	private Integer id;
	private String deviceNo,apkPkgName,ipaBundleId,installTime;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDeviceNo() {
		return deviceNo;
	}
	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}
	public String getApkPkgName() {
		return apkPkgName;
	}
	public void setApkPkgName(String apkPkgName) {
		this.apkPkgName = apkPkgName;
	}
	public String getIpaBundleId() {
		return ipaBundleId;
	}
	public void setIpaBundleId(String ipaBundleId) {
		this.ipaBundleId = ipaBundleId;
	}
	public String getInstallTime() {
		return installTime;
	}
	public void setInstallTime(String installTime) {
		this.installTime = installTime;
	}
}
