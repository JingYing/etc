package com.i8app.install.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * <<视频信息表>>的实体类
 * 
 * @author JingYing
 * 
 */
@Entity
public class VideoInfo {
	private Integer id;
	private String videoName;
	private Float videoSize;
	private Integer videoLen;
	private VideoType videoType; // 视频类型
	private String videoFile;
	private String desc;
	private String videoImgFile;
	private Date dateAdded;
	private Integer totalDownloadNum;
	private Integer weekDownloadNum;
	private Integer monthDownloadNum;
	private Integer hotDegree;
	private Integer userRemarkNum;
	private Float userRemark;
	private String memo;
	private SoftSuffix suffix;

	@Id
	@GeneratedValue
	@Column(name="vid")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 128)
	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public Float getVideoSize() {
		return videoSize;
	}

	public void setVideoSize(Float videoSize) {
		this.videoSize = videoSize;
	}

	public Integer getVideoLen() {
		return videoLen;
	}

	public void setVideoLen(Integer videoLen) {
		this.videoLen = videoLen;
	}

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "videoTypeID")
	public VideoType getVideoType() {
		return videoType;
	}

	public void setVideoType(VideoType videoType) {
		this.videoType = videoType;
	}

	public String getVideoFile() {
		return videoFile;
	}

	public void setVideoFile(String videoFile) {
		this.videoFile = videoFile;
	}

	@Column(name = "description")
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getVideoImgFile() {
		return videoImgFile;
	}

	public void setVideoImgFile(String videoImgFile) {
		this.videoImgFile = videoImgFile;
	}

	@Temporal(TemporalType.DATE)
	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Integer getTotalDownloadNum() {
		return totalDownloadNum;
	}

	public void setTotalDownloadNum(Integer totalDownloadNum) {
		this.totalDownloadNum = totalDownloadNum;
	}

	public Integer getWeekDownloadNum() {
		return weekDownloadNum;
	}

	public void setWeekDownloadNum(Integer weekDownloadNum) {
		this.weekDownloadNum = weekDownloadNum;
	}

	public Integer getMonthDownloadNum() {
		return monthDownloadNum;
	}

	public void setMonthDownloadNum(Integer monthDownloadNum) {
		this.monthDownloadNum = monthDownloadNum;
	}

	public Integer getHotDegree() {
		return hotDegree;
	}

	public void setHotDegree(Integer hotDegree) {
		this.hotDegree = hotDegree;
	}

	public Integer getUserRemarkNum() {
		return userRemarkNum;
	}

	public void setUserRemarkNum(Integer userRemarkNum) {
		this.userRemarkNum = userRemarkNum;
	}

	public Float getUserRemark() {
		return userRemark;
	}

	public void setUserRemark(Float userRemark) {
		this.userRemark = userRemark;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	@ManyToOne
	@JoinColumn(name="suffixId")
	public SoftSuffix getSuffix() {
		return suffix;
	}

	public void setSuffix(SoftSuffix suffix) {
		this.suffix = suffix;
	}

}
