package com.i8app.install.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 2014-03-05 15:57 佣金支取单
 * @author lixiangyang
 * @version 1.0
 *
 */
@Entity
@Table(name="commision_account")
public class CommisionAccount {
	private int id, empId;
	private String  ticketNo, summary, secret;
	private BigDecimal  originalAmount, operAmount, remainAmount;
	private Timestamp financialTime;
	@Id
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public Timestamp getFinancialTime() {
		return financialTime;
	}
	public void setFinancialTime(Timestamp financialTime) {
		this.financialTime = financialTime;
	}
	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}
	public BigDecimal getOperAmount() {
		return operAmount;
	}
	public void setOperAmount(BigDecimal operAmount) {
		this.operAmount = operAmount;
	}
	public BigDecimal getRemainAmount() {
		return remainAmount;
	}
	public void setRemainAmount(BigDecimal remainAmount) {
		this.remainAmount = remainAmount;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public void setId(int id) {
		this.id = id;
	}
}
