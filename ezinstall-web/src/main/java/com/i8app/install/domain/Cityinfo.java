package com.i8app.install.domain;

import javax.persistence.*;

/**
 * <<驱动信息表>>的实体类
 *  表格式: id--主键
 *  vid--表示生产厂商, 一张表里面应该只有一个VID.
 *  pid--表示手机型号,所有PID以逗号隔开放在一条记录中
 *  filePath--表示驱动包的网络地址
 *  osType--表示操作系统类型
 * 
 * @author JingYing
 * 
 */
@Entity
public class Cityinfo {
	private Integer cityID;
	private String cityName;
	private Provinceinfo province;

	@Id
	@GeneratedValue
	public Integer getCityID() {
		return cityID;
	}
	public void setCityID(Integer cityID) {
		this.cityID = cityID;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "provinceID") 
	public Provinceinfo getProvince() {
		return province;
	}
	public void setProvince(Provinceinfo province) {
		this.province = province;
	}
}
