package com.i8app.install.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 2014-03-05 15:57 佣金入账单
 * @author lixiangyang
 * @version 1.0
 *
 */
@Entity
@Table(name="commision_deposit")
public class CommisionDeposit {
	private Integer id,empid, bookkeepingStatus;
	private String inputEmpName,fileName;
	private Timestamp inputTime,bookkeepingTime;
	private BigDecimal inputAmount;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEmpid() {
		return empid;
	}
	public void setEmpid(Integer empid) {
		this.empid = empid;
	}
	public Integer getBookkeepingStatus() {
		return bookkeepingStatus;
	}
	public void setBookkeepingStatus(Integer bookkeepingStatus) {
		this.bookkeepingStatus = bookkeepingStatus;
	}
	public String getInputEmpName() {
		return inputEmpName;
	}
	public void setInputEmpName(String inputEmpName) {
		this.inputEmpName = inputEmpName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Timestamp getInputTime() {
		return inputTime;
	}
	public void setInputTime(Timestamp inputTime) {
		this.inputTime = inputTime;
	}
	public Timestamp getBookkeepingTime() {
		return bookkeepingTime;
	}
	public void setBookkeepingTime(Timestamp bookkeepingTime) {
		this.bookkeepingTime = bookkeepingTime;
	}
	public BigDecimal getInputAmount() {
		return inputAmount;
	}
	public void setInputAmount(BigDecimal inputAmount) {
		this.inputAmount = inputAmount;
	}
}
