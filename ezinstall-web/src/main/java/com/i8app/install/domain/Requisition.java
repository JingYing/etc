package com.i8app.install.domain;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * 2014-03-05 15:57 营业员信息修改申请单
 * @author lixiangyang
 * @version 1.0
 *
 */
@Entity
@Table(name="emp_modify_requisition")
public class Requisition {
	private Integer id,isAppeal, approveStatus;
	private String phoneNo,alipayUser,alipayAccount,alipayPwd,approver,approveNote;
	private Timestamp applyTime,approveTime;
	private Employee empid;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empid") 
	public Employee getEmpid() {
		return empid;
	}
	public void setEmpid(Employee empid) {
		this.empid = empid;
	}
	public Integer getIsAppeal() {
		return isAppeal;
	}
	public void setIsAppeal(Integer isAppeal) {
		this.isAppeal = isAppeal;
	}
	public Integer getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(Integer approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getAlipayUser() {
		return alipayUser;
	}
	public void setAlipayUser(String alipayUser) {
		this.alipayUser = alipayUser;
	}
	public String getAlipayAccount() {
		return alipayAccount;
	}
	public void setAlipayAccount(String alipayAccount) {
		this.alipayAccount = alipayAccount;
	}
	public String getAlipayPwd() {
		return alipayPwd;
	}
	public void setAlipayPwd(String alipayPwd) {
		this.alipayPwd = alipayPwd;
	}
	public String getApprover() {
		return approver;
	}
	public void setApprover(String approver) {
		this.approver = approver;
	}
	public String getApproveNote() {
		return approveNote;
	}
	public void setApproveNote(String approveNote) {
		this.approveNote = approveNote;
	}
	public Timestamp getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(Timestamp applyTime) {
		this.applyTime = applyTime;
	}
	public Timestamp getApproveTime() {
		return approveTime;
	}
	public void setApproveTime(Timestamp approveTime) {
		this.approveTime = approveTime;
	}
}
