package com.i8app.install.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * <<音乐信息表>>的实体类
 * 
 * @author JingYing
 * 
 */
@Entity
public class MusicInfo {

	private Integer id, musicLen, totalDownloadNum, weekDownloadNum,
			monthDownloadNum, hotDegree, userRemarkNum;
	private String musicName, singerName, specialName, musicFile, desc,
			musicImgFile, memo;
	private Float musicFileSize, userRemark;
	private Date dateAdded;
	private MusicType musicType;
	private SoftSuffix suffix;
	

	@Id
	@GeneratedValue
	@Column(name="mid")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 128)
	public String getMusicName() {
		return musicName;
	}

	public void setMusicName(String musicName) {
		this.musicName = musicName;
	}

	public Float getMusicFileSize() {
		return musicFileSize;
	}

	public void setMusicFileSize(Float musicFileSize) {
		this.musicFileSize = musicFileSize;
	}

	public Integer getMusicLen() {
		return musicLen;
	}

	public void setMusicLen(Integer musicLen) {
		this.musicLen = musicLen;
	}

	@Column(length = 64)
	public String getSingerName() {
		return singerName;
	}

	public void setSingerName(String singerName) {
		this.singerName = singerName;
	}

	public String getSpecialName() {
		return specialName;
	}

	public void setSpecialName(String specialName) {
		this.specialName = specialName;
	}

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "musicTypeId", nullable=true)
	public MusicType getMusicType() {
		return musicType;
	}

	public void setMusicType(MusicType musicType) {
		this.musicType = musicType;
	}

	public String getMusicFile() {
		return musicFile;
	}

	public void setMusicFile(String musicFile) {
		this.musicFile = musicFile;
	}

	@Column(name = "description")
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getMusicImgFile() {
		return musicImgFile;
	}

	public void setMusicImgFile(String musicImgFile) {
		this.musicImgFile = musicImgFile;
	}

	@Temporal(TemporalType.DATE)
	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Integer getTotalDownloadNum() {
		return totalDownloadNum;
	}

	public void setTotalDownloadNum(Integer totalDownloadNum) {
		this.totalDownloadNum = totalDownloadNum;
	}

	public Integer getWeekDownloadNum() {
		return weekDownloadNum;
	}

	public void setWeekDownloadNum(Integer weekDownloadNum) {
		this.weekDownloadNum = weekDownloadNum;
	}

	public Integer getMonthDownloadNum() {
		return monthDownloadNum;
	}

	public void setMonthDownloadNum(Integer monthDownloadNum) {
		this.monthDownloadNum = monthDownloadNum;
	}

	public Integer getHotDegree() {
		return hotDegree;
	}

	public void setHotDegree(Integer hotDegree) {
		this.hotDegree = hotDegree;
	}

	public Integer getUserRemarkNum() {
		return userRemarkNum;
	}

	public void setUserRemarkNum(Integer userRemarkNum) {
		this.userRemarkNum = userRemarkNum;
	}

	public Float getUserRemark() {
		return userRemark;
	}

	public void setUserRemark(Float userRemark) {
		this.userRemark = userRemark;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@ManyToOne
	@JoinColumn(name="suffixId")
	public SoftSuffix getSuffix() {
		return suffix;
	}

	public void setSuffix(SoftSuffix suffix) {
		this.suffix = suffix;
	}

}
