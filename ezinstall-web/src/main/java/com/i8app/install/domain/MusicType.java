package com.i8app.install.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
/**
 * <<音乐分类信息表>>的实体类
 * @author JingYing
 *
 */
@Entity
public class MusicType {
	
	private Integer musicTypeId , num;
	private String typeName;
	private MusicType parent;
	private Set<MusicInfo> musicInfoSet;

	@Id
	@GeneratedValue
	public Integer getMusicTypeId() {
		return musicTypeId;
	}

	public void setMusicTypeId(Integer musicTypeId) {
		this.musicTypeId = musicTypeId;
	}

	@Column(length=128)
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="parentMusicTypeId",nullable=true)
	public MusicType getParent() {
		return parent;
	}

	public void setParent(MusicType parent) {
		this.parent = parent;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@OneToMany(mappedBy="musicType")
	@JoinColumn(name="musicTypeId")
	public Set<MusicInfo> getMusicInfoSet() {
		return musicInfoSet;
	}

	public void setMusicInfoSet(Set<MusicInfo> musicInfoSet) {
		this.musicInfoSet = musicInfoSet;
	}
}
