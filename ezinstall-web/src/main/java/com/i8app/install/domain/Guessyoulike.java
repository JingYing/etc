package com.i8app.install.domain;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Guessyoulike {
	 private Integer id;
	 private String peopleId,phoneno,appname,app_uuid,icon,versions;
	 private Float file_size;
	 private Integer friends_count;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getApp_uuid() {
		return app_uuid;
	}
	public void setApp_uuid(String app_uuid) {
		this.app_uuid = app_uuid;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Float getFile_size() {
		return file_size;
	}
	public void setFile_size(Float file_size) {
		this.file_size = file_size;
	}
	public Integer getFriends_count() {
		return friends_count;
	}
	public void setFriends_count(Integer friends_count) {
		this.friends_count = friends_count;
	}
	public String getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
	public String getVersions() {
		return versions;
	}
	public void setVersions(String versions) {
		this.versions = versions;
	}
}
