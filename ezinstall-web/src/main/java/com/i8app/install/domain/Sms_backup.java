package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;


@Entity
public class Sms_backup {
	 private Integer id;
	 private String peopleId;
	 private String phoneno;
	 private Integer threadId,sms_id;
	 private String address,person,backup_date,thread_date;
	 private Integer protocol,isread,sms_status,sms_type,presnet,locked,error_code,seen;
	 private String subject,sms_body,columncenter,long_date;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	@Column(name="thread_id")
	public Integer getThreadId() {
		return threadId;
	}
	public void setThreadId(Integer threadId) {
		this.threadId = threadId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBackup_date() {
		return backup_date;
	}
	public void setBackup_date(String backup_date) {
		this.backup_date = backup_date;
	}
	public Integer getProtocol() {
		return protocol;
	}
	public void setProtocol(Integer protocol) {
		this.protocol = protocol;
	}
	public Integer getIsread() {
		return isread;
	}
	public void setIsread(Integer isread) {
		this.isread = isread;
	}
	@Column(name="reply_path_present")
	public Integer getPresnet() {
		return presnet;
	}
	public void setPresnet(Integer presnet) {
		this.presnet = presnet;
	}
	public Integer getLocked() {
		return locked;
	}
	public void setLocked(Integer locked) {
		this.locked = locked;
	}
	public Integer getSeen() {
		return seen;
	}
	public void setSeen(Integer seen) {
		this.seen = seen;
	}
	@Column(name="back_subject")
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getPerson() {
		return person;
	}
	@Column(name="service_center")
	public String getColumncenter() {
		return columncenter;
	}
	public void setColumncenter(String columncenter) {
		this.columncenter = columncenter;
	}
	public void setPerson(String person) {
		this.person = person;
	}
	public String getThread_date() {
		return thread_date;
	}
	public void setThread_date(String thread_date) {
		this.thread_date = thread_date;
	}
	public Integer getSms_id() {
		return sms_id;
	}
	public void setSms_id(Integer sms_id) {
		this.sms_id = sms_id;
	}
	@Column(name="back_status")
	public Integer getSms_status() {
		return sms_status;
	}
	public void setSms_status(Integer sms_status) {
		this.sms_status = sms_status;
	}
	@Column(name="back_type")
	public Integer getSms_type() {
		return sms_type;
	}
	public void setSms_type(Integer sms_type) {
		this.sms_type = sms_type;
	}
	@Column(name="error_code")
	public Integer getError_code() {
		return error_code;
	}
	public void setError_code(Integer error_code) {
		this.error_code = error_code;
	}
	@Column(name="body")
	public String getSms_body() {
		return sms_body;
	}
	public void setSms_body(String sms_body) {
		this.sms_body = sms_body;
	}
	public String getLong_date() {
		return long_date;
	}
	public void setLong_date(String long_date) {
		this.long_date = long_date;
	}
}
