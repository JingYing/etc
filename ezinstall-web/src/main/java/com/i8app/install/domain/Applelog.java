package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 2014-05-08 16:37 苹果树日志
 * @author lixiangyang
 * @version 1.0
 *
 */
@Entity
@Table(name="applelog")
public class Applelog {
	private Integer id,isOk,isActivate;
	private String adudid,mac,inserttime,phoneversion,rscid;
//	private AppleUser mac;
	@Id
	@Column(name = "id") 
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIsOk() {
		return isOk;
	}
	public void setIsOk(Integer isOk) {
		this.isOk = isOk;
	}
	public Integer getIsActivate() {
		return isActivate;
	}
	public void setIsActivate(Integer isActivate) {
		this.isActivate = isActivate;
	}
	public String getAdudid() {
		return adudid;
	}
	public void setAdudid(String adudid) {
		this.adudid = adudid;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getPhoneversion() {
		return phoneversion;
	}
	public void setPhoneversion(String phoneversion) {
		this.phoneversion = phoneversion;
	}
	public String getRscid() {
		return rscid;
	}
	public void setRscid(String rscid) {
		this.rscid = rscid;
	}
}
