package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;


@Entity
public class Threads {
	 private Integer id;
	 private String peopleId;
	 private String phoneno,backup_date,thread_date;
	 private String ids,snippet;
	 private Integer message_count,snippet_cs,isread,type,error,attachment,thread_id;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getBackup_date() {
		return backup_date;
	}
	public void setBackup_date(String backup_date) {
		this.backup_date = backup_date;
	}
	@Column(name="recipient_ids")
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getSnippet() {
		return snippet;
	}
	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}
	public Integer getMessage_count() {
		return message_count;
	}
	public void setMessage_count(Integer message_count) {
		this.message_count = message_count;
	}
	public Integer getSnippet_cs() {
		return snippet_cs;
	}
	public void setSnippet_cs(Integer snippet_cs) {
		this.snippet_cs = snippet_cs;
	}
	public Integer getIsread() {
		return isread;
	}
	public void setIsread(Integer isread) {
		this.isread = isread;
	}
	@Column(name="back_type")
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getError() {
		return error;
	}
	public void setError(Integer error) {
		this.error = error;
	}
	@Column(name="has_attachment")
	public Integer getAttachment() {
		return attachment;
	}
	public void setAttachment(Integer attachment) {
		this.attachment = attachment;
	}
	public String getThread_date() {
		return thread_date;
	}
	public void setThread_date(String thread_date) {
		this.thread_date = thread_date;
	}
	public Integer getThread_id() {
		return thread_id;
	}
	public void setThread_id(Integer thread_id) {
		this.thread_id = thread_id;
	}
}
