package com.i8app.install.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * 2014-03-24 15:57 标签号表
 * @author lixiangyang
 * @version 1.0
 *
 */
@Entity
@Table(name="tag")
public class Tag {
	private Integer id;
	private String tagname,tagDesc;
//	private Tag parent;
//	private Set<Tag> children;
	private int pid;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}
	public String getTagDesc() {
		return tagDesc;
	}
	public void setTagDesc(String tagDesc) {
		this.tagDesc = tagDesc;
	}
	public void setId(Integer id) {
		this.id = id;
	}
//	@ManyToOne
//	@JoinColumn(name="pid")
//	public Tag getParent() {
//		return parent;
//	}
//	public void setParent(Tag parent) {
//		this.parent = parent;
//	}
//	@OneToMany(mappedBy="parent")
//	public Set<Tag> getChildren() {
//		return children;
//	}
//	public void setChildren(Set<Tag> children) {
//		this.children = children;
//	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
}
