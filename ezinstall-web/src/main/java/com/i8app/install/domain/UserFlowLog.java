package com.i8app.install.domain;

import javax.persistence.*;
/**
 * 用户流量日志
 * @author lixiangyang
 *
 */
@Entity
public class UserFlowLog {
	private String id;
	private String peopleId;
	private String userNumber;
	private String phoneNumber;
	private String phoneIMEI;
	private String phoneIMSI;
	private int privinceId,cityId;
	private String provName,cityName;
	private String manuName;
	private String modelName,deptName,deptId;
	public String getManuName() {
		return manuName;
	}
	public void setManuName(String manuName) {
		this.manuName = manuName;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	private String os;
	private String osver;
	private String periodTime,startTime,endTime;
	private double flowValue,wifiFlowValue,mobileFlowValue;
	@Id
	@GeneratedValue
	@Column(name="id")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
	public String getUserNumber() {
		return userNumber;
	}
	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	@Column(name="imei")
	public String getPhoneIMEI() {
		return phoneIMEI;
	}
	public void setPhoneIMEI(String phoneIMEI) {
		this.phoneIMEI = phoneIMEI;
	}
	@Column(name="imsi")
	public String getPhoneIMSI() {
		return phoneIMSI;
	}
	public void setPhoneIMSI(String phoneIMSI) {
		this.phoneIMSI = phoneIMSI;
	}
	@Column(name="provId")
	public int getPrivinceId() {
		return privinceId;
	}
	public void setPrivinceId(int privinceId) {
		this.privinceId = privinceId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public String getProvName() {
		return provName;
	}
	public void setProvName(String provName) {
		this.provName = provName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	@Column(name="osVersion")
	public String getOsver() {
		return osver;
	}
	public void setOsver(String osver) {
		this.osver = osver;
	}
	public String getPeriodTime() {
		return periodTime;
	}
	public void setPeriodTime(String periodTime) {
		this.periodTime = periodTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public double getFlowValue() {
		return flowValue;
	}
	public void setFlowValue(double flowValue) {
		this.flowValue = flowValue;
	}
	public double getWifiFlowValue() {
		return wifiFlowValue;
	}
	public void setWifiFlowValue(double wifiFlowValue) {
		this.wifiFlowValue = wifiFlowValue;
	}
	public double getMobileFlowValue() {
		return mobileFlowValue;
	}
	public void setMobileFlowValue(double mobileFlowValue) {
		this.mobileFlowValue = mobileFlowValue;
	}
}
