package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 * 信号投诉
 * @author lixiangyang
 *
 */
@Entity
public class Messagelog {
	private Integer id,sendtype,acceptedtype,messagetype,isdelete;
	private String sender,recipients,messagecontent,file_url,jump_url,sendtime,senderid,acceptedid;
	@Id
	@GeneratedValue
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSendtype() {
		return sendtype;
	}
	public void setSendtype(Integer sendtype) {
		this.sendtype = sendtype;
	}
	public Integer getAcceptedtype() {
		return acceptedtype;
	}
	public void setAcceptedtype(Integer acceptedtype) {
		this.acceptedtype = acceptedtype;
	}
	public Integer getMessagetype() {
		return messagetype;
	}
	public void setMessagetype(Integer messagetype) {
		this.messagetype = messagetype;
	}
	public Integer getIsdelete() {
		return isdelete;
	}
	public void setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipients() {
		return recipients;
	}
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	public String getMessagecontent() {
		return messagecontent;
	}
	public void setMessagecontent(String messagecontent) {
		this.messagecontent = messagecontent;
	}
	public String getFile_url() {
		return file_url;
	}
	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}
	public String getJump_url() {
		return jump_url;
	}
	public void setJump_url(String jump_url) {
		this.jump_url = jump_url;
	}
	public String getSendtime() {
		return sendtime;
	}
	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}
	public String getSenderid() {
		return senderid;
	}
	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	public String getAcceptedid() {
		return acceptedid;
	}
	public void setAcceptedid(String acceptedid) {
		this.acceptedid = acceptedid;
	}
}
