package com.i8app.install.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="app_install_pack")
public class AppInstallPack {
	private String uuid;
	private String cpId, originalPackId, appVer, osMinVer, os,
			apkPkgName, ipaBundleId, cpUpdateTime, version,ipaItemId;

	private String fileSize;
	private Long appVerValue, osMinVerValue;
	private Date updateTime;
	private Integer apkVersionCode;
	private App app;
	
	@Id
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getApkPkgName() {
		return apkPkgName;
	}

	public void setApkPkgName(String apkPkgName) {
		this.apkPkgName = apkPkgName;
	}

	public Long getAppVerValue() {
		return appVerValue;
	}
	
	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public void setAppVerValue(Long appVerValue) {
		this.appVerValue = appVerValue;
	}

	public Long getOsMinVerValue() {
		return osMinVerValue;
	}

	public void setOsMinVerValue(Long osMinVerValue) {
		this.osMinVerValue = osMinVerValue;
	}

	public String getAppVer() {
		return appVer;
	}

	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}

	public String getOsMinVer() {
		return osMinVer;
	}

	public void setOsMinVer(String osMinVer) {
		this.osMinVer = osMinVer;
	}

	public String getOriginalPackId() {
		return originalPackId;
	}

	public void setOriginalPackId(String originalPackId) {
		this.originalPackId = originalPackId;
	}

	public String getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(String cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getIpaBundleId() {
		return ipaBundleId;
	}

	public void setIpaBundleId(String ipaBundleId) {
		this.ipaBundleId = ipaBundleId;
	}

	public Integer getApkVersionCode() {
		return apkVersionCode;
	}

	public void setApkVersionCode(Integer apkVersionCode) {
		this.apkVersionCode = apkVersionCode;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@ManyToOne
	@JoinColumn(name="appId")
	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}
	public String getIpaItemId() {
		return ipaItemId;
	}

	public void setIpaItemId(String ipaItemId) {
		this.ipaItemId = ipaItemId;
	}

}
