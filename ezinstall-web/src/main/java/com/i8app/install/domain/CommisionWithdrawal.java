package com.i8app.install.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 2014-03-05 15:57 佣金支取单
 * @author lixiangyang
 * @version 1.0
 *
 */
@Entity
@Table(name="commision_withdrawal")
public class CommisionWithdrawal {
	private String id,approver,approveNote;
	private Timestamp applyTime,approveTime,bookkeepingTime;
	private Integer empid, approveStatus, bookkeepingStatus;
	private BigDecimal apply_amount,apply_amount_fee, apply_amount_tax, bookkeepingRemaining;
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApprover() {
		return approver;
	}
	public void setApprover(String approver) {
		this.approver = approver;
	}
	public String getApproveNote() {
		return approveNote;
	}
	public void setApproveNote(String approveNote) {
		this.approveNote = approveNote;
	}
	public Timestamp getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(Timestamp applyTime) {
		this.applyTime = applyTime;
	}
	public Timestamp getApproveTime() {
		return approveTime;
	}
	public void setApproveTime(Timestamp approveTime) {
		this.approveTime = approveTime;
	}
	public Timestamp getBookkeepingTime() {
		return bookkeepingTime;
	}
	public void setBookkeepingTime(Timestamp bookkeepingTime) {
		this.bookkeepingTime = bookkeepingTime;
	}
	public Integer getEmpid() {
		return empid;
	}
	public void setEmpid(Integer empid) {
		this.empid = empid;
	}
	public Integer getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(Integer approveStatus) {
		this.approveStatus = approveStatus;
	}
	public Integer getBookkeepingStatus() {
		return bookkeepingStatus;
	}
	public void setBookkeepingStatus(Integer bookkeepingStatus) {
		this.bookkeepingStatus = bookkeepingStatus;
	}
	public BigDecimal getApply_amount() {
		return apply_amount;
	}
	public void setApply_amount(BigDecimal apply_amount) {
		this.apply_amount = apply_amount;
	}
	public BigDecimal getBookkeepingRemaining() {
		return bookkeepingRemaining;
	}
	public void setBookkeepingRemaining(BigDecimal bookkeepingRemaining) {
		this.bookkeepingRemaining = bookkeepingRemaining;
	}
	public BigDecimal getApply_amount_fee() {
		return apply_amount_fee;
	}
	public void setApply_amount_fee(BigDecimal apply_amount_fee) {
		this.apply_amount_fee = apply_amount_fee;
	}
	public BigDecimal getApply_amount_tax() {
		return apply_amount_tax;
	}
	public void setApply_amount_tax(BigDecimal apply_amount_tax) {
		this.apply_amount_tax = apply_amount_tax;
	}
	
}
