package com.i8app.install.domain;

import javax.persistence.*;

/**
 * <<驱动信息表>>的实体类
 *  表格式: id--主键
 *  vid--表示生产厂商, 一张表里面应该只有一个VID.
 *  pid--表示手机型号,所有PID以逗号隔开放在一条记录中
 *  filePath--表示驱动包的网络地址
 *  osType--表示操作系统类型
 * 
 * @author JingYing
 * 
 */
@Entity
public class Countyinfo {
	private Integer countyID;
	private String countyName;
	private Cityinfo city;
	private Provinceinfo province;
	@Id
	@GeneratedValue
	public Integer getCountyID() {
		return countyID;
	}
	public void setCountyID(Integer countyID) {
		this.countyID = countyID;
	}
	public String getCountyName() {
		return countyName;
	}
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cityID") 
	public Cityinfo getCity() {
		return city;
	}

	public void setCity(Cityinfo city) {
		this.city = city;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "provinceid") 
	public Provinceinfo getProvince() {
		return province;
	}
	public void setProvince(Provinceinfo province) {
		this.province = province;
	}
	
}
