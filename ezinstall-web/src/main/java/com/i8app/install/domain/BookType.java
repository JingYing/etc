
package com.i8app.install.domain;

import javax.persistence.*;

@Entity
public class BookType {
	private Integer bookTypeId;
	private String typeName;
	private String memo;
	@Id
	@GeneratedValue
	public Integer getBookTypeId() {
		return bookTypeId;
	}
	public void setBookTypeId(Integer bookTypeId) {
		this.bookTypeId = bookTypeId;
	}
	@Column(length =64)
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	@Column(length = 64)
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
}
