package com.i8app.install.domain;

import javax.persistence.*;

/**
 * <<驱动信息表>>的实体类
 *  表格式: id--主键
 *  vid--表示生产厂商, 一张表里面应该只有一个VID.
 *  pid--表示手机型号,所有PID以逗号隔开放在一条记录中
 *  filePath--表示驱动包的网络地址
 *  osType--表示操作系统类型
 * 
 * @author JingYing
 * 
 */
@Entity
@Table(name = "provinceinfo") 
public class Provinceinfo {
	private Integer provinceID;
	private String provinceName;
	@Id
	@GeneratedValue
	public Integer getProvinceID() {
		return provinceID;
	}
	public void setProvinceID(Integer provinceID) {
		this.provinceID = provinceID;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
}
