package com.i8app.install;

/**
 * 见applicationContext.xml对该类的注入
 * 
 * @author JingYing 2013-6-17
 */
public class Config {
	private String fileServerUrl, fileServerDir, shortDomain, longDomain, areaCode,proList;

	public String getFileServerUrl() {
		return fileServerUrl;
	}

	public void setFileServerUrl(String fileServerUrl) {
		this.fileServerUrl = fileServerUrl;
	}

	public String getFileServerDir() {
		return fileServerDir;
	}

	public void setFileServerDir(String fileServerDir) {
		this.fileServerDir = fileServerDir;
	}

	public String getShortDomain() {
		return shortDomain;
	}

	public void setShortDomain(String shortDomain) {
		this.shortDomain = shortDomain;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getLongDomain() {
		return longDomain;
	}

	public void setLongDomain(String longDomain) {
		this.longDomain = longDomain;
	}

	public String getProList() {
		return proList;
	}

	public void setProList(String proList) {
		this.proList = proList;
	}
}
