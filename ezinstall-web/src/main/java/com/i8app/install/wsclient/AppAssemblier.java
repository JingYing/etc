package com.i8app.install.wsclient;

import java.util.List;
import java.util.Map;

/**
 * app汇集接口
 * 该接口也对外发布为hessian接口,由ldms调用.
 * 
 * @author JingYing 2013-7-15
 */
public interface AppAssemblier {

	/**
	 * 汇集指定的cp软件
	 * @param cpId cp商代码
	 * @param originalPackIds cp软件原始id
	 * @return map key:原始id, value:汇集状态值
	 */
	Map<String, Integer> assembly(String cpId, List<String> originalPackIds);

	/**
	 * 启动所有的汇集程序. 
	 * 汇集流程: 
	 * 1. 根据参数选择指定CP商的库进行汇集
	 * 2. 汇集APP
	 * 3. 调用CP商的下架接口
	 * 4. 集中处理app版本值的问题(把版本号转换成long值,便于比较版本高低)
	 * 5. 再次下载失效的ICON
	 */
	void startAssembly();
	
	/**
	 * 把app重新绑定到某cp商下的某个软件
	 * @param appId app表的id
	 * @param cpId 
	 * @param originalAppId
	 */
	void bindSource(String appId, String cpId, String originalAppId);

	/**
	 * 修复app中缺失的ICON
	 */
	void fixMissingIcon();
	
	/**
	 * 根据CP软件表的下架状态, 更新app/appInstallPack表的下架状态
	 */
	void updatePackStatus();
	
	/**
	 * 接收安装包名, 之后根据安装包名对新增软件或更新软件
	 * @param cpId 取值见com.i8app.ezinstall.common.app.Constants
	 * @param os 取值见com.i8app.ezinstall.common.app.Constants
	 * @param pkgname apk或ipa的包名
	 * @return 
	 */
	boolean receivePkgname(String cpId, String os, String pkgname);

}