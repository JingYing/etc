package com.i8app.install.wsclient;

import java.io.IOException;

/**
 * ECS功能的hessian接口
 * 
 */
public interface EcsQuery {

	/**
	 * 查询2G/3G用户上网用的手机号码. 仅限于联通用户使用2/3G环境访问山西服务器时, 才能获取手机号.
	 * @param unikey 长度为32个小写字符的16进制字符串。 
	 * 		 请求端在访问山西服务器时, 在URL后加unikey=xxxxxx参数,联通网关就会把该unikey参数与该上网链路所用的手机号保存起来(有效期一小时)
	 * @param requestIp request请求方的IP地址, 在服务器端获取
	 * @return 返回：手机号码,IMEI,IMSI,Lac,CellID （逗号隔开） 如果未查询到，则返回0
	 * @throws IOException 访问网关或解析信息时异常
	 */
	public String queryMobileNumber(String unikey, String requestIp) throws IOException;

	
	/**
	 * 详见ECS文档5.8.2 	免费资源综合查询接口（沃家庭分册）
	 * 返回示例
	 * <pre>
	 * {@code
	 * <FreeResQueryRsp>
		<RspCode>00</RspCode>
		<RspDesc>成功</RspDesc>
		<ResourceItem>
			<FreeResType>01</FreeResType>
			<FreeResValue>0</FreeResValue>
			<FreeUsedValue>0</FreeUsedValue>
			<FreeResUseValue>0</FreeResUseValue>
			<FreeResUnit>KB(gprs流量)</FreeResUnit>
			<FreeResAwardTime>20131201000000</FreeResAwardTime>
		</ResourceItem>
		<ResourceItem>
			<FreeResType>01</FreeResType>
			<FreeResValue>307200</FreeResValue>
			<FreeUsedValue>3</FreeUsedValue>
			<FreeResUseValue>303134</FreeResUseValue>
			<FreeResUnit>KB(gprs流量)</FreeResUnit>
			<FreeResAwardTime>20131201000000</FreeResAwardTime>
		</ResourceItem>
	   </FreeResQueryRsp>
	 * }
	   字段说明
	   FreeResType: 01：上网流量值，单位 MB（3G用户返回）
				02：国内通话，单位分钟
				03：国内可视电话，单位分钟
				04：短信，单位条
				05： M值，单位个
				06： T值，单位个
				08：彩信，单位条
				09：本地共享时长，单位分钟
				10：国内共享时长，单位分钟（如：沃家庭、长市漫一体等）
				11：本地长途，单位分钟
				12：本地通话，单位分钟
				13：GPRS流量，单位MB（2G用户返回）
		FreeResValue: 免费资源值
		FreeUsedValue: 已用资源值（返回当前用户已使用的免费资源值）
		FreeResUseValue: 剩余资源值
		FreeResUnit:免费资源单位
		FreeResAwardTime: 免费资源赠送时间,YYYYMMDDHHMISS（免费资源生效时间）
	 * </pre>

	 * @param netType 01 2G（GSM）| 02 3G（WCDMA）| 03 固定电话 | 04 宽带（ADSL）| 05 宽带（LAN）| 06 小灵通（含大灵通）
	 * @param userNumber 手机号
	 * @return xml格式 
	 * @throws IOException 访问网关或解析信息时异常
	 */
	public String freeResQueryReq(String netType, String userNumber) throws IOException;
	

	/**
	 * 5.8.1 	账户付费关系查询接口（沃家庭分册）
	 * @param netType 
	 * @see com.i8app.connector.ecs.EcsQuery#freeResQueryReq freeResQueryReq的netType取值
	 * @param userNumber 手机号
	 * @return xml格式
	 * @throws IOException 访问网关或解析信息时异常
	 */
	public String acctRationalReq(String netType, String userNumber) throws IOException;

	/**
	 * 5.3.3 	综合用户信息查询（综合业务分册1）
	 * @param netType
	 * @param userNumber
	 * @return
	 * @throws IOException
	 */
	public String userReq(String netType, String userNumber) throws IOException;

	/**
	 * 5.2.12 	流量查询（查询受理分册）
	 * @param bizType 01：手机上网流量 02：3G后付费上网卡短信提醒
	 * @param userNumber
	 * @return
	 * @throws IOException
	 */
	public String flowSearchReq(String bizType, String userNumber) throws IOException;

	/**
	 * 5.2.8 	3G产品(套餐)查询（业务受理和查询分册）
	 * @param userNumber
	 * @return
	 * @throws IOException
	 */
	public String prodFindReq528(String userNumber) throws IOException;

	/**
	 * 5.2.9 	3G套餐余量查询（业务受理和查询分册）
	 * @param userNumber
	 * @return
	 * @throws IOException
	 */
	public String prodFindReq529(String userNumber) throws IOException;
	
}
