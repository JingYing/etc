package com.i8app.install.wsclient;

import java.io.IOException;

/**
 * 使用门面模式组合调用EcsQuery, 如需要ECS网关返回的原始信息, 直接调用EcsQuery.
 * 
 * @author JingYing 2013-12-9
 */
public interface EcsFacade {
	
	/**
	 * 5.2.8 	3G产品(套餐)查询（业务受理和查询分册）
	 * @param userNumber
	 * @return
	 */
	String prodFindReq528(String userNumber) throws IOException;
	
	/**
	 * 5.2.9 	3G套餐余量查询（业务受理和查询分册）
	 * @param userNumber
	 * @return
	 * @throws IOException
	 */
	String prodFindReq529(String userNumber) throws IOException;
	
	/**
	 * 5.2.12 	流量查询（查询受理分册）
	 * @param netType
	 * @param userNumber
	 * @return
	 * @throws IOException
	 */
	String flowSearchReq(String userNumber) throws IOException;

}
