package com.i8app.install;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

@SuppressWarnings("serial")
public class AuthInterceptor extends AbstractInterceptor {
	public String intercept(ActionInvocation invocation) throws Exception {
		if (ActionContext.getContext().getSession().get(Globals.CURRENT_USER) == null) {
			return Action.LOGIN;
		} 
		return invocation.invoke();
	}
}
