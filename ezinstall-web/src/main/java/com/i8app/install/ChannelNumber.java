package com.i8app.install;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * 给其它访问者分配的渠道号, 渠道号以MD5加密成16位字符串
 * 
 * @author JingYing 2013-6-8
 */
public class ChannelNumber {
	
	private List<String> numbers = new ArrayList<String>();
	
	{
		numbers.add("3b53f23827e6c35f");	//广西用渠道号,使用gx以md5的16位加密生成的字符串, 索引值为0
	}

	public List<String> getNumbers() {
		return numbers;
	}

	public void setNumbers(List<String> numbers) {
		this.numbers = numbers;
	}
}
