package com.i8app.install.web.action;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsSpringTestCase;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.i8app.install.service.AppleService;
import com.opensymphony.xwork2.ActionProxy;

public class AppleActionTest extends StrutsSpringTestCase	{
	
	@Test
	public void testLog1() throws Exception	{
		request.setParameter("userid", "1018");
		request.setParameter("appid", "1001");
		request.setParameter("openudid", "d8c0a38232cf253d7fcd6bdeb48ef21087ab582d");
		request.setParameter("sign", "c0722858723ee758cb4822c5af2fd141");
		request.setParameter("mac", "a8:2b:1e:63:1e:78");
		request.setParameter("idfa", "29633e48-56be-4020-a454-b2123c15e456");
		request.setParameter("version", "2.7");
		
		ActionProxy proxy = getActionProxy("/apple/appleuser");
		AppleAction action = (AppleAction)proxy.getAction();
		
		AppleService mock = createNiceMock(AppleService.class);
//		expect(mock.buildSmsInstallLog(5, 1, "123456", "13112345678",70001)).andReturn(new Integer(100));
//		replay(mock);
		action.setAppleService(mock);
		proxy.execute();
		verify(mock);
	}
	
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		SessionFactory sessionFactory = lookupSessionFactory(request);
		Session hibernateSession= getSession(sessionFactory);
		TransactionSynchronizationManager.bindResource(sessionFactory, 
		                                               new SessionHolder(hibernateSession));
	}
	
	 private Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
	     Session session = SessionFactoryUtils.getSession(sessionFactory, true);
	     FlushMode flushMode = FlushMode.NEVER;
	     if (flushMode != null) {
	        session.setFlushMode(flushMode);
	     }
	      return session;
	 }
	 private SessionFactory lookupSessionFactory(HttpServletRequest request) {
	     return (SessionFactory)this.applicationContext.getBean(SessionFactory.class);
	}

	@Test
	public void testappleuser() throws Exception {
//		String param = "{\"imei\":\"2q3423412\",\"imsi\":\"1342341234134\",\"no\":\"15901031592\",\"empno\":\"218020\",\"provid\":\"44\",\"uuid\":\"325c3894-ae2c-41cd-8346-192cbe4d9621\",\"installType\":\"1\"}";
//		String param ="?userid=1018&appid=1001&mac=a8:2b:1e:63:1e:78&idfa=29633e48-56be-4020-a454-b2123c15e456&openudid=d8c0a38232cf253d7fcd6bdeb48ef21087ab582d&sign=c0722858723ee758cb4822c5af2fd141&version=2.7";
		String param ="?userid=1018&appid=1001&mac=A8:2B:1E:63:1E:78&idfa=29633e48-56be-4020-a454-b2123c15e456&openudid=d8c0a38232cf253d7fcd6bdeb48ef21087ab582d&sign=c0722858723ee758cb4822c5af2fd141&version=2.7";
		request.setContent(param.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/apple/appleuser");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
}
