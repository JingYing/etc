package com.i8app.install.web.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsSpringTestCase;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.opensymphony.xwork2.ActionProxy;

public class LogActionTest extends StrutsSpringTestCase {
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		SessionFactory sessionFactory = lookupSessionFactory(request);
		Session hibernateSession= getSession(sessionFactory);
		TransactionSynchronizationManager.bindResource(sessionFactory, 
		                                               new SessionHolder(hibernateSession));
	}
	
	 private Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
	     Session session = SessionFactoryUtils.getSession(sessionFactory, true);
	     FlushMode flushMode = FlushMode.NEVER;
	     if (flushMode != null) {
	        session.setFlushMode(flushMode);
	     }
	      return session;
	 }
	 private SessionFactory lookupSessionFactory(HttpServletRequest request) {
	     return (SessionFactory)this.applicationContext.getBean(SessionFactory.class);
	}

	@Test
	public void testphonelog() throws Exception {
		String param = "{\"imei\":\"2q3423412\",\"imsi\":\"1342341234134\",\"no\":\"15901031592\",\"empno\":\"218020\",\"provid\":\"44\",\"uuid\":\"325c3894-ae2c-41cd-8346-192cbe4d9621\",\"installType\":\"1\"}";
		request.setContent(param.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/phoneLog/phoneLog");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
	

}
