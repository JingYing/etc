package com.i8app.install.web.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsSpringTestCase;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.opensymphony.xwork2.ActionProxy;

public class PhonewizardActionTest extends StrutsSpringTestCase {
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		SessionFactory sessionFactory = lookupSessionFactory(request);
		Session hibernateSession= getSession(sessionFactory);
		TransactionSynchronizationManager.bindResource(sessionFactory, 
		                                               new SessionHolder(hibernateSession));
	}
	
	 private Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
	     Session session = SessionFactoryUtils.getSession(sessionFactory, true);
	     FlushMode flushMode = FlushMode.NEVER;
	     if (flushMode != null) {
	        session.setFlushMode(flushMode);
	     }
	      return session;
	 }
	 private SessionFactory lookupSessionFactory(HttpServletRequest request) {
	     return (SessionFactory)this.applicationContext.getBean(SessionFactory.class);
	}

	@Test
	public void testphonewizardlog() throws Exception {
//		String param = "{\"imei\":\"2q3423412\",\"imsi\":\"1342341234134\"," +
//				"\"phonenumber\":\"15901031592\",\"seqno\":\"adfa2345234\"," +
//				"\"manuName\":\"HTC\",\"ostype\":\"1\",\"modelName\":\"a6390\"," +
//				"\"osversion\":\"2.1.4\",\"empnumber\":\"aaNC0c\",\"provinceid\":\"64\"," +
//				"\"arraysoft\":[{\"appid\":11,\"appname\":\"QQ\",\"versionname\":\"3.3.4.2\"," +
//				"\"versioncode\":2,\"pkgname\":\"sadads\",\"flow\":313,\"mobileflow\":313," +
//				"\"wififlow\":0},{\"appid\":\"\",\"appname\":\"renren\",\"versionname\":\"3.3.3.2\"," +
//				"\"versioncode\":5,\"pkgname\":\"sada2323ds\",\"flow\":34523,\"mobileflow\":34523," +
//				"\"wififlow\":0}]}";
		
		String param = "{\"imei\":\"2q3423412\",\"imsi\":\"1342341234134\",\"phonenumber\":\"15901031592\",\"seqno\":\"d4e25f92d2ce64e7768c4204a4281979be9e709a\",\"manuName\":\"HTC\",\"ostype\":\"1\",\"modelName\":\"a6390\",\"osversion\":\"2.1.4\",\"empnumber\":\"aaNC0c\",\"provinceid\":\"64\",\"appinfo\":[{\"appid\":11,\"appname\":\"QQ\",\"versionname\":\"3.3.4.2\",\"versioncode\":2,\"pkgname\":\"sadads\",\"flow\":313,\"mobileflow\":313,\"wififlow\":0},{\"appid\":\"\",\"appname\":\"renren\",\"versionname\":\"3.3.3.2\",\"versioncode\":5,\"pkgname\":\"sada2323ds\",\"flow\":34523,\"mobileflow\":34523,\"wififlow\":0}]}";
		request.setContent(param.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/phonewizard/phonewizardlog");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
	
	@Test
	public void testActivelog() throws Exception	{
//		String par = "{\"imei\":\"imeimiei\",\"seqno\":\"seqnoseqno\",\"imsi\":\"imsimisi\",\"provinceid\":\"50\"," +
//				"\"empnumber\":\"000064\",\"pkgname\":\"\",\"isopen\":\"\",\"softid\":\"937127f5-d2c0-4b8b-91d3-137b00da16da\"}";
		
		String par = "{\"imei\":\"imeimiei\",\"seqno\":\"seqnoseqno\",\"imsi\":\"imsimisi\",\"provinceid\":\"50\"," +
		"\"empnumber\":\"000064\",\"pkgname\":\"\",\"isopen\":\"\",\"softid\":\"67\"}";//,\"ostype\":\"200\"
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/phonewizard/activelog");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}

}
