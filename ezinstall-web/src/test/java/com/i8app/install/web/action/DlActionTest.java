package com.i8app.install.web.action;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;

import com.i8app.install.service.LogService;
import com.opensymphony.xwork2.ActionProxy;

public class DlActionTest extends StrutsSpringTestCase	{
	
	@Test
	public void testLog1() throws Exception	{
		//url, type, empId, id, no
		request.setParameter("type", "5");
		request.setParameter("url", "http://www.baidu.com");
		request.setParameter("empId", "1");
		request.setParameter("id", "123456");
		request.setParameter("no", "13112345678");
		
		ActionProxy proxy = getActionProxy("/dl/log");
		DlAction action = (DlAction)proxy.getAction();
		
		LogService mock = createNiceMock(LogService.class);
		expect(mock.buildSmsInstallLog(5, 1, "123456", "13112345678",70001)).andReturn(new Integer(100));
		replay(mock);

		action.setLogService(mock);
		proxy.execute();
		verify(mock);
		System.out.println(action.getLocation());
	}

	@Test
	public void testLog2() throws Exception	{
		//url, type, empId, id, no
		request.setParameter("type", "4");
		request.setParameter("url", "http://www.baidu.com");
		request.setParameter("id", "123456");
		request.setParameter("no", "13112345678");
		
		ActionProxy proxy = getActionProxy("/dl/log");
		DlAction action = (DlAction)proxy.getAction();
		
		LogService mock = createNiceMock(LogService.class);
//		expect(mock.buildSmsInstallLog(5, 1, "123456", "13112345678")).andReturn(new Integer(100));
//		replay(mock);
		
		action.setLogService(mock);
		proxy.execute();
//		verify(mock);
		System.out.println(action.getLocation());
	}

}
