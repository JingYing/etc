package com.i8app.install.service;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.install.BaseTest;

public class SmsHessianServiceTest extends BaseTest{
	
	@Resource SmsHessianService service;
	
	@Test
	@Rollback(false)
	public void testGenShortUrl()	{
		System.out.println(service.genShortUrl("http://sx.phone.i8app.com/", 1));
	}
	@Test
	@Rollback(false)
	public void testSendShortUrl()	{
		try {
			service.sendShortUrl(8, "18608038789", 9858090, "2faf6884-1e51-45d5-bfcd-7b5cc69a6d2a");
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
