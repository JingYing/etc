<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>My JSP 'FromPost.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">

  </head>
  
  <body>
  <form action="${pageContext.request.contextPath}/people/app_backup.action" method="post" enctype="multipart/form-data">
  	<table>
  		<tr>
  			<td>
  				I D：<input type="text" id="uid" name="uid" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				手机：<input type="text" id="number" name="number" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				软件名称：<input type="text" id="name" name="name" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				包名：<input type="text" id="pname" name="pname" />
  			</td>
  		</tr>  		  		
  		<tr>
  			<td>
  				版本：<input type="text" id="version" name="version" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				文件：<input type="file" id="appFile" name="appFile" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				截图：<input type="file" id="picFile" name="picFile" />
  			</td>
  		</tr>   		  		
  		<tr>
  			<td>
  				大小：<input type="text" id="size" name="size" />
  			</td>
  		</tr>   		
  		<tr>
  			<td>
  				版本号：<textarea rows="10" cols="30" id="versioncode" name="versioncode"></textarea>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<input type="submit" id="submit" name="submit" value="保存"/>
  			</td>
  		</tr>   		
  	</table>
  </form>
  </body>
</html>
