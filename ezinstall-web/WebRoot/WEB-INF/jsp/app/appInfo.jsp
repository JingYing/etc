<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns=" http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<script type="text/javascript">
function phoneCheck(){
		if($.trim($('#phone').val()) == ""){
			alert("请输入手机号");
		 	return;
		}
		/*
		var h = /^1[3,5,4,8]{1}\d{9}$/;
		if(!h.test($.trim($('#phone').val()))){
			alert("请输入正确的手机号");
			return;	 			
		} 
		*/
	    $.ajax({
			url:"${pageContext.request.contextPath}/wap/checkPhone.action",
			data:{"phoneNumber":$.trim($('#phone').val())},
			type:"post",
			dataType:"json",
			success:function(data){
				switch(data.success){
				case 1:
					alert("未获取");
					break;
				case 2:
					alert("请查收");
					break;	
				case 3:
					alert("请输入正确的手机号");
					break;	
				case 4:
					alert("请输入手机号");
					break;											
				default:
					alert("获取失败");
					break;
				}
			}
		});      
}
	//短信下发
	function dxtj(){
		if($('#verificationcode').val() == ""){
			alert("请输入验证码");
		 	return;
		}
	    $.ajax({
			url:"${pageContext.request.contextPath}/wap/smsPhone.action",
			data:{"phoneNumber":$.trim($('#phone').val()),"verificationcode":$('#verificationcode').val(),"uuid":$('#uuid').val()},
			type:"post",
			dataType:"json",
			success:function(data){
				switch(data.success){
				case 0:
					alert("系统错误");
					break;				
				case 1:
					alert("请输入验证码");
					break;
				case 2:
					alert("请先获取验证码");
					break;						
				case 3:
					alert("验证码错误");
					break;	
				case 4:
					alert("发送成功");
					break;						
				default:
					alert("请输入手机号");
					break;
				}
			}
		});   		
	} 	
	function dxtjNoCheck(){
		if($.trim($('#phoneNoCheck').val()) == ""){
			alert("请输入手机号");
		 	return;
		}
		var h = /^1[3,5,4,8]{1}\d{9}$/;
		if(!h.test($.trim($('#phoneNoCheck').val()))){
			alert("请输入正确的手机号");
			return;	 			
		} 
	    $.ajax({
			url:"${pageContext.request.contextPath}/wap/smsPhoneNOCheck.action",
			data:{"phoneNumber":$.trim($('#phoneNoCheck').val()),"uuid":$('#uuid').val(),"dis":$('#dis').val(),"packUuid":$('#packuuid').val()},
			type:"post",
			dataType:"json",
			success:function(data){
				switch(data.success){
				case 1:
					alert("发送成功");
					break;						
				case 2:
					alert("请输入手机号");
					break;
				}
			}
		});   				
	}
</script>
</head>

<body>
	<div id="containner">
<jsp:include page="hearder.jsp" flush="true">
   	<jsp:param value="" name=""/>
</jsp:include>
		<div id="main">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="80" rowspan="3">
					<input type="hidden" id="uuid" name="uuid" value="${appDto.uuid }"/>
						<div class="info_icon"><img src="${fileserverUrl}${appDto.icon}" alt="" /></div>
					</td>
					<td>
						<div class="info_name">${appDto.name}</div>
					</td>
					<td>
					<!-- 
						<div class="info_down">
							<a href="${pageContext.request.contextPath}/app/dl.action?" class="info_btn_down">直接下载</a>
						</div>
					 -->
					</td>
				</tr>
				<tr>
					<td>
						<div class="info_size">大小： <span id="sizeSpan"></span>${appDto.packFileSize }K</div>
					</td>
					<td width="100">
						<div class="info_star">
							<c:forEach begin="0" end="4">
								<img src="${pageContext.request.contextPath}/img/star-on.png" width="16" height="16" />
							</c:forEach>
						</div>
					</td>
				</tr>
				<tr>
					<td>
					<input type="hidden" name="dis" id="dis" value="${appDto.discriminator}" ></input>
					<input type="hidden" name="packuuid" id="packuuid" value="${appDto.packUuid}"></input>
						<div class="info_time">上架时间：${appDto.packCpUpdateTime}</div>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			
			<div class="info_img">
				<c:forEach items="${appDto.pics}" var="li"	begin="0" end="0">
					<img src="${fileserverUrl}${li}" alt="Fish" />
				</c:forEach>
			</div>
	
			<div class="info_file">系统： 
				<c:if test="${appDto.packOs eq 'os1'}">Android</c:if>
				<c:if test="${appDto.packOs eq 'os2'}">IOS</c:if>
				<c:if test="${appDto.packOs eq 'os3'}">越狱IOS</c:if>
				<c:if test="${appDto.packOs eq 'os4'}">SYMBIAN</c:if>
				<c:if test="${appDto.packOs eq 'os5'}">JAVA</c:if>
				<c:if test="${appDto.packOs eq 'os6'}">WINMOBILE</c:if>
			</div>
			
			<div class="info_file">版本：${appDto.packAppVer }</div>
			<div class="info_file">下载： ${appDto.downCount}</div>
			
			
			<div class="info_file"  id="fxiangqing">简介：
			<font>${appDto.info}</font>
			</div>
				
		<!-- 短信下发   style=display:none-->
			<div id="duanxinxiazai" title="短信下载" style="padding-left: 10px;">
			<c:choose>
				<c:when test="${fn:contains(checkPage,emp.province.provinceID) }">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="20" colspan="2"><b>请输入手机号</b></td>
					</tr>
					<tr>
						<td height="30" align="center">
							<input type="text" name="phoneNoCheck" id="phoneNoCheck" style="width: 160px; height: 20px; line-height: 20px; background: none; border: 1px solid #888888"></input>
						</td>
						<td height="30" align="center">&nbsp;&nbsp;&nbsp;&nbsp;
							<input onfocus="this.blur()" type="button" name="button1" id="button1" value="确定" onclick="dxtjNoCheck()" class="btn_ping" />
						</td>
					</tr>					
				</table>				
				</c:when>
				<c:otherwise>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="20" colspan="2"><b>请输入手机号</b></td>
					</tr>
					<tr>
						<td height="30" align="center">
							<input type="text" name="phone" id="phone" style="width: 160px; height: 20px; line-height: 20px; background: none; border: 1px solid #888888"></input>
						</td>
						<td height="30" align="center">&nbsp;&nbsp;&nbsp;&nbsp;
							<input onfocus="this.blur()" type="button" name="button1" id="button1" value="获取验证码" onclick="phoneCheck()" class="btn_ping" />
						</td>
					</tr>
					<tr>
						<td height="20" colspan="2"><b>请输入验证码</b></td>
					</tr>					
					<tr>
						<td height="30" align="center">
							<input type="text" name="verificationcode" id="verificationcode" style="width: 160px; height: 20px; line-height: 20px; background: none; border: 1px solid #888888"></input>
						</td>
						<td height="30" align="center">&nbsp;&nbsp;&nbsp;&nbsp;
							<input onfocus="this.blur()" type="button" name="button1" id="button1" value="确定" onclick="dxtj()" class="btn_ping" />
						</td>
					</tr>					
				</table>				
				</c:otherwise>
			</c:choose>
			</div>
		</div>
		<div class="fo"></div>
	</div>
</body>
</html>