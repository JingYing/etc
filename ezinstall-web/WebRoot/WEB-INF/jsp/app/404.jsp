<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style>
	body{font-size:12px;}
	.con-404{width:500px; margin-top:100px; margin-left:100px; padding:10px; line-height:30px;}
	.file1{height:30px; font-size:14px; font-weight:bold; padding-left:10px; border-bottom:1px solid #ccc;}
	.file2{padding-left:10px; line-height:20px; margin-top:10px;}
	.file3{padding-left:70px; line-height:20px; margin-top:20px;}
</style>
</head>

<body>
	<div class="con-404">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="50" rowspan="3" align="center" valign="top"><img src="${pageContext.request.contextPath }/img/biaoqing.png" width="38" height="24" /></td>
            <td>
            <div class="file1">我们没有找到您想要的页面....</div>
            </td>
          </tr>
          <tr>
            <td>
            <div class="file2">
             经过研究可能是以下原因：
            	<ul>
                	<li>您输入的地址可能存在键入错误...</li>
                    <li>我们把界面放在家里忘带了...</li>
                    <li>界面离家出走了...</li>
                </ul>
            </div>
            </td>
          </tr>
          <tr>
            <td>
            <div class="file3">您可以选择<a href="${pageContext.request.contextPath }/index/index.action">返回首页</a>或者<a href="javascript:history.go(-1);">返回上一页</a></div>
            </td>
          </tr>
          
        </table>

    </div>
</body>
</html>
