<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="${pageContext.request.contextPath}/css/style.css"
		rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function () { 
			$("[name=checkbox]:checkbox").each(function() {
			//	if($(this).attr("checked")){
					var obj = $(this);
				    $.ajax({
						url:"${pageContext.request.contextPath}/wap/allAppDownJSON.action",
						data:{_:new Date().getTime()},
						type:"post",
						dataType:"json",
						success:function(data){
							if(data.id==0){
								 obj.attr("checked",false); 
							}else{
								if($.inArray(obj.val(),data.id)==-1){
									obj.attr("checked",false); 
								}else{
									obj.attr("checked",true);
								}
							}
						}
					});  					
			//	}
			});
			$("#checkboxall").click(function() { 
				var flag = $(this).attr("checked"); 
				$("[name=checkbox]:checkbox").each(function() { 
					if($(this).attr("checked")){
						$(this).removeAttr('checked'); 
						if(!$(this).attr("checked")){
						    $.ajax({
								url:"${pageContext.request.contextPath}/wap/deldown.action",
								data:{"uuid":$(this).val()},
								type:"post",
								dataType:"json",
								success:function(data){
								}
							});  						
						}							
					}else{
						$(this).attr("checked",flag);
						if($(this).attr("checked")){
						    $.ajax({
								url:"${pageContext.request.contextPath}/wap/buldown.action",
								data:{"uuid":$(this).val()},
								type:"post",
								dataType:"json",
								success:function(data){
								}
							});  						
						}						
					}
				});
			});
			$("[name=checkbox]:checkbox").click(function() { 
				$("[name=checkbox]:checkbox").each(function() { 
					if($(this).attr("checked")){
					    $.ajax({
							url:"${pageContext.request.contextPath}/wap/buldown.action",
							data:{"uuid":$(this).val()},
							type:"post",
							dataType:"json",
							success:function(data){
							}
						});  						
					}
					if(!$(this).attr("checked")){
					    $.ajax({
							url:"${pageContext.request.contextPath}/wap/deldown.action",
							data:{"uuid":$(this).val()},
							type:"post",
							dataType:"json",
							success:function(data){
							}
						});  						
					}					
				});	
			});		
		});
function goPage(a){
	window.location='${pageContext.request.contextPath}/wap/findsoftByName.action?pageNo='+a+'&softName='+encodeURIComponent($("#softName").val());
}
function goPageappDown(a){
//encodeURIComponent()
	window.location='${pageContext.request.contextPath}/wap/allAppDown.action?pageNo='+a+'&softName='+encodeURI($("#softName").val(),"UTF-8");
}
		</script>		
</head>
<body>
<div id="containner">
	<jsp:include page="hearder.jsp" flush="true">
	   	<jsp:param value="" name=""/>
	</jsp:include>
	<div id="main">	
				<div class="title">
					<input type="checkbox"" id="checkboxall" name="checkboxall">全选</input>
				</div>	
		<div class="tui2">
			<div class="tui2_nei">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<div class="tui2_nei">
								<c:forEach items="${pager.list}" var="soft" varStatus="vs">
										<div class="tui2_kid">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td>
														<input type="checkbox"" id="checkbox${vs.count }" name="checkbox" value="${soft.uuid }"/>
													</td>													
													<td width="60" rowspan="2">
														<div class="tui2_kid_icon">
															<a href="${pageContext.request.contextPath}/wap/findsoftInfo.action?packUuid=${soft.uuid}"><img src="${fileserverUrl}${soft.icon}" /></a>
														</div>
													</td>
													<td><div class="tui2_kid_name">${soft.name}</div></td>

												</tr>
											</table>
										</div>
								</c:forEach>
								<div class="clear"></div>
								</div>
							<div class="page">	
									<a href="javascript:void(0);" onclick="goPage(1);">[首页]</a>
									<c:choose>
										<c:when test="${pageNo>1 }">
											<a href="javascript:void(0);" onclick="goPage(${pageNo-1});">[&lt;&lt;上一页]</a>
										</c:when>
										<c:otherwise>
											[&lt;&lt;上一页]
										</c:otherwise>
									</c:choose>								
									<font color="red">${pageNo}</font>
									<c:choose>
										<c:when test="${pageNo<fn:substringBefore((pager.totalCount/20+1), '.') }">
											<a href="javascript:void(0);" onclick="goPage(${pageNo+1});">[下一页&gt;&gt;]</a>
										</c:when>
										<c:otherwise>
											[下一页&gt;&gt;]
										</c:otherwise>
									</c:choose>
									
									<a href="javascript:void(0);" onclick="goPage(${fn:substringBefore((pager.totalCount/20+1), '.')});">[尾页]</a>
									共${fn:substringBefore((pager.totalCount/20+1), ".")}页		
									
									<a href="javascript:void()0;" onclick="goPageappDown(${pageNo});">下载列表</a>		
									<input type="hidden" id="pageNo" value="${pageNo }"/>
									<input type="hidden" id="pageMax" value="${fn:substringBefore((pager.totalCount/20+1), '.')}"/>			
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="tui2">
		</div>
	</div>
	<div class="fo"></div>
  </div>
</div>
</body>
</html>