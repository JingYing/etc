<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>项目管理系统 by www.mycodes.net</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(${pageContext.request.contextPath}/images/left.gif);
}
-->
</style>
<link href="${pageContext.request.contextPath}/css1/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>

<script type="text/javascript" language=JavaScript>
function tupian(idt){
    $("#xiaotu"+idt).attr("src","${pageContext.request.contextPath}/images/ico05.gif");//图片ico04为白色的正方形
	for(var i=1;i<300;i++){
	  if(i!=idt*1){
		if($("#xiaotu"+i).val()!=undefined){$("#xiaotu"+i).attr("src","${pageContext.request.contextPath}/images/ico06.gif");}//图片ico06为蓝色的正方形
	  }
	}
}
function list(idstr){
	if($("#subtree"+idstr).css('display')=="none"){
		for(i=1;i<100;i++){
			if($("#subtree"+i).val()!=undefined){
				$("#subtree"+i).css('display','none');
				$("#img"+i).attr("src","${pageContext.request.contextPath}/images/ico04.gif");
			}
		}
		$("#subtree"+idstr).css('display','block');
		$("#img"+idstr).attr("src","${pageContext.request.contextPath}/images/ico03.gif");
	}else{
		$("#subtree"+idstr).css('display','none');
		$("#img"+idstr).attr("src","${pageContext.request.contextPath}/images/ico04.gif");
	}
}
</script>
</head>
<body>
<table width="170" border="0" cellpadding="0" cellspacing="0" class="left-table01">
  <tr>
    <TD>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td width="207" height="55" background="${pageContext.request.contextPath}/images/nav01.gif">
				<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
				  <tr>
					<td width="25%" rowspan="2"><img src="${pageContext.request.contextPath}/images/ico02.gif" width="35" height="35" /></td>
					<td width="75%" height="22" class="left-font01">您好，<span class="left-font02">${sessionScope.emp.empName}</span></td>
				  </tr>
				  <tr>
					<td height="22" class="left-font01">
						[&nbsp;<a href="${pageContext.request.contextPath}/push/login.action" target="_top" class="left-font01">退出</a>&nbsp;]</td>
				  </tr>
				</table>
			</td>
		  </tr>
		</table>


	  <!-- 公告管理开始 
	<c:if test="${fn:contains(sessionScope.privilegeIdList,'公告管理')}">
	-->
	      <!-- 
      </c:if>
       -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="left-table03">
          <tr>
            <td height="29"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="8%"><img name="img5" id="img5" src="${pageContext.request.contextPath}/images/ico04.gif" width="8" height="11" /></td>
                  <td width="92%"><a href="javascript:" target="mainFrame" class="left-font03" onClick="list('1');">消息管理</a></td>
                </tr>
                
            </table></td>
          </tr>
      </table>

	  <table id="subtree1" style="DISPLAY: none" width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="left-table02">
        <tr>
          <td width="9%" height="20"><img id="xiaotu1" src="${pageContext.request.contextPath}/images/ico06.gif" width="8" height="12" /></td>
          <td width="91%"><a href="${pageContext.request.contextPath}/push/dailyads.action" target="mainFrame" class="left-font03" onClick="tupian('1');">企业推送</a></td>
        </tr>
        <tr>
          <td width="9%" height="20"><img id="xiaotu2" src="${pageContext.request.contextPath}/images/ico06.gif" width="8" height="12" /></td>
          <td width="91%"><a href="${pageContext.request.contextPath}/push/servicemanager.action" target="mainFrame" class="left-font03" onClick="tupian('2');">服务管理</a></td>
        </tr>
        <tr>
          <td width="9%" height="20"><img id="xiaotu3" src="${pageContext.request.contextPath}/images/ico06.gif" width="8" height="12" /></td>
          <td width="91%"><a href="${pageContext.request.contextPath}/push/tagmanager.action" target="mainFrame" class="left-font03" onClick="tupian('3');">标签管理</a></td>
        </tr>
        <tr>
          <td width="9%" height="20"><img id="xiaotu4" src="${pageContext.request.contextPath}/images/ico06.gif" width="8" height="12" /></td>
          <td width="91%"><a href="${pageContext.request.contextPath}/push/querylog.action" target="mainFrame" class="left-font03" onClick="tupian('4');">推送日志</a></td>
        </tr>
        <tr>
          <td width="9%" height="20"><img id="xiaotu5" src="${pageContext.request.contextPath}/images/ico06.gif" width="8" height="12" /></td>
          <td width="91%"><a href="${pageContext.request.contextPath}/push/systempush.action" target="mainFrame" class="left-font03" onClick="tupian('5');">系统推送</a></td>
        </tr>        
      </table>

      
	  </TD>
  </tr>
</table>
</body>
</html>
