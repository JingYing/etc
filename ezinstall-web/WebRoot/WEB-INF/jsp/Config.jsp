<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Config.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.7.min.js"></script>
<script type="text/javascript">
function saveOrUpdate(){
	    		$.ajax({
	    			url:"${pageContext.request.contextPath }/people/configurationSaveOrUpdate.action",
	    			data:"jsonStr=" + $('#jsonStr').val(),
	    			type:"post",
	    			dataType:"json",
	    			success:function(data)	{
	    				switch(data.errorCode){
	    					case 0:
	    						alert("修改成功");
	    						break;
	    					default :
	    						alert("修改失败！");
	    						break;
	    				}
	    			}
	    		});            	
}
</script>
  </head>
  
  <body>
    <table>
    	<tr>
    		<td>
    			<input type="text" id="jsonStr" name="jsonStr" value='${client.jsonStr }' size="100"/>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<input type="button" value="修改" onclick="saveOrUpdate();"/>
    		</td>
    	</tr>
    </table>
  </body>
</html>
