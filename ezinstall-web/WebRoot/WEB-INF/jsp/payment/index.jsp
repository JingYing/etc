<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="description" content="">
		<title>3G加油站_佣金兑换</title>
		<link rel="shortcut icon" href="${pageContext.request.contextPath }/images/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/style.css" />
		<link rel="stylesheet" type="text/css"
			href="${pageContext.request.contextPath }/js/JQueryUI/css/smoothness/jquery-ui-1.10.4.custom.min.css">
		</link>
		<!--  -->
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.7.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-ui-1.8.17.custom.min.js">
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/login.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/common.js"></script>

		<style>
body {
	width: 1024px;
	padding: 0px;
	margin: 0 auto;
	font-size: 12px;
	color: #555;
	font-family: "微软雅黑";
}

.bottom {
	height: 50px;
	line-height: 50px;
	text-align: center;
	width: auto;
	background-image: url(../img/bottom.png);
	background-repeat: repeat-x;
}

.header {
	padding: 30px 48px 32px 48px;
	background-color: #EEEEEE;
}

.marquee {
	font-size: 14px;
	color: #000;
	padding: 0px 120px 0px 120px;
}

.STYLE3 {
	font-size: 13px;
	color: #F00;
}

.STYLE4 {
	font-size: 14px;
	color: #000
}

.STYLE5 {
	color: #000000;
	font-weight: bold;
	font-size: 15px;
}

.STYLE6 {
	font-size: 10px
}

.STYLE7 {
	color: #FF6600;
	font-size: 15px;
}

.STYLE14 {
	color: #FF0000;
	font-size: 25px;
	font-style: italic;
	font-weight: bold;
}

.STYLE24 {
	font-size: 16px;
	font-weight: bold;
	cursor: pointer;
}

.STYLE25 {
	font-size: 21px;
	font-weight: bold;
}

#bj {
	padding-right: 80px;
	padding-top: 15px;
}

#dl {
	background-image: url(../img/jjbj.png);
	background-repeat: no-repeat;
	background-position: center;
	padding: 7px 24px 8px 24px;
	margin: 25px 0px 0px 0px;
}

#gz {
	padding: 10px 20px 10px 2px;
}

.cancel {
	position: absolute;
	left: 200px;
	top: 210px;
	text-align: center;
	border: 0px;
	background-image: url(../img/Ltd-button-hs.png);
	width: 73px;
	height: 35px;
	background-color: #FFFFFF;
	background-position: center;
	font-size: 14px;
}

.cancel:hover {
	border: 0px;
	background-image: url(../img/Ltd-button-back.png);
	width: 73px;
	height: 35px;
	background-position: center;
	color: #fff;
	font-family: "微软雅黑";
	font-size: 14px;
}

.login {
	position: absolute;
	left: 76px;
	top: 210px;
	text-align: center;
	border: 0px;
	background-image: url(../img/Ltd-button-bh.png);
	width: 73px;
	height: 35px;
	background-color: #FFFFFF;
	background-position: center;
	font-size: 14px;
}

.login:hover {
	border: 0px;
	background-image: url(../img/Ltd-button-bule.png);
	width: 73px;
	height: 35px;
	background-position: center;
	color: #fff;
	font-size: 14px;
}

a:link,a:visited,a:active {
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
	color: #FF0000;
}
</style>
<script type="text/javascript">
	$(function() {
//		$("#button").attr("disabled", "disabled");
		$("#loadImage").hide();
	    $( "#loginDialog" ).dialog({
	    	autoOpen: false,
	        resizable: false,
	        height:240,
	        modal: true,
	        buttons: {
	          "确认": function() {
	        	  verify();
	          },
	          "取消": function() {
	            $( this ).dialog( "close" );
	          }
	        }
	      });
		
	});
</script>
	</head>


	<body>
		<table style="margin: 10px auto; width: 1024px;">
			<tr>
				<td>
					<div class="header" align="left">
						<img src="../img/ltlogo.png" alt="logo" width="156" height="43"
							align="absbottom" class="miui_logo" />
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						<span class="STYLE25"> 3G加油站 </span>
						<span class="STYLE14"> 装软件 赚奖金 </span>&nbsp;
						<span class="STYLE25"> 亲，马上有财宝，赶紧来抢咯！ </span>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="center" class="marquee">
						<marquee>
							您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。
						</marquee>
					</div>
				</td>
			</tr>
		</table>
		<table style="margin: 10px auto; width: 1024px; text-align: center;">
			<tr>
				<td>
					<c:if test="${emp == null }">
					<a onClick="javascript:loginon();" class="STYLE24">
					<img src="../img/user.png" width="64" height="64" hspace="8" vspace="8" align="middle">
					<span>工号登录</span>
					</a>
					</c:if>
					<c:if test="${emp != null }">
					<a onClick="javascript:loginout();" class="STYLE24">
					<img src="../img/user.png" width="64" height="64" hspace="8" vspace="8" align="middle">
					<span>退出登录</span>
					</a>
					</c:if>
				</td>
				<td>
					<a href="${pageContext.request.contextPath }/pay/bonus.action" class="STYLE24">
					<img src="../img/Commission.png" width="64"	height="64" hspace="8" vspace="8" align="middle">
					<span>兑换奖金</span>
					</a>
				</td>
				<td>
					<a href="${pageContext.request.contextPath }/pay/query.action" class="STYLE24">
					<img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle">
					<span>奖金查询</span>
					</a>
				</td>
			</tr>
		</table>
		<table width="948" border="0" align="center" cellspacing="0">
			<tr>
				<td colspan="2">
					<div align="left">
						&nbsp;
						<img src="../img/horse.png" width="453" height="410"
							align="absmiddle">
					</div>
				</td>
				<td width="45%" height="auto" valign="top">
					<div id="dl" class="STYLE4">
						<span>&nbsp; &nbsp;<strong>活动主题：</strong> 
						</span>中国联通3G加油站服务推广活动（全国）；
						<br />
						<span>&nbsp; &nbsp;<strong>活动对象：</strong> 
						</span>针对中国联通全国各省范围内的自有营业厅及社会渠道的3G加油站辅导人员。
					</div>
					<div id="gz">
						<p class="STYLE5">
							&nbsp; &nbsp; 奖金兑换说明：
						</p>
						<ul>
							<p class="STYLE4">
								1、支持单笔兑换金额：最低20元，最高5000元；
							</p>
							<p class="STYLE4">
								2、根据支付宝服务规定，需收取支付宝操作的手续费，单笔扣费0.5%，最低1元/次，最高25元/次；
							</p>
							<p class="STYLE4">
								3、当月兑换总金额大于800元时，需扣除个人所得税，扣税规则：
							</p>
							<p class="STYLE3">
								&nbsp;&nbsp;
								<span class="STYLE6"> ■ </span>&nbsp;当月兑换总金额小于/等于800元，不扣个税；
							</p>
							<p class="STYLE3">
								&nbsp;&nbsp;
								<span class="STYLE6"> ■ </span>&nbsp;当月兑换总金额在800-4000元（含4000元）范围内的，扣除该范围内金额的20%；
							</p>
							<p class="STYLE3">
								&nbsp;&nbsp;
								<span class="STYLE6"> ■ </span>&nbsp;当月兑换总金额在4000元以上（不含4000元）的，扣除该范围内金额的40%。
							</p>
						</ul>
					</div>
				</td>
			</tr>
		</table>
		<div class="bottom">版权所有 &copy; 北京桓源创新科技有限公司</div>
		<div id="loginDialog" title = "登录">
			<fieldset>
			<label>
				<span>帐 号： </sapn>
					<input placeholder=' 请输入您的工号 ' id='LoginForm_email' type='text' maxlength='8' onblur='left();' />
					<img id="loadImage" src="../images/loadingSmall.gif" />
			</label>
			<div id='prodiv' style='display: none'>
				<span>省 份： </sapn>
					<select name='pro' id='pro' style='width: 156px'>
						<option value='0'selected="selected">----请选择----</option>
						<option value="1000">全国</option>
						<option value="44">上海</option>
						<option value="49">广东</option>
						<option value="59">陕西</option>
						<option value="53">湖南</option>
						<option value="41">山西</option>
						<option value="39">山东</option>
						<option value="62">甘肃</option>
						<option value="64">新疆</option>
						<option value="50">广西</option>
						<option value="36">内蒙古</option>
					</select>
			</div>
			<div>
				<span>密 码： </span>
				<input placeholder=' 请输入您的密码 ' id='LoginForm_password' type='password' maxlength='16' />
			</div>
			</fieldset>
		</div>
	</body>
</html>