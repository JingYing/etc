﻿<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
<head>
    <base href="<%=basePath%>"/>
    <title>3G加油站</title>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<style type="text/css">
#containner{
	height:100%;
	width:320px;
	background-repeat: no-repeat;
	background-image: url(${pageContext.request.contextPath}/img/bj-1.png);
	background-position: center center;
	padding-bottom:0px;
}
table tr td{
background-repeat: no-repeat;
background-position: center center;
}
.STYLE2 {
	width:200px; height:42px;
	font-family: "微软雅黑";
	font-size: 15px;
	background-repeat: no-repeat;
	background-position: center;
}
.STYLE5 {
	font-family: "微软雅黑";
	font-size: 15px;
}
.STYLE7 {
	font-family: "微软雅黑"; 
	font-size: 15px; 
}
</style>
<style type="text/css"> 
.centerdiv { 
position:absolute; 
top:50%;           
left:50%;        
margin:-100px 0 0 -50px;   
width:300px; 
height:200px;

}  
</style> 

<script type="text/javascript">
$(function() {
	if($("#checkNameflag").val()==0){
		$("#checkName").attr('checked', false);
	}else{
		$("#checkName").attr('checked', true);
	}
});
function denglu(){
	
	    $.ajax({
			url:"${pageContext.request.contextPath}/login/login.action",
			data:{"empNo":$("#empNo").val(),"provId":$("#provId").val(),"pass":$("#pass").val(),"checkName":$("#checkName").val()},
			type:"post",
			dataType:"json",
			beforeSend:function(){
				$('#loading').show();
			},
			success:function(data){
				switch(data.success){
				case 0:
					$('#loading').hide();
					alert("请选择省份");
					break;				
				case 1:
					$('#loading').hide();
					alert("请输入工号和密码");
					break;
				case 2:
					$('#loading').hide();
					alert("工号不存在");
					break;	
				case 3:
					$('#loading').hide();
					alert("密码不正确");
					break;	
				case 4:
					$('#loading').hide();
					alert("系统错误");
					break;	
				case 5:
					window.location='${pageContext.request.contextPath}/wap/findAllSoftInfoByPage.action';
					break;	
				case 6:
					window.location='${pageContext.request.contextPath}/wap/findDesignationAppByPage.action';
					break;						
				}
			}
		});     	
}
</script>
</head>
<body>
<div id="loading" class="centerdiv" style="display:none">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
<tr>
<td align="left" valign="middle">
<div id="centerdiv">
   	<img src="${pageContext.request.contextPath}/images/loading.gif" alt="" />
</div>
</td>
</tr>
</table>
</div>    
<div id="containner">                
<form action="${pageContext.request.contextPath}/login/login.action" id="form" name="form" method="post">
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<table width="300" border="0" align="center" cellpadding="0" cellspacing="2">
	  <tr>
        <td height="10" valign="middle" >
		  <table width="302" border="0" align="center" cellpadding="0" cellspacing="2">
            <tr>
              <td valign="middle"  background="${pageContext.request.contextPath}/img/srk.png">
                  <table width="58%" border="0" align="center" cellpadding="0" cellspacing="3">
                    <tr>
                      <td width="100%" align="left" valign="middle">
                        <p>
                          <label><span class="STYLE5">省分：</span>
                          <select name="provId" id="provId" class="STYLE7">
                            <option value="0"  ${provId eq 0 ?"selected":""}> 请选择</option>
							<option value="44" ${provId eq 44 ?"selected":""}> 上海</option>
							<option value="49" ${provId eq 49 ?"selected":""}> 广东</option>
							<option value="50" ${provId eq 50 ?"selected":""}> 广西</option>
							<option value="53" ${provId eq 53 ?"selected":""}> 湖南</option>
							<option value="39" ${provId eq 39 ?"selected":""}> 山东</option>
							<option value="41" ${provId eq 41 ?"selected":""}> 山西</option>
							<option value="59" ${provId eq 59 ?"selected":""}> 陕西</option>
							<option value="62" ${provId eq 62 ?"selected":""}> 甘肃</option>
							<option value="64" ${provId eq 64 ?"selected":""}> 新疆</option>
							</select>
                            </label>
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" valign="middle">
                        <p>
                          <label>
                            <span class="STYLE5">工号：</span>
                            <input name="empNo" type="text" id="empNo" value="${empNo }" style="width:100px;"/>
                            </label>
                        </p>
                      </td>
                    </tr>

                    <tr>
                      <td align="left" valign="middle">
                        <p>
                          <span class="STYLE5">密码：</span>
                            <label>
                            <input name="pass" type="password" id="pass" value="${pass }" style="width:100px;" />
                            </label>
                        </p>
                      </td>
                    </tr>
                  </table>
             </td>
            </tr>
          </table>
       </td>
      </tr>
      <tr>
        <td height="28" align="left" valign="middle">
	        <label>&nbsp;<input type="checkbox" name="checkName" id="checkName" value="${checkName }"/>
	        <input type="hidden" name="checkNameflag" id="checkNameflag" value="${checkNameflag }"/>
	        </label>
	        <span class="STYLE7">记住密码</span>     
        </td>
 		</tr>
      <tr>
        <td height="19" valign="middle">
         <div align="center">
          	  <p>
            	<label>
		        <input name="button" type="button" class="STYLE2" value="登    录" onClick="denglu();"/>
		        </label>
		      </p>  
		  	  <p>&nbsp;</p>
          </div>
		</td>
	  </tr>
    </table>
</form>
</div>
</body>
</html>
