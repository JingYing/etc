/**
 * 数据字典-数据项管理首页JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */
var zTree;
//根节点
var zNodes = {
    "id":"0",
    "name":"数据字典类别",
    "isParent":true,
    "iconSkin":"diy1"
};
var setting = {
    async : {
        enable : true,
        url :  springUrl+"/dict/dictType_load",
        autoParam : ["id"]
    },
    callback : {
        beforeClick: beforeClick
    },
    data : {
        simpleData : {
            enable : true,
            idKey : 'id',
            idPKey : 'pId',
            rootPid : null
        }
    },
    view: {
        dblClickExpand: false,
        showLine: true,
        selectedMulti: false,
        expandSpeed: "fast"
    }
};
/**
 *单击节点响应函数
 */
function beforeClick(treeId, treeNode) {
    if(treeNode.id != "null"){
        $("#dictTypeId").val(treeNode.id);
        $("#dictDataTable").load(springUrl+"/dict/dictData_list");
    }
    var zTree = jQuery.fn.zTree.getZTreeObj("tree");
    zTree.expandNode(treeNode);
    $('#parentId').val(treeNode.pId);
    return true;
}
/**
 *初始化函数
 */
$(function() {
    jQuery.fn.zTree.init($("#tree"), setting, zNodes);
});



