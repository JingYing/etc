var defaultSetting = {
    callback : defaultCallBackFunction
};
var orgTree;
//根节点 folders 传入参数
var zNodes = {
    "id" : null,
    "name" : "组织机构",
    "isParent" : true,
    "iconSkin" : "diy1"
};
var setting = {
    async : {
        enable : true,
        url : springUrl + "/system/address_getTreeNodeDataJson",
        autoParam : ["id"]
    },
    check: {
        enable : false
    },
    callback : {
        onClick : treeNodeClick
    },
    data : {
        simpleData : {
            enable : true,
            idKey : 'id',
            idPKey : 'pId',
            rootPid : null
        }
    },
    view: {
        dblClickExpand: false,
        showLine : true,
        selectedMulti : false,
        expandSpeed : "fast"
    }
};

/**
 * 初始化操作
 */
var roleDataTable;
var source;//记录是单选还是复选
$(function() {
    source =$("#source").val();
    //加载组织机构树
    jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
    orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");
    var columns;
    if(source=='radio'){
        columns =  [
            {  "mDataProp": "id","sTitle":"","bSortable":false,"bVisible":true,"sWidth": "20px","fnRender" : function(obj) {
                return '<input type="radio" id="radio" name="radio" roleCode="'+obj.aData.roleCode+'" roleName="'+obj.aData.roleName+'"   value="'+obj.aData.id+'"/>';
            }},
            { "mDataProp": "roleCode","sTitle":"角色码","bSortable":true,"bVisible":true,"sDefaultContent":""},
            { "mDataProp": "roleName","sTitle":"角色名称","bSortable":true,"bVisible":true,"sDefaultContent":""}
        ];
    }else{


        columns =  [
            {  "mDataProp": "id","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>","bSortable":false,"bVisible":true,"sWidth": "20px","fnRender" : function(obj) {
                return '<input type="checkbox" id="checkbox" name="checkbox" class="checkbox" roleCode="'+obj.aData.roleCode+'" roleName="'+obj.aData.roleName+'"   value="'+obj.aData.id+'"/>';
            }},
            { "mDataProp": "roleCode","sTitle":"角色码","bSortable":true,"bVisible":true,"sDefaultContent":""},
            { "mDataProp": "roleName","sTitle":"角色名称","bSortable":true,"bVisible":true,"sDefaultContent":""}
        ];
    }

    var  btns = [];


    roleDataTable = $('#roleDataTable').dataTable( {

        "bProcessing": false,
        "bServerSide":true,
        "sPaginationType": "full_numbers",
        "sAjaxSource":springUrl+"/system/role_page",//address_getOrgUserDataJson",
        "sServerMethod": "POST",
        "bStateSave": false,
        "sScrollY":"100%",
        "bScrollCollapse": true,
        "bPaginate":true,
        "bAutoWidth":true,
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "抱歉， 没有找到",
            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有数据",
            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "前页",
                "sNext": "后页",
                "sLast": "尾页"}
        },
        "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span11'i><'span11'p>>",
        "sPaginationType": "bootstrap",
        "bJQueryUI": true,
        "bFilter":false,
        "fnServerData":function ( sSource, aoData, fnCallback) {
//	            	var sNodes = orgTree.getSelectedNodes();
//					var nodeId = "";
//					if(sNodes.length>0){
//						var node = sNodes[0];
//						if(node.props!=null&&node.props.id!=null){
//							nodeId = node.props.id;
//						}else{
//							return false;
//						}
//					}
//					var roleCode=$("input#roleCode").val(); //获取查询用户姓名
            var roleName = $("input#roleName").val();
            if(roleName!=''){
                nodeId = "";
            }
            aoData.push( { "name": "roleName", "value" : roleName },{ "name": "roleType", "value" : 0 });
            jQuery.ajax( {
                type: "POST",
                url : sSource,
                dataType : "json",
                data : aoData,
                success : function(resp) {
                    //Table.currentPage=resp.current_page;
                    fnCallback(resp);
                },
                error : function(data){

                }
            });
        },
        "fnDrawCallback": function( oSettings ){
            /*添加回调方法*/
        },
        "aaSorting":[],//默认排序
        "aoColumns":columns,
        "oTableTools":{
            "aButtons":btns
        }
    } );

    document.getElementById('mainPanel').style.display = '';

    //搜索按钮
    $('#searchRole').click(function() {
        roleDataTable.fnDraw();
    });
    /*$('#roleName').keyup(function() {
     roleDataTable.fnDraw();
     } );*/

    $("#tree_open_arrow").click(function(){
        if($("#tree_org").css("display") == "none"){
            $("#tree_org").css("display","");
            $("#tree_open_arrow img").attr('src','/static/admin/skin/img/tabs_rightarrow.png');
        }else{
            $("#tree_org").css("display","none");
            $("#tree_open_arrow img").attr('src','/static/admin/skin/img/tabs_leftarrow.png');
        }
    });
});




function treeNodeClick(treeId, treeNode) {
    $("input#roleName").val("");
    roleDataTable.fnDraw();
}


function callBack(modal){
    var roleArray = new Array();
    var authorizeType=$('input[name="subjectradio"]:checked').val();
    if(source=='radio'){
        var radio =$('input[name="radio"]:checked');
        if(radio.index()>=0){
            var id =radio.val();
            var roleCode=radio.attr("roleCode");
            var roleName=radio.attr("roleName");
            roleArray.push({id:id,roleCode:roleCode,roleName:roleName,authorizeType:authorizeType});
        }
    }else{
        $('input[name="checkbox"]:checked').each(function(){
            var id = $(this).val();
            var roleCode=$(this).attr("roleCode");
            var roleName=$(this).attr("roleName");
            roleArray.push({id:id,roleCode:roleCode,roleName:roleName,authorizeType:authorizeType});
        });

    }

    if(roleArray.length<=0){
        Dialog.alert("提示", "请选择角色数据记录!");

    }else{
        setRoleInfo(roleArray);
        Dialog.hideModal(modal);
    }
}

function cancel(){
    Dialog.hide();
}
function defaultCallBackFunction(selectRecords){
    Dialog.alert("提示", "请定义【确定】按钮的回调函数!");
    return;
}
function toggleChecks(obj)
{
    $('.checkbox').prop('checked', obj.checked);
}

function getUrlParam(wlocation,name){

    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

    var r = wlocation.substr(1).match(reg);  //匹配目标参数

    if (r!=null) return unescape(r[2]); return null; //返回参数值

}