﻿var table;
$(document).ready(function () {
	var columns = [
	               {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	               {"mData": "entityType", "sTitle": "实体对象", "bSortable": true },
	               {"mData": "entityName", "sTitle": "实体名称" , "bSortable": true},
	               {"mData": "permissionType", "sTitle": "权限类型" , "bSortable": false},
	               {"mData": "expression", "sTitle": "权限表达式" , "bSortable": false},
//	               {"mData": "subjectId", "sTitle": "对象id" , "bSortable": false},
	               {"mData": "id", "sTitle": "操作", "bSortable": false,"mRender": function (data, type, full) {
	                   var render = '<a href="#" class="btn" title="查看详情" onclick="viewDataPermission(\'' + data + '\')" ><img src="' + springUrl + '/static/admin/skin/img/view_obj.gif" alt="查看详情"/></a>&nbsp;&nbsp;'
	                	   + '<a href="#" class="btn" title="编辑" onclick="editDataPermission(\'' + data + '\')" ><img src="' + springUrl + '/static/admin/skin/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;'
	                       + '<a href="#" class="btn " title="删除" onclick="deleteDataPermission(\'' + data + '\')"><img src="' + springUrl + '/static/admin/skin/img/del.png" alt="删除"/></a>';
	                   return render;
	               }}
	           ];

    var tableBtns= [
        {
            "sExtends": "text",
            "sButtonText": "新增",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                var buttons = [
                    {
                        "label": "取消",
                        "class": "btn-success",
                        "callback": function (modal) {
                        	initFullValues();
                        }
                    },
                    {
                        "label": "保存",
                        "class": "btn-success",
                        "callback": function (modal) {
                            if(!validate($("#addDataPermission"))){
                                return false;
                            }else{
                                save();
                            }
                            return false;//不让弹窗自己关闭
                        }
                    }
                ];
                Dialog.openRemote('dataPermission_ add','新增', springUrl+'/system/dataPermission_add', 750, 600, buttons);
            }
        }
    ];

    table = $('#sysdataPermissionList').dataTable( {
        "bProcessing": false,
        "bServerSide":true,
        "sPaginationType": "full_numbers",
        "sAjaxSource":springUrl+"/system/dataPermission_findDictDataPage",
        "sServerMethod": "POST",
        "bAutoWidth": false,
        "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
        "sScrollY":"100%",
        "bScrollCollapse": true,
        "bPaginate":true,
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "抱歉， 没有找到",
            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有数据",
            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "前页",
                "sNext": "后页",
                "sLast": "尾页"}
        },
        //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bJQueryUI": true,
        "bFilter":false,
        "fnServerData":function (sSource, aoData, fnCallback) {
            var dataPermissionName = $("input#dataPermissionName").val();
            var dataPermissionCode = $("input#dataPermissionCode").val();
            aoData.push({ "name": "dataPermissionName", "value": dataPermissionName },{ "name": "dataPermissionCode", "value": dataPermissionCode });

            jQuery.ajax({
                type: "POST",
                async:true,
                url: sSource,
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    fnCallback(resp);
                }
            });
        },
        "fnDrawCallback": function( oSettings ){
        	var that = this;                                     
			this.$('td:first-child', {"filter":"applied"}).each( function (i) {                    
				that.fnUpdate( i+1, this.parentNode, 0, false, false );                
			} );            
        },
        "aaSorting":[],//默认排序
        "aoColumns":columns,
        "oTableTools":{
            "aButtons":tableBtns
        }
    } );
//        table.rowReordering();
});
//新增
function save(){
    var entityType = $('#entityType').val();
    var entityName = $("#entityName").val();
    var subjectType = "";
    var subjectId = "";
    var permissionType = $('#permissionType').val();
    var expression = $("#expression").val();

    if(permissionType==""||expression==""){
        Dialog.alert("提示","请将必填项填写完整");
        return false;
    }

    $('#info input[type="hidden"]').each(function(){
        subjectType+=$(this).attr("valueType")+";";
        var ids = $(this).val();
        subjectId+=ids.substr(0,ids.length-1)+";";
    });

    if(subjectId!=""){
         subjectType =subjectType.substr(0,subjectType.length-1);
         subjectId=subjectId.substr(0,subjectId.length-1);
    }else{
        Dialog.alert("提示","请选择主体对象");
        return false;
    }
    //新增数据项
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            entityType:entityType,
            entityName:entityName,
            subjectType:subjectType,
            subjectIds:subjectId,
            permissionType:permissionType,
            expression:expression
        },
        url: springUrl+"/system/dataPermission_save",
        success:function (data) {
            Dialog.hide();
            Dialog.alert("提示","新建数据项成功");
            $("#sysdataPermission").load(springUrl+"/system/dataPermission_list");
        },
        error: function (data) {
            if(data.responseText == null){
                Dialog.alert("失败","新建数据项失败");
            }else{
                Dialog.alert("失败",data.responseText);
            }

        }
    });
}
function viewDataPermission(id){
	var buttons = [
	               {
	                   "label": "关闭",
	                   "class": "btn-success",
	                   "callback": function () {
	                   }
	               }
	           ];
	           Dialog.openRemote('dataPermission_view','查看详细', springUrl+'/system/dataPermission_view?id=' + id, 750, 600, buttons);
}

function editDataPermission(id) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            	initFullValues();
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function (modal) {
                if(!validate($("#addDataPermission"))){
                    return false;
                }else{
                    modify(modal);
                }
                return false;
            }
        }
    ];
    Dialog.openRemote('dataPermission_update','编辑操作', springUrl+'/system/dataPermission_update?id=' + id, 750, 600, buttons);
}

function deleteDataPermission(id) {
    Dialog.confirm("提示", "确定要删除该条记录吗?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                cache: false,
                url: springUrl+"/system/dataPermission_delete",
                data: {
                    id: id
                },
                success: function (data) {
                	if (data.operator == false)
                        Dialog.alert("失败！",data.message);
                    else{
                        Dialog.alert("成功！",data.message);
                    }
                	table.fnDraw();
                },
                error: function (msg) {

                }
            });
        }
    });
}