/**
 * Created with IntelliJ IDEA.
 * Desctription:
 * User: yujiahe
 * Date: 13-10-8
 * Time: 下午6:04
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function(){//页面初始化设置
        bindValidate($("#addOperation"));// 绑定验证
    });
    function save(modal) {
        jQuery.ajax({
            type: "POST",
            cache: false,
            url: springUrl+"/system/operation_save",
            data: {
                operationCode: $('#operationCode').val(),
                operationName: $('#operationName').val(),
                description: $('#description').val()
            },
            success: function (msg) {
                Dialog.alert("提示信息","添加成功！");
				Dialog.hideModal(modal);
                table.fnDraw();
            },
            error: function (msg) {

            }
        });
}