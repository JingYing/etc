//############################################################
//### 调用说明，在某个vm中引用以下代码 : 
//###	<script type="text/javascript" src="#springUrl('')/js/system/address_inner.js"></script>
//###  部门选择 : <input type='text' size=60 id='org'><input type='button' value='选择部门' onclick='selectOrgnaztion();return false;'>
//###  <br />
//###  组织用户选择 : <input type='text' size=60 id='user'><input type='button' value='选择用户' onclick='selectSysUser();return false;'>
//###
//############################################################
//选择组织
function selectOrgnaztion(){
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function (modal) {
                callBack(modal);
                return false;
            }
        }
    ];
    Dialog.openRemote('org','选择部门',springUrl+'/system/address_getOrgDept?isMulti=true',400,600,buttons);
}

//选择用户
function selectSysUser(){
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function (modal) {
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function (modal) {
                callBack(modal);
                return false;
            }
        }
    ];
    Dialog.openRemote('user','选择用户',springUrl+'/system/address_getOrgUser',600,600,buttons);
}


//岗位查询
function selectSysPosition(){
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function (modal) {
                callBack(modal);
                return false;
            }
        }
    ];
    Dialog.openRemote('position','选择岗位',springUrl+'/system/address_getPosition',800,600,buttons);
}

//角色查询
function selectSysRole(){
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function (modal) {
                callBack(modal);
                return false;
            }
        }
    ];
    Dialog.openRemote('role','选择角色',springUrl+'/system/address_getRole',600,600,buttons);
}

//工作组查询
function selectSysWorkGroup(){
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function (modal) {
                callBack(modal);
                return false;
            }
        }
    ];
    Dialog.openRemote('workGroup','选择工作组',springUrl+'/system/address_getWorkGroup',600,600,buttons);
}

//权限表达式查询
function selectSysExpression(){
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function (modal) {
                callBack(modal);
                return false;
            }
        }
    ];
    Dialog.openRemote('user','选择权限表达式',springUrl+'/system/address_getExpression',700,600,buttons);
}
var oldUserArray;
var oldPositionArray;
var oldRoleArray;
var oldWorkGroupArray;
var oldAuthExpressionArray;
var oldOrgArray;


//确定后回填用户
function setUserInfo(userArray) {
    oldUserArray = arrayReconstruction(oldUserArray,userArray);
    var tabIndex = $("#info").find("#sysusertab").index();
    var html ="";
    var strIds='';
    if(tabIndex>=0){
        $("#sysusertab tr:eq(1)").nextAll().remove();
        $("#sysusertab").show();
        $("#info").find("#sysusertxt").remove();
        for(var i=0;i<oldUserArray.length;i++) {
            var type = oldUserArray[i].authorizeType;
            html+=' <tr><td >'+oldUserArray[i].userName+'</td><td >'+oldUserArray[i].realName+'</td><td >'+oldUserArray[i].orgName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysuser\',\''+oldUserArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldUserArray[i].id+"&"+type+",";
        }
        $("#sysusertab") .append(html);
        $("#sysusertab").after('<input type="hidden" value="'+strIds+'" id="sysusertxt" valueType="User"/>')
    }else{
        html =' <table id="sysusertab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
        html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="5"  style="text-align: center"> 用户</td></tr>';
        html+=' <tr ><td >ERP</td><td >姓名</td><td >部门</td><td>授权状态</td><td>操作</td></tr>';
        for(var i=0;i<oldUserArray.length;i++) {
            var type = oldUserArray[i].authorizeType;
            html+=' <tr><td >'+oldUserArray[i].userName+'</td><td >'+oldUserArray[i].realName+'</td><td >'+oldUserArray[i].orgName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysuser\',\''+oldUserArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldUserArray[i].id+"&"+type+",";
        }
        html+='</table>';
        html+='<input type="hidden" value="'+strIds+'" id="sysusertxt" valueType="User"/>';
        $("#info").append(html);
    }

}

//确定后回填岗位
function setPositionInfo(positionArray) {
    oldPositionArray = arrayReconstruction(oldPositionArray,positionArray);
    var tabIndex = $("#info").find("#syspositiontab").index();
    var strIds='';
    var html ='';
    if(tabIndex>=0){
        $("#syspositiontab tr:eq(1)").nextAll().remove();
        $("#syspositiontab").show();
        $("#info").find("#syspositiontxt").remove();
        for(var i=0;i<oldPositionArray.length;i++) {
            var type = oldPositionArray[i].authorizeType;
            html+=' <tr><td >'+oldPositionArray[i].positionCode+'</td><td >'+oldPositionArray[i].positionName+'</td><td >'+oldPositionArray[i].parentName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysposition\',\''+oldPositionArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldPositionArray[i].id+"&"+type+",";
        }
        $("#syspositiontab").append(html);
        $("#syspositiontab").after('<input type="hidden" value="'+strIds+'" id="syspositiontxt" valueType="Position"/>');
    }else{
        html =' <table id="syspositiontab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
        html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="5"  style="text-align: center"> 岗位</td></tr>';
        html+=' <tr ><td >岗位编号</td><td >岗位名称</td><td >父岗位名称</td><td>授权状态</td><td>操作</td></tr>';
        for(var i=0;i<oldPositionArray.length;i++) {
            var type = oldPositionArray[i].authorizeType;
            html+=' <tr><td >'+oldPositionArray[i].positionCode+'</td><td >'+oldPositionArray[i].positionName+'</td><td >'+oldPositionArray[i].parentName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysposition\',\''+oldPositionArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldPositionArray[i].id+"&"+type+",";
        }
        html+='</table>';
        html+='<input type="hidden" value="'+strIds+'" id="syspositiontxt" valueType="Position"/>';
        $("#info").append(html);
    }
}

//确定后回填角色
function setRoleInfo(roleArray){
    oldRoleArray = arrayReconstruction(oldRoleArray,roleArray);
    var tabIndex = $("#info").find("#sysroletab").index();
    var strIds='';
    var html ='';
    if(tabIndex>=0){
        $("#sysroletab tr:eq(1)").nextAll().remove();
        $("#sysroletab").show();
        $("#info").find("#sysroletxt").remove();
        for(var i=0;i<oldRoleArray.length;i++) {
            var type = oldRoleArray[i].authorizeType;
            html+=' <tr><td >'+oldRoleArray[i].roleCode+'</td><td >'+oldRoleArray[i].roleName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysrole\',\''+oldRoleArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldRoleArray[i].id+"&"+type+",";
        }
        $("#sysroletab").append(html);
        $("#sysroletab").after('<input type="hidden" value="'+strIds+'" id="sysroletxt" valueType="Role"/>');
    }else{
        html =' <table id="sysroletab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width:93%;margin-left: 30px;">';
        html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="4"  style="text-align: center"> 角色</td></tr>';
        html+=' <tr ><td >角色码</td><td >角色名称</td><td>授权状态</td><td>操作</td></tr>';
        for(var i=0;i<oldRoleArray.length;i++) {
            var type = oldRoleArray[i].authorizeType;
            html+=' <tr><td >'+oldRoleArray[i].roleCode+'</td><td >'+oldRoleArray[i].roleName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysrole\',\''+oldRoleArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldRoleArray[i].id+"&"+type+",";
        }
        html+='</table>';
        html+='<input type="hidden" value="'+strIds+'" id="sysroletxt" valueType="Role"/>';
        $("#info").append(html);
    }
}

//确定后回填工作组
function setWorkGroupInfo(roleArray){
    oldWorkGroupArray = arrayReconstruction(oldWorkGroupArray,roleArray);
    var tabIndex = $("#info").find("#sysworkgrouptab").index();
    var strIds='';
    var html ='';
    if(tabIndex>=0){
        $("#info").find("#sysworkgrouptxt").remove();
        $("#sysworkgrouptab tr:eq(1)").nextAll().remove();
        $("#sysworkgrouptab").show();
        for(var i=0;i<oldWorkGroupArray.length;i++) {
            var type = oldWorkGroupArray[i].authorizeType;
            html+=' <tr><td >'+oldWorkGroupArray[i].roleCode+'</td><td >'+oldWorkGroupArray[i].roleName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysworkgroup\',\''+oldWorkGroupArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldWorkGroupArray[i].id+"&"+type+",";
        }
        $("#sysworkgrouptab").append(html);
        $("#sysworkgrouptab").after('<input type="hidden" value="'+strIds+'" id="sysworkgrouptxt" valueType="Group"/>');
    }else{
        html =' <table id="sysworkgrouptab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
        html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="4"  style="text-align: center"> 工作组</td></tr>';
        html+=' <tr ><td >工作组码</td><td >工作组名称</td><td>授权状态</td><td>操作</td></tr>';
        for(var i=0;i<oldWorkGroupArray.length;i++) {
            var type = oldWorkGroupArray[i].authorizeType;
            html+=' <tr><td >'+oldWorkGroupArray[i].roleCode+'</td><td >'+oldWorkGroupArray[i].roleName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysworkgroup\',\''+oldWorkGroupArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldWorkGroupArray[i].id+"&"+type+",";
        }
        html+='</table>';
        html+='<input type="hidden" value="'+strIds+'" id="sysworkgrouptxt" valueType="Group"/>';
        $("#info").append(html);
    }

}
//确定后回填权限表达式
function setAuthExpressionInfo(authArray){
    oldAuthExpressionArray = arrayReconstruction(oldAuthExpressionArray,authArray);
    var tabIndex = $("#info").find("#sysauthexpressiontab").index();
    var strIds='';
    var html ='';
    if(tabIndex>=0){
        $("#info").find("#sysauthexpressiontxt").remove();
        $("#sysauthexpressiontab tr:eq(1)").nextAll().remove();
        $("#sysauthexpressiontab").show();
        for(var i=0;i<oldAuthExpressionArray.length;i++) {
            var type = oldAuthExpressionArray[i].authorizeType;
            html+=' <tr><td >'+oldAuthExpressionArray[i].authName+'</td><td >'+oldAuthExpressionArray[i].mode+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysauthexpression\',\''+oldAuthExpressionArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldAuthExpressionArray[i].id+"&"+type+",";
        }
        $("#sysauthexpressiontab").append(html);
        $("#sysauthexpressiontab").after('<input type="hidden" value="'+strIds+'" id="sysauthexpressiontxt" valueType="AuthExpression"/>');

    }else{
        html =' <table id="sysauthexpressiontab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
        html+='<tr  style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="4"  style="text-align: center"> 权限表达式</td></tr>';
        html+=' <tr ><td >表达式名称</td><td >表达式</td><td>授权状态</td><td>操作</td></tr>';
        for(var i=0;i<oldAuthExpressionArray.length;i++) {
            var type = oldAuthExpressionArray[i].authorizeType;
            html+=' <tr><td >'+oldAuthExpressionArray[i].authName+'</td><td >'+oldAuthExpressionArray[i].mode+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysauthexpression\',\''+oldAuthExpressionArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldAuthExpressionArray[i].id+"&"+type+",";
        }
        html+='</table>';
        html+='<input type="hidden" value="'+strIds+'" id="sysauthexpressiontxt" valueType="AuthExpression"/>';
        $("#info").append(html);
    }
}



//确定后回填组织机构
function setOrganizationInfo(orgArray){
    oldOrgArray = arrayReconstruction(oldOrgArray,orgArray);
    var tabIndex = $("#info").find("#sysorganizationtab").index();
    var strIds='';
    var html ='';
    if(tabIndex>=0){
        $("#info").find("#sysorganizationtxt").remove();
        $("#sysorganizationtab tr:eq(1)").nextAll().remove();
        $("#sysorganizationtab").show();
        for(var i=0;i<oldOrgArray.length;i++) {
            var type = oldOrgArray[i].authorizeType;
            html+=' <tr><td >'+oldOrgArray[i].orgCode+'</td><td >'+oldOrgArray[i].orgName+'</td><td >'+oldOrgArray[i].fullName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysorganization\',\''+oldOrgArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldOrgArray[i].id+"&"+type+",";
        }
        $("#sysorganizationtab").append(html);
        $("#sysorganizationtab").after('<input type="hidden" value="'+strIds+'" id="sysorganizationtxt" valueType="Organization"/>');

    }else{
        html =' <table id="sysorganizationtab" class="table table-striped table-bordered display dataTable " style="position:relative;float:left;clear: none; width: 93%;margin-left: 30px;">';
        html+='<tr style="font-family: \'微软雅黑\'; font-size: 15px; font-weight: bold;"><td colspan="5"  style="text-align: center"> 组织机构</td></tr>';
        html+=' <tr ><td >组织机构码</td><td >组织机构名称</td><td>组织机构全名</td><td>授权状态</td><td>操作</td></tr>';
        for(var i=0;i<oldOrgArray.length;i++) {
            var type = oldOrgArray[i].authorizeType;
            html+=' <tr><td >'+oldOrgArray[i].orgCode+'</td><td >'+oldOrgArray[i].orgName+'</td><td >'+oldOrgArray[i].fullName+'</td><td >'+(type==1?'允许':'禁止')+'</td>';
            html+='<td><a onclick="delTableTr(\'#sysorganization\',\''+oldOrgArray[i].id+'&'+type+'\',this)" title="删除" class="btn " href="#"><img alt="删除" src="/static/admin/skin/img/del.png"></a></td></tr>';
            strIds+=oldOrgArray[i].id+"&"+type+",";
        }
        html+='</table>';
        html+='<input type="hidden" value="'+strIds+'" id="sysorganizationtxt" valueType="Organization"/>';
        $("#info").append(html);
    }

}


///修改详细显示主体对象的内容
function tableInfo(json){
    var userArray = new Array();
    var positionArray = new Array();
    var roleArray = new Array();
    var workGroupArray = new Array();
    var authArray = new Array();
    var orgArray = new Array();
    if(json != null){
        var labelId ='';
        var id;
        var subjectCode;
        var subjectRualName;
        var subjectParent;
        var authorizeType;

        for(var i=0;i<json.length;i++){
            labelId = json[i].subjectType;
            id=json[i].subjectId;
            subjectCode =json[i].subjectCode ;
            subjectRualName=json[i].subjectRualName;
            subjectParent=json[i].subjectParent;
            authorizeType=json[i].authorizeType;
            if(labelId=='User'){
                userArray.push({id:id,userName:subjectCode,realName:subjectRualName,orgName:subjectParent,authorizeType:authorizeType});
            }else if(labelId=='Position'){
                positionArray.push({id:id,positionCode:subjectCode,positionName:subjectRualName,parentName:subjectParent,authorizeType:authorizeType});
            }else if(labelId=='Role'){
                roleArray.push({id:id,roleCode:subjectCode,roleName:subjectRualName,authorizeType:authorizeType});
            }else if(labelId=='Group'){
                workGroupArray.push({id:id,roleCode:subjectCode,roleName:subjectRualName,authorizeType:authorizeType});
            }else if(labelId=='AuthExpression'){
                authArray.push({id:id,authName:subjectCode,mode:subjectRualName,authorizeType:authorizeType});
            }else if(labelId=='Organization'){
                orgArray.push({id:id,orgCode:subjectCode,orgName:subjectRualName,fullName:subjectParent,authorizeType:authorizeType});
            }
        }

        if(userArray.length>0){
            setUserInfo(userArray);
        }
        if(positionArray.length>0){
            setPositionInfo(positionArray);
        }
        if(roleArray.length>0){
            setRoleInfo(roleArray);
        }
        if(workGroupArray.length>0){
            setWorkGroupInfo(workGroupArray);
        }
        if(authArray.length>0){
            setAuthExpressionInfo(authArray);
        }
        if(orgArray.length>0){
            setOrganizationInfo(orgArray);
        }

    }
}


function getEntityName(obj){
    var entityCode = $(obj).val();
    if(entityCode==null || entityCode==''){
        $("#entityName").val("");
        return false;
    }
    $("#entityName").val($(obj).find("option:selected").text());
}


function delTableTr(labelId,dataIdOrSign,tro){
    var dataId =(dataIdOrSign.split("&"))[0];
    var strIds = $(labelId+"txt").val();
    $(tro).parent().parent().remove();
    strIds = strIds.replace(dataIdOrSign+",","");
    if(strIds == ""){
        $(labelId+"tab").hide();
        //$(labelId+"tab").remove();
        $(labelId+"txt").remove();
    }else{
        $(labelId+"txt").val(strIds);
    }

    if(labelId=='#sysuser'){
        oldUserArray = remove(oldUserArray,dataId);
    }else if(labelId=='#sysposition'){
        oldPositionArray =remove(oldPositionArray,dataId);
    }else if(labelId=='#sysrole'){
        oldRoleArray =remove(oldRoleArray,dataId);
    }else if(labelId=='#sysworkgroup'){
        oldWorkGroupArray =remove(oldWorkGroupArray,dataId);
    }else if(labelId=='#sysauthexpression'){
        oldAuthExpressionArray =remove(oldAuthExpressionArray,dataId);
    }else if(labelId=='#sysorganization'){
        oldOrgArray = remove(oldOrgArray,dataId);
    }


}
function arrayReconstruction(oldArray,newArray){
    if(oldArray!=undefined){//&&oldArray.length>0
        for(var i=0;i<newArray.length;i++) {
            var index = indexOf(oldArray,newArray[i].id);
            if(index==-1){
                oldArray.push(newArray[i]);
            }else{//处理同一用户的不同状态
                if(oldArray[index].authorizeType!=newArray[i].authorizeType)
                    oldArray[index].authorizeType=newArray[i].authorizeType;
            }
        }
    }

    if(oldArray==undefined){
        oldArray = newArray;
    }
    return oldArray;
}


function indexOf(arr,val) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].id == val) return i;
    }
    return -1;
}
function remove(arr,val) {
    var index = indexOf(arr,val);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}

function initFullValues(){
    oldUserArray=null;
    oldPositionArray=null;
    oldRoleArray=null;
    oldWorkGroupArray=null;
    oldAuthExpressionArray=null;
    oldOrgArray=null;
}


$(".select_box").click(function(event){
    event.stopPropagation();
    $(this).find(".option").toggle();
    $(this).parent().siblings().find(".option").hide();
});
$(document).click(function(event){
    var eo=$(event.target);
    if($(".select_box").is(":visible") && eo.attr("class")!="option" && !eo.parent(".option").length)
        $('.option').hide();
});


function subjectChange(o){
    var value=$(o).text();
    $(o).parent().siblings(".select_txt").text(value);
    var labelId = $(o).attr("value");
    if(labelId=='User'){
        selectSysUser();
    }else if(labelId=='Position'){
        selectSysPosition();
    }else if(labelId=='Group'){
        selectSysWorkGroup();
    }else if(labelId=='Organization'){
        selectOrgnaztion();
    }else if (labelId=='AuthExpression'){
        selectSysExpression();
    }else if(labelId=='Role'){
        selectSysRole();
    }
}