var table;
var tableData;
var selectedIds;

//清空添加面板的数据
function clearAddPane(){
	setAuthExpressionId("");
	setAuthExpressionName("");
	setOrganization("","不限","不限");
    setPositionInfo([{id:"",positionName:"不限"}]);
    setUserInfo([{id:"",realName:"任何人"}]);
	setLevel("","","");
	setRole("");
	setGroup("");
}
//获取页面基础组件的信息（角色列表，用户组列表，职级列表）
function getBaseControlInfo(){
	jQuery.ajax( {
        type: "get",
        url:springUrl+"/system/base_info",
        dataType: "json",
        success: function(result) {
        	$.each(result.levels_1,function(i,m){
        		var option='<option value="'+m.levelCode+'">'+m.levelCode+'</option>';
        		$("select[name='levelUp']").append(option);
        	});
        	$.each(result.levels_2,function(i,m){
        		var option='<option value="'+m.levelCode+'">'+m.levelCode+'</option>';
        		$("select[name='levelDown']").append(option);
        	});
        	$.each(result.roles,function(i,m){
        		var option='<option value="'+m.id+'">'+m.roleName+'</option>';
        		$("select[name='roleId']").append(option);
        	});
        	$.each(result.groups,function(i,m){
        		var option='<option value="'+m.id+'">'+m.roleName+'</option>';
        		$("select[name='groupId']").append(option);
        	});
        }
	});
}
//解释表达式
function describeExpressMap(expression){
	var nameVlaue;
	$.each(tableData.names,function(i,n){
		if(n.id==expression.id){
			nameVlaue=n;
			return false;
		}
	});
	
	var str_buf="";
	str_buf+="+[部门："+(nameVlaue.organizationId==null?"不限":nameVlaue.organizationId)+"]";
	str_buf+="+[职位："+(nameVlaue.positionId==null?"不限":nameVlaue.positionId)+"]";
	//levelAbove ,levelUp==levelDown 值为一致的
	if(expression.levelAbove!=null&&expression.levelAbove!=""){
		str_buf+="+[职级："+nameVlaue.levelAbove+"以上]";
	}else{
		str_buf+="+[职级："+(nameVlaue.levelUp==null?"不限":nameVlaue.levelUp)+"]";
	}
	str_buf+="+[用户："+(nameVlaue.userId==null?"任何人":nameVlaue.userId)+"]";
	str_buf+="+[角色："+(nameVlaue.roleId==null?"不限":nameVlaue.roleId)+"]";
	str_buf+="+[用户组："+(nameVlaue.groupId==null?"不限":nameVlaue.groupId)+"]";
	str_buf=str_buf.substring(1);
	return str_buf;
}
//获取要修改操作数据的值
function getExpression4Edit(id){
	var expression;
	$.each(tableData.aaData,function(i,n){
		if(n.id==id){
			expression=n;
			return false;
		}
	});
	var nameVlaue; //权限表达式对应的中文名称
	$.each(tableData.names,function(i,n){
		if(n.id==expression.id){
			nameVlaue=n;
			return false;
		}
	});
	nameVlaue.organizationId=nameVlaue.organizationId==null?"不限":nameVlaue.organizationId;
	nameVlaue.positionId=nameVlaue.positionId==null?"不限":nameVlaue.positionId;
	nameVlaue.userId=nameVlaue.userId==null?"任何人":nameVlaue.userId;
	
	setAuthExpressionId(expression.id);
	setAuthExpressionName(expression.authExpressionName);
	setOrganization(expression.organizationId,nameVlaue.organizationId,nameVlaue.organizationId);
    setPositionInfo([{id:expression.positionId,positionName:nameVlaue.positionName}]);
    setUserInfo([{id:expression.userId,realName:nameVlaue.userName}]);
	setLevel(expression.levelUp,expression.levelDown,expression.levelAbove);
	setRole(expression.roleId);
	setGroup(expression.groupId);
	$("#addPane").show("fast");
}
//赋值权限表达式的ID
function setAuthExpressionId(id){
	$("input[name='id']").val(id);
}
//赋值到权限表达式的名称
function setAuthExpressionName(authExpressionName){
	$("input[name='authExpressionName']").val(authExpressionName);
}
//从新弹窗赋值到"部门"
function setOrganization(orgId,orgName,orgFullname){
	$("input[name='organizationId']").val(orgId);
	$("#organization_name").text(orgName);
	$("#organization_name").attr("title",orgFullname);  
}
////从新弹窗赋值到"职位"
//function setPostion(posId,posName,posFullname){
//	$("input[name='positionId']").val(posId);
//	$("#postion_name").text(posName);
//	$("#postion_name").attr("title",posFullname);
//}

//从新弹窗赋值到"职位"
function setPositionInfo(positionArray){
    $("input[name='positionId']").val(positionArray[0].id);
    $("#postion_name").text(positionArray[0].positionName);
    $("#postion_name").attr("title",positionArray[0].positionName);
}

//从新弹窗赋值到"员工"setUserInfo
function setUserInfo(userArray){
    $("input[name='userId']").val(userArray[0].id);
    $("#user_name").text(userArray[0].realName);
}
//设置职级
function setLevel(levelUp,levelDown,levelAbove){
	if(levelAbove!=null&&levelAbove!=""){
		$("input[name='levelAbove']").attr("checked",true);
		levelUp=levelAbove;
	}else{
		$("input[name='levelAbove']").attr("checked",false);
	}
	if(levelUp==null||levelUp==""){
		$("select[name='levelUp']").find("option[value='']").attr("selected",true);
		$("select[name='levelDown']").find("option[value='']").attr("selected",true);
	}else{
		var level=levelUp.split("-");
		$("select[name='levelUp']").find("option[value='"+level[0]+"']").attr("selected",true);
		$("select[name='levelDown']").find("option[value='"+level[1]+"']").attr("selected",true);
	}
	
}
//设置角色
function setRole(roleId){
	if(roleId==null) roleId='';
	$("select[name='roleId']").find("option[value='"+roleId+"']").attr("selected",true);
}
//设置管理组
function setGroup(groupId){
	if(groupId==null) groupId='';
	$("select[name='groupId']").find("option[value='"+groupId+"']").attr("selected",true);
}
//删除一条表达式
function delAuthExpressionRecord(ids){
	var data={
			"expressionIds":ids
	};
	Dialog.del(springUrl+"/system/authExpression_delete",data,function(result){
		if(result!=null&&result.length>0){
			Dialog.alert("删除操作提示","【"+result[0].authExpressionName+"】存在引用无法删除","确定");
		}else{
			Dialog.alert("删除操作提示","删除成功","确定");
			Table.render(table);
		}
	});
}
//增加一条表达式
function addAuthExpressionRecord(e){
	var mode="";
	var id=$("input[name='id']").val();
	var authExpressionName=$("input[name='authExpressionName']").val();
	var organizationId=$("input[name='organizationId']").val();
	var positionId=$("input[name='positionId']").val();
	var levelUp=$("select[name='levelUp']").val();
	var levelDown=$("select[name='levelDown']").val();
	var levelAbove=$("input[name='levelAbove']").attr("checked")=="checked"?1:0;
	var userId=$("input[name='userId']").val();
	var roleId=$("select[name='roleId']").val();
	var groupId=$("select[name='groupId']").val();
	if(levelAbove==1){
		levelAbove=levelUp+"-"+levelDown;
		levelUp=null;
	}else{
		levelAbove=null;
		levelUp=levelUp+"-"+levelDown;
	}
	if(levelUp=="-") levelUp=null;
	if(levelAbove=="-") levelAbove=null;
	
	//表达式的map对象
	var expressionMap={
		"id":id,
		"mode":mode,
		"authExpressionName":authExpressionName,
		"organizationId":organizationId,
		"positionId":positionId,
		"levelUp":levelUp,
		"levelDown":levelUp,
		"levelAbove":levelAbove,
		"userId":userId,
		"roleId":roleId,
		"groupId":groupId
	};
	//封装成一个数组形式传入后台
	var expressions=new Array();
	expressions.push(expressionMap);
	var toJson=JSON.stringify(expressions);
	var data={
			"expressions":toJson
		};
	Dialog.post(springUrl+"/system/authExpression_save",data,function(result){
		if(result==null){
			return false;
		}
		if(result.operator==true){
			Table.render(table);
			$("#addPane").hide("fast");
		}
		Dialog.alert("操作提示",result.message);
	});
	
}
//显示表达式对应的用户串
function showUsers(authId){
	var map = getUsers(authId);
//	$("#disp").html(map.usersName);	
//	$("#disp").show();
	Dialog.alert("用户信息如下：",map.usersName);
}

//隐藏表达式对应的用户串
function hideUsers(authId){
	$("#disp").hide();
}

//获得表达式对应的用户串Ajax
function getUsers(authId){
	var usersMap ;
    var data={
    		"authId":authId	
    };
	Dialog.post(springUrl+"/system/users_by_expressions_search_str",data,function(result){
		usersMap=result;
	});
	return usersMap;
}

function selectRow(){
	$.each(selectedIds,function(i,n){
		Table.selectRow(table.find("input[name='id'][value='"+n+"']").parent().parent());
	});
}
function selectOk(){
	var ids=Table.getAllSelectedRowIds("hello");
	return ids;
}

function selectOrgnaztion(){
	var btns = [				
				{
					"label": "取消",
					"class": "btn-cancel",
					"callback": function() {}
				},
				{
					"label": "确定",
					"class": "btn-success",
					"callback": function() {						
						var treeNodes = orgTree.getSelectedNodes();
						var treeNode = treeNodes[0];
						if(treeNode.id!=null){
							setOrganization(treeNode.id,treeNode.name,treeNode.props.orgFullname);
						} else {		
					        alert("请选择下级部门！");
						}	
					}
				}
			];

	Dialog.openRemote('选择部门',springUrl+'/system/expression_orgTreeIndex',400,400,btns);
}

function popPosition() {
	var btns = [				
				{
					"label": "取消",
					"class": "btn-cancel",
					"callback": function() {}
				},
				{
					"label": "确定",
					"class": "btn-success",
					"callback": function() {
                        callBack();
                        return false;
					}
				}
			];
    Dialog.openRemote('选择职位',springUrl+'/system/address_getPosition?source=radio',800,600,btns);

}

//
function remoteOrgUserInfo() {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-cancel",
            "callback": function () {
//                cancel();
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function () {
                callBack();
                return false;
            }
        }
    ];
    Dialog.openRemote('选择人员', springUrl + '/system/sysAddress_getOrgUser?source=radio',800, 500, buttons);
}
(function(){	
	//选择部门
	$('#organization_name').click(function() {
		selectOrgnaztion();
    });
	//选择职位
	$('#postion_name').click(function() {
		popPosition();
    });
	//选择员工 
	$('#user_name').click(function() {
		remoteOrgUserInfo();
    });
	
	//判断当前页面是不是在iframe中被调用
	if(byIFrame==1){
		selectedIds=parent.getExpressionId();
	}else{
		if(typeof expressionId == "undefined"){
			expressionId="[]";
		}
		selectedIds=expressionId;
	}
	getBaseControlInfo();
	bindValidate($('#addPane'));
	bindValidate($("#authSearchForm"));
	$("#expression_add_btn").click(function(){
		var validateEl=$('#addPane');
		if(validate2(validateEl)){
			addAuthExpressionRecord();
		}
	});
	$('#search').click(function() {
		if(validate2($("#authSearchForm"))){
			$("#onlySelected").val("0");
			Table.render(table); 
		} 
	} );
	$('#authExpressionName').keyup(function() {
		if(validate2($("#authSearchForm"))){
			$("#onlySelected").val("0");
			Table.render(table); 
		} 
	} );
	
	var options=Table.options;
	options={
			pageUrl:springUrl+"/system/authExpression_page",
			useCheckbox:true,
			defaultSort:[],
			scrollY:"100%",
			sendData:function ( sSource, aoData, fnCallback ) {
				var authExpressionName=$("input#authExpressionName").val();
				if(authExpressionName==null) authExpressionName="";
				authExpressionName=authExpressionName.replace("_","\\_");
				var onlySelected=$("#onlySelected").val();
				var selectedVals="";
				if(onlySelected=="1"&&table!=null){
					selectedVals=JSON.stringify(Table.getAllSelectedRowIds("hello"));
				}
				aoData.push( 
						{ "name": "authExpressionName", "value":authExpressionName },
						{ "name": "selectedVals", "value":selectedVals }
				);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    		tableData=resp;
		                    		Table.currentPage=resp.current_page;
		                            fnCallback(resp);
		                    }
			    });
			},
			columns: [
	            { "mDataProp": "authExpressionName","sTitle":"名称","sWidth":200,"fnRender":function(obj){
	            	return obj.aData.authExpressionName;
	            }},
				{"bSortable":false,"sTitle":"表达式","fnRender":function(obj){
					return describeExpressMap(obj.aData);
				}},
	            { "bSortable":false,"sWidth":100,"sTitle":"操作","fnRender":function(obj){
					var id = obj.aData.id;
					return "<a href='#' onclick='showUsers(\""+id+"\");' class='btn' title='显示用户' ><img src='"+springUrl+"/static/common/img/search.png' alt='显示用户'></a>" +
							"<a href='#' class='btn' title='修改' onclick='getExpression4Edit(\""+id+"\");'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'></a>";
	            }}
	        ],
			btns:[
					{
							    "sExtends":    "text",
							    "sButtonText": "新增",
								"sToolTip": "",
								"fnClick": function ( nButton, oConfig, oFlash ) {
//									top.autoHeight($(document).height()+$("#addPane").outerHeight());
									$("#addPane").show("fast");	
									clearAddPane();
							    }
					},		
					{
							    "sExtends":    "text",
							    "sButtonText": "删除",
								"sToolTip": "",
								"fnClick": function ( nButton, oConfig, oFlash ) {
									var ids=Table.getSelectedRowsIDs(table);
									if(ids.length==0){
										Dialog.alert("消息提示","没有选中项","确定");
									}else{
										delAuthExpressionRecord(JSON.stringify(ids));
									}
							    }
					},
					{
						"sExtends":    "text",
						"sButtonText": "已选/全部",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							var onlySelected=$("#onlySelected").val();
							if(onlySelected=="0"){
								$("#onlySelected").val("1");
							}else{
								$("#onlySelected").val("0");
							}
							Table.render(table);
						}
					}
				],
				callback_fn:function(){
				}
	};
	table=Table.dataTable("hello",options);
	if(selectedIds!=null){
		Table.setSelectedRowsIds("hello",selectedIds);
	}
})();

