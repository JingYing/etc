/*Ztree*/
var orgTree;
//根节点 folders 传入参数
var zNodes ={
	    "id":null,
	    "name":"组织机构",
	    "isParent":true,
	    "iconSkin":"diy1"
};
var setting = {
		async : {
			enable : true,
			url :springUrl+"/system/organization_treeLoad",
			autoParam : ["id"]
		},
		check: {
			enable: false
		},
		 callback : {
	      beforeClick : beforeClick
	  },
		data : {
			simpleData : {
				enable : true,
				idKey : 'id',
				idPKey : 'pId',
				rootPid : null
			}
		},
		view: {
			dblClickExpand: false,
			showLine: true,
			selectedMulti: false,
			expandSpeed: "fast"
		}
};
/**
* 初始化操作
*/
$(function() {
	//加载组织机构树
	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");
});

/**
 *单击节点响应函数
 */
function beforeClick(treeId, treeNode) {
    if (treeNode.id != null) {
        $("#orgid").val(treeNode.props.id);
        if(treeNode.props.id!=""){
        	$("#syspositionTable").load(springUrl+"/system/position_list");
        	return true;
        }
    }
   
}



