var setting = {
    async : {
        enable : true,
        url : getUrl
    },
    check : {
        enable : true,
        nocheckInherit : true
    },
    data : {
        simpleData : {
            enable : true
        }
    },
    view : {
        expandSpeed : "",
        addDiyDom : addDiyDom
    },
    callback: {
        onCheck : onCheck,
        onExpand : expandAll,
        beforeAsync: beforeAsync,
        onAsyncSuccess: onAsyncSuccess,
        onAsyncError: onAsyncError
    }
};

var zNodes = treeNode;

function getUrl(treeId, treeNode) {
    var param = "nodeId=" + treeNode.id + '&level=' + treeNode.level + "&roleId=" + roleId;
    return springUrl + "/system/rolePrivilege_getTreeNodesJson?" + param;
}

var IDMark_A = "_a";
function addDiyDom(treeId, treeNode) {
    if(typeof(treeNode.props) == 'undefined' || typeof(treeNode.props.privileges) == 'undefined'){
        return;
    }
    if(treeNode.isParent){
        return;
    }
    var aObj = $("#" + treeNode.tId + IDMark_A);
    var privileges = treeNode.props.privileges;
    for(var i = 0 ; i < privileges.length ; i++){
        var className = "button chk checkbox_false_full";
        if(privileges[i].rolePrivilege && treeNode.checked){
            className = "button chk checkbox_true_full";
        }
        var editStr = "  <span id=\"node_span_" + treeNode.tId + "_cbspan_" + privileges[i].id + "\" class=\"" + className + "\"  privilegeId=\"" + privileges[i].id + "\"  ></span>" + privileges[i].privilegeName;
        aObj.after(editStr);
        var btn = $("#node_span_"+treeNode.tId+ "_cbspan_" + privileges[i].id );
        if (btn) btn.bind("click", function(){checkbox(this,treeId,treeNode);});
    }
}

function onCheck(e,treeId, treeNode) {
    if(treeNode.checked){//选中
        if(treeNode.isParent){
            var zTree = jQuery.fn.zTree.getZTreeObj("privilegeTree");
            var childNodes = zTree.transformToArray(treeNode);
            for(i = 0; i < childNodes.length; i++) {
                if(!childNodes[i].isParent){
                    eachTreeNodeSave(childNodes[i]);
                }
            }
        } else {
            eachTreeNodeSave(treeNode);
        }
    } else {
        var aObj_spans = $("#" + treeNode.tId + IDMark_A).parent().find('span');
        for(var i = 0 ; i < aObj_spans.length ; i++){
            var aObj_span = aObj_spans[i];
            if('button chk checkbox_true_full' == aObj_span.className){
                aObj_span.className = 'button chk checkbox_false_full';
            }
        }
    }
}
function eachTreeNodeSave(treeNode){
    var aObj_spans = $("#" + treeNode.tId + IDMark_A).parent().find('span');
    for(var i = 0 ; i < aObj_spans.length ; i++){
        var aObj_span = aObj_spans[i];
        if('button chk checkbox_false_full' == aObj_span.className){
            aObj_span.className = 'button chk checkbox_true_full';
        }
    }
}
function checkbox(obj,treeId, treeNode){
    var zTree = $.fn.zTree.getZTreeObj("privilegeTree");

    if(obj.className == 'button chk checkbox_false_full'){
        obj.className = 'button chk checkbox_true_full';

        zTree.checkNode(treeNode, true, true);
    } else {
        obj.className = 'button chk checkbox_false_full';

        var flag = true;
        var aObj_spans = $("#" + treeNode.tId + IDMark_A).parent().find('span');
        for(var i = 0 ; i < aObj_spans.length ; i++){
            var aObj_span = aObj_spans[i];
            var id = aObj_span.id;
            if('button chk checkbox_true_full' == aObj_span.className&&id.indexOf("node_span_"+ treeNode.tId +"_cbspan")>=0){
                flag = false;
                break;
            }
        }

        if(flag){
            zTree.checkNode(treeNode, false, true);
        }
    }
}

/**
 * 授权
 */
function saveAc(model){
    var treeObj = $.fn.zTree.getZTreeObj("privilegeTree");
    var nodes = treeObj.getCheckedNodes(true);
    var treeNodeResourcePrivilegeArray = new Array();
    for ( var i = 0; i < nodes.length; i++) {
        var node = nodes[i];
        if(!node.isParent){
            var aObj_spans = $("#" + node.tId + IDMark_A).parent().find('span');

            var checkedResourceOperationArray = new Array();//树节点对应的选中操作
            for(var j = 0 ; j < aObj_spans.length ; j++){
                var aObj_span = aObj_spans[j];
                var id = aObj_span.id;
                if('button chk checkbox_true_full' == aObj_span.className&&id.indexOf("node_span_"+ node.tId +"_cbspan")>=0){
                    var privilegeId = jQuery(aObj_span).attr('privilegeId');
                    if(typeof(privilegeId) != 'undefined'){
                        var checkedResourceOperation = {
                            privilegeId: privilegeId
                        };
                        checkedResourceOperationArray.push(checkedResourceOperation);
                    }
                }
            }
            var treeNodeResourcePrivilege = {
                resourceId : node.props.id,
                name : node.name,
                code : node.props.code,
                checkedResourceOperationArray : checkedResourceOperationArray
            };
            treeNodeResourcePrivilegeArray.push(treeNodeResourcePrivilege);
        }
    }
    var data = {
        roleId : roleId,
        operationsJson : JSON.stringify(treeNodeResourcePrivilegeArray)
    };
    saveAjax(model,data);
}
function saveAjax(model,data) {
    jQuery.ajax({
        type : "POST",
        cache : false,
        url : springUrl + "/system/rolePrivilege_save",
        data : data,
        success : function(msg) {
            Dialog.alert("提示", msg.message);
            Dialog.hideModal(model);
        },
        error : function(msg) {

        }
    });
}


$(document).ready(function() {
    $.fn.zTree.init($("#privilegeTree"), setting, zNodes);
});

var curStatus = "init", curAsyncCount = 0, asyncForAll = false,
    goAsync = false;
function expandAll() {
    var zTree = $.fn.zTree.getZTreeObj("privilegeTree");
    if(asyncForAll){
        zTree.expandAll(true);
    }else{
        expandNodes(zTree.getNodes());
        if(!goAsync){
            curStatus="";
        }
    }

}
function expandNodes(nodes) {
    if (!nodes) return;
    curStatus="expand";
    var zTree = $.fn.zTree.getZTreeObj("privilegeTree");
    for (var i = 0, l = nodes.length; i < l; i++) {
        zTree.expandNode(nodes[i], true, false, false);
        if (nodes[i].isParent&&nodes[i].zAsync) {
            expandNodes(nodes[i].children);
        }else {
            goAsync=true;
        }
    }
}

function beforeAsync() {
    curAsyncCount++;
}

function onAsyncSuccess(event, treeId, treeNode, msg) {
    curAsyncCount--;
    if (curStatus == "expand") {
        expandNodes(treeNode.children);
    } else if (curStatus == "async") {
        asyncNodes(treeNode.children);
    }

    if (curAsyncCount <= 0) {
        if (curStatus != "init" && curStatus != "") {
            asyncForAll = true;
        }
        curStatus = "";
    }
}

function onAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
    curAsyncCount--;
    if (curAsyncCount <= 0) {
        curStatus = "";
        if (treeNode!=null) asyncForAll = true;
    }
}

function asyncNodes(nodes) {
    if (!nodes) return;
    curStatus = "async";
    var zTree = $.fn.zTree.getZTreeObj("privilegeTree");
    for (var i=0, l=nodes.length; i<l; i++) {
        if (nodes[i].isParent && nodes[i].zAsync) {
            asyncNodes(nodes[i].children);
        } else {
            goAsync = true;
            zTree.reAsyncChildNodes(nodes[i], "refresh", true);
        }
    }
}