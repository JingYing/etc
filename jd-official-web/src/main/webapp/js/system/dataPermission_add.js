/**
 * 数据字典-数据项新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 初始化操作
 */

$(function() {
	bindValidate($('#dataPermissionForm'));
	$.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/dict/dictData_findDictDataList",
        data: {
        	dictTypeCode: "ENTITY_TYPE"//之后改成数据字典中对应的业务对象字典类型编码
        },
        dataType: "json",
        success: function(json){
            var ary = new Array();
            var entity = '<option value="">不限</option>';
            if(json != null){
                for(var i=0;i<json.length;i++){
                	entity +='<option value="'+json[i].dictDataCode+'">'+json[i].dictDataName+'</option>';  
                }
            }
            $("#entityType").html(entity);
        }
    });

    $.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/dict/dictData_findDictDataList",
        data: {
            dictTypeCode: "AUTHORIZE_SUBJECT"//之后改成数据字典中对应的可授权主体对象字典类型编码
        },
        dataType: "json",
        success: function(json){
            var entity = '<a onclick="subjectChange(this)"  value="">不限</a>';
            if(json != null){
                for(var i=0;i<json.length;i++){
                    if(json[i].dictDataCode=='Organization'){
                        entity +='<a onclick="subjectChange(this)" value="'+json[i].dictDataCode+'" >'+json[i].dictDataName+'</a>';
                    }else if(json[i].dictDataCode=='AuthExpression'){
                        entity +='<a onclick="subjectChange(this)"  value="'+json[i].dictDataCode+'" >'+json[i].dictDataName+'</a>';
                    }else if(json[i].dictDataCode=='Group'){
                        entity +='<a onclick="subjectChange(this)"  value="'+json[i].dictDataCode+'" >'+json[i].dictDataName+'</a>';
                    }else{
                        entity +='<a onclick="subjectChange(this)"  value="'+json[i].dictDataCode+'">'+json[i].dictDataName+'</a>';
                    }

                }
            }
            $("#subjectType").html(entity);
        }
    });

});

function subjectChange(o){
    var labelId = $(o).val();
    if(labelId=='User'){
        selectSysUser();
    }else if(labelId=='Position'){
        selectSysPosition();
    }else if(labelId=='Group'){
        selectSysWorkGroup();
    }else if(labelId=='Organization'){
        selectOrgnaztion();
    }else if (labelId=='AuthExpression'){
        selectSysExpression();
    }else if(labelId=='Role'){
        selectSysRole();
    }
}

