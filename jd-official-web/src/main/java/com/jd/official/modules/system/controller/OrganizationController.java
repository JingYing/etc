package com.jd.official.modules.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.official.core.tree.TreeNode;
import com.jd.official.modules.system.model.Organization;
import com.jd.official.modules.system.model.Resource;
import com.jd.official.modules.system.service.OrganizationService;

/**
 * 
 * @author: xulin
 */
@Controller
@RequestMapping(value = "/system")
public class OrganizationController {
	private static final Logger logger = Logger.getLogger(OrganizationController.class);

	@Autowired
	private OrganizationService organizationService;

	/**
	 * 初始化组织架构树
	 * 
	 * @param locale
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/organization_index", method = RequestMethod.GET)
	public ModelAndView index(
			@RequestParam(value = "locale", required = false) Locale locale,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("system/organization_index");
		return mav;
	}
	
	
	  /**
     * 
     * @desc 树数据加载
     * @author WXJ
     * @date 2013-9-2 上午11:24:45
     *
     * @param organization
     * @return 树节点列表
     */
    @RequestMapping(value = "/organization_treeLoad", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<TreeNode> orgTreeLoad(String id){
    	Map mp = new HashMap();
    	mp.put("id", id);
        List<Organization> organizationList = organizationService.findByParentId(mp);
        List<TreeNode> listData= this.getOrgTreeNodes(organizationList);    //转化成Ztree支持的数据格式
        return listData;
    }
    
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-9-2 上午11:30:38
     *
     * @param organizationList
     * @return
     */
    private List<TreeNode> getOrgTreeNodes(List<Organization> organizationList){
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        for(Organization organization : organizationList){
            TreeNode treeNode = new TreeNode();
            treeNode.setId(organization.getId().toString());
            treeNode.setName(organization.getOrganizationName());
            if(organization.getParentId()!=null&&!organization.getParentId().equals("")){
                treeNode.setpId(organization.getParentId().toString());
            }
            treeNode.setIsParent(organization.getIsParent());
            treeNode.setIconSkin(Resource.class.getSimpleName().toLowerCase());
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("id", organization.getId());
            props.put("parentId", organization.getParentId());
            props.put("orgFullname", organization.getOrganizationFullname());
            props.put("orgCode", organization.getId().toString());
            treeNode.setProps(props);
            treeNodes.add(treeNode);
        }
        return treeNodes;
	}
	
	/**
	 * 组织架构查看页面
	 * 
	 * @param organization
	 *  组织架构对象
	 */
	@RequestMapping(value = "/organization_view", method = RequestMethod.GET)
	public ModelAndView folderView(Organization organization) {
		organization = organizationService.get(organization.getId());
		ModelAndView mav = new ModelAndView("system/organization_view");
		mav.addObject("organization", organization);
		return mav;
	}
}
