package com.jd.official.modules.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.tree.TreeNode;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.modules.dict.model.DictType;
import com.jd.official.modules.system.model.Privilege;
import com.jd.official.modules.system.model.Resource;
import com.jd.official.modules.system.service.PrivilegeService;
import com.jd.official.modules.system.service.ResourceService;

/**
 * 
 * @author: xulin
 */
@Controller
@RequestMapping(value = "/system")
public class ResourceController {
	private static final Logger logger = Logger.getLogger(ResourceController.class);

	@Autowired
	private ResourceService resourceService;
	@Autowired
	private PrivilegeService privilegeService;

	/**
	 * 初始化资源树
	 * 
	 * @param locale
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource_index", method = RequestMethod.GET)
	public ModelAndView list(
			@RequestParam(value = "locale", required = false) Locale locale,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("system/resource_index");

		TreeNode rootNode = new TreeNode();
		rootNode.setId(null);
		rootNode.setName("京东-办公自动化");
		rootNode.setIsParent(Boolean.TRUE);
		rootNode.setIconSkin("diy1");

		mav.addObject("applications", JsonUtils.toJsonByGoogle(rootNode));

		return mav;
	}



	/**
	 * 资源数异步加载
	 * 
	 * @param id
	 *            树节点ID
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource_load", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<TreeNode>  resourceLoad(Long id) {
//		try {
//			if (!nodeId.equals("undefined")) {
			List<Resource> resources =  resourceService.findByParentId(id);
            List<TreeNode> listData = this.getTreeNodes(resources);
////			}
//
//		} catch (Exception e1) {
//			logger.error(e1.getMessage(), e1);
//		}
        return listData;
	}


	/**
	 * 资源查看页面
	 * 
	 * @param resource
	 *  资源对象
	 */
	@RequestMapping(value = "/resource_view", method = RequestMethod.GET)
	public ModelAndView folderView(Resource resource) {
		resource = resourceService.get(resource.getId());
		ModelAndView mav = new ModelAndView("system/resource_view");
		mav.addObject("resource", resource);
		return mav;
	}

	/**
	 * 资源增加页面
	 * 
	 * @param id
	 *            资源对象
	 */
	@RequestMapping(value = "/resource_add", method = RequestMethod.GET)
	public ModelAndView resourceAdd(String id) {
		ModelAndView mav = new ModelAndView("system/resource_add");
        Resource resource=null;
        if (("null").equals(id)){
            resource = new Resource();
        } else{
            resource = resourceService.get(Long.parseLong(id));
        }
		mav.addObject("resource", resource);
		return mav;
	}

	/**
	 * 资源修改页面
	 * 
	 * @param id
	 *  资源对象
	 * @return
	 */
	@RequestMapping(value = "/resource_update", method = RequestMethod.GET)
	public ModelAndView resourceUpdate(Long id) {
		Resource resource = resourceService.get(id);
		ModelAndView mav = new ModelAndView("system/resource_update");
		mav.addObject("resource", resource);
		return mav;
	}

	/**
	 * 保存新资源
	 * 
	 * @param resource
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.add, entityName="Resource")
	@RequestMapping(value = "/resource_addSave")
	@ResponseBody
	public String addSave(Resource resource,HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
			resource.setStatus("1");
			resource.setChildrenNum(0);
			resourceService.insertResource(resource);
			Long id = resource.getId();
			request.setAttribute("entityId", id);
			map.put("id", resource.getId());
		return JSONObject.fromObject(map).toString();
	}
	
	@RequestMapping(value="/resource_isExistResourceCodePage",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public boolean isExistResourceCodePage(Resource resource){
    	boolean flag = resourceService.isExistResourceCode(resource);
    	return flag;
    }
	
	@RequestMapping(value="/resource_isExistResourceNamePage",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
	public boolean isExistResourceNamePage(Resource resource){
		boolean flag = resourceService.isExistResourceName(resource);
    	return flag;
	}

	/**
	 * 修改资源
	 * 
	 * @param resource
	 * @return
	 */
	@RecordLog(operationType=OperationTypeValue.update, entityName="Resource")
	@RequestMapping(value = "/resource_updateSave")
	@ResponseBody
	public String updateSave(Resource resource) {
		Map<String, Object> map = new HashMap<String, Object>();
		resource.setStatus("1");
		resourceService.update(resource);
		map.put("check", true);

		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 删除资源
	 * 
	 * @param resource
	 * 资源对象
	 */
	@RecordLog(operationType=OperationTypeValue.delete, entityName="Resource")
	@RequestMapping(value = "/resource_delete", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String resourceDelete(Resource resource) {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean check = true;
		String msg = null;
		
		//查询当前资源是否使用(T_OFFICIAL_SYS_PRIVILEGE)
		List<Resource> resList = this.resourceService.findByParentId(resource.getId());
		if(resList!=null&&resList.size()>0){
			check = false;
			msg = "当前资源下存在子资源，无法删除！";
		} else {
			//查询当前资源是否存在子资源
			Privilege entity = new Privilege();
			entity.setResourceId(resource.getId());
			List<Privilege> privileges = privilegeService.find(entity);
			if(privileges!=null && privileges.size()>0){
				check = false;
				msg = "当前资源已经使用，无法删除！";
			} else {
				Resource model = this.resourceService.get(resource.getId());
				model.setStatus("0");
				this.resourceService.deleteResource(model);
			}
		}
		
		map.put("check", check);
		map.put("msg", msg);
		
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 整理树节点
	 * @param resources
	 * @return
	 */
	private List<TreeNode> getTreeNodes(List<Resource> resources) {
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (Resource resource : resources) {
			TreeNode treeNode = new TreeNode();
			if (resource.getChildrenNum() !=null && resource.getChildrenNum() > 0) {
				treeNode.setIsParent(true);
			} else {
				treeNode.setIsParent(false);
			}

			treeNode.setId(String.valueOf(resource.getId()));
			treeNode.setpId(String.valueOf(resource.getParentId()));
			treeNode.setName(resource.getResourceName());
			treeNode.setIconSkin(Resource.class.getSimpleName().toLowerCase());
			
			Map<String, Object> props = new HashMap<String, Object>();
			props.put("id", resource.getId());
			//增加节点的父节点
			props.put("parentid", resource.getParentId());
			treeNode.setProps(props);

			treeNodes.add(treeNode);
		}
		return treeNodes;
	}
}
