package com.jd.official.modules.system.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.Application;
import com.jd.official.modules.system.service.ApplicationService;

/**
 * 
 * @author: zhouhq
 */
@Controller
@RequestMapping(value="/application")
public class ApplicationController {
    private static final Logger logger = Logger.getLogger(ApplicationController.class);

    @Autowired
    private ApplicationService applicationService;
    
/*    @Autowired
    private TestHelloService helloService;*/
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String list(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
//    	List<Application> applications = applicationService.find(new Application());
    	
    	
        return "table";
    }
    
	@RequestMapping(value="/page")
	@ResponseBody
	public Object page(HttpServletRequest request){
		//创建pageWrapper
		PageWrapper<Application> pageWrapper=new PageWrapper<Application>(request);
		//添加搜索条件
		String erp_count=request.getParameter("erp_count");
//		if(null!=erp_count&&!erp_count.equals("")){
//		}
		pageWrapper.addSearch("name",erp_count);
		//后台取值
	    applicationService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
	    //返回到页面的额外数据
	    pageWrapper.addResult("returnKey","returnValue");
	    //返回到页面的数据
		String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug(json);
        return json;
	}
	
	@RequestMapping(value="/applications")
	@ResponseBody
	public String applications(Application application){
		List<Application> applications = applicationService.find(new Application());
		
		
		return JSONArray.fromObject(applications).toString();
	}
	
	@RequestMapping(value="/application_list")
	public ModelAndView application_list(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("system/application_list");  
		List<Application> applications = applicationService.find(new Application());
		mav.addObject("dt", new Date());
		mav.addObject("applications", applications);
		return mav;
	}
	
    @RequestMapping(value="/application_save")
	@ResponseBody
	public String save(Application application){
		Map<String,Object> map = new HashMap<String,Object>();
		applicationService.insert(application);
		map.put("operator", true);
		map.put("message", "添加成功");
		
		
		return JSONObject.fromObject(map).toString();
	}
    
    @RequestMapping(value = "/application_add",method=RequestMethod.GET) 
    public String add() {
    	ModelAndView model = new ModelAndView("greeting");  
        model.addObject("msg" , "Hello to Andy Clark");  
		return "system/application_add";
    }
    
    
    /*@RequestMapping(value = "/saf",method=RequestMethod.GET) 
    public String saf() {
    	ModelAndView model = new ModelAndView("greeting");  
        model.addObject("msg" , "Hello to Andy Clark");  
        System.out.println("----------------saf"+helloService.test("hello,saf"));
		return "system/application_add";
    }*/
    @RequestMapping(value = "/validate_test",method=RequestMethod.GET) 
    public String validate() {
		return "validate_test";
    }
    

    
}
