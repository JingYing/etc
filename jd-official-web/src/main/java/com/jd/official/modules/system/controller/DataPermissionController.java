/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.official.modules.system.model.*;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.service.AuthExpressionService;
import com.jd.official.modules.system.service.DataPermissionService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserService;

/**
 *@Description: 系统功能 Controller
 *@author liub
 *
 */

@Controller
@RequestMapping(value="/system")
public class DataPermissionController {
	
	 private static final Logger logger = Logger.getLogger(DataPermissionController.class);
	
	 @Autowired
	 private DataPermissionService dataPermissionService;
	 
	 @Autowired
	 private UserService userService;
	 
	 @Autowired
	 private OrganizationService organizationService;
	 
	 @Autowired
	 private PositionService positionService;
	 
	 @Autowired
	 private RoleService roleService;
	 
	 @Autowired
	 private AuthExpressionService authExpressionService;
	 
	 @RequestMapping(value = "/dataPermission_index", method = RequestMethod.GET)
	 public String index() throws Exception {
	     return "system/dataPermission_index";
	 }
	 
	 @RequestMapping(value = "/dataPermission_findDictDataPage")
	 @ResponseBody
	 public Object list(HttpServletRequest request,DataPermission dataPermission) throws Exception {
		 PageWrapper<DataPermission> pageWrapper = new PageWrapper<DataPermission>(request);
		 if(dataPermission != null){
			 if (null != dataPermission.getEntityType() && !dataPermission.getEntityType().equals("")) {
				 pageWrapper.addSearch("entityType", dataPermission.getEntityType());
			 }
			 if (null != dataPermission.getEntityName() && !dataPermission.getEntityName().equals("")) {
				 pageWrapper.addSearch("entityName", dataPermission.getEntityName());
			 }
		 }
		 dataPermissionService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		 pageWrapper.addResult("returnKey","returnValue");
		 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	     logger.debug("result:"+json);
	     return json;
	 }
	 
	 @RequestMapping(value = "/dataPermission_list", method = RequestMethod.GET)
	    public ModelAndView dictDataList()
	            throws Exception {
	        ModelAndView mav = new ModelAndView("system/dataPermission_list");
	        return mav;
	    }
	 
	 /**
	  * add view
	  * @return
	  * @throws Exception
	  */
	 @RequestMapping(value = "/dataPermission_add", method = RequestMethod.GET)
	 public String add(Model model) throws Exception {
	     return "system/dataPermission_add";
	 }
	 
    /**
     *
     * @param entity
     * @return
     */
	 @RecordLog(operationType=OperationTypeValue.add_update, entityName="DataPermission")
	 @RequestMapping(value="/dataPermission_save" , method = RequestMethod.POST)
	 @ResponseBody
	 public String save(DataPermission entity,DpSubject dpentity,HttpServletRequest request){
	    Map<String,Object> map = new HashMap<String,Object>();
	    if(entity != null)
	    	entity.setStatus("1");
	 	if(entity.getId() != null){
	 		dataPermissionService.updateData(entity,dpentity);
            map.put("message", "修改成功");
	 	} else {
            Long id =  dataPermissionService.insertData(entity, dpentity);
	 		request.setAttribute("entityId", id);
            map.put("message", "添加成功");
	 	}
	 	map.put("operator", true);

	 	return JSONObject.fromObject(map).toString();
	 }
	 
	 
	 /**
	  * delete 
	  * @param  entity id
	  * @return
	  */
	 @RecordLog(operationType=OperationTypeValue.delete, entityName="DataPermission")
	 @RequestMapping(value="/dataPermission_delete" , method = RequestMethod.POST,produces = "application/json")
	 @ResponseBody
	 public String delete(DataPermission entity){
		 Map<String,Object> map = new HashMap<String,Object>();
		 int result = dataPermissionService.deleteData(entity);
		 if(result>0){
			 map.put("operator", true);
			 map.put("message", "删除成功");
		 }else{
			 map.put("operator", false);
			 map.put("message", "删除失败");
		 }
		 return JSONObject.fromObject(map).toString();
	 }
	 
	 /**
	  * edit 
	  * @param id entity id
	  * @return
	  */
	 @RequestMapping(value="/dataPermission_update" , method = RequestMethod.GET)
	 public ModelAndView get(Long id){
		 ModelAndView mv = new ModelAndView();
		 DataPermission dataPermission = dataPermissionService.get(id);
		 mv.setViewName("system/dataPermission_edit");
		 mv.addObject("dataPermission", dataPermission);
		 return mv;
	 }
	 
	 @RequestMapping(value="/dataPermission_view" , method = RequestMethod.GET)
	 public ModelAndView view(Long id){
         DataPermission dataPermission = dataPermissionService.get(id);
         ModelAndView mv = new ModelAndView();
         mv.setViewName("system/dataPermission_view");
         mv.addObject("dataPermission", dataPermission);
		 return mv;
	 }
	 
}
