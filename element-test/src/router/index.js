import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {path: '/login',component: resolve => require(['@/components/Login.vue'], resolve)},
        {path: '/',component: resolve => require(['@/components/Home.vue'], resolve),
            children:[
                {path: '/a',component: resolve => require(['@/components/biz/FormTest.vue'], resolve)},
                {path: '/b',component: resolve => require(['@/components/biz/TableTest1.vue'], resolve)},
                {path: '/c',component: resolve => require(['@/components/biz/TableTest2.vue'], resolve)},
            ]
        },
        {path: '*',component: resolve => require(['@/components/NotFound.vue'], resolve)}
    ]
})
