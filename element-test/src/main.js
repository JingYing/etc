import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'
import router from './router';
import * as MyMock from './mock'
import axios from 'axios'

Vue.config.productionTip = false

var axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API
});
axiosInstance.interceptors.response.use(
    response => {
      console.log("axios interceptor onFulfilled",response);
      let err=response.data.err;
      if(err==403){
        router.push('/login');
      }else if(err==500){
          alert('server error');
      }else{
          return response;
      }
    },
    error => {
      // console.log("axios interceptor onRejected");
      // return Promise.reject(error);
        alert('无法连接服务器,请稍后重试,或者联系开发人员');
    }
);
Vue.prototype.$axios=axiosInstance;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

process.env.VUE_APP_MOCK=='true' && MyMock.mock();

