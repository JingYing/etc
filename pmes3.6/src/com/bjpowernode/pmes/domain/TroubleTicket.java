package com.bjpowernode.pmes.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="t_trouble_ticket")
public class TroubleTicket {

	public static final String NEW = "新建";
	
	public static final String END = "结束";
	
	private Integer id;
	
	private String desc;
	
	private String level;
	
	private String location;
	
	private String happenTime;
	
	private String createTime;
	
	private Set<ApproveInfo> approveInfoSet;
	
	private User creator;
	
	private String status;
	
	@Id @GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="desc_")
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getHappenTime() {
		return happenTime;
	}

	public void setHappenTime(String happenTime) {
		this.happenTime = happenTime;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	@OneToMany(mappedBy="troubleTicket")
	public Set<ApproveInfo> getApproveInfoSet() {
		return approveInfoSet;
	}

	public void setApproveInfoSet(Set<ApproveInfo> approveInfoSet) {
		this.approveInfoSet = approveInfoSet;
	}

	@ManyToOne
	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
