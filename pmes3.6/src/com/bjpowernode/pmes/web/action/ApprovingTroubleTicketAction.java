package com.bjpowernode.pmes.web.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.domain.TroubleTicket;
import com.bjpowernode.pmes.service.TroubleTicketService;

@Controller
@Scope("prototype")
//public class ApprovingTroubleTicketAction extends BaseAction {
public class ApprovingTroubleTicketAction extends AbtractTroubleTicketAction {

	private TroubleTicketService troubleTicketService;
	
	@Autowired
	public void setTroubleTicketService(TroubleTicketService troubleTicketService) {
		this.troubleTicketService = troubleTicketService;
	}
	
	public String index() throws Exception {
		return SUCCESS;
	}
	
	public String list() throws Exception {
		Pager<TroubleTicket> pager = troubleTicketService.findApprovingTroubleTicketList(currentUser().getId());
		//{total=100, data:[{},{}]}
//		StringBuffer sbJson = new StringBuffer();
//		sbJson.append("{");
//		sbJson.append("total:");
//		sbJson.append(pager.getTotal());
//		sbJson.append(", data:[");
//		List<TroubleTicket> troubleTicketList = pager.getData();
//		for (int i=0; i<troubleTicketList.size(); i++) {
//			TroubleTicket troubleTicket = troubleTicketList.get(i);
//			sbJson.append("{");
//			sbJson.append("id:");
//			sbJson.append(troubleTicket.getId());
//			sbJson.append(",desc:'");
//			sbJson.append(troubleTicket.getDesc());
//			sbJson.append("',level:'");
//			sbJson.append(troubleTicket.getLevel());
//			sbJson.append("',location:'");
//			sbJson.append(troubleTicket.getLocation());
//			sbJson.append("',happenTime:'");
//			sbJson.append(troubleTicket.getHappenTime());
//			sbJson.append("',createTime:'");
//			sbJson.append(troubleTicket.getHappenTime());
//			sbJson.append("',creator:'");
//			sbJson.append(troubleTicket.getCreator().getName());
//			sbJson.append("'}");
//			if (i <(troubleTicketList.size() -1)) {
//				sbJson.append(",");
//			}
//		}
//		sbJson.append("]");
//		sbJson.append("}");		
//		return ajaxOut(sbJson.toString());
		return troubleTicket2JsonString(pager);
	}
}
