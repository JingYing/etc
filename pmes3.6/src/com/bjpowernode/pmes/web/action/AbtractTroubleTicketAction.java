package com.bjpowernode.pmes.web.action;

import java.util.List;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.domain.TroubleTicket;

public class AbtractTroubleTicketAction extends BaseAction {

	protected String troubleTicket2JsonString(Pager<TroubleTicket> pager) throws Exception{
		StringBuffer sbJson = new StringBuffer();
		sbJson.append("{");
		sbJson.append("total:");
		sbJson.append(pager.getTotal());
		sbJson.append(", data:[");
		List<TroubleTicket> troubleTicketList = pager.getData();
		for (int i=0; i<troubleTicketList.size(); i++) {
			TroubleTicket troubleTicket = troubleTicketList.get(i);
			sbJson.append("{");
			sbJson.append("id:");
			sbJson.append(troubleTicket.getId());
			sbJson.append(",desc:'");
			sbJson.append(troubleTicket.getDesc());
			sbJson.append("',level:'");
			sbJson.append(troubleTicket.getLevel());
			sbJson.append("',location:'");
			sbJson.append(troubleTicket.getLocation());
			sbJson.append("',happenTime:'");
			sbJson.append(troubleTicket.getHappenTime());
			sbJson.append("',createTime:'");
			sbJson.append(troubleTicket.getHappenTime());
			sbJson.append("',status:'");
			sbJson.append(troubleTicket.getStatus());
			sbJson.append("',creator:'");
			sbJson.append(troubleTicket.getCreator().getName());			
			sbJson.append("'}");
			if (i <(troubleTicketList.size() -1)) {
				sbJson.append(",");
			}
		}
		sbJson.append("]");
		sbJson.append("}");	
		return ajaxOut(sbJson.toString());
	}
}
