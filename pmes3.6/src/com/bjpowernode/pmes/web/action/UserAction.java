package com.bjpowernode.pmes.web.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bjpowernode.pmes.Globals;
import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.domain.User;
import com.bjpowernode.pmes.service.UserService;

@Controller
@Scope("prototype")
public class UserAction extends BaseAction {

	private UserService userService;
	
	private User user;
	
	private Integer[] userId;
	
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer[] getUserId() {
		return userId;
	}

	public void setUserId(Integer[] userId) {
		this.userId = userId;
	}

	public String index() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * 返回JSON字符串
	 * {total:100, data:[{},{},{}]}
	 * @return
	 * @throws Exception
	 */
	public String list() throws Exception {
		Pager<User> pager = userService.findUserList();
		//{total:100, data:[{},{},{}]}
		StringBuffer sbJson = new StringBuffer();
		sbJson.append("{");
		sbJson.append("total:");
		sbJson.append(pager.getTotal());
		sbJson.append(", data:[");
		List<User> userList = pager.getData();
		for (int i=0; i<userList.size(); i++) {
			User user = userList.get(i);
//			sbJson.append("{");
//			sbJson.append("id:");
//			sbJson.append(user.getId());
//			sbJson.append(",code:'");
//			sbJson.append(user.getCode());
//			sbJson.append("',name:'");
//			sbJson.append(user.getName());
//			sbJson.append("',sex:'");
//			sbJson.append(user.getSex());
//			sbJson.append("',deptId:");
//			sbJson.append(user.getDept().getId());
//			sbJson.append(",deptName:'");
//			sbJson.append(user.getDept().getName());
//			sbJson.append("'}");
			sbJson.append(user2JsonString(user));
			if (i<(userList.size() - 1)) {
				sbJson.append(",");
			}
		}
		sbJson.append("]");
		sbJson.append("}");
//		getResponse().setContentType("text/html;charset=utf-8");
//		getResponse().getWriter().print(sbJson.toString());
		return ajaxOut(sbJson.toString());
	}
	
	public String addOrUpdate() throws Exception {
		userService.addOrUpdate(user);
		return ajaxOut("{success: true}");
	} 	
	
	public String edit() throws Exception {
		user = userService.findUserById(user.getId());
		return ajaxOut("{success:true, data:" + user2JsonString(user) + "}");
	}
	
	private String user2JsonString(User user) {
		StringBuilder sbJson = new StringBuilder();
		sbJson.append("{");
		sbJson.append("id:");
		sbJson.append(user.getId());
		sbJson.append(",code:'");
		sbJson.append(user.getCode());
		sbJson.append("',name:'");
		sbJson.append(user.getName());
		sbJson.append("',password:'");
		sbJson.append(user.getPassword());
		sbJson.append("',sex:'");
		sbJson.append(user.getSex());
		sbJson.append("',deptId:");
		sbJson.append(user.getDept().getId());
		sbJson.append(",deptName:'");
		sbJson.append(user.getDept().getName());
		sbJson.append("'}");
		return sbJson.toString();
	}
	
	public String del() throws Exception {
		userService.del(userId);
		return ajaxOut();
	}
	
	public String login() throws Exception {
		user = userService.login(user.getCode(), user.getPassword());
		if (user != null) {
			getSession().setAttribute(Globals.CURRENT_USER, user);
			return ajaxOut("{success: true}");
		}else {
			return ajaxOut("{success: false}");
		}
	}
	
	
}
