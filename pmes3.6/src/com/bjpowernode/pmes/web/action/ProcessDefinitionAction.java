package com.bjpowernode.pmes.web.action;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.jbpm.api.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.service.ProcessDefinitionService;

@Controller
@Scope("prototype")
public class ProcessDefinitionAction extends BaseAction {

	private ProcessDefinitionService processDefinitionService;
	
	private File processDefinitionFile;
	
	private String[] deploymentIds;
	
	private String imageResourceName;
	
	@Autowired
	public void setProcessDefinitionService(
			ProcessDefinitionService processDefinitionService) {
		this.processDefinitionService = processDefinitionService;
	}
	
	public File getProcessDefinitionFile() {
		return processDefinitionFile;
	}

	public void setProcessDefinitionFile(File processDefinitionFile) {
		this.processDefinitionFile = processDefinitionFile;
	}

	public String[] getDeploymentIds() {
		return deploymentIds;
	}

	public void setDeploymentIds(String[] deploymentIds) {
		this.deploymentIds = deploymentIds;
	}

	public String getImageResourceName() {
		return imageResourceName;
	}

	public void setImageResourceName(String imageResourceName) {
		this.imageResourceName = imageResourceName;
	}

	public String index() throws Exception {
		return SUCCESS;
	}
	
	public String list() throws Exception {
		Pager<ProcessDefinition> pager = processDefinitionService.findProcessDefinitionList();
		//{total:100, data:[{},{}]}
		StringBuilder sbJson = new StringBuilder(); 
		sbJson.append("{");
		sbJson.append("total:");
		sbJson.append(pager.getTotal());
		sbJson.append(",data:[");
		List<ProcessDefinition> processDefinitionList = pager.getData();
		for (int i = 0; i < processDefinitionList.size(); i++) {
			ProcessDefinition processDefinition = processDefinitionList.get(i);
			sbJson.append("{");
			sbJson.append("deploymentId:'");
			sbJson.append(processDefinition.getDeploymentId());
			sbJson.append("',id:'");
			sbJson.append(processDefinition.getId());
			sbJson.append("',name:'");
			sbJson.append(processDefinition.getName());
			sbJson.append("',version:");
			sbJson.append(processDefinition.getVersion());
			sbJson.append(",imageResourceName:'");
			sbJson.append(processDefinition.getImageResourceName());
			sbJson.append("'}");
			if (i <(processDefinitionList.size() - 1)) {
				sbJson.append(",");
			}
		}
		sbJson.append("]");
		sbJson.append("}");
		return ajaxOut(sbJson.toString());
	}
	
	public String add() throws Exception {
		processDefinitionService.add(processDefinitionFile);
		return ajaxOut("{success:true}");
	}
	
	public String del() throws Exception {
		processDefinitionService.del(deploymentIds);
		return ajaxOut();
	}
	
	public String viewImage() throws Exception {
		InputStream is = processDefinitionService.viewImage(deploymentIds[0], imageResourceName);
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = is.read(buffer)) != -1) {
			getResponse().getOutputStream().write(buffer, 0, len);	
		}
		return null;
	}
	
	public String viewConent() throws Exception {
		InputStream is = processDefinitionService.viewConent(deploymentIds[0]);
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = is.read(buffer)) != -1) {
			getResponse().getOutputStream().write(buffer, 0, len);	
		}
		return null;
	}
	
}
