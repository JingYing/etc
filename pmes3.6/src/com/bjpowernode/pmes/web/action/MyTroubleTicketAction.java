package com.bjpowernode.pmes.web.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.domain.TroubleTicket;
import com.bjpowernode.pmes.service.TroubleTicketService;

@Controller
@Scope("prototype")
//public class MyTroubleTicketAction extends BaseAction {
public class MyTroubleTicketAction extends AbtractTroubleTicketAction {

	private TroubleTicketService troubleTicketService;
	
	private TroubleTicket troubleTicket;
	
	private Integer[] troubleTicketIds;
	
	@Autowired
	public void setTroubleTicketService(TroubleTicketService troubleTicketService) {
		this.troubleTicketService = troubleTicketService;
	}

	public TroubleTicket getTroubleTicket() {
		return troubleTicket;
	}

	public void setTroubleTicket(TroubleTicket troubleTicket) {
		this.troubleTicket = troubleTicket;
	}

	public Integer[] getTroubleTicketIds() {
		return troubleTicketIds;
	}

	public void setTroubleTicketIds(Integer[] troubleTicketIds) {
		this.troubleTicketIds = troubleTicketIds;
	}

	public String index() throws Exception {
		return SUCCESS;
	}
	
	public String list() throws Exception {
		Pager<TroubleTicket> pager = troubleTicketService.findMyTroubleTicketList(currentUser().getId());
		//{total=100, data:[{},{}]}
//		StringBuffer sbJson = new StringBuffer();
//		sbJson.append("{");
//		sbJson.append("total:");
//		sbJson.append(pager.getTotal());
//		sbJson.append(", data:[");
//		List<TroubleTicket> troubleTicketList = pager.getData();
//		for (int i=0; i<troubleTicketList.size(); i++) {
//			TroubleTicket troubleTicket = troubleTicketList.get(i);
//			sbJson.append("{");
//			sbJson.append("id:");
//			sbJson.append(troubleTicket.getId());
//			sbJson.append(",desc:'");
//			sbJson.append(troubleTicket.getDesc());
//			sbJson.append("',level:'");
//			sbJson.append(troubleTicket.getLevel());
//			sbJson.append("',location:'");
//			sbJson.append(troubleTicket.getLocation());
//			sbJson.append("',happenTime:'");
//			sbJson.append(troubleTicket.getHappenTime());
//			sbJson.append("',createTime:'");
//			sbJson.append(troubleTicket.getHappenTime());
//			sbJson.append("',status:'");
//			sbJson.append(troubleTicket.getStatus());
//			sbJson.append("'}");
//			if (i <(troubleTicketList.size() -1)) {
//				sbJson.append(",");
//			}
//		}
//		sbJson.append("]");
//		sbJson.append("}");		
//		return ajaxOut(sbJson.toString());
		return troubleTicket2JsonString(pager);
	}
	
	public String add() throws Exception {
		//设置建障人
		troubleTicket.setCreator(currentUser());
		troubleTicketService.addOrUpdate(troubleTicket);
		return ajaxOut("{success:true}");
	}
	
	public String dispatch() throws Exception {
		troubleTicketService.dispatch(troubleTicketIds);
		return ajaxOut();
	}
}
