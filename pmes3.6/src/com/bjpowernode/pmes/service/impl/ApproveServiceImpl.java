package com.bjpowernode.pmes.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjpowernode.pmes.dao.ApproveDao;
import com.bjpowernode.pmes.dao.TroubleTicketDao;
import com.bjpowernode.pmes.domain.ApproveInfo;
import com.bjpowernode.pmes.service.ApproveService;
import com.sun.org.apache.bcel.internal.generic.NEW;

@Service
public class ApproveServiceImpl implements ApproveService {

	private ApproveDao approveDao;
	
	private TroubleTicketDao troubleTicketDao;
	
	@Autowired
	public void setApproveDao(ApproveDao approveDao) {
		this.approveDao = approveDao;
	}
	
	@Autowired
	public void setTroubleTicketDao(TroubleTicketDao troubleTicketDao) {
		this.troubleTicketDao = troubleTicketDao;
	}

	@Override
	public void add(ApproveInfo approveInfo) {
		approveInfo.setApproveTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		approveDao.add(approveInfo);
		//流程继续向下走
		troubleTicketDao.completeTask(approveInfo.getTroubleTicket().getId(), approveInfo.getApprover().getId());
	}

	@Override
	public List<ApproveInfo> findHistoryByTroubleTicketId(
			Integer troubleTicketId) {
		return approveDao.findHistoryByTroubleTicketId(troubleTicketId);
	}
}
