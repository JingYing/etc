package com.bjpowernode.pmes.service;

import java.io.File;
import java.io.InputStream;

import org.jbpm.api.ProcessDefinition;

import com.bjpowernode.pmes.Pager;

public interface ProcessDefinitionService {

	/**
	 * 分页查询流程定义
	 * @return
	 */
	public Pager<ProcessDefinition> findProcessDefinitionList();

	/**
	 * 部署流程 
	 * @param processDefinitionFile
	 */
	public void add(File processDefinitionFile);
	
	/**
	 * 根据部署id集合删除
	 * @param deploymentId
	 */
	public void del(String[] deploymentIds);
	
	/**
	 * 查看图片
	 * @param deploymentId
	 * @param imageResourceName
	 */
	public InputStream viewImage(String deploymentId, String imageResourceName);

	/**
	 * 查询jpdl内容
	 * @param deploymentId
	 * @return
	 */
	public InputStream viewConent(String deploymentId);
	
}
