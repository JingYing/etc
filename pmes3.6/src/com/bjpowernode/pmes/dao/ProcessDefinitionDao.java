package com.bjpowernode.pmes.dao;

import java.io.File;
import java.io.InputStream;

import org.jbpm.api.ProcessDefinition;

import com.bjpowernode.pmes.Pager;

public interface ProcessDefinitionDao {

	/**
	 * 分页查询流程定义
	 * @return
	 */
	public Pager<ProcessDefinition> findProcessDefinitionList();
	
	/**
	 * 部署流程 
	 * @param processDefinitionFile
	 */
	public void add(File processDefinitionFile);
	
	/**
	 * 根据部署id删除
	 * @param deploymentId
	 */
	public void del(String deploymentId);
	
	/**
	 * 查看图片
	 * @param deploymentId
	 * @param imageResourceName
	 */
	public InputStream viewImage(String deploymentId, String imageResourceName);
	
	/**
	 * 查询jpdl内容
	 * @param deploymentId
	 * @return
	 */
	public InputStream viewConent(String deploymentId);
}
