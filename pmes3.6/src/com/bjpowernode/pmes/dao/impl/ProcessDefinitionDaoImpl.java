package com.bjpowernode.pmes.dao.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipInputStream;

import org.jbpm.api.ProcessDefinition;
import org.jbpm.api.ProcessDefinitionQuery;
import org.jbpm.api.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.PagerContext;
import com.bjpowernode.pmes.dao.ProcessDefinitionDao;

@Repository
public class ProcessDefinitionDaoImpl extends BaseDao implements
		ProcessDefinitionDao {

	private RepositoryService repositoryService;
	
	@Autowired
	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

	@Override
	public Pager<ProcessDefinition> findProcessDefinitionList() {
		List<ProcessDefinition> processDefinitionList = repositoryService.createProcessDefinitionQuery()
				.processDefinitionName("TroubleTicket")
				.page(PagerContext.getOffset(), PagerContext.getPageSize())
				.orderDesc(ProcessDefinitionQuery.PROPERTY_VERSION)
				.list();
		long total = repositoryService.createProcessDefinitionQuery().count();
		
		Pager<ProcessDefinition> pager = new Pager<ProcessDefinition>();
		pager.setData(processDefinitionList);
		pager.setTotal(new Long(total).intValue());
		return pager;
	}

	@Override
	public void add(File processDefinitionFile) {
		try {
			repositoryService.createDeployment()
					.addResourcesFromZipInputStream(
							new ZipInputStream(
									new FileInputStream(processDefinitionFile)))
					.deploy();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}				
	}

	@Override
	public void del(String deploymentId) {
		repositoryService.deleteDeploymentCascade(deploymentId);
	}

	@Override
	public InputStream viewImage(String deploymentId, String imageResourceName) {
		return repositoryService.getResourceAsStream(deploymentId, imageResourceName);
	}

	@Override
	public InputStream viewConent(String deploymentId) {
		String jpdlResourceName = null;
		Set<String> resouceNameSet = repositoryService.getResourceNames(deploymentId);
		for (String resouceName : resouceNameSet) {
			if (resouceName.endsWith(".jpdl.xml")) {
				jpdlResourceName = resouceName;
				break;
			}
		}
		return repositoryService.getResourceAsStream(deploymentId, jpdlResourceName);		
	}
}
