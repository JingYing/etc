package com.bjpowernode.pmes.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bjpowernode.pmes.dao.ApproveDao;
import com.bjpowernode.pmes.domain.ApproveInfo;

@Repository
public class ApproveDaoImpl extends BaseDao implements ApproveDao {

	@Override
	public void add(ApproveInfo approveInfo) {
		getHibernateTemplate().save(approveInfo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApproveInfo> findHistoryByTroubleTicketId(
			Integer troubleTicketId) {
		return getHibernateTemplate().find("from ApproveInfo ai where ai.troubleTicket.id=? order by ai.approveTime", troubleTicketId);
	}

}
