package com.jd.ump.log4j.spi;

import java.io.Writer;

/** @deprecated */
class NullWriter extends Writer
{
  public void close()
  {
  }

  public void flush()
  {
  }

  public void write(char[] cbuf, int off, int len)
  {
  }
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.NullWriter
 * JD-Core Version:    0.6.2
 */