/*     */ package com.jd.ump.log4j.helpers;
/*     */ 
/*     */ import com.jd.ump.log4j.Level;
/*     */ import com.jd.ump.log4j.PropertyConfigurator;
/*     */ import com.jd.ump.log4j.spi.Configurator;
/*     */ import com.jd.ump.log4j.spi.LoggerRepository;
/*     */ import java.io.InterruptedIOException;
/*     */ import java.lang.reflect.InvocationTargetException;
/*     */ import java.lang.reflect.Method;
/*     */ import java.net.URL;
/*     */ import java.util.Properties;
/*     */ 
/*     */ public class OptionConverter
/*     */ {
/*  42 */   static String DELIM_START = "${";
/*  43 */   static char DELIM_STOP = '}';
/*  44 */   static int DELIM_START_LEN = 2;
/*  45 */   static int DELIM_STOP_LEN = 1;
/*     */ 
/*     */   public static String[] concatanateArrays(String[] l, String[] r)
/*     */   {
/*  53 */     int len = l.length + r.length;
/*  54 */     String[] a = new String[len];
/*     */ 
/*  56 */     System.arraycopy(l, 0, a, 0, l.length);
/*  57 */     System.arraycopy(r, 0, a, l.length, r.length);
/*     */ 
/*  59 */     return a;
/*     */   }
/*     */ 
/*     */   public static String convertSpecialChars(String s)
/*     */   {
/*  66 */     int len = s.length();
/*  67 */     StringBuffer sbuf = new StringBuffer(len);
/*     */ 
/*  69 */     int i = 0;
/*  70 */     while (i < len) {
/*  71 */       char c = s.charAt(i++);
/*  72 */       if (c == '\\') {
/*  73 */         c = s.charAt(i++);
/*  74 */         if (c == 'n') c = '\n';
/*  75 */         else if (c == 'r') c = '\r';
/*  76 */         else if (c == 't') c = '\t';
/*  77 */         else if (c == 'f') c = '\f';
/*  78 */         else if (c == '\b') c = '\b';
/*  79 */         else if (c == '"') c = '"';
/*  80 */         else if (c == '\'') c = '\'';
/*  81 */         else if (c == '\\') c = '\\';
/*     */       }
/*  83 */       sbuf.append(c);
/*     */     }
/*  85 */     return sbuf.toString();
/*     */   }
/*     */ 
/*     */   public static String getSystemProperty(String key, String def)
/*     */   {
/*     */     try
/*     */     {
/* 103 */       return System.getProperty(key, def);
/*     */     } catch (Throwable e) {
/* 105 */       LogLog.debug("Was not allowed to read system property \"" + key + "\".");
/* 106 */     }return def;
/*     */   }
/*     */ 
/*     */   public static Object instantiateByKey(Properties props, String key, Class superClass, Object defaultValue)
/*     */   {
/* 117 */     String className = findAndSubst(key, props);
/* 118 */     if (className == null) {
/* 119 */       LogLog.error("Could not find value for key " + key);
/* 120 */       return defaultValue;
/*     */     }
/*     */ 
/* 123 */     return instantiateByClassName(className.trim(), superClass, defaultValue);
/*     */   }
/*     */ 
/*     */   public static boolean toBoolean(String value, boolean dEfault)
/*     */   {
/* 137 */     if (value == null)
/* 138 */       return dEfault;
/* 139 */     String trimmedVal = value.trim();
/* 140 */     if ("true".equalsIgnoreCase(trimmedVal))
/* 141 */       return true;
/* 142 */     if ("false".equalsIgnoreCase(trimmedVal))
/* 143 */       return false;
/* 144 */     return dEfault;
/*     */   }
/*     */ 
/*     */   public static int toInt(String value, int dEfault)
/*     */   {
/* 150 */     if (value != null) {
/* 151 */       String s = value.trim();
/*     */       try {
/* 153 */         return Integer.valueOf(s).intValue();
/*     */       }
/*     */       catch (NumberFormatException e) {
/* 156 */         LogLog.error("[" + s + "] is not in proper int form.");
/* 157 */         e.printStackTrace();
/*     */       }
/*     */     }
/* 160 */     return dEfault;
/*     */   }
/*     */ 
/*     */   public static Level toLevel(String value, Level defaultValue)
/*     */   {
/* 186 */     if (value == null) {
/* 187 */       return defaultValue;
/*     */     }
/* 189 */     value = value.trim();
/*     */ 
/* 191 */     int hashIndex = value.indexOf(35);
/* 192 */     if (hashIndex == -1) {
/* 193 */       if ("NULL".equalsIgnoreCase(value)) {
/* 194 */         return null;
/*     */       }
/*     */ 
/* 197 */       return Level.toLevel(value, defaultValue);
/*     */     }
/*     */ 
/* 201 */     Level result = defaultValue;
/*     */ 
/* 203 */     String clazz = value.substring(hashIndex + 1);
/* 204 */     String levelName = value.substring(0, hashIndex);
/*     */ 
/* 207 */     if ("NULL".equalsIgnoreCase(levelName)) {
/* 208 */       return null;
/*     */     }
/*     */ 
/* 211 */     LogLog.debug("toLevel:class=[" + clazz + "]" + ":pri=[" + levelName + "]");
/*     */     try
/*     */     {
/* 215 */       Class customLevel = Loader.loadClass(clazz);
/*     */ 
/* 219 */       Class[] paramTypes = { String.class, Level.class };
/*     */ 
/* 222 */       Method toLevelMethod = customLevel.getMethod("toLevel", paramTypes);
/*     */ 
/* 226 */       Object[] params = { levelName, defaultValue };
/* 227 */       Object o = toLevelMethod.invoke(null, params);
/*     */ 
/* 229 */       result = (Level)o;
/*     */     } catch (ClassNotFoundException e) {
/* 231 */       LogLog.warn("custom level class [" + clazz + "] not found.");
/*     */     } catch (NoSuchMethodException e) {
/* 233 */       LogLog.warn("custom level class [" + clazz + "]" + " does not have a class function toLevel(String, Level)", e);
/*     */     }
/*     */     catch (InvocationTargetException e) {
/* 236 */       if (((e.getTargetException() instanceof InterruptedException)) || ((e.getTargetException() instanceof InterruptedIOException)))
/*     */       {
/* 238 */         Thread.currentThread().interrupt();
/*     */       }
/* 240 */       LogLog.warn("custom level class [" + clazz + "]" + " could not be instantiated", e);
/*     */     }
/*     */     catch (ClassCastException e) {
/* 243 */       LogLog.warn("class [" + clazz + "] is not a subclass of org.apache.log4j.Level", e);
/*     */     }
/*     */     catch (IllegalAccessException e) {
/* 246 */       LogLog.warn("class [" + clazz + "] cannot be instantiated due to access restrictions", e);
/*     */     }
/*     */     catch (RuntimeException e) {
/* 249 */       LogLog.warn("class [" + clazz + "], level [" + levelName + "] conversion failed.", e);
/*     */     }
/*     */ 
/* 252 */     return result;
/*     */   }
/*     */ 
/*     */   public static long toFileSize(String value, long dEfault)
/*     */   {
/* 258 */     if (value == null) {
/* 259 */       return dEfault;
/*     */     }
/* 261 */     String s = value.trim().toUpperCase();
/* 262 */     long multiplier = 1L;
/*     */     int index;
/* 265 */     if ((index = s.indexOf("KB")) != -1) {
/* 266 */       multiplier = 1024L;
/* 267 */       s = s.substring(0, index);
/*     */     }
/* 269 */     else if ((index = s.indexOf("MB")) != -1) {
/* 270 */       multiplier = 1048576L;
/* 271 */       s = s.substring(0, index);
/*     */     }
/* 273 */     else if ((index = s.indexOf("GB")) != -1) {
/* 274 */       multiplier = 1073741824L;
/* 275 */       s = s.substring(0, index);
/*     */     }
/* 277 */     if (s != null) {
/*     */       try {
/* 279 */         return Long.valueOf(s).longValue() * multiplier;
/*     */       }
/*     */       catch (NumberFormatException e) {
/* 282 */         LogLog.error("[" + s + "] is not in proper int form.");
/* 283 */         LogLog.error("[" + value + "] not in expected format.", e);
/*     */       }
/*     */     }
/* 286 */     return dEfault;
/*     */   }
/*     */ 
/*     */   public static String findAndSubst(String key, Properties props)
/*     */   {
/* 298 */     String value = props.getProperty(key);
/* 299 */     if (value == null)
/* 300 */       return null;
/*     */     try
/*     */     {
/* 303 */       return substVars(value, props);
/*     */     } catch (IllegalArgumentException e) {
/* 305 */       LogLog.error("Bad option value [" + value + "].", e);
/* 306 */     }return value;
/*     */   }
/*     */ 
/*     */   public static Object instantiateByClassName(String className, Class superClass, Object defaultValue)
/*     */   {
/* 324 */     if (className != null) {
/*     */       try {
/* 326 */         Class classObj = Loader.loadClass(className);
/* 327 */         if (!superClass.isAssignableFrom(classObj)) {
/* 328 */           LogLog.error("A \"" + className + "\" object is not assignable to a \"" + superClass.getName() + "\" variable.");
/*     */ 
/* 330 */           LogLog.error("The class \"" + superClass.getName() + "\" was loaded by ");
/* 331 */           LogLog.error("[" + superClass.getClassLoader() + "] whereas object of type ");
/* 332 */           LogLog.error("\"" + classObj.getName() + "\" was loaded by [" + classObj.getClassLoader() + "].");
/*     */ 
/* 334 */           return defaultValue;
/*     */         }
/* 336 */         return classObj.newInstance();
/*     */       } catch (ClassNotFoundException e) {
/* 338 */         LogLog.error("Could not instantiate class [" + className + "].", e);
/*     */       } catch (IllegalAccessException e) {
/* 340 */         LogLog.error("Could not instantiate class [" + className + "].", e);
/*     */       } catch (InstantiationException e) {
/* 342 */         LogLog.error("Could not instantiate class [" + className + "].", e);
/*     */       } catch (RuntimeException e) {
/* 344 */         LogLog.error("Could not instantiate class [" + className + "].", e);
/*     */       }
/*     */     }
/* 347 */     return defaultValue;
/*     */   }
/*     */ 
/*     */   public static String substVars(String val, Properties props)
/*     */     throws IllegalArgumentException
/*     */   {
/* 391 */     StringBuffer sbuf = new StringBuffer();
/*     */ 
/* 393 */     int i = 0;
/*     */     while (true)
/*     */     {
/* 397 */       int j = val.indexOf(DELIM_START, i);
/* 398 */       if (j == -1)
/*     */       {
/* 400 */         if (i == 0) {
/* 401 */           return val;
/*     */         }
/* 403 */         sbuf.append(val.substring(i, val.length()));
/* 404 */         return sbuf.toString();
/*     */       }
/*     */ 
/* 407 */       sbuf.append(val.substring(i, j));
/* 408 */       int k = val.indexOf(DELIM_STOP, j);
/* 409 */       if (k == -1) {
/* 410 */         throw new IllegalArgumentException('"' + val + "\" has no closing brace. Opening brace at position " + j + '.');
/*     */       }
/*     */ 
/* 414 */       j += DELIM_START_LEN;
/* 415 */       String key = val.substring(j, k);
/*     */ 
/* 417 */       String replacement = getSystemProperty(key, null);
/*     */ 
/* 419 */       if ((replacement == null) && (props != null)) {
/* 420 */         replacement = props.getProperty(key);
/*     */       }
/*     */ 
/* 423 */       if (replacement != null)
/*     */       {
/* 429 */         String recursiveReplacement = substVars(replacement, props);
/* 430 */         sbuf.append(recursiveReplacement);
/*     */       }
/* 432 */       i = k + DELIM_STOP_LEN;
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void selectAndConfigure(URL url, String clazz, LoggerRepository hierarchy)
/*     */   {
/* 463 */     Configurator configurator = null;
/* 464 */     String filename = url.getFile();
/*     */ 
/* 466 */     if ((clazz == null) && (filename != null) && (filename.endsWith(".xml"))) {
/* 467 */       clazz = "com.jd.ump.log4j.xml.DOMConfigurator";
/*     */     }
/*     */ 
/* 470 */     if (clazz != null) {
/* 471 */       LogLog.debug("Preferred configurator class: " + clazz);
/* 472 */       configurator = (Configurator)instantiateByClassName(clazz, Configurator.class, null);
/*     */ 
/* 475 */       if (configurator == null)
/* 476 */         LogLog.error("Could not instantiate configurator [" + clazz + "].");
/*     */     }
/*     */     else
/*     */     {
/* 480 */       configurator = new PropertyConfigurator();
/*     */     }
/*     */ 
/* 483 */     configurator.doConfigure(url, hierarchy);
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.helpers.OptionConverter
 * JD-Core Version:    0.6.2
 */