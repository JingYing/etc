/*    */ package com.jd.ump.log4j.or.sax;
/*    */ 
/*    */ import com.jd.ump.log4j.or.ObjectRenderer;
/*    */ import org.xml.sax.Attributes;
/*    */ 
/*    */ public class AttributesRenderer
/*    */   implements ObjectRenderer
/*    */ {
/*    */   public String doRender(Object o)
/*    */   {
/* 42 */     if ((o instanceof Attributes)) {
/* 43 */       StringBuffer sbuf = new StringBuffer();
/* 44 */       Attributes a = (Attributes)o;
/* 45 */       int len = a.getLength();
/* 46 */       boolean first = true;
/* 47 */       for (int i = 0; i < len; i++) {
/* 48 */         if (first)
/* 49 */           first = false;
/*    */         else {
/* 51 */           sbuf.append(", ");
/*    */         }
/* 53 */         sbuf.append(a.getQName(i));
/* 54 */         sbuf.append('=');
/* 55 */         sbuf.append(a.getValue(i));
/*    */       }
/* 57 */       return sbuf.toString();
/*    */     }
/*    */     try {
/* 60 */       return o.toString();
/*    */     } catch (Exception ex) {
/* 62 */       return ex.toString();
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.or.sax.AttributesRenderer
 * JD-Core Version:    0.6.2
 */