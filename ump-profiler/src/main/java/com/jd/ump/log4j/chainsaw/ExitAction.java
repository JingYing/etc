/*    */ package com.jd.ump.log4j.chainsaw;
/*    */ 
/*    */ import com.jd.ump.log4j.Logger;
/*    */ import java.awt.event.ActionEvent;
/*    */ import javax.swing.AbstractAction;
/*    */ 
/*    */ class ExitAction extends AbstractAction
/*    */ {
/* 34 */   private static final Logger LOG = Logger.getLogger(ExitAction.class);
/*    */ 
/* 36 */   public static final ExitAction INSTANCE = new ExitAction();
/*    */ 
/*    */   public void actionPerformed(ActionEvent aIgnore)
/*    */   {
/* 46 */     LOG.info("shutting down");
/* 47 */     System.exit(0);
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.ExitAction
 * JD-Core Version:    0.6.2
 */