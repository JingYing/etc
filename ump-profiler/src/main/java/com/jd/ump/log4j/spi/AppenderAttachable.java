package com.jd.ump.log4j.spi;

import com.jd.ump.log4j.Appender;
import java.util.Enumeration;

public abstract interface AppenderAttachable
{
  public abstract void addAppender(Appender paramAppender);

  public abstract Enumeration getAllAppenders();

  public abstract Appender getAppender(String paramString);

  public abstract boolean isAttached(Appender paramAppender);

  public abstract void removeAllAppenders();

  public abstract void removeAppender(Appender paramAppender);

  public abstract void removeAppender(String paramString);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.AppenderAttachable
 * JD-Core Version:    0.6.2
 */