/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import com.jd.ump.log4j.helpers.Loader;
/*     */ import com.jd.ump.log4j.helpers.ThreadLocalMap;
/*     */ import java.util.Hashtable;
/*     */ 
/*     */ public class MDC
/*     */ {
/*  46 */   static final MDC mdc = new MDC();
/*     */   static final int HT_SIZE = 7;
/*     */   boolean java1;
/*     */   Object tlm;
/*     */ 
/*     */   private MDC()
/*     */   {
/*  56 */     this.java1 = Loader.isJava1();
/*  57 */     if (!this.java1)
/*  58 */       this.tlm = new ThreadLocalMap();
/*     */   }
/*     */ 
/*     */   public static void put(String key, Object o)
/*     */   {
/*  74 */     if (mdc != null)
/*  75 */       mdc.put0(key, o);
/*     */   }
/*     */ 
/*     */   public static Object get(String key)
/*     */   {
/*  87 */     if (mdc != null) {
/*  88 */       return mdc.get0(key);
/*     */     }
/*  90 */     return null;
/*     */   }
/*     */ 
/*     */   public static void remove(String key)
/*     */   {
/* 101 */     if (mdc != null)
/* 102 */       mdc.remove0(key);
/*     */   }
/*     */ 
/*     */   public static Hashtable getContext()
/*     */   {
/* 112 */     if (mdc != null) {
/* 113 */       return mdc.getContext0();
/*     */     }
/* 115 */     return null;
/*     */   }
/*     */ 
/*     */   public static void clear()
/*     */   {
/* 124 */     if (mdc != null)
/* 125 */       mdc.clear0();
/*     */   }
/*     */ 
/*     */   private void put0(String key, Object o)
/*     */   {
/* 132 */     if ((this.java1) || (this.tlm == null)) {
/* 133 */       return;
/*     */     }
/* 135 */     Hashtable ht = (Hashtable)((ThreadLocalMap)this.tlm).get();
/* 136 */     if (ht == null) {
/* 137 */       ht = new Hashtable(7);
/* 138 */       ((ThreadLocalMap)this.tlm).set(ht);
/*     */     }
/* 140 */     ht.put(key, o);
/*     */   }
/*     */ 
/*     */   private Object get0(String key)
/*     */   {
/* 146 */     if ((this.java1) || (this.tlm == null)) {
/* 147 */       return null;
/*     */     }
/* 149 */     Hashtable ht = (Hashtable)((ThreadLocalMap)this.tlm).get();
/* 150 */     if ((ht != null) && (key != null)) {
/* 151 */       return ht.get(key);
/*     */     }
/* 153 */     return null;
/*     */   }
/*     */ 
/*     */   private void remove0(String key)
/*     */   {
/* 160 */     if ((!this.java1) && (this.tlm != null)) {
/* 161 */       Hashtable ht = (Hashtable)((ThreadLocalMap)this.tlm).get();
/* 162 */       if (ht != null)
/* 163 */         ht.remove(key);
/*     */     }
/*     */   }
/*     */ 
/*     */   private Hashtable getContext0()
/*     */   {
/* 171 */     if ((this.java1) || (this.tlm == null)) {
/* 172 */       return null;
/*     */     }
/* 174 */     return (Hashtable)((ThreadLocalMap)this.tlm).get();
/*     */   }
/*     */ 
/*     */   private void clear0()
/*     */   {
/* 180 */     if ((!this.java1) && (this.tlm != null)) {
/* 181 */       Hashtable ht = (Hashtable)((ThreadLocalMap)this.tlm).get();
/* 182 */       if (ht != null)
/* 183 */         ht.clear();
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.MDC
 * JD-Core Version:    0.6.2
 */