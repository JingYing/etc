/*    */ package com.jd.ump.log4j.pattern;
/*    */ 
/*    */ import com.jd.ump.log4j.helpers.PatternConverter;
/*    */ import com.jd.ump.log4j.helpers.PatternParser;
/*    */ 
/*    */ public final class BridgePatternParser extends PatternParser
/*    */ {
/*    */   public BridgePatternParser(String conversionPattern)
/*    */   {
/* 38 */     super(conversionPattern);
/*    */   }
/*    */ 
/*    */   public PatternConverter parse()
/*    */   {
/* 46 */     return new BridgePatternConverter(this.pattern);
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.pattern.BridgePatternParser
 * JD-Core Version:    0.6.2
 */