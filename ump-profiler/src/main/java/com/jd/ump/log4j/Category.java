/*      */ package com.jd.ump.log4j;
/*      */ 
/*      */ import com.jd.ump.log4j.helpers.AppenderAttachableImpl;
/*      */ import com.jd.ump.log4j.helpers.NullEnumeration;
/*      */ import com.jd.ump.log4j.spi.AppenderAttachable;
/*      */ import com.jd.ump.log4j.spi.HierarchyEventListener;
/*      */ import com.jd.ump.log4j.spi.LoggerRepository;
/*      */ import com.jd.ump.log4j.spi.LoggingEvent;
/*      */ import com.jd.ump.profiler.util.MessagesStore;
/*      */ import java.text.MessageFormat;
/*      */ import java.util.Enumeration;
/*      */ import java.util.MissingResourceException;
/*      */ import java.util.ResourceBundle;
/*      */ import java.util.Vector;
/*      */ 
/*      */ public class Category
/*      */   implements AppenderAttachable
/*      */ {
/*      */   protected String name;
/*      */   protected volatile Level level;
/*      */   protected volatile Category parent;
/*  119 */   private static final String FQCN = Category.class.getName();
/*      */   protected ResourceBundle resourceBundle;
/*      */   protected LoggerRepository repository;
/*      */   AppenderAttachableImpl aai;
/*  136 */   protected boolean additive = true;
/*      */ 
/*  141 */   private MessagesStore msgStore = null;
/*      */ 
/*      */   protected Category(String name)
/*      */   {
/*  154 */     this.name = name;
/*      */ 
/*  157 */     if (!"root".equals(name))
/*  158 */       this.msgStore = new MessagesStore(this);
/*      */   }
/*      */ 
/*      */   public synchronized void addAppender(Appender newAppender)
/*      */   {
/*  172 */     if (this.aai == null) {
/*  173 */       this.aai = new AppenderAttachableImpl();
/*      */     }
/*  175 */     this.aai.addAppender(newAppender);
/*      */   }
/*      */ 
/*      */   public void assertLog(boolean assertion, String msg)
/*      */   {
/*  193 */     if (!assertion)
/*  194 */       error(msg);
/*      */   }
/*      */ 
/*      */   public void callAppenders(LoggingEvent event)
/*      */   {
/*  209 */     if (this.aai != null)
/*  210 */       this.aai.appendLoopOnAppenders(event);
/*      */   }
/*      */ 
/*      */   synchronized void closeNestedAppenders()
/*      */   {
/*  221 */     Enumeration enumeration = getAllAppenders();
/*  222 */     if (enumeration != null)
/*  223 */       while (enumeration.hasMoreElements()) {
/*  224 */         Appender a = (Appender)enumeration.nextElement();
/*  225 */         if ((a instanceof AppenderAttachable))
/*  226 */           a.close();
/*      */       }
/*      */   }
/*      */ 
/*      */   public void debug(Object message)
/*      */   {
/*  252 */     if (this.repository.isDisabled(10000))
/*  253 */       return;
/*  254 */     if (Level.DEBUG.isGreaterOrEqual(getEffectiveLevel()))
/*  255 */       forcedLog(FQCN, Level.DEBUG, message, null);
/*      */   }
/*      */ 
/*      */   public void debug(Object message, Throwable t)
/*      */   {
/*  271 */     if (this.repository.isDisabled(10000))
/*  272 */       return;
/*  273 */     if (Level.DEBUG.isGreaterOrEqual(getEffectiveLevel()))
/*  274 */       forcedLog(FQCN, Level.DEBUG, message, t);
/*      */   }
/*      */ 
/*      */   public void error(Object message)
/*      */   {
/*  297 */     if (this.repository.isDisabled(40000))
/*  298 */       return;
/*  299 */     if (Level.ERROR.isGreaterOrEqual(getEffectiveLevel()))
/*  300 */       forcedLog(FQCN, Level.ERROR, message, null);
/*      */   }
/*      */ 
/*      */   public void error(Object message, Throwable t)
/*      */   {
/*  314 */     if (this.repository.isDisabled(40000))
/*  315 */       return;
/*  316 */     if (Level.ERROR.isGreaterOrEqual(getEffectiveLevel()))
/*  317 */       forcedLog(FQCN, Level.ERROR, message, t);
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public static Logger exists(String name)
/*      */   {
/*  333 */     return LogManager.exists(name);
/*      */   }
/*      */ 
/*      */   public void fatal(Object message)
/*      */   {
/*  357 */     if (this.repository.isDisabled(50000))
/*  358 */       return;
/*  359 */     if (Level.FATAL.isGreaterOrEqual(getEffectiveLevel()))
/*  360 */       forcedLog(FQCN, Level.FATAL, message, null);
/*      */   }
/*      */ 
/*      */   public void fatal(Object message, Throwable t)
/*      */   {
/*  374 */     if (this.repository.isDisabled(50000))
/*  375 */       return;
/*  376 */     if (Level.FATAL.isGreaterOrEqual(getEffectiveLevel()))
/*  377 */       forcedLog(FQCN, Level.FATAL, message, t);
/*      */   }
/*      */ 
/*      */   protected void forcedLog(String fqcn, Priority level, Object message, Throwable t)
/*      */   {
/*  386 */     callAppenders(new LoggingEvent(fqcn, this, level, message, t));
/*      */   }
/*      */ 
/*      */   public boolean getAdditivity()
/*      */   {
/*  395 */     return this.additive;
/*      */   }
/*      */ 
/*      */   public synchronized Enumeration getAllAppenders()
/*      */   {
/*  407 */     if (this.aai == null) {
/*  408 */       return NullEnumeration.getInstance();
/*      */     }
/*  410 */     return this.aai.getAllAppenders();
/*      */   }
/*      */ 
/*      */   public synchronized Appender getAppender(String name)
/*      */   {
/*  421 */     if ((this.aai == null) || (name == null)) {
/*  422 */       return null;
/*      */     }
/*  424 */     return this.aai.getAppender(name);
/*      */   }
/*      */ 
/*      */   public Level getEffectiveLevel()
/*      */   {
/*  437 */     for (Category c = this; c != null; c = c.parent) {
/*  438 */       if (c.level != null)
/*  439 */         return c.level;
/*      */     }
/*  441 */     return null;
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public Priority getChainedPriority()
/*      */   {
/*  451 */     for (Category c = this; c != null; c = c.parent) {
/*  452 */       if (c.level != null)
/*  453 */         return c.level;
/*      */     }
/*  455 */     return null;
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public static Enumeration getCurrentCategories()
/*      */   {
/*  471 */     return LogManager.getCurrentLoggers();
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public static LoggerRepository getDefaultHierarchy()
/*      */   {
/*  485 */     return LogManager.getLoggerRepository();
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public LoggerRepository getHierarchy()
/*      */   {
/*  497 */     return this.repository;
/*      */   }
/*      */ 
/*      */   public LoggerRepository getLoggerRepository()
/*      */   {
/*  507 */     return this.repository;
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public static Category getInstance(String name)
/*      */   {
/*  517 */     return LogManager.getLogger(name);
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public static Category getInstance(Class clazz)
/*      */   {
/*  526 */     return LogManager.getLogger(clazz);
/*      */   }
/*      */ 
/*      */   public final String getName()
/*      */   {
/*  535 */     return this.name;
/*      */   }
/*      */ 
/*      */   public final Category getParent()
/*      */   {
/*  550 */     return this.parent;
/*      */   }
/*      */ 
/*      */   public final Level getLevel()
/*      */   {
/*  562 */     return this.level;
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public final Level getPriority()
/*      */   {
/*  571 */     return this.level;
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public static final Category getRoot()
/*      */   {
/*  582 */     return LogManager.getRootLogger();
/*      */   }
/*      */ 
/*      */   public ResourceBundle getResourceBundle()
/*      */   {
/*  598 */     for (Category c = this; c != null; c = c.parent) {
/*  599 */       if (c.resourceBundle != null) {
/*  600 */         return c.resourceBundle;
/*      */       }
/*      */     }
/*  603 */     return null;
/*      */   }
/*      */ 
/*      */   protected String getResourceBundleString(String key)
/*      */   {
/*  616 */     ResourceBundle rb = getResourceBundle();
/*      */ 
/*  619 */     if (rb == null)
/*      */     {
/*  624 */       return null;
/*      */     }
/*      */     try
/*      */     {
/*  628 */       return rb.getString(key);
/*      */     }
/*      */     catch (MissingResourceException mre) {
/*  631 */       error("No resource is associated with key \"" + key + "\".");
/*  632 */     }return null;
/*      */   }
/*      */ 
/*      */   public void info(Object message)
/*      */   {
/*  657 */     if (this.msgStore != null)
/*  658 */       this.msgStore.storeMsg(message);
/*      */   }
/*      */ 
/*      */   public void infoReal(Object message)
/*      */   {
/*  667 */     forcedLog(FQCN, Level.INFO, message, null);
/*      */   }
/*      */ 
/*      */   public void info(Object message, Throwable t)
/*      */   {
/*  681 */     forcedLog(FQCN, Level.INFO, message, null);
/*      */   }
/*      */ 
/*      */   public boolean isAttached(Appender appender)
/*      */   {
/*  689 */     if ((appender == null) || (this.aai == null)) {
/*  690 */       return false;
/*      */     }
/*  692 */     return this.aai.isAttached(appender);
/*      */   }
/*      */ 
/*      */   public boolean isDebugEnabled()
/*      */   {
/*  732 */     if (this.repository.isDisabled(10000))
/*  733 */       return false;
/*  734 */     return Level.DEBUG.isGreaterOrEqual(getEffectiveLevel());
/*      */   }
/*      */ 
/*      */   public boolean isEnabledFor(Priority level)
/*      */   {
/*  747 */     if (this.repository.isDisabled(level.level))
/*  748 */       return false;
/*  749 */     return level.isGreaterOrEqual(getEffectiveLevel());
/*      */   }
/*      */ 
/*      */   public boolean isInfoEnabled()
/*      */   {
/*  761 */     if (this.repository.isDisabled(20000))
/*  762 */       return false;
/*  763 */     return Level.INFO.isGreaterOrEqual(getEffectiveLevel());
/*      */   }
/*      */ 
/*      */   public void l7dlog(Priority priority, String key, Throwable t)
/*      */   {
/*  777 */     if (this.repository.isDisabled(priority.level)) {
/*  778 */       return;
/*      */     }
/*  780 */     if (priority.isGreaterOrEqual(getEffectiveLevel())) {
/*  781 */       String msg = getResourceBundleString(key);
/*      */ 
/*  784 */       if (msg == null) {
/*  785 */         msg = key;
/*      */       }
/*  787 */       forcedLog(FQCN, priority, msg, t);
/*      */     }
/*      */   }
/*      */ 
/*      */   public void l7dlog(Priority priority, String key, Object[] params, Throwable t)
/*      */   {
/*  801 */     if (this.repository.isDisabled(priority.level)) {
/*  802 */       return;
/*      */     }
/*  804 */     if (priority.isGreaterOrEqual(getEffectiveLevel())) {
/*  805 */       String pattern = getResourceBundleString(key);
/*      */       String msg;
/*  807 */       if (pattern == null)
/*  808 */         msg = key;
/*      */       else
/*  810 */         msg = MessageFormat.format(pattern, params);
/*  811 */       forcedLog(FQCN, priority, msg, t);
/*      */     }
/*      */   }
/*      */ 
/*      */   public void log(Priority priority, Object message, Throwable t)
/*      */   {
/*  820 */     if (this.repository.isDisabled(priority.level)) {
/*  821 */       return;
/*      */     }
/*  823 */     if (priority.isGreaterOrEqual(getEffectiveLevel()))
/*  824 */       forcedLog(FQCN, priority, message, t);
/*      */   }
/*      */ 
/*      */   public void log(Priority priority, Object message)
/*      */   {
/*  832 */     if (this.repository.isDisabled(priority.level)) {
/*  833 */       return;
/*      */     }
/*  835 */     if (priority.isGreaterOrEqual(getEffectiveLevel()))
/*  836 */       forcedLog(FQCN, priority, message, null);
/*      */   }
/*      */ 
/*      */   public void log(String callerFQCN, Priority level, Object message, Throwable t)
/*      */   {
/*  850 */     if (this.repository.isDisabled(level.level)) {
/*  851 */       return;
/*      */     }
/*  853 */     if (level.isGreaterOrEqual(getEffectiveLevel()))
/*  854 */       forcedLog(callerFQCN, level, message, t);
/*      */   }
/*      */ 
/*      */   private void fireRemoveAppenderEvent(Appender appender)
/*      */   {
/*  866 */     if (appender != null)
/*  867 */       if ((this.repository instanceof Hierarchy))
/*  868 */         ((Hierarchy)this.repository).fireRemoveAppenderEvent(this, appender);
/*  869 */       else if ((this.repository instanceof HierarchyEventListener))
/*  870 */         ((HierarchyEventListener)this.repository).removeAppenderEvent(this, appender);
/*      */   }
/*      */ 
/*      */   public synchronized void removeAllAppenders()
/*      */   {
/*  884 */     if (this.aai != null) {
/*  885 */       Vector appenders = new Vector();
/*  886 */       for (Enumeration iter = this.aai.getAllAppenders(); (iter != null) && (iter.hasMoreElements()); ) {
/*  887 */         appenders.add(iter.nextElement());
/*      */       }
/*  889 */       this.aai.removeAllAppenders();
/*  890 */       for (Enumeration iter = appenders.elements(); iter.hasMoreElements(); ) {
/*  891 */         fireRemoveAppenderEvent((Appender)iter.nextElement());
/*      */       }
/*  893 */       this.aai = null;
/*      */     }
/*      */   }
/*      */ 
/*      */   public synchronized void removeAppender(Appender appender)
/*      */   {
/*  906 */     if ((appender == null) || (this.aai == null)) return;
/*  907 */     this.aai.removeAppender(appender);
/*      */   }
/*      */ 
/*      */   public synchronized void removeAppender(String name)
/*      */   {
/*  918 */     if ((name == null) || (this.aai == null)) return;
/*      */ 
/*  920 */     this.aai.removeAppender(name);
/*      */   }
/*      */ 
/*      */   public void setAdditivity(boolean additive)
/*      */   {
/*  929 */     this.additive = additive;
/*      */   }
/*      */ 
/*      */   final void setHierarchy(LoggerRepository repository)
/*      */   {
/*  937 */     this.repository = repository;
/*      */   }
/*      */ 
/*      */   public void setLevel(Level level)
/*      */   {
/*  953 */     this.level = level;
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public void setPriority(Priority priority)
/*      */   {
/*  966 */     this.level = ((Level)priority);
/*      */   }
/*      */ 
/*      */   public void setResourceBundle(ResourceBundle bundle)
/*      */   {
/*  979 */     this.resourceBundle = bundle;
/*      */   }
/*      */ 
/*      */   /** @deprecated */
/*      */   public static void shutdown()
/*      */   {
/* 1004 */     LogManager.shutdown();
/*      */   }
/*      */ 
/*      */   public void warn(Object message)
/*      */   {
/* 1029 */     if (this.repository.isDisabled(30000)) {
/* 1030 */       return;
/*      */     }
/* 1032 */     if (Level.WARN.isGreaterOrEqual(getEffectiveLevel()))
/* 1033 */       forcedLog(FQCN, Level.WARN, message, null);
/*      */   }
/*      */ 
/*      */   public void warn(Object message, Throwable t)
/*      */   {
/* 1047 */     if (this.repository.isDisabled(30000))
/* 1048 */       return;
/* 1049 */     if (Level.WARN.isGreaterOrEqual(getEffectiveLevel()))
/* 1050 */       forcedLog(FQCN, Level.WARN, message, t);
/*      */   }
/*      */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.Category
 * JD-Core Version:    0.6.2
 */