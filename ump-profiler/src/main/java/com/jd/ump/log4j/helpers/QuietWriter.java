/*    */ package com.jd.ump.log4j.helpers;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.ErrorHandler;
/*    */ import java.io.FilterWriter;
/*    */ import java.io.Writer;
/*    */ 
/*    */ public class QuietWriter extends FilterWriter
/*    */ {
/*    */   protected ErrorHandler errorHandler;
/*    */ 
/*    */   public QuietWriter(Writer writer, ErrorHandler errorHandler)
/*    */   {
/* 41 */     super(writer);
/* 42 */     setErrorHandler(errorHandler);
/*    */   }
/*    */ 
/*    */   public void write(String string)
/*    */   {
/* 47 */     if (string != null)
/*    */       try {
/* 49 */         this.out.write(string);
/*    */       } catch (Exception e) {
/* 51 */         this.errorHandler.error("Failed to write [" + string + "].", e, 1);
/*    */       }
/*    */   }
/*    */ 
/*    */   public void flush()
/*    */   {
/*    */     try
/*    */     {
/* 60 */       this.out.flush();
/*    */     } catch (Exception e) {
/* 62 */       this.errorHandler.error("Failed to flush writer,", e, 2);
/*    */     }
/*    */   }
/*    */ 
/*    */   public void setErrorHandler(ErrorHandler eh)
/*    */   {
/* 70 */     if (eh == null)
/*    */     {
/* 72 */       throw new IllegalArgumentException("Attempted to set null ErrorHandler.");
/*    */     }
/* 74 */     this.errorHandler = eh;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.helpers.QuietWriter
 * JD-Core Version:    0.6.2
 */