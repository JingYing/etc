/*     */ package com.jd.ump.log4j.chainsaw;
/*     */ 
/*     */ import com.jd.ump.log4j.Level;
/*     */ import java.util.StringTokenizer;
/*     */ import org.xml.sax.Attributes;
/*     */ import org.xml.sax.SAXException;
/*     */ import org.xml.sax.helpers.DefaultHandler;
/*     */ 
/*     */ class XMLFileHandler extends DefaultHandler
/*     */ {
/*     */   private static final String TAG_EVENT = "log4j:event";
/*     */   private static final String TAG_MESSAGE = "log4j:message";
/*     */   private static final String TAG_NDC = "log4j:NDC";
/*     */   private static final String TAG_THROWABLE = "log4j:throwable";
/*     */   private static final String TAG_LOCATION_INFO = "log4j:locationInfo";
/*     */   private final MyTableModel mModel;
/*     */   private int mNumEvents;
/*     */   private long mTimeStamp;
/*     */   private Level mLevel;
/*     */   private String mCategoryName;
/*     */   private String mNDC;
/*     */   private String mThreadName;
/*     */   private String mMessage;
/*     */   private String[] mThrowableStrRep;
/*     */   private String mLocationDetails;
/*  69 */   private final StringBuffer mBuf = new StringBuffer();
/*     */ 
/*     */   XMLFileHandler(MyTableModel aModel)
/*     */   {
/*  77 */     this.mModel = aModel;
/*     */   }
/*     */ 
/*     */   public void startDocument()
/*     */     throws SAXException
/*     */   {
/*  84 */     this.mNumEvents = 0;
/*     */   }
/*     */ 
/*     */   public void characters(char[] aChars, int aStart, int aLength)
/*     */   {
/*  89 */     this.mBuf.append(String.valueOf(aChars, aStart, aLength));
/*     */   }
/*     */ 
/*     */   public void endElement(String aNamespaceURI, String aLocalName, String aQName)
/*     */   {
/*  97 */     if ("log4j:event".equals(aQName)) {
/*  98 */       addEvent();
/*  99 */       resetData();
/* 100 */     } else if ("log4j:NDC".equals(aQName)) {
/* 101 */       this.mNDC = this.mBuf.toString();
/* 102 */     } else if ("log4j:message".equals(aQName)) {
/* 103 */       this.mMessage = this.mBuf.toString();
/* 104 */     } else if ("log4j:throwable".equals(aQName)) {
/* 105 */       StringTokenizer st = new StringTokenizer(this.mBuf.toString(), "\n\t");
/*     */ 
/* 107 */       this.mThrowableStrRep = new String[st.countTokens()];
/* 108 */       if (this.mThrowableStrRep.length > 0) {
/* 109 */         this.mThrowableStrRep[0] = st.nextToken();
/* 110 */         for (int i = 1; i < this.mThrowableStrRep.length; i++)
/* 111 */           this.mThrowableStrRep[i] = ("\t" + st.nextToken());
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   public void startElement(String aNamespaceURI, String aLocalName, String aQName, Attributes aAtts)
/*     */   {
/* 123 */     this.mBuf.setLength(0);
/*     */ 
/* 125 */     if ("log4j:event".equals(aQName)) {
/* 126 */       this.mThreadName = aAtts.getValue("thread");
/* 127 */       this.mTimeStamp = Long.parseLong(aAtts.getValue("timestamp"));
/* 128 */       this.mCategoryName = aAtts.getValue("logger");
/* 129 */       this.mLevel = Level.toLevel(aAtts.getValue("level"));
/* 130 */     } else if ("log4j:locationInfo".equals(aQName)) {
/* 131 */       this.mLocationDetails = (aAtts.getValue("class") + "." + aAtts.getValue("method") + "(" + aAtts.getValue("file") + ":" + aAtts.getValue("line") + ")");
/*     */     }
/*     */   }
/*     */ 
/*     */   int getNumEvents()
/*     */   {
/* 140 */     return this.mNumEvents;
/*     */   }
/*     */ 
/*     */   private void addEvent()
/*     */   {
/* 149 */     this.mModel.addEvent(new EventDetails(this.mTimeStamp, this.mLevel, this.mCategoryName, this.mNDC, this.mThreadName, this.mMessage, this.mThrowableStrRep, this.mLocationDetails));
/*     */ 
/* 157 */     this.mNumEvents += 1;
/*     */   }
/*     */ 
/*     */   private void resetData()
/*     */   {
/* 162 */     this.mTimeStamp = 0L;
/* 163 */     this.mLevel = null;
/* 164 */     this.mCategoryName = null;
/* 165 */     this.mNDC = null;
/* 166 */     this.mThreadName = null;
/* 167 */     this.mMessage = null;
/* 168 */     this.mThrowableStrRep = null;
/* 169 */     this.mLocationDetails = null;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.XMLFileHandler
 * JD-Core Version:    0.6.2
 */