/*    */ package com.jd.ump.log4j.lf5;
/*    */ 
/*    */ import com.jd.ump.log4j.lf5.viewer.LogBrokerMonitor;
/*    */ import java.io.PrintStream;
/*    */ 
/*    */ public class AppenderFinalizer
/*    */ {
/* 41 */   protected LogBrokerMonitor _defaultMonitor = null;
/*    */ 
/*    */   public AppenderFinalizer(LogBrokerMonitor defaultMonitor)
/*    */   {
/* 52 */     this._defaultMonitor = defaultMonitor;
/*    */   }
/*    */ 
/*    */   protected void finalize()
/*    */     throws Throwable
/*    */   {
/* 66 */     System.out.println("Disposing of the default LogBrokerMonitor instance");
/* 67 */     this._defaultMonitor.dispose();
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.AppenderFinalizer
 * JD-Core Version:    0.6.2
 */