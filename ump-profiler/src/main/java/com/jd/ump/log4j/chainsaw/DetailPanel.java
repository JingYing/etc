/*     */ package com.jd.ump.log4j.chainsaw;
/*     */ 
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import java.awt.BorderLayout;
/*     */ import java.text.MessageFormat;
/*     */ import java.util.Date;
/*     */ import javax.swing.BorderFactory;
/*     */ import javax.swing.JEditorPane;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JScrollPane;
/*     */ import javax.swing.JTable;
/*     */ import javax.swing.ListSelectionModel;
/*     */ import javax.swing.event.ListSelectionEvent;
/*     */ import javax.swing.event.ListSelectionListener;
/*     */ 
/*     */ class DetailPanel extends JPanel
/*     */   implements ListSelectionListener
/*     */ {
/*  43 */   private static final Logger LOG = Logger.getLogger(DetailPanel.class);
/*     */ 
/*  47 */   private static final MessageFormat FORMATTER = new MessageFormat("<b>Time:</b> <code>{0,time,medium}</code>&nbsp;&nbsp;<b>Priority:</b> <code>{1}</code>&nbsp;&nbsp;<b>Thread:</b> <code>{2}</code>&nbsp;&nbsp;<b>NDC:</b> <code>{3}</code><br><b>Logger:</b> <code>{4}</code><br><b>Location:</b> <code>{5}</code><br><b>Message:</b><pre>{6}</pre><b>Throwable:</b><pre>{7}</pre>");
/*     */   private final MyTableModel mModel;
/*     */   private final JEditorPane mDetails;
/*     */ 
/*     */   DetailPanel(JTable aTable, MyTableModel aModel)
/*     */   {
/*  71 */     this.mModel = aModel;
/*  72 */     setLayout(new BorderLayout());
/*  73 */     setBorder(BorderFactory.createTitledBorder("Details: "));
/*     */ 
/*  75 */     this.mDetails = new JEditorPane();
/*  76 */     this.mDetails.setEditable(false);
/*  77 */     this.mDetails.setContentType("text/html");
/*  78 */     add(new JScrollPane(this.mDetails), "Center");
/*     */ 
/*  80 */     ListSelectionModel rowSM = aTable.getSelectionModel();
/*  81 */     rowSM.addListSelectionListener(this);
/*     */   }
/*     */ 
/*     */   public void valueChanged(ListSelectionEvent aEvent)
/*     */   {
/*  87 */     if (aEvent.getValueIsAdjusting()) {
/*  88 */       return;
/*     */     }
/*     */ 
/*  91 */     ListSelectionModel lsm = (ListSelectionModel)aEvent.getSource();
/*  92 */     if (lsm.isSelectionEmpty()) {
/*  93 */       this.mDetails.setText("Nothing selected");
/*     */     } else {
/*  95 */       int selectedRow = lsm.getMinSelectionIndex();
/*  96 */       EventDetails e = this.mModel.getEventDetails(selectedRow);
/*  97 */       Object[] args = { new Date(e.getTimeStamp()), e.getPriority(), escape(e.getThreadName()), escape(e.getNDC()), escape(e.getCategoryName()), escape(e.getLocationDetails()), escape(e.getMessage()), escape(getThrowableStrRep(e)) };
/*     */ 
/* 108 */       this.mDetails.setText(FORMATTER.format(args));
/* 109 */       this.mDetails.setCaretPosition(0);
/*     */     }
/*     */   }
/*     */ 
/*     */   private static String getThrowableStrRep(EventDetails aEvent)
/*     */   {
/* 124 */     String[] strs = aEvent.getThrowableStrRep();
/* 125 */     if (strs == null) {
/* 126 */       return null;
/*     */     }
/*     */ 
/* 129 */     StringBuffer sb = new StringBuffer();
/* 130 */     for (int i = 0; i < strs.length; i++) {
/* 131 */       sb.append(strs[i]).append("\n");
/*     */     }
/*     */ 
/* 134 */     return sb.toString();
/*     */   }
/*     */ 
/*     */   private String escape(String aStr)
/*     */   {
/* 144 */     if (aStr == null) {
/* 145 */       return null;
/*     */     }
/*     */ 
/* 148 */     StringBuffer buf = new StringBuffer();
/* 149 */     for (int i = 0; i < aStr.length(); i++) {
/* 150 */       char c = aStr.charAt(i);
/* 151 */       switch (c) {
/*     */       case '<':
/* 153 */         buf.append("&lt;");
/* 154 */         break;
/*     */       case '>':
/* 156 */         buf.append("&gt;");
/* 157 */         break;
/*     */       case '"':
/* 159 */         buf.append("&quot;");
/* 160 */         break;
/*     */       case '&':
/* 162 */         buf.append("&amp;");
/* 163 */         break;
/*     */       default:
/* 165 */         buf.append(c);
/*     */       }
/*     */     }
/*     */ 
/* 169 */     return buf.toString();
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.DetailPanel
 * JD-Core Version:    0.6.2
 */