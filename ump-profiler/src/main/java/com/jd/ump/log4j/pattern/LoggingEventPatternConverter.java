/*    */ package com.jd.ump.log4j.pattern;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ 
/*    */ public abstract class LoggingEventPatternConverter extends PatternConverter
/*    */ {
/*    */   protected LoggingEventPatternConverter(String name, String style)
/*    */   {
/* 38 */     super(name, style);
/*    */   }
/*    */ 
/*    */   public abstract void format(LoggingEvent paramLoggingEvent, StringBuffer paramStringBuffer);
/*    */ 
/*    */   public void format(Object obj, StringBuffer output)
/*    */   {
/* 53 */     if ((obj instanceof LoggingEvent))
/* 54 */       format((LoggingEvent)obj, output);
/*    */   }
/*    */ 
/*    */   public boolean handlesThrowable()
/*    */   {
/* 68 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.pattern.LoggingEventPatternConverter
 * JD-Core Version:    0.6.2
 */