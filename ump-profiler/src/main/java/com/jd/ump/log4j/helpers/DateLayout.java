/*     */ package com.jd.ump.log4j.helpers;
/*     */ 
/*     */ import com.jd.ump.log4j.Layout;
/*     */ import com.jd.ump.log4j.spi.LoggingEvent;
/*     */ import java.text.DateFormat;
/*     */ import java.text.FieldPosition;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.TimeZone;
/*     */ 
/*     */ public abstract class DateLayout extends Layout
/*     */ {
/*     */   public static final String NULL_DATE_FORMAT = "NULL";
/*     */   public static final String RELATIVE_TIME_DATE_FORMAT = "RELATIVE";
/*  52 */   protected FieldPosition pos = new FieldPosition(0);
/*     */ 
/*     */   /** @deprecated */
/*     */   public static final String DATE_FORMAT_OPTION = "DateFormat";
/*     */ 
/*     */   /** @deprecated */
/*     */   public static final String TIMEZONE_OPTION = "TimeZone";
/*     */   private String timeZoneID;
/*     */   private String dateFormatOption;
/*     */   protected DateFormat dateFormat;
/*  72 */   protected Date date = new Date();
/*     */ 
/*     */   /** @deprecated */
/*     */   public String[] getOptionStrings()
/*     */   {
/*  80 */     return new String[] { "DateFormat", "TimeZone" };
/*     */   }
/*     */ 
/*     */   /** @deprecated */
/*     */   public void setOption(String option, String value)
/*     */   {
/*  89 */     if (option.equalsIgnoreCase("DateFormat"))
/*  90 */       this.dateFormatOption = value.toUpperCase();
/*  91 */     else if (option.equalsIgnoreCase("TimeZone"))
/*  92 */       this.timeZoneID = value;
/*     */   }
/*     */ 
/*     */   public void setDateFormat(String dateFormat)
/*     */   {
/* 104 */     if (dateFormat != null) {
/* 105 */       this.dateFormatOption = dateFormat;
/*     */     }
/* 107 */     setDateFormat(this.dateFormatOption, TimeZone.getDefault());
/*     */   }
/*     */ 
/*     */   public String getDateFormat()
/*     */   {
/* 115 */     return this.dateFormatOption;
/*     */   }
/*     */ 
/*     */   public void setTimeZone(String timeZone)
/*     */   {
/* 124 */     this.timeZoneID = timeZone;
/*     */   }
/*     */ 
/*     */   public String getTimeZone()
/*     */   {
/* 132 */     return this.timeZoneID;
/*     */   }
/*     */ 
/*     */   public void activateOptions()
/*     */   {
/* 137 */     setDateFormat(this.dateFormatOption);
/* 138 */     if ((this.timeZoneID != null) && (this.dateFormat != null))
/* 139 */       this.dateFormat.setTimeZone(TimeZone.getTimeZone(this.timeZoneID));
/*     */   }
/*     */ 
/*     */   public void dateFormat(StringBuffer buf, LoggingEvent event)
/*     */   {
/* 145 */     if (this.dateFormat != null) {
/* 146 */       this.date.setTime(event.timeStamp);
/* 147 */       this.dateFormat.format(this.date, buf, this.pos);
/* 148 */       buf.append(' ');
/*     */     }
/*     */   }
/*     */ 
/*     */   public void setDateFormat(DateFormat dateFormat, TimeZone timeZone)
/*     */   {
/* 158 */     this.dateFormat = dateFormat;
/* 159 */     this.dateFormat.setTimeZone(timeZone);
/*     */   }
/*     */ 
/*     */   public void setDateFormat(String dateFormatType, TimeZone timeZone)
/*     */   {
/* 178 */     if (dateFormatType == null) {
/* 179 */       this.dateFormat = null;
/* 180 */       return;
/*     */     }
/*     */ 
/* 183 */     if (dateFormatType.equalsIgnoreCase("NULL")) {
/* 184 */       this.dateFormat = null;
/* 185 */     } else if (dateFormatType.equalsIgnoreCase("RELATIVE")) {
/* 186 */       this.dateFormat = new RelativeTimeDateFormat();
/* 187 */     } else if (dateFormatType.equalsIgnoreCase("ABSOLUTE"))
/*     */     {
/* 189 */       this.dateFormat = new AbsoluteTimeDateFormat(timeZone);
/* 190 */     } else if (dateFormatType.equalsIgnoreCase("DATE"))
/*     */     {
/* 192 */       this.dateFormat = new DateTimeDateFormat(timeZone);
/* 193 */     } else if (dateFormatType.equalsIgnoreCase("ISO8601"))
/*     */     {
/* 195 */       this.dateFormat = new ISO8601DateFormat(timeZone);
/*     */     } else {
/* 197 */       this.dateFormat = new SimpleDateFormat(dateFormatType);
/* 198 */       this.dateFormat.setTimeZone(timeZone);
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.helpers.DateLayout
 * JD-Core Version:    0.6.2
 */