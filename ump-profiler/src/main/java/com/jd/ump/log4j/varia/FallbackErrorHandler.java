/*     */ package com.jd.ump.log4j.varia;
/*     */ 
/*     */ import com.jd.ump.log4j.Appender;
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import com.jd.ump.log4j.helpers.LogLog;
/*     */ import com.jd.ump.log4j.spi.ErrorHandler;
/*     */ import com.jd.ump.log4j.spi.LoggingEvent;
/*     */ import java.io.InterruptedIOException;
/*     */ import java.util.Vector;
/*     */ 
/*     */ public class FallbackErrorHandler
/*     */   implements ErrorHandler
/*     */ {
/*     */   Appender backup;
/*     */   Appender primary;
/*     */   Vector loggers;
/*     */ 
/*     */   public void setLogger(Logger logger)
/*     */   {
/*  58 */     LogLog.debug("FB: Adding logger [" + logger.getName() + "].");
/*  59 */     if (this.loggers == null) {
/*  60 */       this.loggers = new Vector();
/*     */     }
/*  62 */     this.loggers.addElement(logger);
/*     */   }
/*     */ 
/*     */   public void activateOptions()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void error(String message, Exception e, int errorCode)
/*     */   {
/*  79 */     error(message, e, errorCode, null);
/*     */   }
/*     */ 
/*     */   public void error(String message, Exception e, int errorCode, LoggingEvent event)
/*     */   {
/*  88 */     if ((e instanceof InterruptedIOException)) {
/*  89 */       Thread.currentThread().interrupt();
/*     */     }
/*  91 */     LogLog.debug("FB: The following error reported: " + message, e);
/*  92 */     LogLog.debug("FB: INITIATING FALLBACK PROCEDURE.");
/*  93 */     if (this.loggers != null)
/*  94 */       for (int i = 0; i < this.loggers.size(); i++) {
/*  95 */         Logger l = (Logger)this.loggers.elementAt(i);
/*  96 */         LogLog.debug("FB: Searching for [" + this.primary.getName() + "] in logger [" + l.getName() + "].");
/*     */ 
/*  98 */         LogLog.debug("FB: Replacing [" + this.primary.getName() + "] by [" + this.backup.getName() + "] in logger [" + l.getName() + "].");
/*     */ 
/* 100 */         l.removeAppender(this.primary);
/* 101 */         LogLog.debug("FB: Adding appender [" + this.backup.getName() + "] to logger " + l.getName());
/*     */ 
/* 103 */         l.addAppender(this.backup);
/*     */       }
/*     */   }
/*     */ 
/*     */   public void error(String message)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void setAppender(Appender primary)
/*     */   {
/* 126 */     LogLog.debug("FB: Setting primary appender to [" + primary.getName() + "].");
/* 127 */     this.primary = primary;
/*     */   }
/*     */ 
/*     */   public void setBackupAppender(Appender backup)
/*     */   {
/* 135 */     LogLog.debug("FB: Setting backup appender to [" + backup.getName() + "].");
/* 136 */     this.backup = backup;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.varia.FallbackErrorHandler
 * JD-Core Version:    0.6.2
 */