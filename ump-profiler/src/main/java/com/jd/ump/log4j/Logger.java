/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import com.jd.ump.log4j.spi.LoggerFactory;
/*     */ import com.jd.ump.log4j.spi.LoggerRepository;
/*     */ 
/*     */ @Deprecated
/*     */ public class Logger extends Category
/*     */ {
/*  34 */   private static final String FQCN = Logger.class.getName();
/*     */ 
/*     */   protected Logger(String name)
/*     */   {
/*  39 */     super(name);
/*     */   }
/*     */ 
/*     */   public static Logger getLogger(String name)
/*     */   {
/* 103 */     return LogManager.getLogger(name);
/*     */   }
/*     */ 
/*     */   public static Logger getLogger(Class clazz)
/*     */   {
/* 116 */     return LogManager.getLogger(clazz.getName());
/*     */   }
/*     */ 
/*     */   public static Logger getRootLogger()
/*     */   {
/* 134 */     return LogManager.getRootLogger();
/*     */   }
/*     */ 
/*     */   public static Logger getLogger(String name, LoggerFactory factory)
/*     */   {
/* 154 */     return LogManager.getLogger(name, factory);
/*     */   }
/*     */ 
/*     */   public void trace(Object message)
/*     */   {
/* 165 */     if (this.repository.isDisabled(5000)) {
/* 166 */       return;
/*     */     }
/*     */ 
/* 169 */     if (Level.TRACE.isGreaterOrEqual(getEffectiveLevel()))
/* 170 */       forcedLog(FQCN, Level.TRACE, message, null);
/*     */   }
/*     */ 
/*     */   public void trace(Object message, Throwable t)
/*     */   {
/* 187 */     if (this.repository.isDisabled(5000)) {
/* 188 */       return;
/*     */     }
/*     */ 
/* 191 */     if (Level.TRACE.isGreaterOrEqual(getEffectiveLevel()))
/* 192 */       forcedLog(FQCN, Level.TRACE, message, t);
/*     */   }
/*     */ 
/*     */   public boolean isTraceEnabled()
/*     */   {
/* 204 */     if (this.repository.isDisabled(5000)) {
/* 205 */       return false;
/*     */     }
/*     */ 
/* 208 */     return Level.TRACE.isGreaterOrEqual(getEffectiveLevel());
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.Logger
 * JD-Core Version:    0.6.2
 */