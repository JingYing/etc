package com.jd.ump.log4j.lf5.viewer.categoryexplorer;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

public class TreeModelAdapter
  implements TreeModelListener
{
  public void treeNodesChanged(TreeModelEvent e)
  {
  }

  public void treeNodesInserted(TreeModelEvent e)
  {
  }

  public void treeNodesRemoved(TreeModelEvent e)
  {
  }

  public void treeStructureChanged(TreeModelEvent e)
  {
  }
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.viewer.categoryexplorer.TreeModelAdapter
 * JD-Core Version:    0.6.2
 */