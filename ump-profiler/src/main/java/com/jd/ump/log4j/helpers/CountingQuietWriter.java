/*    */ package com.jd.ump.log4j.helpers;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.ErrorHandler;
/*    */ import java.io.IOException;
/*    */ import java.io.Writer;
/*    */ 
/*    */ public class CountingQuietWriter extends QuietWriter
/*    */ {
/*    */   protected long count;
/*    */ 
/*    */   public CountingQuietWriter(Writer writer, ErrorHandler eh)
/*    */   {
/* 39 */     super(writer, eh);
/*    */   }
/*    */ 
/*    */   public void write(String string)
/*    */   {
/*    */     try {
/* 45 */       this.out.write(string);
/* 46 */       this.count += string.length();
/*    */     }
/*    */     catch (IOException e) {
/* 49 */       this.errorHandler.error("Write failure.", e, 1);
/*    */     }
/*    */   }
/*    */ 
/*    */   public long getCount()
/*    */   {
/* 55 */     return this.count;
/*    */   }
/*    */ 
/*    */   public void setCount(long count)
/*    */   {
/* 60 */     this.count = count;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.helpers.CountingQuietWriter
 * JD-Core Version:    0.6.2
 */