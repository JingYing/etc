/*    */ package com.jd.ump.log4j.helpers;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.ErrorHandler;
/*    */ import java.io.Writer;
/*    */ 
/*    */ public class SyslogQuietWriter extends QuietWriter
/*    */ {
/*    */   int syslogFacility;
/*    */   int level;
/*    */ 
/*    */   public SyslogQuietWriter(Writer writer, int syslogFacility, ErrorHandler eh)
/*    */   {
/* 39 */     super(writer, eh);
/* 40 */     this.syslogFacility = syslogFacility;
/*    */   }
/*    */ 
/*    */   public void setLevel(int level)
/*    */   {
/* 45 */     this.level = level;
/*    */   }
/*    */ 
/*    */   public void setSyslogFacility(int syslogFacility)
/*    */   {
/* 50 */     this.syslogFacility = syslogFacility;
/*    */   }
/*    */ 
/*    */   public void write(String string)
/*    */   {
/* 55 */     super.write("<" + (this.syslogFacility | this.level) + ">" + string);
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.helpers.SyslogQuietWriter
 * JD-Core Version:    0.6.2
 */