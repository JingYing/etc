/*      */ package com.jd.ump.log4j.xml;
/*      */ 
/*      */ import com.jd.ump.log4j.Appender;
/*      */ import com.jd.ump.log4j.Layout;
/*      */ import com.jd.ump.log4j.Level;
/*      */ import com.jd.ump.log4j.LogManager;
/*      */ import com.jd.ump.log4j.Logger;
/*      */ import com.jd.ump.log4j.config.PropertySetter;
/*      */ import com.jd.ump.log4j.helpers.Loader;
/*      */ import com.jd.ump.log4j.helpers.LogLog;
/*      */ import com.jd.ump.log4j.helpers.OptionConverter;
/*      */ import com.jd.ump.log4j.or.RendererMap;
/*      */ import com.jd.ump.log4j.spi.AppenderAttachable;
/*      */ import com.jd.ump.log4j.spi.Configurator;
/*      */ import com.jd.ump.log4j.spi.ErrorHandler;
/*      */ import com.jd.ump.log4j.spi.Filter;
/*      */ import com.jd.ump.log4j.spi.LoggerFactory;
/*      */ import com.jd.ump.log4j.spi.LoggerRepository;
/*      */ import com.jd.ump.log4j.spi.RendererSupport;
/*      */ import com.jd.ump.log4j.spi.ThrowableRenderer;
/*      */ import com.jd.ump.log4j.spi.ThrowableRendererSupport;
/*      */ import java.io.File;
/*      */ import java.io.IOException;
/*      */ import java.io.InputStream;
/*      */ import java.io.InterruptedIOException;
/*      */ import java.io.Reader;
/*      */ import java.lang.reflect.InvocationTargetException;
/*      */ import java.lang.reflect.Method;
/*      */ import java.net.URL;
/*      */ import java.net.URLConnection;
/*      */ import java.util.Hashtable;
/*      */ import java.util.Properties;
/*      */ import javax.xml.parsers.DocumentBuilder;
/*      */ import javax.xml.parsers.DocumentBuilderFactory;
/*      */ import javax.xml.parsers.FactoryConfigurationError;
/*      */ import org.w3c.dom.Document;
/*      */ import org.w3c.dom.Element;
/*      */ import org.w3c.dom.NamedNodeMap;
/*      */ import org.w3c.dom.Node;
/*      */ import org.w3c.dom.NodeList;
/*      */ import org.xml.sax.InputSource;
/*      */ import org.xml.sax.SAXException;
/*      */ 
/*      */ public class DOMConfigurator
/*      */   implements Configurator
/*      */ {
/*      */   static final String CONFIGURATION_TAG = "log4j:configuration";
/*      */   static final String OLD_CONFIGURATION_TAG = "configuration";
/*      */   static final String RENDERER_TAG = "renderer";
/*      */   private static final String THROWABLE_RENDERER_TAG = "throwableRenderer";
/*      */   static final String APPENDER_TAG = "appender";
/*      */   static final String APPENDER_REF_TAG = "appender-ref";
/*      */   static final String PARAM_TAG = "param";
/*      */   static final String LAYOUT_TAG = "layout";
/*      */   static final String CATEGORY = "category";
/*      */   static final String LOGGER = "logger";
/*      */   static final String LOGGER_REF = "logger-ref";
/*      */   static final String CATEGORY_FACTORY_TAG = "categoryFactory";
/*      */   static final String LOGGER_FACTORY_TAG = "loggerFactory";
/*      */   static final String NAME_ATTR = "name";
/*      */   static final String CLASS_ATTR = "class";
/*      */   static final String VALUE_ATTR = "value";
/*      */   static final String ROOT_TAG = "root";
/*      */   static final String ROOT_REF = "root-ref";
/*      */   static final String LEVEL_TAG = "level";
/*      */   static final String PRIORITY_TAG = "priority";
/*      */   static final String FILTER_TAG = "filter";
/*      */   static final String ERROR_HANDLER_TAG = "errorHandler";
/*      */   static final String REF_ATTR = "ref";
/*      */   static final String ADDITIVITY_ATTR = "additivity";
/*      */   static final String THRESHOLD_ATTR = "threshold";
/*      */   static final String CONFIG_DEBUG_ATTR = "configDebug";
/*      */   static final String INTERNAL_DEBUG_ATTR = "debug";
/*      */   private static final String RESET_ATTR = "reset";
/*      */   static final String RENDERING_CLASS_ATTR = "renderingClass";
/*      */   static final String RENDERED_CLASS_ATTR = "renderedClass";
/*      */   static final String EMPTY_STR = "";
/*  125 */   static final Class[] ONE_STRING_PARAM = { String.class };
/*      */   static final String dbfKey = "javax.xml.parsers.DocumentBuilderFactory";
/*      */   Hashtable appenderBag;
/*      */   Properties props;
/*      */   LoggerRepository repository;
/*  136 */   protected LoggerFactory catFactory = null;
/*      */ 
/*      */   public DOMConfigurator()
/*      */   {
/*  143 */     this.appenderBag = new Hashtable();
/*      */   }
/*      */ 
/*      */   protected Appender findAppenderByName(Document doc, String appenderName)
/*      */   {
/*  151 */     Appender appender = (Appender)this.appenderBag.get(appenderName);
/*      */ 
/*  153 */     if (appender != null) {
/*  154 */       return appender;
/*      */     }
/*      */ 
/*  160 */     Element element = null;
/*  161 */     NodeList list = doc.getElementsByTagName("appender");
/*  162 */     for (int t = 0; t < list.getLength(); t++) {
/*  163 */       Node node = list.item(t);
/*  164 */       NamedNodeMap map = node.getAttributes();
/*  165 */       Node attrNode = map.getNamedItem("name");
/*  166 */       if (appenderName.equals(attrNode.getNodeValue())) {
/*  167 */         element = (Element)node;
/*  168 */         break;
/*      */       }
/*      */ 
/*      */     }
/*      */ 
/*  173 */     if (element == null) {
/*  174 */       LogLog.error("No appender named [" + appenderName + "] could be found.");
/*  175 */       return null;
/*      */     }
/*  177 */     appender = parseAppender(element);
/*  178 */     if (appender != null) {
/*  179 */       this.appenderBag.put(appenderName, appender);
/*      */     }
/*  181 */     return appender;
/*      */   }
/*      */ 
/*      */   protected Appender findAppenderByReference(Element appenderRef)
/*      */   {
/*  190 */     String appenderName = subst(appenderRef.getAttribute("ref"));
/*  191 */     Document doc = appenderRef.getOwnerDocument();
/*  192 */     return findAppenderByName(doc, appenderName);
/*      */   }
/*      */ 
/*      */   private static void parseUnrecognizedElement(Object instance, Element element, Properties props)
/*      */     throws Exception
/*      */   {
/*  208 */     boolean recognized = false;
/*  209 */     if ((instance instanceof UnrecognizedElementHandler)) {
/*  210 */       recognized = ((UnrecognizedElementHandler)instance).parseUnrecognizedElement(element, props);
/*      */     }
/*      */ 
/*  213 */     if (!recognized)
/*  214 */       LogLog.warn("Unrecognized element " + element.getNodeName());
/*      */   }
/*      */ 
/*      */   private static void quietParseUnrecognizedElement(Object instance, Element element, Properties props)
/*      */   {
/*      */     try
/*      */     {
/*  231 */       parseUnrecognizedElement(instance, element, props);
/*      */     } catch (Exception ex) {
/*  233 */       if (((ex instanceof InterruptedException)) || ((ex instanceof InterruptedIOException))) {
/*  234 */         Thread.currentThread().interrupt();
/*      */       }
/*  236 */       LogLog.error("Error in extension content: ", ex);
/*      */     }
/*      */   }
/*      */ 
/*      */   protected Appender parseAppender(Element appenderElement)
/*      */   {
/*  245 */     String className = subst(appenderElement.getAttribute("class"));
/*  246 */     LogLog.debug("Class name: [" + className + ']');
/*      */     try {
/*  248 */       Object instance = Loader.loadClass(className).newInstance();
/*  249 */       Appender appender = (Appender)instance;
/*  250 */       PropertySetter propSetter = new PropertySetter(appender);
/*      */ 
/*  252 */       appender.setName(subst(appenderElement.getAttribute("name")));
/*      */ 
/*  254 */       NodeList children = appenderElement.getChildNodes();
/*  255 */       int length = children.getLength();
/*      */ 
/*  257 */       for (int loop = 0; loop < length; loop++) {
/*  258 */         Node currentNode = children.item(loop);
/*      */ 
/*  261 */         if (currentNode.getNodeType() == 1) {
/*  262 */           Element currentElement = (Element)currentNode;
/*      */ 
/*  265 */           if (currentElement.getTagName().equals("param")) {
/*  266 */             setParameter(currentElement, propSetter);
/*      */           }
/*  269 */           else if (currentElement.getTagName().equals("layout")) {
/*  270 */             appender.setLayout(parseLayout(currentElement));
/*      */           }
/*  273 */           else if (currentElement.getTagName().equals("filter")) {
/*  274 */             parseFilters(currentElement, appender);
/*      */           }
/*  276 */           else if (currentElement.getTagName().equals("errorHandler")) {
/*  277 */             parseErrorHandler(currentElement, appender);
/*      */           }
/*  279 */           else if (currentElement.getTagName().equals("appender-ref")) {
/*  280 */             String refName = subst(currentElement.getAttribute("ref"));
/*  281 */             if ((appender instanceof AppenderAttachable)) {
/*  282 */               AppenderAttachable aa = (AppenderAttachable)appender;
/*  283 */               LogLog.debug("Attaching appender named [" + refName + "] to appender named [" + appender.getName() + "].");
/*      */ 
/*  285 */               aa.addAppender(findAppenderByReference(currentElement));
/*      */             } else {
/*  287 */               LogLog.error("Requesting attachment of appender named [" + refName + "] to appender named [" + appender.getName() + "] which does not implement org.apache.log4j.spi.AppenderAttachable.");
/*      */             }
/*      */           }
/*      */           else
/*      */           {
/*  292 */             parseUnrecognizedElement(instance, currentElement, this.props);
/*      */           }
/*      */         }
/*      */       }
/*  296 */       propSetter.activate();
/*  297 */       return appender;
/*      */     }
/*      */     catch (Exception oops)
/*      */     {
/*  302 */       if (((oops instanceof InterruptedException)) || ((oops instanceof InterruptedIOException))) {
/*  303 */         Thread.currentThread().interrupt();
/*      */       }
/*  305 */       LogLog.error("Could not create an Appender. Reported error follows.", oops);
/*      */     }
/*  307 */     return null;
/*      */   }
/*      */ 
/*      */   protected void parseErrorHandler(Element element, Appender appender)
/*      */   {
/*  316 */     ErrorHandler eh = (ErrorHandler)OptionConverter.instantiateByClassName(subst(element.getAttribute("class")), ErrorHandler.class, null);
/*      */ 
/*  321 */     if (eh != null) {
/*  322 */       eh.setAppender(appender);
/*      */ 
/*  324 */       PropertySetter propSetter = new PropertySetter(eh);
/*  325 */       NodeList children = element.getChildNodes();
/*  326 */       int length = children.getLength();
/*      */ 
/*  328 */       for (int loop = 0; loop < length; loop++) {
/*  329 */         Node currentNode = children.item(loop);
/*  330 */         if (currentNode.getNodeType() == 1) {
/*  331 */           Element currentElement = (Element)currentNode;
/*  332 */           String tagName = currentElement.getTagName();
/*  333 */           if (tagName.equals("param")) {
/*  334 */             setParameter(currentElement, propSetter);
/*  335 */           } else if (tagName.equals("appender-ref")) {
/*  336 */             eh.setBackupAppender(findAppenderByReference(currentElement));
/*  337 */           } else if (tagName.equals("logger-ref")) {
/*  338 */             String loggerName = currentElement.getAttribute("ref");
/*  339 */             Logger logger = this.catFactory == null ? this.repository.getLogger(loggerName) : this.repository.getLogger(loggerName, this.catFactory);
/*      */ 
/*  341 */             eh.setLogger(logger);
/*  342 */           } else if (tagName.equals("root-ref")) {
/*  343 */             Logger root = this.repository.getRootLogger();
/*  344 */             eh.setLogger(root);
/*      */           } else {
/*  346 */             quietParseUnrecognizedElement(eh, currentElement, this.props);
/*      */           }
/*      */         }
/*      */       }
/*  350 */       propSetter.activate();
/*  351 */       appender.setErrorHandler(eh);
/*      */     }
/*      */   }
/*      */ 
/*      */   protected void parseFilters(Element element, Appender appender)
/*      */   {
/*  360 */     String clazz = subst(element.getAttribute("class"));
/*  361 */     Filter filter = (Filter)OptionConverter.instantiateByClassName(clazz, Filter.class, null);
/*      */ 
/*  364 */     if (filter != null) {
/*  365 */       PropertySetter propSetter = new PropertySetter(filter);
/*  366 */       NodeList children = element.getChildNodes();
/*  367 */       int length = children.getLength();
/*      */ 
/*  369 */       for (int loop = 0; loop < length; loop++) {
/*  370 */         Node currentNode = children.item(loop);
/*  371 */         if (currentNode.getNodeType() == 1) {
/*  372 */           Element currentElement = (Element)currentNode;
/*  373 */           String tagName = currentElement.getTagName();
/*  374 */           if (tagName.equals("param"))
/*  375 */             setParameter(currentElement, propSetter);
/*      */           else {
/*  377 */             quietParseUnrecognizedElement(filter, currentElement, this.props);
/*      */           }
/*      */         }
/*      */       }
/*  381 */       propSetter.activate();
/*  382 */       LogLog.debug("Adding filter of type [" + filter.getClass() + "] to appender named [" + appender.getName() + "].");
/*      */ 
/*  384 */       appender.addFilter(filter);
/*      */     }
/*      */   }
/*      */ 
/*      */   protected void parseCategory(Element loggerElement)
/*      */   {
/*  394 */     String catName = subst(loggerElement.getAttribute("name"));
/*      */ 
/*  398 */     String className = subst(loggerElement.getAttribute("class"));
/*      */     Logger cat;
/*  401 */     if ("".equals(className)) {
/*  402 */       LogLog.debug("Retreiving an instance of org.apache.log4j.Logger.");
/*  403 */       cat = this.catFactory == null ? this.repository.getLogger(catName) : this.repository.getLogger(catName, this.catFactory);
/*      */     }
/*      */     else {
/*  406 */       LogLog.debug("Desired logger sub-class: [" + className + ']');
/*      */       try {
/*  408 */         Class clazz = Loader.loadClass(className);
/*  409 */         Method getInstanceMethod = clazz.getMethod("getLogger", ONE_STRING_PARAM);
/*      */ 
/*  411 */         cat = (Logger)getInstanceMethod.invoke(null, new Object[] { catName });
/*      */       } catch (InvocationTargetException oops) {
/*  413 */         if (((oops.getTargetException() instanceof InterruptedException)) || ((oops.getTargetException() instanceof InterruptedIOException)))
/*      */         {
/*  415 */           Thread.currentThread().interrupt();
/*      */         }
/*  417 */         LogLog.error("Could not retrieve category [" + catName + "]. Reported error follows.", oops);
/*      */ 
/*  419 */         return;
/*      */       } catch (Exception oops) {
/*  421 */         LogLog.error("Could not retrieve category [" + catName + "]. Reported error follows.", oops);
/*      */ 
/*  423 */         return;
/*      */       }
/*      */ 
/*      */     }
/*      */ 
/*  430 */     synchronized (cat) {
/*  431 */       boolean additivity = OptionConverter.toBoolean(subst(loggerElement.getAttribute("additivity")), true);
/*      */ 
/*  435 */       LogLog.debug("Setting [" + cat.getName() + "] additivity to [" + additivity + "].");
/*  436 */       cat.setAdditivity(additivity);
/*  437 */       parseChildrenOfLoggerElement(loggerElement, cat, false);
/*      */     }
/*      */   }
/*      */ 
/*      */   protected void parseCategoryFactory(Element factoryElement)
/*      */   {
/*  447 */     String className = subst(factoryElement.getAttribute("class"));
/*      */ 
/*  449 */     if ("".equals(className)) {
/*  450 */       LogLog.error("Category Factory tag class attribute not found.");
/*  451 */       LogLog.debug("No Category Factory configured.");
/*      */     }
/*      */     else {
/*  454 */       LogLog.debug("Desired category factory: [" + className + ']');
/*  455 */       Object factory = OptionConverter.instantiateByClassName(className, LoggerFactory.class, null);
/*      */ 
/*  458 */       if ((factory instanceof LoggerFactory))
/*  459 */         this.catFactory = ((LoggerFactory)factory);
/*      */       else {
/*  461 */         LogLog.error("Category Factory class " + className + " does not implement org.apache.log4j.LoggerFactory");
/*      */       }
/*  463 */       PropertySetter propSetter = new PropertySetter(factory);
/*      */ 
/*  465 */       Element currentElement = null;
/*  466 */       Node currentNode = null;
/*  467 */       NodeList children = factoryElement.getChildNodes();
/*  468 */       int length = children.getLength();
/*      */ 
/*  470 */       for (int loop = 0; loop < length; loop++) {
/*  471 */         currentNode = children.item(loop);
/*  472 */         if (currentNode.getNodeType() == 1) {
/*  473 */           currentElement = (Element)currentNode;
/*  474 */           if (currentElement.getTagName().equals("param"))
/*  475 */             setParameter(currentElement, propSetter);
/*      */           else
/*  477 */             quietParseUnrecognizedElement(factory, currentElement, this.props);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */ 
/*      */   protected void parseRoot(Element rootElement)
/*      */   {
/*  490 */     Logger root = this.repository.getRootLogger();
/*      */ 
/*  492 */     synchronized (root) {
/*  493 */       parseChildrenOfLoggerElement(rootElement, root, true);
/*      */     }
/*      */   }
/*      */ 
/*      */   protected void parseChildrenOfLoggerElement(Element catElement, Logger cat, boolean isRoot)
/*      */   {
/*  505 */     PropertySetter propSetter = new PropertySetter(cat);
/*      */ 
/*  509 */     cat.removeAllAppenders();
/*      */ 
/*  512 */     NodeList children = catElement.getChildNodes();
/*  513 */     int length = children.getLength();
/*      */ 
/*  515 */     for (int loop = 0; loop < length; loop++) {
/*  516 */       Node currentNode = children.item(loop);
/*      */ 
/*  518 */       if (currentNode.getNodeType() == 1) {
/*  519 */         Element currentElement = (Element)currentNode;
/*  520 */         String tagName = currentElement.getTagName();
/*      */ 
/*  522 */         if (tagName.equals("appender-ref")) {
/*  523 */           Element appenderRef = (Element)currentNode;
/*  524 */           Appender appender = findAppenderByReference(appenderRef);
/*  525 */           String refName = subst(appenderRef.getAttribute("ref"));
/*  526 */           if (appender != null) {
/*  527 */             LogLog.debug("Adding appender named [" + refName + "] to category [" + cat.getName() + "].");
/*      */           }
/*      */           else {
/*  530 */             LogLog.debug("Appender named [" + refName + "] not found.");
/*      */           }
/*  532 */           cat.addAppender(appender);
/*      */         }
/*  534 */         else if (tagName.equals("level")) {
/*  535 */           parseLevel(currentElement, cat, isRoot);
/*  536 */         } else if (tagName.equals("priority")) {
/*  537 */           parseLevel(currentElement, cat, isRoot);
/*  538 */         } else if (tagName.equals("param")) {
/*  539 */           setParameter(currentElement, propSetter);
/*      */         } else {
/*  541 */           quietParseUnrecognizedElement(cat, currentElement, this.props);
/*      */         }
/*      */       }
/*      */     }
/*  545 */     propSetter.activate();
/*      */   }
/*      */ 
/*      */   protected Layout parseLayout(Element layout_element)
/*      */   {
/*  553 */     String className = subst(layout_element.getAttribute("class"));
/*  554 */     LogLog.debug("Parsing layout of class: \"" + className + "\"");
/*      */     try {
/*  556 */       Object instance = Loader.loadClass(className).newInstance();
/*  557 */       Layout layout = (Layout)instance;
/*  558 */       PropertySetter propSetter = new PropertySetter(layout);
/*      */ 
/*  560 */       NodeList params = layout_element.getChildNodes();
/*  561 */       int length = params.getLength();
/*      */ 
/*  563 */       for (int loop = 0; loop < length; loop++) {
/*  564 */         Node currentNode = params.item(loop);
/*  565 */         if (currentNode.getNodeType() == 1) {
/*  566 */           Element currentElement = (Element)currentNode;
/*  567 */           String tagName = currentElement.getTagName();
/*  568 */           if (tagName.equals("param"))
/*  569 */             setParameter(currentElement, propSetter);
/*      */           else {
/*  571 */             parseUnrecognizedElement(instance, currentElement, this.props);
/*      */           }
/*      */         }
/*      */       }
/*      */ 
/*  576 */       propSetter.activate();
/*  577 */       return layout;
/*      */     }
/*      */     catch (Exception oops) {
/*  580 */       if (((oops instanceof InterruptedException)) || ((oops instanceof InterruptedIOException))) {
/*  581 */         Thread.currentThread().interrupt();
/*      */       }
/*  583 */       LogLog.error("Could not create the Layout. Reported error follows.", oops);
/*      */     }
/*  585 */     return null;
/*      */   }
/*      */ 
/*      */   protected void parseRenderer(Element element)
/*      */   {
/*  591 */     String renderingClass = subst(element.getAttribute("renderingClass"));
/*  592 */     String renderedClass = subst(element.getAttribute("renderedClass"));
/*  593 */     if ((this.repository instanceof RendererSupport))
/*  594 */       RendererMap.addRenderer((RendererSupport)this.repository, renderedClass, renderingClass);
/*      */   }
/*      */ 
/*      */   protected ThrowableRenderer parseThrowableRenderer(Element element)
/*      */   {
/*  606 */     String className = subst(element.getAttribute("class"));
/*  607 */     LogLog.debug("Parsing throwableRenderer of class: \"" + className + "\"");
/*      */     try {
/*  609 */       Object instance = Loader.loadClass(className).newInstance();
/*  610 */       ThrowableRenderer tr = (ThrowableRenderer)instance;
/*  611 */       PropertySetter propSetter = new PropertySetter(tr);
/*      */ 
/*  613 */       NodeList params = element.getChildNodes();
/*  614 */       int length = params.getLength();
/*      */ 
/*  616 */       for (int loop = 0; loop < length; loop++) {
/*  617 */         Node currentNode = params.item(loop);
/*  618 */         if (currentNode.getNodeType() == 1) {
/*  619 */           Element currentElement = (Element)currentNode;
/*  620 */           String tagName = currentElement.getTagName();
/*  621 */           if (tagName.equals("param"))
/*  622 */             setParameter(currentElement, propSetter);
/*      */           else {
/*  624 */             parseUnrecognizedElement(instance, currentElement, this.props);
/*      */           }
/*      */         }
/*      */       }
/*      */ 
/*  629 */       propSetter.activate();
/*  630 */       return tr;
/*      */     }
/*      */     catch (Exception oops) {
/*  633 */       if (((oops instanceof InterruptedException)) || ((oops instanceof InterruptedIOException))) {
/*  634 */         Thread.currentThread().interrupt();
/*      */       }
/*  636 */       LogLog.error("Could not create the ThrowableRenderer. Reported error follows.", oops);
/*      */     }
/*  638 */     return null;
/*      */   }
/*      */ 
/*      */   protected void parseLevel(Element element, Logger logger, boolean isRoot)
/*      */   {
/*  647 */     String catName = logger.getName();
/*  648 */     if (isRoot) {
/*  649 */       catName = "root";
/*      */     }
/*      */ 
/*  652 */     String priStr = subst(element.getAttribute("value"));
/*  653 */     LogLog.debug("Level value for " + catName + " is  [" + priStr + "].");
/*      */ 
/*  655 */     if (("inherited".equalsIgnoreCase(priStr)) || ("null".equalsIgnoreCase(priStr))) {
/*  656 */       if (isRoot)
/*  657 */         LogLog.error("Root level cannot be inherited. Ignoring directive.");
/*      */       else
/*  659 */         logger.setLevel(null);
/*      */     }
/*      */     else {
/*  662 */       String className = subst(element.getAttribute("class"));
/*  663 */       if ("".equals(className)) {
/*  664 */         logger.setLevel(OptionConverter.toLevel(priStr, Level.DEBUG));
/*      */       } else {
/*  666 */         LogLog.debug("Desired Level sub-class: [" + className + ']');
/*      */         try {
/*  668 */           Class clazz = Loader.loadClass(className);
/*  669 */           Method toLevelMethod = clazz.getMethod("toLevel", ONE_STRING_PARAM);
/*      */ 
/*  671 */           Level pri = (Level)toLevelMethod.invoke(null, new Object[] { priStr });
/*      */ 
/*  673 */           logger.setLevel(pri);
/*      */         } catch (Exception oops) {
/*  675 */           if (((oops instanceof InterruptedException)) || ((oops instanceof InterruptedIOException))) {
/*  676 */             Thread.currentThread().interrupt();
/*      */           }
/*  678 */           LogLog.error("Could not create level [" + priStr + "]. Reported error follows.", oops);
/*      */ 
/*  680 */           return;
/*      */         }
/*      */       }
/*      */     }
/*  684 */     LogLog.debug(catName + " level set to " + logger.getLevel());
/*      */   }
/*      */ 
/*      */   protected void setParameter(Element elem, PropertySetter propSetter)
/*      */   {
/*  689 */     String name = subst(elem.getAttribute("name"));
/*  690 */     String value = elem.getAttribute("value");
/*  691 */     value = subst(OptionConverter.convertSpecialChars(value));
/*  692 */     propSetter.setProperty(name, value);
/*      */   }
/*      */ 
/*      */   public static void configure(Element element)
/*      */   {
/*  704 */     DOMConfigurator configurator = new DOMConfigurator();
/*  705 */     configurator.doConfigure(element, LogManager.getLoggerRepository());
/*      */   }
/*      */ 
/*      */   public static void configureAndWatch(String configFilename)
/*      */   {
/*  719 */     configureAndWatch(configFilename, 60000L);
/*      */   }
/*      */ 
/*      */   public static void configureAndWatch(String configFilename, long delay)
/*      */   {
/*  736 */     XMLWatchdog xdog = new XMLWatchdog(configFilename);
/*  737 */     xdog.setDelay(delay);
/*  738 */     xdog.start();
/*      */   }
/*      */ 
/*      */   public void doConfigure(final String filename, LoggerRepository repository)
/*      */   {
/*  748 */     ParseAction action = new ParseAction() {
/*      */       public Document parse(DocumentBuilder parser) throws SAXException, IOException {
/*  750 */         return parser.parse(new File(filename));
/*      */       }
/*      */       public String toString() {
/*  753 */         return "file [" + filename + "]";
/*      */       }
/*      */     };
/*  756 */     doConfigure(action, repository);
/*      */   }
/*      */ 
/*      */   public void doConfigure(final URL url, LoggerRepository repository)
/*      */   {
/*  762 */     ParseAction action = new ParseAction() {
/*      */       public Document parse(DocumentBuilder parser) throws SAXException, IOException {
/*  764 */         URLConnection uConn = url.openConnection();
/*  765 */         uConn.setUseCaches(false);
/*  766 */         InputSource src = new InputSource(uConn.getInputStream());
/*  767 */         src.setSystemId(url.toString());
/*  768 */         return parser.parse(src);
/*      */       }
/*      */       public String toString() {
/*  771 */         return "url [" + url.toString() + "]";
/*      */       }
/*      */     };
/*  774 */     doConfigure(action, repository);
/*      */   }
/*      */ 
/*      */   public void doConfigure(final InputStream inputStream, LoggerRepository repository)
/*      */     throws FactoryConfigurationError
/*      */   {
/*  785 */     ParseAction action = new ParseAction() {
/*      */       public Document parse(DocumentBuilder parser) throws SAXException, IOException {
/*  787 */         InputSource inputSource = new InputSource(inputStream);
/*  788 */         inputSource.setSystemId("dummy://log4j.dtd");
/*  789 */         return parser.parse(inputSource);
/*      */       }
/*      */       public String toString() {
/*  792 */         return "input stream [" + inputStream.toString() + "]";
/*      */       }
/*      */     };
/*  795 */     doConfigure(action, repository);
/*      */   }
/*      */ 
/*      */   public void doConfigure(final Reader reader, LoggerRepository repository)
/*      */     throws FactoryConfigurationError
/*      */   {
/*  806 */     ParseAction action = new ParseAction() {
/*      */       public Document parse(DocumentBuilder parser) throws SAXException, IOException {
/*  808 */         InputSource inputSource = new InputSource(reader);
/*  809 */         inputSource.setSystemId("dummy://log4j.dtd");
/*  810 */         return parser.parse(inputSource);
/*      */       }
/*      */       public String toString() {
/*  813 */         return "reader [" + reader.toString() + "]";
/*      */       }
/*      */     };
/*  816 */     doConfigure(action, repository);
/*      */   }
/*      */ 
/*      */   protected void doConfigure(final InputSource inputSource, LoggerRepository repository)
/*      */     throws FactoryConfigurationError
/*      */   {
/*  827 */     if (inputSource.getSystemId() == null) {
/*  828 */       inputSource.setSystemId("dummy://log4j.dtd");
/*      */     }
/*  830 */     ParseAction action = new ParseAction() {
/*      */       public Document parse(DocumentBuilder parser) throws SAXException, IOException {
/*  832 */         return parser.parse(inputSource);
/*      */       }
/*      */       public String toString() {
/*  835 */         return "input source [" + inputSource.toString() + "]";
/*      */       }
/*      */     };
/*  838 */     doConfigure(action, repository);
/*      */   }
/*      */ 
/*      */   private final void doConfigure(ParseAction action, LoggerRepository repository)
/*      */     throws FactoryConfigurationError
/*      */   {
/*  844 */     DocumentBuilderFactory dbf = null;
/*  845 */     this.repository = repository;
/*      */     try {
/*  847 */       LogLog.debug("System property is :" + OptionConverter.getSystemProperty("javax.xml.parsers.DocumentBuilderFactory", null));
/*      */ 
/*  850 */       dbf = DocumentBuilderFactory.newInstance();
/*  851 */       LogLog.debug("Standard DocumentBuilderFactory search succeded.");
/*  852 */       LogLog.debug("DocumentBuilderFactory is: " + dbf.getClass().getName());
/*      */     } catch (FactoryConfigurationError fce) {
/*  854 */       Exception e = fce.getException();
/*  855 */       LogLog.debug("Could not instantiate a DocumentBuilderFactory.", e);
/*  856 */       throw fce;
/*      */     }
/*      */     try
/*      */     {
/*  860 */       dbf.setValidating(true);
/*      */ 
/*  862 */       DocumentBuilder docBuilder = dbf.newDocumentBuilder();
/*      */ 
/*  864 */       docBuilder.setErrorHandler(new SAXErrorHandler());
/*  865 */       docBuilder.setEntityResolver(new Log4jEntityResolver());
/*      */ 
/*  867 */       Document doc = action.parse(docBuilder);
/*  868 */       parse(doc.getDocumentElement());
/*      */     } catch (Exception e) {
/*  870 */       if (((e instanceof InterruptedException)) || ((e instanceof InterruptedIOException))) {
/*  871 */         Thread.currentThread().interrupt();
/*      */       }
/*      */ 
/*  874 */       LogLog.error("Could not parse " + action.toString() + ".", e);
/*      */     }
/*      */   }
/*      */ 
/*      */   public void doConfigure(Element element, LoggerRepository repository)
/*      */   {
/*  882 */     this.repository = repository;
/*  883 */     parse(element);
/*      */   }
/*      */ 
/*      */   public static void configure(String filename)
/*      */     throws FactoryConfigurationError
/*      */   {
/*  892 */     new DOMConfigurator().doConfigure(filename, LogManager.getLoggerRepository());
/*      */   }
/*      */ 
/*      */   public static void configure(URL url)
/*      */     throws FactoryConfigurationError
/*      */   {
/*  902 */     new DOMConfigurator().doConfigure(url, LogManager.getLoggerRepository());
/*      */   }
/*      */ 
/*      */   protected void parse(Element element)
/*      */   {
/*  914 */     String rootElementName = element.getTagName();
/*      */ 
/*  916 */     if (!rootElementName.equals("log4j:configuration")) {
/*  917 */       if (rootElementName.equals("configuration")) {
/*  918 */         LogLog.warn("The <configuration> element has been deprecated.");
/*      */ 
/*  920 */         LogLog.warn("Use the <log4j:configuration> element instead.");
/*      */       } else {
/*  922 */         LogLog.error("DOM element is - not a <log4j:configuration> element.");
/*  923 */         return;
/*      */       }
/*      */ 
/*      */     }
/*      */ 
/*  928 */     String debugAttrib = subst(element.getAttribute("debug"));
/*      */ 
/*  930 */     LogLog.debug("debug attribute= \"" + debugAttrib + "\".");
/*      */ 
/*  933 */     if ((!debugAttrib.equals("")) && (!debugAttrib.equals("null")))
/*  934 */       LogLog.setInternalDebugging(OptionConverter.toBoolean(debugAttrib, true));
/*      */     else {
/*  936 */       LogLog.debug("Ignoring debug attribute.");
/*      */     }
/*      */ 
/*  943 */     String resetAttrib = subst(element.getAttribute("reset"));
/*  944 */     LogLog.debug("reset attribute= \"" + resetAttrib + "\".");
/*  945 */     if ((!"".equals(resetAttrib)) && 
/*  946 */       (OptionConverter.toBoolean(resetAttrib, false))) {
/*  947 */       this.repository.resetConfiguration();
/*      */     }
/*      */ 
/*  953 */     String confDebug = subst(element.getAttribute("configDebug"));
/*  954 */     if ((!confDebug.equals("")) && (!confDebug.equals("null"))) {
/*  955 */       LogLog.warn("The \"configDebug\" attribute is deprecated.");
/*  956 */       LogLog.warn("Use the \"debug\" attribute instead.");
/*  957 */       LogLog.setInternalDebugging(OptionConverter.toBoolean(confDebug, true));
/*      */     }
/*      */ 
/*  960 */     String thresholdStr = subst(element.getAttribute("threshold"));
/*  961 */     LogLog.debug("Threshold =\"" + thresholdStr + "\".");
/*  962 */     if ((!"".equals(thresholdStr)) && (!"null".equals(thresholdStr))) {
/*  963 */       this.repository.setThreshold(thresholdStr);
/*      */     }
/*      */ 
/*  975 */     String tagName = null;
/*  976 */     Element currentElement = null;
/*  977 */     Node currentNode = null;
/*  978 */     NodeList children = element.getChildNodes();
/*  979 */     int length = children.getLength();
/*      */ 
/*  981 */     for (int loop = 0; loop < length; loop++) {
/*  982 */       currentNode = children.item(loop);
/*  983 */       if (currentNode.getNodeType() == 1) {
/*  984 */         currentElement = (Element)currentNode;
/*  985 */         tagName = currentElement.getTagName();
/*      */ 
/*  987 */         if ((tagName.equals("categoryFactory")) || (tagName.equals("loggerFactory"))) {
/*  988 */           parseCategoryFactory(currentElement);
/*      */         }
/*      */       }
/*      */     }
/*      */ 
/*  993 */     for (int loop = 0; loop < length; loop++) {
/*  994 */       currentNode = children.item(loop);
/*  995 */       if (currentNode.getNodeType() == 1) {
/*  996 */         currentElement = (Element)currentNode;
/*  997 */         tagName = currentElement.getTagName();
/*      */ 
/*  999 */         if ((tagName.equals("category")) || (tagName.equals("logger")))
/* 1000 */           parseCategory(currentElement);
/* 1001 */         else if (tagName.equals("root"))
/* 1002 */           parseRoot(currentElement);
/* 1003 */         else if (tagName.equals("renderer"))
/* 1004 */           parseRenderer(currentElement);
/* 1005 */         else if (tagName.equals("throwableRenderer")) {
/* 1006 */           if ((this.repository instanceof ThrowableRendererSupport)) {
/* 1007 */             ThrowableRenderer tr = parseThrowableRenderer(currentElement);
/* 1008 */             if (tr != null)
/* 1009 */               ((ThrowableRendererSupport)this.repository).setThrowableRenderer(tr);
/*      */           }
/*      */         }
/* 1012 */         else if ((!tagName.equals("appender")) && (!tagName.equals("categoryFactory")) && (!tagName.equals("loggerFactory")))
/*      */         {
/* 1015 */           quietParseUnrecognizedElement(this.repository, currentElement, this.props);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */ 
/*      */   protected String subst(String value)
/*      */   {
/* 1024 */     return subst(value, this.props);
/*      */   }
/*      */ 
/*      */   public static String subst(String value, Properties props)
/*      */   {
/*      */     try
/*      */     {
/* 1039 */       return OptionConverter.substVars(value, props);
/*      */     } catch (IllegalArgumentException e) {
/* 1041 */       LogLog.warn("Could not perform variable substitution.", e);
/* 1042 */     }return value;
/*      */   }
/*      */ 
/*      */   public static void setParameter(Element elem, PropertySetter propSetter, Properties props)
/*      */   {
/* 1058 */     String name = subst(elem.getAttribute("name"), props);
/* 1059 */     String value = elem.getAttribute("value");
/* 1060 */     value = subst(OptionConverter.convertSpecialChars(value), props);
/* 1061 */     propSetter.setProperty(name, value);
/*      */   }
/*      */ 
/*      */   public static Object parseElement(Element element, Properties props, Class expectedClass)
/*      */     throws Exception
/*      */   {
/* 1081 */     String clazz = subst(element.getAttribute("class"), props);
/* 1082 */     Object instance = OptionConverter.instantiateByClassName(clazz, expectedClass, null);
/*      */ 
/* 1085 */     if (instance != null) {
/* 1086 */       PropertySetter propSetter = new PropertySetter(instance);
/* 1087 */       NodeList children = element.getChildNodes();
/* 1088 */       int length = children.getLength();
/*      */ 
/* 1090 */       for (int loop = 0; loop < length; loop++) {
/* 1091 */         Node currentNode = children.item(loop);
/* 1092 */         if (currentNode.getNodeType() == 1) {
/* 1093 */           Element currentElement = (Element)currentNode;
/* 1094 */           String tagName = currentElement.getTagName();
/* 1095 */           if (tagName.equals("param"))
/* 1096 */             setParameter(currentElement, propSetter, props);
/*      */           else {
/* 1098 */             parseUnrecognizedElement(instance, currentElement, props);
/*      */           }
/*      */         }
/*      */       }
/* 1102 */       return instance;
/*      */     }
/* 1104 */     return null;
/*      */   }
/*      */ 
/*      */   private static abstract interface ParseAction
/*      */   {
/*      */     public abstract Document parse(DocumentBuilder paramDocumentBuilder)
/*      */       throws SAXException, IOException;
/*      */   }
/*      */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.xml.DOMConfigurator
 * JD-Core Version:    0.6.2
 */