/*    */ package com.jd.ump.log4j.pattern;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ 
/*    */ public class ThreadPatternConverter extends LoggingEventPatternConverter
/*    */ {
/* 32 */   private static final ThreadPatternConverter INSTANCE = new ThreadPatternConverter();
/*    */ 
/*    */   private ThreadPatternConverter()
/*    */   {
/* 39 */     super("Thread", "thread");
/*    */   }
/*    */ 
/*    */   public static ThreadPatternConverter newInstance(String[] options)
/*    */   {
/* 49 */     return INSTANCE;
/*    */   }
/*    */ 
/*    */   public void format(LoggingEvent event, StringBuffer toAppendTo)
/*    */   {
/* 56 */     toAppendTo.append(event.getThreadName());
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.pattern.ThreadPatternConverter
 * JD-Core Version:    0.6.2
 */