/*     */ package com.jd.ump.log4j.helpers;
/*     */ 
/*     */ import com.jd.ump.log4j.Appender;
/*     */ import com.jd.ump.log4j.spi.AppenderAttachable;
/*     */ import com.jd.ump.log4j.spi.LoggingEvent;
/*     */ import java.util.Enumeration;
/*     */ import java.util.Vector;
/*     */ 
/*     */ public class AppenderAttachableImpl
/*     */   implements AppenderAttachable
/*     */ {
/*     */   protected Appender appender;
/*     */ 
/*     */   public void addAppender(Appender newAppender)
/*     */   {
/*  46 */     if (newAppender == null) {
/*  47 */       return;
/*     */     }
/*  49 */     this.appender = newAppender;
/*     */   }
/*     */ 
/*     */   public int appendLoopOnAppenders(LoggingEvent event)
/*     */   {
/*  55 */     Appender aa = this.appender;
/*  56 */     if (aa != null) {
/*  57 */       aa.doAppend(event);
/*     */     }
/*     */ 
/*  60 */     return 1;
/*     */   }
/*     */ 
/*     */   public Appender getAppender()
/*     */   {
/*  71 */     Appender aa = this.appender;
/*  72 */     return aa;
/*     */   }
/*     */ 
/*     */   public Appender getAppender(String name)
/*     */   {
/*  83 */     Appender aa = this.appender;
/*     */ 
/*  85 */     if ((name == null) || (aa == null)) {
/*  86 */       return null;
/*     */     }
/*     */ 
/*  89 */     if (name.equals(aa.getName())) {
/*  90 */       return aa;
/*     */     }
/*     */ 
/*  93 */     return null;
/*     */   }
/*     */ 
/*     */   public boolean isAttached(Appender appender)
/*     */   {
/* 103 */     if (appender == null) {
/* 104 */       return false;
/*     */     }
/*     */ 
/* 107 */     return this.appender == appender;
/*     */   }
/*     */ 
/*     */   public void removeAllAppenders()
/*     */   {
/* 116 */     this.appender = null;
/*     */   }
/*     */ 
/*     */   public void removeAppender(Appender appender)
/*     */   {
/* 124 */     if (appender == null) {
/* 125 */       return;
/*     */     }
/* 127 */     if (this.appender == appender)
/* 128 */       this.appender = null;
/*     */   }
/*     */ 
/*     */   public void removeAppender(String name)
/*     */   {
/* 139 */     if ((name == null) || (this.appender == null)) {
/* 140 */       return;
/*     */     }
/*     */ 
/* 143 */     if (name.equals(this.appender.getName()))
/* 144 */       this.appender = null;
/*     */   }
/*     */ 
/*     */   public Enumeration getAllAppenders()
/*     */   {
/* 150 */     Appender aa = this.appender;
/* 151 */     if (aa == null) {
/* 152 */       return null;
/*     */     }
/* 154 */     Vector list = new Vector();
/* 155 */     list.add(aa);
/*     */ 
/* 157 */     return list.elements();
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.helpers.AppenderAttachableImpl
 * JD-Core Version:    0.6.2
 */