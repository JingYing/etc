/*     */ package com.jd.ump.log4j.chainsaw;
/*     */ 
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import com.jd.ump.log4j.spi.LoggingEvent;
/*     */ import java.io.EOFException;
/*     */ import java.io.IOException;
/*     */ import java.io.ObjectInputStream;
/*     */ import java.net.InetAddress;
/*     */ import java.net.ServerSocket;
/*     */ import java.net.Socket;
/*     */ import java.net.SocketException;
/*     */ 
/*     */ class LoggingReceiver extends Thread
/*     */ {
/*  37 */   private static final Logger LOG = Logger.getLogger(LoggingReceiver.class);
/*     */   private MyTableModel mModel;
/*     */   private ServerSocket mSvrSock;
/*     */ 
/*     */   LoggingReceiver(MyTableModel aModel, int aPort)
/*     */     throws IOException
/*     */   {
/* 100 */     setDaemon(true);
/* 101 */     this.mModel = aModel;
/* 102 */     this.mSvrSock = new ServerSocket(aPort);
/*     */   }
/*     */ 
/*     */   public void run()
/*     */   {
/* 107 */     LOG.info("Thread started");
/*     */     try {
/*     */       while (true) {
/* 110 */         LOG.debug("Waiting for a connection");
/* 111 */         Socket client = this.mSvrSock.accept();
/* 112 */         LOG.debug("Got a connection from " + client.getInetAddress().getHostName());
/*     */ 
/* 114 */         Thread t = new Thread(new Slurper(client));
/* 115 */         t.setDaemon(true);
/* 116 */         t.start();
/*     */       }
/*     */     } catch (IOException e) {
/* 119 */       LOG.error("Error in accepting connections, stopping.", e);
/*     */     }
/*     */   }
/*     */ 
/*     */   private class Slurper
/*     */     implements Runnable
/*     */   {
/*     */     private final Socket mClient;
/*     */ 
/*     */     Slurper(Socket aClient)
/*     */     {
/*  55 */       this.mClient = aClient;
/*     */     }
/*     */ 
/*     */     public void run()
/*     */     {
/*  60 */       LoggingReceiver.LOG.debug("Starting to get data");
/*     */       try {
/*  62 */         ObjectInputStream ois = new ObjectInputStream(this.mClient.getInputStream());
/*     */         while (true)
/*     */         {
/*  65 */           LoggingEvent event = (LoggingEvent)ois.readObject();
/*  66 */           LoggingReceiver.this.mModel.addEvent(new EventDetails(event));
/*     */         }
/*     */       } catch (EOFException e) {
/*  69 */         LoggingReceiver.LOG.info("Reached EOF, closing connection");
/*     */       } catch (SocketException e) {
/*  71 */         LoggingReceiver.LOG.info("Caught SocketException, closing connection");
/*     */       } catch (IOException e) {
/*  73 */         LoggingReceiver.LOG.warn("Got IOException, closing connection", e);
/*     */       } catch (ClassNotFoundException e) {
/*  75 */         LoggingReceiver.LOG.warn("Got ClassNotFoundException, closing connection", e);
/*     */       }
/*     */       try
/*     */       {
/*  79 */         this.mClient.close();
/*     */       } catch (IOException e) {
/*  81 */         LoggingReceiver.LOG.warn("Error closing connection", e);
/*     */       }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.LoggingReceiver
 * JD-Core Version:    0.6.2
 */