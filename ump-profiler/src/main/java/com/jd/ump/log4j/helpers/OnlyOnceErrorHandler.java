/*    */ package com.jd.ump.log4j.helpers;
/*    */ 
/*    */ import com.jd.ump.log4j.Appender;
/*    */ import com.jd.ump.log4j.Logger;
/*    */ import com.jd.ump.log4j.spi.ErrorHandler;
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ import java.io.InterruptedIOException;
/*    */ 
/*    */ public class OnlyOnceErrorHandler
/*    */   implements ErrorHandler
/*    */ {
/* 43 */   final String WARN_PREFIX = "log4j warning: ";
/* 44 */   final String ERROR_PREFIX = "log4j error: ";
/*    */ 
/* 46 */   boolean firstTime = true;
/*    */ 
/*    */   public void setLogger(Logger logger)
/*    */   {
/*    */   }
/*    */ 
/*    */   public void activateOptions()
/*    */   {
/*    */   }
/*    */ 
/*    */   public void error(String message, Exception e, int errorCode)
/*    */   {
/* 70 */     error(message, e, errorCode, null);
/*    */   }
/*    */ 
/*    */   public void error(String message, Exception e, int errorCode, LoggingEvent event)
/*    */   {
/* 79 */     if (((e instanceof InterruptedIOException)) || ((e instanceof InterruptedException))) {
/* 80 */       Thread.currentThread().interrupt();
/*    */     }
/* 82 */     if (this.firstTime) {
/* 83 */       LogLog.error(message, e);
/* 84 */       this.firstTime = false;
/*    */     }
/*    */   }
/*    */ 
/*    */   public void error(String message)
/*    */   {
/* 95 */     if (this.firstTime) {
/* 96 */       LogLog.error(message);
/* 97 */       this.firstTime = false;
/*    */     }
/*    */   }
/*    */ 
/*    */   public void setAppender(Appender appender)
/*    */   {
/*    */   }
/*    */ 
/*    */   public void setBackupAppender(Appender appender)
/*    */   {
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.helpers.OnlyOnceErrorHandler
 * JD-Core Version:    0.6.2
 */