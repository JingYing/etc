/*      */ package com.jd.ump.log4j.xml;
/*      */ 
/*      */ import com.jd.ump.log4j.LogManager;
/*      */ import com.jd.ump.log4j.helpers.FileWatchdog;
/*      */ 
/*      */ class XMLWatchdog extends FileWatchdog
/*      */ {
/*      */   XMLWatchdog(String filename)
/*      */   {
/* 1113 */     super(filename);
/*      */   }
/*      */ 
/*      */   public void doOnChange()
/*      */   {
/* 1121 */     new DOMConfigurator().doConfigure(this.filename, LogManager.getLoggerRepository());
/*      */   }
/*      */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.xml.XMLWatchdog
 * JD-Core Version:    0.6.2
 */