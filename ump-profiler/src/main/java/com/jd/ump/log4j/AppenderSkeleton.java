/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import com.jd.ump.log4j.helpers.LogLog;
/*     */ import com.jd.ump.log4j.helpers.OnlyOnceErrorHandler;
/*     */ import com.jd.ump.log4j.spi.ErrorHandler;
/*     */ import com.jd.ump.log4j.spi.Filter;
/*     */ import com.jd.ump.log4j.spi.LoggingEvent;
/*     */ import com.jd.ump.log4j.spi.OptionHandler;
/*     */ 
/*     */ public abstract class AppenderSkeleton
/*     */   implements Appender, OptionHandler
/*     */ {
/*     */   protected Layout layout;
/*     */   protected String name;
/*     */   protected Priority threshold;
/*  53 */   protected ErrorHandler errorHandler = new OnlyOnceErrorHandler();
/*     */   protected Filter headFilter;
/*     */   protected Filter tailFilter;
/*  64 */   protected boolean closed = false;
/*     */ 
/*     */   public AppenderSkeleton()
/*     */   {
/*     */   }
/*     */ 
/*     */   protected AppenderSkeleton(boolean isActive)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void activateOptions()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void addFilter(Filter newFilter)
/*     */   {
/* 102 */     if (this.headFilter == null) {
/* 103 */       this.headFilter = (this.tailFilter = newFilter);
/*     */     } else {
/* 105 */       this.tailFilter.setNext(newFilter);
/* 106 */       this.tailFilter = newFilter;
/*     */     }
/*     */   }
/*     */ 
/*     */   protected abstract void append(LoggingEvent paramLoggingEvent);
/*     */ 
/*     */   public void clearFilters()
/*     */   {
/* 128 */     this.headFilter = (this.tailFilter = null);
/*     */   }
/*     */ 
/*     */   public void finalize()
/*     */   {
/* 140 */     if (this.closed) {
/* 141 */       return;
/*     */     }
/* 143 */     LogLog.debug("Finalizing appender named [" + this.name + "].");
/* 144 */     close();
/*     */   }
/*     */ 
/*     */   public ErrorHandler getErrorHandler()
/*     */   {
/* 155 */     return this.errorHandler;
/*     */   }
/*     */ 
/*     */   public Filter getFilter()
/*     */   {
/* 166 */     return this.headFilter;
/*     */   }
/*     */ 
/*     */   public final Filter getFirstFilter()
/*     */   {
/* 178 */     return this.headFilter;
/*     */   }
/*     */ 
/*     */   public Layout getLayout()
/*     */   {
/* 186 */     return this.layout;
/*     */   }
/*     */ 
/*     */   public final String getName()
/*     */   {
/* 197 */     return this.name;
/*     */   }
/*     */ 
/*     */   public Priority getThreshold()
/*     */   {
/* 207 */     return this.threshold;
/*     */   }
/*     */ 
/*     */   public boolean isAsSevereAsThreshold(Priority priority)
/*     */   {
/* 219 */     return (this.threshold == null) || (priority.isGreaterOrEqual(this.threshold));
/*     */   }
/*     */ 
/*     */   public synchronized void doAppend(LoggingEvent event)
/*     */   {
/* 231 */     if (this.closed) {
/* 232 */       LogLog.error("Attempted to append to closed appender named [" + this.name + "].");
/* 233 */       return;
/*     */     }
/*     */ 
/* 236 */     append(event);
/*     */   }
/*     */ 
/*     */   public synchronized void setErrorHandler(ErrorHandler eh)
/*     */   {
/* 246 */     if (eh == null)
/*     */     {
/* 249 */       LogLog.warn("You have tried to set a null error-handler.");
/*     */     }
/* 251 */     else this.errorHandler = eh;
/*     */   }
/*     */ 
/*     */   public void setLayout(Layout layout)
/*     */   {
/* 263 */     this.layout = layout;
/*     */   }
/*     */ 
/*     */   public void setName(String name)
/*     */   {
/* 272 */     this.name = name;
/*     */   }
/*     */ 
/*     */   public void setThreshold(Priority threshold)
/*     */   {
/* 287 */     this.threshold = threshold;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.AppenderSkeleton
 * JD-Core Version:    0.6.2
 */