package com.jd.ump.log4j.spi;

public abstract interface ThrowableRendererSupport
{
  public abstract ThrowableRenderer getThrowableRenderer();

  public abstract void setThrowableRenderer(ThrowableRenderer paramThrowableRenderer);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.ThrowableRendererSupport
 * JD-Core Version:    0.6.2
 */