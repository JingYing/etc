package com.jd.ump.log4j.spi;

import com.jd.ump.log4j.Logger;

public abstract interface LoggerFactory
{
  public abstract Logger makeNewLoggerInstance(String paramString);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.LoggerFactory
 * JD-Core Version:    0.6.2
 */