/*     */ package com.jd.ump.log4j.spi;
/*     */ 
/*     */ public abstract class Filter
/*     */   implements OptionHandler
/*     */ {
/*     */ 
/*     */   /** @deprecated */
/*     */   public Filter next;
/*     */   public static final int DENY = -1;
/*     */   public static final int NEUTRAL = 0;
/*     */   public static final int ACCEPT = 1;
/*     */ 
/*     */   public void activateOptions()
/*     */   {
/*     */   }
/*     */ 
/*     */   public abstract int decide(LoggingEvent paramLoggingEvent);
/*     */ 
/*     */   public void setNext(Filter next)
/*     */   {
/* 114 */     this.next = next;
/*     */   }
/*     */ 
/*     */   public Filter getNext()
/*     */   {
/* 121 */     return this.next;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.Filter
 * JD-Core Version:    0.6.2
 */