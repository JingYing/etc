/*    */ package com.jd.ump.log4j.lf5.viewer;
/*    */ 
/*    */ import javax.swing.table.DefaultTableModel;
/*    */ 
/*    */ public class LogTableModel extends DefaultTableModel
/*    */ {
/*    */   private static final long serialVersionUID = 3593300685868700894L;
/*    */ 
/*    */   public LogTableModel(Object[] colNames, int numRows)
/*    */   {
/* 49 */     super(colNames, numRows);
/*    */   }
/*    */ 
/*    */   public boolean isCellEditable(int row, int column)
/*    */   {
/* 57 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.viewer.LogTableModel
 * JD-Core Version:    0.6.2
 */