/*    */ package com.jd.ump.log4j.pattern;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.LocationInfo;
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ 
/*    */ public final class FullLocationPatternConverter extends LoggingEventPatternConverter
/*    */ {
/* 34 */   private static final FullLocationPatternConverter INSTANCE = new FullLocationPatternConverter();
/*    */ 
/*    */   private FullLocationPatternConverter()
/*    */   {
/* 41 */     super("Full Location", "fullLocation");
/*    */   }
/*    */ 
/*    */   public static FullLocationPatternConverter newInstance(String[] options)
/*    */   {
/* 51 */     return INSTANCE;
/*    */   }
/*    */ 
/*    */   public void format(LoggingEvent event, StringBuffer output)
/*    */   {
/* 58 */     LocationInfo locationInfo = event.getLocationInformation();
/*    */ 
/* 60 */     if (locationInfo != null)
/* 61 */       output.append(locationInfo.fullInfo);
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.pattern.FullLocationPatternConverter
 * JD-Core Version:    0.6.2
 */