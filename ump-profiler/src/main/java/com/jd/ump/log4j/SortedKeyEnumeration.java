/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import java.util.Enumeration;
/*     */ import java.util.Hashtable;
/*     */ import java.util.Vector;
/*     */ 
/*     */ class SortedKeyEnumeration
/*     */   implements Enumeration
/*     */ {
/*     */   private Enumeration e;
/*     */ 
/*     */   public SortedKeyEnumeration(Hashtable ht)
/*     */   {
/* 943 */     Enumeration f = ht.keys();
/* 944 */     Vector keys = new Vector(ht.size());
/* 945 */     for (int last = 0; f.hasMoreElements(); last++) {
/* 946 */       String key = (String)f.nextElement();
/* 947 */       for (int i = 0; i < last; i++) {
/* 948 */         String s = (String)keys.get(i);
/* 949 */         if (key.compareTo(s) <= 0) break;
/*     */       }
/* 951 */       keys.add(i, key);
/*     */     }
/* 953 */     this.e = keys.elements();
/*     */   }
/*     */ 
/*     */   public boolean hasMoreElements() {
/* 957 */     return this.e.hasMoreElements();
/*     */   }
/*     */ 
/*     */   public Object nextElement() {
/* 961 */     return this.e.nextElement();
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.SortedKeyEnumeration
 * JD-Core Version:    0.6.2
 */