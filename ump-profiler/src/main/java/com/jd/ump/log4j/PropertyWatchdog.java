/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import com.jd.ump.log4j.helpers.FileWatchdog;
/*     */ 
/*     */ class PropertyWatchdog extends FileWatchdog
/*     */ {
/*     */   PropertyWatchdog(String filename)
/*     */   {
/* 914 */     super(filename);
/*     */   }
/*     */ 
/*     */   public void doOnChange()
/*     */   {
/* 922 */     new PropertyConfigurator().doConfigure(this.filename, LogManager.getLoggerRepository());
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.PropertyWatchdog
 * JD-Core Version:    0.6.2
 */