/*    */ package com.jd.ump.profiler.common;
/*    */ 
/*    */ import com.jd.ump.profiler.jvm.JvmInfoPicker;
/*    */ import com.jd.ump.profiler.jvm.JvmInfoPickerFactory;
/*    */ import com.jd.ump.profiler.util.CacheUtil;
/*    */ import com.jd.ump.profiler.util.CustomLogger;
/*    */ import com.jd.ump.profiler.util.LogFormatter;
/*    */ import java.util.Map;
/*    */ import java.util.Map.Entry;
/*    */ 
/*    */ public class BizModule
/*    */ {
/*    */   private static final String LOG_TYPE = "BIZ";
/*    */   private static final String PROC_TYPE = "4";
/* 17 */   private static final String INSTANCE_ID = JvmInfoPickerFactory.create("Local").getJvmInstanceCode();
/*    */ 
/* 22 */   private static final String BIZ_LOG_TEMPLATE1 = new StringBuilder().append("{\"bTime\":\"{}\",\"logtype\":\"BIZ\",\"bKey\":\"{}\",\"bHost\":\"").append(CacheUtil.HOST_NAME).append("\"").append(",\"type\":\"{}\"").append(",\"{}\":\"{}\"").append("}").toString();
/*    */ 
/* 32 */   private static final String BIZ_LOG_TEMPLATE2 = new StringBuilder().append("{\"bTime\":\"{}\",\"logtype\":\"BIZ\",\"bKey\":\"{}\",\"bHost\":\"").append(CacheUtil.HOST_NAME).append("\"").append(",\"type\":\"{}\"").append("{}").append("}").toString();
/*    */ 
/* 43 */   private static final String PROC_LOG_TEMPLATE = new StringBuilder().append("{\"bTime\":\"{}\",\"logtype\":\"BIZ\",\"bKey\":\"{}\",\"bHost\":\"").append(CacheUtil.HOST_NAME).append("\"").append(",\"type\":\"").append("4").append("\"").append(",\"iCode\":\"").append(INSTANCE_ID).append("\"").append(",\"data\":{}").append("}").toString();
/*    */ 
/*    */   public static void bizHandle(String key, int type, Number value)
/*    */   {
/*    */     try
/*    */     {
/* 54 */       CustomLogger.BizLogger.info(LogFormatter.format(BIZ_LOG_TEMPLATE1, new Object[] { CacheUtil.getNowTime(), key, Integer.valueOf(type), type == 1 ? "bValue" : "bCount", value }));
/*    */     } catch (Exception e) {
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void bizHandle(String key, int type, Map<String, ?> dataMap) {
/*    */     try {
/* 61 */       StringBuilder sb = new StringBuilder(32);
/*    */ 
/* 63 */       for (Map.Entry entry : dataMap.entrySet()) {
/* 64 */         sb.append(",").append("\"").append((String)entry.getKey()).append("\"").append(":").append("\"").append(entry.getValue()).append("\"");
/*    */       }
/*    */ 
/* 68 */       Object[] args = { CacheUtil.getNowTime(), key, Integer.valueOf(type), sb.toString() };
/*    */ 
/* 70 */       CustomLogger.BizLogger.info(LogFormatter.format(BIZ_LOG_TEMPLATE2, args));
/*    */     } catch (Exception e) {
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void bizNode(String nodeID, String data) {
/* 76 */     CustomLogger.BizLogger.info(LogFormatter.format(PROC_LOG_TEMPLATE, new Object[] { CacheUtil.getNowTime(), nodeID, data }));
/*    */   }
/*    */ 
/*    */   public static void bizNode(String nodeID, Map<String, String> data) {
/* 80 */     StringBuilder sb = new StringBuilder("{");
/*    */ 
/* 82 */     for (Map.Entry entry : data.entrySet()) {
/* 83 */       sb.append("\"").append((String)entry.getKey()).append("\"").append(":").append("\"").append((String)entry.getValue()).append("\"").append(",");
/*    */     }
/*    */ 
/* 87 */     int length = sb.length();
/*    */ 
/* 89 */     if (length > 1)
/*    */     {
/* 91 */       sb.deleteCharAt(length - 1);
/*    */     }
/*    */ 
/* 94 */     sb.append("}");
/*    */ 
/* 96 */     Object[] args = { CacheUtil.getNowTime(), nodeID, sb.toString() };
/*    */ 
/* 98 */     CustomLogger.BizLogger.info(LogFormatter.format(PROC_LOG_TEMPLATE, args));
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.common.BizModule
 * JD-Core Version:    0.6.2
 */