/*     */ package com.jd.ump.profiler;
/*     */ 
/*     */ import com.jd.ump.profiler.common.AliveModule;
/*     */ import com.jd.ump.profiler.common.BizModule;
/*     */ import com.jd.ump.profiler.common.BusinessModule;
/*     */ import com.jd.ump.profiler.common.CommonModule;
/*     */ import com.jd.ump.profiler.common.JvmModule;
/*     */ import com.jd.ump.profiler.util.CacheUtil;
/*     */ import com.jd.ump.profiler.util.CustomLogger;
/*     */ import com.jd.ump.profiler.util.TPCounter;
/*     */ import java.util.Map;
/*     */ import java.util.Timer;
/*     */ import java.util.TimerTask;
/*     */ 
/*     */ public final class ProfilerImpl
/*     */ {
/*  17 */   private static final TPCounter tpCounter = TPCounter.getInstance();
/*     */ 
/*     */   public static CallerInfo scopeStart(String key, boolean enableHeart, boolean enableTP)
/*     */   {
/*  31 */     CallerInfo callerInfo = null;
/*     */     try
/*     */     {
/*  34 */       if (enableTP) {
/*  35 */         callerInfo = new CallerInfo(key);
/*     */       }
/*     */ 
/*  38 */       if (enableHeart) {
/*  39 */         if (!CacheUtil.FUNC_HB.containsKey(key))
/*     */         {
/*  41 */           synchronized (CacheUtil.FUNC_HB) {
/*  42 */             if (!CacheUtil.FUNC_HB.containsKey(key))
/*     */             {
/*  44 */               CacheUtil.FUNC_HB.put(key, Long.valueOf(System.currentTimeMillis()));
/*     */ 
/*  46 */               CustomLogger.AliveLogger.info("{\"key\":\"" + key + "\"" + ",\"hostname\":" + "\"" + CacheUtil.HOST_NAME + "\"" + ",\"time\":" + "\"" + CacheUtil.getNowTime() + "\"}");
/*     */             }
/*     */ 
/*     */           }
/*     */ 
/*     */         }
/*     */         else
/*     */         {
/*  54 */           Long hbPoint = (Long)CacheUtil.FUNC_HB.get(key);
/*  55 */           if (System.currentTimeMillis() - hbPoint.longValue() >= 20000L)
/*     */           {
/*  57 */             synchronized (CacheUtil.FUNC_HB) {
/*  58 */               if (System.currentTimeMillis() - ((Long)CacheUtil.FUNC_HB.get(key)).longValue() >= 20000L) {
/*  59 */                 CacheUtil.FUNC_HB.put(key, Long.valueOf(System.currentTimeMillis()));
/*  60 */                 CustomLogger.AliveLogger.info("{\"key\":\"" + key + "\"" + ",\"hostname\":" + "\"" + CacheUtil.HOST_NAME + "\"" + ",\"time\":" + "\"" + CacheUtil.getNowTime() + "\"}");
/*     */               }
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */ 
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */ 
/*  72 */     return callerInfo;
/*     */   }
/*     */ 
/*     */   public static CallerInfo scopeStart(String key)
/*     */   {
/*  85 */     return scopeStart(key, false, true);
/*     */   }
/*     */ 
/*     */   public static void scopeEnd(CallerInfo callerInfo) {
/*     */     try {
/*  90 */       if (callerInfo != null)
/*     */       {
/*  92 */         if (callerInfo.getProcessState() == 1) {
/*  93 */           callerInfo.stop();
/*     */         } else {
/*  95 */           long elapsedTime = callerInfo.getElapsedTime();
/*  96 */           if (elapsedTime >= 400L)
/*  97 */             callerInfo.stop();
/*     */           else
/*  99 */             tpCounter.count(callerInfo, elapsedTime);
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (Throwable t) {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static synchronized void scopeAlive(String key) {
/*     */     try {
/* 109 */       if (!CacheUtil.SYSTEM_HEART_INIT.booleanValue()) {
/* 110 */         Timer timer = new Timer("UMP-AliveThread", true);
/* 111 */         timer.scheduleAtFixedRate(new AliveModule(key), 1000L, 20000L);
/* 112 */         CacheUtil.SYSTEM_HEART_INIT = Boolean.valueOf(true);
/*     */       }
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void registerBusiness(String key, int type, int value, String detail)
/*     */   {
/*     */     try
/*     */     {
/* 128 */       BusinessModule.businessHandle(key, type, value, detail);
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void registerBusiness(String key, int type, int value, String detail, String rtxList, String mailList, String smsList)
/*     */   {
/*     */     try
/*     */     {
/* 147 */       BusinessModule.businessHandle(key, type, value, detail, rtxList, mailList, smsList);
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void scopeFunctionError(CallerInfo callerInfo)
/*     */   {
/*     */     try
/*     */     {
/* 159 */       if (callerInfo != null)
/* 160 */         callerInfo.error();
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void registerBizData(String key, int type, Number value)
/*     */   {
/*     */     try
/*     */     {
/* 175 */       BizModule.bizHandle(key, type, value);
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void registerBizData(String key, int type, Map<String, ?> dataMap)
/*     */   {
/*     */     try
/*     */     {
/* 189 */       BizModule.bizHandle(key, type, dataMap);
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static synchronized void registerJvmData(String key)
/*     */   {
/*     */     try
/*     */     {
/* 201 */       if (!CacheUtil.JVM_MONITOR_INIT.booleanValue())
/*     */       {
/* 204 */         Timer timer = new Timer("UMP-CollectJvmInfoThread", true);
/* 205 */         timer.scheduleAtFixedRate(new JvmModule(key), 1000L, 10000L);
/*     */ 
/* 207 */         timer.scheduleAtFixedRate(new TimerTask()
/*     */         {
/*     */           public void run() {
/* 210 */             JvmModule.jvmHandle(this.val$key);
/*     */           }
/*     */         }
/*     */         , 0L, 14400000L);
/*     */ 
/* 215 */         CacheUtil.JVM_MONITOR_INIT = Boolean.valueOf(true);
/*     */       }
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void bizNode(String nodeID, Map<String, String> data)
/*     */   {
/* 228 */     BizModule.bizNode(nodeID, data);
/*     */   }
/*     */ 
/*     */   public static void bizNode(String nodeID, String data)
/*     */   {
/* 238 */     BizModule.bizNode(nodeID, data);
/*     */   }
/*     */ 
/*     */   public static void log(String type, String data)
/*     */   {
/* 247 */     CommonModule.log(type, data);
/*     */   }
/*     */ 
/*     */   public static void log(String type, Map<String, String> data)
/*     */   {
/* 256 */     CommonModule.log(type, data);
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.ProfilerImpl
 * JD-Core Version:    0.6.2
 */