/*     */ package com.jd.ump.profiler.util;
/*     */ 
/*     */ import com.jd.ump.log4j.Hierarchy;
/*     */ import com.jd.ump.log4j.Level;
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import com.jd.ump.log4j.PropertyConfigurator;
/*     */ import com.jd.ump.log4j.RollingFileAppender;
/*     */ import com.jd.ump.log4j.SimpleLayout;
/*     */ import com.jd.ump.log4j.spi.LoggerRepository;
/*     */ import com.jd.ump.log4j.spi.RootLogger;
/*     */ import com.jd.ump.profiler.common.UpdateModule;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.lang.management.ManagementFactory;
/*     */ import java.lang.management.RuntimeMXBean;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.Properties;
/*     */ import java.util.Random;
/*     */ import java.util.Timer;
/*     */ import java.util.TimerTask;
/*     */ 
/*     */ public class CustomLogFactory
/*     */ {
/*  27 */   private static LoggerRepository h = new Hierarchy(new RootLogger(Level.INFO));
/*     */   private static final String DEFAULTPATH = "/export/home/tomcat/UMP-Monitor";
/*  30 */   private static String MaxFileSize = "50MB";
/*  31 */   private static int MaxBackupIndex = 3;
/*  32 */   private static String logPath = null;
/*     */ 
/*  34 */   private static String TIMESTAMP = getTimeStamp();
/*  35 */   private static int PID = getPid();
/*  36 */   private static int RANDOM_CODE = getRandomCode();
/*     */ 
/*  38 */   private static long CHECK_FILE_REMOVED_PERIOD = 2000L;
/*     */ 
/* 161 */   private static int tpLoggerFileRemovedCount = 0;
/* 162 */   private static int aliveLoggerFileRemovedCount = 0;
/* 163 */   private static int businessLoggerFileRemovedCount = 0;
/* 164 */   private static int bizLoggerFileRemovedCount = 0;
/* 165 */   private static int jvmLoggerFileRemovedCount = 0;
/* 166 */   private static int commonLoggerFileRemovedCount = 0;
/*     */ 
/*     */   private static int getPid()
/*     */   {
/*     */     try
/*     */     {
/*  92 */       RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
/*     */ 
/*  94 */       String name = runtime.getName();
/*  95 */       return Integer.parseInt(name.substring(0, name.indexOf('@'))); } catch (Throwable e) {
/*     */     }
/*  97 */     return new Random().nextInt(50000) + 9900000;
/*     */   }
/*     */ 
/*     */   private static int getRandomCode()
/*     */   {
/* 106 */     return new Random().nextInt(1000000);
/*     */   }
/*     */ 
/*     */   private static String getTimeStamp()
/*     */   {
/* 114 */     return new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date());
/*     */   }
/*     */ 
/*     */   public static Logger getLogger(String loggerName) {
/* 118 */     return h.getLogger(loggerName);
/*     */   }
/*     */ 
/*     */   private static Properties InitLog4jProperties(String path)
/*     */   {
/* 123 */     Properties properties = new Properties();
/*     */ 
/* 125 */     String tpLogFile = createFileName("tp.log");
/* 126 */     String aliveLogFile = createFileName("alive.log");
/* 127 */     String businessLogFile = createFileName("business.log");
/* 128 */     String bizLogFile = createFileName("biz.log");
/* 129 */     String jvmLogFile = createFileName("jvm.log");
/* 130 */     String commonLogFile = createFileName("common.log");
/*     */ 
/* 132 */     setProperties(properties, "tpLogger", "A1", tpLogFile);
/* 133 */     setProperties(properties, "aliveLogger", "A2", aliveLogFile);
/* 134 */     setProperties(properties, "businessLogger", "A3", businessLogFile);
/* 135 */     setProperties(properties, "bizLogger", "A4", bizLogFile);
/* 136 */     setProperties(properties, "jvmLogger", "A5", jvmLogFile);
/* 137 */     setProperties(properties, "commonLogger", "A6", commonLogFile);
/*     */ 
/* 139 */     return properties;
/*     */   }
/*     */ 
/*     */   private static void setProperties(Properties properties, String loggerName, String appenderName, String fileName) {
/* 143 */     properties.setProperty(String.format("log4j.logger.%s", new Object[] { loggerName }), String.format("INFO,%s", new Object[] { appenderName }));
/* 144 */     properties.setProperty(String.format("log4j.appender.%s", new Object[] { appenderName }), "com.jd.ump.log4j.RollingFileAppender");
/* 145 */     properties.setProperty(String.format("log4j.appender.%s.File", new Object[] { appenderName }), fileName);
/* 146 */     properties.setProperty(String.format("log4j.appender.%s.MaxFileSize", new Object[] { appenderName }), MaxFileSize);
/* 147 */     properties.setProperty(String.format("log4j.appender.%s.MaxBackupIndex", new Object[] { appenderName }), String.valueOf(MaxBackupIndex));
/* 148 */     properties.setProperty(String.format("log4j.appender.%s.layout", new Object[] { appenderName }), "com.jd.ump.log4j.SimpleLayout");
/* 149 */     properties.setProperty(String.format("log4j.appender.%s.encoding", new Object[] { appenderName }), "UTF-8");
/*     */   }
/*     */ 
/*     */   private static String createFileName(String name)
/*     */   {
/* 158 */     return logPath + TIMESTAMP + "_" + PID + "_" + RANDOM_CODE + "_" + name;
/*     */   }
/*     */ 
/*     */   static
/*     */   {
/*  41 */     Properties conf = new Properties();
/*  42 */     Properties props = null;
/*  43 */     InputStream is = null;
/*     */     try {
/*  45 */       is = CacheUtil.class.getResourceAsStream("/config.properties");
/*  46 */       if (is != null) {
/*  47 */         conf.load(is);
/*  48 */         logPath = conf.getProperty("jiankonglogPath", "/export/home/tomcat/UMP-Monitor");
/*  49 */         if (logPath.equals(""))
/*  50 */           logPath = "/export/home/tomcat/UMP-Monitor";
/*     */       }
/*     */       else {
/*  53 */         logPath = "/export/home/tomcat/UMP-Monitor";
/*     */       }
/*     */ 
/*  56 */       logPath = logPath + File.separator + "logs" + File.separator;
/*  57 */       File ump_root_path = new File(logPath);
/*  58 */       if ((ump_root_path.exists()) && (ump_root_path.isDirectory())) {
/*  59 */         props = InitLog4jProperties(logPath);
/*     */       } else {
/*  61 */         ump_root_path.mkdir();
/*  62 */         props = InitLog4jProperties(logPath);
/*     */       }
/*     */ 
/*  67 */       new PropertyConfigurator().doConfigure(props, h);
/*     */ 
/*  69 */       Timer timer = new Timer("UMP-ProfilerFileUpdateThread", true);
/*  70 */       timer.scheduleAtFixedRate(new UpdateModule(), 43200000L, 43200000L);
/*     */ 
/*  72 */       Timer checkFileRemovedTimer = new Timer("UMP-CheckFileRemovedThread", true);
/*  73 */       checkFileRemovedTimer.schedule(new CheckFileRemovedTask(), CHECK_FILE_REMOVED_PERIOD, CHECK_FILE_REMOVED_PERIOD);
/*     */     } catch (Throwable e) {
/*     */     } finally {
/*  76 */       if (is != null)
/*     */         try {
/*  78 */           is.close();
/*     */         }
/*     */         catch (Throwable ex)
/*     */         {
/*     */         }
/*     */     }
/*     */   }
/*     */ 
/*     */   static class CheckFileRemovedTask extends TimerTask
/*     */   {
/*     */     public void run()
/*     */     {
/*     */       try
/*     */       {
/* 177 */         checkFileRemoved(CustomLogger.TpLogger.getLogger(), "tp.log", "A1");
/* 178 */         checkFileRemoved(CustomLogger.AliveLogger.getLogger(), "alive.log", "A2");
/* 179 */         checkFileRemoved(CustomLogger.BusinessLogger.getLogger(), "business.log", "A3");
/* 180 */         checkFileRemoved(CustomLogger.BizLogger.getLogger(), "biz.log", "A4");
/* 181 */         checkFileRemoved(CustomLogger.JVMLogger.getLogger(), "jvm.log", "A5");
/* 182 */         checkFileRemoved(CustomLogger.CommonLogger.getLogger(), "common.log", "A6");
/*     */       }
/*     */       catch (Throwable e)
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     private void checkFileRemoved(Logger logger, String logName, String appenderName)
/*     */       throws IOException
/*     */     {
/* 195 */       RollingFileAppender appender = (RollingFileAppender)logger.getAppender(appenderName);
/*     */ 
/* 197 */       if (appender != null) {
/* 198 */         String oldFile = appender.getFile();
/* 199 */         File file = new File(oldFile);
/* 200 */         if (file.exists())
/*     */         {
/* 202 */           setCount2Zero(logName);
/*     */         }
/*     */         else {
/* 205 */           incrCount(logName);
/*     */         }
/*     */ 
/* 209 */         if (getCount(logName) >= 2) {
/* 210 */           String newFile = CustomLogFactory.createFileName(logName);
/*     */ 
/* 215 */           SimpleLayout layout = new SimpleLayout();
/*     */ 
/* 217 */           RollingFileAppender newAppender = new RollingFileAppender(layout, newFile);
/* 218 */           newAppender.setName(appenderName);
/* 219 */           newAppender.setMaxFileSize(CustomLogFactory.MaxFileSize);
/* 220 */           newAppender.setMaxBackupIndex(CustomLogFactory.MaxBackupIndex);
/* 221 */           newAppender.setEncoding("UTF-8");
/*     */ 
/* 224 */           logger.removeAppender(appenderName);
/* 225 */           logger.addAppender(newAppender);
/*     */ 
/* 228 */           setCount2Zero(logName);
/*     */         }
/*     */       }
/*     */     }
/*     */ 
/*     */     private int getCount(String logName)
/*     */     {
/* 239 */       if ("tp.log".equals(logName)) {
/* 240 */         return CustomLogFactory.tpLoggerFileRemovedCount;
/*     */       }
/*     */ 
/* 243 */       if ("alive.log".equals(logName)) {
/* 244 */         return CustomLogFactory.aliveLoggerFileRemovedCount;
/*     */       }
/*     */ 
/* 247 */       if ("business.log".equals(logName)) {
/* 248 */         return CustomLogFactory.businessLoggerFileRemovedCount;
/*     */       }
/*     */ 
/* 251 */       if ("biz.log".equals(logName)) {
/* 252 */         return CustomLogFactory.bizLoggerFileRemovedCount;
/*     */       }
/*     */ 
/* 255 */       if ("jvm.log".equals(logName)) {
/* 256 */         return CustomLogFactory.jvmLoggerFileRemovedCount;
/*     */       }
/*     */ 
/* 259 */       if ("common.log".equals(logName)) {
/* 260 */         return CustomLogFactory.commonLoggerFileRemovedCount;
/*     */       }
/*     */ 
/* 263 */       return 0;
/*     */     }
/*     */ 
/*     */     private void setCount2Zero(String logName)
/*     */     {
/* 271 */       if ("tp.log".equals(logName)) {
/* 272 */         CustomLogFactory.access$302(0);
/*     */       }
/*     */ 
/* 275 */       if ("alive.log".equals(logName)) {
/* 276 */         CustomLogFactory.access$402(0);
/*     */       }
/*     */ 
/* 279 */       if ("business.log".equals(logName)) {
/* 280 */         CustomLogFactory.access$502(0);
/*     */       }
/*     */ 
/* 283 */       if ("biz.log".equals(logName)) {
/* 284 */         CustomLogFactory.access$602(0);
/*     */       }
/*     */ 
/* 287 */       if ("jvm.log".equals(logName)) {
/* 288 */         CustomLogFactory.access$702(0);
/*     */       }
/*     */ 
/* 291 */       if ("common.log".equals(logName))
/* 292 */         CustomLogFactory.access$802(0);
/*     */     }
/*     */ 
/*     */     private void incrCount(String logName)
/*     */     {
/* 301 */       if ("tp.log".equals(logName)) {
/* 302 */         CustomLogFactory.access$308();
/*     */       }
/*     */ 
/* 305 */       if ("alive.log".equals(logName)) {
/* 306 */         CustomLogFactory.access$408();
/*     */       }
/*     */ 
/* 309 */       if ("business.log".equals(logName)) {
/* 310 */         CustomLogFactory.access$508();
/*     */       }
/*     */ 
/* 313 */       if ("biz.log".equals(logName)) {
/* 314 */         CustomLogFactory.access$608();
/*     */       }
/*     */ 
/* 317 */       if ("jvm.log".equals(logName)) {
/* 318 */         CustomLogFactory.access$708();
/*     */       }
/*     */ 
/* 321 */       if ("common.log".equals(logName))
/* 322 */         CustomLogFactory.access$808();
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.CustomLogFactory
 * JD-Core Version:    0.6.2
 */