/*    */ package com.jd.ump.profiler.util;
/*    */ 
/*    */ import com.jd.ump.log4j.Level;
/*    */ import com.jd.ump.log4j.Logger;
/*    */ import com.jd.ump.log4j.helpers.LogLog;
/*    */ 
/*    */ public final class RootLogger extends Logger
/*    */ {
/*    */   public RootLogger(Level level)
/*    */   {
/* 26 */     super("root");
/* 27 */     setLevel(level);
/*    */   }
/*    */ 
/*    */   public final Level getChainedLevel()
/*    */   {
/* 34 */     return this.level;
/*    */   }
/*    */ 
/*    */   public final void setLevel(Level level)
/*    */   {
/* 44 */     if (level == null)
/* 45 */       LogLog.error("You have tried to set a null level to root.", new Throwable());
/*    */     else
/* 47 */       this.level = level;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.RootLogger
 * JD-Core Version:    0.6.2
 */