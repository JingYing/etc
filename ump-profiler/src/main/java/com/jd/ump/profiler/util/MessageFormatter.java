/*     */ package com.jd.ump.profiler.util;
/*     */ 
/*     */ public final class MessageFormatter
/*     */ {
/*     */   static final char DELIM_START = '{';
/*     */   static final char DELIM_STOP = '}';
/*     */   static final String DELIM_STR = "{}";
/*     */   private static final char ESCAPE_CHAR = '\\';
/*     */ 
/*     */   public static final String arrayFormat(String messagePattern, Object[] argArray)
/*     */   {
/* 115 */     if (messagePattern == null) {
/* 116 */       return null;
/*     */     }
/*     */ 
/* 119 */     if (argArray == null) {
/* 120 */       return messagePattern;
/*     */     }
/*     */ 
/* 123 */     int i = 0;
/*     */ 
/* 125 */     StringBuilder sbuf = new StringBuilder(messagePattern.length() + 64);
/*     */ 
/* 128 */     for (int L = 0; L < argArray.length; L++)
/*     */     {
/* 130 */       int j = messagePattern.indexOf("{}", i);
/*     */ 
/* 132 */       if (j == -1)
/*     */       {
/* 134 */         if (i == 0) {
/* 135 */           return messagePattern;
/*     */         }
/*     */ 
/* 138 */         sbuf.append(messagePattern.substring(i, messagePattern.length()));
/* 139 */         return sbuf.toString();
/*     */       }
/*     */ 
/* 142 */       if (isEscapedDelimeter(messagePattern, j)) {
/* 143 */         if (!isDoubleEscaped(messagePattern, j)) {
/* 144 */           L--;
/* 145 */           sbuf.append(messagePattern.substring(i, j - 1));
/* 146 */           sbuf.append('{');
/* 147 */           i = j + 1;
/*     */         }
/*     */         else
/*     */         {
/* 152 */           sbuf.append(messagePattern.substring(i, j - 1));
/* 153 */           deeplyAppendParameter(sbuf, argArray[L]);
/* 154 */           i = j + 2;
/*     */         }
/*     */       }
/*     */       else {
/* 158 */         sbuf.append(messagePattern.substring(i, j));
/* 159 */         deeplyAppendParameter(sbuf, argArray[L]);
/* 160 */         i = j + 2;
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 165 */     sbuf.append(messagePattern.substring(i, messagePattern.length()));
/* 166 */     return sbuf.toString();
/*     */   }
/*     */ 
/*     */   static final boolean isEscapedDelimeter(String messagePattern, int delimeterStartIndex)
/*     */   {
/* 172 */     if (delimeterStartIndex == 0) {
/* 173 */       return false;
/*     */     }
/* 175 */     char potentialEscape = messagePattern.charAt(delimeterStartIndex - 1);
/* 176 */     if (potentialEscape == '\\') {
/* 177 */       return true;
/*     */     }
/* 179 */     return false;
/*     */   }
/*     */ 
/*     */   static final boolean isDoubleEscaped(String messagePattern, int delimeterStartIndex)
/*     */   {
/* 185 */     if ((delimeterStartIndex >= 2) && (messagePattern.charAt(delimeterStartIndex - 2) == '\\'))
/*     */     {
/* 187 */       return true;
/*     */     }
/* 189 */     return false;
/*     */   }
/*     */ 
/*     */   private static void deeplyAppendParameter(StringBuilder sbuf, Object o)
/*     */   {
/* 195 */     if (o == null) {
/* 196 */       sbuf.append("null");
/* 197 */       return;
/*     */     }
/*     */ 
/* 200 */     safeObjectAppend(sbuf, o);
/*     */   }
/*     */ 
/*     */   private static void safeObjectAppend(StringBuilder sbuf, Object o) {
/*     */     try {
/* 205 */       String oAsString = o.toString();
/* 206 */       sbuf.append(oAsString);
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.MessageFormatter
 * JD-Core Version:    0.6.2
 */