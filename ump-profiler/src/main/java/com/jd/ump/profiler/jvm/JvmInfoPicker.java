package com.jd.ump.profiler.jvm;

public abstract interface JvmInfoPicker
{
  public abstract String pickJvmEnvironmentInfo();

  public abstract String pickJvmRumtimeInfo();

  public abstract String getJvmInstanceCode();
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.jvm.JvmInfoPicker
 * JD-Core Version:    0.6.2
 */