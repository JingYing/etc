/*    */ package com.jd.ump.profiler.util;
/*    */ 
/*    */ import java.util.Set;
/*    */ import java.util.concurrent.ConcurrentHashMap;
/*    */ import java.util.concurrent.ConcurrentMap;
/*    */ import java.util.concurrent.atomic.AtomicInteger;
/*    */ 
/*    */ public class ConcurrentHashMultiSet<E>
/*    */ {
/*    */   private final transient ConcurrentMap<E, AtomicInteger> counterMap;
/*    */ 
/*    */   public ConcurrentHashMultiSet()
/*    */   {
/* 19 */     this.counterMap = new ConcurrentHashMap();
/*    */   }
/*    */ 
/*    */   public int add(E element)
/*    */   {
/* 28 */     if (element == null) {
/* 29 */       return 0;
/*    */     }
/*    */ 
/* 32 */     AtomicInteger existingCounter = (AtomicInteger)this.counterMap.get(element);
/* 33 */     if (existingCounter == null)
/*    */     {
/* 35 */       AtomicInteger newCounter = new AtomicInteger();
/* 36 */       existingCounter = (AtomicInteger)this.counterMap.putIfAbsent(element, newCounter);
/* 37 */       if (existingCounter == null)
/*    */       {
/* 39 */         existingCounter = newCounter;
/*    */       }
/*    */ 
/*    */     }
/*    */ 
/* 44 */     return existingCounter.incrementAndGet();
/*    */   }
/*    */ 
/*    */   public Set<E> elementSet()
/*    */   {
/* 52 */     return this.counterMap.keySet();
/*    */   }
/*    */ 
/*    */   public int count(E element)
/*    */   {
/* 61 */     if (element == null) {
/* 62 */       return 0;
/*    */     }
/* 64 */     AtomicInteger existingCounter = (AtomicInteger)this.counterMap.get(element);
/* 65 */     return existingCounter == null ? 0 : existingCounter.get();
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.ConcurrentHashMultiSet
 * JD-Core Version:    0.6.2
 */