package com.jd.common.springmvc.interceptor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jd.common.web.DotnetAuthenticationTicket;
import com.jd.common.web.DotnetAuthenticationUtil;
import com.jd.common.web.cookie.CookieUtils;


/**
 * 基于dotnet cookie认证的 将dotnet认证的ticket放到actioncontext
 * <p/>
 * Date: 10-12-16 Time: 上午11:20
 * 
 * port from com.jd.common.struts.interceptor.DotnetTicketContextInterceptor,
 * remove dependency with struts.
 * 
 * @see com.jd.common.struts.interceptor.DotnetTicketContextInterceptor
 * 
 * @author xiaofei
 */
public class DotnetTicketContextInterceptor extends HandlerInterceptorAdapter {
    private final static Log log = LogFactory
            .getLog(DotnetTicketContextInterceptor.class);

    /**
     * 读取cookie
     */
    protected CookieUtils cookieUtils;
    /**
     * dotnet的cookie key
     */
    protected String dotnetAuthCookieName = "ceshi3.com";
    /**
     * dotnet cookie的解密key
     */
    protected String dotnetAuthenticationKey;

    @Override
    public final boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler)
            throws ServletException,
            IOException {

        try {
            parseDotnetTicket(request);
        } catch (Exception e) {
            log.warn("update dotnet login error!", e);
        }

        return true;
    }

    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response,
            Object handler, Exception ex)
            throws Exception {
        DotnetAuthenticationTicket.remove();
    }

    /**
     * 解析dotnet的cookie的ticket到actionconext中。
     * 
     * @param request
     * @return
     * @see com.jd.common.struts.context.LoginContext#HTTP_DOTNET_LOGIN_TICKET_CONTEXT
     */
    protected DotnetAuthenticationTicket parseDotnetTicket(
            HttpServletRequest request) {
        if (dotnetAuthCookieName == null) {
            log.debug("dotnetAuthCookieName is null!");
        }
        String cookieValue = "";
        //如果是flex端附件上传，利post参数方式，获取token,
        if(request instanceof DefaultMultipartHttpServletRequest && request.getRequestURI().toString().indexOf("process_uploadFile") != -1){
        	cookieValue = request.getParameter("token");
        } else {
        	cookieValue = cookieUtils.getCookieValue(request,dotnetAuthCookieName);
        }
        if (StringUtils.isNotBlank(cookieValue)) { // 先check passport的cookie有没有
            DotnetAuthenticationTicket ticket = null;
            try {
                ticket = DotnetAuthenticationUtil.getFormsAuthenticationTicket(
                        cookieValue, dotnetAuthenticationKey);
            } catch (Exception e) {
                log.error("decrypt dotnet cookie error!", e);
            }
            if (ticket != null && StringUtils.isNotBlank(ticket.getUsername())
                    && !ticket.isExpired()) {
                DotnetAuthenticationTicket.setTicket(ticket);
                return ticket;
            } else {
                log.debug("tick error or ticket expired!");
            }
        } else {
            log.debug("dotnetAuthCookieName [" + dotnetAuthCookieName
                    + "] is null!");
        }
        return null;
    }

    public CookieUtils getCookieUtils() {
		return cookieUtils;
	}

	public void setCookieUtils(CookieUtils cookieUtils) {
        this.cookieUtils = cookieUtils;
    }
    
    public String getDotnetAuthCookieName() {
		return dotnetAuthCookieName;
	}

	public void setDotnetAuthCookieName(String dotnetAuthCookieName) {
        this.dotnetAuthCookieName = dotnetAuthCookieName;
    }

	public String getDotnetAuthenticationKey() {
		return dotnetAuthenticationKey;
	}
	
    public void setDotnetAuthenticationKey(String dotnetAuthenticationKey) {
        this.dotnetAuthenticationKey = dotnetAuthenticationKey;
    }

}