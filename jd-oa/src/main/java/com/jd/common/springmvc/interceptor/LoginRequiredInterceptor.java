package com.jd.common.springmvc.interceptor;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jd.common.web.url.UrlBuilders;
import com.jd.common.web.url.UrlBuilder.Builder;
import com.jd.common.web.LoginContext;

/**
 * port from com.jd.common.struts.interceptor.SecurityInterceptor, remove
 * dependency with struts.
 * 
 * @see com.jd.common.struts.interceptor.SecurityInterceptor
 * 
 * @author xiaofei
 * 
 */
public class LoginRequiredInterceptor extends HandlerInterceptorAdapter {


    protected UrlBuilders urlBuilders;

    protected String homeModule = "homeModule";
    protected String loginUrl = "loginUrl";


    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws IOException,
            MalformedURLException {
        // check 有没有登录
        LoginContext context = getLoginContext();
        if (context == null || !context.isLogin()) {// 没登录
            response.sendRedirect(getLoginUrl(request));
            return false;
        }
        return true;
    }

    /**
     * 取出登录的信息
     * 
     * @return
     */
    protected LoginContext getLoginContext() {
        return LoginContext.getLoginContext();
    }

    @SuppressWarnings("unchecked")
    protected String getLoginUrl(HttpServletRequest request)
            throws MalformedURLException {
        Builder currentUrlBuilder = urlBuilders.get(homeModule).forPath(
                request.getRequestURI());
        currentUrlBuilder.put(homeModule, request.getParameterMap());

        Builder loginUrlBuilder = urlBuilders.get(loginUrl).forPath(null);

        loginUrlBuilder.put("ReturnUrl", currentUrlBuilder.build());

        return loginUrlBuilder.build();

    }

    public void setUrlBuilders(UrlBuilders urlBuilders) {
        this.urlBuilders = urlBuilders;
    }

    public void setHomeModule(String homeModule) {
        this.homeModule = homeModule;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }
}
