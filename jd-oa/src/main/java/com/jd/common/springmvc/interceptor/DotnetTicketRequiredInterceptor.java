package com.jd.common.springmvc.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jd.common.web.DotnetAuthenticationTicket;

/**
 * 
 * @see com.jd.common.struts.interceptor.NewDotnetSecurityInterceptor
 * @author xiaofei
 * 
 */
public class DotnetTicketRequiredInterceptor extends
        LoginRequiredInterceptor {
    private final static Log log = LogFactory
            .getLog(DotnetTicketRequiredInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler)
            throws IOException {

        DotnetAuthenticationTicket ticket = DotnetAuthenticationTicket
                .getTicket();

        if (ticket == null) { // 先check passport的cookie有没有
            log.debug("actioncontext hasn't a ticked!");
            response.sendRedirect(getLoginUrl(request));
            return false;
        }

        return true;
    }

}
