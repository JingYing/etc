package com.jd.common.hrm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jd.common.cached.CacheUtils;
import com.jd.common.hrm.ws.DeptWebServiceSoap;
import com.jd.common.web.LoginContext;

/**
 * port from com.jd.common.struts.interceptor.HrmPrivilegeHelper. remove
 * dependency with struts, cxf
 * <p>
 * 
 * 权限认证 User: yangsiyong@360buy.com Date: 2010-6-3 Time: 14:21:20
 * 
 * @author xiaofei
 */

public class HrmPrivilegeHelper {
    private final static Log log = LogFactory.getLog(HrmPrivilegeHelper.class);
    private CacheUtils cacheUtils;
    /**
     * 资源权限缓存时间
     */
    protected int privilegeCacheTime = 60 * 15;

    public void setCacheUtils(CacheUtils cacheUtils) {
        this.cacheUtils = cacheUtils;
    }

    private DeptWebServiceSoap deptWebService;

    public void setDeptWebService(DeptWebServiceSoap deptWebService) {
        this.deptWebService = deptWebService;
    }

    /**
     * 判断用户是否对此资源有权限。注意，比较时不分大不写
     * 
     * @param my
     *            被check的权限
     * @param resources
     *            用户所拥有的资源
     * @return true 表示拥有，false 表示没有
     */
    public static boolean checkResource(String[] my, Set<String> resources) {
        if (resources != null && my != null) {
            for (String string : my) {
                if (resources.contains(string)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 取得用户的资源
     * 
     * @param pin
     * @return
     */
    public Set<String> getResources(String pin) {
        Set<String> set = null;
        if (cacheUtils != null) {
            String key = getKey(pin);
            Object obj = null;
            try {
                obj = cacheUtils.get(key);
            } catch (Exception e) {
                log.error("get resources(" + pin + ") from the cache error!", e);
            }
            if (obj != null) {
                if (obj instanceof Set) {
                    log.debug(pin + " resource(" + pin
                            + ") been the cached hit!");
                    return (Set<String>) obj;
                }
            }
            String str = deptWebService.resourceNames(pin);
            if (StringUtils.isNotEmpty(str)) {
                String[] temp = str.split(",");
                set = new HashSet<String>(Arrays.asList(temp));
                log.debug(pin + "dept resources(" + pin + ") size:"
                        + set.size());
                try {
                    cacheUtils.set(key, privilegeCacheTime, set);
                } catch (Exception e) {
                    log.error("set resources(" + pin + ") to the cache error!",
                            e);
                }
            } else {
                log.debug(pin + " dept resource(" + pin + ") is empty!");
            }
            return set;
        } else {
            log.warn("dept resources(" + pin + ") didn't hit the cache !");
            String str = deptWebService.resourceNames(pin);
            if (StringUtils.isNotEmpty(str)) {
                String[] temp = str.split(",");
                set = new HashSet<String>(Arrays.asList(temp));
            }
        }
        return set;
    }

    /**
     * 判断是否有权限
     * 
     * @param pin
     *            指定用户。
     * @param resouce
     *            被check的权限
     * @return
     */
    public boolean hasHrmPrivilege(String pin, String resouce) {
        String[] strings = resouce.split(",");
        Set<String> resources = getResources(pin);
        return checkResource(strings, resources);
    }

    /**
     * 判断是否有权限。用户从logincontext中取出，如果未登录，由返回false
     * 
     * @param resouce
     * @return true 表示有 false表示没有
     */
    public boolean hasHrmPrivilege(String resouce) {
        LoginContext context = LoginContext.getLoginContext();
        return context != null && context.isLogin()
                && hasHrmPrivilege(context.getPin(), resouce);
    }

    private String getKey(String pin) {
        if (pin == null) {
            throw new NullPointerException("Pin is null");
        }
        return "pop_admin_hrm_resources_" + pin;
    }

    public void setPrivilegeCacheTime(int privilegeCacheTime) {
        this.privilegeCacheTime = privilegeCacheTime;
    }
}