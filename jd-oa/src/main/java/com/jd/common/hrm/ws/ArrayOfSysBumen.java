
package com.jd.common.hrm.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSys_bumen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSys_bumen">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Sys_bumen" type="{http://360buy.com/}Sys_bumen" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSys_bumen", propOrder = {
    "sysBumen"
})
public class ArrayOfSysBumen {

    @XmlElement(name = "Sys_bumen", nillable = true)
    protected List<SysBumen> sysBumen;

    /**
     * Gets the value of the sysBumen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sysBumen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSysBumen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SysBumen }
     * 
     * 
     */
    public List<SysBumen> getSysBumen() {
        if (sysBumen == null) {
            sysBumen = new ArrayList<SysBumen>();
        }
        return this.sysBumen;
    }

}
