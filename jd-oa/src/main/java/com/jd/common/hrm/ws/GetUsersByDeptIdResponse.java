
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUsersByDeptIdResult" type="{http://360buy.com/}ArrayOfUser" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUsersByDeptIdResult"
})
@XmlRootElement(name = "GetUsersByDeptIdResponse")
public class GetUsersByDeptIdResponse {

    @XmlElement(name = "GetUsersByDeptIdResult")
    protected ArrayOfUser getUsersByDeptIdResult;

    /**
     * Gets the value of the getUsersByDeptIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUser }
     *     
     */
    public ArrayOfUser getGetUsersByDeptIdResult() {
        return getUsersByDeptIdResult;
    }

    /**
     * Sets the value of the getUsersByDeptIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUser }
     *     
     */
    public void setGetUsersByDeptIdResult(ArrayOfUser value) {
        this.getUsersByDeptIdResult = value;
    }

}
