package com.jd.common.hrm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 人事权限定义 User: yangsiyong@360buy.com Date: 2010-6-2 Time: 9:54:25
 * 
 * port from com.jd.common.struts.interceptor.HrmPrivilege, rename package
 * 
 * @author xiaofei
 * @see com.jd.common.struts.interceptor.HrmPrivilege
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface HrmPrivilege {
    /**
     * 权限码。可以用“,”分隔多个。表示都可以访问。
     * 
     * @return
     */
    public String value();
}