
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUsersByidsResult" type="{http://360buy.com/}ArrayOfEmployeeQuery" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUsersByidsResult"
})
@XmlRootElement(name = "GetUsersByidsResponse")
public class GetUsersByidsResponse {

    @XmlElement(name = "GetUsersByidsResult")
    protected ArrayOfEmployeeQuery getUsersByidsResult;

    /**
     * Gets the value of the getUsersByidsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEmployeeQuery }
     *     
     */
    public ArrayOfEmployeeQuery getGetUsersByidsResult() {
        return getUsersByidsResult;
    }

    /**
     * Sets the value of the getUsersByidsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEmployeeQuery }
     *     
     */
    public void setGetUsersByidsResult(ArrayOfEmployeeQuery value) {
        this.getUsersByidsResult = value;
    }

}
