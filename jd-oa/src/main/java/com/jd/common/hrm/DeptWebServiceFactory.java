package com.jd.common.hrm;

import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

import com.jd.common.hrm.ws.DeptWebService;
import com.jd.common.hrm.ws.DeptWebServiceSoap;

/**
 * Simple factory bean for create DeptWebService, with authheader, timeout.
 * 
 * @author xiaofei
 * 
 */
public class DeptWebServiceFactory {

    @SuppressWarnings("rawtypes")
    private List<Handler> handlers;

    private int connectTimeout = 2000;
    private int requestTimeout = 120000;

    public void setHandlers(@SuppressWarnings("rawtypes") List<Handler> handlers) {
        this.handlers = handlers;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setRequestTimeout(int requestTimeout) {
        this.requestTimeout = requestTimeout;
    }

    public DeptWebServiceSoap create() {

        // ensure retrieve WSDL from local
        DeptWebService service = new DeptWebService();

        if (handlers != null) {
            service.setHandlerResolver(new HandlerResolver() {
                @SuppressWarnings("rawtypes")
                @Override
                public List<Handler> getHandlerChain(PortInfo portInfo) {
                    System.out.println("getHandlerChain:" + portInfo);
                    return handlers;
                }
            });
        }

        DeptWebServiceSoap port = service.getDeptWebServiceSoap();

        if (port instanceof BindingProvider) {
            Map<String, Object> ctx = ((BindingProvider) port)
                    .getRequestContext();
            ctx.put("com.sun.xml.internal.ws.connect.timeout", connectTimeout);
            ctx.put("com.sun.xml.internal.ws.request.timeout", requestTimeout);
        }

        return port;

    }
}
