
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResourceNamesResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resourceNamesResult"
})
@XmlRootElement(name = "ResourceNamesResponse")
public class ResourceNamesResponse {

    @XmlElement(name = "ResourceNamesResult")
    protected String resourceNamesResult;

    /**
     * Gets the value of the resourceNamesResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceNamesResult() {
        return resourceNamesResult;
    }

    /**
     * Sets the value of the resourceNamesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceNamesResult(String value) {
        this.resourceNamesResult = value;
    }

}
