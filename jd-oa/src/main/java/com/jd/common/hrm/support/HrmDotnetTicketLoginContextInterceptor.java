package com.jd.common.hrm.support;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jd.common.hrm.ws.DeptWebServiceSoap;
import com.jd.common.hrm.ws.User;
import com.jd.common.springmvc.interceptor.DotnetTicketLoginContextInterceptor;
import com.jd.common.web.DotnetAuthenticationTicket;
import com.jd.common.web.LoginContext;

/**
 * 保持logincontext中的session和从 dotnet ticket中读取出来的一致。<br/>
 * 后台hrm系统使用，这里会填写真实姓名<br/>
 * User: Administrator<br/>
 * Date: 10-12-16<br/>
 * Time: 下午1:10<br/>
 * 
 * port from
 * com.jd.common.struts.interceptor.HrmDotnetTicketLoginContextInterceptor
 * 
 * @see com.jd.common.struts.interceptor.HrmDotnetTicketLoginContextInterceptor
 * @author xiaofei
 */
public class HrmDotnetTicketLoginContextInterceptor extends
        DotnetTicketLoginContextInterceptor {
    private final static Log log = LogFactory
            .getLog(HrmDotnetTicketLoginContextInterceptor.class);

    private DeptWebServiceSoap deptWebService;

    public void setDeptWebService(DeptWebServiceSoap deptWebService) {
        this.deptWebService = deptWebService;
    }

    @Override
    protected LoginContext getLoginContextFromTicket(
            DotnetAuthenticationTicket ticket) {
        LoginContext loginContext = new LoginContext();
        String username = ticket.getUsername();
        loginContext.setPin(username);
        if (deptWebService != null) {
            User userByName = null;
            try {
                userByName = deptWebService.getUserByName(username);
            } catch (Exception e) {
                log.error("call deptWebService#getUserByName error! ", e);
            }
            if (userByName == null) {
                log.debug("get userinfo error!");
            } else {
                loginContext.setNick(userByName.getRealName());
            }
        } else {
            log.debug("deptWebService is null!");
        }
        return loginContext;
    }

}
