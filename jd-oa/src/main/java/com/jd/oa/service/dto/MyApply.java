package com.jd.oa.service.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * 我的申请dto对象
 * 
 * @author zhouhuaqi
 * 
 */
@XmlType(propOrder={"title", "applyTime" , "url"}) 
public class MyApply {
	/**
	 * 我的申请主键ID
	 */
	private String id;
	/**
	 * 申请标题
	 */
	private String title;
	/**
	 * 申请时间
	 */
	private String applyTime;
	/**
	 * 访问URL
	 */
	private String url;

	/**
	 * 申请状态
	 */
	private String status;
	
	public MyApply() {
	}

	public MyApply(String id, String title, String applyTime, String url) {
		this.id = id;
		this.title = title;
		this.applyTime = applyTime;
		this.url = url;
	}
	
	@XmlAttribute
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
