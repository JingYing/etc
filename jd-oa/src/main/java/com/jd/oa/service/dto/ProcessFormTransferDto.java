/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-4-17
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.service.dto;

import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessNode;

import java.util.List;
import java.util.Map;

public class ProcessFormTransferDto {
	private String subject; //主题
	private ProcessDefinition processDefinition; //流程定义信息
	private Form mainForm; //主表信息
	private List<FormItem> mainFormItems; //主表字段信息
	private ProcessNode firstProessNode;
	private List<Form> subFormList; //子表列表
	private Map<String,List<FormItem>> subFormItemsMap; //子表字段信息

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public ProcessDefinition getProcessDefinition() {
		return processDefinition;
	}

	public void setProcessDefinition(ProcessDefinition processDefinition) {
		this.processDefinition = processDefinition;
	}

	public Form getMainForm() {
		return mainForm;
	}

	public void setMainForm(Form mainForm) {
		this.mainForm = mainForm;
	}

	public List<FormItem> getMainFormItems() {
		return mainFormItems;
	}

	public void setMainFormItems(List<FormItem> mainFormItems) {
		this.mainFormItems = mainFormItems;
	}

	public List<Form> getSubFormList() {
		return subFormList;
	}

	public void setSubFormList(List<Form> subFormList) {
		this.subFormList = subFormList;
	}

	public Map<String, List<FormItem>> getSubFormItemsMap() {
		return subFormItemsMap;
	}

	public void setSubFormItemsMap(Map<String, List<FormItem>> subFormItemsMap) {
		this.subFormItemsMap = subFormItemsMap;
	}

	public ProcessNode getFirstProessNode() {
		return firstProessNode;
	}

	public void setFirstProessNode(ProcessNode firstProessNode) {
		this.firstProessNode = firstProessNode;
	}
}
