package com.jd.oa.service.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * 我的待办dto对象
 * 
 * @author zhouhuaqi
 * 
 */
@XmlType(propOrder={"title", "organizationName" , "applicant", "beginTime", "url"}) 
public class MyTask {
	/**
	 * 我的待办主键ID
	 */
	private String id;
	/**
	 * 待办标题
	 */
	private String title;
	/**
	 * 申请人所在部门
	 */
	private String organizationName;
	/**
	 * 申请人
	 */
	private String applicant;
	/**
	 * 任务开始时间
	 */
	private String beginTime;
	/**
	 * 访问URL
	 */
	private String url;

	public MyTask() {
	}

	public MyTask(String id, String title, String organizationName, String applicant,
			String beginTime, String url) {
		this.id = id;
		this.title = title;
		this.organizationName = organizationName;
		this.applicant = applicant;
		this.beginTime = beginTime;
		this.url = url;
	}
	
	@XmlAttribute
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
