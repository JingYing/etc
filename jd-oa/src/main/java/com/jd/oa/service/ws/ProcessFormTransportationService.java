/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-4-17
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.service.ws;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ProcessFormTransportationService {

	public String requestFormDefinition(@WebParam(name="processDefinitionId") String processDefinitionId);

	public String transportFormData(@WebParam(name="xml") String xml);
}
