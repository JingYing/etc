package com.jd.oa.service.ws.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.oa.app.dao.ProcessSearchDao;
import com.jd.oa.app.dto.ProcessAndTaskInfoDto;
import com.jd.oa.app.model.Conditions;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.service.dto.MyApply;
import com.jd.oa.service.dto.MyTask;
import com.jd.oa.service.dto.ServiceResponse;
import com.jd.oa.service.ws.ProcessService;

@WebService(endpointInterface = "com.jd.oa.service.ws.ProcessService")
public class ProcessServiceImpl implements ProcessService {

	private static final Logger logger = Logger
			.getLogger(ProcessServiceImpl.class);
	
	private final static String OA_APPLY_URL="http://oa.jd.com/app?from=portal&enter=apply";
	private final static String OA_MTTASK_URL="http://oa.jd.com/app?from=portal&enter=mytask";
	
	@Autowired
	private ProcessSearchService processSearchService;
	@Autowired
	private OaPafService oaPafService;
	@Autowired
	private ProcessSearchDao processSearchDao;
	@Autowired
	private ProcessNodeService processNodeService;
	@Override
	public ServiceResponse<MyApply> getMyApplys(String userName) {
		ServiceResponse<MyApply> sr = new ServiceResponse<MyApply>();
		sr.setMoreUrl(OA_APPLY_URL);
		
		List<MyApply> myApplys = new ArrayList<MyApply>();
		List<ProcessAndTaskInfoDto> applys=null;
		try{
			applys=processSearchService.getMyApplyProTaskInfo(userName);
		}catch (Exception e) {
			logger.error(e);
			sr.setTotalCount(0);
			sr.setData(new ArrayList<MyApply>()); 
			return sr;
		}
		if(applys==null||applys.size()==0){
			sr.setTotalCount(0);
			sr.setData(new ArrayList<MyApply>()); 
			return sr;
		}
		int num=10;
		MyApply cursorApply=null;
		if(applys.size() > 0){
			ProcessAndTaskInfoDto dto = null;
			for(int i = 0; i < applys.size(); i++){
				if(i == num){
					break;
				}
				dto = applys.get(i);
				cursorApply=new MyApply();
				cursorApply.setId(dto.getBusinessInstanceId());
				cursorApply.setTitle(dto.getProcessDifinitionName()+"-"+dto.getProcessInstanceName());
				cursorApply.setApplyTime(dto.getBeginTime());
				cursorApply.setUrl(OA_APPLY_URL);
				myApplys.add(cursorApply);
			}
		}
		Collections.sort(myApplys,new Comparator<MyApply>() {
			@Override
			public int compare(MyApply o1, MyApply o2) {
				if(o1.getApplyTime()!=null&&o2.getApplyTime()!=null){
					return o2.getApplyTime().compareTo(o1.getApplyTime());
				}
				return o2.getApplyTime().compareTo(o1.getApplyTime());
			}
		});
		sr.setTotalCount(applys.size());
		sr.setData(myApplys);
		return sr;
	}
	/**
	 * 获取待办任务
	 */
	@Override
	public ServiceResponse<MyTask> getMyTasks(String userName) {
		ServiceResponse<MyTask> sr = new ServiceResponse<MyTask>();
		sr.setMoreUrl(OA_MTTASK_URL);
		
		List<MyTask> myTasks = new ArrayList<MyTask>();
		Conditions conditions=new Conditions();
		conditions.setSize("10");
		conditions.setOrder("desc");
		conditions.setSort("createTime");
		//从paf取回数据
		List<OaTaskInstance> ownerTaskList=null;
		String totalNum;
		try {
    		Map<String, Object> mp = this.oaPafService.getTaskAndAllTotal(userName, conditions);    		
    		ownerTaskList = (List<OaTaskInstance>) mp.get("taskList");
    		totalNum=mp.get("total").toString();
		} catch (Exception e) {
			logger.error(e);
			sr.setTotalCount(0);
			sr.setData(new ArrayList<MyTask>());
			return sr;
		}
		if(ownerTaskList==null||ownerTaskList.size()==0){
			sr.setTotalCount(0);
			sr.setData(new ArrayList<MyTask>());
			return sr;
		}
		
		
		//遍历数据库过滤资源
		MyTask cursorTask=null;
		OaTaskInstance task;
		String processInstanceId = null;
		String nodeId = null;
		
		//组合流程实例ID做一次数据库查询
		List<String> processInstanceIds=new ArrayList<String>();
		for(int i=0; i<ownerTaskList.size();i++){
			task = ownerTaskList.get(i);
			processInstanceId = task.getProcessInstanceId();
			processInstanceIds.add(processInstanceId);
		}
		
		Map<String,ProcessSearch> mapTemp = new HashMap<String, ProcessSearch>();
		if(processInstanceIds.size() > 0){
			List<ProcessSearch> tempList = processSearchDao.getProcessInfoByInstanceId(processInstanceIds);
			if(tempList != null && tempList.size() > 0){
				for(ProcessSearch bean:tempList){
					mapTemp.put(bean.getProcessInstanceId(), bean);
				}
			}
		}
		
		//整合数据到taskSearchs
		List<ProcessSearch> taskSearchs=new ArrayList<ProcessSearch>();
		ProcessSearch oldBean = null;
		for(int i=0; i<ownerTaskList.size();i++){
			task = ownerTaskList.get(i);
			//对应的流程实例ID
			processInstanceId = task.getProcessInstanceId();
			nodeId = task.getNodeId();

			ProcessSearch ps = new ProcessSearch();
			processInstanceIds.clear();
			processInstanceIds.add(processInstanceId);
			oldBean = mapTemp.get(processInstanceId);
			if(oldBean!=null){
				try {
					ComUtils.copyProperties(oldBean, ps);
				} catch (Exception e) {
					logger.error(e);
					sr.setTotalCount(0);
					sr.setData(new ArrayList<MyTask>());
					return sr;
				}
			}else{
				continue;
			}
			
			//判断当前节点是否是首节点
			ProcessNode node = processNodeService.findAddsignRule(task.getProcessDefinitionId(), nodeId);
			if(node!=null){
				if(!"1".equals(node.getIsFirstNode())){
					taskSearchs.add(ps);
				}else{
					totalNum=(Integer.parseInt(totalNum)-1)+"";
				}
			}
		}
		//组织数据taskSearchs到myTasks返回
		for(ProcessSearch processSearch:taskSearchs){
			cursorTask=new MyTask();
			cursorTask.setId(processSearch.getBusinessInstanceId());
			cursorTask.setTitle(processSearch.getProcessDefinitionName()+"-"+processSearch.getProcessInstanceName());
			cursorTask.setBeginTime(DateUtils.datetimeFormat24(processSearch.getBeginTime()));
			cursorTask.setApplicant(processSearch.getRealName());
			cursorTask.setOrganizationName(processSearch.getOrganizationName());
			cursorTask.setUrl(OA_MTTASK_URL);
			myTasks.add(cursorTask);
		}
		Collections.sort(myTasks,new Comparator<MyTask>() {
			@Override
			public int compare(MyTask o1, MyTask o2) {
				if(o1.getBeginTime()!=null&&o2.getBeginTime()!=null){
					return o2.getBeginTime().compareTo(o1.getBeginTime());
				}
				return o2.getBeginTime().compareTo(o1.getBeginTime());
			}
		});
		sr.setTotalCount(Integer.parseInt(totalNum));
		sr.setData(myTasks);
		return sr;
	}

}
