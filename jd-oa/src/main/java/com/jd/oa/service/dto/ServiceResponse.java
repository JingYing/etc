package com.jd.oa.service.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 服务的响应
 * 
 * @author zhouhuaqi
 *
 */
@XmlRootElement(name = "response", namespace="")
@XmlType(propOrder={"totalCount", "moreUrl" , "data", "responseTime"}) 
public class ServiceResponse<T> {
	
	/**
	 * 数据数量
	 */
	private int totalCount;
	
	/**
	 * 更多数据页面的链接URL
	 */
	private String moreUrl;
	
	/**
	 * 列表数据
	 */
	private List<T> data;
	
	/**
	 * 响应时间
	 */
	private Date responseTime;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
	public String getMoreUrl() {
		return moreUrl;
	}

	public void setMoreUrl(String moreUrl) {
		this.moreUrl = moreUrl;
	}

	@XmlElementWrapper(name = "dataList")
	@XmlElement(name="data")
	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public Date getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}
}
