package com.jd.oa.service.http;

import com.jd.oa.service.dto.MyApply;
import com.jd.oa.service.dto.MyTask;
import com.jd.oa.service.dto.ServiceResponse;

public interface HttpProcessService {
	/**
	 * 获取我的申请数据列表
	 * 
	 * @param userName
	 *            用户名，erpid
	 * @return
	 */
	public ServiceResponse<MyApply> getMyApplys(String userName,String processType,int size);

	/**
	 * 获取我的代办数据列表
	 * 
	 * @param userName
	 *            用户名，erpid
	 * @return
	 */
	public ServiceResponse<MyTask> getMyTasks(String userName,String processType,int size);
}
