package com.jd.oa.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.dts.JobDef;
import com.jd.dts.TaskExecutor;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.common.utils.OtherDbUtil;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.PsService;
import com.jd.oa.system.service.SysUserService;

/**
 * 用户同步执行器
 * 
 * @author zhouhuaqi
 * 
 */
public class UserSyncExecutor implements TaskExecutor<SysUser> {

	private static final Logger logger = LoggerFactory
			.getLogger(UserSyncExecutor.class);

	@Autowired
	SysUserService sysUserService;

	@Autowired
	ProcessTaskService processTaskService;
	
	@Autowired
	PsService psService;

	@Override
	public boolean execute(List<SysUser> users) {
		logger.info("Task execute begin[job:userSyncJob, users.size:" + users.size() + "]");
		sysUserService.transferUserInfoFromHR(users);
		logger.info("Task execute end[job:userSyncJob, users.size:" + users.size() + "]");
		return false;
	}

	@Override
	public List<SysUser> fetch(JobDef job, Set<String> partitions) {
		logger.info("Task fetch begin[job:" + job.getName() + ", partitions:"
				+ partitions.toString() + "]");
		List<SysUser> result = new ArrayList<SysUser>();
		//List<SysUser> allUser = OtherDbUtil.getAllUserFromEHR();
		
		DataSourceHandle.setDataSourceType("ps");
		List<SysUser> allUser = psService.getAllUsers();
		for (SysUser user : allUser) {
//			String partition = String
//					.valueOf(Math.abs(user.getUserCode().hashCode()) % 12);
			String partition = "1";
			if (partitions.contains(partition)) {
				result.add(user);
			}
		}
		logger.info("Task fetch end[job:" + job.getName() + ", partitions:"
				+ partitions.toString() + ", task.size:" + result.size() + "]");
		return result;
	}
}
