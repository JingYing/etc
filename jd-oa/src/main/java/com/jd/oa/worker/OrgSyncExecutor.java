package com.jd.oa.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.dts.JobDef;
import com.jd.dts.TaskExecutor;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.common.utils.OtherDbUtil;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.PsService;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;


/**
 * 组织结构同步执行器
 * 
 * @author liubo
 * 
 */
public class OrgSyncExecutor implements TaskExecutor<SysOrganization> {

	private static final Logger logger = LoggerFactory
			.getLogger(OrgSyncExecutor.class);

	@Autowired
	SysOrganizationService sysOrganizationService;

	@Autowired
	ProcessTaskService processTaskService;
	
	@Autowired
	PsService psService;

	@Override
	public boolean execute(List<SysOrganization> orgs) {
		logger.info("Task execute begin[job:orgSyncJob, users.size:" + orgs.size() + "]");
		sysOrganizationService.transferOrgInfoFromHR(orgs);
		logger.info("Task execute end[job:orgSyncJob, users.size:" + orgs.size() + "]");
		return false;
	}

	@Override
	public List<SysOrganization> fetch(JobDef job, Set<String> partitions) {
		logger.info("Task fetch begin[job:" + job.getName() + ", partitions:"+ partitions.toString() + "]");
		List<SysOrganization> result = new ArrayList<SysOrganization>();
		//List<SysOrganization> allOrg = OtherDbUtil.getOrganizationFromEHR();
		
		DataSourceHandle.setDataSourceType("ps");
		List<SysOrganization> allOrg = psService.getAllOrganizations();
		for (SysOrganization org : allOrg) {
//			String partition = String.valueOf(Math.abs(org.getOrganizationCode().hashCode()) % 1);
			String partition = "1";
			if (partitions.contains(partition)) {
				result.add(org);
			}
		}
		logger.info("Task fetch end[job:" + job.getName() + ", partitions:"	+ partitions.toString() + ", task.size:" + result.size() + "]");
		return result;
	}
}
