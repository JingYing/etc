package com.jd.oa.worker;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jd.dts.manager.JobHandlerZkManagerFactoryBean;
import com.jd.oa.common.exception.BusinessException;

/**
 * 分布式调度启动器
 * 
 * @author zhouhuaqi
 *
 */
public class WorkerStart {
	
	private static final Logger logger = LoggerFactory
			.getLogger(WorkerStart.class);
	
	@Autowired
	private JobHandlerZkManagerFactoryBean jobHandlerZkManager;

	public void start(){
		try {
			jobHandlerZkManager.create().start();
			logger.info("定时任务启动成功");
		} catch (Exception e) {
			throw new BusinessException("定时任务启动失败", e);
		}
	}

}
