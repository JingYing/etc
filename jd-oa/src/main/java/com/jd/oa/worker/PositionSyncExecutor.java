package com.jd.oa.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.dts.JobDef;
import com.jd.dts.TaskExecutor;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.common.utils.OtherDbUtil;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.service.PsService;
import com.jd.oa.system.service.SysPositionService;

/**
 * 岗位信息同步执行器
 * 
 * @author liubo
 * 
 */
public class PositionSyncExecutor implements TaskExecutor<SysPosition> {

	private static final Logger logger = LoggerFactory
			.getLogger(PositionSyncExecutor.class);

	@Autowired
	SysPositionService sysPositionService;

	@Autowired
	ProcessTaskService processTaskService;
	
	@Autowired
	PsService psService;

	@Override
	public boolean execute(List<SysPosition> sysPositions) {
		logger.info("Task execute begin[job:PositionSyncJob, users.size:" + sysPositions.size() + "]");
		sysPositionService.transferPositionInfoFromHR(sysPositions);
		logger.info("Task execute end[job:PositionSyncJob, users.size:" + sysPositions.size() + "]");
		return false;
	}

	@Override
	public List<SysPosition> fetch(JobDef job, Set<String> partitions) {
		logger.info("Task fetch begin[job:" + job.getName() + ", partitions:"+ partitions.toString() + "]");
		List<SysPosition> result = new ArrayList<SysPosition>();
		//List<SysPosition> allPosition = OtherDbUtil.getPositionFromEHR();
		
		DataSourceHandle.setDataSourceType("ps");
		List<SysPosition> allPosition = psService.getAllPositions();
		for (SysPosition position : allPosition) {
//			String partition = String.valueOf(Math.abs(org.getOrganizationCode().hashCode()) % 1);
			String partition = "1";
			if (partitions.contains(partition)) {
				result.add(position);
			}
		}
		logger.info("Task fetch end[job:" + job.getName() + ", partitions:"	+ partitions.toString() + ", task.size:" + result.size() + "]");
		return result;
	}
}
