package com.jd.oa.agent.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.Element;

/**
 * 审批结果. 用来反序列化json.
 * Processkey:  //流程key,
	ReqId:  //申请单ID, 
	taskId:  //任务ID,
	submitUserErp:  //审批人erp,
	submitUserName:  //审批人姓名,
	submitTime:  //审批时间(yyyy-MM-dd hh:mm:ss),
	submitResult:  //审批结果(1:批准 2:拒绝  3:驳回 4:取消  5:归档),
	submitComments:  //审批意见,
	processStatus:  //流程状态
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see
 * @author 荆营 部门:综合职能研发部
 * @version 1.0
 * @date 2014年6月27日
 */
public class ApprovalResultDTO {
	private String processkey, reqId, taskId, submitUserErp, submitUserName,
			submitComments, processStatus;
	private Date submitTime;
	private int submitResult;
	
	public static ApprovalResultDTO fromXml(Document xml)	{
		Element root = xml.getRootElement();
		ApprovalResultDTO dto = new ApprovalResultDTO();
		dto.setProcesskey(root.elementText("processKey"));
		dto.setProcessStatus(root.elementText("processStatus"));
		dto.setReqId(root.elementText("reqId"));
		dto.setSubmitComments(root.elementText("submitComments"));
		dto.setSubmitResult(Integer.parseInt(root.elementText("submitResult")));
		try {
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(root.elementText("submitTime"));
			dto.setSubmitTime(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		dto.setSubmitUserErp(root.elementText("submitUserErp"));
		dto.setSubmitUserName(root.elementText("submitUserName"));
		dto.setTaskId(root.elementText("taskId"));
		return dto;
	}

	public String getProcesskey() {
		return processkey;
	}

	public void setProcesskey(String processkey) {
		this.processkey = processkey;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getSubmitUserErp() {
		return submitUserErp;
	}

	public void setSubmitUserErp(String submitUserErp) {
		this.submitUserErp = submitUserErp;
	}

	public String getSubmitUserName() {
		return submitUserName;
	}

	public void setSubmitUserName(String submitUserName) {
		this.submitUserName = submitUserName;
	}

	public String getSubmitComments() {
		return submitComments;
	}

	public void setSubmitComments(String submitComments) {
		this.submitComments = submitComments;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public Date getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	public int getSubmitResult() {
		return submitResult;
	}

	public void setSubmitResult(int submitResult) {
		this.submitResult = submitResult;
	}

}
