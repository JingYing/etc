/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-14
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.agent.service.impl;

import com.jd.oa.agent.dao.AgentBizDataDao;
import com.jd.oa.agent.model.AgentBizData;
import com.jd.oa.agent.service.AgentBizDataService;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("agentBizDataService")
public class AgentBizDataServiceImpl extends BaseServiceImpl<AgentBizData, String> implements AgentBizDataService {
	@Resource
	private AgentBizDataDao agentBizDataDao;

	@Override
	public BaseDao<AgentBizData, String> getDao() {
		return agentBizDataDao;
	}
}
