package com.jd.oa.agent.service.impl;

import com.jd.activemq.producer.JMSProducer;
import com.jd.oa.agent.service.MqProduceEbsService;
import com.jd.oa.common.utils.PlatformProperties;
import javax.annotation.Resource;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component("mqProduceEbsService")
public class MqProduceEbsServiceImpl implements MqProduceEbsService {
	@Resource(name = "producerEbs")
	private JMSProducer producerEbs;
	private static final Log logger = LogFactory.getLog(MqProduceEbsService.class);

	@Override
	public String mqProducer(String textMessage) {
		// TODO Auto-generated method stub
		
		String producer = PlatformProperties.getProperty("mq.ebs.producer.destination");
		
		try {
			logger.info("send PS message: " + textMessage);
			producerEbs.send(producer, textMessage, "");
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
		
		return "success";
	}
	

}