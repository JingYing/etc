package com.jd.oa.agent.service;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;

/**
 * 待办任务. 用来反序列化json.
 * {
	Processkey:  //流程key,
	ReqId:  //申请单ID, 
	tasks:  
	[{taskId:  //待办任务ID,
	taskName:  //待办任务名称,
	candidatUsers:  //待办人
	},
	{taskId:  //待办任务ID,
	taskName:  //待办任务名称,
	candidatUsers:  //待办人
	}]
	}
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年6月27日
 */
public class TodoListDTO {
	private String processkey, reqId;
	
	private List<TaskDTO> tasks;
	
	public static TodoListDTO fromXml(Document xml)	{
		Element root = xml.getRootElement();
		TodoListDTO dto = new TodoListDTO();
		dto.setProcesskey(root.elementText("processkey"));
		dto.setReqId(root.elementText("reqId"));
		List<TaskDTO> tasks = new ArrayList<TaskDTO>();
		for(Element e : (List<Element>)root.elements("tasks"))	{
			TaskDTO task = new TaskDTO();
			task.setCandidatUsers(e.elementText("candidatUsers"));
			task.setTaskId(e.elementText("taskId"));
			task.setTaskName(e.elementText("taskName"));
			tasks.add(task);
		}
		dto.setTasks(tasks);
		return dto;
	}
	
	public String getProcesskey() {
		return processkey;
	}
	public void setProcesskey(String processkey) {
		this.processkey = processkey;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public List<TaskDTO> getTasks() {
		return tasks;
	}
	public void setTasks(List<TaskDTO> tasks) {
		this.tasks = tasks;
	}
}
