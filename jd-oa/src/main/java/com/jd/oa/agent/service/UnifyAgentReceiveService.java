package com.jd.oa.agent.service;

/**
 * 统一代办相关的数据解析服务.
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年6月30日
 */
public interface UnifyAgentReceiveService {
	
	/**
	 * 解析json或xml串, 根据数据结构定义, 分配到相关receiveXXX()接口进行解析 
	 * @param json
	 */
	void receive(String text);
	
}
