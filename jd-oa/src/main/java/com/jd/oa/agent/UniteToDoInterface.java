package com.jd.oa.agent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 统一待办整合接口
 * @author liubo
 *
 */
public interface UniteToDoInterface {
	/**
	 * 表单数据展现
	 * @param request
	 * @param processDefinitionKey
	 * @param businessObjectId
	 * @param processNodeId
	 * @param processInstanceId
	 * @param processInstanceStatus
	 * @return
	 * @throws Exception
	 */
	public abstract String getExecuteWorklistBindReportFormContext(
			HttpServletRequest request, HttpServletResponse response,String processDefinitionKey,
			String businessObjectId, String processNodeId,
			String processInstanceId, String processInstanceStatus) throws Exception;
	/**
	 * toolbar 
	 * @return
	 */
	public abstract String getBuninessOperationToolbar();
}
