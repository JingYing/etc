package com.jd.oa.agent.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.agent.dao.AgentBizDataDao;
import com.jd.oa.agent.model.AgentBizData;
import com.jd.oa.common.dao.MyBatisDaoImpl;

@Component("agentBizDataDao")
public class AgentBizDataDaoImpl extends MyBatisDaoImpl<AgentBizData, String> implements AgentBizDataDao{

}
