//package com.jd.oa.agent.service.impl;
//
//import com.jd.activemq.producer.JMSProducer;
//import com.jd.oa.agent.service.MqProduceAgentService;
//import com.jd.oa.agent.service.MqProduceRemedyWebService;
//import com.jd.oa.common.mq.MessageHandlerAgent;
//import com.jd.oa.common.utils.PlatformProperties;
//
//import javax.annotation.Resource;
//import javax.jms.JMSException;
//import javax.jws.WebService;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.stereotype.Component;
//
//@Component("mqProduceAgentService")
//public class MqProduceAgentServiceImpl implements MqProduceAgentService {
//	@Resource(name = "producerAgent")
//	private JMSProducer producerAgent;
//	private static final Log logger = LogFactory.getLog(MessageHandlerAgent.class);
//
//	@Override
//	public String mqProducer(String textMessage) {
//		// TODO Auto-generated method stub
//		
//		String producer = PlatformProperties.getProperty("mq.producer.destination");
//		
//		try {
//			logger.info("send Agent message: " + textMessage);
//			producerAgent.send(producer, textMessage, "");
//		} catch (JMSException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "error";
//		}
//		
//		return "success";
//	}
//	
//
//}