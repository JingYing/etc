package com.jd.oa.agent.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * 此处必须添加@WebServer注解，若不添加，则发布的服务当中无任何方法
 * @author wds
 *
 */
@WebService
public interface MqProducePsWebService {
	@WebMethod(operationName = "mqProducer",action = "http://service.di.oa.jd.com/mqProducer")
	public String mqProducer(@WebParam(name="textMessage",targetNamespace = "") String textMessage);
}
