package com.jd.oa.agent.dao;

import com.jd.oa.agent.model.AgentTodoList;
import com.jd.oa.common.dao.BaseDao;

public interface AgentTodoListDao extends BaseDao<AgentTodoList,String>{

}
