package com.jd.oa.agent.service;

import com.jd.oa.agent.model.AgentTodoList;
import com.jd.oa.common.service.BaseService;

public interface AgentTodoListService extends BaseService<AgentTodoList, String>{

}
