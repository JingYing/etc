package com.jd.oa.agent.service;

/**
 * 待办任务详情. 用来反序列化json
 * {taskId:  //待办任务ID,
	taskName:  //待办任务名称,
	candidatUsers:  //待办人
	}
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年6月27日
 */
public class TaskDTO {
	private String taskId, taskName, candidatUsers;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getCandidatUsers() {
		return candidatUsers;
	}

	public void setCandidatUsers(String candidatUsers) {
		this.candidatUsers = candidatUsers;
	}

}
