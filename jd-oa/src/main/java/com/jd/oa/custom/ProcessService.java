package com.jd.oa.custom;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.w3c.dom.Document;

@WebService
public interface ProcessService {
	@WebMethod(operationName = "findProcessTaskHistoryByBusinessId")
	@WebResult(name = "result")
	public String findProcessTaskHistoryByBusinessId(
			@WebParam(name = "businessId") String businessId);
	
	public String updatePurchaseDetail(@WebParam(name = "purchaseDetail") String purchaseDetail);
		
	
}
