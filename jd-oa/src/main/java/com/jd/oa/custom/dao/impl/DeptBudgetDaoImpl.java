package com.jd.oa.custom.dao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.custom.dao.DeptBudgetDao;
import com.jd.oa.custom.model.DeptBudget;

@Component("deptBudgetDao")
public class DeptBudgetDaoImpl extends MyBatisDaoImpl<DeptBudget,String> implements DeptBudgetDao {

	@Override
	public List<DeptBudget> findByOrgcode(String orgCode, String yearMonth) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("orgCode", orgCode);
		map.put("yearMonth", yearMonth);
		return getSqlSession().selectList("findByOrgcode", map);
	}

	@Override
	public BigDecimal calcTotalMoneyFromBizTrip(Date submitTime, String orgCode) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("submitTime", submitTime);
		map.put("orgCode", orgCode);
		return getSqlSession().selectOne("calcTotalMoneyFromBizTrip", map);
	}

}
