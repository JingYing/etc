package com.jd.oa.custom;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.webservice.CXFDynamicClient;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;
import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;

/**
 * @desc 证照借用申请流程
 * @author qiuyang
 *
 */
public class AdminLicenceManager {
	private static final Logger logger = Logger.getLogger(AdminLicenceManager.class);
	
	JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
	//头信息定义
	private Map<String,Object> soapHeaderMap=new HashMap<String, Object>();
	public AdminLicenceManager(){
		//处理头信息
		soapHeaderMap.put("userName","oa_ws");
		soapHeaderMap.put("password","oa@123#");
		soapHeaderMap.put("authentication","");
		soapHeaderMap.put("locale","");
		soapHeaderMap.put("timeZone","");
	}

	/**
	 * 向remdy推送证照借用申请单信息
	 * 
	 * @param BusinessId
	 * @param MainTable
	 * @param DetailTable
	 * @param wsdl
	 * @param relationwsdl
	 * @param processInstanceId
	 * @param UserTask1400727648041_submitUser
	 * @param beginTime
	 * @param endTime
	 * @param submitUser
	 * @param submitUserRealName
	 * @return
	 */
	public String createLicenceInfo(String BusinessId,String MainTable,String DetailTable,String wsdl,String relationwsdl,String UserTask1404131487986_submitUser,String beginTime,String endTime,String submitUser,String submitUserRealName){
		CallerInfo info = Profiler.registerInfo("wfp_createlicenceinfo",false,true); //方法性能监控
		try {
			//加载需要使用的service
			ProcessInstanceService processInstanceService = SpringContextUtils.getBean("processInstanceService");
			FormBusinessTableService formBusinessTableService= SpringContextUtils.getBean("formBusinessTableService");
			SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");

			//test
			//UserTask1400727648041_submitUser = "zhouhuaqi";
			
			String userTaskSubmitUser = UserTask1404131487986_submitUser;
			SysUser user = sysUserService.getByUserName(userTaskSubmitUser);
			String userEmail = user.getEmail();
			
			//test
			//BusinessId = "118631335209567";
			//取回主表记录
			Map<String,Object> mainRecored=formBusinessTableService.getBusinessData(MainTable, BusinessId);
			if(mainRecored == null) {
				throw new BusinessException("找不到证照记录");
			}
			//取回流程实例信息
			ProcessInstance processInstance = new ProcessInstance();
			processInstance.setBusinessInstanceId(mainRecored.get("ID").toString());
			List<ProcessInstance> processInstances = processInstanceService.find(processInstance);
			if(processInstances == null || processInstances.size() == 0){
				throw new BusinessException("找不到流程实例信息!");
			}
			
			String processInstanceId = processInstances.get(0).getProcessInstanceId();
			
			/*if(!processInstances.get(0).getStatus().equals(SystemConstant.PROCESS_STATUS_5)){
				return "process has not finished!";
			}*/
			
			//取回子表记录，并发送创建证照借用信息
			List<Map<String,Object>> subRecoredList=formBusinessTableService.getBusinessDatas(DetailTable, mainRecored.get("ID").toString());
			
			//test
			/*beginTime = new Date();
			endTime = new Date();
			submitUser = "zhouhuaqi";
			submitUserRealName = "周华旗";*/
			createMainLicenceInfo(mainRecored,wsdl,processInstanceId,userTaskSubmitUser,userEmail,beginTime,endTime,submitUser,submitUserRealName);
			createDetailLicenceInfo(mainRecored,subRecoredList,relationwsdl,processInstanceId);
			return "sucess";
		} catch (Exception e) {
			logger.error("jme_createLicenceInfo error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return "error";
		} finally {
			Profiler.registerInfoEnd(info);
		}
	}
	/**
	 * 证照申请单创建
	 * 
	 * @param mainRecored
	 * @param wsdl
	 * @param processInstanceId
	 * @param UserTask1400727648041_submitUser
	 * @param userEmail
	 * @param beginTime
	 * @param endTime
	 * @param submitUser
	 * @param submitUserRealName
	 */
	private void createMainLicenceInfo(Map<String,Object> mainRecored,String wsdl,String processInstanceId,String userTaskSubmitUser,String userEmail,String beginTime,String endTime,String submitUser,String submitUserRealName){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		
		//给数组赋值
		Object[] params = new Object[21];
		params[0] = processInstanceId;
		params[1] = mainRecored.get("cChr_RequesterCompany");
		params[2] = mainRecored.get("cChr_RequesterOrg");
		params[3] = mainRecored.get("cChr_RequesterDept");
		params[4] = mainRecored.get("cChr_Requester");
		params[5] = mainRecored.get("cChr_RequestERP");
		params[6] = mainRecored.get("cChr_RequesterEmail");
		params[7] = mainRecored.get("cChr_RequesterPhone");
		params[8] = mainRecored.get("cChr_SubmitTarget");
		params[9] = userTaskSubmitUser;
		params[10] = userEmail;
		
		params[12] = mainRecored.get("cChr_Usage");
		if(mainRecored.get("cDT_UseDateStart") !=null){
			try {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime((java.sql.Date)mainRecored.get("cDT_UseDateStart"));
				params[13] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}
		if(mainRecored.get("cDT_UseDateEnd") !=null){
			try {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime((java.sql.Date)mainRecored.get("cDT_UseDateEnd"));
				params[14] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}
		
		if(beginTime !=null){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date beginDate = sdf.parse(beginTime);
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(beginDate);
				params[17] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(endTime !=null){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date endDate = sdf.parse(endTime);
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(endDate);
				params[18] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		params[19] = submitUserRealName;
		params[20] = submitUser;
		try {
			Object[] res=client.invoke("Create",params);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}
	/**
	 * 证照申请关联信息创建
	 * 
	 * @param mainRecored
	 * @param subRecoredList
	 * @param relationwsdl
	 * @param processInstanceId
	 */
	private void createDetailLicenceInfo(Map<String,Object> mainRecored,List<Map<String,Object>> subRecoredList,String wsdl,String processInstanceId){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		
		for(Map<String,Object> subRecored:subRecoredList){
			//给数组赋值
			Object[] params = new Object[16];
			params[0] = processInstanceId;
			params[1] = subRecored.get("cChr_CertificateRequestId");
			params[2] = subRecored.get("cChr_CompanyName");
			params[3] = subRecored.get("cChr_CertificateName");
			params[4] = subRecored.get("cChr_CertificateType");
			if(subRecored.get("cRadio_RequestClass").equals("原件")){
				params[5] = 0;
			}else if(subRecored.get("cRadio_RequestClass").equals("扫描件")){
				params[5] = 10;
			}
			if(mainRecored.get("cDT_UseDateStart") !=null){
				try {
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime((java.sql.Date)mainRecored.get("cDT_UseDateStart"));
					params[6] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}
			}
			if(mainRecored.get("cDT_UseDateEnd") !=null){
				try {
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime((java.sql.Date)mainRecored.get("cDT_UseDateEnd"));
					params[7] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}
			}
			
			params[10] = mainRecored.get("cChr_RequestERP");
			params[11] = mainRecored.get("cChr_Requester");
			params[12] = mainRecored.get("cChr_RequesterEmail");
			params[13] = mainRecored.get("cChr_RequesterCompany");
			params[14] = mainRecored.get("cChr_RequesterOrg");
			params[15] = mainRecored.get("cChr_RequesterDept");
			try {
				Object[] res=client.invoke("Create",params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
		}
	}
}
