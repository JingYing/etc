package com.jd.oa.custom.model;

/**
 * 行政差旅 - 部门预算
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年6月25日
 */
public class DeptBudget {
	private String id;
	private String org1Name;
	private Float monthlyBudget, usedBudget, remainBudget, yearMonth;
	
	public String getOrg1Name() {
		return org1Name;
	}

	public void setOrg1Name(String org1Name) {
		this.org1Name = org1Name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Float getMonthlyBudget() {
		return monthlyBudget;
	}

	public void setMonthlyBudget(Float monthlyBudget) {
		this.monthlyBudget = monthlyBudget;
	}

	public Float getUsedBudget() {
		return usedBudget;
	}

	public void setUsedBudget(Float usedBudget) {
		this.usedBudget = usedBudget;
	}

	public Float getRemainBudget() {
		return remainBudget;
	}

	public void setRemainBudget(Float remainBudget) {
		this.remainBudget = remainBudget;
	}

	public Float getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(Float yearMonth) {
		this.yearMonth = yearMonth;
	}
}
