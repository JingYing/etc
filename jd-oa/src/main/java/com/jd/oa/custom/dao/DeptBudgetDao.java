package com.jd.oa.custom.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.custom.model.DeptBudget;

public interface DeptBudgetDao extends BaseDao<DeptBudget, String>{
	
	List<DeptBudget> findByOrgcode(String orgCode, String yearMonth);
	
	BigDecimal calcTotalMoneyFromBizTrip(Date submitTime, String orgCode);
	

}
