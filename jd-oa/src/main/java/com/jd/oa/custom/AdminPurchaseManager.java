package com.jd.oa.custom;

import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.mail.MailSendService;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.webservice.CXFDynamicClient;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.form.model.FormAttachment;
import com.jd.oa.form.service.FormAttachmentService;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;
import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 
 * @desc 行政采购流程
 * @author WXJ
 * @date 2014-3-5 上午11:25:26
 *
 */
public class AdminPurchaseManager {
	private static final Logger logger = Logger.getLogger(ProcessServiceImpl.class);
	
	
	JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
	
	/**
	 * 
	 * @desc 行政采购流程获取办理人
	 * @author WXJ
	 * @date 2014-3-5 上午11:28:20
	 *
	 * @param submitUser
	 * @return
	 */
	public String getManager(String submitUser) {
		String managerUser = "";
		
		SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
		ProcessTaskService processTaskService = SpringContextUtils.getBean("processTaskService");
				
		//
		SysUser su = sysUserService.getByUserName(submitUser);
		String levelCode = su.getLevelCode();
					 
		if (levelCode.startsWith("M")) {
			managerUser = submitUser;
		}else{
			managerUser = processTaskService.getManagerUser(submitUser);
		}				
		
		return managerUser;
		
	}
	
	/**
	 * 
	 * @desc 计算行政采购申请单号 前缀+年月日+3位序号
	 * @author WXJ
	 * @date 2014-3-7 下午04:45:56
	 *
	 * @param purchaseMianId
	 */
	public String setPurchaseRequisitionID(String purchaseMianId) {//809526f3f13b4bb886778bbb48b580d8
		String applyId = "";
		boolean flag = true ;
		//从数据字典获取前缀和组织编码关系
		DictDataService dictDataService = SpringContextUtils.getBean("dictDataService");
		List<DictData> dictData = dictDataService.findDictDataList("areaPrefix");
		Map<String, String> deptCodeMap = new HashMap<String, String>();
		for (DictData dd : dictData) {
			deptCodeMap.put(dd.getDictCode(), dd.getDictName());
		}
		//查找本条数据的  采购类型  及   使用人部门的组织编码
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append(" select cInt_PurchaseType,ORGANIZATION_FULLPATH ");// 采购类型 使用人部门组织编码
		sqlQuery.append(" from T_JDOA_DEPT_PURCHASE_MAIN PM   ");
		sqlQuery.append(" left join T_JDOA_SYS_ORGANIZATION O on PM.cChr_UseDeptCode = O.ORGANIZATION_CODE");
		sqlQuery.append(" where PM.ID = '" + purchaseMianId + "'");

		List rows = jdbcTemplate.queryForList(sqlQuery.toString());
		if (rows != null && rows.size() > 0) {
			Map map = (Map) rows.get(0);
			String purchaseType = map.get("cInt_PurchaseType").toString();// 采购类型
			String organizationFullpath = map.get("ORGANIZATION_FULLPATH").toString();// 使用人部门的组织编码

			// 前两位的生成，通过 当前使用人部门的组织编码 及 已获取的数据字典（前缀和组织编码关系）
			if (organizationFullpath.contains("00008416")
					|| organizationFullpath.contains("00008417")) {// 如果为宿迁，特殊处理
				applyId += "SQ";
			} else {
			// 非宿迁，则根据数据字典维护情况  /隔开的第二个组织编码为准
				String[] orgFullPathArray = organizationFullpath.split("/");
				if (orgFullPathArray.length > 3) {
					//首选判断是否是分公司
					String code = orgFullPathArray[2];
					String deptCode = deptCodeMap.get(code);
					if (StringUtils.isEmpty(deptCode)) {						
						code = orgFullPathArray[3];
						deptCode = deptCodeMap.get(code);
					}					
					applyId += deptCode;
				} else {
					flag = false ;
				}
			}
			if(applyId.equals("\"\"")){
				applyId = "";
			}
			// 第三位的生成
			// 根据采购类型确定内采or外采，现有规则除了内采以外都是外采
			if (purchaseType.equals("20")) {// 内采
				applyId += "N";
			} else {// 外采
				applyId += "W";
			}

			// 日期编码
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			String date = dateFormat.format(new Date());
			applyId += date;
			
			// 后三位序号，在数据库中查找前缀相同最大序号，+1生成新序号
			StringBuffer sqlQueryOrg = new StringBuffer();
			sqlQueryOrg.append(" select max(cChr_RequisitionID) as cChr_RequisitionID ");
			sqlQueryOrg.append(" from T_JDOA_DEPT_PURCHASE_MAIN ");
			sqlQueryOrg.append(" where cChr_RequisitionID like  '" + applyId + "%'");
			List requisitionIdList = jdbcTemplate.queryForList(sqlQueryOrg
					.toString());
			int num;
			if (requisitionIdList != null && requisitionIdList.size() > 0) {
				Map requisitionIdResult = (Map) requisitionIdList.get(0);
				if(null!=requisitionIdResult.get("cChr_RequisitionID")){
					String requisitionId = requisitionIdResult.get("cChr_RequisitionID").toString();
					String last3Code = requisitionId.substring(requisitionId.length() - 3, requisitionId.length()).replaceAll("^(0+)", "");//截取后三位，并去除前面的0
					num = Integer.parseInt(last3Code)+1;//新序号生成
				}else{
					num = 1;
				}
			}else{
				num = 1;
			}
			String requisitionID = applyId + "" +  String.format("%03d", num);  ;//新申请单号生成
			//更新为新申请单号
			if (org.apache.commons.lang3.StringUtils.isNotEmpty(requisitionID)
					&& flag == true) {
				StringBuffer sqlUpdate = new StringBuffer();
				sqlUpdate.append(" update T_JDOA_DEPT_PURCHASE_MAIN ");
				sqlUpdate.append(" set cChr_RequisitionID = '" + requisitionID
						+ "'");
				sqlUpdate.append(" where ID = '" + purchaseMianId +"'");
				sqlUpdate.append(" and not exists ");
				sqlUpdate.append(" (select 1 from T_JDOA_PROCESS_INSTANCE where STATUS <> '0' and BUSINESS_INSTANCE_ID = '" + purchaseMianId +"') ");		
				
				logger.debug("修改T_JDOA_DEPT_PURCHASE_MAIN SQL语句："
						+ sqlUpdate.toString());
				jdbcTemplate.execute(sqlUpdate.toString());
				
				//修改主题为：[申请单号]+主题				
			}
		}
		
		return "success";
	}

	/**
	 * 向remdy推送商品子表信息
	 * @param BusinessId 业务ID
	 * @param MainTable 商品主表名
	 * @param DetailTable 商品子表名
	 */
		public String  pushPurchaseInfo(String BusinessId,String MainTable,String DetailTable,String wsdl){
			CallerInfo info = Profiler.registerInfo("wfp_pushpurchaseinfo",false,true); //方法性能监控
			try {
				//加载需要使用的service
				ProcessInstanceService processInstanceService = SpringContextUtils.getBean("processInstanceService");
				ProcessTaskHistoryService processTaskHistoryService = SpringContextUtils.getBean(ProcessTaskHistoryService.class);
				FormBusinessTableService formBusinessTableService= SpringContextUtils.getBean("formBusinessTableService");
				//注入下载附件需要使用的service
				FormAttachmentService formAttachmentService = SpringContextUtils.getBean(FormAttachmentService.class);
				JssService jssService = SpringContextUtils.getBean("jssService");
				//取回主表记录
				Map<String,Object> mainRecored=formBusinessTableService.getBusinessData(MainTable, BusinessId);
				if(mainRecored == null) {
					throw new BusinessException("找不到商品记录");
				}
				//取回流程实例信息
				ProcessInstance processInstance = new ProcessInstance();
				processInstance.setBusinessInstanceId(mainRecored.get("ID").toString());
				List<ProcessInstance> processInstances = processInstanceService.find(processInstance);
				if(processInstances == null || processInstances.size() == 0){
					throw new BusinessException("找不到流程实例信息!");
				}
				if(!processInstances.get(0).getStatus().equals(SystemConstant.PROCESS_STATUS_5)){
					return "process has not finished!";
				}

				//取出流程的最后审批通过时间
				List<ProcessTaskHistory> processTaskHistoryList = processTaskHistoryService.find("findMaxEndtimeByProcessInstanceId",processInstances.get(0).getProcessInstanceId());
				Date endTime = processTaskHistoryList.get(0).getEndTime();
				//取回字表记录，并发送创建商品信息
				List<Map<String,Object>> subRecoredList=formBusinessTableService.getBusinessDatas(DetailTable, mainRecored.get("ID").toString());

//			String wsdl="http://itilkf:800/arsys/WSDL/public/itilkf/JDXUHX:ApplyItemInterface_ProductOperate";
				CXFDynamicClient client=new CXFDynamicClient(wsdl);
				//处理头信息
				Map<String,Object> soapHeaderMap=new HashMap<String, Object>();
				soapHeaderMap.put("userName","oa_ws");
				soapHeaderMap.put("password","oa@123#");
				soapHeaderMap.put("authentication","");
				soapHeaderMap.put("locale","");
				soapHeaderMap.put("timeZone","");
				client.addSoapHeader("AuthenticationInfo",soapHeaderMap);

				for(Map<String,Object> subRecored:subRecoredList){
					//给数组赋值
					Object[] params = new Object[50];
					params[0] = mainRecored.get("cChr_RequesterCompany");
					params[1] = mainRecored.get("cChr_ReqDept1");
					params[2] = mainRecored.get("cChr_ReqDept2");
					params[3] = mainRecored.get("cChr_RequesterERP");
					params[4] = mainRecored.get("cChr_Requester");
					params[5] = mainRecored.get("cChr_RequestDeptCode");
					params[6] = mainRecored.get("cChr_UsePeopleCompany");
					params[7] = mainRecored.get("cChr_UsePeopleDept1");
					params[8] = mainRecored.get("cChr_UsePeopleDept2");
					params[9] = mainRecored.get("cChr_UsePeople");
					params[10] = mainRecored.get("cChr_UsePeopleERP");
					params[11] = mainRecored.get("cChr_UseDeptCode");
					params[12] = mainRecored.get("cChr_RequestCompanyName");
					params[13] = mainRecored.get("cChr_RequestCompanyCode");
					params[14] = mainRecored.get("cChr_EbsCompanyCode");
					if(mainRecored.get("cDT_NeedDate") !=null ){
						try {
							GregorianCalendar cal = new GregorianCalendar();
							cal.setTime((java.sql.Date)mainRecored.get("cDT_NeedDate"));
							params[15] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
						} catch (DatatypeConfigurationException e) {
							e.printStackTrace();
						}
					}
					params[16] = subRecored.get("cChr_PurchasePurpose");
					params[17] = subRecored.get("cChr_PurchasePurposeCode");
					params[18] = subRecored.get("cChr_ProjectDetail");
					params[19] = subRecored.get("cChr_ProjectCode");
					params[20] = subRecored.get("cChr_ProjectCarCode");
					params[21] = subRecored.get("cChr_ProductCode");
					params[22] = subRecored.get("cChr_PartCode");
					params[23] = subRecored.get("cChr_ProductName");
					params[24] = subRecored.get("cChr_ProductDetail");
					if(subRecored.get("cInt_RequestNum") != null){
						params[25] = Integer.parseInt(subRecored.get("cInt_RequestNum").toString());
					}
					if(subRecored.get("cInt_PredictUnitPrice") != null){
						params[26] = BigDecimal.valueOf(Double.parseDouble(subRecored.get("cInt_PredictUnitPrice").toString()));
					}
					params[27] = subRecored.get("cChr_Unit");
					params[28] = subRecored.get("cChr_Comments");

					List<FormAttachment> formAttachmentList = formAttachmentService.find("findByBusinessId",subRecored.get("ID").toString());
					List<byte[]> formAttachmentByteList=new ArrayList<byte[]>();
					for(FormAttachment attachment : formAttachmentList){
						InputStream fileBody = jssService.downloadFile(SystemConstant.BUCKET, attachment.getAttachmentKey());//读取JSS文件流
						byte[] formAttachmentBytes = null;
						try {
							formAttachmentBytes = FileUtil.getBytes( fileBody );
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							try {
								if(fileBody != null) fileBody.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						formAttachmentByteList.add(formAttachmentBytes);
					}
					if(formAttachmentByteList.size() >0){
						if(formAttachmentByteList.get(0) != null){
							params[29] = formAttachmentList.get(0).getAttachmentName();
							params[30] = formAttachmentByteList.get(0);
							params[31] = Integer.parseInt(formAttachmentList.get(0).getAttachmentSize()+"");
						}
						if(formAttachmentByteList.size() > 1 && formAttachmentByteList.get(1) != null){
							params[32] = formAttachmentList.get(1).getAttachmentName();
							params[33] = formAttachmentByteList.get(1);
							params[34] =  Integer.parseInt(formAttachmentList.get(1).getAttachmentSize()+"");
						}
						if(formAttachmentByteList.size() >2 && formAttachmentByteList.get(2) != null){
							params[35] = formAttachmentList.get(2).getAttachmentName();
							params[36] = formAttachmentByteList.get(2);
							params[37] = Integer.parseInt(formAttachmentList.get(2).getAttachmentSize()+"");
						}
					}

					params[38] = mainRecored.get("ID");
					if(mainRecored.get("cInt_PurchaseType") != null){
						params[39] = Integer.parseInt(mainRecored.get("cInt_PurchaseType").toString());
					}
					params[40] = mainRecored.get("cChr_UseAddress");
					if(mainRecored.get("cInt_Budget") != null){
						params[41] = Integer.parseInt(mainRecored.get("cInt_Budget").toString());
					}
					params[42] = mainRecored.get("cChr_RequestReasons");
					if(mainRecored.get("cDecimal_PreTotal") != null){
						params[43] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDecimal_PreTotal").toString()));
					}
					params[44] = subRecored.get("ID");
					if(subRecored.get("cInt_ProductStatus") != null){
						params[45] = Integer.parseInt(subRecored.get("cInt_ProductStatus").toString());
					}
					if(mainRecored.get("cChr_ReqDate") !=null){
						try {
							GregorianCalendar cal = new GregorianCalendar();
							cal.setTime((java.sql.Date)mainRecored.get("cChr_ReqDate"));
							params[46] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
						} catch (DatatypeConfigurationException e) {
							e.printStackTrace();
						}
					}
					if(endTime !=null){
						try {
							GregorianCalendar cal = new GregorianCalendar();
							cal.setTime(endTime);
							params[47] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
						} catch (DatatypeConfigurationException e) {
							e.printStackTrace();
						}
					}
					if(subRecored.get("cInt_SubTotal") != null){
						params[48] = BigDecimal.valueOf(Double.parseDouble(subRecored.get("cInt_SubTotal").toString()));
					}
					params[49] = mainRecored.get("cChr_RequisitionID");				
					try {
						Object[] res=client.invoke("Create",params);
					} catch (Exception e) {
						e.printStackTrace();
						throw new BusinessException(e.getMessage());
					}
				}			
				updateProductStatusToComplete(BusinessId);
				return "sucess";
			} catch (NumberFormatException e) {
				logger.error("jme_pushPurchaseInfo error!", e);
				Profiler.functionError(info);  //方法可用率监控
				return "error";
			} finally {
				Profiler.registerInfoEnd(info);
			}
	}



    public String sendMail(String submitUser,String subject) {
        SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");

        SysUser user = sysUserService.getByUserName(submitUser);

        if (user != null  && StringUtils.isNotEmpty(user.getEmail())) {
            MailSendService mailSendService = SpringContextUtils.getBean("mailSendService");
            mailSendService.sendSimpleMailByAsyn(new String[]{user.getEmail()}, "【京东企业门户-流程中心】", "您好！您有一个行政采购申请已提交，主题："+subject);
        }

		return "success";
    }


    //向申请人发送邮件，在ProcessTaskServiceImpl中的sendMailTAssignee也有，但不再使用
//    public String  sendMailToStarter(String submitUser, String processInstanceId,String processInstanceName,String businessInstanceId,String flag){
//        ProcessTaskService processTaskService = SpringContextUtils.getBean("processTaskService");
//        SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
//
//        ProcessInstance p =  new ProcessInstance();
//        p.setProcessInstanceId(processInstanceId);
//
//        ProcessInstanceService processInstanceService = SpringContextUtils.getBean("processInstanceService");
//        List<ProcessInstance> list = processInstanceService.find(p);
//        if(list == null) return "success";
//        if(list.get(0) != null){
//            if(list.get(0).getStarterId() != null) {
//                SysUser u = sysUserService.get(list.get(0).getStarterId());
//                if(u != null && u.getEmail() != null){
//                    List<String > mails = new ArrayList<String>();
//                    mails.add(u.getEmail());
//                    processTaskService.sendMailToSomeone( submitUser,  processInstanceId, processInstanceName, businessInstanceId,flag,   mails);
//                }
//            }
//
//        }
//
//        return "success";
//    }

    /**
     * 行政采购--商品状态--	等待审批
     * @param purchaseMianId
     * @return
     */
	public String updateProductStatusToApprove(String purchaseMianId) {

        //行政采购主题和单号合并
		try {
			contactInstanceNameAndRequisitionID(purchaseMianId);
		} catch (Exception e) {
			logger.debug("行政采购主题和单号合并失败！");
		}
		String sqlQuery = " update T_JDOA_DEPT_PURCHASE_DETAIL set cInt_ProductStatus = 10 "
				+ " where PARENT_ID =   '" + purchaseMianId + "'";
		try {
			int num = jdbcTemplate.update(sqlQuery);
			return "" + num;
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}
	}
	/**
     * 行政采购--商品状态--	审批完成
     * @param purchaseMianId
     * @return
     */
	public String updateProductStatusToComplete(String purchaseMianId) {
		String sqlQuery = " update T_JDOA_DEPT_PURCHASE_DETAIL set cInt_ProductStatus = 13 "
						+ " where PARENT_ID =   '" + purchaseMianId + "'";
		try {
			int num = jdbcTemplate.update(sqlQuery);
			return "" + num;
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}
	}

    public String get2OrgCode(String erpId){
       return OfficialSealManager.getOrgCode(erpId,2);
    }
    
    
    /**
     * @author zhengbing 
     * @desc 行政采购，主题和申请单号做合并处理，提交申请后，主题名称上自动加上申请单号
     * @date 2014年7月17日 上午9:54:43
     */
    private void contactInstanceNameAndRequisitionID(String purchaseMianId){
    	if(null!=purchaseMianId && !"".equals(purchaseMianId)){
    		String updataSql ="update T_JDOA_PROCESS_INSTANCE  set PROCESS_INSTANCE_NAME = "
        			+ "concat(concat(concat('[',(IFNULL((SELECT cChr_RequisitionID FROM T_JDOA_DEPT_PURCHASE_MAIN where ID='"+purchaseMianId+"'),''))),']'),IFNULL(PROCESS_INSTANCE_NAME,'')) "
        					+ "where BUSINESS_INSTANCE_ID='"+purchaseMianId+"'";
        	jdbcTemplate.execute(updataSql);
    	}
    }
}

