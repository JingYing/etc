package com.jd.oa.design.dao.Impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.design.dao.RouterDao;
import com.jd.oa.form.model.FormItem;
import org.springframework.stereotype.Repository;
import com.jd.oa.form.model.Form;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-13
 * Time: 上午10:53
 * 路由规则DaoImpl
 */
@Repository("routerDao")
public class RouterDaoImpl extends MyBatisDaoImpl<FormItem, Long> implements RouterDao{
    /**
     * 查询表单对应的主表和明细表
     * @param form
     * @return
     */
    public List<Form> findTableByFormId(Form form) {
        return this.getSqlSession().selectList("com.jd.oa.form.model.Form.findFormList",form);
    }
    /**
     * 获取表单字段名称
     * @param formItem
     * @return
     */
	public List<FormItem> getItemNameById(FormItem formItem) {
		return this.getSqlSession().selectList("com.jd.oa.form.model.FormItem.getItemNameById",formItem);
	}

	
	
    /**
     * 根据ID查询 formItem信息
     * @param formItem
     * @return
     */
    public List<FormItem> findFormItemByFormId(FormItem formItem) {
        return this.getSqlSession().selectList("com.jd.oa.form.model.FormItem.findFormItemByFormId",formItem);
    }

    /**
     * 同上，但不过滤
     */
    public List<FormItem> findAllFormItemsByFormId(FormItem formItem) {
        return this.getSqlSession().selectList("com.jd.oa.form.model.FormItem.findAllFormItemsByFormId",formItem);
    }
    
    /**
     * 获取对应表单下的配置变量个数
     * @param form
     * @return
     */
    public Integer getCountVariable(Form form) {
        return this.getSqlSession().selectOne("com.jd.oa.form.model.Form.getCountVariable",form);
    }
}
