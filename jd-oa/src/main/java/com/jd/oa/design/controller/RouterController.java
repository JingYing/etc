package com.jd.oa.design.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jd.oa.common.exception.BusinessException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.design.service.RouterService;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormItemService;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-12
 * Time: 上午11:36
 * 路由规则
 */
@Controller
@RequestMapping(value="/design")
public class RouterController {
     private static  final Logger logger = Logger.getLogger(RouterController.class);

     @Autowired
     private RouterService routerService;
    /**
     * 路由规则
     */
    @RequestMapping(value = "/router_index", method = RequestMethod.GET)
    public ModelAndView index(String formId,String exp,String processDefinitionId){
        ModelAndView mav = new ModelAndView("design/router_index");
        mav.addObject("formId",formId);
//        try {
//			exp=URLDecoder.decode(exp,"UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			 logger.error(e);
//			 throw new BusinessException("转码失败,表达式异常！");
//		}
        mav.addObject("exp", exp);
        mav.addObject("processDefinitionId",processDefinitionId);
        return mav;
    }

    
    @RequestMapping(value = "/routerForVariable_index", method = RequestMethod.GET)
    public ModelAndView routerForVariable_index(String formId,String exp,String processDefinitionId){
        ModelAndView mav = new ModelAndView("design/routerForVariable_index");
        mav.addObject("formId",formId);
//        try {
//			exp=URLDecoder.decode(exp,"UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			 logger.error(e);
//			 throw new BusinessException("转码失败,表达式异常！");
//		}
        mav.addObject("exp", exp);
        mav.addObject("processDefinitionId",processDefinitionId);
        return mav;
    }
    
    /**
     * 根据表单ID加载主表和明细表
     * @param form
     */
    @RequestMapping(value = "/router_findForms", method=RequestMethod.POST)
    @ResponseBody
    public List<Form>  findForms(Form form){
           List<Form> list=null;
        try{
            list= routerService.findTableByFormId(form);
        }catch (Exception e){
            logger.error(e);
            throw  new BusinessException("加载主表和明细表失败！",e);
        }
           return  list;
    }
    
    @RequestMapping(value = "/router_findAllForms", method=RequestMethod.POST)
    @ResponseBody
    public List<Form>  findAllForms(Form form){
           List<Form> list=null;
        try{
            list= routerService.findAllTablesByFormId(form);
        }catch (Exception e){
            logger.error(e);
            throw  new BusinessException("加载主表和明细表失败！",e);
        }
           return  list;
    }
    
    /**
     * 根据表ID获取表里的信息
     * @param formItem
     * @return
     */
    @RequestMapping(value = "/router_findFormItems",method = RequestMethod.POST)
    @ResponseBody
    public List<FormItem> findFormItems(FormItem formItem){
        return routerService.findFormItemByFormId(formItem);
    }
    
    
    /**
     * 根据表ID获取表里的信息
     * @param formItem
     * @return
     */
    @RequestMapping(value = "/router_findAllFormItemsByFormId",method = RequestMethod.POST)
    @ResponseBody
    public List<FormItem> findAllFormItemsByFormId(FormItem formItem){
        return routerService.findAllFormItemsByFormId(formItem);
    }
    

    
    
    
    /**
     * 获取表单项名称
     * @param formItem
     * @return
     */
    @RequestMapping(value="/router_getFormItemName",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,String> getFormItemName(FormItem formItem){
    	Map<String, String> map = new HashMap<String, String>();
    	FormItem item=routerService.getItemNameById(formItem);
        if(null!=item){
            map.put("itemName", item.getFieldChineseName());
        }
    	return map;
    }
    

}
