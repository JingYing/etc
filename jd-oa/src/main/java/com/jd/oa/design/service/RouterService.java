package com.jd.oa.design.service;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-13
 * Time: 上午10:49
 * 路由规则SEVICE
 */
public interface RouterService {
    /**
     * 查询form详细信息
     * @param form
     * @return
     */
    public List<Form> findTableByFormId(Form form);
    
    public List<Form> findAllTablesByFormId(Form form);

    /**
     * 根据formID获取formItem信息
     * @param formItem
     * @return
     */
    public List<FormItem> findFormItemByFormId(FormItem formItem);
    /**
     * 根据ID和itemId获取Item名称 
     * @param formItem
     * @return
     */
    
    public List<FormItem> findAllFormItemsByFormId(FormItem formItem); 
    
    public FormItem getItemNameById(FormItem formItem);
}
