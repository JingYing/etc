package com.jd.oa.design.service.impl;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.design.dao.RouterDao;
import com.jd.oa.design.service.RouterService;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-13
 * Time: 上午10:49
 * 路由规则ServiceImpl
 */
@Service("routerService")
public class RouterServiceImpl extends BaseServiceImpl<FormItem, Long> implements RouterService{
    private static final Logger logger = Logger.getLogger(RouterServiceImpl.class);
    @Autowired
    private RouterDao routerDao;

    /**
     * 查询表单对应的主表和明细表
     * @param form
     * @return  List
     */
    public List<Form> findTableByFormId(Form form) {
        List<Form> list= routerDao.findTableByFormId(form);
        List<Form> resultList = new ArrayList<Form>();
        for(Form info:list){
          int count=  routerDao.getCountVariable(info);
            if(count>0) {
                resultList.add(info) ;
            }
        }
        return resultList;
    }

    
    public List<Form> findAllTablesByFormId(Form form) {
        List<Form> list= routerDao.findTableByFormId(form);

        return list;
    }
    
    /**
     * 根据formID获取formItem信息
     * @param formItem
     * @return
     */
    public List<FormItem> findFormItemByFormId(FormItem formItem) {
        return routerDao.findFormItemByFormId(formItem);
    }
    
   

    @Override
	public List<FormItem> findAllFormItemsByFormId(FormItem formItem) {
		return routerDao.findAllFormItemsByFormId(formItem) ;
	}

	/**
     * 根据ID和itemId获取Item名称 
     * @param formItem
     * @return
     */
	public FormItem getItemNameById(FormItem formItem) {
		List<FormItem> list=routerDao.getItemNameById(formItem);
		FormItem item= new FormItem();
		if(null!=list&&list.size()>0){
			item=list.get(0);
		}
		return item;
	}
    public RouterDao getDao() {
        return routerDao;
    }
}
