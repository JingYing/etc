/**
 * 
 */
package com.jd.oa.system.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysOperation;

/**
 * @author liub
 *
 */
public interface SysOperationDao extends BaseDao<SysOperation, String>{

}
