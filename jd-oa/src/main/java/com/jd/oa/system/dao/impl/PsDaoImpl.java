package com.jd.oa.system.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.system.dao.PsDao;

@Component("psDao")
public class PsDaoImpl extends MyBatisDaoImpl<Object, Long> 
	implements PsDao {

	private String sqlMapNamespace = "com.jd.oa.system.model.ps";
	
	public Object sql(SqlAdapter sqlAdapter){
		try {
			switch (sqlAdapter.getSqlType()) {
			case DDL_CREATE_TABLE:
				return getSqlSession().update(
						sqlMapNamespace + ".ddl",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DDL_DROP_TABLE:
				return getSqlSession().update(
						sqlMapNamespace + ".ddl",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DDL_ADD_COLUMN:
				return getSqlSession().update(
						sqlMapNamespace + ".ddl",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DDL_DROP_COLUMN:
				return getSqlSession().update(
						sqlMapNamespace + ".ddl",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DDL_CHANGE_COLUMN:
				return getSqlSession().update(
						sqlMapNamespace + ".ddl",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DML_SELECT:
				return getSqlSession().selectList(
						sqlMapNamespace + ".dmlSelect",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DML_INSERT:
				sqlAdapter.addColumn("CREATOR", ComUtils.getLoginName(),
						DataType.STRING);
				SimpleDateFormat formatter = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				sqlAdapter.addColumn("CREATE_TIME",
						formatter.format(new Date()), DataType.DATETIME);
				return getSqlSession().insert(
						sqlMapNamespace + ".dmlInsert",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DML_UPDATE:
				sqlAdapter.addColumn("MODIFIER", ComUtils.getLoginName(),
						DataType.STRING);
				SimpleDateFormat formatter1 = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				sqlAdapter.addColumn("MODIFY_TIME",
						formatter1.format(new Date()), DataType.DATETIME);
				return getSqlSession().update(
						sqlMapNamespace + ".dmlUpdate",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			case DML_DELETE:
				return getSqlSession().delete(
						sqlMapNamespace + ".dmlDelete",
						sqlAdapter.getSql() != null ? sqlAdapter.getSql()
								: sqlAdapter.toSql());
			default:
				return null;
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

}
