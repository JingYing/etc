package com.jd.oa.system.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysAuthExpressionDao;
import com.jd.oa.system.model.SysAuthExpression;

@Component("sysAuthExpressionDao")
public class SysAuthExpressionDaoImpl extends MyBatisDaoImpl<SysAuthExpression, String> implements SysAuthExpressionDao{

}
