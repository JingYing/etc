/**
 * 
 */
package com.jd.oa.system.service;


import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysUserRole;

import java.util.List;

/**
 * @author yujiahe
 * @date 2013年8月30日
 *
 */
public interface SysUserRoleService extends BaseService<SysUserRole, Long> {
    /**
     * @Description: 根据用户ID查询用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
	public List<SysUserRole> getSysUserRoleListByUserId(String UserId);
    /**
     * @Description: 针对一个用户新增多个角色
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public void saveEditSysUserRole(String roleIds,String UserId);
    /**
     * @Description: 批量授权，将一个角色批量授权给多个用户
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public void saveEditSysUserRoles(String roleId,String UserIds);
    /**
     * @Description: 根据角色ID查找用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public List<SysUserRole> findSysUserRoleByRoleId(String roleId);
    
    public List<SysUserRole> findByUserId(String userId);
    /**
     * @Description: 删除某角色下某用户
     * @param roleId ,userId
     * @return String
     * @author yujiahe
     */
    public void sysUserRolesDeletebyUR(String roleId,String userId);

    /**
     * 根据角色查询到用户列表
     * @param roleName
     * @return
     */
    public List<SysUserRole> findUserListByRoleName(String roleName);

}
