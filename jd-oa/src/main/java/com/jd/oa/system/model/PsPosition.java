package com.jd.oa.system.model;

public class PsPosition {
	private String posId, effdt, effStatus, jobCode, descr, descrshort,
			gcPosnFullname, orgId, keyPosition, parentId, reportDottedLine,
			gcMainSequence, gcSequence, gcSubsequence, lastupddttm;

	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	public String getEffdt() {
		return effdt;
	}

	public void setEffdt(String effdt) {
		this.effdt = effdt;
	}

	public String getEffStatus() {
		return effStatus;
	}

	public void setEffStatus(String effStatus) {
		this.effStatus = effStatus;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getDescrshort() {
		return descrshort;
	}

	public void setDescrshort(String descrshort) {
		this.descrshort = descrshort;
	}

	public String getGcPosnFullname() {
		return gcPosnFullname;
	}

	public void setGcPosnFullname(String gcPosnFullname) {
		this.gcPosnFullname = gcPosnFullname;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getKeyPosition() {
		return keyPosition;
	}

	public void setKeyPosition(String keyPosition) {
		this.keyPosition = keyPosition;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getReportDottedLine() {
		return reportDottedLine;
	}

	public void setReportDottedLine(String reportDottedLine) {
		this.reportDottedLine = reportDottedLine;
	}

	public String getGcMainSequence() {
		return gcMainSequence;
	}

	public void setGcMainSequence(String gcMainSequence) {
		this.gcMainSequence = gcMainSequence;
	}

	public String getGcSequence() {
		return gcSequence;
	}

	public void setGcSequence(String gcSequence) {
		this.gcSequence = gcSequence;
	}

	public String getGcSubsequence() {
		return gcSubsequence;
	}

	public void setGcSubsequence(String gcSubsequence) {
		this.gcSubsequence = gcSubsequence;
	}

	public String getLastupddttm() {
		return lastupddttm;
	}

	public void setLastupddttm(String lastupddttm) {
		this.lastupddttm = lastupddttm;
	}

}
