package com.jd.oa.system.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysPositionDao;
import com.jd.oa.system.model.SysPosition;

@Component("sysPositionDao")
public class SysPositionDaoImpl extends MyBatisDaoImpl<SysPosition, String> implements SysPositionDao {
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 下午07:53:14
	 *
	 * @param mp
	 * @return
	 */
	@Override
	public List<SysPosition> selectPosByParentId(Map mp) {
        return this.getSqlSession().selectList("com.jd.oa.system.model.SysPosition.findPosByParentId", mp);
    }

	@Override
	public List<SysPosition> findNotInPs() {
		return getSqlSession().selectList(sqlMapNamespace + ".findNotInPs");
	}
}