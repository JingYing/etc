package com.jd.oa.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.tree.TreeNode;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.model.SysLevel;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysResource;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.system.service.SysLevelService;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysPositionService;
import com.jd.oa.system.service.SysRoleService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;


/**
 * 
 * @author: wangdongxing@jd.com
 */
@Controller
@RequestMapping("/auth")
public class SysAuthExpressionController {
    private static final Logger logger = Logger.getLogger(SysAuthExpressionController.class);

    @Autowired
	private UserAuthorizationService userAuthorizationService;
    
    @Autowired
	private SysUserService sysUserService;
    
    @Autowired
	private SysOrganizationService sysOrganizationService;    
    
    @Autowired
	private SysPositionService sysPositionService; 
    
    @Autowired
    private SysAuthExpressionService sysAuthExpressionService;
    
    @Autowired
    private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
    
    @Autowired
    private SysRoleService sysRoleService;
    
    @Autowired
    private SysLevelService sysLevelService;
    
    
    
    /**
     * 
    
    * @desc Index页
    
    * @author WANGHUI
    
    * @date 2013年9月9日 上午10:57:29
    
    *
    
    * @param locale
    * @param request
    * @param response
    * @return
    * @throws Exception
     */
    
    @RequestMapping(value = "/authExpression_index")
    public ModelAndView auth_expression(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception {
    
    	ModelAndView mav = new ModelAndView("system/authExpression_index");
		
		return mav;
    }
    
    
    /**
     * 
    
    * @desc Update页、Save页
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:49:45
    
    *
    
    * @param locale
    * @param expressions
    * @return
    * @throws Exception
     */
    @RequestMapping(value = "/authExpression_save", produces = "application/json")
    @ResponseBody
    public Object save(@RequestParam(required = false) String expressions) throws Exception {
    	Map<String,Object> resultMap=new HashMap<String, Object>();
    	
    	List<SysAuthExpression> sysAuthExpressions = JsonUtils.fromJsonByGoogle(expressions, new TypeToken<List<SysAuthExpression>>(){}) ;
    	
    	//id不为空
    	SysAuthExpression one = sysAuthExpressions.get(0);
    	
    	if(one!=null && "".equals(one.getId())) {
    		
        	//判断是否重名
        	SysAuthExpression sysAuthExpression=new SysAuthExpression();
        	sysAuthExpression.setAuthExpressionName(one.getAuthExpressionName());
        	sysAuthExpression.setOwner(sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
        	
        	List<SysAuthExpression> list=sysAuthExpressionService.find(sysAuthExpression);
        	if(null!=list&&list.size()>0){
        		resultMap.put("operator", false);
        		resultMap.put("message","存在重名数据，请修改名称！");
        		return resultMap;
        	}
        	
    	//插入授权表达式，并返回授权表达式的id列表
    		one.setId(null);
    		one.setOwner(sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
    		List<String> pks = sysAuthExpressionService.insert(sysAuthExpressions);
    		resultMap.put("operator", true);
    		resultMap.put("message","操作成功");
    		resultMap.put("pks",pks);
    	}else {
        	//判断是否重名
        	SysAuthExpression sysAuthExpression=new SysAuthExpression();
        	sysAuthExpression.setAuthExpressionName(one.getAuthExpressionName());
        	sysAuthExpression.setOwner(sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
        	
        	List<SysAuthExpression> list=sysAuthExpressionService.find(sysAuthExpression);
        	if(null!=list&&list.size()>0){
        		if(!list.get(0).getId().equals(one.getId())){
        			resultMap.put("operator", false);
        			resultMap.put("message","存在重名数据，请修改名称！");
        			return resultMap;
        		}
        	}
    		
    		SysAuthExpression auth = sysAuthExpressionService.get(one.getId());
    		auth.setAuthExpressionName(one.getAuthExpressionName());
    		auth.setGroupId(one.getGroupId());
    		auth.setLevelAbove(one.getLevelAbove());
    		auth.setLevelDown(one.getLevelDown());
    		auth.setRoleId(one.getRoleId());
    		auth.setPositionId(one.getPositionId());
    		auth.setOrganizationId(one.getOrganizationId());
    		auth.setUserId(one.getUserId());
    		sysAuthExpressionService.update(auth);
    		resultMap.put("operator", true);
    		resultMap.put("message","操作成功");
		}
		return resultMap;
    }
    
    /**
     * 
    
    * @desc Edit页（被引用的不删除）
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:50:41
    
    *
    
    * @param locale
    * @param expressionIds
    * @return
     */
    //删除对应id的权限表达式
    @RequestMapping(value = "/authExpression_delete")
    @ResponseBody
    public String expression_delete(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam( required = false) String expressionIds){
    	//sysBussinessAuthExpressionService.deleteBusinessIdKeyAndExpressionId(businessId, expressionId);
    	List<String> ids = JsonUtils.fromJsonByGoogle(expressionIds,new TypeToken<List<String>>(){});
    	 List<SysAuthExpression>  undeleted=sysAuthExpressionService.deleteOnlyUnused("deleteByPrimaryKey", ids.toArray());

    	 return JsonUtils.toJsonByGoogle(undeleted);
    }
    
    
    /**
     * 
    
    * @desc 权限表达式查询ajax
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:48:34
    
    *
    
    * @param request
    * @return
     */
	@RequestMapping(value="/authExpression_page")
	@ResponseBody
	public Object auth_expression_page(HttpServletRequest request){
		//创建pageWrapper
		PageWrapper<SysAuthExpression> pageWrapper=new PageWrapper<SysAuthExpression>(request);
		
		//添加搜索条件
		String auth_expression_name = request.getParameter("authExpressionName");
		boolean meIsAdmin=sysUserService.isAdministrator(ComUtils.getLoginNamePin());
		if(meIsAdmin==false){
			pageWrapper.addSearch("owner",sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
		}
		if(auth_expression_name!=null && ! ("").equals(auth_expression_name))
			pageWrapper.addSearch("auth_expression_name",auth_expression_name );
		
		String selectedVals=request.getParameter("selectedVals");
		if(selectedVals!=null&&!selectedVals.equals("")){
			List<String> ids = JsonUtils.fromJsonByGoogle(selectedVals,new TypeToken<List<String>>(){});
			pageWrapper.addSearch("idList", ids);
		}
		//后台取值
	    sysAuthExpressionService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
	    
	    //返回到页面的额外数据
	    pageWrapper.addResult("returnKey","returnValue");
	    //返回到页面的数据
	    List<SysAuthExpression> sysAuthExpressions  = (List<SysAuthExpression>)pageWrapper.getResult().get("aaData");
	    List<SysAuthExpression> names = userAuthorizationService.getAuthExpressionNameListByIds(sysAuthExpressions);
	    pageWrapper.addResult("names",names);
	    
		String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		
		
        logger.debug(json);
        return json;
	}
    
    
    
    /**
     * 
    
    * @desc 获取所有角色、组、级别的列表
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:47:27
    
    *
    
    * @param request
    * @return
     */
    @RequestMapping(value="/base_info")
	@ResponseBody
	public String base__info(HttpServletRequest request){
    	Map map = new HashMap<String, List>();

    	List<SysRole> roles = sysRoleService.find("findAllRoles",new String[]{""});
    	List<SysRole> groups = sysRoleService.find("findAllGroups",new String[]{""});
		List<SysLevel> levels_1 = sysLevelService.find("findLevel1List",new String[]{""});
		List<SysLevel> levels_2 = sysLevelService.find("findLevel2List",new String[]{""});
		
		map.put("roles", roles);
		map.put("groups", groups);
		map.put("levels_1", levels_1);
		map.put("levels_2", levels_2);
		
    	return JsonUtils.toJsonByGoogle( map);
    }
    
    
   
    
   
  //根据权限表达式得到用户列表vm页面
    @RequestMapping(value = "/users_by_expressions_list")
    public ModelAndView users_by_expressions_list(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception {
    
    	ModelAndView mav = new ModelAndView("system/authExpression_list");
		
		return mav;
    }
    
    //根据权限表达式得到用户列表Ajax
    @RequestMapping(value = "/users_by_expressions_search_list")
    @ResponseBody
    public String users_by_expressions_search_list(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam(required = false)  String authId) throws Exception {
    
    	if(authId== null || authId.equals(""))
    		return  "{\"aaData\" :[]}";
    	
		List<SysAuthExpression> list = sysAuthExpressionService.findAuthExpressionListByIds(new String[] { authId });
		List<SysUser> userList = userAuthorizationService	.getUserListBySysAuthExpressionList(list);

		
		return "{\"aaData\" :"+JsonUtils.toJsonByGoogle(userList)+"}";
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-9-25 上午11:14:13
     *
     * @param locale
     * @param authId
     * @return
     * @throws Exception
     */
    //根据权限表达式得到用户ERPID/NAME字符串Ajax
    @RequestMapping(value="/users_by_expressions_search_str",method=RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map users_by_expressions_search_str(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam(required = false)  String authId) throws Exception {
    	
    	if(authId== null || authId.equals(""))
    		return  null;
    	
		List<SysAuthExpression> list = sysAuthExpressionService.findAuthExpressionListByIds(authId);		
		Map<String, String> map = userAuthorizationService.getUserMapBySysAuthExpressionList(list);
		return map;	
    }
     
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-9-25 上午11:14:13
     *
     * @param locale
     * @param authId
     * @return
     * @throws Exception
     */
    //根据business得到用户ERPID/NAME字符串Ajax
    @RequestMapping(value="/users_by_business",method=RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map users_by_business(String businessType, String businessId) throws Exception {

    	if(businessType == null || businessType.equals("") || businessId == null || businessId.equals(""))
    		return  null;
    	
    	//转化任务类型
		String busType="";
		if("process".equals(businessType))  busType=SysBussinessAuthExpression.BUSSINESS_TYPE_PROCESS;
		if("node".equals(businessType))  busType=SysBussinessAuthExpression.BUSSINESS_TYPE_NODE;
		if("supervise".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_SUPERVISE;
		
    	List<SysAuthExpression> list = sysAuthExpressionService.findExpressionByBusinessIdAndBusinessType(busType, businessId);
		List<SysUser> userList = userAuthorizationService.getUserListBySysAuthExpressionList(list);
		
		StringBuffer usersErpidB = new StringBuffer();
		StringBuffer usersNameB = new StringBuffer();
		for (SysUser u : userList){
			usersErpidB.append(u.getUserName()).append(",");
			usersNameB.append(u.getRealName()).append(",");
		}
		
		if (usersErpidB.length()>0 || usersNameB.length()>0 ){
			String usersErpid = usersErpidB.toString();
			String usersName = usersNameB.toString();
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("usersErpid", usersErpid.substring(0, usersErpid.length()-1));
			map.put("usersName", usersName.substring(0, usersName.length()-1));
			
			return map;
		}else{
			return null;
		}
		
    }
    
    
    @RequestMapping(value = "/authExpression_assgin")
    public String assign(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception {
    	
    	return "system/authExpression_assgin";
    }
    
    
    
    
    /**
     * 
    
    * @desc Edit页
    
    * @author WANGHUI
    
    * @date 2013年9月9日 上午11:30:28
    
    *
    
    * @param locale
    * @param expressions
    * @return
    * @throws Exception
     */
    
    @RequestMapping(value = "/authExpression_edit")
    @ResponseBody
    public String expression_edit(
            @RequestParam(value = "locale", required = false) Locale locale,
            @RequestParam(required = false) String expressions) throws Exception {
    	
    	List<SysAuthExpression> sysAuthExpressions = JsonUtils.fromJsonByGoogle(expressions, new TypeToken<List<SysAuthExpression>>(){}) ;
    	
    	sysAuthExpressionService.update(sysAuthExpressions);
    	
    	logger.debug("expressions==="+expressions);

    	
    	 
        return "ok:" ;//+ userList.size();
    }
    
    
    
    //根据文档Id列出所有权限表达式
    @RequestMapping(value = "/expression_by_businessid_list")
    @ResponseBody
    public String expressions_list(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam( required = false) String businessId){
    	
    	String doc=businessId;
    	
    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("business_id", doc);
    	List<SysBussinessAuthExpression> sysBussinessAuthExpressions = sysBussinessAuthExpressionService.find("findByBusinessId",map);
    	
    	List<SysAuthExpression> sysAuthExpressions = new ArrayList<SysAuthExpression>();
    	int length = sysBussinessAuthExpressions.size();
    	for (int i=0;i<length;i++) {
    		String id = sysBussinessAuthExpressions.get(i).getAuthExpressionId();
			sysAuthExpressions.add(sysAuthExpressionService.get(id));
		}
    	
    	String json = JsonUtils.toJsonByGoogle(sysAuthExpressions);
    	
    	
    	return json;
    }
    
    
    
    
  //删除对应id的权限表达式
    @RequestMapping(value = "/resource_update")
    @ResponseBody
    public String resource_update(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam( required = false) String expressionIds){
    	//sysBussinessAuthExpressionService.deleteBusinessIdKeyAndExpressionId(businessId, expressionId);
    	List<SysBussinessAuthExpression> sysBussinessAuthExpressions = JsonUtils.fromJsonByGoogle(expressionIds,new TypeToken<List<SysBussinessAuthExpression>>(){});
        sysBussinessAuthExpressionService.updateByResources(sysBussinessAuthExpressions);
    	return "ok";
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-8-30 上午11:02:20
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/expression_findUserIndex")    
	public String findAuthExpressionUserIndex(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response){
		
        return "/system/authExpressionUser_list";
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-8-30 下午12:21:27
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/expression_findUserList")
    @ResponseBody
	public Object findAuthExpressionUserBySearch(HttpServletRequest request){
		//创建pageWrapper
		PageWrapper<SysUser> pageWrapper=new PageWrapper<SysUser>(request);
		//添加搜索条件
		String realName=request.getParameter("realName");
		pageWrapper.addSearch("realName",realName);
		//后台取值
		sysUserService.find(pageWrapper.getPageBean(), "findAuthExpressionUserBySearch", pageWrapper.getConditionsMap());
	    //返回到页面的数据
		String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        return json;
    }
    
    /**
     * 
     * @desc 组织机构Tree
     * @author WXJ
     * @date 2013-9-2 下午01:25:54
     *
     * @param locale
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/expression_orgTreeIndex")    
	public String orgTreeIndex(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response){
		
        return "/system/sysOrganization_tree";
    }
    
    /**
     * 
     * @desc 树数据加载
     * @author WXJ
     * @date 2013-9-2 上午11:24:45
     *
     * @param sysOrganization
     * @return 树节点列表
     */
    @RequestMapping(value = "/expression_orgTreeLoad", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<TreeNode> orgTreeLoad(SysOrganization sysOrganization){
    	Map mp = new HashMap();
    	mp.put("id", sysOrganization.getId());
        List<SysOrganization> sysOrganizationList = sysOrganizationService.selectOrgByParentId(mp);
        List<TreeNode> listData= this.getOrgTreeNodes(sysOrganizationList);    //转化成Ztree支持的数据格式
        return listData;
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-9-2 上午11:30:38
     *
     * @param sysOrganizationList
     * @return
     */
    private List<TreeNode> getOrgTreeNodes(List<SysOrganization> sysOrganizationList){
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        for(SysOrganization sysOrganization : sysOrganizationList){
            TreeNode treeNode = new TreeNode();
            treeNode.setId(sysOrganization.getOrganizationCode());
            treeNode.setpId(sysOrganization.getParentId());
            treeNode.setName(sysOrganization.getOrganizationName());
            treeNode.setIsParent(sysOrganization.getIsParent());  
            treeNode.setIconSkin(SysResource.class.getSimpleName().toLowerCase());
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("id", sysOrganization.getId());
            props.put("parentId", sysOrganization.getParentId());
            props.put("orgFullname", sysOrganization.getOrganizationFullname());
            props.put("orgCode", sysOrganization.getOrganizationCode());
            treeNode.setProps(props);
            treeNodes.add(treeNode);
        }
        return treeNodes;
	}
        
    
    
    /**
     * 
     * @desc 职位Tree
     * @author WXJ
     * @date 2013-9-2 下午8:24:45
     *
     * @param locale
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/expression_posTreeIndex")    
	public String posTreeIndex(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response){
		
        return "/system/sysPosition_tree";
    }
    
   /**
    * 
    * @desc 树数据加载
    * @author WXJ
    * @date 2013-9-2 下午8:24:45
    *
    * @param sysOrganization
    * @return 树节点列表
    */
   @RequestMapping(value = "/expression_posTreeLoad", method = RequestMethod.POST, produces = "application/json")
   @ResponseBody
   public List<TreeNode> posTreeLoad(SysPosition sysPosition){
	   Map mp = new HashMap();
	   mp.put("id", sysPosition.getId());
	   List<SysPosition> sysPositionList = sysPositionService.selectPosByParentId(mp);
	   List<TreeNode> listData= this.getPosTreeNodes(sysPositionList);    //转化成Ztree支持的数据格式
	   return listData;
   }
   
   /**
    * 
    * @desc 
    * @author WXJ
    * @date 2013-9-2 下午8:24:45
    *
    * @param sysOrganizationList
    * @return
    */
   private List<TreeNode> getPosTreeNodes(List<SysPosition> sysPositionList){
       List<TreeNode> treeNodes = new ArrayList<TreeNode>();
       for(SysPosition sysPosition : sysPositionList){
           TreeNode treeNode = new TreeNode();
           treeNode.setId(sysPosition.getPositionCode());
           treeNode.setpId(sysPosition.getParentId());
           treeNode.setName(sysPosition.getPositionName());
           treeNode.setIsParent(sysPosition.getIsParent());
           treeNode.setIconSkin(SysResource.class.getSimpleName().toLowerCase());
           Map<String, Object> props = new HashMap<String, Object>();
           props.put("id", sysPosition.getId());
           props.put("parentId", sysPosition.getParentId());
           treeNode.setProps(props);
           treeNodes.add(treeNode);
       }
       return treeNodes;
	}
   @RequestMapping(value="/authExpression_select")
   public ModelAndView selectAuthExpression(String selectedIds,String byIFrame){
	   ModelAndView mav=new ModelAndView("system/authExpression_select");
	   //传递到页面上需要被选中的ID
	   mav.addObject("selectedIds", selectedIds);
	   //传递到页面授权页面是不是iframe中
	   mav.addObject("byIFrame",byIFrame==null?"0":byIFrame);
	   return mav;
   }
   
   
   @RequestMapping(value = "/resources_by_userid")
   @ResponseBody
   public String resources_by_userid(
		   @RequestParam(value = "locale", required = false) Locale locale,
   			@RequestParam( required = false) String userid){
	   if(userid!=null && !userid.equals("")){
		   List<SysBussinessAuthExpression> list =  userAuthorizationService.getResourceByUserId(userid, "1");
	   	   return JsonUtils.toJsonByGoogle(list);
	   }
	   return "no argument : userid";
	   
   }
   
   @RequestMapping(value = "/resources_owned_by_userid")
   @ResponseBody
   public String resources_owned_by_userid(
		   @RequestParam(value = "locale", required = false) Locale locale,
   		   @RequestParam( required = false) String userid,
   		   @RequestParam( required = false) String resourceid){
	   
	   if(userid!=null && !userid.equals("") && resourceid!=null && !resourceid.equals("")){
		   boolean is= userAuthorizationService.isResourceOwnedByUser(resourceid, "1", userid);
		  
		  return JsonUtils.toJsonByGoogle(is);
	  
	   }
	   
	   return "no arguments : userid, resourceid";
	   
   }
}
