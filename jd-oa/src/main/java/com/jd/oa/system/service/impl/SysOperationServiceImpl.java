
package com.jd.oa.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysOperationDao;
import com.jd.oa.system.model.SysOperation;
import com.jd.oa.system.service.SysOperationService;

/**
 * @author liub
 *
 */
@Component
public class SysOperationServiceImpl extends BaseServiceImpl<SysOperation, String> implements SysOperationService {
	
	@Autowired(required=true)
	private SysOperationDao sysOperationDao;
	
	public SysOperationDao getDao() {
		return sysOperationDao;
	}
}