package com.jd.oa.system.model;

import java.util.Date;

import com.jd.oa.common.model.IdModel;

public class SysOperation extends IdModel{
    

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_OPERATION.OPERATION_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String operationCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_OPERATION.OPERATION_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String operationName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_OPERATION.DESCRIPTION
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String description;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_OPERATION.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String creator;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_OPERATION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_OPERATION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String modifier;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_OPERATION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private Date modifyTime;

    
    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_OPERATION.OPERATION_CODE
     *
     * @return the value of T_JDOA_SYS_OPERATION.OPERATION_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getOperationCode() {
        return operationCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_OPERATION.OPERATION_CODE
     *
     * @param operationCode the value for T_JDOA_SYS_OPERATION.OPERATION_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode == null ? null : operationCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_OPERATION.OPERATION_NAME
     *
     * @return the value of T_JDOA_SYS_OPERATION.OPERATION_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getOperationName() {
        return operationName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_OPERATION.OPERATION_NAME
     *
     * @param operationName the value for T_JDOA_SYS_OPERATION.OPERATION_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setOperationName(String operationName) {
        this.operationName = operationName == null ? null : operationName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_OPERATION.DESCRIPTION
     *
     * @return the value of T_JDOA_SYS_OPERATION.DESCRIPTION
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_OPERATION.DESCRIPTION
     *
     * @param description the value for T_JDOA_SYS_OPERATION.DESCRIPTION
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_OPERATION.CREATOR
     *
     * @return the value of T_JDOA_SYS_OPERATION.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_OPERATION.CREATOR
     *
     * @param creator the value for T_JDOA_SYS_OPERATION.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_OPERATION.CREATE_TIME
     *
     * @return the value of T_JDOA_SYS_OPERATION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_OPERATION.CREATE_TIME
     *
     * @param createTime the value for T_JDOA_SYS_OPERATION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_OPERATION.MODIFIER
     *
     * @return the value of T_JDOA_SYS_OPERATION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_OPERATION.MODIFIER
     *
     * @param modifier the value for T_JDOA_SYS_OPERATION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_OPERATION.MODIFY_TIME
     *
     * @return the value of T_JDOA_SYS_OPERATION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_OPERATION.MODIFY_TIME
     *
     * @param modifyTime the value for T_JDOA_SYS_OPERATION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}