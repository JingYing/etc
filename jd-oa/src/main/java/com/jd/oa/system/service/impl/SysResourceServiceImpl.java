package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.redis.annotation.CacheKey;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.redis.annotation.CachePut;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysResourceDao;
import com.jd.oa.system.model.SysResource;
import com.jd.oa.system.service.SysResourceService;

/**
 * 
 * @author xulin
 *
 */
//Spring Service Bean的标识.
@Service("sysResourceService")
//默认将类中的所有函数纳入事务管理.
@Transactional
@CacheProxy
public class SysResourceServiceImpl extends BaseServiceImpl<SysResource, String>
		implements SysResourceService {
	
	@Autowired
	private SysResourceDao resourceDao;

	public SysResourceDao getDao() {
		return resourceDao;
	}
	
	/**
	 * 根据父ID查找子资源列表
	 * @param parentId 父ID
	 * @return 子资源列表
	 */
	@Override
	public List<SysResource> findByParentId(String parentId) {
		return getDao().find("findByParentId", parentId);
	}
	
	/**
	 * 判断是否存在资源
	 */
	@Override
	public Integer findCountByRes(SysResource sysResource) {
		return resourceDao.findCountByRes(sysResource);
	}
	
	@Override
	public List<SysResource> findByUserId(String userId) {
		return getDao().findByUserId(userId);
	}
	
	@CachePut(key = @CacheKey(template = "JDOA/AssignedResources/${p0}"), expire = 43200)
	public List<SysResource> findAssignedResources(String userId) {

		Set<SysResource> assignedResources = new HashSet<SysResource>(
				findByUserId(userId));

		Set<SysResource> sortResources = new TreeSet<SysResource>(
				new Comparator() {
					@Override
					public int compare(Object arg0, Object arg1) {
						int r = 0;
						SysResource resource0 = (SysResource) arg0;
						SysResource resource1 = (SysResource) arg1;
						if (resource0.getId() != resource1.getId()) {
							if (resource0.getParent() == null) {
								if (resource1.getParent() == null) {
									if (resource0.getSortNo() > resource1
											.getSortNo())
										r = 1;
									else if (resource0.getSortNo() < resource1
											.getSortNo())
										r = -1;
									else
										r = 1;
								} else {
									if (resource1.getParent().getId()
											.equals(resource0.getId()))
										r = -1;
									else {
										if (resource0.getSortNo() > resource1
												.getParent().getSortNo())
											r = 1;
										else if (resource0.getSortNo() < resource1
												.getParent().getSortNo())
											r = -1;
										else
											r = 1;
									}
								}
							} else {
								if (resource1.getParent() == null) {
									if (resource0.getParent().getId()
											.equals(resource1.getId()))
										r = 1;
									else {
										if (resource0.getParent().getSortNo() > resource1
												.getSortNo())
											r = 1;
										else if (resource0.getParent()
												.getSortNo() < resource1
												.getSortNo())
											r = -1;
										else
											r = 1;
									}
								} else {
									if (resource0
											.getParent()
											.getId()
											.equals(resource1.getParent()
													.getId())) {
										if (resource0.getSortNo() > resource1
												.getSortNo())
											r = 1;
										else if (resource0.getSortNo() < resource1
												.getSortNo())
											r = -1;
										else
											r = 1;
									} else {
										if (resource0.getParent().getSortNo() > resource1
												.getParent().getSortNo())
											r = 1;
										else if (resource0.getParent()
												.getSortNo() < resource1
												.getParent().getSortNo())
											r = -1;
										else
											r = 1;
									}
								}
							}
						}
						return r;
					}
				});

		for (SysResource r : assignedResources) {
			if(r.isDisplayStatus())
				sortResources.addAll(getResourcesTree(r));
		}
		
		return new ArrayList<SysResource>(sortResources);
	}
	
	/**
	 * 获得该资源的资源树数据，包含该资源的所有上级节点
	 * 
	 * @param resource
	 *            资源
	 * @return
	 */
	private Set<SysResource> getResourcesTree(SysResource resource) {

		Set<SysResource> resources = new HashSet<SysResource>();

		// 是否有父节点
		if (resource.getParent() != null) {
			resources.addAll(getResourcesTree(resource.getParent()));
		}
		resources.add(resource);
		return resources;
	}
	
	/**
	 * 新增资源
	 * @param bean
	 */
	public void insertResource(SysResource bean) {
		this.insert(bean);
		if(!"root".equals(bean.getParentId())){
			SysResource parentBean = this.get(bean.getParentId());
			parentBean.setChildrenNum(parentBean.getChildrenNum()+1);
			this.update(parentBean);
		}
	}
	
	/**
	 * 删除资源
	 * @param bean
	 */
	public void deleteResource(SysResource bean) {
		this.delete(bean);
		if(!"root".equals(bean.getParentId())){
			SysResource parentBean = this.get(bean.getParentId());
			parentBean.setChildrenNum(parentBean.getChildrenNum()-1);
			this.update(parentBean);
		}
	}
}
