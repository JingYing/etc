package com.jd.oa.system.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysResource;
import com.jd.oa.system.service.SysOrganizationService;

/**
 * 
 * @author: xulin
 */
@Controller
@RequestMapping(value = "/system")
public class SysOrganizationController {
	private static final Logger logger = Logger.getLogger(SysOrganizationController.class);

	@Autowired
	private SysOrganizationService sysOrganizationService;

	/**
	 * 初始化组织架构树
	 * 
	 * @param locale
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sysOrganization_index", method = RequestMethod.GET)
	public ModelAndView index(
			@RequestParam(value = "locale", required = false) Locale locale,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("system/sysOrganization_index");
		return mav;
	}
	
	/**
	 * 组织架构查看页面
	 * 
	 * @param sysOrganization
	 *  组织架构对象
	 */
	@RequestMapping(value = "/sysOrganization_view", method = RequestMethod.GET)
	public ModelAndView folderView(SysOrganization sysOrganization) {
		sysOrganization = sysOrganizationService.get(sysOrganization.getId());
		ModelAndView mav = new ModelAndView("system/sysOrganization_view");
		mav.addObject("sysOrganization", sysOrganization);
		return mav;
	}
}
