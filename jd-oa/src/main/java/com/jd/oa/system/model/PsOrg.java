package com.jd.oa.system.model;

public class PsOrg {
	private String hrOrgOrgId, hrOrgName, hrOrgFullName, hrOrgParentId,
			hrOrgRank, gcOrgLevel, gcDeptDescr02, gcDeptDescr03,
			hrOrgTimecreated, hrOrgTimemodified, gid;

	public String getHrOrgOrgId() {
		return hrOrgOrgId;
	}

	public void setHrOrgOrgId(String hrOrgOrgId) {
		this.hrOrgOrgId = hrOrgOrgId;
	}

	public String getHrOrgName() {
		return hrOrgName;
	}

	public void setHrOrgName(String hrOrgName) {
		this.hrOrgName = hrOrgName;
	}

	public String getHrOrgFullName() {
		return hrOrgFullName;
	}

	public void setHrOrgFullName(String hrOrgFullName) {
		this.hrOrgFullName = hrOrgFullName;
	}

	public String getHrOrgParentId() {
		return hrOrgParentId;
	}

	public void setHrOrgParentId(String hrOrgParentId) {
		this.hrOrgParentId = hrOrgParentId;
	}

	public String getHrOrgRank() {
		return hrOrgRank;
	}

	public void setHrOrgRank(String hrOrgRank) {
		this.hrOrgRank = hrOrgRank;
	}

	public String getGcOrgLevel() {
		return gcOrgLevel;
	}

	public void setGcOrgLevel(String gcOrgLevel) {
		this.gcOrgLevel = gcOrgLevel;
	}

	public String getGcDeptDescr02() {
		return gcDeptDescr02;
	}

	public void setGcDeptDescr02(String gcDeptDescr02) {
		this.gcDeptDescr02 = gcDeptDescr02;
	}

	public String getGcDeptDescr03() {
		return gcDeptDescr03;
	}

	public void setGcDeptDescr03(String gcDeptDescr03) {
		this.gcDeptDescr03 = gcDeptDescr03;
	}

	public String getHrOrgTimecreated() {
		return hrOrgTimecreated;
	}

	public void setHrOrgTimecreated(String hrOrgTimecreated) {
		this.hrOrgTimecreated = hrOrgTimecreated;
	}

	public String getHrOrgTimemodified() {
		return hrOrgTimemodified;
	}

	public void setHrOrgTimemodified(String hrOrgTimemodified) {
		this.hrOrgTimemodified = hrOrgTimemodified;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}
}