package com.jd.oa.system.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysBussinessAuthExpressionDao;
import com.jd.oa.system.model.SysBussinessAuthExpression;

@Component("sysBussinessAuthExpressionDao")
public class SysBussinessAuthExpressionDaoImpl extends MyBatisDaoImpl<SysBussinessAuthExpression, String> implements SysBussinessAuthExpressionDao{

}
