package com.jd.oa.system.service;

import java.util.List;
import java.util.Set;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysResource;

/**
 * 
 * @author xulin
 *
 */
public interface SysResourceService extends BaseService<SysResource, String>{
	
	/**
	 * 根据父ID查找子资源列表
	 * @param parentId 父ID
	 * @return 子资源列表
	 */
	public List<SysResource> findByParentId(String parentId);
	
	/**
	 * 判断是否存在资源
	 * @param sysResource
	 * @return
	 */
	public Integer findCountByRes(SysResource sysResource);
	
	/**
	 * 根据用户ID获取用户分配的所有叶子资源信息
	 * @param userId 用户Id，主键ID
	 * 
	 * @author zhouhuaqi
	 * @date 2013-10-08 15:33:00瑟
	 * @version V1.0
	 */
	public List<SysResource> findByUserId(String userId);
	
	/**
	 * 根据用户ID获取用户分配的资源数据列表
	 * @param userId 用户Id，主键ID
	 * @return
	 */
	public List<SysResource> findAssignedResources(String userId);
	
	/**
	 * 新增资源
	 * @param bean
	 */
	public void insertResource(SysResource bean);
	
	/**
	 * 删除资源
	 * @param bean
	 */
	public void deleteResource(SysResource bean);
}
