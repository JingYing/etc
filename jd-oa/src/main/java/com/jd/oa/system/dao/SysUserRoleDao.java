/**
 * 
 */
package com.jd.oa.system.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysUserRole;

import java.util.List;

/**
 * @author yujiahe
 * @date 2013年8月30日
 *
 */
public interface SysUserRoleDao extends BaseDao<SysUserRole, Long> {
    /**
     * @Description: 新增用户角色配置
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public void insertSysUserRole(SysUserRole sysUserRole);
    /**
     * @Description: 根据用户ID获取用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public List<SysUserRole> findSysUserRoleListByUserId(String userId);
    
    
    public List<SysUserRole> findByUserId(String userId);
    /**
     * @Description: 根据D用户ID，删除用户角色配置
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public int deleteUserRoleByUserId(String userId);
    /**
     * @Description: 根据角色ID用户ID，删除用户角色配置
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public int deleteSysUserRoleByUR(SysUserRole sysUserRole);
    /**
     * @Description: 根据角色ID查找用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public List<SysUserRole> findSysUserRoleListByRoleId(String roleId);
    /**
     * 根据角色查询到用户列表
     * @param roleName
     * @return
     */
    public List<SysUserRole> findUserListByRoleName(String roleName);
}
