package com.jd.oa.system.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysUser;

public interface SysUserDao extends BaseDao<SysUser, String> {

	/**
	 * 根据用户名和组织机构类别获取用户信息
	 * 
	 * @param userName
	 *            用户名
	 * @param organizationType
	 *            组织机构类型
	 * @return 用户信息
	 * 
	 * @author zhouhuaqi
	 * @date 2013-10-23 10:07:00
	 * @version V1.0
	 */
	public SysUser getByUserName(String userName, String organizationType);
	
	/**
	 * 根据邮箱、手机和组织机构类别获取用户信息
	 * 
	 * @param email
	 *            邮箱
	 * @param mobile
	 *            手机
	 * @param organizationType
	 *            组织机构类型
	 * @return 用户信息
	 * 
	 * @author zhouhuaqi
	 * @date 2013-11-20 23:48:00
	 * @version V1.0
	 */
	public SysUser getByEmailAndMobile(String email, String mobile, String organizationType);

	/**
	 * 根据用户编码和组织机构类型获取用户的email和mobile
	 * @param userName 用户名称，erpId
	 * @param organizationType 组织机构类型
	 * @return
	 */
	public SysUser getUserMailAndPhoneByUserName(String userName, String... organizationType);

	/**
	 * 查询在peoplesoft中存在, 在OA中不存在的记录
	 * @return
	 */
	public List<SysUser> findNotInPs();
	
	/**
	 * 根据id更新状态
	 * @param id
	 * @param status
	 */
	public void updateStatus(String id, String status);
	
}
