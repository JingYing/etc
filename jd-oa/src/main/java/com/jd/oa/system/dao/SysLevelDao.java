package com.jd.oa.system.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysLevel;

public interface SysLevelDao extends BaseDao<SysLevel, String> {
	/**
	 * 
	 * @desc 获得1级职级
	 * @author WXJ
	 * @date 2013-9-3 下午05:02:51
	 *
	 * @return
	 */
	public List<SysLevel> findLevel1List();
	
	/**
	 * 
	 * @desc 获得2级职级
	 * @author WXJ
	 * @date 2013-9-3 下午05:02:51
	 *
	 * @return
	 */
	public List<SysLevel> findLevel2List();
}
