package com.jd.oa.system.model;

public class PsUser {
    private String hrPersonPersonId;

    private String misUserId;

    private String hrPersonPersonname;

    private String misUserUsername;

    private String cth440Codename;

    private String hrPersonNationalId;

    private String hrPersonErpid;

    private String hrPersonQyyx;

    private String hrPersonEntryDate;

    private String hrPersonLzrq;

    private String hrPersonTimecreated;

    private String hrPersonTimemodified;

    private String hrPersonGrlxdht;

    private String hrPersonAddress;

    private String hrPersonSex;

    private String hrPersonBirthday;

    private String hrPersonSyqjs;

    private String hrPersonMaritalStatus;

    private String hrPersonIdentity;

    private String hrPersonNation;

    private String hrPersonHometown;

    private String a04A0401;

    private String cth121Codename;

    private String a04A0403;

    private String a04A0407;

    private String hrPersonHousehold;

    private String hrPersonJjlxro;

    private String hrPersonDabh;

    private String hrPersonHouseholdType;

    private String cth206Codeid;

    private String cth206Codename;

    private String hrOrgOrgId;

    private String hrOrgFullName;

    private String hrOrgParentId;

    private String hrOrgRank;

    private String hrOrg1;

    private String hrOrg2;

    private String hrOrg3;

    private String hrOrg4;

    private String hrOrg5;

    private String hrOrgTimecreated;

    private String hrOrgTimemodified;

    private String hrPositionPosId;

    private String hrPositionName;

    private String hrPositionParentId;

    private String posJobtypeNameP;

    private String posJobtypeNamePp;

    private String posJobtypeName;

    private String posJoblevel;

    private String posAssignTimecreated;

    private String posJob;

    private String descr1006;

    private String ctj021Codeid;

    private String cth148Codename;

    private String hrPersonTel;

    private String cth148Codeid;

    public String getHrPersonPersonId() {
        return hrPersonPersonId;
    }

    public void setHrPersonPersonId(String hrPersonPersonId) {
        this.hrPersonPersonId = hrPersonPersonId == null ? null : hrPersonPersonId.trim();
    }

    public String getMisUserId() {
        return misUserId;
    }

    public void setMisUserId(String misUserId) {
        this.misUserId = misUserId == null ? null : misUserId.trim();
    }

    public String getHrPersonPersonname() {
        return hrPersonPersonname;
    }

    public void setHrPersonPersonname(String hrPersonPersonname) {
        this.hrPersonPersonname = hrPersonPersonname == null ? null : hrPersonPersonname.trim();
    }

    public String getMisUserUsername() {
        return misUserUsername;
    }

    public void setMisUserUsername(String misUserUsername) {
        this.misUserUsername = misUserUsername == null ? null : misUserUsername.trim();
    }

    public String getCth440Codename() {
        return cth440Codename;
    }

    public void setCth440Codename(String cth440Codename) {
        this.cth440Codename = cth440Codename == null ? null : cth440Codename.trim();
    }

    public String getHrPersonNationalId() {
        return hrPersonNationalId;
    }

    public void setHrPersonNationalId(String hrPersonNationalId) {
        this.hrPersonNationalId = hrPersonNationalId == null ? null : hrPersonNationalId.trim();
    }

    public String getHrPersonErpid() {
        return hrPersonErpid;
    }

    public void setHrPersonErpid(String hrPersonErpid) {
        this.hrPersonErpid = hrPersonErpid == null ? null : hrPersonErpid.trim();
    }

    public String getHrPersonQyyx() {
        return hrPersonQyyx;
    }

    public void setHrPersonQyyx(String hrPersonQyyx) {
        this.hrPersonQyyx = hrPersonQyyx == null ? null : hrPersonQyyx.trim();
    }

    public String getHrPersonEntryDate() {
        return hrPersonEntryDate;
    }

    public void setHrPersonEntryDate(String hrPersonEntryDate) {
        this.hrPersonEntryDate = hrPersonEntryDate == null ? null : hrPersonEntryDate.trim();
    }

    public String getHrPersonLzrq() {
        return hrPersonLzrq;
    }

    public void setHrPersonLzrq(String hrPersonLzrq) {
        this.hrPersonLzrq = hrPersonLzrq == null ? null : hrPersonLzrq.trim();
    }

    public String getHrPersonTimecreated() {
        return hrPersonTimecreated;
    }

    public void setHrPersonTimecreated(String hrPersonTimecreated) {
        this.hrPersonTimecreated = hrPersonTimecreated == null ? null : hrPersonTimecreated.trim();
    }

    public String getHrPersonTimemodified() {
        return hrPersonTimemodified;
    }

    public void setHrPersonTimemodified(String hrPersonTimemodified) {
        this.hrPersonTimemodified = hrPersonTimemodified == null ? null : hrPersonTimemodified.trim();
    }

    public String getHrPersonGrlxdht() {
        return hrPersonGrlxdht;
    }

    public void setHrPersonGrlxdht(String hrPersonGrlxdht) {
        this.hrPersonGrlxdht = hrPersonGrlxdht == null ? null : hrPersonGrlxdht.trim();
    }

    public String getHrPersonAddress() {
        return hrPersonAddress;
    }

    public void setHrPersonAddress(String hrPersonAddress) {
        this.hrPersonAddress = hrPersonAddress == null ? null : hrPersonAddress.trim();
    }

    public String getHrPersonSex() {
        return hrPersonSex;
    }

    public void setHrPersonSex(String hrPersonSex) {
        this.hrPersonSex = hrPersonSex == null ? null : hrPersonSex.trim();
    }

    public String getHrPersonBirthday() {
        return hrPersonBirthday;
    }

    public void setHrPersonBirthday(String hrPersonBirthday) {
        this.hrPersonBirthday = hrPersonBirthday == null ? null : hrPersonBirthday.trim();
    }

    public String getHrPersonSyqjs() {
        return hrPersonSyqjs;
    }

    public void setHrPersonSyqjs(String hrPersonSyqjs) {
        this.hrPersonSyqjs = hrPersonSyqjs == null ? null : hrPersonSyqjs.trim();
    }

    public String getHrPersonMaritalStatus() {
        return hrPersonMaritalStatus;
    }

    public void setHrPersonMaritalStatus(String hrPersonMaritalStatus) {
        this.hrPersonMaritalStatus = hrPersonMaritalStatus == null ? null : hrPersonMaritalStatus.trim();
    }

    public String getHrPersonIdentity() {
        return hrPersonIdentity;
    }

    public void setHrPersonIdentity(String hrPersonIdentity) {
        this.hrPersonIdentity = hrPersonIdentity == null ? null : hrPersonIdentity.trim();
    }

    public String getHrPersonNation() {
        return hrPersonNation;
    }

    public void setHrPersonNation(String hrPersonNation) {
        this.hrPersonNation = hrPersonNation == null ? null : hrPersonNation.trim();
    }

    public String getHrPersonHometown() {
        return hrPersonHometown;
    }

    public void setHrPersonHometown(String hrPersonHometown) {
        this.hrPersonHometown = hrPersonHometown == null ? null : hrPersonHometown.trim();
    }

    public String getA04A0401() {
        return a04A0401;
    }

    public void setA04A0401(String a04A0401) {
        this.a04A0401 = a04A0401 == null ? null : a04A0401.trim();
    }

    public String getCth121Codename() {
        return cth121Codename;
    }

    public void setCth121Codename(String cth121Codename) {
        this.cth121Codename = cth121Codename == null ? null : cth121Codename.trim();
    }

    public String getA04A0403() {
        return a04A0403;
    }

    public void setA04A0403(String a04A0403) {
        this.a04A0403 = a04A0403 == null ? null : a04A0403.trim();
    }

    public String getA04A0407() {
        return a04A0407;
    }

    public void setA04A0407(String a04A0407) {
        this.a04A0407 = a04A0407 == null ? null : a04A0407.trim();
    }

    public String getHrPersonHousehold() {
        return hrPersonHousehold;
    }

    public void setHrPersonHousehold(String hrPersonHousehold) {
        this.hrPersonHousehold = hrPersonHousehold == null ? null : hrPersonHousehold.trim();
    }

    public String getHrPersonJjlxro() {
        return hrPersonJjlxro;
    }

    public void setHrPersonJjlxro(String hrPersonJjlxro) {
        this.hrPersonJjlxro = hrPersonJjlxro == null ? null : hrPersonJjlxro.trim();
    }

    public String getHrPersonDabh() {
        return hrPersonDabh;
    }

    public void setHrPersonDabh(String hrPersonDabh) {
        this.hrPersonDabh = hrPersonDabh == null ? null : hrPersonDabh.trim();
    }

    public String getHrPersonHouseholdType() {
        return hrPersonHouseholdType;
    }

    public void setHrPersonHouseholdType(String hrPersonHouseholdType) {
        this.hrPersonHouseholdType = hrPersonHouseholdType == null ? null : hrPersonHouseholdType.trim();
    }

    public String getCth206Codeid() {
        return cth206Codeid;
    }

    public void setCth206Codeid(String cth206Codeid) {
        this.cth206Codeid = cth206Codeid == null ? null : cth206Codeid.trim();
    }

    public String getCth206Codename() {
        return cth206Codename;
    }

    public void setCth206Codename(String cth206Codename) {
        this.cth206Codename = cth206Codename == null ? null : cth206Codename.trim();
    }

    public String getHrOrgOrgId() {
        return hrOrgOrgId;
    }

    public void setHrOrgOrgId(String hrOrgOrgId) {
        this.hrOrgOrgId = hrOrgOrgId == null ? null : hrOrgOrgId.trim();
    }

    public String getHrOrgFullName() {
        return hrOrgFullName;
    }

    public void setHrOrgFullName(String hrOrgFullName) {
        this.hrOrgFullName = hrOrgFullName == null ? null : hrOrgFullName.trim();
    }

    public String getHrOrgParentId() {
        return hrOrgParentId;
    }

    public void setHrOrgParentId(String hrOrgParentId) {
        this.hrOrgParentId = hrOrgParentId == null ? null : hrOrgParentId.trim();
    }

    public String getHrOrgRank() {
        return hrOrgRank;
    }

    public void setHrOrgRank(String hrOrgRank) {
        this.hrOrgRank = hrOrgRank == null ? null : hrOrgRank.trim();
    }

    public String getHrOrg1() {
        return hrOrg1;
    }

    public void setHrOrg1(String hrOrg1) {
        this.hrOrg1 = hrOrg1 == null ? null : hrOrg1.trim();
    }

    public String getHrOrg2() {
        return hrOrg2;
    }

    public void setHrOrg2(String hrOrg2) {
        this.hrOrg2 = hrOrg2 == null ? null : hrOrg2.trim();
    }

    public String getHrOrg3() {
        return hrOrg3;
    }

    public void setHrOrg3(String hrOrg3) {
        this.hrOrg3 = hrOrg3 == null ? null : hrOrg3.trim();
    }

    public String getHrOrg4() {
        return hrOrg4;
    }

    public void setHrOrg4(String hrOrg4) {
        this.hrOrg4 = hrOrg4 == null ? null : hrOrg4.trim();
    }

    public String getHrOrg5() {
        return hrOrg5;
    }

    public void setHrOrg5(String hrOrg5) {
        this.hrOrg5 = hrOrg5 == null ? null : hrOrg5.trim();
    }

    public String getHrOrgTimecreated() {
        return hrOrgTimecreated;
    }

    public void setHrOrgTimecreated(String hrOrgTimecreated) {
        this.hrOrgTimecreated = hrOrgTimecreated == null ? null : hrOrgTimecreated.trim();
    }

    public String getHrOrgTimemodified() {
        return hrOrgTimemodified;
    }

    public void setHrOrgTimemodified(String hrOrgTimemodified) {
        this.hrOrgTimemodified = hrOrgTimemodified == null ? null : hrOrgTimemodified.trim();
    }

    public String getHrPositionPosId() {
        return hrPositionPosId;
    }

    public void setHrPositionPosId(String hrPositionPosId) {
        this.hrPositionPosId = hrPositionPosId == null ? null : hrPositionPosId.trim();
    }

    public String getHrPositionName() {
        return hrPositionName;
    }

    public void setHrPositionName(String hrPositionName) {
        this.hrPositionName = hrPositionName == null ? null : hrPositionName.trim();
    }

    public String getHrPositionParentId() {
        return hrPositionParentId;
    }

    public void setHrPositionParentId(String hrPositionParentId) {
        this.hrPositionParentId = hrPositionParentId == null ? null : hrPositionParentId.trim();
    }

    public String getPosJobtypeNameP() {
        return posJobtypeNameP;
    }

    public void setPosJobtypeNameP(String posJobtypeNameP) {
        this.posJobtypeNameP = posJobtypeNameP == null ? null : posJobtypeNameP.trim();
    }

    public String getPosJobtypeNamePp() {
        return posJobtypeNamePp;
    }

    public void setPosJobtypeNamePp(String posJobtypeNamePp) {
        this.posJobtypeNamePp = posJobtypeNamePp == null ? null : posJobtypeNamePp.trim();
    }

    public String getPosJobtypeName() {
        return posJobtypeName;
    }

    public void setPosJobtypeName(String posJobtypeName) {
        this.posJobtypeName = posJobtypeName == null ? null : posJobtypeName.trim();
    }

    public String getPosJoblevel() {
        return posJoblevel;
    }

    public void setPosJoblevel(String posJoblevel) {
        this.posJoblevel = posJoblevel == null ? null : posJoblevel.trim();
    }

    public String getPosAssignTimecreated() {
        return posAssignTimecreated;
    }

    public void setPosAssignTimecreated(String posAssignTimecreated) {
        this.posAssignTimecreated = posAssignTimecreated == null ? null : posAssignTimecreated.trim();
    }

    public String getPosJob() {
        return posJob;
    }

    public void setPosJob(String posJob) {
        this.posJob = posJob == null ? null : posJob.trim();
    }

    public String getDescr1006() {
        return descr1006;
    }

    public void setDescr1006(String descr1006) {
        this.descr1006 = descr1006 == null ? null : descr1006.trim();
    }

    public String getCtj021Codeid() {
        return ctj021Codeid;
    }

    public void setCtj021Codeid(String ctj021Codeid) {
        this.ctj021Codeid = ctj021Codeid == null ? null : ctj021Codeid.trim();
    }

    public String getCth148Codename() {
        return cth148Codename;
    }

    public void setCth148Codename(String cth148Codename) {
        this.cth148Codename = cth148Codename == null ? null : cth148Codename.trim();
    }

    public String getHrPersonTel() {
        return hrPersonTel;
    }

    public void setHrPersonTel(String hrPersonTel) {
        this.hrPersonTel = hrPersonTel == null ? null : hrPersonTel.trim();
    }

    public String getCth148Codeid() {
        return cth148Codeid;
    }

    public void setCth148Codeid(String cth148Codeid) {
        this.cth148Codeid = cth148Codeid == null ? null : cth148Codeid.trim();
    }
}