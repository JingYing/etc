package com.jd.oa.system.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;


/**
 * @author qiuyang
 *
 */
public interface PsService extends BaseService<Object, Long> {
	
	/**
	 * 获得所有的组织机构信息
	 * @return
	 */
	public List<SysOrganization> getAllOrganizations();
	
	/**
	 * 获取所有的岗位信息
	 * @return
	 */
	public List<SysPosition> getAllPositions();
	
	/**
	 * 获取所有的用户信息
	 * @return
	 */
	public List<SysUser> getAllUsers();
	
	/**
	 * 根据sql查询信息
	 * 
	 * @param sql
	 * @return
	 */
	public Map<String, Object> getList(String sql);
}
