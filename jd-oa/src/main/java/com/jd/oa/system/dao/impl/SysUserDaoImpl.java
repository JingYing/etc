package com.jd.oa.system.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysUserDao;
import com.jd.oa.system.model.SysUser;


@Component("sysUserDao")

@Transactional
public class SysUserDaoImpl extends MyBatisDaoImpl<SysUser, String> implements SysUserDao{

	@Override
	public SysUser getByUserName(String userName, String organizationType) {
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("userName", userName);
		parameters.put("organizationType", organizationType);
		List<SysUser> sysUsers = getSqlSession().selectList(sqlMapNamespace +".getByUserName",parameters);
		SysUser sysUser = null;
		if(sysUsers!=null && sysUsers.size()>0)
			sysUser = sysUsers.get(0);
		return sysUser;
	}
	
	@Override
	public SysUser getByEmailAndMobile(String email, String mobile, String organizationType){
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("email", email);
		parameters.put("mobile", mobile);
		parameters.put("organizationType", organizationType);
		List<SysUser> sysUsers = getSqlSession().selectList(sqlMapNamespace +".getByEmailAndMobile",parameters);
		SysUser sysUser = null;
		if(sysUsers!=null && sysUsers.size()>0)
			sysUser = sysUsers.get(0);
		return sysUser;
	}	
	
	@Override
	public SysUser getUserMailAndPhoneByUserName(String userName, String... organizationType) {
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("userName", userName);
		if(organizationType!=null)
			parameters.put("organizationType", organizationType[0]);
		List<SysUser> user = getSqlSession().selectList(sqlMapNamespace +".getUserMailAndPhoneByUserName",parameters);
		if(user!=null && user.size() > 0) return user.get(0);
		return null;
	}

	@Override
	public List<SysUser> findNotInPs() {
		return getSqlSession().selectList(sqlMapNamespace + ".findNotInPs");
	}

	@Override
	public void updateStatus(String id, String status) {
		Map map = new HashMap();
		map.put("id", id);
		map.put("status", status);
		getSqlSession().update(sqlMapNamespace + ".updateStatus", map);
	}
}
