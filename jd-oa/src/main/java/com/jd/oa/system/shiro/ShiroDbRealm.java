package com.jd.oa.system.shiro;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.oa.common.shiro.ShiroUser;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPrivilege;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysPrivilegeService;
import com.jd.oa.system.service.SysRoleService;
import com.jd.oa.system.service.SysUserService;

public class ShiroDbRealm extends AuthorizingRealm {

	private static final Logger logger = LoggerFactory
			.getLogger(ShiroDbRealm.class);

	@Autowired
	private SysOrganizationService sysOrganizationService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysPrivilegeService sysPrivilegeService;

	public ShiroDbRealm() {
		// setCredentialsMatcher(new HashedCredentialsMatcher("SHA-1"));
		setCredentialsMatcher(new SimpleCredentialsMatcher());
	}

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken authcToken) {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		SysUser user;
		user = (SysUser) sysUserService.getByUserName(token.getUsername());
		if (user != null) {
			clearCachedAuthorizationInfo(user.getUserCode());
			SysOrganization organization = sysOrganizationService.get(user
					.getOrganizationId());
			return new SimpleAuthenticationInfo(new ShiroUser(
					user.getUserName(), user.getRealName(),
					organization != null ? organization.getOrganizationFullname()
							: ""), user.getUserName(), getName());
		} else {
			return null;
		}
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(
			PrincipalCollection principals) {
		String userName = ((ShiroUser) (principals.fromRealm(getName())
				.iterator().next())).getLoginName();
		SysUser user = (SysUser) sysUserService.getByUserName(userName);
		if (user != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			Set<String> privilegeSet = new HashSet<String>();
			for (SysRole role : sysRoleService.findByUserId(user.getId())) {
				for (SysPrivilege privilege : sysPrivilegeService
						.findByRoleId(role.getId())) {
					for (String privilegeStr : privilege.getUrl().split(",")) {
						privilegeSet.add(StringUtils.trim(privilegeStr));
					}
				}
			}
			info.setStringPermissions(privilegeSet);
			return info;
		} else {
			return null;
		}
	}

	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(String principal) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection(
				principal, getName());
		clearCachedAuthorizationInfo(principals);
	}
}
