/**
 * 
 */
package com.jd.oa.system.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysRoleDao;
import com.jd.oa.system.dao.SysRolePrivilegeDao;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysRolePrivilege;

/**
 * @author liub
 *
 */
@Component("sysRolePrivilegeDao")
public class SysRolePrivilegeDaoImpl extends MyBatisDaoImpl<SysRolePrivilege, String> implements SysRolePrivilegeDao {
}