package com.jd.oa.system.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.PsPositionDao;
import com.jd.oa.system.model.PsPosition;

@Component("psPositionDao")
public class PsPositionDaoImpl extends MyBatisDaoImpl<PsPosition, String> implements PsPositionDao{

	private static final String NAMESPACE = PsPosition.class.getName();
	
	@Override
	public void truncate() {
		getSqlSession().update(NAMESPACE + ".truncate");
	}

	@Override
	public void add(PsPosition psPosition) {
		getSqlSession().insert(NAMESPACE + ".add", psPosition);
		
	}

	@Override
	public void add(List<PsPosition> psPositions) {
		for(PsPosition p : psPositions)	{
			this.add(p);
		}
	}

	@Override
	public List<PsPosition> findByEffStatus(String effStatus) {
		return getSqlSession().selectList(NAMESPACE + ".findByEffStatus", effStatus);
	}

}
