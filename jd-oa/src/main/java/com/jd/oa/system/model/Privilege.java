package com.jd.oa.system.model;

import com.jd.oa.common.model.IdModel;

/**
 * 权限
 * 
 * @author zhouhq
 */
public class Privilege extends IdModel {

	private static final long serialVersionUID = -1946243071566237401L;

	public static final String ENTITY_NAME = "权限";
	
	/**
	 * 权限编码
	 */
	private String code;
	/**
	 * 权限名称
	 */
	private String name;
	/**
	 * 权限URL
	 */
	private String url;
	
	/**
	 * 权限对应的资源
	 */
	private Resource resource;
	/**
	 * 权限对应的操作
	 */
	private Operation operation;
	
	/* [CONSTRUCTOR MARKER BEGIN] */
	public Privilege() {

	}

	/* [CONSTRUCTOR MARKER END] */

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}
}
