package com.jd.oa.system.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysPrivilege;

/**
 * @author liub
 * 
 */
public interface SysPrivilegeService extends BaseService<SysPrivilege, String> {
	/**
	 * 根据角色ID获取用户的关联角色列表信息
	 * 
	 * @param userId
	 *            用户ID
	 * 
	 * @author zhouhuaqi
	 * @date 2013-09-02 17:49:00
	 * @version V1.0
	 */
	public List<SysPrivilege> findByRoleId(String roleId);
	
	/**
	 * 根据用户ID获取用户关联的权限信息
	 * @param userId 用户Id，主键ID
	 * 
	 * @author zhouhuaqi
	 * @date 2013-10-08 15:17:00
	 * @version V1.0
	 */
	public List<SysPrivilege> findByUserId(String userId);
	
	/**
	 * 根据用户ID获取用户分配的权限数据列表
	 * @param userId 用户Id，主键ID
	 * @return
	 */
	public List<String> findAssignedPrivileges(String userId);
}
