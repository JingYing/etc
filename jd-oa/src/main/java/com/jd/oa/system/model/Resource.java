package com.jd.oa.system.model;

import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.jd.oa.common.model.IdModel;

/**
 * 资源
 * 
 * @author zhouhq
 */
public class Resource extends IdModel{

	private static final long serialVersionUID = -1311035009393278678L;
	
	public static final String ENTITY_NAME = "应用资源";
	
	/**
	 * 资源级别
	 */
	private int layer;
	/**
	 * 资源类型
	 */
	private int type;
	/**
	 * 资源编码
	 */
	private String code;
	/**
	 * 资源名称
	 */
	private String name;
	/**
	 * 资源URL
	 */
	private String url;
	/**
	 * 资源描述
	 */
	private String description;
	/**
	 * 排序序号
	 */
	private int orderNo;
	
	/**
	 * 隶属的应用
	 */
	private Application application;
	
	/**
	 * 父资源
	 */
	private Resource parent = null;
	/**
	 * 子资源
	 */
	private Set<Resource> children;
	
	/**
	 * 包含权限
	 */
	private Set<Privilege> privileges;
	
	
	public Resource() {
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}
	
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Resource getParent() {
		return parent;
	}

	public void setParent(Resource parent) {
		this.parent = parent;
	}

	public Set<Resource> getChildren() {
		return children;
	}

	public void setChildren(Set<Resource> children) {
		this.children = children;
	}

	public Set<Privilege> getPrivileges() {
		return privileges;
	}
	
	public void setPrivileges(Set<Privilege> privileges) {
		this.privileges = privileges;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
		.append("id", getId())
		.append("code", getCode())
        .append("name", getName())
        .append("url", getUrl())
        .toString();
	}
}
