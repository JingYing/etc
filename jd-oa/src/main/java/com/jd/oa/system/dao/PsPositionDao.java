package com.jd.oa.system.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.PsPosition;

public interface PsPositionDao extends BaseDao<PsPosition,String>{
	
	void truncate();

	void add(PsPosition psPosition);
	
	void add(List<PsPosition> psPositions);

	List<PsPosition> findByEffStatus(String effStatus);
	
}
