package com.jd.oa.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.oa.system.service.SysUserService;

@Controller
@RequestMapping(value = "/system")
public class RemoveRepeatSysUserData {
	private static final Logger logger = Logger.getLogger(RemoveRepeatSysUserData.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private SysUserService sysUserService;
	
	@RequestMapping(value = "/removeRepeatSysUserData", method = RequestMethod.GET)
	@ResponseBody
	private String run(){
		logger.debug("执行删除USER表数据重复》》》》》》》》》》开始");
		List<Map<String, Object>> repeateList = jdbcTemplate.queryForList("select USER_CODE,USER_NAME from T_JDOA_SYS_USER group by user_code having count(*) > 1");
		logger.debug("总共检索出[" + repeateList.size() + "]条重复数据");
		for(int i = 0 ; i < repeateList.size() ; i++){
			Map<String,Object> repeateMap = repeateList.get(i);
			if(repeateMap != null){
				logger.debug("检 查 USER_CODE[" + repeateMap.get("USER_CODE") + "],USER_NAME[" + repeateMap.get("USER_NAME") + "]用户数据重复性!");
				List<Map<String, Object>> userList = jdbcTemplate.queryForList("select ID,USER_NAME,USER_CODE from T_JDOA_SYS_USER where USER_CODE='" + repeateMap.get("USER_CODE") + "'");
				logger.debug("检索出 USER_CODE[" + repeateMap.get("USER_CODE") + ",USER_NAME[" + repeateMap.get("USER_NAME") + "]有[" + userList.size() + "]条重复数据");
				for(int j = 0 ; j < userList.size() ; j++){
					//针对ID判断是否有引用的，如果有引用的，则保留，反之删除
					Map<String, Object> userMap = userList.get(j);
					if(userMap != null){
						String id = String.valueOf(userMap.get("ID"));
						logger.debug("检查 ID[" + id + "],USER_CODE[" + repeateMap.get("USER_CODE") + ",USER_NAME[" + repeateMap.get("USER_NAME") + "]");
						if(checkFor_T_JDOA_SYS_USER_ROLE(id)
						   && checkFor_T_JDOA_SYS_AUTH_EXPRESSION(id)
						   && checkFor_T_JDOA_PROCESS_TASK_HISTORY(id)
						   && checkFor_T_JDOA_PROCESS_PROXY_TASK(id)
						   && checkFor_T_JDOA_PROCESS_PROXY(id)
						   && checkFor_T_JDOA_PROCESS_INSTANCEK(id)
						   && checkFor_T_JDOA_PROCESS_ADDSIGNER(id)
						   && checkFor_T_JDOA_FORM(id)
						   && checkFor_T_JDOA_PROCESS_DEFINITION(id)
						   && checkFor_T_JDOA_PROCESS_SUPERVISE(id)
								){
							//删除操作
							sysUserService.delete(id, true);
//							jdbcTemplate.execute("delete from T_JDOA_SYS_USER where id='" + id + "'");
							logger.debug("已删除ID[" + id + "],USER_CODE[" + repeateMap.get("USER_CODE") + ",USER_NAME[" + repeateMap.get("USER_NAME") + "]用户数据");
						}
					}
				}
			}
		}
		
		logger.debug("执行删除USER表数据重复》》》》》》》》》》结束");
		return "执行删除USER表数据重复》》》》》》》》》》结束";
	}
	private boolean checkFor_T_JDOA_SYS_USER_ROLE(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_SYS_USER_ROLE where USER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_SYS_USER_ROLE]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_SYS_AUTH_EXPRESSION(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_SYS_AUTH_EXPRESSION where USER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_SYS_AUTH_EXPRESSION]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_PROCESS_TASK_HISTORY(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_PROCESS_TASK_HISTORY where USER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_TASK_HISTORY]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_PROCESS_PROXY_TASK(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_PROCESS_PROXY_TASK where PROXY_USER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_PROXY_TASK]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_PROCESS_PROXY(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_PROCESS_PROXY where PROXY_ID='" + id + "' or ASSIGNER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_PROXY]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_PROCESS_INSTANCEK(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_PROCESS_INSTANCE where STARTER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_INSTANCEK]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_PROCESS_ADDSIGNER(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_PROCESS_ADDSIGNER where USER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_ADDSIGNER]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_FORM(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_FORM where OWNER='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_ADDSIGNER]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_PROCESS_DEFINITION(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_PROCESS_DEFINITION where OWNER='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_ADDSIGNER]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
	private boolean checkFor_T_JDOA_PROCESS_SUPERVISE(String id){
		int count = jdbcTemplate.queryForInt("select count(*) from T_JDOA_PROCESS_SUPERVISE where SUPERVISE_USER_ID='" + id + "'");
		logger.debug("检查[T_JDOA_PROCESS_ADDSIGNER]数据表，");
		if(count > 0){
			return false;
		}
		return true;
	}
}
