package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysAuthExpressionDao;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;


@Service("sysAuthExpressionService")
@Transactional
public class SysAuthExpressionServiceImpl extends BaseServiceImpl<SysAuthExpression, String> implements SysAuthExpressionService{
	@Autowired
	private SysAuthExpressionDao sysAuthExpressionDao;
	
    @Autowired
    private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
    
	public SysAuthExpressionDao getDao(){
		return sysAuthExpressionDao;
	}

	//删除未引用的权限表达式
	@Override
	public List<SysAuthExpression> deleteOnlyUnused(String ql, Object... values) {
		List<SysAuthExpression> listUnDelete = new ArrayList<SysAuthExpression>();
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("expressionid", values[i]);
				
				
				List<SysBussinessAuthExpression> list = sysBussinessAuthExpressionService.find("findByExpressionId",map);
				if(list==null || list.size() <= 0){
					delete(ql,new Object[]{values[i]});
				}else{
					listUnDelete.add(get(values[i].toString()));
				}
					
			}
		}
		return listUnDelete;
	}
	
	
	
	//根据授权表达式ID获取所有其详细的记录
	@Override
	public List<SysAuthExpression> findAuthExpressionListByIds(String... values) {
		List<SysAuthExpression> list = new ArrayList<SysAuthExpression>();
		for(int i=0;i<values.length;i++){
			SysAuthExpression sysAuthExpression = get(String.valueOf(values[i]));
			if(sysAuthExpression!=null)
				list.add(sysAuthExpression);
		}
		return list;

	}

	//根据授权表达式对象获取所有其详细的记录
	@Override
	public List<SysAuthExpression> findAuthExpressionListByIds(SysAuthExpression... values){
		List<SysAuthExpression> list = new ArrayList<SysAuthExpression>();
		for(int i=0;i<values.length;i++)
			list.add(get(values[i].getId()));
		return list;
	}
	
	/**
	 * 
	 * @desc 获得指定业务数据对应的表达式列表
	 * @author WXJ
	 * @date 2013-9-25 上午11:29:20
	 *
	 * @param businessType
	 * @param businessId
	 * @return
	 */
	@Override
	public List<SysAuthExpression> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId){
		Map<String, String> map = new HashMap<String, String>();
		map.put("businessType", businessType);
		map.put("businessId", businessId);
		List<SysAuthExpression> list = sysAuthExpressionDao.find("findExpressionByBusinessIdAndBusinessType",map);

		return list;
	}
	
}
