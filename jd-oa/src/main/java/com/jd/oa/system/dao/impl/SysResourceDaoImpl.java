package com.jd.oa.system.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysResourceDao;
import com.jd.oa.system.model.SysResource;

/**
 * 
 * @author xulin
 *
 */
@Component("sysResourceDao")
public class SysResourceDaoImpl extends MyBatisDaoImpl<SysResource, String>
		implements SysResourceDao {
	
	 /**
     * 根据资源编码或名称获取资源
     * @param sysResource 文件夹对象
     * @return  Integer
     */
    public Integer findCountByRes(SysResource sysResource){
    	return this.getSqlSession().selectOne("com.jd.oa.system.model.SysResource.findCountByRes", sysResource);
    }
    
    @Override
	public List<SysResource> findByUserId(String userId) {
		return getSqlSession().selectList(sqlMapNamespace +".findByUserId",userId);
	}
}
