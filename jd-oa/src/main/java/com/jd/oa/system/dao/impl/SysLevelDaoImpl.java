package com.jd.oa.system.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysLevelDao;
import com.jd.oa.system.model.SysLevel;

@Component("sysLevelDao")
public class SysLevelDaoImpl  extends MyBatisDaoImpl<SysLevel, String> implements SysLevelDao{
	/**
	 * 
	 * @desc 获得1级职级
	 * @author WXJ
	 * @date 2013-9-3 下午05:02:51
	 *
	 * @return
	 */
	@Override
	public List<SysLevel> findLevel1List() {
		// TODO Auto-generated method stub
		return this.getSqlSession().selectList("com.jd.oa.system.model.SysLevel.findLevel1List");
	}
	
	/**
	 * 
	 * @desc 获得2级职级
	 * @author WXJ
	 * @date 2013-9-3 下午05:02:51
	 *
	 * @return
	 */
	@Override
	public List<SysLevel> findLevel2List() {
		// TODO Auto-generated method stub
		return this.getSqlSession().selectList("com.jd.oa.system.model.SysLevel.findLevel2List");
	}
	
}
