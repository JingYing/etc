package com.jd.oa.system.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;

public interface SysPositionService extends BaseService<SysPosition, String> {
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 下午07:57:10
	 *
	 * @param mp
	 * @return
	 */
	public List<SysPosition> selectPosByParentId(Map mp);
	
	/**
	 * 定时任务：传输EHR岗位信息数据到OA
	 */
	public void transferPositionInfoFromHR(List<SysPosition> allPositions);

	/**
	 * 根据源职位, 更新数据库中已有职位
	 * @param positions
	 */
	void insertOrUpdate(List<SysPosition> positions);
}
