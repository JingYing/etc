package com.jd.oa.system.dao;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysPosition;

public interface SysPositionDao extends BaseDao<SysPosition, String> {
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 下午07:53:14
	 *
	 * @param mp
	 * @return
	 */
	public List<SysPosition> selectPosByParentId(Map mp);
	
	
	/**
	 * 查询在peoplesoft中存在, 在OA中不存在的记录
	 * @return
	 */
	public List<SysPosition> findNotInPs();
}
