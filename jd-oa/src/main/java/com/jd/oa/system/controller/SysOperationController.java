/**
 * 
 */
package com.jd.oa.system.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.system.model.SysOperation;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.service.SysOperationService;

/**
 *@Description: 系统功能 Controller
 *@author liub
 *
 */
@Controller
@RequestMapping(value="/system")
public class SysOperationController {
	
	 private static final Logger logger = Logger.getLogger(SysOperationController.class);
	
	 @Autowired
	 private SysOperationService sysOperationService;
	 
	 @RequestMapping(value = "/sysOperation_index", method = RequestMethod.GET)
	 public String index() throws Exception {
	     return "system/sysOperation_index";
	 }
	 @RequestMapping(value = "/sysOperation_list")
	 @ResponseBody
	 public Object list(HttpServletRequest request,@RequestParam String  operationName,@RequestParam String  operationCode) throws Exception {
		 PageWrapper<SysOperation> pageWrapper = new PageWrapper<SysOperation>(request);
         if (null != operationName && !operationName.equals("")) {
             pageWrapper.addSearch("operationName", operationName);
         }
         if (null != operationCode && !operationCode.equals("")) {
             pageWrapper.addSearch("operationCode", operationCode);
         }
		 sysOperationService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		 pageWrapper.addResult("returnKey","returnValue");
		 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	     logger.debug("result:"+json);
	     return json;
	 }
	 /**
	  * add view
	  * @return
	  * @throws Exception
	  */
	 @RequestMapping(value = "/sysOperation_add", method = RequestMethod.GET)
	 public String list() throws Exception {
	     return "system/sysOperation_add";
	 }

    /**
     *
     * @param entity
     * @return
     */
	 @RequestMapping(value="/sysOperation_save" , method = RequestMethod.POST)
	 @ResponseBody
	 public String save(SysOperation entity){
	    Map<String,Object> map = new HashMap<String,Object>();
	 	if(entity.getId() != null && entity.getId().trim().length() > 0){
	 		SysOperation sysOperation = sysOperationService.get(entity.getId());
	 		try {
				ComUtils.copyProperties(entity, sysOperation);
			} catch (Exception e) {
				throw new BusinessException("对象模板复制失败：" + e.getMessage(),e);
			}
	 		sysOperationService.update(sysOperation);
	 	} else {
	 		sysOperationService.insert(entity);
	 	}
	 	map.put("operator", true);
	 	map.put("message", "添加成功");
	 	return JSONObject.fromObject(map).toString();
	 }
	 /**
	  * delete 
	  * @param id entity id
	  * @return
	  */
	 @RequestMapping(value="/sysOperation_delete" , method = RequestMethod.POST)
	 @ResponseBody
	 public String delete(String id){
		 Map<String,Object> map = new HashMap<String,Object>();
		 SysOperation sysOperation = sysOperationService.get(id);
		 if(sysOperation != null){
			 sysOperationService.delete(sysOperation,true);
		 }
		 map.put("operator", true);
		 map.put("message", "添加成功");
		 return JSONObject.fromObject(map).toString();
	 }
	 
	 /**
	  * edit 
	  * @param id entity id
	  * @return
	  */
	 @RequestMapping(value="/sysOperation_update" , method = RequestMethod.GET)
	 public ModelAndView get(String id){
		 ModelAndView mv = new ModelAndView();
		 SysOperation operation = sysOperationService.get(id);
		 mv.setViewName("system/sysOperation_edit");
		 mv.addObject("operation", operation);
		 return mv;
	 }
}
