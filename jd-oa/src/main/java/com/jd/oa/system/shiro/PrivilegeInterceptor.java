package com.jd.oa.system.shiro;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jd.common.web.DotnetAuthenticationTicket;
import com.jd.common.web.LoginContext;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.system.service.SysUserService;

/**
 * 拦截方法执行时的注释。以确定是否有权限执行。 <br/>
 * 做的Logincontext认证<br/>
 * User: zhouhuaqi@jd.com<br/>
 * Date: 2013-9-9<br/>
 * Time: 14:32:00<br/>
 * 
 * @author zhouhuaqi
 */
public class PrivilegeInterceptor extends HandlerInterceptorAdapter {

	private PrivilegeHelper privilegeHelper;
	
	@Autowired
	private SysUserService sysUserService;
	
	@Override
	public final boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) {

		String url = request.getRequestURI().substring(
				request.getContextPath().length());

		if (StringUtils.isNotEmpty(url)) {
			String userId = sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin());
			if (userId == null
					|| !privilegeHelper.hasPrivilege(userId, url)) {
				throw new BusinessException("没有URL访问权限:"+url);
			}
		}
		return true;
	}

	/**
	 * 取得用户名
	 * 
	 * @return
	 */
	protected String getUsername() {
		String username = getDotnetTicketUsername();
		if (username == null) {
			LoginContext context = LoginContext.getLoginContext();
			if (context != null && StringUtils.isNotBlank(context.getPin())) {
				username = context.getPin();
			}
		}
		return username;
	}

	protected String getDotnetTicketUsername() {
		DotnetAuthenticationTicket ticket = DotnetAuthenticationTicket
				.getTicket();
		if (ticket != null && StringUtils.isNotBlank(ticket.getUsername())) {
			return ticket.getUsername();
		} else {
			return null;
		}
	}

	public void setPrivilegeHelper(PrivilegeHelper privilegeHelper) {
		this.privilegeHelper = privilegeHelper;
	}
}