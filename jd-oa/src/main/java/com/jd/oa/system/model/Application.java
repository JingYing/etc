package com.jd.oa.system.model;

import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.jd.oa.common.model.IdModel;

/**
 * 应用系统
 * 
 * @author zhouhq
 */
public class Application extends IdModel {

	private static final long serialVersionUID = 5979431447592526937L;
	
	public static final String ENTITY_NAME = "应用系统";

	/**
	 * 应用编码
	 */
	private String code;
	/**
	 * 应用名称
	 */
	private String name;
	/**
	 * 应用类型
	 */
	private String type;
	/**
	 * 是否开通
	 */
	private String isOpen;
	/**
	 * 开通日期
	 */
	private Date openDate;
	/**
	 * 访问地址
	 */
	private String url;
	/**
	 * 应用描述
	 */
	private String description;
	/**
	 * IP地址
	 */
	private String ipAddress;
	/**
	 * 端口
	 */
	private String ipPort;
	
	/**
	 * 应用下的资源列表
	 */
	private Set<Resource> resources;
	
	public Application() {
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIpPort() {
		return ipPort;
	}

	public void setIpPort(String ipPort) {
		this.ipPort = ipPort;
	}
	
	public Set<Resource> getResources() {
		return resources;
	}

	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
		.append("id", getId())
		.append("code", getCode())
        .append("name", getName())
        .append("url", getUrl())
        .toString();
	}
}
