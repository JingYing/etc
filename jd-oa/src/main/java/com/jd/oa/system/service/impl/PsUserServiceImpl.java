package com.jd.oa.system.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.system.dao.PsUserDao;
import com.jd.oa.system.dao.SysOrganizationDao;
import com.jd.oa.system.dao.SysUserDao;
import com.jd.oa.system.model.PsUser;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.PsUserService;
import com.jd.oa.system.service.SysUserService;

@Component("psUserService")
public class PsUserServiceImpl implements PsUserService{
	
	@Resource
	private SysUserService sysUserService;
	@Resource
	private PsUserDao psUserDao;
	@Resource
	private SysUserDao sysUserDao;
	@Resource
	private SysOrganizationDao sysOrgDao;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * 不更新的账号
	 */
	private String extraAccount = "lczxgly";
	
	@Override
	public void insertOrUpdateSysUser()	{
		List<SysOrganization> allOrganization = sysOrgDao.findByOrganizationType(SystemConstant.ORGNAIZATION_TYPE_HR);
		Map<String,String> orgcode_orgid = new HashMap<String, String>();	//orgCode = orgId
		for(SysOrganization org : allOrganization){
			orgcode_orgid.put(org.getOrganizationCode(), org.getId());
		}
		
		List<PsUser> psUsers = psUserDao.findValidUser();
		for(PsUser p : psUsers)	{
			if(extraAccount.equalsIgnoreCase(p.getHrPersonErpid()))	{
				continue;	//不处理特殊账号
			}
			
			SysUser exist = findSysUser(p.getHrPersonPersonId());
			if(exist == null)	{		//新增
				SysUser sys = new SysUser();
				try {
					sys.setAge(Integer.parseInt(p.getA04A0401()));
				} catch (NumberFormatException e) {		}
				try {
					sys.setBirthday(dateFormat.parse(p.getHrPersonBirthday()));
				} catch (ParseException e) {		}
				sys.setCodeid(p.getCtj021Codeid());
				sys.setCodename(p.getCth148Codename());
				sys.setCreateTime(new Date());
				sys.setCreator("ps同步");
				sys.setEmail(p.getHrPersonQyyx());
				sys.setId(IdUtils.uuid2());
				if(p.getDescr1006() == null)	{
					sys.setLevelCode(p.getPosJoblevel());	
				} else	{
					sys.setLevelCode(p.getPosJoblevel() + "-" + p.getDescr1006());
				}
				sys.setMobile(p.getHrPersonGrlxdht());
//				sys.setModifier();	//不存
//				sys.setModifyTime();	//不存
//				sys.setOrganizationFullname(organizationFullname);	//无此字段
				sys.setOrganizationId(orgcode_orgid.get(p.getHrOrgOrgId()));
//				sys.setOrganizationName(organizationName);	//无此字段
				sys.setPositionCode(p.getHrPositionPosId());
//				sys.setPositionName(positionName);	//无此字段
				sys.setPositionParentCode(p.getHrPositionParentId());
				sys.setRealName(p.getHrPersonPersonname());
				sys.setSex(toSex(p.getHrPersonSex()));	
				sys.setStatus("0");
				sys.setUserCode(p.getHrPersonPersonId());
				sys.setUserName(p.getHrPersonErpid());
				sysUserDao.insert(sys);
			} else	{		//更新
				try {
					exist.setAge(Integer.parseInt(p.getA04A0401()));
				} catch (NumberFormatException e) {		}
				try {
					exist.setBirthday(dateFormat.parse(p.getHrPersonBirthday()));
				} catch (Exception e) {		}
				exist.setCodeid(p.getCtj021Codeid());
				exist.setCodename(p.getCth148Codename());
//				exist.setCreateTime();	//不处理
//				exist.setCreator();		//不处理
				exist.setEmail(p.getHrPersonQyyx());
//				exist.setId(IdUtils.uuid2());	//不处理
				if(p.getDescr1006() == null)	{
					exist.setLevelCode(p.getPosJoblevel());	
				} else	{
					exist.setLevelCode(p.getPosJoblevel() + "-" + p.getDescr1006());
				}
				exist.setMobile(p.getHrPersonGrlxdht());
				exist.setModifier("ps同步");	
				exist.setModifyTime(new Date());	
//				exist.setOrganizationFullname(organizationFullname);	//无此字段
				exist.setOrganizationId(orgcode_orgid.get(p.getHrOrgOrgId()));
//				exist.setOrganizationName(organizationName);	//无此字段
				exist.setPositionCode(p.getHrPositionPosId());
//				exist.setPositionName(positionName);	//无此字段
				exist.setPositionParentCode(p.getHrPositionParentId());
				exist.setRealName(p.getHrPersonPersonname());
				exist.setSex(toSex(p.getHrPersonSex()));
				exist.setStatus("0");
				exist.setUserCode(p.getHrPersonPersonId());
				exist.setUserName(p.getHrPersonErpid());
				sysUserDao.update(exist);
			}
		}
		psUsers.clear();
	}
	
	private String toSex(String psSex)	{
		if("M".equalsIgnoreCase(psSex))	{
			return "男";
		} else if("F".equalsIgnoreCase(psSex))	{
			return "女";
		} else	{
			return "未知";
		}
	}
	
	@Override
	public void deleteSysUser()	{
		int psUserCount = psUserDao.findAllCount();
		if(psUserCount > 0)	{		//ps_user无数据时, 表示数据可能有问题, 不delete
			List<SysUser> delUsers = sysUserDao.findNotInPs();
			for(SysUser s : delUsers)	{
				if(!extraAccount.equalsIgnoreCase(s.getUserName()))	{
					sysUserDao.updateStatus(s.getId(), "1");	//数量较大,不使用对象更新方式
				}
			}
			delUsers.clear();
		}
	}
	
	private SysUser findSysUser(String userCode)	{
		List<SysUser> list = sysUserDao.find("findByUsercode", userCode);
		return list.isEmpty() ? null : list.get(0);
	}
}
