package com.jd.oa.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.ApplicationDao;
import com.jd.oa.system.model.Application;
import com.jd.oa.system.service.ApplicationService;

//Spring Service Bean的标识.
@Service("applicationService")
//默认将类中的所有函数纳入事务管理.
@Transactional
public class ApplicationServiceImpl extends BaseServiceImpl<Application, Long>
		implements ApplicationService {
	
	@Autowired
	private ApplicationDao applicationDao;

	public ApplicationDao getDao() {
		return applicationDao;
	}
}
