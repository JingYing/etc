/**
 * 
 */
package com.jd.oa.system.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysRolePrivilege;

/**
 * @author liub
 *
 */
public interface SysRolePrivilegeDao extends BaseDao<SysRolePrivilege, String>{
	
}
