
package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jd.oa.common.redis.annotation.CacheKey;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.redis.annotation.CachePut;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysPrivilegeDao;
import com.jd.oa.system.model.SysPrivilege;
import com.jd.oa.system.service.SysPrivilegeService;

/**
 * @author liub
 *
 */
@Component
@CacheProxy
public class SysPrivilegeServiceImpl extends BaseServiceImpl<SysPrivilege, String> implements SysPrivilegeService {
	
	@Autowired(required=true)
	private SysPrivilegeDao sysPrivilegeDao;

	public SysPrivilegeDao getDao() {
		return sysPrivilegeDao;
	}

	@Override
	public List<SysPrivilege> findByRoleId(String roleId) {
		return getDao().findByRoleId(roleId);
	}

	@Override
	public List<SysPrivilege> findByUserId(String userId) {
		List<SysPrivilege> privileges = getDao().findByUserId(userId);
		// 如果用户没有在系统中指定具体角色，则默认该用户是普通用户（Normal User），获取该角色的权限数据
		if(privileges == null || privileges.size() == 0)
			privileges = getDao().findByRoleId("ROLE_000003");
		return privileges;
	}
	
	@CachePut(key = @CacheKey(template = "JDOA/AssignedPrivileges/${p0}"), expire = 43200)
	public List<String> findAssignedPrivileges(String userId) {
		Set<String> privilegeSet = new HashSet<String>();
		for (SysPrivilege privilege : findByUserId(userId)) {
			for (String privilegeStr : privilege.getUrl().split(",")) {
				privilegeSet.add(StringUtils.trim(privilegeStr));
			}
		}
        return new ArrayList<String>(privilegeSet);
	}
}