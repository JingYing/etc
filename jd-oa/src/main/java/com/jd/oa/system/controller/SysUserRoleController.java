/**
 * 
 */
package com.jd.oa.system.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysRoleService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.system.model.SysUserRole;
import com.jd.oa.system.service.SysUserRoleService;

/**
 * @author yujiahe
 * @date 2013年8月30日
 * @Description: 给用户分配角色
 * 
 */
@Controller
@RequestMapping(value = "/system")
public class SysUserRoleController {
	private static final Logger logger = Logger
			.getLogger(SysRoleController.class);
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysOrganizationService sysOrganizationService;
	
	@Autowired
	private UserAuthorizationService userAuthorizationService ;
	
	/**
	 * @Description: 进入到查询分配角色主页面
	 * @author yujiahe
	 */

	@RequestMapping(value = "/sysUser_index", method = RequestMethod.GET)
	public String index() {
		return "system/sysUserRole_index";
	}

	/**
	 * @Description: 进入到批量角色分配页面
	 * @author yujiahe
	 */

	@RequestMapping(value = "/sysUserRoles_update", method = RequestMethod.GET)
	public String sysUserRolesUpdate(Model model) {
		SysUser sysUser = new SysUser();
		List<SysUser> sysUserList = sysUserService.find(sysUser);
		model.addAttribute("sysUserList", sysUserList);
		List<SysRole> sysRoleList = sysRoleService.findSysRoleList();
		model.addAttribute("sysRoleList", sysRoleList);
		List<SysRole> sysGroupList = sysRoleService.findGroupList();
		model.addAttribute("sysGroupList", sysGroupList);
		return "system/sysUserRoles_update";
	}

	/**
	 * @Description:用户角色分配分页数据查询
	 * @author yujiahe
	 */
	@RequestMapping(value = "/userRolePage")
	@ResponseBody
	public Object userRolePage(@RequestParam String realName,@RequestParam String roleId,
			@RequestParam String orgId, HttpServletRequest request) {
		// 创建pageWrapper
		PageWrapper<SysUserRole> pageWrapper = new PageWrapper<SysUserRole>(
				request);
		// 添加搜索条件 用户组 用户角色 用户名，其中用户组用户角色统一由roleId处理
        if( realName.contains("%")){
            realName=realName.replace("%","\\%");
        }
        if( realName.contains("_")){
            realName=realName.replace("_","\\_");
        }
		if (null != realName && !realName.equals("")) {
			pageWrapper.addSearch("realName", realName);
		}else if (null != orgId && !orgId.equals("")) {
			SysOrganization organization = sysOrganizationService.get(orgId) ; 
			List<String> allOrgIds = userAuthorizationService.getAllChildOrganizationByCode(organization.getOrganizationCode()) ;
			pageWrapper.addSearch("allOrgIds", allOrgIds);
//            pageWrapper.addSearch("orgId", orgId);
        }
		if (null != roleId && !roleId.equals("")) {
			pageWrapper.addSearch("roleId", roleId);
		}
		
		// 后台取值
		sysUserRoleService.find(pageWrapper.getPageBean(), "findByMap",
				pageWrapper.getConditionsMap());
		// 返回到页面的数据
		String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		logger.debug("result:" + json);
		return json;

	}

	/**
	 * @Description:进入配置用户角色页面
	 * @author yujiahe
	 */

	@RequestMapping(value = "/sysUserRole_update", method = RequestMethod.GET)
	public String sysUserRoleUpdate(@RequestParam String Id, Model model) {
		// 根据数据字典类别ID查询类别信息
		logger.debug("sysUserRole_update" + Id);
		List<SysUserRole> sysUserRoleList = sysUserRoleService
				.getSysUserRoleListByUserId(Id);
		// String json=JsonUtils.toJsonByGoogle(sysUserRoleList);
		model.addAttribute("sysUserRoleList", sysUserRoleList);
		// if(StringUtils.isEmpty(roleType) ){
		// map.put("message", "角色类别不能为空");
		// }
		List<SysRole> sysRoleList = sysRoleService
				.findUnUseSysRoleListByUserId(Id);
		model.addAttribute("sysRoleList", sysRoleList);
		model.addAttribute("Id", Id);
		SysUser sysUser = sysUserService.get(Id);
		model.addAttribute("sysUser", sysUser);
		return "system/sysUserRole_update";
	}
    /**
     * @Description: 根据用户ID查询用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
	@RequestMapping(value = "/getSysUserRoleListByUserId", method = RequestMethod.GET)
	@ResponseBody
	public String getSysUserRoleListByUserId(@RequestParam String Id,
			Model model) {
		logger.debug("sysUserRole_update" + Id);
		List<SysUserRole> sysUserRoleList = sysUserRoleService
				.getSysUserRoleListByUserId(Id);
		String json = JsonUtils.toJsonByGoogle(sysUserRoleList);
		model.addAttribute("sysUserRoleList", json);

		return json;
	}

	/**
	 * @Description: 保存修改某用户的角色
	 * @param roleIds
	 *            ,userId
	 * @return String
	 * @author yujiahe
	 */
	@RequestMapping(value = "/sysUserRole_updateSave")
	@ResponseBody
	public String saveEditSysUserRole(@RequestParam String roleIds,
			@RequestParam String userId) {
		Map<String, Object> map = new HashMap<String, Object>();

		// sysUserRole.setUserId(userId);
		// sysUserRoleService.delete(sysUserRole);

		sysUserRoleService.saveEditSysUserRole(roleIds, userId);
		map.put("operator", true);
		map.put("message", "添加成功");

		return JSONObject.fromObject(map).toString();
	}

	/**
	 * @Description: 保存批量修改某用户的角色
	 * @param roleId
	 *            ,userIds
	 * @return String
	 * @author yujiahe
	 */
	@RequestMapping(value = "/sysUserRoles_updateSave")
	@ResponseBody
	public String saveEditSysUserRoles(@RequestParam String roleId,
			@RequestParam String userIds) {
		Map<String, Object> map = new HashMap<String, Object>();
		sysUserRoleService.saveEditSysUserRoles(roleId, userIds);
		map.put("operator", true);
		map.put("message", "添加成功");

		return JSONObject.fromObject(map).toString();
	}
    /**
     * @Description: 删除某角色下某用户
     * @param roleId ,userId
     * @return String
     * @author yujiahe
     */
    @RequestMapping(value = "/sysUserRoles_deletebyUR")
    @ResponseBody
    public String sysUserRolesDeletebyUR(@RequestParam String roleId,
                                       @RequestParam String userId) {
        Map<String, Object> map = new HashMap<String, Object>();
        sysUserRoleService.sysUserRolesDeletebyUR(roleId, userId);
        map.put("operator", true);
        map.put("message", "添加成功");
        return JSONObject.fromObject(map).toString();
    }
    
    /**
     * 递归获取所选节点下的所有叶子节点
     * @param nodeId
     * @param allLeafNodes
     * @return
     */
    private List<String> getAllLeafNodes(String nodeId,List<String> allLeafNodes){
    	List<String> result = new ArrayList<String>();
    	Map<String,String> mp = new HashMap<String,String>();
    	mp.put("id", nodeId);
        List<SysOrganization> sysOrganizationList = sysOrganizationService.selectOrgByParentId(mp);
        for(SysOrganization bean:sysOrganizationList){
        	if(!bean.getIsParent()){
        		allLeafNodes.add(bean.getId());
        	} else {
        		result = this.getAllLeafNodes(bean.getOrganizationCode(),allLeafNodes);
        	}
        }
        return result;
    }
}
