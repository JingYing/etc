package com.jd.oa.system.dao;

import com.jd.oa.common.dao.BaseDao;

/**
 * @author qiuyang
 *
 */
public interface PsDao extends BaseDao<Object, Long> {

}
