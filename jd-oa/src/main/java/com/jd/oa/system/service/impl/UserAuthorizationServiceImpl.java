package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.impl.jam.mutable.MPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;

@Service("userAuthorizationService")
@Transactional

public class UserAuthorizationServiceImpl implements UserAuthorizationService{
	
	
	@Autowired
	private SysAuthExpressionService sysAuthExpressionService;
	

	@Autowired
	private SysUserService sysUserService;
	


	@Autowired
	private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
	
	@Autowired
	private SysOrganizationService sysOrganizationService ;
	
	//根据每条授权表达式获取符合的用户
	public List<SysUser> getUserListBySysAuthExpressionBack(SysAuthExpression sysAuthExpression){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("role_id", sysAuthExpression.getRoleId());
		map.put("position_id", sysAuthExpression.getPositionId());
		
		//如果选择了“以上”，则levelTo为最大值
		//if(sysAuthExpression.getLevelAbove()!=null && sysAuthExpression.getLevelAbove().equals(""))
		//	map.put("level_to", sysAuthExpression.getLevelAbove());
		//else {
			map.put("level_to", sysAuthExpression.getLevelUp());
		//}
		map.put("level_from", sysAuthExpression.getLevelDown());
		
		map.put("organization_id", sysAuthExpression.getOrganizationId());
		map.put("userid", sysAuthExpression.getUserId());
		map.put("group_id", sysAuthExpression.getGroupId());

		List<SysUser> list = sysUserService.find("findBySysAuthExpressionBackup",map);
		
		return list;
	}
	
	
	//根据每条授权表达式获取符合的用户
		@Override
		public List<SysUser> getUserListBySysAuthExpression(SysAuthExpression sysAuthExpression){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("authid", sysAuthExpression.getId());
			if(sysAuthExpression.getOrganizationId() != null && ! "".equals(sysAuthExpression.getOrganizationId()))
				map.put("organizationId", getAllChildOrganizationByCode(sysAuthExpression.getOrganizationId()));

			List<SysUser> list = sysUserService.find("findBySysAuthExpression",map);
			
			return list;
		}
	
	//根据授权表达式列表获取所有符合的用户
	@Override
	public List<SysUser> getUserListBySysAuthExpressionList(List<SysAuthExpression> sysAuthExpressionList) {
		List<SysUser> userList = new ArrayList<SysUser>();
		for (SysAuthExpression sysAuthExpression : sysAuthExpressionList) {
			List<SysUser> temp = getUserListBySysAuthExpression(sysAuthExpression);
			userList.removeAll(temp);
			userList.addAll(temp);
			
		}
		return userList;
	}

	
	//根据授权表达式列表获取所有符合的用户,同上，返回类型不同
	public Map<String, String> getUserMapBySysAuthExpressionList(List<SysAuthExpression>  sysAuthExpressionList) {
		List<SysUser> userList = getUserListBySysAuthExpressionList(sysAuthExpressionList);
		
		StringBuffer usersErpidB = new StringBuffer();
		StringBuffer usersNameB = new StringBuffer();
		for (SysUser u : userList){
			usersErpidB.append(u.getUserName()).append(",");
			usersNameB.append(u.getRealName()).append(",");
		}
		
		if (usersErpidB.length()>0 || usersNameB.length()>0 ){
			String usersErpid = usersErpidB.toString();
			String usersName = usersNameB.toString();
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("usersErpid", usersErpid.substring(0, usersErpid.length()-1));
			map.put("usersName", usersName.substring(0, usersName.length()-1));
			
			return map;
		}else{
			return null;
		}
	}
	
	@Override
	public List<SysUser> getUserListByMultiSysAuthExpressionList(		List<SysAuthExpression> sysAuthExpressionList) {
		List<String> list = new ArrayList<String>();
		if(sysAuthExpressionList != null && sysAuthExpressionList.size() > 0){
			for (SysAuthExpression auth : sysAuthExpressionList) {
				list.add(auth.getId());
			}
			
			
			List<SysUser> users = sysUserService.find("findByMultiSysAuthExpression",list);
			
			return users;
		}
		return null;
	}


	//根据授权表达式ID获取中文名称	
	@Override
	public List<SysAuthExpression> getAuthExpressionNameListByIds(List<SysAuthExpression> list){
		List<SysAuthExpression> all = new ArrayList<SysAuthExpression>();
		for (SysAuthExpression item : list) {
			SysAuthExpression auth = getAuthExpressionNameListById(item);
			if(auth!=null)
				all.add(auth);
			
		}
		return all;
	}
	
	public SysAuthExpression getAuthExpressionNameListById(SysAuthExpression sysAuthExpression){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("expressionid", sysAuthExpression.getId());


		List<SysAuthExpression>  list= sysAuthExpressionService.find("findNamesByIdLeftJoin",map);
		
		if(list.size()>0)
			return list.get(0);
		return null;
					
	}
	
	
	//查看某个资源是否属于指定用户  pram2 常量：SysBussinessAuthExpression.BUSSINESS_TYPE_ADMINISTRATOR   parm sysuser.id
	@Override
	public 	boolean isResourceOwnedByUser(String businessId, String businessType, String userId){
		
		
//		List<String> ids = new ArrayList<String>();
//		for (SysBussinessAuthExpression item : sysBusinessAuthExpressions) {
//			ids.add(item.getAuthExpressionId());
//		}
//		
//		//可优化
//		List<SysAuthExpression> list = getAuthExpressionListByIds((String[])ids.toArray());
//		List<SysUser> users = getUserListBySysAuthExpressionList(list);
//		
//		for (SysUser user : users) {
//			if(user.getUserCode().equals(userName))
//				return true;
//		}
		
//		List<SysBussinessAuthExpression> list = getResourceByUserId(userName, businessType);
//		for (SysBussinessAuthExpression item : list) {
//			if(item.getBusinessId().equals(businessId))
//				return true;
//		}

		Map<String,Object> map = new HashMap<String, Object>();
		
		//测试“12”
		map.put("userId",userId);
		
		map.put("businessType", businessType);
		
		map.put("businessId", businessId);
		
		List<SysBussinessAuthExpression> sysBusinessAuthExpressions = sysBussinessAuthExpressionService.find("isResourceOwnedByUserId",map);
		
		if(sysBusinessAuthExpressions==null || sysBusinessAuthExpressions.size()<=0)
			return false;
		return true;
	}

	//列出某个用户的所有资源
	@Override
	public List<SysBussinessAuthExpression> getResourceByUserId(String userId, String businessType){
		Map<String,Object> map = new HashMap<String, Object>();
		
		//测试“12”
		map.put("userId",userId);
		
		//测试“1”
		map.put("businessType", businessType);
		
		SysUser  user = sysUserService.get(userId);
		if(user != null){
			String organizationId = user.getOrganizationId() ;
			
			if(organizationId != null){
				SysOrganization org = sysOrganizationService.get(organizationId) ;
				if(org != null){
					List<String> str = getAllParentOrganizationByCode(org.getOrganizationCode()) ;
					map.put("allOrgIds", str);
				}
			}
			
		}
		
		List<SysBussinessAuthExpression> sysBusinessAuthExpressions = sysBussinessAuthExpressionService.find("getResourceByUserId",map);
		
		return sysBusinessAuthExpressions;
	}


	
	/*
	 * 根据组织id获取所有的子组织结构id，包括其自身
	 */
	@Override
	public List<String> getAllChildOrganizationByCode(String organizationCode) {
		
		Map<String,Object> map = new HashMap<String, Object>();
		
		map.put("organizationCode",organizationCode);
		
//		List<SysUser> list = sysUserService.find("getAllChildOrganizationByCode",map);
//		
//		if( list != null && list.size() > 0 ){
//			List<String> l = new ArrayList<String>();
//			for (SysUser sysUser : list) {
//				l.add(sysUser.getId());
//			}
//			
//			return l;
//		}
		
		//Add By WXJ At 2014-08-06
		List<SysOrganization> list = sysOrganizationService.find("getAllChildOrganizationByCode",map);
		
		if( list != null && list.size() > 0 ){
			List<String> l = new ArrayList<String>();
			for (SysOrganization sysOrganization : list) {
				l.add(sysOrganization.getId());
			}
			
			return l;
		}
		
		return null;
//		StringBuffer sb = new StringBuffer();
//		if( list != null && list.size() > 0 ){
//			for (SysUser sysUser : list) {
//				sb.append("'");
//				sb.append(sysUser.getId()) ;
//				sb.append("',") ;
//			}
//		}
//		
//		String s = sb.toString();
//		if(s != null){
//			int index = s.lastIndexOf(",");
//			if(index >= 0 ){
//				s = s.substring(0 , index);
//			}
//		}
//		
//		
// 		return s ;
	}
	
	/*
	 * 根据组织id获取所有的父组织结构id，包括其自身
	 */
	@Override
	public List<String> getAllParentOrganizationByCode(String organizationCode) {
		List<String> str = new ArrayList<String>() ;
		
				
		while(organizationCode != null && ! organizationCode.equals("") && ! organizationCode.equals("00000000")){
			Map<String, String> map = new HashMap<String, String>() ;
			map.put("organizationCode", organizationCode);
			List<SysOrganization> list = sysOrganizationService.find("findByOrganizationCode" , map) ;
			if( list != null && list.size() > 0){
				str.add(list.get(0).getOrganizationCode()) ;
				organizationCode = list.get(0).getParentId() ;
			}else{
				break ;
			}
		}
		
		str.add(	"00000000") ;
		
		return str ;
		
	}

	

	
}
