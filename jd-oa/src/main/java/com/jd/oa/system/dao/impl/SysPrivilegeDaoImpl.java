/**
 * 
 */
package com.jd.oa.system.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysPrivilegeDao;
import com.jd.oa.system.model.SysPrivilege;

/**
 * @author liub
 *
 */
@Component
public class SysPrivilegeDaoImpl extends MyBatisDaoImpl<SysPrivilege, String> implements SysPrivilegeDao {

	@Override
	public List<SysPrivilege> findByRoleId(String roleId) {
		return getSqlSession().selectList(sqlMapNamespace +".findByRoleId",roleId);
	}

	@Override
	public List<SysPrivilege> findByUserId(String userId) {
		return getSqlSession().selectList(sqlMapNamespace +".findByUserId",userId);
	}
}