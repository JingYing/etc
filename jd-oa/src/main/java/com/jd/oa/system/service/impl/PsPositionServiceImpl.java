package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.jd.oa.system.dao.PsPositionDao;
import com.jd.oa.system.dao.SysPositionDao;
import com.jd.oa.system.model.PsPosition;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.service.PsPositionService;
import com.jd.oa.system.service.SysPositionService;

@Component("psPositionService")
public class PsPositionServiceImpl implements PsPositionService{
	
	@Resource
	private SysPositionDao sysPositionDao;
	@Resource
	private SysPositionService sysPositionService;
	@Resource
	private PsPositionDao psPositionDao;
	
	@Override
	public void insertOrUpdateSysPosition()	{
		List<PsPosition> psPos = psPositionDao.findByEffStatus("A");
		List<SysPosition> sysPos = new ArrayList<SysPosition>();
		for(PsPosition p : psPos)	{
			sysPos.add(this.toSysPosition(p));
		}
		psPos.clear();
		
		sysPositionService.insertOrUpdate(sysPos);	//调用原有代码
		sysPos.clear();
	}
	
	@Override
	public void deleteSysPosition()	{
		List<SysPosition> delPos = sysPositionDao.findNotInPs();
		for(SysPosition p : delPos)	{
			p.setStatus("1");
			sysPositionService.update(p);
		}
	}
	
	private SysPosition toSysPosition(PsPosition p) {
		SysPosition sys = new SysPosition();
		sys.setPositionCode(p.getPosId());
		sys.setPositionName(p.getDescr());
		sys.setParentId(p.getParentId());
		sys.setOrganizationId(p.getOrgId());
		return sys;
	}

}
