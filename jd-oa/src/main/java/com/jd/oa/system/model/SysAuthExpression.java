package com.jd.oa.system.model;

import java.util.Date;

public class SysAuthExpression {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String id;
    
    private String authExpressionName;
    
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.MODE
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String mode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.ORGANIZATION_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String organizationId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.POSITION_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String positionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_UP
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String levelUp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_DOWN
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String levelDown;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_ABOVE
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String levelAbove;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.USER_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.ROLE_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String roleId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.GROUP_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String groupId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.CREATOR
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String creator;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private String modifier;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_AUTH_EXPRESSION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    private Date modifyTime;
    
    private String owner;
    
    public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.ID
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.ID
     *
     * @param id the value for T_JDOA_SYS_AUTH_EXPRESSION.ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    public String getAuthExpressionName() {
		return authExpressionName;
	}

	public void setAuthExpressionName(String authExpressionName) {
		this.authExpressionName = authExpressionName;
	}

	/**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.MODE
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.MODE
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getMode() {
        return mode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.MODE
     *
     * @param mode the value for T_JDOA_SYS_AUTH_EXPRESSION.MODE
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setMode(String mode) {
        this.mode = mode == null ? null : mode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.ORGANIZATION_ID
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.ORGANIZATION_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getOrganizationId() {
        return organizationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.ORGANIZATION_ID
     *
     * @param organizationId the value for T_JDOA_SYS_AUTH_EXPRESSION.ORGANIZATION_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId == null ? null : organizationId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.POSITION_ID
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.POSITION_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getPositionId() {
        return positionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.POSITION_ID
     *
     * @param positionId the value for T_JDOA_SYS_AUTH_EXPRESSION.POSITION_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setPositionId(String positionId) {
        this.positionId = positionId == null ? null : positionId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_UP
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_UP
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getLevelUp() {
        return levelUp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_UP
     *
     * @param levelUp the value for T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_UP
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setLevelUp(String levelUp) {
        this.levelUp = levelUp == null ? null : levelUp.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_DOWN
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_DOWN
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getLevelDown() {
        return levelDown;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_DOWN
     *
     * @param levelDown the value for T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_DOWN
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setLevelDown(String levelDown) {
        this.levelDown = levelDown == null ? null : levelDown.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_ABOVE
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_ABOVE
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getLevelAbove() {
        return levelAbove;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_ABOVE
     *
     * @param levelAbove the value for T_JDOA_SYS_AUTH_EXPRESSION.LEVEL_ABOVE
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setLevelAbove(String levelAbove) {
        this.levelAbove = levelAbove == null ? null : levelAbove.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.USER_ID
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.USER_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.USER_ID
     *
     * @param userId the value for T_JDOA_SYS_AUTH_EXPRESSION.USER_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.ROLE_ID
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.ROLE_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.ROLE_ID
     *
     * @param roleId the value for T_JDOA_SYS_AUTH_EXPRESSION.ROLE_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.GROUP_ID
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.GROUP_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.GROUP_ID
     *
     * @param groupId the value for T_JDOA_SYS_AUTH_EXPRESSION.GROUP_ID
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.CREATOR
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.CREATOR
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.CREATOR
     *
     * @param creator the value for T_JDOA_SYS_AUTH_EXPRESSION.CREATOR
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.CREATE_TIME
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.CREATE_TIME
     *
     * @param createTime the value for T_JDOA_SYS_AUTH_EXPRESSION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.MODIFIER
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.MODIFIER
     *
     * @param modifier the value for T_JDOA_SYS_AUTH_EXPRESSION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.MODIFY_TIME
     *
     * @return the value of T_JDOA_SYS_AUTH_EXPRESSION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_AUTH_EXPRESSION.MODIFY_TIME
     *
     * @param modifyTime the value for T_JDOA_SYS_AUTH_EXPRESSION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 14:18:11 CST 2013
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;  
        if (obj == null) return false;  
        if (getClass() != obj.getClass()) return false;  
        SysAuthExpression other = (SysAuthExpression) obj;  
        if (!strNull2Empty(authExpressionName).equals(strNull2Empty(other.authExpressionName))) return false;
        if (!strNull2Empty(mode).equals(strNull2Empty(other.mode))) return false;
        if (!strNull2Empty(organizationId).equals(strNull2Empty(other.organizationId))) return false;
        if (!strNull2Empty(positionId).equals(strNull2Empty(other.positionId))) return false;
        if (!strNull2Empty(levelAbove).equals(strNull2Empty(other.levelAbove))) return false;
        if (!strNull2Empty(levelDown).equals(strNull2Empty(other.levelDown))) return false;
        if (!strNull2Empty(levelUp).equals(strNull2Empty(other.levelUp))) return false;
        if (!strNull2Empty(userId).equals(strNull2Empty(other.userId))) return false;
        if (!strNull2Empty(groupId).equals(strNull2Empty(other.groupId))) return false;
        if (!strNull2Empty(roleId).equals(strNull2Empty(other.roleId))) return false;
        return true;  
	}
	private String  strNull2Empty(String string){
		return string==null?"":string;
	}
    
}