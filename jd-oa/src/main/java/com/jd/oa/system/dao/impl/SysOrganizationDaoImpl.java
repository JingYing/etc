package com.jd.oa.system.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysOrganizationDao;
import com.jd.oa.system.model.SysOrganization;

import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Component("sysOrganizationDao")

public class SysOrganizationDaoImpl extends MyBatisDaoImpl<SysOrganization, String> implements SysOrganizationDao{

	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 上午11:33:04
	 *
	 * @param mp
	 * @return
	 */
	@Override
	public List<SysOrganization> selectOrgByParentId(Map mp) {
        return this.getSqlSession().selectList("com.jd.oa.system.model.SysOrganization.findOrgByParentId", mp);
    }

	@Override
	public List<SysOrganization> findByOrganizationType(String organizationType) {
		return this.getSqlSession().selectList("com.jd.oa.system.model.SysOrganization.findByOrganizationType", organizationType);
	}
	/**
	 * @see
	 */
	public String getMaxId(){
		SysOrganization org = this.getSqlSession().selectOne("com.jd.oa.system.model.SysOrganization.getMaxId");
		return org.getId();
	}

	@Override
	public Map<String, String> getOrganizationLevelMap(String organizationFullPath) {
		if(organizationFullPath == null || organizationFullPath.equals("")) return null;
		//组织数据返回使用
		Map<String,String> resultMap = new LinkedHashMap<String, String>();
//	    fullPath格式 "/00000000/00000840/00003046/00010900/00010914/00010968/00010993"
		String[] orgIds = organizationFullPath.split("/");
		// orgIds[0] 为空串
		// orgIds[1] 为"京东"
		// orgIds[2] 为"总部"、"子公司"、"区域分公司"
		if(orgIds.length <3) return null;
		Map<String,String> organizationMap = new LinkedHashMap<String, String>();
		for(int i=2;i<orgIds.length;i++){
			List<SysOrganization> organizationList = this.find("findByOrganizationCode",orgIds[i]);
			if(organizationList != null && organizationList.size() ==1 ){
				organizationMap.put(orgIds[i],organizationList.get(0).getOrganizationName());
			}
		}
/*		总部和子公司的一级部门都为4级，区域分公司的一级部门为3级
		总部和子公司的二级部门都为5级，区域分公司的二级部门为4级*/
		String secondOrgName = organizationMap.get(orgIds[2]);
		if(secondOrgName.equals("总部") || secondOrgName.equals("子公司")){
			organizationMap.remove(orgIds[2]);
			if(orgIds.length >=4){
				resultMap.put("ORG_0_CODE",orgIds[3]);
				resultMap.put("ORG_0_NAME",organizationMap.get(orgIds[3]));
				organizationMap.remove(orgIds[3]);
			}
		}else{
			resultMap.put("ORG_0_CODE",orgIds[2]);
			resultMap.put("ORG_0_NAME",secondOrgName);
			organizationMap.remove(orgIds[2]);
		}
		int i=0;
		for(Map.Entry<String,String> entry:organizationMap.entrySet()){
			i++;
			resultMap.put("ORG_"+i+"_CODE",entry.getKey());
			resultMap.put("ORG_"+i+"_NAME",entry.getValue());
		}
		return resultMap;
	}

	@Override
	public List<SysOrganization> findNotInPs() {
		return getSqlSession().selectList(sqlMapNamespace + ".findNotInPs");
	}
}
