package com.jd.oa.system.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysOrganization;

import java.util.List;
import java.util.Map;

public interface SysOrganizationService extends BaseService<SysOrganization, String> {
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 上午11:33:04
	 *
	 * @param id
	 * @return
	 */
	public List<SysOrganization> selectOrgByParentId(Map mp);
	
	/**
	 * 定时任务：传输EHR组织数据到OA
	 */
	public void transferOrgInfoFromHR(List<SysOrganization> allOrg);
	/**
	 * 获取下属organizationCode List
	 * @return
	 */
	public List<String> findUnderlingList(String userName );


	public Map<String,String> getOrganizationLevelMap(String organizationFullPath);

	/**
	 * 根据源ORG对象, 对已有sysorg新增或更新
	 * @param orgs
	 */
	void insertOrUpdate(List<SysOrganization> orgs);


}
