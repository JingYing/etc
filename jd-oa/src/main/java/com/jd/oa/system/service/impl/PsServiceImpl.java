package com.jd.oa.system.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.dao.sqladpter.OracleAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.SqlType;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.system.dao.PsDao;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.PsService;


/**
 * @author qiuyang
 *
 */
@Service("psService")
@Transactional
public class PsServiceImpl extends BaseServiceImpl<Object, Long> implements PsService {

	@Autowired
	private PsDao psDao;

	public PsDao getDao() {
		return psDao;
	}
	
	@Override
	public List<SysOrganization> getAllOrganizations() {
		List<SysOrganization> orgs = new ArrayList<SysOrganization>();
		SqlAdapter sqlAdapter = new OracleAdapter(SqlType.DML_SELECT);
		StringBuffer sql = new StringBuffer();
		sql.append("");
		sql.append("select HR_ORG_ORG_ID        as ORGANIZATION_CODE,");
		sql.append("       HR_ORG_NAME          as ORGANIZATION_NAME,");
		sql.append("   	   HR_ORG_RANK          as ORGANIZATION_FULLPATH,");
		sql.append("   	   HR_ORG_FULL_NAME     as ORGANIZATION_FULLNAME,");
		sql.append("   	   HR_ORG_PARENT_ID     as PARENT_CODE");
		sql.append(" from  JDHR.V_PUBLIC_ORG");
		sqlAdapter.setSql(sql.toString());
		List<Map> list = (List<Map>)this.sql(sqlAdapter);
		for(Map<String,Object> maps: list){
			SysOrganization org = new SysOrganization();
			org.setOrganizationCode(object2Str(maps.get("ORGANIZATION_CODE")));
        	org.setOrganizationName(object2Str(maps.get("ORGANIZATION_NAME")));
        	org.setOrganizationFullname(object2Str(maps.get("ORGANIZATION_FULLNAME")));
        	org.setOrganizationFullPath(object2Str(maps.get("ORGANIZATION_FULLPATH")));
        	org.setParentId(object2Str(maps.get("PARENT_CODE")));
        	org.setStatus("0");
        	orgs.add(org);
		}
		return orgs;
	}
	
	@Override
	public List<SysPosition> getAllPositions(){
		List<SysPosition> pos = new ArrayList<SysPosition>();
		SqlAdapter sqlAdapter = new OracleAdapter(SqlType.DML_SELECT);
		StringBuffer sql = new StringBuffer();
		sql.append("");
		sql.append("select POS_ID         as POSITION_CODE,");
		sql.append("   	   DESCR          as POSITION_NAME,");
		sql.append("       PARENT_ID      as PARENT_CODE,");
		sql.append("       ORG_ID         as ORGANIZATION_ID");
		sql.append("  from JDHR.HR_POSITION");
		sql.append(" where EFF_STATUS = 'A'");
		sqlAdapter.setSql(sql.toString());
		List<Map> list = (List<Map>)this.sql(sqlAdapter);
		for(Map<String,Object> maps: list){
			SysPosition position = new SysPosition();
			position.setPositionCode(object2Str(maps.get("POSITION_CODE")));
        	position.setPositionName(object2Str(maps.get("POSITION_NAME")));
        	position.setOrganizationId(object2Str(maps.get("ORGANIZATION_ID")));
        	position.setParentId(object2Str(maps.get("PARENT_CODE")));
        	position.setStatus("0");
        	pos.add(position);
		}
		return pos;
	}
	
	@Override
	public List<SysUser> getAllUsers(){
		List<SysUser> users = new ArrayList<SysUser>();
		SqlAdapter sqlAdapter = new OracleAdapter(SqlType.DML_SELECT);
		StringBuffer sql = new StringBuffer();
		sql.append("");
		sql.append("select HR_ORG_ORG_ID            as orgId,");               //组织机构ID
		sql.append("       HR_PERSON_PERSON_ID      as user_code,");           //唯一标示
		sql.append("       HR_PERSON_ERPID          as user_name,");           //ERPID
		sql.append("       HR_PERSON_PERSONNAME     as real_name,");           //姓名
		sql.append("       HR_POSITION_POS_ID       as position_code,");       //职位ID
		sql.append("       decode(DESCR100_6, ");
		sql.append("              null, ");
		sql.append("              POS_JOBLEVEL, ");
		sql.append("              POS_JOBLEVEL || '-' || DESCR100_6) as levelcode,"); //职级        
		sql.append("       HR_PERSON_QYYX           as email,");               //企业邮箱
		//sql.append("       HR_PERSON_TEL            as mobile,");              //手机号
		sql.append("       HR_PERSON_GRLXDHT            as mobile,");              //手机号
		sql.append("       HR_PERSON_SEX            as sex,");                 //性别
		sql.append("       HR_PERSON_BIRTHDAY       as bierthday,");           //生日
		sql.append("       A04_A0401                as age,");                 //年龄
		sql.append("       HR_ORG_RANK              as org_full_path,");       //所在部门全路径ID
		sql.append("       HR_ORG_FULL_NAME         as org_full_name,");       //所在部门全路径名称
		sql.append("       HR_POSITION_PARENT_ID    as position_parent_code,");//上级岗位ID
		sql.append("       CTJ021_CODEID            as codeid,");              //主体公司ID
		sql.append("       CTH148_CODENAME          as codename");            //主体公司名称
		//sql.append("       CTH440_CODENAME          as status");
		sql.append(" from JDHR.V_PUBLIC ");
		sql.append(" where CTH440_CODENAME not in ('301', '302') ");
		sqlAdapter.setSql(sql.toString());
		List<Map> list = (List<Map>)this.sql(sqlAdapter);
		for(Map<String,Object> maps: list){
			SysUser userBean  = new SysUser();
			userBean.setOrganizationId(object2Str(maps.get("ORGID")));
			userBean.setUserCode(object2Str(maps.get("USER_CODE")));
			userBean.setUserName(object2Str(maps.get("USER_NAME")));
			userBean.setRealName(object2Str(maps.get("REAL_NAME")));
			userBean.setPositionCode(object2Str(maps.get("POSITION_CODE")));
			userBean.setLevelCode(object2Str(maps.get("LEVELCODE")));
			userBean.setEmail(object2Str(maps.get("EMAIL")));
			userBean.setMobile(object2Str(maps.get("MOBILE")));
			userBean.setSex(object2Str(maps.get("SEX")).equals("M")? "男":"女");
			Date birthday = null;
            if(maps.get("BIERTHDAY") != null && !"".equals(maps.get("BIERTHDAY"))){
                Timestamp  time = (Timestamp)maps.get("BIERTHDAY");
            	birthday = DateUtils.convertFormatDateString_yyyy_MM_dd(time.toString());
            }
            userBean.setBirthday(birthday);
            int age = 0;
            if(maps.get("AGE") != null && !"".equals(maps.get("AGE"))){
            	age = Integer.parseInt(object2Str(maps.get("AGE")));
            }
            //userBean.setStatus(object2Str(maps.get("STATUS")));
            userBean.setAge(age);
            userBean.setStatus("0");
            userBean.setPositionParentCode(object2Str(maps.get("POSITION_PARENT_CODE")));
            userBean.setCodeid(object2Str(maps.get("CODEID")));
            userBean.setCodename(object2Str(maps.get("CODENAME")));
            users.add(userBean);
		}
		return users;
	}
	
	private String object2Str(Object value){
        if (value!=null)
            return value.toString();
        else
            return  "";
    }
	
	public Map<String, Object> getList(String sql){
		Map<String,Object> mapResult = new HashMap<String, Object>(2);
		if(null != sql && sql.trim().toUpperCase().contains("SELECT")){
			SqlAdapter sqlAdapter = new OracleAdapter(SqlType.DML_SELECT);
			sqlAdapter.setSql(sql);
			List<Map> list = new ArrayList<Map>();
			try {
				list = (List<Map>)this.sql(sqlAdapter);
			} catch (Exception e) {
				mapResult.put("operate", "error");
				mapResult.put("data", "表或视图不存在！");
				return mapResult;
			}
			mapResult.put("operate", "select");
			mapResult.put("data", list);
		}else{
			mapResult.put("operate", "error");
			mapResult.put("data", "不允许非查询操作！");
		}
		return mapResult;
	}

}
