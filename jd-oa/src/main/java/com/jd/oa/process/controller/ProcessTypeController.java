package com.jd.oa.process.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.FastArrayList;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessType;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.process.service.ProcessTypeService;
import com.jd.oa.system.model.SysAuthExpression;




@Controller
@RequestMapping("/process")
public class ProcessTypeController {
	private static final Logger logger = Logger.getLogger(ProcessTypeController.class);
	
	@Autowired
	private ProcessDefService processDefService;
	
	@Autowired
	private ProcessTypeService processTypeService;
	
	@Autowired
	private ProcessNodeService processNodeService;
	
	@RequestMapping(value = "/processType_index")
	 public ModelAndView processType_index(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response) throws Exception {
	    
	    	ModelAndView mav = new ModelAndView("process/processType_index");
			
			return mav;
	    }
	
	
	//查询URL
	@RequestMapping(value = "/processType_view")
	@ResponseBody
	 public String processType_view(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response) throws Exception {
			//创建pageWrapper
			PageWrapper<ProcessType> pageWrapper=new PageWrapper<ProcessType>(request);

			//添加搜索条件
			String processTypeName=request.getParameter("processTypeName");
			if(processTypeName!=null && !("").equals(processTypeName)){
				 if( processTypeName.contains("%")){
					 processTypeName=processTypeName.replace("%","\\%");
			        }
				 if( processTypeName.contains("_")){
					 processTypeName=processTypeName.replace("_","\\_");
			        }
				pageWrapper.addSearch("processTypeName", processTypeName);
			}
			
			//默认按照时间降序
			pageWrapper.addSort("createTime","desc");
			
			processTypeService.find(pageWrapper.getPageBean(),"findByMap", pageWrapper.getConditionsMap());
			
			 //返回到页面的额外数据
		    pageWrapper.addResult("returnKey","returnValue");

		    String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
			
			
	        logger.debug(json);
	        return json;
	    }
	
	//查询一级名称URL
	@RequestMapping(value = "/processNameAjax_view")
	@ResponseBody
	 public String processNameAjax_view(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response) throws Exception {
			
		
			Map<String, String> map = new HashMap<String, String>();
			//参数null必传，否则得不出结果
			List<ProcessType> list = processTypeService.find("findProcessName",map);
			

		    String json=JsonUtils.toJsonByGoogle(list);
			
			
	        logger.debug(json);
	        return json;
	    }
	
	//查询二级名称URL
		@RequestMapping(value = "/processChildNameAjax_view")
		@ResponseBody
		 public String processChildName_view(
		    		@RequestParam(value = "locale", required = false) Locale locale,
		    		@RequestParam(value = "parentId", required = false) String parentId
		    		) throws Exception {
				
				//参数null必传，否则得不出结果
			
				Map<String, String> map = new HashMap<String, String>();
				map.put("parentId", parentId);
				List<ProcessType> list = processTypeService.find("findProcessName",map);
				

			    String json=JsonUtils.toJsonByGoogle(list);
				
				
		        logger.debug(json);
		        return json;
		    }
	
		
	// 根据子类id返回父类id
	@RequestMapping(value = "/processParentIdByIdAjax_view")
	@ResponseBody
	public String processParentIdByChildIdAjax_view(
			@RequestParam(value = "locale", required = false) Locale locale,
			@RequestParam(value = "childId", required = false) String childId)
			throws Exception {

		// 参数null必传，否则得不出结果

		ProcessType pt = processTypeService.get(childId);
		
		if(pt!=null&&pt.getParentId()!=null)
			return pt.getParentId();

		return "";
	}
		
		
	@RequestMapping(value = "/processType_add")
	 public ModelAndView processType_add(
	    		@RequestParam(value = "locale", required = false) Locale locale) throws Exception {
	    
		
		
		ModelAndView mav = new ModelAndView("process/processType_add");
		List<ProcessType> parentProcessType = processTypeService.find("findProcessName",null);
		
		mav.addObject("parentProcessType",parentProcessType);
		//insertByProducingProcessTypeCode
		
		return mav;
	    }
	
	@RequestMapping(value = "/processTypeAjax_add")
	@ResponseBody
	 public String processTypeAjax_add(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		@RequestParam(required = false) String processTypeId,
	    		@RequestParam(required = false) String processTypeName,
	    		@RequestParam(required = false) String sortNo,
	    		@RequestParam(required = false) String modifier,
	    		@RequestParam(required = false) String isOuter,
	    		@RequestParam(required = false) String linkUrl,
	    		@RequestParam(required = false) String parentId) throws Exception {
	    
			int num=0;

			
			//判断是否存在该流程类别
			ProcessType processType = new ProcessType();
			processType.setProcessTypeName(processTypeName);
			if(parentId!=null && ! "".equals(parentId))
				processType.setParentId(parentId);
			processType.setYn(0);
			List<ProcessType> list = processTypeService.find(processType);
			//存在该流程类别种类，新增时
			if(list!=null && list.size()>0 && (processTypeId==null))
						return "0";
			
			processType.setSortNo(Integer.parseInt(sortNo));
			processType.setIsOuter(isOuter);
			processType.setLinkUrl(linkUrl);
			
			
			//新增
			if(processTypeId==null){		
				
				
				processType.setId(null);
				
				//新增时code新增1
				String result =  processTypeService.insertByProducingProcessTypeCode(processType);
				if(result!=null){
					
					//如果创建二级流程类别
					if(parentId != null && !"".equals(parentId)){
						ProcessDefinition def = new ProcessDefinition();
						def.setProcessTypeId(parentId);
						List<ProcessDefinition> pdefList = processDefService.find(def) ;
						//判断一级流程类别下是否有流程定义
						if(pdefList != null && pdefList.size() > 0)
							return "100";
					}
					
					return "1"; //	不存在该流程类别种类
				}
			}
			//修改
			else {
				processType = processTypeService.get(processTypeId);
				//防止更新时重名
				if( list != null && list.size() > 0 )
					if( ! processType.getProcessTypeName().equals(processTypeName) && ! list.get(0).getId().equals(processType.getId()))
						return "0";
				if(processType != null ){
					processType.setProcessTypeName(processTypeName);
					processType.setSortNo(Integer.parseInt(sortNo));
					processType.setParentId(parentId);
					processType.setIsOuter(isOuter);
					processType.setLinkUrl(linkUrl);
					if(parentId!=null && ! "".equals(parentId))
						processType.setParentId(parentId);
					num = processTypeService.update(processType);
				}
			}
			
			
			
			return Integer.valueOf(num).toString();
	    }
	
	
	@RequestMapping(value = "/processType_delete")
	@ResponseBody
	 public String processType_delete(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		@RequestParam(required = false) String processTypeId,
	    		@RequestParam(required = false) String[] processTypeIds) throws Exception {
		
		logger.debug("processTypeId ="+processTypeId);
		logger.debug("processTypeIds ="+processTypeIds);
		int count=0;
		
		//删除单条记录
		if(processTypeId!=null){
	    	ProcessType processType = new ProcessType();
	    	processType.setId(processTypeId);
	    	if(isReferencedByProcessDef(processTypeId))
	    		return Integer.valueOf(count).toString();
			count = processTypeService.delete(processType);
		}
		
		//删除多条记录
		if(processTypeIds!=null){
			int size=processTypeIds.length;
			
			boolean isExist = false;
			for(int j=0;j<size;j++){
				if(isReferencedByProcessDef(processTypeIds[j])){
					isExist=true;
					break;
				}
			}
			
			//如果存在被引用的，则全部不删除
			if(isExist)
				return new Integer(0).toString();
			
			for(int i=0;i<size;i++){
				ProcessType pt = new ProcessType();
		    	pt.setId(processTypeIds[i]);
				count = count+ processTypeService.delete(pt);
			}
		}
		
		return new Integer(count).toString();
	    	

	    }
	
	//更新(未用)
	@RequestMapping(value = "/processType_update")
	@ResponseBody
	 public String processType_update(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		@RequestParam(required = false) String processTypes) throws Exception {
	    
		logger.debug("processTypes=="+processTypes);

    	
		List<ProcessType> sysAuthExpressions = JsonUtils.fromJsonByGoogle(processTypes, new TypeToken<List<ProcessType>>(){}) ;
		
		return "ok";
			
	    }
	
	
	//获取指定id的流程类别
		@RequestMapping(value = "/processTypeById")
		@ResponseBody
		 public String processTypeById(
		    		@RequestParam(value = "locale", required = false) Locale locale,
		    		@RequestParam(required = false) String processTypeId) throws Exception {
		    
			logger.debug("processTypes=="+processTypeId);

	    	ProcessType processType = processTypeService.get(processTypeId);
			
			
			return JsonUtils.toJsonByGoogle(processType);
				
		    }
		
		
		//根据类别种类id，判断其是否被流程定义引用
		private boolean isReferencedByProcessDef(String processTypeId){
			//查看流程定义是否引用流程类别
			ProcessDefinition processDefinition = new ProcessDefinition();
			processDefinition.setProcessTypeId(processTypeId);
			
			List<ProcessDefinition> list = processDefService.find(processDefinition);
			if(list!=null && list.size()>0)
				return true;
			
			//查看子节点是否引用流程类别
			ProcessType processType = new ProcessType();
			processType.setParentId(processTypeId);
			List<ProcessType> list2 = 	processTypeService.find(processType);
			if(list2!=null && list2.size()>0)
				return true;
			
			return false;
						
		}
}
