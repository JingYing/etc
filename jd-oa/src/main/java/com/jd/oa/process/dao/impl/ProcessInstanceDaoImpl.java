package com.jd.oa.process.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessInstanceDao;
import com.jd.oa.process.model.ProcessInstance;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository("ProcessInstanceDao")
public class ProcessInstanceDaoImpl extends  MyBatisDaoImpl<ProcessInstance, String> implements ProcessInstanceDao{
	
	@Override
	public List<ProcessInstance> findProcessInstanceByKey(String processDefKey) {
		// TODO Auto-generated method stub
		Map<String,String> mp = new HashMap<String, String>();
		mp.put("processDefKey", processDefKey);
		List<ProcessInstance> pdList  = this.find("findProcessInstanceByKey", mp);
		return pdList;
	}
	
	@Override
	public List<String> findAllInstanceByStartId(String starterId, List<String> statusList) {
		Map<String,Object> mp = new HashMap<String, Object>();
		mp.put("starterId", starterId);
		mp.put("status", statusList);		
		List<String> temp = this.getSqlSession().selectList("com.jd.oa.process.model.ProcessInstance.findAllInstanceByStartId", mp);
		return temp;
	}

    /**
     * 根据流程实例ID查询流程实例信息
     * @param processInstance 流程实例实体类
     * @return 流程实例信息
     */
    public ProcessInstance selectByProcessInstanceId(ProcessInstance processInstance){
         return (ProcessInstance) this.getSqlSession().selectOne("com.jd.oa.process.model.ProcessInstance.selectByProcessInstanceId", processInstance);
    }

    /**
     * 取消流程实例
     * @param processInstance 流程实例实体类
     */
    public void cancelByProcessInstanceId(ProcessInstance processInstance){
        this.getSqlSession().update("com.jd.oa.process.model.ProcessInstance.cancelByProcessInstanceId", processInstance);
    }

	@Override
	public List<ProcessInstance> selectByProcessInstanceIds(List<String> processInstanceIds) {
		List<ProcessInstance> result = null;
		result = this.getSqlSession().selectList(
			"com.jd.oa.process.model.ProcessInstance.selectByProcessInstanceIds", processInstanceIds);
    	return result;
	}

	@Override
	public ProcessInstance findByInstanceId(String processInstanceId) {
		ProcessInstance param = new ProcessInstance();
		param.setProcessInstanceId(processInstanceId);
		List<ProcessInstance> list = find(param);
		return list.isEmpty() ? null : list.get(0);
	}
}
