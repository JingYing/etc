package com.jd.oa.process.model;

import com.jd.oa.common.model.IdModel;

import java.util.Date;

public class ProcessProxy extends IdModel {
    /**
     * 办理人ID
     */
    private String assignerId;

    /**
     * 代理人ID
     */
    private String proxyId;

    /**
     * 代理开始时间
     */
    private Date proxyBeginTime;

    /**
     * 代理结束时间
     */
    private Date proxyEndTime;

    /**
     * 代理开始时间(页面)
     */
    private String beginTime;

    /**
     * 代理结束时间(页面)
     */
    private String endTime;
    
    /**
     * 被代理人名称
     */
    private String userNameAddSinger;
    
    /**
     * 被代理人ERPID
     */
    private String assignerIdErpId;
    
	/**
     * 逻辑删除
     */
    private int yn;
    public String getBeginTime() {
        return beginTime;
    }
    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime == null ? null : beginTime.trim();
    }
    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime == null ? null : endTime.trim();
    }
    public String getAssignerId() {
        return assignerId;
    }

    public void setAssignerId(String assignerId) {
        this.assignerId = assignerId == null ? null : assignerId.trim();
    }

    public String getProxyId() {
        return proxyId;
    }


    public void setProxyId(String proxyId) {
        this.proxyId = proxyId == null ? null : proxyId.trim();
    }

    public Date getProxyBeginTime() {
        return proxyBeginTime;
    }

    public void setProxyBeginTime(Date proxyBeginTime) {
        this.proxyBeginTime = proxyBeginTime;
    }

    public Date getProxyEndTime() {
        return proxyEndTime;
    }

    public void setProxyEndTime(Date proxyEndTime) {
        this.proxyEndTime = proxyEndTime;
    }

    public int getYn() {
        return yn;
    }

    public void setYn(int yn) {
        this.yn = yn;
    }
    
    public String getUserNameAddSinger() {
		return userNameAddSinger;
	}
    
	public void setUserNameAddSinger(String userNameAddSinger) {
		this.userNameAddSinger = userNameAddSinger;
	}
	public String getAssignerIdErpId() {
		return assignerIdErpId;
	}
	public void setAssignerIdErpId(String assignerIdErpId) {
		this.assignerIdErpId = assignerIdErpId;
	}
}