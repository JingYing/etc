package com.jd.oa.process.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessDefinitionConfigDao;
import com.jd.oa.process.model.ProcessDefinitionConfig;

@Component("processDefinitionConfigDao")
public class ProcessDefinitionConfigDaoImpl extends MyBatisDaoImpl<ProcessDefinitionConfig, String> implements ProcessDefinitionConfigDao  {

	@Override
	public List<ProcessDefinitionConfig> findConfigItem(String processDefId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("processDefId", processDefId);
		return super.find("findConfigItem", map);
	}

}
