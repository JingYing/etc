package com.jd.oa.process.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessNodeFormPrivilegeDao;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 节点表单权限配置Dao实现类
 * User: zhaoming
 * Date: 13-9-12
 * Time: 下午3:55
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ProcessNodeFormPrivilegeDaoImpl extends MyBatisDaoImpl<ProcessNodeFormPrivilege, String> implements ProcessNodeFormPrivilegeDao {

    /**
     * 根据主键查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置实体类
     */
    public ProcessNodeFormPrivilege selectByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege){
        return this.getSqlSession().selectOne("com.jd.oa.ProcessNodeFormPrivilegeMapper.selectByPrimaryKey", processNodeFormPrivilege.getId());
    }

    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置列表
     */
    public List<ProcessNodeFormPrivilege> selectByCondition(ProcessNodeFormPrivilege processNodeFormPrivilege){
        return this.getSqlSession().selectList("com.jd.oa.ProcessNodeFormPrivilegeMapper.selectByCondition", processNodeFormPrivilege);
    }

    /**
     * 新增节点表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void insertProcessNodeFormPrivilege(ProcessNodeFormPrivilege processNodeFormPrivilege){
        this.getSqlSession().insert("com.jd.oa.ProcessNodeFormPrivilegeMapper.insert", processNodeFormPrivilege);
    }

    /**
     * 根据主键修改表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void updateByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege){
        this.getSqlSession().update("com.jd.oa.ProcessNodeFormPrivilegeMapper.updateByPrimaryKey", processNodeFormPrivilege);
    }

    /**
     * 根据主键删除节点表单权限配置（逻辑删除）
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void deleteByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege){
        this.getSqlSession().delete("com.jd.oa.ProcessNodeFormPrivilegeMapper.deleteByPrimaryKey", processNodeFormPrivilege);
    }

    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置数量
     */
	public Integer countByCondition(
			ProcessNodeFormPrivilege processNodeFormPrivilege) {
		 return this.getSqlSession().selectOne("com.jd.oa.ProcessNodeFormPrivilegeMapper.countByCondition", processNodeFormPrivilege);
	}
}
