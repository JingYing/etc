package com.jd.oa.process.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessAddsigner;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: xulin
 * Date: 13-9-17
 * Time: 下午4:33
 * 加签
 */
public interface ProcessAddsignerDao extends BaseDao<ProcessAddsigner ,String> {
    
	/**
     * 根据加签人ID获取还未办理的流程实例
     * @param userId
     * @param status 1:未办 2:已办
     * @return
     */
    public List<ProcessAddsigner> findProcessInstanceByAddUserId(String userId,String status);
    
    /**
     * 
     * @desc 判断是否是转发任务
     * @author WXJ
     * @date 2013-10-30 下午09:09:17
     *
     * @param processInstanceId
     * @param taskId
     * @param nodeId
     * @return
     */
    public ProcessAddsigner getAddSignTask(String processInstanceId, String taskId, String nodeId);
    
    /**
     * 根据加签人ID获取还未办理的流程实例并根据流程定义ID限定范围
     * @param userId
     * @param status
     * @return
     */
    public List<ProcessAddsigner> findProcessInstanceByAddUserId(String userId,String status,String processDefinitionId);

}
