package com.jd.oa.process.dao;
import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessDefinitionConfig;

public interface ProcessDefinitionConfigDao extends BaseDao<ProcessDefinitionConfig ,String>{
	
	List<ProcessDefinitionConfig> findConfigItem(String processDefId);

}
