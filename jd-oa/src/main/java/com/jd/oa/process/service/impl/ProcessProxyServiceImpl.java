package com.jd.oa.process.service.impl;

import com.jd.common.util.DateFormatUtils;
import com.jd.common.web.LoginContext;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.process.dao.ProcessProxyDao;
import com.jd.oa.process.model.ProcessProxy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.oa.process.service.ProcessProxyService;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-17
 * Time: 下午4:24
 * 代办人service
 */
@Service("processProxyService")
public class ProcessProxyServiceImpl extends BaseServiceImpl<ProcessProxy, String> implements ProcessProxyService{
    private static final Logger logger = Logger.getLogger(ProcessProxyServiceImpl.class);
    @Autowired
    private ProcessProxyDao processproxyDao;

    /**
     * 保存代理人
     * @param processProxy
     * @return
     */
    public List<ProcessProxy> findList(ProcessProxy processProxy){
        List<ProcessProxy>  resultProxy = null;
        processProxy.setProxyEndTime(new Date());
        resultProxy= processproxyDao.findProcessProxysByUserId(processProxy) ;
        return resultProxy;
    }
    /**
     * 查询相同条件的代办人数量
     * @param processProxy
     * @return
     */
    public int getCount(ProcessProxy processProxy) {
        String beginTime= processProxy.getBeginTime()+" 00:00:00" ;
        processProxy.setProxyBeginTime(DateFormatUtils.parseDate(beginTime,"yyyy-MM-dd HH:mm:ss"));
        String endTime= processProxy.getEndTime()+" 23:59:59" ;
        processProxy.setProxyEndTime(DateFormatUtils.parseDate(endTime,"yyyy-MM-dd HH:mm:ss"));
        return processproxyDao.getCount(processProxy);
    }

    /**
     * 新增保存代理人
     * @param processProxy
     * @return
     */
    public void insertProcessProxyInfo(ProcessProxy processProxy) {
        String beginTime= processProxy.getBeginTime()+" 00:00:00" ;
        processProxy.setProxyBeginTime(DateFormatUtils.parseDate(beginTime,"yyyy-MM-dd HH:mm:ss"));
        String endTime= processProxy.getEndTime()+" 23:59:59" ;
        processProxy.setProxyEndTime(DateFormatUtils.parseDate(endTime,"yyyy-MM-dd HH:mm:ss"));
        try{
            processproxyDao.insert(processProxy);
        }catch (Exception e){
           logger.error(e);
           throw  new BusinessException("新增代理人失败!",e);
        }
    }

    /**
     * 删除代理人信息
     * @param processProxy
     */
    public void deleteProcessProxyById(ProcessProxy processProxy) {
        try{
            processproxyDao.delete(processProxy) ;
        }catch (Exception e){
            logger.error(e);
            throw  new BusinessException("删除代理人失败!",e);
        }
    }
    
    public BaseDao<ProcessProxy, String> getDao() {
        return processproxyDao;
    }
}
