package com.jd.oa.process.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessNodeDao;
import com.jd.oa.process.dao.ProcessNodeListenerDao;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeListener;

@Component("processNodeListenerDao")
public class ProcessNodeListenerDaoImpl extends MyBatisDaoImpl<ProcessNodeListener, String>implements ProcessNodeListenerDao  {

   

}
