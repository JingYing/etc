package com.jd.oa.process.controller;

import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.paf.model.PafResult;
import com.jd.oa.common.paf.model.ProcessInstance;
import com.jd.oa.common.paf.model.TaskInstance;
import com.jd.oa.common.paf.service.PafService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.system.service.SysUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="/process")
public class ProcessAppController {
	
	private static final Logger logger = Logger.getLogger(ProcessAppController.class);
	
	@Autowired
	private OaPafService oaPafService;
	@Autowired
	private PafService pafService;
	@Autowired
	private ProcessInstanceService processInstanceService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private ProcessTaskService processTaskService;
	@Autowired
	private FormBusinessTableService formBusinessTableService;
	@Autowired
	private ProcessTaskHistoryService processTaskHistoryService;
	@Autowired
	private ProcessNodeService processNodeService;
	
//	@Autowired
//	private PafService pafService;
	
    /**
     * 流程DEMO初始化页面
     * @return 流程实例ID
     */
    @RequestMapping(value = "/processApp_index", method = RequestMethod.GET)
    public ModelAndView processAppIndex(){
        ModelAndView mav = new ModelAndView("/process/processApp_index");
        return mav;
    }
    
    /**
     * 流程DEMO初始化页面
     * @return 流程实例ID
     */
    @RequestMapping(value = "/processApp_getTaskInit", method = RequestMethod.GET)
    public ModelAndView processAppGetTask(){
        ModelAndView mav = new ModelAndView("/process/processApp_getTask");
        return mav;
    }
    
    /**
     * 创建流程实例
     * @param processDefinitionKey 流程KEY
     * @return 流程实例ID
     */
    @RequestMapping(value = "/processApp_createProcessInstance", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
	public String processAppCreateProcessInstance(String processDefinitionKey,String businessObjectId){
        ProcessInstance result =  oaPafService.createProcessInstance(processDefinitionKey, businessObjectId);        
        return result.getId();
	}
    

    /**
     * 获取待办任务列表
     * @param processDefinitionKey 流程KEY
     * @return 待办任务列表
     */
    @RequestMapping(value = "/processApp_getTask", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
	public List<TaskInstance> processAppGetTask(String processDefinitionKey){
        PafResult<List<TaskInstance>> result = pafService.taskQuery(ComUtils.getLoginNamePin(), processDefinitionKey, null);
        List<TaskInstance> taskInstanceList = result.getResult();
		return taskInstanceList;
	}

    /**
     * 推进流程实例
     * @param taskInstanceId 任务实例ID
     * @return 执行结果
     */
    @RequestMapping(value = "/processApp_completeTask", method = RequestMethod.POST, produces = "application/json")
    public Boolean processAppCompleteTask(String taskInstanceId){
        Map map = new HashMap<String,String>();
        Boolean result = oaPafService.completeUserTask("", taskInstanceId, map);
        
        return result;
    }
    
    
    
//    /**
//     * 创建流程实例
//     * @param processDefinitionKey 流程KEY
//     * @return 流程实例ID
//     */
//    @RequestMapping(value = "/processApp_createProcessInstance", method = RequestMethod.POST, produces = "application/json")
//    @ResponseBody
//	public String processAppCreateProcessInstance(String processDefinitionKey){
//        PafResult<ProcessInstance> result =  pafService.createProcessInstance(ComUtils.getLoginNamePin(), processDefinitionKey, null);
//        if(!result.isSuccess()){
//            throw new RuntimeException(result.getErrorCode()+" : "+result.getLocalizedMessage());
//        }
//        return result.getResult().getId();
//	}
//    
//    /**
//     * 创建流程实例并推进第一个任务
//     * @param processDefinitionKey 流程KEY
//     * @return 流程实例ID
//     */
//    @RequestMapping(value = "/processApp_submitInstance", method = RequestMethod.POST, produces = "application/json")
//    @ResponseBody
//	public String processAppSubmitInstance(String processDefinitionKey){
//        PafResult<ProcessInstance> result =  pafService.submitHumanProcessInstance(ComUtils.getLoginNamePin(), processDefinitionKey, null, null);
//        if(!result.isSuccess()){
//            throw new RuntimeException(result.getErrorCode()+" : "+result.getLocalizedMessage());
//        }
//        return result.getResult().getId();
//	}
//
//    /**
//     * 获取待办任务列表
//     * @param processDefinitionKey 流程KEY
//     * @return 待办任务列表
//     */
//    @RequestMapping(value = "/processApp_getTask", method = RequestMethod.POST, produces = "application/json")
//    @ResponseBody
//	public List<TaskInstance> processAppGetTask(String processDefinitionKey){
//
//        List<TaskInstance> taskInstanceList = null;
//        if(processDefinitionKey!=null && !processDefinitionKey.equals("")){
//            Map map = new HashMap<String,String>();
//            map.put("start","0");
//            map.put("size","100");
//
//        	PafResult<List<TaskInstance>>  result = pafService.taskQuery(ComUtils.getLoginNamePin(), processDefinitionKey, map);
//            if(!result.isSuccess()){
//                throw new RuntimeException(result.getErrorCode()+" : "+result.getLocalizedMessage());
//            }
//            taskInstanceList = result.getResult();
//        }
//		return taskInstanceList;
//	}
//
//    /**
//     * 推进流程实例
//     * @param taskInstanceId 任务实例ID
//     * @return 执行结果
//     */
//    @RequestMapping(value = "/processApp_completeTask", method = RequestMethod.POST, produces = "application/json")
//    public Boolean processAppCompleteTask(String taskInstanceId){
//        Map map = new HashMap<String,String>();
//        PafResult<Boolean> result =  pafService.completeUserTask(ComUtils.getLoginNamePin(), taskInstanceId, map);
//        if(!result.isSuccess()){
//            throw new RuntimeException(result.getErrorCode()+" : "+result.getLocalizedMessage());
//        }
//        return result.getResult();
//    }
	
}
