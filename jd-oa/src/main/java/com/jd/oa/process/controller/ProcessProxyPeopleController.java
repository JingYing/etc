package com.jd.oa.process.controller;
import com.jd.common.util.DateFormatUtils;
import com.jd.common.web.LoginContext;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.form.execute.RuntimeFormManager;
import com.jd.oa.process.model.ProcessProxy;
import com.jd.oa.process.service.ProcessProxyService;
import com.jd.oa.system.service.SysUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-17
 * Time: 下午1:18
 * 代理人
 */
@Controller
@RequestMapping("/process")
public class ProcessProxyPeopleController {
    private static final Logger logger = Logger.getLogger(ProcessProxyPeopleController.class);

    @Autowired
    private ProcessProxyService processProxyService;
    @Autowired
    private SysUserService sysUserService;
    /**
     * 代理人
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/processProxy_proxy_index",method = RequestMethod.GET)
    public ModelAndView proxyIndex(String count) throws Exception {
        ModelAndView mav = new ModelAndView("/process/processProxy/processProxy_index");
        mav.addObject("today", DateFormatUtils.formatDate(new Date(),"yyyy-MM-dd"));
        mav.addObject("count",count);
        return mav;
    }

    /**
     * 代办人信息保存
     * @param processProxy
     */
    @RequestMapping(value="/processProxy_addSave",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Map<String,Object> saveProcessProxy(ProcessProxy processProxy){
        Map<String,Object>  map = new HashMap<String,Object>();
        String systemUserID=sysUserService.getUserIdByUserName(LoginContext.getLoginContext().getPin(),"1");
        if(StringUtils.isNotEmpty(systemUserID)){
            if (systemUserID.equals(processProxy.getProxyId()))  {
                map.put("success",false);
                map.put("message","不能指定自己为代理人!");
            }else{
                try
                {
                    processProxy.setAssignerId(systemUserID);
                    processProxyService.insertProcessProxyInfo(processProxy);
                    map.put("success",true);
                    map.put("message","添加代理人成功!");
                }catch (Exception e){
                    logger.error(e);
                    map.put("success",false);
                    map.put("message","添加代理人失败!");
                    throw new BusinessException("添加代理人失败!",e);
                }
            }
        }
        return map;
    }

    /**
     * 代办人信息查询
     * @param request
     * @return
     */
    @RequestMapping(value="/processProxy_findList",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Map<String, Object> findList(HttpServletRequest request){
        //创建pageWrapper
        PageWrapper<ProcessProxy> pageWrapper = new PageWrapper<ProcessProxy>(request);
        String systemUserID=sysUserService.getUserIdByUserName(LoginContext.getLoginContext().getPin(),"1");
        //添加搜索条件
        pageWrapper.addSearch("assignerId",systemUserID);
        //后台取值
        try{
            processProxyService.find(pageWrapper.getPageBean(),"findProcessProxysByUserId",pageWrapper.getConditionsMap());
        }catch(Exception e){
            logger.error(e);
            throw new BusinessException("代理人列表查询失败!",e);
        }
        return pageWrapper.getResult();
    }

    /**
     * 获取相同条件的代理人数量
     * @param processProxy
     * @return
     */
    @RequestMapping(value="/processProxy_getCount",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public int getCount(ProcessProxy processProxy){
        String systemUserID=sysUserService.getUserIdByUserName(LoginContext.getLoginContext().getPin(),"1");
        processProxy.setAssignerId(systemUserID);
        return processProxyService.getCount(processProxy);
    }
    /**
     * 删除代理人信息
     * @param processProxy
     */
    @RequestMapping(value="/processProxy_delete",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public void deleteProcessProxy(ProcessProxy processProxy){
        processProxyService.deleteProcessProxyById(processProxy);
    }

    /**
     * @Description: 进入到查询分配角色主页面
     * @author yaohaibin
     */
    @RequestMapping(value = "/processProxy_getOrgUser", method = RequestMethod.GET)
    public ModelAndView getOrgUser(){
        ModelAndView mav = new ModelAndView("process/processProxy/processProxy_tree");
        return mav;
    }

    /**
     *获取代理人数量
     * @return
     */
    @RequestMapping(value="/processProxy_getProxyCount",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object getCount(){
        ProcessProxy processProxy = new ProcessProxy();
        String systemUserID=sysUserService.getUserIdByUserName(LoginContext.getLoginContext().getPin(),"1");
        processProxy.setAssignerId(systemUserID);
        List<ProcessProxy> list=processProxyService.findList(processProxy);
        return list.size();
    }
}
