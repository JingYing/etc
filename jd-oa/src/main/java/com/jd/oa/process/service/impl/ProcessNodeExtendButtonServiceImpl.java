package com.jd.oa.process.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.process.dao.ProcessNodeExtendButtonDao;
import com.jd.oa.process.model.ProcessNodeExtendButton;
import com.jd.oa.process.service.ProcessNodeExtendButtonService;

@Service("processNodeExtendButtonService")
@Transactional
public class ProcessNodeExtendButtonServiceImpl extends BaseServiceImpl<ProcessNodeExtendButton, String>
		implements ProcessNodeExtendButtonService {
	
	@Autowired
	private	ProcessNodeExtendButtonDao processNodeExtendButtonDao;
	
	@Autowired
	private FormItemService formItemService;
	
	public ProcessNodeExtendButtonDao getDao(){
		return processNodeExtendButtonDao;
	}
	
	/**
	 * 根据条件查询结点扩展按钮配置
	 */
	public List<ProcessNodeExtendButton> selectByCondition(ProcessNodeExtendButton processNodeExtendButton){
		return processNodeExtendButtonDao.selectByCondition(processNodeExtendButton);
	}
	
	/**
	 * 自动创建标准按钮
	 */
	public void createStandardButtons(String formId,String processId,String nodeId){
		if(null != formId){
			List<FormBean> subForms = formItemService.getFormAndItemsByFormId(formId, SystemConstant.FORM_TYPE_S);
			if(null != subForms && !subForms.isEmpty()){
				for(FormBean subForm: subForms){
					if(null != subForm){
						insertNewButtonRecord(subForm.getFormId(),processId,nodeId,subForm.getFormCode());
						insertSaveButtonRecord(subForm.getFormId(),processId,nodeId,subForm.getFormCode());
						insertDeleteButtonRecord(subForm.getFormId(),processId,nodeId,subForm.getFormCode());
						insertRefreshButtonRecord(subForm.getFormId(),processId,nodeId,subForm.getFormCode());
					}
				}
			}
		}
	}
	
	/**
	 * 创建新增按钮
	 * 
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @param formCode
	 */
	private void insertNewButtonRecord(String formId,String processId,String nodeId,String formCode){
		if(isHad(formId,processId,nodeId,"新建") > 0){
			return;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("新建");
		record.setButtonExegesis("新建");
		record.setFunctionName("subSheetNew_" + formCode + "('" + formCode + "');return false;");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processId);
		record.setFormId(formId);
		record.setIsExtendButton(0);
		record.setStatus("0");
		record.setYn(0);
		record.setOrderNo(1);
		processNodeExtendButtonDao.insert(record);
		
	}
	
	/**
	 * 创建保存按钮
	 * 
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @param formCode
	 */
	private void insertSaveButtonRecord(String formId,String processId,String nodeId,String formCode){
		if(isHad(formId,processId,nodeId,"保存") > 0){
			return;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("保存");
		record.setButtonExegesis("保存");
		record.setFunctionName("subSheetSave('" + formCode + "')");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processId);
		record.setFormId(formId);
		record.setIsExtendButton(0);
		record.setStatus("0");
		record.setYn(0);
		record.setOrderNo(2);
		processNodeExtendButtonDao.insert(record);
	}
	
	/**
	 * 创建删除按钮
	 * 
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @param formCode
	 */
	private void insertDeleteButtonRecord(String formId,String processId,String nodeId,String formCode){
		if(isHad(formId,processId,nodeId,"删除") > 0){
			return;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("删除");
		record.setButtonExegesis("删除");
		record.setFunctionName("subSheetDelete('" + formCode + "')");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processId);
		record.setFormId(formId);
		record.setIsExtendButton(0);
		record.setStatus("0");
		record.setYn(0);
		record.setOrderNo(3);
		processNodeExtendButtonDao.insert(record);
	}
	
	/**
	 * 创建刷新按钮
	 * 
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @param formCode
	 */
	private void insertRefreshButtonRecord(String formId,String processId,String nodeId,String formCode){
		if(isHad(formId,processId,nodeId,"刷新") > 0){
			return;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("刷新");
		record.setButtonExegesis("刷新");
		record.setFunctionName("subSheetRefresh('" + formCode + "')");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processId);
		record.setFormId(formId);
		record.setIsExtendButton(0);
		record.setStatus("0");
		record.setYn(0);
		record.setOrderNo(4);
		processNodeExtendButtonDao.insert(record);
	}
	
	/**
	 * 判断是否存在标准按钮
	 * 
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @param buttonName
	 * @return
	 */
	private int isHad(String formId,String processId,String nodeId,String buttonName){
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processId);
		record.setFormId(formId);
		record.setButtonName(buttonName);
		record.setIsExtendButton(0);
		List<ProcessNodeExtendButton> records = processNodeExtendButtonDao.selectByCondition(record);
		if(records == null){
			return 0;
		}
		return records.size();
	}

}
