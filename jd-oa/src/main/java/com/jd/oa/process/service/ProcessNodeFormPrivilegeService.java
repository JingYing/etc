package com.jd.oa.process.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;

/**
 * 节点表单权限配置Service接口类
 * User: zhaoming
 * Date: 13-9-12
 * Time: 下午4:26
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessNodeFormPrivilegeService {

    /**
     * 根据主键查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置实体类
     */
    public ProcessNodeFormPrivilege selectByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置列表
     */
    public List<ProcessNodeFormPrivilege> selectByCondition(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 新增节点表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void insertProcessNodeFormPrivilege(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 保存节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体对象
     * @param processNodeFormPrivileges 节点表单权限配置JSON
     * @param processNode 流程节点实体对象
     */
    public void saveProcessNodeFormPrivileges(ProcessNodeFormPrivilege processNodeFormPrivilege, String processNodeFormPrivileges, ProcessNode processNode);

    /**
     * 根据主键修改表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void updateByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 根据主键删除节点表单权限配置（逻辑删除）
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void deleteByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege);
    /**
     * 运行时刻表单权限
     * @param request
     * @param processNodeFormPrivileges 流程节点对象的表单权限对象
     * @param formTemplateId 表单模板ID
     * @param formId 表单ID
     * @return
     */
    public String getTemplateContextRuntime(HttpServletRequest request,List<ProcessNodeFormPrivilege> processNodeFormPrivileges,String formTemplateId,String formId);
   
    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置数
     */
    public Integer countByCondition(ProcessNodeFormPrivilege processNodeFormPrivilege);
}
