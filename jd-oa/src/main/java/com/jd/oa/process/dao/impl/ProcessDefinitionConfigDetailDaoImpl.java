package com.jd.oa.process.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessDefinitionConfigDetailDao;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;


@Component("processDefinitionConfigDetailDao")
public class ProcessDefinitionConfigDetailDaoImpl extends MyBatisDaoImpl<ProcessDefinitionConfigDetail, String> implements ProcessDefinitionConfigDetailDao{
	
	public List<ProcessDefinitionConfigDetail> findByMap(String processDefinitionId, String configItem, String configType) {
		Map<String,String> mp = new HashMap<String,String>();
		mp.put("processDefinitionId", processDefinitionId);
		mp.put("configItem", configItem);
		mp.put("configType", configType);		
		return this.find("findSubSheetItemByMap", mp);		 
	}
}
