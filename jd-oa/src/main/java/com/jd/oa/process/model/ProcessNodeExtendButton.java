package com.jd.oa.process.model;

import java.util.Date;

import com.jd.oa.form.model.Form;

public class ProcessNodeExtendButton {
	
	
	private String id;
	
	/**
     * 流程定义ID
     */
	private String processDefinitionId;
	
	/**
     * 节点ID
     */
	private String nodeId;
	
	/**
     * 表单ID
     */
	private String formId;
	
	/**
	 * 是否扩展按钮
	 */
	private Integer isExtendButton;
	
	/**
	 * 操作按钮名称
	 */
	private String buttonName;
	
	/**
	 * 操作按钮注释
	 */
	private String buttonExegesis;
	
	/**
	 * function名称
	 */
	private String functionName;
	
	/**
	 * 状态
	 */
	private String status;
	
	/**
	 * 排序码
	 */
	private Integer orderNo;
	
	/**
	 * 逻辑删除
	 */
	private int yn;
	
	private String creator;
	
	private Date createTime;
	
	private String modifier;
	
	private Date modifyTime;
	
	private Form form;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public Integer getIsExtendButton() {
		return isExtendButton;
	}

	public void setIsExtendButton(Integer isExtendButton) {
		this.isExtendButton = isExtendButton;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}

	public String getButtonExegesis() {
		return buttonExegesis;
	}

	public void setButtonExegesis(String buttonExegesis) {
		this.buttonExegesis = buttonExegesis;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

	public int getYn() {
		return yn;
	}

	public void setYn(int yn) {
		this.yn = yn;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

}
