package com.jd.oa.process.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessProxy;
import com.jd.oa.process.model.ProcessSupervise;

public interface ProcessSuperviseDao extends BaseDao<ProcessSupervise, String> {
	 
	/**
     * 根据督办人ID获取流程流程实例ID（代表已督办过的流程）
     * @param userId
     * @return
     */
    public List<ProcessSupervise> findProcessInstanceByUserId(String userId);
}
