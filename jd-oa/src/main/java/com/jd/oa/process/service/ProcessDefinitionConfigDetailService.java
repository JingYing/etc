package com.jd.oa.process.service;


import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;

public interface ProcessDefinitionConfigDetailService extends BaseService<ProcessDefinitionConfigDetail, String>{
	/**
	 * 
	 * @desc 获取流程配置子项
	 * @author WXJ
	 * @date 2014-6-5 下午12:22:32
	 *
	 * @param processDefinitionId
	 * @param configItem
	 * @param configType
	 * @return
	 */
	public List<ProcessDefinitionConfigDetail> findByMap(String processDefinitionId, String configItem, String configType);
	
	/**
	 * 
	 * @desc 获得办理人变量
	 * @author WXJ
	 * @date 2013-12-12 下午04:44:39
	 *
	 * @param configId
	 * @param businessData
	 * @return
	 */
	public String getSubmitUsers(String configId, Map<String,Object> businessData);
	
	/**
	 * 
	 * @desc 获得流程定义属性扩展ID
	 * @author WXJ
	 * @date 2013-12-12 下午03:46:25
	 *
	 * @param configId
	 * @param businessData
	 * @return
	 */
	public String getConfigDetailId(String configId, Map<String,Object> businessData);
	
}
