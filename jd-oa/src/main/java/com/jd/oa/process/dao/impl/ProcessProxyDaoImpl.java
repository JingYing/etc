package com.jd.oa.process.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessProxyDao;
import com.jd.oa.process.model.ProcessProxy;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-17
 * Time: 下午4:33
 * 代办人
 */
@Repository("processProxyDao")
public class ProcessProxyDaoImpl  extends MyBatisDaoImpl<ProcessProxy, String> implements ProcessProxyDao {
    /**
     * 根据办理人ID查询代办信息
     * @param processProxy
     * @return
     */
    public List<ProcessProxy> findProcessProxysByUserId(ProcessProxy processProxy) {
        return this.getSqlSession().selectList("com.jd.oa.process.model.ProcessProxy.findProcessProxysByUserId",processProxy);
    }

    /**
     * 获取相同条件的代办人数量
     * @param processProxy
     * @return
     */
    public Integer getCount(ProcessProxy processProxy) {
        return  this.getSqlSession().selectOne("com.jd.oa.process.model.ProcessProxy.getCount",processProxy);
    }
    
    /**
     * 根据代理人ID查询对应的办理人
     * @param processProxy
     * @return
     */
   public List<ProcessProxy> findListByproxyId(String proxyId,String byProxyUserId) {
	   Map<String,Object> mapParms = new HashMap<String, Object>();
	   mapParms.put("proxyId", proxyId);
	   mapParms.put("assingerId", byProxyUserId);
	   return this.getSqlSession().selectList("com.jd.oa.process.model.ProcessProxy.findAssigneIdByProxyId", mapParms);
   }
   
   @Override
   public List<ProcessProxy> findByAssigneeId(String assigneeId)	{
	   return getSqlSession().selectList(ProcessProxy.class.getName() + ".findByAssigneeId", assigneeId);
   }
   
}
