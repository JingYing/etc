package com.jd.oa.process.dao;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeListener;

public interface ProcessNodeListenerDao extends BaseDao<ProcessNodeListener ,String>{

    /**
     * 查询存在上级节点的配置的数量
     * @param processNode 流程节点对象
     * @return 数量
     */

}
