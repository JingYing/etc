package com.jd.oa.process.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessTypeDao;
import com.jd.oa.process.model.ProcessType;

@Component("processTypeDao")
public class ProcessTypeDaoImpl extends MyBatisDaoImpl<ProcessType, String> implements ProcessTypeDao{
	
	@Override
	public List<ProcessType> findByPid(String pid) {
		return getSqlSession().selectList(ProcessType.class.getName() + ".findByPid", pid);
	}

}
