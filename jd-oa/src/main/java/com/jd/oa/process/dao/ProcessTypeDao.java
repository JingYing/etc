package com.jd.oa.process.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessType;

public interface ProcessTypeDao extends BaseDao<ProcessType, String> {

	List<ProcessType> findByPid(String pid);

}
