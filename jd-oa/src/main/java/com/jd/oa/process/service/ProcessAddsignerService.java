package com.jd.oa.process.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessAddsigner;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessType;

public interface ProcessAddsignerService extends BaseService<ProcessAddsigner, String>{
	/**
     * 
     * @desc 判断是否是转发任务
     * @author WXJ
     * @date 2013-10-30 下午09:09:17
     *
     * @param processInstanceId
     * @param taskId
     * @param nodeId
     * @return
     */
    public ProcessAddsigner getAddSignTask(String processInstanceId, String taskId, String nodeId);
}
