package com.jd.oa.process.service.impl;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessNodeDao;
import com.jd.oa.process.dao.ProcessNodeListenerDao;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeListener;
import com.jd.oa.process.service.ProcessNodeListenerService;
import com.jd.oa.process.service.ProcessNodeService;

import org.jboss.netty.bootstrap.Bootstrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service("processNodeListenerService")
@Transactional
public class ProcessNodeListenerServiceImpl extends BaseServiceImpl<ProcessNodeListener, String> implements ProcessNodeListenerService{
	@Autowired
	private ProcessNodeListenerDao processNodeListenerDao;
	
	public ProcessNodeListenerDao getDao(){
		return processNodeListenerDao;
	}
	
	public List<ProcessNodeListener> getBeforeEvents(String processDefinitionId,String nodeId ){
		ProcessNodeListener listener = new ProcessNodeListener();
		listener.setNodeId(nodeId);
		listener.setProcessDefinitionId(processDefinitionId);
		listener.setEvent("b");
		listener.setYn(0);
		return find(listener);
	}
	
	public List<ProcessNodeListener>  getEndEvents(String processDefinitionId,String nodeId){
		ProcessNodeListener listener = new ProcessNodeListener();
		listener.setNodeId(nodeId);
		listener.setProcessDefinitionId(processDefinitionId);
		listener.setEvent("e");
		listener.setYn(0);
		return find(listener);
				
	}

    public List<ProcessNodeListener>  getCallbackEvents(String processDefinitionId,String nodeId){
        ProcessNodeListener listener = new ProcessNodeListener();
        listener.setNodeId(nodeId);
        listener.setProcessDefinitionId(processDefinitionId);
        listener.setEvent("r");
        listener.setYn(0);
        return find(listener);

    }
	
	
}
