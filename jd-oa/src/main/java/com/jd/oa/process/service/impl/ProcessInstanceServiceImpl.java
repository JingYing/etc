package com.jd.oa.process.service.impl;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessInstanceDao;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessInstanceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("processInstanceService")
@Transactional
public class ProcessInstanceServiceImpl extends BaseServiceImpl<ProcessInstance, String> implements ProcessInstanceService{
	@Autowired
	private ProcessInstanceDao processInstanceDao;

	@Override
	public BaseDao<ProcessInstance, String> getDao() {
		// TODO Auto-generated method stub
		return processInstanceDao;
	}

	@Override
	public List<ProcessInstance> findProcessInstanceByKey(String processDefKey) {
		// TODO Auto-generated method stub
		return processInstanceDao.findProcessInstanceByKey(processDefKey);
	}
	
	public List<String> findAllInstanceByStartId(String starterId, List<String> statusList) {
		return processInstanceDao.findAllInstanceByStartId(starterId, statusList);
	}
	
    /**
     * 根据流程实例ID查询流程实例信息
     * @param processInstance 流程实例实体类
     * @return 流程实例信息
     */
    public ProcessInstance selectByProcessInstanceId(ProcessInstance processInstance){
        return processInstanceDao.selectByProcessInstanceId(processInstance);
    }

    /**
     * 取消流程实例
     * @param processInstance 流程实例实体类
     */
    public void cancelByProcessInstanceId(ProcessInstance processInstance){
        processInstanceDao.cancelByProcessInstanceId(processInstance);
    }
	
    /**
     * 根据流程实例ID查询流程实例信息(一次性获取多个)
     * @param processInstance 流程实例实体类
     * @return 流程实例信息
     */
    public List<ProcessInstance> selectByProcessInstanceIds(List<String> processInstanceIds){
    	return this.processInstanceDao.selectByProcessInstanceIds(processInstanceIds);
    }
}
