package com.jd.oa.process.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeListener;

public interface ProcessNodeListenerService extends BaseService<ProcessNodeListener, String>{
	public List<ProcessNodeListener> getBeforeEvents(String processDefinitionId,String nodeId );
	public List<ProcessNodeListener>  getEndEvents(String processDefinitionId,String nodeId);
    public List<ProcessNodeListener>  getCallbackEvents(String processDefinitionId,String nodeId);
}
