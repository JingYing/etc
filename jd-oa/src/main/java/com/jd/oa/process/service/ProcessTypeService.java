package com.jd.oa.process.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessType;

public interface ProcessTypeService extends BaseService<ProcessType, String>{
	String insertByProducingProcessTypeCode(ProcessType processType);
	
	//根据流程定义ID获取流程类别
	public ProcessType findByProcessDefId(String processDefId);

	List<ProcessType> findByPid(String pid);
	
}
