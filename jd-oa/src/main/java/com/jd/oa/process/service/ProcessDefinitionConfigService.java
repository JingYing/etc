package com.jd.oa.process.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessType;

public interface ProcessDefinitionConfigService extends BaseService<ProcessDefinitionConfig, String>{

}
