package com.jd.oa.process.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessTypeDao;
import com.jd.oa.process.model.ProcessType;
import com.jd.oa.process.service.ProcessTypeService;

@Service("processTypeService")
@Transactional
public class ProcessTypeServiceImpl extends BaseServiceImpl<ProcessType, String> implements ProcessTypeService{
	@Autowired
	private ProcessTypeDao processTypeDao;
	
	public ProcessTypeDao getDao(){
		return processTypeDao;
	}

	@Override
	public String insertByProducingProcessTypeCode(ProcessType processType) {
		Long code =(Long) get("findMaxProcessTypeCode",null);
		//Long code = Long.parseLong(temp.getProcessTypeCode());
		if(code==null)//数据库还没有任何数据
			code = 0l;
		code = code+1;
		processType.setProcessTypeCode(code.toString());
		return insert(processType);
		
		
	}
	
	@Override
	public ProcessType findByProcessDefId(String processDefinitionId) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("processDefinitionId", processDefinitionId);
		
		List<ProcessType> list = find("findByDefinitionId", map);
		
		if(list != null && list.size() > 0 )
			return list.get(0);
					
		
		return null;
	}

	@Override
	public List<ProcessType> findByPid(String pid) {
		return processTypeDao.findByPid(pid);
	}
	
}
