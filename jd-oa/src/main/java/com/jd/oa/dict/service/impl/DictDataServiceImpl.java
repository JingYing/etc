package com.jd.oa.dict.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.redis.annotation.CacheKey;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.redis.annotation.CachePut;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.dict.dao.DictDataDao;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;

/** 
 * @Description: 数据字典数据Service实现类
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0 
 */

@Service("dictDataService")
@CacheProxy
public class DictDataServiceImpl extends BaseServiceImpl<DictData, String>
		implements DictDataService {

    private static final Logger logger = Logger.getLogger(DictDataServiceImpl.class);

	@Autowired
	private DictDataDao dictDataDao;

	public DictDataDao getDao() {
		return dictDataDao;
	}
	
	/**
	  * @Description: 根据数据项ID查询信息
	  * @param dictDataId
	  * @return DictData
	  * @author guoqingfu
	  * @date 2013-8-30下午01:49:54 
	  * @version V1.0
	 */
	public DictData getDictData(String dictDataId){
        try{
            return dictDataDao.getDictData(dictDataId);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据ID获取数据项失败",e);
        }
	}
	/**
	  * @Description: 查询父编号（新增数据项时使用）
	  * @param dictTypeId
	  * @return List<DictData>
	  * @author guoqingfu
	  * @date 2013-8-30下午04:19:11 
	  * @version V1.0
	 */
	public List<DictData> findParentDictDataList(String dictTypeId){
        try{
            return dictDataDao.findParentDictDataList(dictTypeId);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询父数据编号失败",e);
        }
	}
	
	/**
	  * @Description: 根据字典类别编码查询数据项
	  * @param dictCode
	  * @return List<DictData>
	  * @author guoqingfu
	  * @date 2013-8-30下午07:51:46 
	  * @version V1.0
	 */
	@CachePut(key = @CacheKey(template = "JDOA/DictData/findDictDataListByDictCode_${p0}"), expire = 3600)
	public List<DictData> findDictDataList(String dictCode){
        try{
            return dictDataDao.findDictDataList(dictCode);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据字典类别编码获取数据项失败",e);
        }
	}

    /**
     * @Description: 根据数据项编码查询子数据项列表
     * @param pid
     * @return List<DictData>
     * @author guoqingfu
     * @date 2013-8-30下午07:51:46
     * @version V1.0
     */
	@CachePut(key = @CacheKey(template = "JDOA/DictData/findDictDataListByPid_${p0}"), expire = 3600)
    public List<DictData> findDictDataListByPid(String pid) {
        try{
            return dictDataDao.findDictDataListByPid(pid);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据数据项ID查询子数据项",e);
        }
    }

    /**
	  * @Description: 根据数据项ID查询数据项信息、父数据项、字典类别名称
	  * @param dictDataId
	  * @return DictData
	  * @author guoqingfu
	  * @date 2013-8-30下午08:58:27 
	  * @version V1.0
	 */
	public DictData getViewDictInfo(String dictDataId){
        try{
            return dictDataDao.getViewDictInfo(dictDataId);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据数据项ID获取数据项、数据类别信息失败",e);
        }
	}
	/**
	  * @Description: 逻辑删除数据项
	  * @param dictDataIds
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-8-31下午01:46:11 
	  * @version V1.0
	 */
	@Transactional
	public Map<String,Object> deleteDictData(List<String> dictDataIds){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag= false;
		map.put("dictDataIds", dictDataIds);
		
        try{
        	flag = dictDataDao.isOwnSubDataOrForm(map);
        	if(flag){
				map.put("operator", false);
	            map.put("message", "存在子数据项或该数据项已被表单引用,删除失败");
	            return map;
			}
        	flag =  dictDataDao.deleteDictData(map);
        	if(flag){
                map.put("operator", true);
                map.put("message", "删除成功");
            }else{
                map.put("operator", false);
                map.put("message", "删除失败");
            }
          return map;
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("逻辑删除数据项失败",e);
        }
	}
	/**
	  * @Description: 根据数据项编码查看数据是否存在
	  * @param dictDataCode
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-2下午06:03:23 
	  * @version V1.0
	 */
	public boolean isExistDictDataCode(DictData dictData){
        try{
            return dictDataDao.isExistDictDataCode(dictData);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据数据项编码查看数据是否存在，查询失败",e);
        }
	}
	
	/**
	 * 
	 * @author zhengbing 
	 * @desc  根据字典类别编码和父id查询数据项
	 * @date 2014年7月9日 下午1:45:15
	 * @return List<DictData>
	 */
	public List<DictData> findDictDataListByCodeAndParentId(Map map){
		try{
            return dictDataDao.findDictDataListByCodeAndParentId(map);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据字典类别编码和父id查询数据项",e);
        }
	}
}
