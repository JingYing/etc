package com.jd.oa.dict.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.dict.model.DictType;

/** 
 * @Description: 数据字典类别DAO
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0 
 */
public interface DictTypeDao extends BaseDao<DictType, String>{

	/**
	  * @Description: 获取父类别List
	  * @return List<DictType>
	  * @author guoqingfu
	  * @date 2013-8-27下午04:02:40 
	  * @version V1.0
	 */
	public List<DictType> findParentDictTypeList();
	/**
	  * @Description: 根据数据字典类别ID查询类别信息
	  * @param dicttypeId
	  * @return DictType
	  * @author guoqingfu
	  * @date 2013-8-27下午04:39:51 
	  * @version V1.0
	 */
	public DictType getDictType(String dicttypeId);
	/**
	  * @Description: 根据类别ID删除数据字典类别
	  * @param dicttypeId
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-8-27下午05:55:48 
	  * @version V1.0
	 */
	public boolean deleteDictType(String dicttypeId);
	/**
	  * @Description: 根据类别编码查看类别是否存在
	  * @param dictTypeCode
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-2上午09:54:32 
	  * @version V1.0
	 */
	public boolean isExistDictTypeCode(String dictTypeCode);
	
	/**
	 * @Description: 查看该类别下是否存在相同的类别名称
	  * @param dictType
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-11-12上午11:32:14 
	  * @version V1.0
	 */
	public boolean isExistDictTypeName(DictType dictType);
	
	/**
	  * @Description: 根据类别实体查询类别信息（如可通过ID，也可通过Code等进行查询）
	  * @param dictType
	  * @return DictType
	  * @author guoqingfu
	  * @date 2013-9-4下午03:30:48 
	  * @version V1.0
	 */
	public DictType getDictTypeByModel(DictType dictType);
	/**
	  * @Description: 根据类别ID查看是否存在子类别或是该类别下是否有数据
	  * @param dicttypeId
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-5上午09:02:33 
	  * @version V1.0
	 */
	public boolean isOwnSubTypeOrData(String dicttypeId);
	
	/**
	 * 
	 * @author zhengbing 
	 * @desc 根据codes查询类别信息list
	 * @date 2014年7月8日 下午5:37:52
	 * @return List<DictType>
	 */
	public List<DictType> getDictTypeCodes(List<String> dictTypeCodes);
	
	/**
	 * 
	 * @author zhengbing 
	 * @desc 根据父id递归查询类别tree
	 * @date 2014年7月9日 下午7:23:10
	 * @return DictType
	 */
	public List<DictType> getDictTypeChildren(String parentId);
}
