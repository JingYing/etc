package com.jd.oa.dict.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.dict.dao.DictTypeDao;
import com.jd.oa.dict.model.DictType;
import com.jd.oa.dict.service.DictTypeService;

/** 
 * @Description: 数据字典类别Service实现类
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0 
 */

@Service("dictTypeService")
public class DictTypeServiceImpl extends BaseServiceImpl<DictType, String>
		implements DictTypeService {
	
	private static final Logger logger = Logger.getLogger(DictTypeServiceImpl.class);
	
	@Autowired
	private DictTypeDao dictTypeDao;

	public DictTypeDao getDao() {
		return dictTypeDao;
	}

	/**
	  * @Description: 获取父类别List
	  * @return List<DictType>
	  * @author guoqingfu
	  * @date 2013-8-27下午04:02:40 
	  * @version V1.0
	 */
	public List<DictType> findParentDictTypeList(){
		try{
			return dictTypeDao.findParentDictTypeList();
		} catch (Exception e) {
			logger.error(e);
			throw new BusinessException("获取父类别列表失败",e);
		}
	}
	/**
	  * @Description: 根据数据字典类别ID查询类别信息
	  * @param dicttypeId
	  * @return DictType
	  * @author guoqingfu
	  * @date 2013-8-27下午04:39:51 
	  * @version V1.0
	 */
	public DictType getDictType(String dicttypeId){
		try{
			return dictTypeDao.getDictType(dicttypeId);
		}catch (Exception e) {
			logger.error(e);
			throw new BusinessException("查询类别信息失败",e);
		}
	}
	
	/**
	  * @Description: 根据类别ID删除数据字典类别
	  * @param dicttypeId
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-8-27下午05:55:48 
	  * @version V1.0
	 */
	@Transactional
	public Map<String,Object> deleteDictType(String dicttypeId){
		Map<String,Object> map = new HashMap<String,Object>();
		boolean flag= false;
		try{
			//根据类别ID查看是否存在子类别或是该类别下是否有数据
			flag = dictTypeDao.isOwnSubTypeOrData(dicttypeId);
			if(flag){
				map.put("operator", false);
	            map.put("message", "存在子类别或该类别下存在数据,删除失败");
	            return map;
			}
			flag = dictTypeDao.deleteDictType(dicttypeId);
			if(flag){
	            map.put("operator", true);
	            map.put("message", "删除成功");
	        }else{
	            map.put("operator", false);
	            map.put("message", "删除失败");
	        }		
			 return map;
		}catch (Exception e) {
			logger.error(e);
			throw new BusinessException("删除字典类别失败",e);
		}
	}
	/**
	 * 根据父ID查找子类别列表
	 * @param parentId 父ID
	 * @return 子资源列表
	 */
	public List<DictType> findByParentId(String parentId){
		try{
			return getDao().find("findByParentId", parentId);
		}catch (Exception e) {
			logger.error(e);
			throw new BusinessException("查询子类别列表失败",e);
		}
	}
	/**
	  * @Description: 根据类别编码查看类别是否存在
	  * @param dictTypeCode
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-2上午09:54:32 
	  * @version V1.0
	 */
	public Map<String,Object> isExistDictTypeCodeOrName(DictType dictType){
		Map<String,Object> map = new HashMap<String,Object>();
		boolean flag= false;
		
		try{
			flag = dictTypeDao.isExistDictTypeCode(dictType.getDicttypeCode());
			if(flag){
				map.put("operator", false);
	            map.put("message", "类别编码不能重复");
	            return map;
			}
			flag = dictTypeDao.isExistDictTypeName(dictType);
			if(flag){
	            map.put("operator", false);
	            map.put("message", "类别名称不能重复");
	        }else{
	            map.put("operator", true);
	            map.put("message", "可以新增类别");
	        }		
			 return map;
			
		}catch (Exception e) {
			logger.error(e);
			throw new BusinessException("查看类别是否存在失败",e);
		}
	}
	
	/**
	  * @Description: 根据类别实体查询类别信息（如可通过ID，也可通过Code等进行查询）
	  * @param dictType
	  * @return DictType
	  * @author guoqingfu
	  * @date 2013-9-4下午03:30:48 
	  * @version V1.0
	 */
	public DictType getDictTypeByModel(DictType dictType){
		try{
			return dictTypeDao.getDictTypeByModel(dictType);
		}catch (Exception e) {
			logger.error(e);
			throw new BusinessException("根据类别实体查询类别信息失败",e);
		}
	}

	/** 
	 * @author zhengbing 
	 * @desc 根据codes查询类别信息list
	 * @date 2014年7月8日 下午5:37:52
	 * @return List<DictType>
	 */
	public List<DictType> getDictTypeCodes(List<String> dictTypeCodes) {
		try{
			return dictTypeDao.getDictTypeCodes(dictTypeCodes);
		}catch (Exception e) {
			logger.error(e);
			throw new BusinessException("根据类别编码集合查询类别信息失败",e);
		}
	}
	
	/**
	 * 
	 * @author zhengbing 
	 * @desc 根据父id递归查询类别tree
	 * @date 2014年7月9日 下午7:23:10
	 * @return DictType
	 */
	public List<DictType> getDictTypeChildren(String parentId){
		try{
			return dictTypeDao.getDictTypeChildren(parentId);
		}catch (Exception e) {
			logger.error(e);
			throw new BusinessException("查询子类别列表失败",e);
		}
	}
}
