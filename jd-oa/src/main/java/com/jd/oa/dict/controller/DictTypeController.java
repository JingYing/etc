package com.jd.oa.dict.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.tree.TreeNode;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.dict.model.DictType;
import com.jd.oa.dict.service.DictTypeService;
import com.jd.oa.system.model.SysResource;

/**
 * @Description: 数据字典类别Controller
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0
 */

@Controller
@RequestMapping(value="/dict")
public class DictTypeController {
    private static final Logger logger = Logger.getLogger(DictTypeController.class);

    @Autowired
    private DictTypeService dictTypeService;

    /**
     * 字典类别树异步加载
     * @param dictType 数据字典类别
     * @throws Exception
     */
    @RequestMapping(value = "/dictType_load", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<TreeNode> sysResourceLoad(DictType dictType) {
    	try{
    		List<DictType> dictTypeList =  dictTypeService.findByParentId("null".equals(dictType.getId())?"":dictType.getId());
    	    List<TreeNode> listData= this.getTreeNodes(dictTypeList);
    	    return listData;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("字典类别树异步加载失败",e);
        }
    }

    /**
     * 字典类别树数据封装
     * @param dictTypes
     * @return
     */
    private List<TreeNode> getTreeNodes(List<DictType> dictTypes) {
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        for (DictType dictType : dictTypes) {
            TreeNode treeNode = new TreeNode();
            treeNode.setIsParent(true);
            treeNode.setId(dictType.getId());
            treeNode.setpId(dictType.getParentId() != null ? dictType.getParentId()
                    : "null");
            treeNode.setName(dictType.getDicttypeName());
            treeNode.setIconSkin(SysResource.class.getSimpleName().toLowerCase());
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("id", dictType.getId());
            props.put("parentId", dictType.getParentId());
            treeNode.setProps(props);
            treeNodes.add(treeNode);
        }
        return treeNodes;
    }


    /**
     * @Description: 进入到查询数据字典类别页面
     * @return
     * @throws Exception String
     * @author guoqingfu
     * @date 2013-8-27下午03:29:58
     * @version V1.0
     */
    @RequestMapping(value = "/dictType_index", method = RequestMethod.GET)
    public ModelAndView list()
            throws Exception {
        ModelAndView mav = new ModelAndView("dict/dicttype_index");
        return mav;
    }

    /**
     * @Description: 进入新增数据字典类别页面
     * @param id 数据类别id
     * @return String
     * @author guoqingfu
     * @date 2013-8-27下午03:49:05
     * @version V1.0
     */
    @RequestMapping(value = "/dictType_add",method=RequestMethod.GET)
    public ModelAndView addDictType(String id) {
        ModelAndView mav = new ModelAndView("dict/dicttype_add");
        DictType dictType = dictTypeService.getDictType(id);
        if(null == dictType) {
            mav.addObject("root", "1");
        }
        mav.addObject("dictType", dictType);
        return mav;
    }
    /**
     *
     * @Description: 保存新增数据类别
     * @param dictType
     * @return String
     * @author guoqingfu
     * @date 2013-8-27下午03:57:03 
     * @version V1.0
     */
    @RequestMapping(value="/dictType_addSave" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String saveAddDictType(DictType dictType){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
        	//获取操作人姓名
            String userName = ComUtils.getLoginName();
            dictType.setCreator(userName);
            dictType.setCreateTime(new Date());
            dictType.setModifier(userName);
            dictType.setModifyTime(new Date());
            dictType.setYn(0); //有效
            String primaryKey = dictTypeService.insert(dictType);
            if(StringUtils.isNotEmpty(primaryKey)){
    	 		map.put("operator", true);
    	        map.put("message", "添加成功");
    	 	}else{
    	 		map.put("operator", false);
    	        map.put("message", "添加失败");
    	 	}
            return JSONObject.fromObject(map).toString();
        }catch(Exception e){
        	logger.error(e);
			throw new BusinessException("保存新增数据字典类别失败",e);
        }
    }
    /**
     * @Description: 进入到修改页面的公共方法
     * @param dicttypeId
     * @return ModelAndView
     * @author guoqingfu
     * @date 2013-8-27下午04:46:16
     * @version V1.0
     */
    @RequestMapping(value = "/dictType_update",method=RequestMethod.GET)
    public ModelAndView updateDictType(String dicttypeId) {
        DictType dictType = dictTypeService.getDictType(dicttypeId);
        ModelAndView mav = new ModelAndView("dict/dicttype_update");
        mav.addObject("dictType",dictType);
        return mav;
    }
    /**
     * @Description: 进入到查看数据字典类别页面
     * @param dictTypeInfo
     * @return ModelAndView
     * @author guoqingfu
     * @date 2013-8-27下午04:46:16
     * @version V1.0
     */
    @RequestMapping(value = "/dictType_view",method=RequestMethod.GET)
    public ModelAndView viewDictType(DictType dictTypeInfo) {
        //根据类别实体查询类别信息（如可通过ID，也可通过Code等进行查询）
        DictType dictType = dictTypeService.getDictTypeByModel(dictTypeInfo);
        ModelAndView mav = new ModelAndView("dict/dicttype_view");
        mav.addObject("dictType",dictType);
        return mav;
    }
    /**
     * @Description: 保存修改数据字典类别
     * @param dictType
     * @return String
     * @author guoqingfu
     * @date 2013-8-27下午05:01:21
     * @version V1.0
     */
    @RequestMapping(value="/dictType_updateSave" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String saveEditDictType(DictType dictType){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
        	//获取操作人姓名
            String userName = ComUtils.getLoginName();
            dictType.setModifier(userName);
            dictType.setModifyTime(new Date());
            int i = dictTypeService.update(dictType);
            if(i>0){
            	map.put("operator", true);
     	        map.put("message", "修改成功");
            }else{
            	map.put("operator", false);
     	        map.put("message", "修改失败");
            }
            return JSONObject.fromObject(map).toString();
        }catch(Exception e){
        	logger.error(e);
        	throw new BusinessException("保存修改数据字典类别失败",e);
        }
    }
    /**
     * @Description: 删除数据字典类别
     * @param dicttypeId
     * @return String
     * @author guoqingfu
     * @date 2013-8-27下午06:20:48
     * @version V1.0
     */
    @RequestMapping(value="/dictType_delete" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String deleteDictType(String dicttypeId){
        Map<String,Object> map  = dictTypeService.deleteDictType(dicttypeId);
        return JSONObject.fromObject(map).toString();
    }
    /**
      * @Description: 根据类别编码查看类别是否存在
      * @param dictTypeCode
      * @return String
      * @author guoqingfu
      * @date 2013-9-2上午10:09:58 
      * @version V1.0
     */
    @RequestMapping(value="/dictType_isExistDictTypeCode",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String isExistDictTypeCode(DictType dictType){
    	Map<String,Object> map = dictTypeService.isExistDictTypeCodeOrName(dictType);
    	
    	return JSONObject.fromObject(map).toString();
//        boolean flag = dictTypeService.isExistDictTypeCode(dictTypeCode);
//        return flag;
    }
    
}
