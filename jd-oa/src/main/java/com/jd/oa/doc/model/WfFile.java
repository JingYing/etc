package com.jd.oa.doc.model;

import java.util.Date;
import java.util.List;

import com.jd.oa.common.model.IdModel;
import com.jd.oa.system.model.SysAuthExpression;

public class WfFile  extends IdModel {

    /**
     * 文件夹ID
     */
    private String folderId;

    /**
     * 文件夹名称
     */
    private String folderName;

    /**
     * 桶名
     */
    private String bucket;

    /**
     * 文件KEY
     */
    private String fileKey;

    /**
     * 文件类型
     */
    private String fileType;

   /**
    * 文件名称
    */
    private String fileName;

    /**
     * 文件内容
     */
    private String fileContent;

	 /**
	  * 文件后缀名
	  */
    private String fileSuffix;

    /**
     * 文件大小
     */
    private Long fileSize;

   /**
    * 创建人
    */
    private String creator;

   /**
    * 创建时间
    */
    private Date createTime;

   /**
    * 修改人
    */
    private String modifier;

    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 逻辑删除
     */
    private int yn;
    /**
     * 权限表达式
     */
    private List<SysAuthExpression> list;
    /**
     * 权限表达式ID
     */
    private String authExpressionId;
    public String getAuthExpressionId() {
		return authExpressionId;
	}

	public void setAuthExpressionId(String authExpressionId) {
		this.authExpressionId = authExpressionId;
	}

	public List<SysAuthExpression> getList() {
		return list;
	}

	public void setList(List<SysAuthExpression> list) {
		this.list = list;
	}

	public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileSuffix() {
        return fileSuffix;
    }

    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getYn() {
        return yn;
    }

    public void setYn(int yn) {
        this.yn = yn;
    }
}