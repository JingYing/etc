package com.jd.oa.doc.model;

import com.jd.oa.common.model.IdModel;

import java.util.Date;

public class WfFolder extends IdModel {

	private static final long serialVersionUID = -1848151297023149167L;

	/**
	 * 父文件夹ID
	 */
    private String parentId;

    /**
     * 文件夹名称
     */
    private String folderName;

    /**
     * 文件夹描述
     */
    private String folderDesc;
   
    /**
     * JSS桶名
     */
    private String bucket;
    
   /**
    * 创建人
    */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 更新时间
     */
    private Date modifyTime;
    /**
     * 逻辑删除标识 有效：0 无效：1
     */
    private Integer yn;

	public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName == null ? null : folderName.trim();
    }

    public String getFolderDesc() {
        return folderDesc;
    }

    public void setFolderDesc(String folderDesc) {
        this.folderDesc = folderDesc == null ? null : folderDesc.trim();
    }
    
    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket == null ? null : bucket.trim();
    }
    
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getYn() {
        return yn;
    }

    public void setYn(Integer yn) {
        this.yn = yn;
    }
}