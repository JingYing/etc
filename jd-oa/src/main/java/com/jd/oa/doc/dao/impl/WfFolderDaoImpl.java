package com.jd.oa.doc.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.doc.dao.WfFolderDao;
import com.jd.oa.doc.model.WfFolder;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 文件夹管理Dao实现类
 * @author yaohaibin
 * @version 1.0
 */
@Repository("wfFolderDao")
public class WfFolderDaoImpl extends MyBatisDaoImpl<WfFolder, String> implements WfFolderDao {

    /**
     * 文件夹树数据加载
     * @param id 文件夹ID
     * @return 文件夹列表
     */
    @Override
    public List<WfFolder> selectFolderByParentId(String id) {
        return this.getSqlSession().selectList("com.jd.oa.doc.model.WfFolder.selectFolderByParentId", id);
    }

    /**
     * 获取同一节点下同名文件夹数量
     * @param wfFolder 文件夹对象
     * @return  文件夹对象
     */
    @Override
    public Integer findFolderCount(WfFolder wfFolder){
        return this.getSqlSession().selectOne("com.jd.oa.doc.model.WfFolder.findFolderCount", wfFolder);
    }

    /**
     * 创建文件夹
     * @param wfFolder 文件夹对象
     * @throws RuntimeException
     */
    @Override
    public void folderInsert(WfFolder wfFolder){
        this.getSqlSession().insert("com.jd.oa.doc.model.WfFolder.insert", wfFolder);
    }

    /**
     * 修改文件夹
     * @param wfFolder 文件夹对象
     */
	@Override
	public void updateByPrimaryKey(WfFolder wfFolder) {
		this.getSqlSession().update("com.jd.oa.doc.model.WfFolder.updateByPrimaryKey", wfFolder);
	}

    /**
     * 删除文件夹(逻辑删除)
     * @param folder 文件夹对象
     */
    @Override
    public void deleteFolderById(WfFolder folder) {
        this.getSqlSession().update("com.jd.oa.doc.model.WfFolder.deleteFolderById", folder);
    }

    /**
     * 删除文件夹(物理删除)
     * @param id 文件夹主键ID
     */
    @Override
    public void deleteByPrimaryKey(String id) {
        this.getSqlSession().update("com.jd.oa.doc.model.WfFolder.deleteByPrimaryKey", id);
    }

    /**
     * 文件夹查看页面
     * @param id 文件夹ID
     */
    @Override
    public WfFolder selectByPrimaryKey(String id) {
        return this.getSqlSession().selectOne("com.jd.oa.doc.model.WfFolder.selectByPrimaryKey", id);
    }

}
