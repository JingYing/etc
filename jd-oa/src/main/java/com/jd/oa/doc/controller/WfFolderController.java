package com.jd.oa.doc.controller;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.tree.TreeNode;
import com.jd.oa.doc.model.WfFolder;
import com.jd.oa.doc.service.WfFolderService;
import com.jd.oa.system.model.SysResource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件夹管理Controller
 * @author yaohaibin
 * @version 1.0
 * @date 2013-8-27
 */
@Controller
@RequestMapping(value="/doc")
public class WfFolderController {

	 private static final Logger logger = Logger.getLogger(WfFileController.class);

     @Autowired
     private WfFolderService wfFolderService;

     /**
      * 文件夹初始页面
      */
 	@RequestMapping(value = "/wffolder_index", method = RequestMethod.GET)
     public ModelAndView index(){	
 		ModelAndView mav = new ModelAndView("doc/folder/wffolder_index");
		return mav;
     }

    /**
     * 文件夹树数据加载
     * @param wfFolder 文件夹对象
     * @return 树节点列表
     */
    @RequestMapping(value = "/wffolder_treeLoad", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<TreeNode> treeLoad(WfFolder wfFolder){
        List<TreeNode> listData = null;
        try{
            List<WfFolder> folderList = wfFolderService.selectFolderByParentId(wfFolder.getId());
            listData = this.getTreeNodes(folderList);    //转化成Ztree支持的数据格式
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("文件夹树加载失败!",e);
        }
        return listData;
    }

    /**
     * 文件夹添加页面
     * @param wfFolder 文件夹对象
     */
    @RequestMapping(value = "/wffolder_add", method = RequestMethod.GET)
    public ModelAndView addWfFolder(WfFolder wfFolder){
        ModelAndView mav = new ModelAndView("doc/folder/wffolder_add");
        mav.addObject("wfFolder",wfFolder);
        return mav;
    }

    /**
     * 获取同一节点下同名文件夹数量
     * @param wfFolder 文件夹对象
     * @return  文件夹对象
     */
    @RequestMapping(value="/wffolder_getFolderCount", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Integer getFolderCount(WfFolder wfFolder){
        Integer folderCount = 0;
        try{
            folderCount = wfFolderService.findFolderCount(wfFolder);
        } catch (Exception e){
            logger.error(e);
            throw new BusinessException("获取同一节点下同名文件夹数量失败!",e);
        }
        return folderCount;
    }

    /**
     * 添加文件夹
     * @param wfFolder 文件夹对象
     * @return  文件夹对象
     */
    @RequestMapping(value="/wffolder_addSave", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public WfFolder addSaveWfFolder(WfFolder wfFolder){
        try {
             wfFolder = wfFolderService.folderInsert(wfFolder);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("文件夹创建失败!",e);
        }
        return wfFolder;
    }

    /**
     * 文件夹修改页面
     * @param wfFolder 文件夹对象
     */
    @RequestMapping(value = "/wffolder_update", method = RequestMethod.GET)
    public ModelAndView updateWfFolder(WfFolder wfFolder){
        wfFolder = wfFolderService.selectByPrimaryKey(wfFolder.getId());
        ModelAndView mav = new ModelAndView("doc/folder/wffolder_update");
        mav.addObject("wfFolder",wfFolder);
        return mav;
    }

    /**
     * 修改文件夹
     * @param wfFolder 文件夹对象
     * @return 文件夹对象
     */
    @RequestMapping(value="/wffolder_updateSave", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public WfFolder saveUpdateWfFolder(WfFolder wfFolder){
        try {
            wfFolder = wfFolderService.updateByPrimaryKey(wfFolder);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("文件夹修改失败!",e);
        }
        return wfFolder;
    }

	 /**
 	  * 删除文件夹
 	  * @param wfFolder 文件夹对象
 	  */
	 @RequestMapping(value="/wffolder_delete", method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public void deleteWfFolder(WfFolder wfFolder){
	    wfFolderService.deleteFolderById(wfFolder);
	 }
	 /**
 	  * 获取文件夹下的子文件夹或文件的数量
 	  * @param wfFolder 文件夹对象
 	  */
	 @RequestMapping(value="/wffolder_getChildCount", method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public Integer getChildCount(WfFolder wfFolder){
	    return wfFolderService.getChildCount(wfFolder);
	 }

    /**
     * 文件夹查看页面
     * @param wfFolder 文件夹对象
     */
    @RequestMapping(value = "/wffolder_view", method = RequestMethod.GET)
    public ModelAndView viewWfFolder(WfFolder wfFolder){
        wfFolder = wfFolderService.selectByPrimaryKey(wfFolder.getId());
        ModelAndView mav = new ModelAndView("doc/folder/wffolder_view");
        mav.addObject("wfFolder",wfFolder);
        return mav;
    }
    /**
     * 文档夹树
     */
    @RequestMapping(value = "/wffolder_treeIndex", method = RequestMethod.GET)
    public ModelAndView treeIndex(){
        ModelAndView mav = new ModelAndView("doc/folder/wffolder_tree");
        return mav;
    }
    /**
     * tree 加载转换格式
     * @param wfFolderList
     * @return
     */
	 private List<TreeNode> getTreeNodes(List<WfFolder> wfFolderList){
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        for(WfFolder wfFolder : wfFolderList){
            TreeNode treeNode = new TreeNode();
            treeNode.setId(wfFolder.getId());
            treeNode.setpId(wfFolder.getParentId());
            treeNode.setName(wfFolder.getFolderName());
            treeNode.setIsParent(Boolean.TRUE);
            treeNode.setIconSkin(SysResource.class.getSimpleName().toLowerCase());
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("id", wfFolder.getId());
            props.put("parentId", wfFolder.getParentId());
            props.put("bucket", wfFolder.getBucket());
            treeNode.setProps(props);
            treeNodes.add(treeNode);
        }
        return treeNodes;
	 }
}
