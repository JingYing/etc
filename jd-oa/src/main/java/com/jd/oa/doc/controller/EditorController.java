package com.jd.oa.doc.controller;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @desc 
 * @author yfwangxiaojun
 * @date 2013-8-27 下午03:21:37
 * @param 
 *
 */

@Controller
@RequestMapping("/doc")
public class EditorController {
    private static final Logger logger = Logger.getLogger(EditorController.class);
    
	@RequestMapping(value = "/editor_init")
    public String init() throws Exception {

        return "doc/editor";
    }
	
	@RequestMapping(value = "/editor_save")
    public ModelAndView save(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		String editorContent = request.getParameter("editorContent");
//		InputStream myIn= new ByteArrayInputStream(editorContent.getBytes("UTF-8")); 
		ModelAndView mav = new ModelAndView("doc/editor");
		mav.addObject("returnContent",editorContent);

        return mav;
    }
	
}
