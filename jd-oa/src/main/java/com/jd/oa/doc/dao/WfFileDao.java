package com.jd.oa.doc.dao;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.system.model.SysAuthExpression;

/**
 * 文档管理Dao接口
 * @author yaohaibin
 * @version 1.0
 */
public interface WfFileDao extends BaseDao<WfFile, Long> {

    /**
     * 新增文件
     * @param wfFile 文件对象
     */
    public void insertFile(WfFile wfFile);

    /**
     * 更新文件信息
     * @param wfFile 文件对象
     */
    public void updateByPrimaryKey(WfFile wfFile);

    /**
     * 根据主键删除文件（逻辑删除）
     * @param wfFile 文件对象
     */
    public void deleteFileByPrimaryKey(WfFile wfFile);

    /**
     * 根据主键删除文件（物理删除）
     * @param wfFile 文件对象
     */
    public void deleteByPrimaryKey(WfFile wfFile);

    /**
     * 根据文件ID查询文件夹
     * @param wfFile 文件对象
     * @return 文件对象
     */
    public WfFile selectByPrimaryKey(WfFile wfFile);

    /**
     * 获取相同文件夹下同名的文档数量
     * @param wfFile 文件对象
     * @return 相同文件数量
     */
    public Integer findFileCount(WfFile wfFile);
    /**
     * 获取文件的权限表达式
     * @param	权限表达式类型和资源ID
     * @return 权限表达式list
     */
    public List<SysAuthExpression> findBusinessExpression(Map map);
    /**
     * 获取文件的权限表达式名称
     * @param	权限表达式ID
     * @return 权限表达式list
     */
    public List<SysAuthExpression>findExpression(Map map);
}
