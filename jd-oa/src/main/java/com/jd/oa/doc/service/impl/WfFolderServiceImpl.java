package com.jd.oa.doc.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.doc.dao.WfFileDao;
import com.jd.oa.doc.dao.WfFolderDao;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.doc.model.WfFolder;
import com.jd.oa.doc.service.WfFolderService;

/**
 * 文件夹管理Service实现类
 * User: yaohaibin
 * Date: 13-8-28
 * Time: 下午4:49
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class WfFolderServiceImpl extends BaseServiceImpl<WfFolder, String> implements WfFolderService {

	private static final Logger logger = Logger.getLogger(WfFolderServiceImpl.class);

	@Autowired
	private WfFolderDao wfFolderDao;
	@Autowired
	private WfFileDao wfFileDao;

    /**
     * 文件夹树数据加载
     * @param id 文件夹ID
     * @return 文件夹列表
     */
    @Override
    public List<WfFolder> selectFolderByParentId(String id) {
        return wfFolderDao.selectFolderByParentId(id);
    }

    /**
     * 获取同一节点下同名文件夹数量
     * @param wfFolder 文件夹对象
     * @return  文件夹对象
     */
    public Integer findFolderCount(WfFolder wfFolder){
        return wfFolderDao.findFolderCount(wfFolder);
    }

    /**
     * 创建文件夹
     * @param wfFolder 文件夹对象
     * @return 文件夹对象
     * @throws RuntimeException
     */
	@Override
	public WfFolder folderInsert(WfFolder wfFolder) {
        wfFolder.setId(IdUtils.uuid2());
        wfFolder.setBucket(SystemConstant.BUCKET);
        String loginName= ComUtils.getLoginName();
        wfFolder.setCreator(loginName);
        wfFolder.setCreateTime(new Date());
        wfFolder.setModifier(loginName);
        wfFolder.setModifyTime(new Date());
        try{
            wfFolderDao.folderInsert(wfFolder);
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("创建文件夹失败!",e);
        }
        return wfFolder;
	}

    /**
     * 修改文件夹
     * @param folder 文件夹对象
     * @return 文件夹对象
     * @throws RuntimeException
     */
    @Override
    public WfFolder updateByPrimaryKey(WfFolder folder){
        folder.setModifier(ComUtils.getLoginName());
        folder.setModifyTime(new Date());
        try{
            wfFolderDao.updateByPrimaryKey(folder);
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("创建文件夹失败!",e);
        }
        return folder;
    }

    /**
     * 删除文件夹（逻辑删除）
     * @param folder 文件夹对象
     * @throws RuntimeException
     */
    @Override
    public void deleteFolderById(WfFolder folder) throws RuntimeException {
        List<WfFolder> wfFolderList = wfFolderDao.selectFolderByParentId(folder.getId());
        if(wfFolderList == null || wfFolderList.size()==0){
            try {
                folder.setModifier(ComUtils.getLoginName());
                folder.setModifyTime(new Date());
                wfFolderDao.deleteFolderById(folder);
            } catch (Exception e) {
                logger.error(e);
                throw new BusinessException("删除文件夹失败!",e);
            }
        }else{
            throw new BusinessException("删除文件夹失败,该文件夹下存在子文件夹!");
        }
    }

    /**
     * 文件夹查看页面
     * @param id 文件夹ID
     */
	public WfFolder selectByPrimaryKey(String id) {
		return wfFolderDao.selectByPrimaryKey(id);
	}
	/**
     * 获取文件夹下的子文件夹和文件的数量
     * @param folder
     * @return
     */
	public Integer getChildCount(WfFolder folder) {
		  int i =0;
		  List<WfFolder> wfFolderList = wfFolderDao.selectFolderByParentId(folder.getId());
		  if(wfFolderList!=null && wfFolderList.size()>0){
			  i=wfFolderList.size();
		  }else {
                WfFile wfFile = new WfFile();
                wfFile.setFolderId(folder.getId());
                i=wfFileDao.findFileCount(wfFile);
	        }
		return i;
	}
    public WfFolderDao getDao() {
        return wfFolderDao;
    }
  

}
