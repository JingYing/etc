package com.jd.oa.transfer.exp.service;

import java.io.File;

public interface ProcessDefinitionExportService {
	/**
	 * @author wangdongxing@jd.com
	 * 单个流程定义导出操作
	 * @param processdefId
	 * @return
	 */
	public File exportProcessDefinition(String processdefId);
}
