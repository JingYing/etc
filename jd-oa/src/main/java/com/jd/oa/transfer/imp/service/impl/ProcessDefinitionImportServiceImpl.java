package com.jd.oa.transfer.imp.service.impl;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.xmlbeans.process.xpackage.DiCcInterfaceDocument;
import org.apache.xmlbeans.process.xpackage.DiCcInterfacesDocument.DiCcInterfaces;
import org.apache.xmlbeans.process.xpackage.DiDatasourceDocument;
import org.apache.xmlbeans.process.xpackage.DiDatasourcesDocument.DiDatasources;
import org.apache.xmlbeans.process.xpackage.FormBusinessFieldsDocument.FormBusinessFields;
import org.apache.xmlbeans.process.xpackage.FormBusinessTableDocument;
import org.apache.xmlbeans.process.xpackage.FormDocument;
import org.apache.xmlbeans.process.xpackage.FormItemsDocument.FormItems;
import org.apache.xmlbeans.process.xpackage.FormTemplateDocument;
import org.apache.xmlbeans.process.xpackage.FormTemplateDocument.FormTemplate;
import org.apache.xmlbeans.process.xpackage.FormTemplatesDocument;
import org.apache.xmlbeans.process.xpackage.NodeDocument.Node;
import org.apache.xmlbeans.process.xpackage.NodesDocument.Nodes;
import org.apache.xmlbeans.process.xpackage.PackageDocument;
import org.apache.xmlbeans.process.xpackage.PackageDocument.Package;
import org.apache.xmlbeans.process.xpackage.PackageHeaderDocument.PackageHeader;
import org.apache.xmlbeans.process.xpackage.PrintTemplateDocument.PrintTemplate;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDetailDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDetailsDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigsDocument.ProcessDefinitionConfigs;
import org.apache.xmlbeans.process.xpackage.ProcessNodeExtendButtonDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeExtendButtonsDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeExtendButtonsDocument.ProcessNodeExtendButtons;
import org.apache.xmlbeans.process.xpackage.ProcessNodeFormPrivilegeDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeFormPrivilegesDocument.ProcessNodeFormPrivileges;
import org.apache.xmlbeans.process.xpackage.ProcessNodeListenerDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeListenersDocument.ProcessNodeListeners;
import org.apache.xmlbeans.process.xpackage.SonFormsDocument;
import org.apache.xmlbeans.process.xpackage.SysAuthExpressionDocument;
import org.apache.xmlbeans.process.xpackage.SysAuthExpressionsDocument;
import org.apache.xmlbeans.process.xpackage.SysBussinessAuthExpressionDocument;
import org.apache.xmlbeans.process.xpackage.SysBussinessAuthExpressionsDocument;
import org.apache.xmlbeans.process.xpackage.SysBussinessAuthExpressionsDocument.SysBussinessAuthExpressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.jd.common.web.LoginContext;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.JavascriptEscape;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiCcInterfaceService;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.form.dao.FormBusinessFieldDao;
import com.jd.oa.form.dao.FormBusinessTableDao;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormBusinessFieldService;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeExtendButton;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.model.ProcessNodeListener;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigDetailService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessNodeExtendButtonService;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeListenerService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.transfer.exp.dto.FormInfoTransferDto;
import com.jd.oa.transfer.exp.dto.ProcessDefinitionTransferDto;
import com.jd.oa.transfer.imp.service.ProcessDefinitionImportService;
/**
 * 流程导入服务
 * @author wangdongxing
 *
 */
@Transactional
public class ProcessDefinitionImportServiceImpl implements ProcessDefinitionImportService {
	
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private ProcessNodeService processNodeService;
	@Autowired
	private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
	@Autowired
	private ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
	@Autowired
	ProcessNodeExtendButtonService processNodeExtendButtonService;
	@Autowired
	private ProcessDefinitionConfigService processDefinitionConfigService;
	@Autowired
	private ProcessDefinitionConfigDetailService processDefinitionConfigDetailService;
	@Autowired
	private SysAuthExpressionService sysAuthExpressionService;
	@Autowired
	ProcessNodeListenerService processNodeListenerService;
	@Autowired
	DiDatasourceService diDatasourceService;
	@Autowired
	DiCcInterfaceService diCcInterfaceService;
	// form 相关
	@Autowired
	private FormService formService;
	@Autowired
	private FormItemService formItemService;
	@Autowired
	private FormBusinessTableService formBusinessTableService;
	@Autowired
	private FormBusinessFieldService formBusinessFieldService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private FormBusinessTableDao formBusinessTableDao;
	@Autowired
	private FormBusinessFieldDao formBusinessFieldDao;
	@Autowired
	private FormTemplateService formTemplateService;
	
	/**
	 * 导入流程定义文件
	 */
	@Override
	public void importProcessDefinition(InputStream inputStream) {
		//解压zip文件
		List<File> files=FileUtil.tarZipFiles(inputStream);
	
		File processPropertyFile=null; //流程定义文件
		File processRouteFile=null;  //流程图文件
		Map<String,File> templateFileMap=new HashMap<String, File>();
		for(File file:files){
			if(file.getName().contains(".definition")) processPropertyFile=file;
			if(file.getName().contains(".bpmn20")) processRouteFile=file;
			if(file.getName().contains(".htm") || file.getName().contains(".js")){
				templateFileMap.put(file.getName(),file);
			}
		}
		if(processPropertyFile==null){
			throw new BusinessException("缺少流程定义文件！");
		}
		if(processRouteFile==null){
			throw new BusinessException("缺少流程图文件！");
		}
		if(templateFileMap.size()==0){
			throw new BusinessException("缺少表单模板文件！");
		}
		ProcessDefinitionTransferDto dto=new ProcessDefinitionTransferDto();
		PackageDocument doc = parseXml(processPropertyFile.getAbsolutePath());
		
		PackageDocument.Package packageElement = doc.getPackage();
		//解析流程主属性
		parseProcessDefinition(packageElement,dto);
		//解析流程节点
		parseProcessNodes(packageElement,dto);
		//解析节点个性化设置
		parseProcessNodeFormPrivileges(packageElement, dto);
		//解析节点动态按钮配置
		parseProcessNodeExtendButtons(packageElement,dto);
		//解析流程的关键字段
		parseProcessDefinitionConfigs(packageElement, dto);
		//解析流程对应的权限表达式
		parseSysBusinessAuthExpression4Process(packageElement, dto);
		//解析流程所需要的权限表达式
		parseSysAuthExpressions(packageElement, dto);
		//解析流程结点事件接口
		parseProcessNodeListeners(packageElement, dto);
		//解析数据源
		parseDiDatasources(packageElement, dto);
		//解析接口
		parseDiCcInterfaces(packageElement, dto);
		
		//解析流程对应的流程图
		try {
			dto.getProcessDefinition().setProcessDefinitionFile(new String(FileUtil.getFileBytes(processRouteFile),"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new BusinessException("解析流程图文件失败",e);
		}
		//解析表单的部分
		parseDocumentForm(packageElement,dto);
		
		//表单业务处理部分
		importProcessFormPropertity(dto,templateFileMap);
		
		//使用dto做业务处理部分
		importProcessDefinitionPropertity(dto);
		
	}
	/**
	 * 解析流程需要使用的权限表达式
	 * @param packageElement
	 * @param dto
	 */
	private void parseSysAuthExpressions(Package packageElement,
			ProcessDefinitionTransferDto dto) {
		ArrayList<com.jd.oa.system.model.SysAuthExpression> sysAuthExpressionsList=new ArrayList<com.jd.oa.system.model.SysAuthExpression>();
		SysAuthExpressionsDocument.SysAuthExpressions sysAuthExpressionsElement=packageElement.getSysAuthExpressions();
		if(sysAuthExpressionsElement==null) return;
		SysAuthExpressionDocument.SysAuthExpression[] sysAuthExpressionArray=sysAuthExpressionsElement.getSysAuthExpressionArray();
		if(sysAuthExpressionArray==null||sysAuthExpressionArray.length==0) return;
		SysAuthExpression sysAuthExpression;
		for(int i=0;i<sysAuthExpressionArray.length;i++){
			sysAuthExpression=new SysAuthExpression();
			sysAuthExpression.setId(sysAuthExpressionArray[i].getId());
			sysAuthExpression.setMode(sysAuthExpressionArray[i].getMode());
			sysAuthExpression.setAuthExpressionName(sysAuthExpressionArray[i].getAuthExpressionName());
			sysAuthExpression.setOrganizationId(sysAuthExpressionArray[i].getOrganizationId());
			sysAuthExpression.setLevelAbove(sysAuthExpressionArray[i].getLevelAbove());
			sysAuthExpression.setLevelDown(sysAuthExpressionArray[i].getLevelDown());
			sysAuthExpression.setLevelUp(sysAuthExpressionArray[i].getLevelUp());
			sysAuthExpression.setUserId(sysAuthExpressionArray[i].getUserId());
			sysAuthExpression.setGroupId(sysAuthExpressionArray[i].getGroupId());
			sysAuthExpression.setRoleId(sysAuthExpressionArray[i].getRoleId());
			sysAuthExpression.setPositionId(sysAuthExpressionArray[i].getPositionId());
			sysAuthExpression.setOwner(sysAuthExpressionArray[i].getOwner());
			sysAuthExpressionsList.add(sysAuthExpression);
		}
		dto.setSysAuthExpressions(sysAuthExpressionsList);
	}

	/**
	 * 表单业务处理部分
	 * 
	 * @param dto
	 * @author yujiahe
	 */
	private void importProcessFormPropertity(ProcessDefinitionTransferDto dto,Map<String,File> templateFileMap) {
		Form formForQueryByFormCode = new Form();
		formForQueryByFormCode.setFormCode(dto.getFormInfoTransferDto()
				.getForm().getFormCode());
		if (formService.find(formForQueryByFormCode) != null&&formService.find(formForQueryByFormCode).size()>0) {
			Form existForm = formService.find(formForQueryByFormCode).get(0);
			List<FormItem> existFormItemList = formItemService
					.getFormItemByFormId(existForm.getId());
			FormBusinessTable existFormBusinessTable = formBusinessTableService
					.get(existForm.getTableId());
			List<FormBusinessField> existFormBusinessField = formBusinessFieldService
					.selectTableFieldsByTableId(dto.getFormInfoTransferDto()
							.getFormBusinessTable().getId());
			FormInfoTransferDto existFormDto = new FormInfoTransferDto(
					existForm, existFormItemList, existFormBusinessTable,
					existFormBusinessField);

			ProcessDefinitionTransferDto existDto = new ProcessDefinitionTransferDto();
			existDto.setFormInfoTransferDto(existFormDto);
			// 拿到子表
			Form formForQueryByParentId = new Form();
			formForQueryByParentId.setParentId(existForm.getId());
			formService.find(formForQueryByParentId);

			if (formService.find(formForQueryByParentId) != null
					&& formService.find(formForQueryByParentId).size() > 0) {
				for (Form existSonForm : formService
						.find(formForQueryByParentId)) {

					List<FormItem> existSonFormItemList = formItemService
							.getFormItemByFormId(existSonForm.getId());
					FormBusinessTable existSonFormBusinessTable = formBusinessTableService
							.get(existSonForm.getTableId());
					List<FormBusinessField> existSonFormBusinessField = formBusinessFieldService
							.selectTableFieldsByTableId(existSonForm.getTableId());
					FormInfoTransferDto existSonFormDto = new FormInfoTransferDto(
							existSonForm, existSonFormItemList,
							existSonFormBusinessTable,
							existSonFormBusinessField);
					existDto.getSonFormInfoTransferDto().add(existSonFormDto);

				}

			}
			// 主要业务逻辑部分
			if (formService.find(formForQueryByFormCode).size() > 0
					&&
					(
					(dto.getSonFormInfoTransferDto().size()==0)
						&&	dto.equalMainForm(existFormDto)
					&& dto.equalMainFormBusiness(existFormDto)
					 
					||
					(
							 dto.getSonFormInfoTransferDto().size()>0
							 &&dto.equalMainForm(existFormDto)
					&& dto.equalMainFormBusiness(existFormDto)
					
					&& dto.equalSubForm(existDto.getSonFormInfoTransferDto())
					&& dto.equalSubFormBusiness(existDto
							.getSonFormInfoTransferDto())
							)
							)
							
			) {// 如果都相等
															// 把已有的表单id设置给流程定义

				dto.getProcessDefinition().setFormId(existForm.getId());

			} else if (formService.find(formForQueryByFormCode).size() > 0
					&& dto.equalMainFormBusiness(existFormDto)
					&& dto.equalSubFormBusiness(existDto
							.getSonFormInfoTransferDto())) {// 业务表相等 --重新生成FORM
				
															// FORMITEM部分
				createFormInfoToDB(dto, existDto,templateFileMap);

			} else {// 不存在业务表，所有数据增量插入 或者业务表存在但是不相等 ---全量新增
				if (dto.getFormInfoTransferDto().getForm() != null) {// 若主表数据不为空

					createFormInfoToDB(dto,templateFileMap,true);//业务表重新起名
				} else {
					throw new BusinessException("缺少表单数据！");
				}

			}		
		}else {// 不存在业务表，所有数据增量插入 或者业务表存在但是不相等 ---全量新增
				if (dto.getFormInfoTransferDto().getForm() != null) {// 若主表数据不为空

					createFormInfoToDB(dto,templateFileMap,false);//直接用原有的业务表名字
				} else {
					throw new BusinessException("缺少表单数据！");
				}

			}


	}

	/**
	 * 全量新增 form formItem formBusinessTable formBusinessField
	 * 
	 * @param dto
	 * @author yujiahe
	 */
	private void createFormInfoToDB(ProcessDefinitionTransferDto dto,Map<String,File> templateFileMap,boolean flag) {
		
		
		
	
		

		String loginUserId = "";
		LoginContext context = LoginContext.getLoginContext();
		if (context != null) {
			loginUserId = sysUserService.getUserIdByUserName(
					ComUtils.getLoginNamePin(), "1");
		}

		List<Form> preAddFormList = new ArrayList<Form>();
		List<FormItem> preAddFormItemList = new ArrayList<FormItem>();
		List<FormBusinessTable> preAddFormBusinessTableList = new ArrayList<FormBusinessTable>();
		List<FormBusinessField> preAddFormBusinessFieldList = new ArrayList<FormBusinessField>();

		// 从DTO 拿数据
		Form form = dto.getFormInfoTransferDto().getForm();
		List<FormItem> formItemList = dto.getFormInfoTransferDto()
				.getFormItems();
		FormBusinessTable formBusinessTable = dto.getFormInfoTransferDto()
				.getFormBusinessTable();
		List<FormBusinessField> formBusinessFieldList = dto
				.getFormInfoTransferDto().getFormBusinessFields();

		List<FormInfoTransferDto> subFormList = dto.getSonFormInfoTransferDto();

		// 重设ID
		String oldFormId=form.getId();
		String formUUID = IdUtils.uuid2();// form 重新设置新ID
		String formBusinessTableUUID = IdUtils.uuid2();// formBusinessTable
		Map<String,String> formIDsMap=new HashMap<String, String>(); //记录需要替换的恶formId
		formIDsMap.put(oldFormId, formUUID);														// 重新设置新ID

		// 将从DTO拿出数据 放入预新增列表

		dto.getProcessDefinition().setFormId(formUUID);
		
		
		if(flag){
			String mainFormCode = SystemConstant.T_JDOA_.concat(Long.toString(IdUtils.randomLong()));
			form.setFormCode(mainFormCode);
			formBusinessTable.setTableName(mainFormCode);
			
		}
		

		// 表单相关
		form.setId(formUUID);
		form.setTableId(formBusinessTableUUID);
		form.setOwner(loginUserId);
		
		
		
		preAddFormList.add(form);
		//表单模板相关
		List<com.jd.oa.form.model.FormTemplate> formTemplates = dto.getFormTemplates();
		for(int i=0;i<formTemplates.size();i++){
			String newTemplateId=IdUtils.uuid2();
			String oldTemplateId=formTemplates.get(i).getId();
			formTemplates.get(i).setId(newTemplateId);
			formTemplates.get(i).setFormId(formUUID);
			File newFile=templateFileMap.get("workflow_"+formTemplates.get(i).getTemplatePath());
			String fileContent="";
			try {
				fileContent = new String(FileUtil.getFileBytes(newFile),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			newTemplateId=formTemplateService.save(formTemplates.get(i),JavascriptEscape.escape(fileContent));
			for(ProcessNode node:dto.getProcessNodes()){
				if(node.getFormTemplateId().equals(oldTemplateId)){
					node.setFormTemplateId(newTemplateId);
				}
				if(node.getPrintTemplateId().equals(oldTemplateId)){
					node.setPrintTemplateId(newTemplateId);
				}
			}
		}
		
		// 业务表相关
		formBusinessTable.setId(formBusinessTableUUID);
		preAddFormBusinessTableList.add(formBusinessTable);

		// 业务表项相关
		Map<String,String> fieldIDsMap=new HashMap<String, String>(); //记录需要替换的fieldId
		if (formBusinessFieldList != null && formBusinessFieldList.size() > 0) {
			for (FormBusinessField formBusinessField : formBusinessFieldList) {
				String oldFieldId = formBusinessField.getId();
				String newFieldId = IdUtils.uuid2();
				formBusinessField.setId(newFieldId);
				formBusinessField.setTableId(formBusinessTableUUID);
				preAddFormBusinessFieldList.add(formBusinessField);
				fieldIDsMap.put(oldFieldId, newFieldId);
			}

		}
		
		// 表单项相关
		Map<String,String> formItemIDsMap=new HashMap<String, String>();  //记录需要替换的formItemId
		if (formItemList != null && formItemList.size() > 0) {
			for (FormItem formItem : formItemList) {
				String oldFormItemId=formItem.getId();
				String newFormItemId=IdUtils.uuid2();
				formItem.setId(newFormItemId);
				formItem.setFormId(formUUID);
				formItem.setFieldId(fieldIDsMap.get(formItem.getFieldId())==null?
						formItem.getFieldId():fieldIDsMap.get(formItem.getFieldId()));
				preAddFormItemList.add(formItem);
				formItemIDsMap.put(oldFormItemId,newFormItemId);
			}

		}

		// 子表业务处理
		if (subFormList != null && subFormList.size() > 0) {
			for (FormInfoTransferDto subFormDto : subFormList) {

				// 从DTO 拿数据
				Form subForm = subFormDto.getForm();
				List<FormItem> subFormItemList = subFormDto.getFormItems();
				FormBusinessTable subFormBusinessTable = subFormDto
						.getFormBusinessTable();
				List<FormBusinessField> subFormBusinessFieldList = subFormDto
						.getFormBusinessFields();

				// 重设ID
				String oldSubFormUUID=subForm.getId();
				String subFormUUID = IdUtils.uuid2();// form 重新设置新ID
				String subFormBusinessTableUUID = IdUtils.uuid2();// formBusinessTable
																	// 重新设置新ID
				formIDsMap.put(oldSubFormUUID, subFormUUID);
				
				if(flag){
					String subFormCode = SystemConstant.T_JDOA_.concat(Long.toString(IdUtils.randomLong()));
					subForm.setFormCode(subFormCode);
					subFormBusinessTable.setTableName(subFormCode);
					
				}
				

				// 将从DTO拿出数据 放入预新增列表
				// 表单相关
				subForm.setId(subFormUUID);
				subForm.setTableId(subFormBusinessTableUUID);
				subForm.setOwner(loginUserId);
				subForm.setParentId(formUUID);
				preAddFormList.add(subForm);

				// 业务表相关
				subFormBusinessTable.setId(subFormBusinessTableUUID);
				subFormBusinessTable.setParentId(formBusinessTableUUID);
				preAddFormBusinessTableList.add(subFormBusinessTable);

				// 业务表项相关
				if (subFormBusinessFieldList != null
						&& subFormBusinessFieldList.size() > 0) {
					for (FormBusinessField subFormBusinessField : subFormBusinessFieldList) {
						String oldFieldId = subFormBusinessField.getId();
						String newFieldId = IdUtils.uuid2();
						subFormBusinessField.setId(newFieldId);
						subFormBusinessField
								.setTableId(subFormBusinessTableUUID);
						preAddFormBusinessFieldList.add(subFormBusinessField);
						fieldIDsMap.put(oldFieldId, newFieldId);
					}

				}
				
				// 表单项相关
				if (subFormItemList != null && subFormItemList.size() > 0) {
					for (FormItem subFormItem : subFormItemList) {
						String oldSubFormItemId=subFormItem.getId();
						String newSubFormItemId=IdUtils.uuid2();
						subFormItem.setId(newSubFormItemId);
						subFormItem.setFormId(subFormUUID);
						subFormItem.setFieldId(fieldIDsMap.get(subFormItem.getFieldId())==null?
								subFormItem.getFieldId():fieldIDsMap.get(subFormItem.getFieldId()));
						preAddFormItemList.add(subFormItem);
						formItemIDsMap.put(oldSubFormItemId,newSubFormItemId);
					}

				}
			}

		}
		// 数据库操作
		// form formItem 插入
		formService.createFormAndFormItem(preAddFormList, preAddFormItemList);
		// formBusinessTable
		this.formBusinessTableDao.insert(preAddFormBusinessTableList);
		// formBusinessField
		this.formBusinessFieldDao.insert(preAddFormBusinessFieldList);
		// 业务表生成
		Map<String, Object> result = this.formBusinessFieldService
				.createTableIncludeChild(formBusinessTableUUID);
		
		//表单js事件操作
		File newFile=templateFileMap.get("workflow_"+oldFormId+".js");
		String fileContent="";
		try {
			fileContent = new String(FileUtil.getFileBytes(newFile),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if(null != fileContent && !fileContent.equals("")){
			formTemplateService.saveJavascript(formUUID,JavascriptEscape.escape(fileContent));//JS事件保存到JSS中
		}
		
		
		
		//将dto中流程的与form相关联的UUID进行替换  包含ProcessNodeFormPrivilege、ProcessNodeExtend两张表
		for(ProcessNodeFormPrivilege privilege:dto.getProcessNodeFormPrivileges()){
			privilege.setFormId(formIDsMap.get(privilege.getFormId())==null?privilege.getFormId():formIDsMap.get(privilege.getFormId()));
			privilege.setItemId(formItemIDsMap.get(privilege.getItemId())==null?privilege.getItemId():formItemIDsMap.get(privilege.getItemId()));
		}
		for(ProcessNodeExtendButton extendButton:dto.getProcessNodeExtendButtons()){
			extendButton.setFormId(formIDsMap.get(extendButton.getFormId())==null?extendButton.getFormId():formIDsMap.get(extendButton.getFormId()));
		}
		//替换流程定义文件中线上流程变量formId
		if(formIDsMap != null && formIDsMap.size() > 0){
			String tempStr="";
			for(Entry<String,String> entry:formIDsMap.entrySet()){
				tempStr=dto.getProcessDefinition().getProcessDefinitionFile().replaceAll(entry.getKey(),entry.getValue());
				dto.getProcessDefinition().setProcessDefinitionFile(tempStr);
			}
		}
	}

	/**
	 * 只更新form formItem 在业务表相等的情况下调用此方法
	 * 
	 * @param dto
	 * @param existDto
	 * @author yujiahe
	 */

	private void createFormInfoToDB(ProcessDefinitionTransferDto dto,
			ProcessDefinitionTransferDto existDto,Map<String,File> templateFileMap) {

		String loginUserId = "";
		LoginContext context = LoginContext.getLoginContext();
		if (context != null) {
			loginUserId = sysUserService.getUserIdByUserName(
					ComUtils.getLoginNamePin(), "1");
		}

		// 收集打算新增的数据
		List<Form> preAddFormList = new ArrayList<Form>();
		List<FormItem> preAddFormItemList = new ArrayList<FormItem>();

		// 从DTO 拿数据
		// 主表相关
		Form form = dto.getFormInfoTransferDto().getForm();
		List<FormItem> formItemList = dto.getFormInfoTransferDto()
				.getFormItems();
		// 子表相关
		List<FormInfoTransferDto> subFormList = dto.getSonFormInfoTransferDto();

		// 重设ID
		String oldFormId=form.getId();
		String formUUID = IdUtils.uuid2();// form 重新设置新ID
		String formBusinessTableUUID = existDto.getFormInfoTransferDto()
				.getFormBusinessTable().getId();// 待修改

		// 将从DTO拿出数据 放入预新增列表

		dto.getProcessDefinition().setFormId(formUUID);

		// 表单相关
		form.setId(formUUID);
		form.setTableId(formBusinessTableUUID);
		form.setOwner(loginUserId);
		// form.setTableId(existDto.getFormInfoTransferDto().getFormBusinessFields().get(0).getTableId());
		preAddFormList.add(form);
		Map<String,String> formIDsMap=new HashMap<String, String>(); //记录需要替换的恶formId
		formIDsMap.put(oldFormId, formUUID);
		//表单模板相关
		List<com.jd.oa.form.model.FormTemplate> formTemplates = dto.getFormTemplates();
		for(int i=0;i<formTemplates.size();i++){
			String newTemplateId=IdUtils.uuid2();
			String oldTemplateId=formTemplates.get(i).getId();
			formTemplates.get(i).setId(newTemplateId);
			formTemplates.get(i).setFormId(formUUID);
			File newFile=templateFileMap.get("workflow_"+formTemplates.get(i).getTemplatePath());
			String fileContent="";
			try {
				fileContent = new String(FileUtil.getFileBytes(newFile),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			newTemplateId=formTemplateService.save(formTemplates.get(i),JavascriptEscape.escape(fileContent));
			for(ProcessNode node:dto.getProcessNodes()){
				if(node.getFormTemplateId().equals(oldTemplateId)){
					node.setFormTemplateId(newTemplateId);
				}
				if(node.getPrintTemplateId().equals(oldTemplateId)){
					node.setPrintTemplateId(newTemplateId);
				}
			}
		}
		// 表单项相关
		Map<String,String> formItemIDsMap=new HashMap<String, String>();  //记录需要替换的formItemId
		if (formItemList != null && formItemList.size() > 0) {
			for (FormItem formItem : formItemList) {
				String oldFormItemId=formItem.getId();
				String newFormItemId=IdUtils.uuid2();
				formItem.setId(newFormItemId);
				formItem.setFormId(formUUID);
				preAddFormItemList.add(formItem);
				formItemIDsMap.put(oldFormItemId,newFormItemId);
			}
		}

		// 子表业务处理
		if (subFormList != null && subFormList.size() > 0) {
			for (FormInfoTransferDto subFormDto : subFormList) {

				// 从DTO 拿数据
				Form subForm = subFormDto.getForm();
				List<FormItem> subFormItemList = subFormDto.getFormItems();

				// 重设ID
				String oldSubFormUUID=subForm.getId();
				String subFormUUID = IdUtils.uuid2();// form 重新设置新ID
				String subFormBusinessTableUUID = "";// formBusinessTable
														// 重新设置新ID 找到业务表中存储的 id
														// 待修改
				
				for (FormInfoTransferDto fbt : existDto
						.getSonFormInfoTransferDto()) {
					if (fbt.getForm().getFormName()
							.equals(subForm.getFormName())) {
						subFormBusinessTableUUID = fbt.getFormBusinessTable()
								.getId();
					}

				}
				formIDsMap.put(oldSubFormUUID, subFormUUID);
				// 将从DTO拿出数据 放入预新增列表

				// 表单相关
				subForm.setId(subFormUUID);
				subForm.setTableId(subFormBusinessTableUUID);
				subForm.setOwner(loginUserId);
				subForm.setParentId(formUUID);
				preAddFormList.add(subForm);

				// 表单项相关
				if (subFormItemList != null && subFormItemList.size() > 0) {
					for (FormItem subFormItem : subFormItemList) {
						String oldSubFormItemId=subFormItem.getId();
						String newSubFormItemId=IdUtils.uuid2();
						subFormItem.setId(newSubFormItemId);
						subFormItem.setFormId(subFormUUID);
						preAddFormItemList.add(subFormItem);
						formItemIDsMap.put(oldSubFormItemId, newSubFormItemId);
					}
				}
			}

		}
		// 数据库操作
		// form formItem 插入
		formService.createFormAndFormItem(preAddFormList, preAddFormItemList);
		
		//表单js事件操作
		File newFile=templateFileMap.get("workflow_"+oldFormId+".js");
		String fileContent="";
		try {
			fileContent = new String(FileUtil.getFileBytes(newFile),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if(null != fileContent && !fileContent.equals("")){
			formTemplateService.saveJavascript(formUUID,JavascriptEscape.escape(fileContent));//JS事件保存到JSS中
		}
		
		//将dto中流程的与form相关联的UUID进行替换  包含ProcessNodeFormPrivilege、ProcessNodeExtend两张表
		for(ProcessNodeFormPrivilege privilege:dto.getProcessNodeFormPrivileges()){
			privilege.setFormId(formIDsMap.get(privilege.getFormId())==null?privilege.getFormId():formIDsMap.get(privilege.getFormId()));
			privilege.setItemId(formItemIDsMap.get(privilege.getItemId())==null?privilege.getItemId():formItemIDsMap.get(privilege.getItemId()));
		}
		for(ProcessNodeExtendButton extendButton:dto.getProcessNodeExtendButtons()){
			extendButton.setFormId(formIDsMap.get(extendButton.getFormId())==null?extendButton.getFormId():formIDsMap.get(extendButton.getFormId()));
		}
		//替换流程定义文件中线上流程变量formId
		if(formIDsMap != null && formIDsMap.size() > 0){
			String tempStr="";
			for(Entry<String,String> entry:formIDsMap.entrySet()){
				tempStr=dto.getProcessDefinition().getProcessDefinitionFile().replaceAll(entry.getKey(),entry.getValue());
				dto.getProcessDefinition().setProcessDefinitionFile(tempStr);
			}
		}
	}

	/**
	 * 表单数据导入DTO
	 * 
	 * @param packageElement
	 * @param dto
	 * @author yujiahe
	 */
	private void parseDocumentForm(Package packageElement,
			ProcessDefinitionTransferDto dto) {

		org.apache.xmlbeans.process.xpackage.FormDocument.Form formAttribute = packageElement
				.getForm();
		Form form = new Form();
		// 主表相关
		form.setId(formAttribute.getId());
		form.setFormCode(formAttribute.getFormCode());
		form.setFormName(formAttribute.getFormName());
		form.setFormType(formAttribute.getFormType());
		form.setParentId(formAttribute.getParentId());
		form.setTableId(formAttribute.getTableId());
		form.setFormDesc(formAttribute.getFormDesc());
		form.setYn(formAttribute.getYn());
		form.setOwner(formAttribute.getOwner());
		form.setCreateType(formAttribute.getCreateType());
		dto.getFormInfoTransferDto().setForm(form);
		parseDocumentFormItem(formAttribute, dto.getFormInfoTransferDto());
		parseFormTemplate(formAttribute,dto);
		parseDocumentFormBusinessTable(formAttribute,
				dto.getFormInfoTransferDto());
		parseDocumentFormBusinessField(formAttribute,
				dto.getFormInfoTransferDto());
		// 子表相关
		if (formAttribute.getSonFormsArray() != null
				&& formAttribute.getSonFormsArray().length > 0) {
			for (org.apache.xmlbeans.process.xpackage.FormDocument.Form sfs : formAttribute
					.getSonFormsArray(0).getFormArray()) {
				Form sonForm = new Form();
				sonForm.setId(sfs.getId());
				sonForm.setFormCode(sfs.getFormCode());
				sonForm.setFormName(sfs.getFormName());
				sonForm.setFormType(sfs.getFormType());
				sonForm.setParentId(sfs.getParentId());
				sonForm.setTableId(sfs.getTableId());
				sonForm.setFormDesc(sfs.getFormDesc());
				sonForm.setYn(sfs.getYn());
				sonForm.setOwner(sfs.getOwner());
				sonForm.setCreateType(sfs.getCreateType());

				FormInfoTransferDto sonformInfoTransferDto = new FormInfoTransferDto();
				sonformInfoTransferDto.setForm(sonForm);
				//parseDocumentFormItem(formAttribute, sonformInfoTransferDto);
				parseDocumentFormItem(sfs, sonformInfoTransferDto);
				//parseDocumentFormBusinessTable(formAttribute,sonformInfoTransferDto);
				parseDocumentFormBusinessTable(sfs,sonformInfoTransferDto);
				//parseDocumentFormBusinessField(formAttribute,sonformInfoTransferDto);
				parseDocumentFormBusinessField(sfs,sonformInfoTransferDto);
				dto.getSonFormInfoTransferDto().add(sonformInfoTransferDto);

			}
		}

	}
	/**
	 * form对应的表单模板解析
	 * @param formAttribute
	 * @param dto
	 */
	private void parseFormTemplate(
			org.apache.xmlbeans.process.xpackage.FormDocument.Form formAttribute,
			ProcessDefinitionTransferDto dto) {
		ArrayList<com.jd.oa.form.model.FormTemplate> formTemplateList=new ArrayList<com.jd.oa.form.model.FormTemplate>();
		FormTemplatesDocument.FormTemplates[] formTemplatesElement=formAttribute.getFormTemplatesArray();
		for(int i=0;i<formTemplatesElement.length;i++){
			FormTemplateDocument.FormTemplate[] templateArray=formTemplatesElement[i].getFormTemplateArray();
			for(int j=0;j<templateArray.length;j++){
				com.jd.oa.form.model.FormTemplate ft=new com.jd.oa.form.model.FormTemplate();
				ft.setId(templateArray[j].getId());
				ft.setFormId(templateArray[j].getFormId());
				ft.setTemplateDesc(templateArray[i].getTemplateDesc());
				ft.setTemplateName(templateArray[i].getTemplateName());
				ft.setTemplatePath(templateArray[i].getTemplatePath());
				ft.setTemplateType(templateArray[i].getTemplateType());
				ft.setYn(templateArray[i].getYn());
				formTemplateList.add(ft);
			}
		}
		dto.setFormTemplates(formTemplateList);
	}


	/**
	 * 表单项放入DTO
	 * 
	 * @param packageElement
	 * @param dto
	 * @author yujiahe
	 */
	private void parseDocumentFormItem(FormDocument.Form formAttribute,
			FormInfoTransferDto formInfoTransferDto) {
		List<FormItem> formItems = new ArrayList<FormItem>();
		if (formAttribute.getFormItemsArray(0) != null) {
			FormItems formItemsArray = formAttribute.getFormItemsArray(0);
			for (int i = 0; i < formItemsArray.getFormItemArray().length; i++) {
				FormItem formItem = new FormItem();
				formItem.setId(formItemsArray.getFormItemArray(i).getId());

				formItem.setFormId(formItemsArray.getFormItemArray(i)
						.getFormId());
				formItem.setFieldName(formItemsArray.getFormItemArray(i)
						.getFieldName());
				formItem.setFieldChineseName(formItemsArray.getFormItemArray(i)
						.getFieldChineseName());
				formItem.setTableType(formItemsArray.getFormItemArray(i)
						.getTableType());
				formItem.setFieldId(formItemsArray.getFormItemArray(i)
						.getFieldId());
				formItem.setDataType(formItemsArray.getFormItemArray(i)
						.getDataType());
				formItem.setLength(formItemsArray.getFormItemArray(i)
						.getLength());
				formItem.setDefaultValue(formItemsArray.getFormItemArray(i)
						.getDefaultValue());
				formItem.setInputType(formItemsArray.getFormItemArray(i)
						.getInputType());
				formItem.setInputTypeConfig(formItemsArray.getFormItemArray(i)
						.getInputTypeConfig());
				formItem.setDictTypeCode(formItemsArray.getFormItemArray(i)
						.getDictTypeCode());
				formItem.setSortNo(formItemsArray.getFormItemArray(i)
						.getSortNo());
				formItem.setValidateRule(formItemsArray.getFormItemArray(i)
						.getValidateRule());
				formItem.setYn(formItemsArray.getFormItemArray(i).getYn());
				formItem.setComputeExpress(formItemsArray.getFormItemArray(i)
						.getComputeExpress());
				formItem.setComputeExpressDesc(formItemsArray.getFormItemArray(
						i).getComputeExpressDesc());
				formItems.add(formItem);
			}
			formInfoTransferDto.setFormItems(formItems);
		}

	}

	/**
	 * 业务表放入DTO
	 * 
	 * @param packageElement
	 * @param dto
	 * @author yujiahe
	 */
	private void parseDocumentFormBusinessTable(
			FormDocument.Form formAttribute,
			FormInfoTransferDto formInfoTransferDto) {

		if (formAttribute.getFormBusinessTableArray(0) != null) {
			FormBusinessTable formBusinessTable = new FormBusinessTable();
			FormBusinessTableDocument.FormBusinessTable formBusinessTableAttribute = formAttribute
					.getFormBusinessTableArray(0);
			formBusinessTable.setId(formBusinessTableAttribute.getId());
			formBusinessTable.setTableName(formBusinessTableAttribute
					.getTableName());
			formBusinessTable.setTableChineseName(formBusinessTableAttribute
					.getTableChineseName());
			formBusinessTable.setTableDesc(formBusinessTableAttribute
					.getTableDesc());
			formBusinessTable.setTableType(formBusinessTableAttribute
					.getTableType());
			formBusinessTable.setParentId(formBusinessTableAttribute
					.getParentId());
			formBusinessTable.setCreateType(formBusinessTableAttribute
					.getCreateType());
			formBusinessTable.setYn(formBusinessTableAttribute.getYn());
			formBusinessTable.setOwner(formBusinessTableAttribute.getOwner());
			formInfoTransferDto.setFormBusinessTable(formBusinessTable);

		}

	}

	/**
	 * 业务表数据项放入DTO
	 * 
	 * @param packageElement
	 * @param dto
	 * @author yujiahe
	 */
	private void parseDocumentFormBusinessField(
			FormDocument.Form formAttribute,
			FormInfoTransferDto formInfoTransferDto) {
		if (formAttribute.getFormBusinessFieldsArray(0) != null) {
			List<FormBusinessField> formBusinessFieldList = new ArrayList<FormBusinessField>();
			FormBusinessFields formBusinessFieldsArray = formAttribute
					.getFormBusinessFieldsArray(0);
			for (int i = 0; i < formBusinessFieldsArray
					.getFormBusinessFieldArray().length; i++) {
				FormBusinessField formBusinessField = new FormBusinessField();
				formBusinessField.setId(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getId());
				formBusinessField.setTableId(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getTableId());
				formBusinessField.setFieldName(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getFieldName());
				formBusinessField.setFieldChineseName(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getFieldChineseName());
				formBusinessField.setDataType(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getDataType());
				formBusinessField.setIsNull(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getIsNull());
				formBusinessField.setLength(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getLength());
				formBusinessField.setDefaultValue(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getDefaultValue());
				formBusinessField.setInputType(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getInputType());
				formBusinessField.setInputTypeConfig(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getInputTypeConfig());
				formBusinessField.setDictTypeCode(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getDictTypeCode());
				formBusinessField.setSortNo(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getSortNo());
				formBusinessField.setYn(formBusinessFieldsArray
						.getFormBusinessFieldArray(i).getYn());
				formBusinessFieldList.add(formBusinessField);

			}
			formInfoTransferDto.setFormBusinessFields(formBusinessFieldList);

		}

	}
	
	private void importProcessDefinitionPropertity(
			ProcessDefinitionTransferDto dto) {
		//取得当前登录用户的Id
		String loginUserId = "";
		LoginContext context = LoginContext.getLoginContext();
		if (context != null) {
			loginUserId = sysUserService.getUserIdByUserName(
					ComUtils.getLoginNamePin(), "1");
		}
		
		//插入一条主流程
		ProcessDefinition processDefinition=dto.getProcessDefinition();
		if(processDefinition == null) {
			throw new BusinessException("解析流程定义文件失败，未取到流程定义主属性！");
		}
		String oldPafId=processDefinition.getPafProcessDefinitionId();
		String newPafId="process"+Calendar.getInstance().getTimeInMillis();
		
		processDefinition.setId(null);
		processDefinition.setPafProcessDefinitionId(newPafId);
		processDefinition.setIsIssue("0");
		processDefinition.setSortNo(0);
		processDefinition.setOwner(loginUserId);
		String newProcessdefId=processDefService.addOrUpdateProcessDefinition(processDefinition);
		if(null==newProcessdefId||newProcessdefId.equals("")){
			throw new BusinessException("插入流程主记录失败!");
		}else if(newProcessdefId.equals("-1")){
			throw new BusinessException("已存在同名的流程定义！");
		}
		//插入流程所需要的权限表达式
		Map<String,String> expressionIdMap=new HashMap<String, String>();   //需要重新替换的权限表达式Id
		List<SysAuthExpression> sysAuthExpressionList=dto.getSysAuthExpressions();
		if(sysAuthExpressionList!=null&&sysAuthExpressionList.size()>0){
			for(SysAuthExpression expression:sysAuthExpressionList){
				String oldExpressionId=expression.getId();
				//判断是否已存在相同的权限表达式
				SysAuthExpression queryExpression=new SysAuthExpression();
				queryExpression.setAuthExpressionName(expression.getAuthExpressionName());
				List<SysAuthExpression> resultExpressions=sysAuthExpressionService.find(queryExpression);
				if(resultExpressions==null||resultExpressions.size()==0||!expression.equals(resultExpressions.get(0))){
					//当作新记录插入数据库
					expression.setId(null);
					expression.setOwner(loginUserId);
					String newExpressionId=sysAuthExpressionService.insert(expression);
					expressionIdMap.put(oldExpressionId,newExpressionId);
				}else{
					expressionIdMap.put(oldExpressionId,resultExpressions.get(0).getId());
				}
			}
		}
		//插入流程节点
		Map<String,String> nodeIDsMap=new HashMap<String, String>(); //记录需要替换的NodeId
		List<ProcessNode> nodeList=dto.getProcessNodes();
		if(nodeList != null && nodeList.size() > 0){
			//遍历流程结点保存
			for(ProcessNode processNode:nodeList){
				//取得流程节点授权列表
				//List<SysBussinessAuthExpression> nodeExpressionList=dto.getSysBussinessAuthExpression4ProcessNodeMap().get(processNode);
				String oldNodeId=processNode.getId();
				
				//插入一条新结点记录
				processNode.setId(null);
				processNode.setProcessDefinitionId(newProcessdefId);
				String newNodeID=processNodeService.insert(processNode);
				nodeIDsMap.put(oldNodeId, newNodeID);
				/*if(nodeExpressionList != null && nodeExpressionList.size() > 0){
					for(SysBussinessAuthExpression authExpression:nodeExpressionList){
						authExpression.setId(null);
						authExpression.setBusinessId(newNodeID);
						authExpression.setAuthExpressionId(expressionIdMap.get(authExpression.getAuthExpressionId()));
					}
					sysBussinessAuthExpressionService.insert(nodeExpressionList);
				}*/
			}
		}
		
		//插入流程节点的个性化设置
		List<ProcessNodeFormPrivilege> privilegeList=dto.getProcessNodeFormPrivileges();
		if(privilegeList != null && privilegeList.size() > 0){
			for(ProcessNodeFormPrivilege processNodeFormPrivilege:privilegeList){
				processNodeFormPrivilege.setId(IdUtils.uuid2());
				processNodeFormPrivilege.setProcessDefinitionId(newProcessdefId);
				processNodeFormPrivilege.setNodeId(nodeIDsMap.get(processNodeFormPrivilege.getNodeId())==null?
					processNodeFormPrivilege.getNodeId():nodeIDsMap.get(processNodeFormPrivilege.getNodeId()));
				processNodeFormPrivilegeService.insertProcessNodeFormPrivilege(processNodeFormPrivilege);
			}
		}
		
		//插入流程节点的动态按钮配置
		List<ProcessNodeExtendButton> processNodeExtendButtonList = dto.getProcessNodeExtendButtons();
		if(null != processNodeExtendButtonList && !processNodeExtendButtonList.isEmpty()){
			for(ProcessNodeExtendButton processNodeExtendButton:processNodeExtendButtonList){
				processNodeExtendButton.setId(IdUtils.uuid2());
				processNodeExtendButton.setProcessDefinitionId(newProcessdefId);
				processNodeExtendButton.setNodeId(nodeIDsMap.get(processNodeExtendButton.getNodeId())==null?
						processNodeExtendButton.getNodeId():nodeIDsMap.get(processNodeExtendButton.getNodeId()));
				processNodeExtendButtonService.insert(processNodeExtendButton);
			}
		}
		
		//插入接口
		Map<String,String> interfaceIDsMap=new HashMap<String, String>();
		List<DiCcInterface> diCcInterfaceList = dto.getDiCcInterfaces();
		if(null != diCcInterfaceList && !diCcInterfaceList.isEmpty()){
			for(DiCcInterface diCcInterface:diCcInterfaceList){
				String oldId = diCcInterface.getId();
				String newId = IdUtils.uuid2();
				diCcInterface.setId(newId);
				diCcInterfaceService.insert(diCcInterface);
				interfaceIDsMap.put(oldId, newId);
			}
		}
		
		//插入数据源
		Map<String,String> datasourceIDsMap=new HashMap<String, String>();
		List<DiDatasource> diDatasourceList = dto.getDiDataSources();
		if(null != diDatasourceList && !diDatasourceList.isEmpty()){
			for(DiDatasource diDatasource:diDatasourceList){
				String oldId = diDatasource.getId();
				String newId = IdUtils.uuid2();
				diDatasource.setId(newId);
				diDatasource.setDataAdapter(interfaceIDsMap.get(diDatasource.getDataAdapter())==null?
						diDatasource.getDataAdapter():interfaceIDsMap.get(diDatasource.getDataAdapter()));
				diDatasourceService.insert(diDatasource);
				datasourceIDsMap.put(oldId, newId);
			}
		}
		
		//插入流程节点事件接口
		List<ProcessNodeListener> nodeListenerList = dto.getProcessNodeListeners();
		if(null != nodeListenerList && !nodeListenerList.isEmpty()){
			for(ProcessNodeListener processNodeListener:nodeListenerList){
				processNodeListener.setId(IdUtils.uuid2());
				processNodeListener.setProcessDefinitionId(newProcessdefId);
				processNodeListener.setNodeId(nodeIDsMap.get(processNodeListener.getNodeId())==null?
						processNodeListener.getNodeId():nodeIDsMap.get(processNodeListener.getNodeId()));
				processNodeListener.setInterfaceName(datasourceIDsMap.get(processNodeListener.getInterfaceName())==null?
						processNodeListener.getInterfaceName():datasourceIDsMap.get(processNodeListener.getInterfaceName()));
				processNodeListenerService.insert(processNodeListener);
			}
		}
		
		//取得流程定义授权列表
		/*List<SysBussinessAuthExpression> authExpressionList=dto.getSysBussinessAuthExpressions();
		if(authExpressionList != null && authExpressionList.size() > 0){
			for(SysBussinessAuthExpression authExpression:authExpressionList){
				authExpression.setId(null);
				authExpression.setBusinessId(newProcessdefId);
				authExpression.setAuthExpressionId(expressionIdMap.get(authExpression.getAuthExpressionId()));
			}
			sysBussinessAuthExpressionService.insert(authExpressionList);
		}*/
		
		//保存流程关键字段的配置表
		List<ProcessDefinitionConfig> configs=dto.getProcessDefinitionConfigs();
		if(configs != null && configs.size() > 0){
			for(ProcessDefinitionConfig cfg:configs){
				List<ProcessDefinitionConfigDetail> details=dto.getConfigDetails4ProcessdefinitionConfigMap().get(cfg);
				cfg.setId(null);
				cfg.setProcessDefinitionId(newProcessdefId);
				cfg.setCreateTime(null);
				cfg.setCreator(ComUtils.getLoginName());
				String newConfigId=processDefinitionConfigService.insert(cfg);
				//如果存在流程配置详情表，则处理，只针对configType==2有效
				if(details!=null&&details.size()>0){
					for(ProcessDefinitionConfigDetail detail:details){
						List<SysBussinessAuthExpression> bussinessAuthExpressionList=dto.getSysBusinessExpressions4ConfigDetailMap().get(detail);
						detail.setId(null);
						detail.setConfigId(newConfigId);
						String newDetailId=processDefinitionConfigDetailService.insert(detail);
						if(bussinessAuthExpressionList != null && bussinessAuthExpressionList.size() > 0){
							for(SysBussinessAuthExpression expression:bussinessAuthExpressionList){
								expression.setId(null);
								expression.setBusinessId(newDetailId);
								expression.setAuthExpressionId(expressionIdMap.get(expression.getAuthExpressionId()));
							}
							sysBussinessAuthExpressionService.insert(bussinessAuthExpressionList);
						}
					}
				}
			}
		}
		
		//保存一份流程图设计
		if(processDefinition.getProcessDefinitionFile()!=null){
			String fileString=processDefinition.getProcessDefinitionFile();
			fileString=fileString.replaceAll(oldPafId,newPafId);
			processDefinition.setProcessDefinitionFile(fileString);
			processDefService.saveProcessDefinitionToJss(processDefinition);   //保存流程图到JSS
		}
		
	}
	
	/**
	 * 解析流程对应的权限表达式
	 * @param packageElement
	 * @param dto
	 */
	private void parseSysBusinessAuthExpression4Process(Package packageElement,
			ProcessDefinitionTransferDto dto) {
		List<SysBussinessAuthExpression> sysBussinessAuthExpressions=new ArrayList<SysBussinessAuthExpression>();
		SysBussinessAuthExpressions  sysBussinessAuthExpressionsAttribute=packageElement.getSysBussinessAuthExpressions();
		if(sysBussinessAuthExpressionsAttribute == null) return;
		SysBussinessAuthExpressionDocument.SysBussinessAuthExpression[] sysBussinessAuthExpressionsArray=sysBussinessAuthExpressionsAttribute.getSysBussinessAuthExpressionArray();
		if(sysBussinessAuthExpressionsArray == null ||sysBussinessAuthExpressionsArray.length == 0) return;
		SysBussinessAuthExpression expression;
		for (int i = 0; i < sysBussinessAuthExpressionsArray.length; i++) {
			expression=new SysBussinessAuthExpression();
			expression.setAuthExpressionId(sysBussinessAuthExpressionsArray[i].getAuthExpressionId());
			expression.setBusinessId(sysBussinessAuthExpressionsArray[i].getBusinessId());
			expression.setBusinessType(sysBussinessAuthExpressionsArray[i].getBusinessType());
			
			sysBussinessAuthExpressions.add(expression);
		}
		dto.setSysBussinessAuthExpressions(sysBussinessAuthExpressions);
		
	}
	/**
	 * 解析流程配置的关键字段
	 * @param packageElement
	 * @param dto
	 */
	private void parseProcessDefinitionConfigs(Package packageElement,
			ProcessDefinitionTransferDto dto) {
		List<ProcessDefinitionConfig> processDefinitionConfigs=new ArrayList<ProcessDefinitionConfig>();
		ProcessDefinitionConfigs processDefinitionConfigsAttribute=packageElement.getProcessDefinitionConfigs();
		if(processDefinitionConfigsAttribute == null) return;
		ProcessDefinitionConfigDocument.ProcessDefinitionConfig[] processDefinitionConfigsArray=processDefinitionConfigsAttribute.getProcessDefinitionConfigArray();
		if(processDefinitionConfigsArray == null || processDefinitionConfigsArray.length == 0) return;
		ProcessDefinitionConfig config;
		for (int i = 0; i < processDefinitionConfigsArray.length; i++) {
			config=new ProcessDefinitionConfig();
			config.setConfigType(processDefinitionConfigsArray[i].getConfigType());
			config.setConfigItem(processDefinitionConfigsArray[i].getConfigItem());
			config.setProcessDefinitionId(processDefinitionConfigsArray[i].getProcessDefinitionId());
			config.setYn(processDefinitionConfigsArray[i].getYn());
			config.setSortNo(processDefinitionConfigsArray[i].getSortNo());
			processDefinitionConfigs.add(config);
			//取流程变量
			ProcessDefinitionConfigDetailsDocument.ProcessDefinitionConfigDetails[]  configDetails=processDefinitionConfigsArray[i].getProcessDefinitionConfigDetailsArray();
			List<ProcessDefinitionConfigDetail> processDefinitionConfigDetailList=new ArrayList<ProcessDefinitionConfigDetail>();
			for(int j=0;j<configDetails.length;j++){
				ProcessDefinitionConfigDetailDocument.ProcessDefinitionConfigDetail[] detailArray=configDetails[j].getProcessDefinitionConfigDetailArray();
				for(int k=0;k<detailArray.length;k++){
					ProcessDefinitionConfigDetail cd=new ProcessDefinitionConfigDetail();
					cd.setId(detailArray[k].getId());
					cd.setName(detailArray[k].getName());
					cd.setExpression(detailArray[k].getExpression());
					cd.setConfigId(detailArray[k].getConfigId());
					cd.setValNo(detailArray[k].getValNo());
					cd.setYn(detailArray[k].getYn());
					processDefinitionConfigDetailList.add(cd);
					//赋值流程变量的权限表达式
					SysBussinessAuthExpressionsDocument.SysBussinessAuthExpressions[] sysBussinessAuthExpressionsArray=detailArray[k].getSysBussinessAuthExpressionsArray();
					List<SysBussinessAuthExpression> bussinessAuthExpressionList=new ArrayList<SysBussinessAuthExpression>();
					for(int m=0;m<sysBussinessAuthExpressionsArray.length;m++){
						SysBussinessAuthExpressionDocument.SysBussinessAuthExpression[] expressionArray=sysBussinessAuthExpressionsArray[0].getSysBussinessAuthExpressionArray();
						for(int n=0;n<expressionArray.length;n++){
							SysBussinessAuthExpression expression=new SysBussinessAuthExpression();
							expression.setId(expressionArray[n].getId());
							expression.setBusinessType(expressionArray[n].getBusinessType());
							expression.setBusinessId(expressionArray[n].getBusinessId());
							expression.setAuthExpressionId(expressionArray[n].getAuthExpressionId());
							bussinessAuthExpressionList.add(expression);
						}
					}
					dto.getSysBusinessExpressions4ConfigDetailMap().put(cd, bussinessAuthExpressionList);
				}
			}
			dto.getConfigDetails4ProcessdefinitionConfigMap().put(config,processDefinitionConfigDetailList);
		}
		dto.setProcessDefinitionConfigs(processDefinitionConfigs);
		
	}
	/**
	 * 解析流程节点的个性化
	 * @param packageElement
	 * @param dto
	 */
	private void parseProcessNodeFormPrivileges(Package packageElement,
			ProcessDefinitionTransferDto dto) {
		List<ProcessNodeFormPrivilege> processNodeFormPrivileges=new ArrayList<ProcessNodeFormPrivilege>();
		ProcessNodeFormPrivileges processNodeFormPrivilegesAttribute=packageElement.getProcessNodeFormPrivileges();
		if(processNodeFormPrivilegesAttribute == null) return;
		ProcessNodeFormPrivilegeDocument.ProcessNodeFormPrivilege[] processNodeFormPrivilegesArray=processNodeFormPrivilegesAttribute.getProcessNodeFormPrivilegeArray();
		if(processNodeFormPrivilegesArray == null || processNodeFormPrivilegesArray.length == 0) return;
		ProcessNodeFormPrivilege privilege;
		for(int i=0;i<processNodeFormPrivilegesArray.length;i++){
			//设值
			privilege=new ProcessNodeFormPrivilege();
			privilege.setItemId(processNodeFormPrivilegesArray[i].getItemId());
			privilege.setFormId(processNodeFormPrivilegesArray[i].getFormId());
			privilege.setProcessDefinitionId(processNodeFormPrivilegesArray[i].getProcessDefinitionId());
			privilege.setNodeId(processNodeFormPrivilegesArray[i].getNodeId());
			privilege.setEdit(processNodeFormPrivilegesArray[i].getIsEdit());
			privilege.setHidden(processNodeFormPrivilegesArray[i].getIsHidden());
			privilege.setVariable(processNodeFormPrivilegesArray[i].getIsVariable());
			privilege.setNull(processNodeFormPrivilegesArray[i].getIsNull());
			privilege.setYn(processNodeFormPrivilegesArray[i].getYn());
			//加入List
			processNodeFormPrivileges.add(privilege);
		}
		dto.setProcessNodeFormPrivileges(processNodeFormPrivileges);
	}
	/**
	 * 解析流程节点的动态按钮配置
	 * @param packageElement
	 * @param dto
	 */
	private void parseProcessNodeExtendButtons(Package packageElement,
			ProcessDefinitionTransferDto dto){
		List<ProcessNodeExtendButton> processNodeExtendButtons = new ArrayList<ProcessNodeExtendButton>();
		ProcessNodeExtendButtons  processNodeExtendButtonsAttribute = packageElement.getProcessNodeExtendButtons();
		if(null == processNodeExtendButtonsAttribute) return;
		ProcessNodeExtendButtonDocument.ProcessNodeExtendButton[] processNodeExtendButtonsArray = 
				processNodeExtendButtonsAttribute.getProcessNodeExtendButtonArray();
		if(null == processNodeExtendButtonsArray || processNodeExtendButtonsArray.length == 0) return;
		ProcessNodeExtendButton extendButton;
		for(int i=0; i<processNodeExtendButtonsArray.length;i++){
			extendButton = new ProcessNodeExtendButton();
			extendButton.setProcessDefinitionId(processNodeExtendButtonsArray[i].getProcessDefinitionId());
			extendButton.setNodeId(processNodeExtendButtonsArray[i].getNodeId());
			extendButton.setFormId(processNodeExtendButtonsArray[i].getFormId());
			extendButton.setIsExtendButton(processNodeExtendButtonsArray[i].getIsExtendButton());
			extendButton.setButtonName(processNodeExtendButtonsArray[i].getButtonName());
			extendButton.setButtonExegesis(processNodeExtendButtonsArray[i].getButtonExegesis());
			extendButton.setFunctionName(processNodeExtendButtonsArray[i].getFunctionName());
			extendButton.setStatus(processNodeExtendButtonsArray[i].getStatus());
			extendButton.setOrderNo(processNodeExtendButtonsArray[i].getOrderNo());
			extendButton.setYn(processNodeExtendButtonsArray[i].getYn());
			processNodeExtendButtons.add(extendButton);
		}
		dto.setProcessNodeExtendButtons(processNodeExtendButtons);
	}
	/**
	 * 解析流程结点事件接口
	 * @param packageElement
	 * @param dto
	 */
	private void parseProcessNodeListeners(Package packageElement,
			ProcessDefinitionTransferDto dto){
		List<ProcessNodeListener> nodeListeners = new ArrayList<ProcessNodeListener>();
		ProcessNodeListeners processNodeListenerAttribute = packageElement.getProcessNodeListeners();
		if(null == processNodeListenerAttribute) return;
		ProcessNodeListenerDocument.ProcessNodeListener[] processNodeListenerArray = 
				processNodeListenerAttribute.getProcessNodeListenerArray();
		if(null == processNodeListenerArray || processNodeListenerArray.length == 0) return;
		ProcessNodeListener processNodeListener;
		for(int i=0; i<processNodeListenerArray.length; i++){
			processNodeListener = new ProcessNodeListener();
			processNodeListener.setProcessDefinitionId(processNodeListenerArray[i].getProcessDefinitionId());
			processNodeListener.setNodeId(processNodeListenerArray[i].getNodeId());
			processNodeListener.setEvent(processNodeListenerArray[i].getEvent());
			processNodeListener.setInterfaceName(processNodeListenerArray[i].getInterfaceName());
			processNodeListener.setYn(processNodeListenerArray[i].getYn());
			nodeListeners.add(processNodeListener);
		}
		dto.setProcessNodeListeners(nodeListeners);
	}
	/**
	 * 解析数据源
	 * @param packageElement
	 * @param dto
	 */
	private void parseDiDatasources(Package packageElement,
			ProcessDefinitionTransferDto dto){
		List<DiDatasource> diDatasources = new ArrayList<DiDatasource>();
		DiDatasources diDatasourceAttribute = packageElement.getDiDatasources();
		if(null == diDatasourceAttribute) return;
		DiDatasourceDocument.DiDatasource[] diDatasourceArray = 
				diDatasourceAttribute.getDiDatasourceArray();
		if(null == diDatasourceArray || diDatasourceArray.length == 0) return;
		DiDatasource diDatasource;
		for(int i=0; i<diDatasourceArray.length; i++){
			diDatasource = new DiDatasource();
			diDatasource.setId(diDatasourceArray[i].getId());
			diDatasource.setName(diDatasourceArray[i].getName());
			diDatasource.setDataAdapter(diDatasourceArray[i].getDataAdapter());
			diDatasource.setDataAdapterType(diDatasourceArray[i].getDataAdapterType());
			diDatasource.setYn(diDatasourceArray[i].getYn());
			diDatasources.add(diDatasource);
		}
		dto.setDiDataSources(diDatasources);
	}
	/**
	 * 解析接口
	 * @param packageElement
	 * @param dto
	 */
	private void parseDiCcInterfaces(Package packageElement,
			ProcessDefinitionTransferDto dto){
		List<DiCcInterface> diCcInterfaces = new ArrayList<DiCcInterface>();
		DiCcInterfaces diCcInterfaceAttribute = packageElement.getDiCcInterfaces();
		if(null == diCcInterfaceAttribute) return;
		DiCcInterfaceDocument.DiCcInterface[] diCcInterfaceArray = 
				diCcInterfaceAttribute.getDiCcInterfaceArray();
		if(null == diCcInterfaceArray || diCcInterfaceArray.length == 0) return;
		DiCcInterface diCcInterface;
		for(int i=0; i<diCcInterfaceArray.length; i++){
			diCcInterface = new DiCcInterface();
			diCcInterface.setId(diCcInterfaceArray[i].getId());
			diCcInterface.setInterfaceType(diCcInterfaceArray[i].getInterfaceType());
			diCcInterface.setInterfaceName(diCcInterfaceArray[i].getInterfaceName());
			diCcInterface.setInterfaceDesc(diCcInterfaceArray[i].getInterfaceDesc());
			diCcInterface.setUrl(diCcInterfaceArray[i].getUrl());
			diCcInterface.setMethod(diCcInterfaceArray[i].getMethod());
			diCcInterface.setAuthHeader(diCcInterfaceArray[i].getAuthHeader());
			diCcInterface.setInParameterType(diCcInterfaceArray[i].getInParameterType());
			diCcInterface.setInParameterTemplate(diCcInterfaceArray[i].getInParameterTemplate());
			diCcInterface.setOutParameterType(diCcInterfaceArray[i].getOutParameterType());
			diCcInterface.setOutParameterTemplate(diCcInterfaceArray[i].getOutParameterTemplate());
			diCcInterface.setYn(diCcInterfaceArray[i].getYn());
			diCcInterfaces.add(diCcInterface);
		}
		dto.setDiCcInterfaces(diCcInterfaces);
	}
	/**
	 * 节点对应的formtemplate
	 * @param node
	 */
	private void getFormTemplate(Node node,ProcessNode processNode,ProcessDefinitionTransferDto dto){
		com.jd.oa.form.model.FormTemplate formTemplate = new com.jd.oa.form.model.FormTemplate();
		FormTemplate[] formTemplateArray = node.getFormTemplateArray();
		if(formTemplateArray == null || formTemplateArray.length == 0) return;
		for(int i = 0 ; i < formTemplateArray.length ; i++){
			formTemplate.setId(formTemplateArray[i].getId());
			formTemplate.setFormId(formTemplateArray[i].getFormId());
			formTemplate.setTemplateDesc(formTemplateArray[i].getTemplateDesc());
			formTemplate.setYn(formTemplateArray[i].getYn());
			formTemplate.setTemplateName(formTemplateArray[i].getTemplateName());
			formTemplate.setTemplatePath(formTemplateArray[i].getTemplatePath());
			formTemplate.setTemplateType(formTemplateArray[i].getTemplatePath());
		}
		
		dto.getFormTemplate4ProcessNodeMap().put(processNode, formTemplate);
	}
	/**
	 * 节点对应的printtemplate
	 * @param node
	 */
	private void getPrintTemplate(Node node,ProcessNode processNode,ProcessDefinitionTransferDto dto){
		com.jd.oa.form.model.FormTemplate formTemplate = new com.jd.oa.form.model.FormTemplate();
		PrintTemplate[] printTemplateArray = node.getPrintTemplateArray();
		if(printTemplateArray == null || printTemplateArray.length == 0) return; 
		for(int i = 0 ; i < printTemplateArray.length ; i++){
			formTemplate.setId(printTemplateArray[i].getId());
			formTemplate.setFormId(printTemplateArray[i].getFormId());
			formTemplate.setTemplateDesc(printTemplateArray[i].getTemplateDesc());
			formTemplate.setYn(printTemplateArray[i].getYn());
			formTemplate.setTemplateName(printTemplateArray[i].getTemplateName());
			formTemplate.setTemplatePath(printTemplateArray[i].getTemplatePath());
			formTemplate.setTemplateType(printTemplateArray[i].getTemplatePath());
		}
		dto.getFormTemplate4ProcessNodeMap().put(processNode, formTemplate);
	}
	/**
	 * 节点对应的权限表达式
	 * @param node
	 * @param processNode
	 * @param dto
	 */
	private void getSysBusinessAuthExpression4ProcessNode(Node node,ProcessNode processNode,ProcessDefinitionTransferDto dto){
		List<com.jd.oa.system.model.SysBussinessAuthExpression> sysBussinessAuthExpressions=new ArrayList<com.jd.oa.system.model.SysBussinessAuthExpression>();
		SysBussinessAuthExpressions[] sysBussinessAuthExpressionsArray=node.getSysBussinessAuthExpressionsArray();
		if(sysBussinessAuthExpressionsArray == null ||sysBussinessAuthExpressionsArray.length == 0) return;
		com.jd.oa.system.model.SysBussinessAuthExpression sysBussinessAuthExpression;
		for(int i = 0 ; i < sysBussinessAuthExpressionsArray.length ; i++){
			for(int j=0;j<sysBussinessAuthExpressionsArray[i].getSysBussinessAuthExpressionArray().length;j++){
				sysBussinessAuthExpression=new SysBussinessAuthExpression();
				sysBussinessAuthExpression.setId(sysBussinessAuthExpressionsArray[i].getSysBussinessAuthExpressionArray()[j].getId());
				sysBussinessAuthExpression.setBusinessId(sysBussinessAuthExpressionsArray[i].getSysBussinessAuthExpressionArray()[j].getBusinessId());
				sysBussinessAuthExpression.setBusinessType(sysBussinessAuthExpressionsArray[i].getSysBussinessAuthExpressionArray()[j].getBusinessType());
				sysBussinessAuthExpression.setAuthExpressionId(sysBussinessAuthExpressionsArray[i].getSysBussinessAuthExpressionArray()[j].getAuthExpressionId());
				sysBussinessAuthExpressions.add(sysBussinessAuthExpression);
			}
		}
		dto.getSysBussinessAuthExpression4ProcessNodeMap().put(processNode,sysBussinessAuthExpressions);
	}
	/**
	 * 解析流程节点
	 * @param packageElement
	 * @param dto
	 */
	private void parseProcessNodes(Package packageElement,
			ProcessDefinitionTransferDto dto) {
		List<ProcessNode> nodes = new ArrayList<ProcessNode>();
		Nodes nodesAttribute = packageElement.getNodes();
		if(nodesAttribute == null) return;
		Node[] nodeArray = nodesAttribute.getNodeArray();
		if(nodeArray==null || nodeArray.length == 0) return;
		for (int i = 0; i < nodeArray.length; i++) {
			Node node = nodeArray[i];
			if(node != null){
				ProcessNode processNode = new ProcessNode();
				processNode.setId(node.getId());
				processNode.setProcessDefinitionId(node.getProcessDefinitionId());
				processNode.setNodeId(node.getNodeId());
				processNode.setFormTemplateId(node.getFormTemplateId());
				processNode.setPrintTemplateId(node.getPrintTemplateId());
				processNode.setIsFirstNode(node.getIsFirstNode());
				processNode.setAddsignRule(node.getAddsignRule());
				processNode.setManagerNodeId(node.getManagerNodeId());
				processNode.setCandidateUser(node.getCandidateUser());
				processNode.setYn(node.getYn());
				nodes.add(processNode);
				/**
				 * 加载node 对应的 formTemplate
				 */
				getFormTemplate(node,processNode,dto);
				/**
				 * 加载node 对应的 printTemplate
				 */
				getPrintTemplate(node,processNode,dto);
				/**
				 * 加载node对应的权限表达式
				 */
				getSysBusinessAuthExpression4ProcessNode(node,processNode,dto);
			}
		}
		dto.setProcessNodes(nodes);
	}
	//解析流程
	private void parseProcessDefinition(Package packageElement,ProcessDefinitionTransferDto dto) {
		PackageHeader packageHeaderAttribute = packageElement.getPackageHeader();
		if(packageHeaderAttribute==null) return;
		ProcessDefinition processDefinition=new ProcessDefinition();
		processDefinition.setProcessTypeId(packageHeaderAttribute.getProcessTypeId());
		processDefinition.setPafProcessDefinitionId(packageHeaderAttribute.getPafProcessDefinitionId());
		processDefinition.setProcessDefinitionName(packageHeaderAttribute.getProcessDefinitionName());
		processDefinition.setOrganizationType(packageHeaderAttribute.getOrganizationType());
		processDefinition.setFilePath(packageHeaderAttribute.getFilePath());
		processDefinition.setIsIssue(packageHeaderAttribute.getIsIssue());
		processDefinition.setFormId(packageHeaderAttribute.getFormId());
		processDefinition.setAlertMode(packageHeaderAttribute.getAlertMode());
		processDefinition.setIsComment(packageHeaderAttribute.getIsComment());
		processDefinition.setIsUploadAttach(packageHeaderAttribute.getIsUploadAttach());
		processDefinition.setIsRelateDoc(packageHeaderAttribute.getIsRelateDoc());
		processDefinition.setIsRelateProcessInstance(packageHeaderAttribute.getIsRelateProcessInstance());
		processDefinition.setIsTemplate(packageHeaderAttribute.getIsTemplate());
		processDefinition.setIsManagerInspect(packageHeaderAttribute.getIsManagerInspect());
		processDefinition.setIsOuter(packageHeaderAttribute.getIsOuter());
		processDefinition.setLinkUrl(packageHeaderAttribute.getLinkUrl());
		processDefinition.setSortNo(packageHeaderAttribute.getSortNo());
		processDefinition.setYn(packageHeaderAttribute.getYn());
		processDefinition.setOwner(packageHeaderAttribute.getOwner());
		processDefinition.setTopLevelCode(packageHeaderAttribute.getTopLevelCode());
		processDefinition.setFollowCodePrefix(packageHeaderAttribute.getFollowCodePrefix());
		processDefinition.setDataSourceId(packageHeaderAttribute.getDataSourceId());
		processDefinition.setRejectDataSourceId(packageHeaderAttribute.getRejectDataSourceId());
		dto.setProcessDefinition(processDefinition);
	}
	public PackageDocument parseXml(String file) {
		File xmlfile = new File(file);
		PackageDocument doc = null;
		try {
			doc = PackageDocument.Factory.parse(xmlfile);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.out.println("解析xml模型文件格式时失败，期望格式是一个符合BPDL-Process.xsd格式的xml文件，错误信息参考：" + e.toString());
		} 
		return doc;
	}

}
