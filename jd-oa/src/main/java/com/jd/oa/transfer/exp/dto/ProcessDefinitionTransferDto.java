package com.jd.oa.transfer.exp.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jd.cachecloud.driver.javassist.expr.NewArray;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeExtendButton;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.model.ProcessNodeListener;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;

public class ProcessDefinitionTransferDto {

	private ProcessDefinition processDefinition; // 流程定义

	private List<ProcessNode> processNodes; // 流程结点

	private Map<ProcessNode, List<SysBussinessAuthExpression>> sysBussinessAuthExpression4ProcessNodeMap 
		= new HashMap<ProcessNode, List<SysBussinessAuthExpression>>(); // 结点办理人的权限表达式

	private Map<ProcessNode, FormTemplate> formTemplate4ProcessNodeMap = new HashMap<ProcessNode, FormTemplate>(); // 表单模版

	private Map<ProcessNode, FormTemplate> printTemplate4ProcessNodeMap = new HashMap<ProcessNode, FormTemplate>(); // 打印模板

	private List<ProcessNodeFormPrivilege> processNodeFormPrivileges; // 结点个性化
	
	private List<ProcessNodeExtendButton> processNodeExtendButtons; // 结点扩展按钮配置

	private List<SysBussinessAuthExpression> sysBussinessAuthExpressions; // 流程的授权，加签,督办权限表达式
	
	private List<SysAuthExpression> sysAuthExpressions; //这条流程使用到的权限表达式

	private List<ProcessDefinitionConfig> processDefinitionConfigs; // 流程的关键字段
	
	private Map<ProcessDefinitionConfig,List<ProcessDefinitionConfigDetail>>  configDetails4ProcessdefinitionConfigMap 
		= new HashMap<ProcessDefinitionConfig, List<ProcessDefinitionConfigDetail>>();   //流程变量
	
	private Map<ProcessDefinitionConfigDetail,List<SysBussinessAuthExpression>> sysBusinessExpressions4ConfigDetailMap 
		= new HashMap<ProcessDefinitionConfigDetail, List<SysBussinessAuthExpression>>();  //流程变量对应的办理人权限表达式映射
	// yjh
	private List<FormTemplate> formTemplates; // 表单绑定的模板包括UI模板和打印模板


	private FormInfoTransferDto formInfoTransferDto = new FormInfoTransferDto();//表单相关的数据

	private List<FormInfoTransferDto> SonFormInfoTransferDto = new ArrayList<FormInfoTransferDto>();
	
	private List<ProcessNodeListener> processNodeListeners; //流程节点事件接口
	
	private List<DiDatasource> diDataSources;//数据源
	
	private List<DiCcInterface> diCcInterfaces;//接口

	//

	public ProcessDefinition getProcessDefinition() {
		return processDefinition;
	}

	public void setProcessDefinition(ProcessDefinition processDefinition) {
		this.processDefinition = processDefinition;
	}

	public List<ProcessNode> getProcessNodes() {
		return processNodes;
	}

	public void setProcessNodes(List<ProcessNode> processNodes) {
		this.processNodes = processNodes;
	}

	public Map<ProcessNode, List<SysBussinessAuthExpression>> getSysBussinessAuthExpression4ProcessNodeMap() {
		return sysBussinessAuthExpression4ProcessNodeMap;
	}

	public void setSysBussinessAuthExpression4ProcessNodeMap(
			Map<ProcessNode, List<SysBussinessAuthExpression>> sysBussinessAuthExpression4ProcessNodeMap) {
		this.sysBussinessAuthExpression4ProcessNodeMap = sysBussinessAuthExpression4ProcessNodeMap;
	}

	public List<ProcessNodeFormPrivilege> getProcessNodeFormPrivileges() {
		return processNodeFormPrivileges;
	}

	public void setProcessNodeFormPrivileges(
			List<ProcessNodeFormPrivilege> processNodeFormPrivileges) {
		this.processNodeFormPrivileges = processNodeFormPrivileges;
	}

	public List<SysBussinessAuthExpression> getSysBussinessAuthExpressions() {
		return sysBussinessAuthExpressions;
	}

	public void setSysBussinessAuthExpressions(
			List<SysBussinessAuthExpression> sysBussinessAuthExpressions) {
		this.sysBussinessAuthExpressions = sysBussinessAuthExpressions;
	}

	public List<ProcessDefinitionConfig> getProcessDefinitionConfigs() {
		return processDefinitionConfigs;
	}

	public void setProcessDefinitionConfigs(
			List<ProcessDefinitionConfig> processDefinitionConfigs) {
		this.processDefinitionConfigs = processDefinitionConfigs;
	}

	public Map<ProcessNode, FormTemplate> getFormTemplate4ProcessNodeMap() {
		return formTemplate4ProcessNodeMap;
	}

	public Map<ProcessDefinitionConfig, List<ProcessDefinitionConfigDetail>> getConfigDetails4ProcessdefinitionConfigMap() {
		return configDetails4ProcessdefinitionConfigMap;
	}

	public void setConfigDetails4ProcessdefinitionConfigMap(
			Map<ProcessDefinitionConfig, List<ProcessDefinitionConfigDetail>> configDetails4ProcessdefinitionConfigMap) {
		this.configDetails4ProcessdefinitionConfigMap = configDetails4ProcessdefinitionConfigMap;
	}

	public void setFormTemplate4ProcessNodeMap(
			Map<ProcessNode, FormTemplate> formTemplate4ProcessNodeMap) {
		this.formTemplate4ProcessNodeMap = formTemplate4ProcessNodeMap;
	}

	public Map<ProcessNode, FormTemplate> getPrintTemplate4ProcessNodeMap() {
		return printTemplate4ProcessNodeMap;
	}

	public void setPrintTemplate4ProcessNodeMap(
			Map<ProcessNode, FormTemplate> printTemplate4ProcessNodeMap) {
		this.printTemplate4ProcessNodeMap = printTemplate4ProcessNodeMap;
	}

	public List<FormTemplate> getFormTemplates() {
		return formTemplates;
	}

	public void setFormTemplates(List<FormTemplate> formTemplates) {
		this.formTemplates = formTemplates;
	}
	
	public Map<ProcessDefinitionConfigDetail, List<SysBussinessAuthExpression>> getSysBusinessExpressions4ConfigDetailMap() {
		return sysBusinessExpressions4ConfigDetailMap;
	}

	public void setSysBusinessExpressions4ConfigDetailMap(
			Map<ProcessDefinitionConfigDetail, List<SysBussinessAuthExpression>> sysBusinessExpressions4ConfigDetailMap) {
		this.sysBusinessExpressions4ConfigDetailMap = sysBusinessExpressions4ConfigDetailMap;
	}

	public FormInfoTransferDto getFormInfoTransferDto() {
		return formInfoTransferDto;
	}

	public void setFormInfoTransferDto(FormInfoTransferDto formInfoTransferDto) {
		this.formInfoTransferDto = formInfoTransferDto;
	}

	public List<FormInfoTransferDto> getSonFormInfoTransferDto() {
		return SonFormInfoTransferDto;
	}

	public void setSonFormInfoTransferDto(
			List<FormInfoTransferDto> sonFormInfoTransferDto) {
		SonFormInfoTransferDto = sonFormInfoTransferDto;
	}
	/**
	 * 主表比对，表单Form FormItem
	 * @param mainFormInfo
	 * @return
	 */
	public boolean equalMainForm(FormInfoTransferDto mainFormInfo){
		if (this.formInfoTransferDto.equalForm(mainFormInfo.getForm())
				&&this.formInfoTransferDto.equalFormItemList(mainFormInfo.getFormItems())) {
			
			return true;

		} else {
			return false;
		}
		
	}
	/**
	 * 主表对比 formBusinessTable formBusinessField
	 * @param mainFormInfo
	 * @return
	 */
	public boolean equalMainFormBusiness(FormInfoTransferDto mainFormInfo){
		if (this.formInfoTransferDto.equalFormBusinessTable(mainFormInfo.getFormBusinessTable())
				&&this.formInfoTransferDto.equalFormBusinessFieldList(mainFormInfo.getFormBusinessFields())) {
			
			return true;

		} else {
			return false;
		}
		
	}

	
	/**
	 * 子表比对，表单Form FormItem
	 * @param subFormInfo
	 * @return
	 */
	public boolean equalSubForm(List<FormInfoTransferDto> subFormInfo){
		if (this.SonFormInfoTransferDto.size() == subFormInfo.size()) {
			for (FormInfoTransferDto sf : subFormInfo) {
				if ( this.containsSubForm(sf)) {
					continue;
				} else {
					return false;
				}
			}
			return true;

		} else {
			return false;
		}
		
	}
	public boolean containsSubForm(FormInfoTransferDto subFormInfo){
		for (FormInfoTransferDto sf : this.SonFormInfoTransferDto) {
			if (sf.equalForm(subFormInfo.getForm())&&
					sf.equalFormItemList(subFormInfo.getFormItems())) {
				continue;
			} else {
				return false;
			}

		}
		return true;
	}
	/**
	 * 子表比对 业务表部分
	 * @param subFormInfo
	 * @return
	 */
	public boolean equalSubFormBusiness(List<FormInfoTransferDto> subFormInfo){
		if (this.SonFormInfoTransferDto.size() == subFormInfo.size()) {
			for (FormInfoTransferDto sf : subFormInfo) {
				if ( this.containsSubFormBusiness(sf)) {
					continue;
				} else {
					return false;
				}
			}
			return true;

		} else {
			return false;
		}
		
	}
	public boolean containsSubFormBusiness(FormInfoTransferDto subFormInfo){
		for (FormInfoTransferDto sf : this.SonFormInfoTransferDto) {
			if (sf.equalFormBusinessTable(subFormInfo.getFormBusinessTable())&&
					sf.equalFormBusinessFieldList(subFormInfo.getFormBusinessFields())
					) {
				continue;
			} else {
				return false;
			}

		}
		return true;
	}

	public List<SysAuthExpression> getSysAuthExpressions() {
		return sysAuthExpressions;
	}

	public void setSysAuthExpressions(List<SysAuthExpression> sysAuthExpressions) {
		this.sysAuthExpressions = sysAuthExpressions;
	}

	public List<ProcessNodeExtendButton> getProcessNodeExtendButtons() {
		return processNodeExtendButtons;
	}

	public void setProcessNodeExtendButtons(
			List<ProcessNodeExtendButton> processNodeExtendButtons) {
		this.processNodeExtendButtons = processNodeExtendButtons;
	}

	public List<ProcessNodeListener> getProcessNodeListeners() {
		return processNodeListeners;
	}

	public void setProcessNodeListeners(
			List<ProcessNodeListener> processNodeListeners) {
		this.processNodeListeners = processNodeListeners;
	}

	public List<DiDatasource> getDiDataSources() {
		return diDataSources;
	}

	public void setDiDataSources(List<DiDatasource> diDataSources) {
		this.diDataSources = diDataSources;
	}

	public List<DiCcInterface> getDiCcInterfaces() {
		return diCcInterfaces;
	}

	public void setDiCcInterfaces(List<DiCcInterface> diCcInterfaces) {
		this.diCcInterfaces = diCcInterfaces;
	}

}
