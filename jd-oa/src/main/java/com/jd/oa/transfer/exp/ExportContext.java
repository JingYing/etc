package com.jd.oa.transfer.exp;

import org.apache.xmlbeans.XmlOptions;

public class ExportContext {
	/**
	 * 获取生成xml文件的默认选项
	 */
	public static XmlOptions getDefaultXmlOptions(){
        XmlOptions xmlOptions = new XmlOptions();
        xmlOptions.setCharacterEncoding("GB2312");
        xmlOptions.setSavePrettyPrint();
        xmlOptions.setSavePrettyPrintIndent(5);	
        return xmlOptions;
	}
	
	/**
	 * 模型版本号
	 * @return
	 */
	public static String getVersion(){
		return "1.0";
	}
	
	/**
	 * 提供商
	 * @return
	 */
	public static String getVendor(){
		return "http://oa.jd.com";
	}
}
