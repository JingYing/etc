package com.jd.oa.app.service.impl;

import com.jd.oa.app.dao.ProcessTaskTimerDao;
import com.jd.oa.app.model.ProcessTaskTimer;
import com.jd.oa.app.service.ProcessTaskTimerService;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @desc 
 * @author WXJ
 * @date 2014-6-16 下午10:20:03
 *
 */
@Service
public class ProcessTaskTimerServiceImpl extends BaseServiceImpl<ProcessTaskTimer,String> implements ProcessTaskTimerService {

    @Autowired
    private ProcessTaskTimerDao processTaskTimerDao;

	@Override
	public BaseDao<ProcessTaskTimer, String> getDao() {
		return processTaskTimerDao;
	}
	
}
