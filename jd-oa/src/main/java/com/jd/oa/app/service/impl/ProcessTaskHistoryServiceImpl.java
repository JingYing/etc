package com.jd.oa.app.service.impl;

import com.jd.oa.app.dao.ProcessTaskHistoryDao;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.ProcessTaskHistoryService;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 任务历史Service服务类
 * User: zhaoming
 * Date: 13-10-22
 * Time: 下午9:05
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ProcessTaskHistoryServiceImpl extends BaseServiceImpl<ProcessTaskHistory,String> implements ProcessTaskHistoryService {

    @Autowired
    private ProcessTaskHistoryDao processTaskHistoryDao;

	@Override
	public BaseDao<ProcessTaskHistory, String> getDao() {
		return processTaskHistoryDao;
	}

	/**
     * 查询流程实例已结案的任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectEndProcessTaskHistory(ProcessTaskHistory processTaskHistory){
        return processTaskHistoryDao.selectEndProcessTaskHistory(processTaskHistory);
    }

    /**
     * 查询流程实例未结案的任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectProcessTaskHistory(ProcessTaskHistory processTaskHistory){
        return processTaskHistoryDao.selectProcessTaskHistory(processTaskHistory);
    }

    /**
     * 根据条件查询任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectByCondition(ProcessTaskHistory processTaskHistory){
        return processTaskHistoryDao.selectByCondition(processTaskHistory);
    }

    /**
     * 新增任务历史日志
     * @param processTaskHistory 任务历史模型类
     */
    public void insertProcessTask(ProcessTaskHistory processTaskHistory){
        processTaskHistoryDao.insertProcessTask(processTaskHistory);
    }
    
    /**
     * 查询流程实例未结案的任务历史(一次性读取多个，按照流程实例和创建时间降序排序)
     * @param List<ProcessTaskHistory> 任务历史模型类
     * @return List<ProcessTaskHistory>
     */
    public Map<String,List<ProcessTaskHistory>> selectProcessTaskHistoryAll(List<String> processInstanceIds) {
    	List<ProcessTaskHistory> resultList = this.processTaskHistoryDao.selectProcessTaskHistoryAll(processInstanceIds);
    	Map<String,List<ProcessTaskHistory>> map = null;
    	if(resultList != null && resultList.size() > 0){
    		map = new HashMap<String, List<ProcessTaskHistory>>(resultList.size());
    		List<ProcessTaskHistory> tempList = null;
    		for(ProcessTaskHistory bean:resultList){
    			if(!map.containsKey(bean.getProcessInstanceId())){
    				tempList = new ArrayList<ProcessTaskHistory>();
    				tempList.add(bean);
    				map.put(bean.getProcessInstanceId(), tempList);
    			} else {
    				tempList.add(bean);
    			}
    		}
    	}
    	return map;
    }

	@Override
	public ProcessTaskHistory getRecentHistoryByNode(ProcessTaskHistory processTaskHistory) {
		// TODO Auto-generated method stub
		return processTaskHistoryDao.getRecentHistoryByNode(processTaskHistory);
	}
}
