package com.jd.oa.app.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jd.oa.app.model.OaProcessInstance;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.model.ProcessProxyTask;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Repository;

import com.jd.oa.app.dao.ProcessTaskDao;
import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.model.ProcessInstance;
/**
 * 待办任务
 * @author wangdongxing
 *
 */
@Repository("processTaskDao")
public class ProcessTaskDaoImpl extends MyBatisDaoImpl<T, String>
		implements ProcessTaskDao {
	/**
	 * 查询流程实例
	 */
	@Override
	public List<ProcessInstance> findProcessInstances(ProcessInstance processInstance) {
		return this.getSqlSession().selectList("com.jd.oa.process.model.ProcessInstance.find", processInstance);
	}

    /**
     * 更加节点ID 查询流程变量
     * @param nodeId
     * @return
     */
    public List<ProcessNodeFormPrivilege> findVariableByNodeId(String processDefinitionId, String nodeId) {
    	Map<String, String> mp = new HashMap<String, String>();
    	mp.put("processDefinitionId", processDefinitionId);
    	mp.put("nodeId", nodeId);
        return this.getSqlSession().selectList("com.jd.oa.app.model.OaTaskInstance.findVariableByNodeId",mp);
    }

    /**
     * 根据paf返回的流程实例ID 更新任务状态
     * @param oaProcessInstance
     */
    public void updateProcessInstance(OaProcessInstance oaProcessInstance) {
         this.getSqlSession().update("com.jd.oa.app.model.OaTaskInstance.updateProcessInstance",oaProcessInstance);
    }
    /**
     * 审批成功后插入任务历史数据
     * @param processTaskHistory
     * @return
     */
    public void taskHistoryAdd(ProcessTaskHistory processTaskHistory) {
        this.getSqlSession().insert("com.jd.oa.app.model.ProcessTaskHistory.insertProcessTask",processTaskHistory);
    }

    /**
     *代理人审批成功后 插入历史记录
     * @param processProxyTask
     */
    public void processProxyTaskAdd(ProcessProxyTask processProxyTask) {
        this.getSqlSession().insert("com.jd.oa.ProcessProxyTaskMapper.insertSelective",processProxyTask);
    }

}
