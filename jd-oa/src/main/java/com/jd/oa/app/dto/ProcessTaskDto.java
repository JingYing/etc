package com.jd.oa.app.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.process.model.ProcessDefinitionConfig;

public class ProcessTaskDto {
	
	private String processDefinitionId;
	private String processDefinitionName;
	private String processDefinitionKey;
	private String tableName;
	private int totalNum;
	private List<String> processColumns=new ArrayList<String>();
	private List<String> bussinessColumns=new ArrayList<String>();
	private List<Map<String,Object>> dataList=new ArrayList<Map<String,Object>>();
	
	public ProcessTaskDto() {
		processColumns.add("主题");
		processColumns.add("申请人");
		processColumns.add("到达时间");
	}
	public ProcessTaskDto(List<ProcessDefinitionConfig> pdfcs) {
		
		processColumns.add("主题");
		processColumns.add("申请人");
		processColumns.add("到达时间");
		
		for(int i=0;i<pdfcs.size();i++){
			bussinessColumns.add(pdfcs.get(i).getConfigItem());
		}
	}
	public void addProcessColumns(String string){
		processColumns.add(string);
	}
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessDefinitionName() {
		return processDefinitionName;
	}

	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}

	public String getProcessDefinitionKey() {
		return processDefinitionKey;
	}

	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}

	public List<String> getProcessColumns() {
		return processColumns;
	}
	public void setProcessColumns(List<String> processColumns) {
		this.processColumns = processColumns;
	}
	public List<String> getBussinessColumns() {
		return bussinessColumns;
	}
	public void setBussinessColumns(List<String> bussinessColumns) {
		this.bussinessColumns = bussinessColumns;
	}
	public List<Map<String, Object>> getDataList() {
		return dataList;
	}

	public int getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	public void addDataList(ProcessSearch search,Object data) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("followCode",search.getFollowCode()==null?"":search.getFollowCode());
		map.put("processInstanceId",search.getProcessInstanceId());
		map.put("processInstanceName",search.getProcessInstanceName());
		map.put("businessId",search.getBusinessInstanceId());
		map.put("taskId",search.getCurrentTaskNoteInfo()==null?"":
				(search.getCurrentTaskNoteInfo().getId()==null?"":search.getCurrentTaskNoteInfo().getId()));
		map.put("taskName",search.getCurrentTaskNoteInfo()==null?"":
				(search.getCurrentTaskNoteInfo().getName()==null?"":search.getCurrentTaskNoteInfo().getName()));
		map.put("nodeId",search.getCurrentTaskNoteInfo()==null?"":
				(search.getCurrentTaskNoteInfo().getNodeId()==null?"":search.getCurrentTaskNoteInfo().getNodeId()));
		map.put("isFirstNode",search.getCurrentTaskNoteInfo()==null?"":search.getCurrentTaskNoteInfo().isFirstNode());
		map.put("processDefinitionIsOuter",search.getCurrentTaskNoteInfo()==null?"0":search.getCurrentTaskNoteInfo().getProcessDefinitionIsOuter());
//		map.put("applyName",search.getRealName()+"("+search.getOrganizationName()+")");
		map.put("applyName",search.getRealName());
		map.put("status",search.getStatus());
		map.put("startTime",search.getCurrentTaskNoteInfo()==null?new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(search.getLastTaskTime()):search.getCurrentTaskNoteInfo().getStartTime());
		map.put("taskType", search.getCurrentTaskNoteInfo()==null?"":search.getCurrentTaskNoteInfo().getTaskType());
		map.put("bySignerUserId",search.getBySignerUserId()==null?"":search.getBySignerUserId());
		map.put("realName",search.getRealName()==null?"":search.getRealName());
		map.put("organizationName",search.getOrganizationName()==null?"":search.getOrganizationName());
		map.put("data",data);
		this.dataList.add(map);
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setDataList(List<Map<String, Object>> dataList) {
		this.dataList = dataList;
	}
	
}
