package com.jd.oa.app.model;

/**
 * 内置流程变量对象
 * User: WXJ
 * Date: 13-10-16
 * Time: 下午6:14
 */
public class Variables {	
	/**
	 * 实例名称(主题)
	 */
	private String processInstanceName;
	
	/**
	 * 关键业务数据
	 */
	private String mailBussinessData;
	
	/**
	 * 邮件标题
	 */
	private String mailSubject;
	/**
	 * 邮件内容
	 */
	private String mailContent;
	/**
	 * 短信标题
	 */
	private String smsSubject;
	/**
	 * 短信内容
	 */
	private String smsContent;
	/**
	 * lync标题
	 */
	private String lyncSubject;
	/**
	 * lync内容
	 */
	private String lyncContent;
	
		
	//Method
	public String getProcessInstanceName() {
		return processInstanceName;
	}
	public void setProcessInstanceName(String processInstanceName) {
		this.processInstanceName = processInstanceName;
	}	
	public String getMailBussinessData() {
		return mailBussinessData;
	}
	public void setMailBussinessData(String mailBussinessData) {
		this.mailBussinessData = mailBussinessData;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailContent() {
		return mailContent;
	}
	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;	
	}
	public String getSmsSubject() {
		return smsSubject;
	}
	public void setSmsSubject(String smsSubject) {
		this.smsSubject = smsSubject;
	}
	public String getSmsContent() {
		return smsContent;
	}
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}
	public String getLyncSubject() {
		return lyncSubject;
	}
	public void setLyncSubject(String lyncSubject) {
		this.lyncSubject = lyncSubject;
	}
	public String getLyncContent() {
		return lyncContent;
	}
	public void setLyncContent(String lyncContent) {
		this.lyncContent = lyncContent;
	}
	
	
	
}
