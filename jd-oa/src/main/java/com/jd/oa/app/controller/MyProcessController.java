package com.jd.oa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.oa.common.utils.MD5Util;
import com.jd.oa.service.http.HttpProcessService;
/**
 * 处理来自portal的请求
 * @author wangdongxing
 *	
 */
@Controller
@RequestMapping(value = "/app/service")
public class MyProcessController {
	
	private final static String CHECK_TOKEN="_JDOA_";
	
	@Autowired
	private HttpProcessService httpProcessService;
	/**
	 * 获取我的申请任务的JSON数据
	 * @param erpId
	 * @return
	 */
	@RequestMapping(value="/getMyApplys",method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getMyApplyJson(String userName,String token,String processType,String size){
		if(userName==null||userName.equals("")) return null;
		if(token==null||token.equals("")) return null;
		if(!isRightUserName(userName, token)){
			return null;
		}
		if(size == null || size.equals("")) size="10";
		return httpProcessService.getMyApplys(userName,processType,Integer.parseInt(size));
	}
	/**
	 * 获取我的待办任务的JSON数据
	 * @param erpId
	 * @return
	 */
	@RequestMapping(value="/getMyTasks",method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getMyTaskJson(String userName,String token,String processType,String size){
		if(userName==null||userName.equals("")) return null;
		if(token==null||token.equals("")) return null;
		if(!isRightUserName(userName, token)){
			return null;
		}
		if(size == null || size.equals("")) size="10";
		return httpProcessService.getMyTasks(userName,processType,Integer.parseInt(size));
	}
	
	private boolean isRightUserName(String userName,String token){
		return token.equals(MD5Util.MD5(CHECK_TOKEN+userName));
	}
}
