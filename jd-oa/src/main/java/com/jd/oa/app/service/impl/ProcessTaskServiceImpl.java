package com.jd.oa.app.service.impl;

import com.jd.common.util.DateFormatUtils;
import com.jd.common.util.StringUtils;
import com.jd.oa.agent.service.AgentBizDataService;
import com.jd.oa.app.dao.ProcessTaskDao;
import com.jd.oa.app.dto.ProcessTaskDto;
import com.jd.oa.app.model.*;
import com.jd.oa.app.service.*;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.mail.MailReceiveService;
import com.jd.oa.common.mail.MailReceiveService.MailFlag;
import com.jd.oa.common.mail.MailSendService;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.*;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.form.execute.FormUIUtil;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.dao.ProcessAddsignerDao;
import com.jd.oa.process.dao.ProcessProxyDao;
import com.jd.oa.process.model.*;
import com.jd.oa.process.service.*;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ServletContextAware;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

@Service("processTaskService")
@SuppressWarnings({"unchecked","rawtypes","unused"})
public class ProcessTaskServiceImpl extends BaseServiceImpl<T, String> implements ProcessTaskService, ServletContextAware {
    private final static Log log = LogFactory.getLog(ProcessTaskServiceImpl.class);

    private String contextPath;

    @Autowired
    private ProcessTaskDao processTaskDao;
    @Autowired
    private ProcessDefinitionConfigService processDefinitionConfigService;
    @Autowired
    private FormService formService;
    @Autowired
    private FormItemService formItemService;
    @Autowired
    private ProcessSearchService processSearchService;
    @Autowired
    private FormBusinessTableService formBusinessTableService;
    @Autowired
    private OaPafService oaPafService;
    @Autowired
    private ProcessNodeService processNodeService;
    @Autowired
    private ProcessInstanceService processInstanceService;
    @Autowired
    private ProcessTaskHistoryService processTaskHistoryService;
    @Autowired
    private ProcessDefService processDefService;
    @Autowired
    private ProcessTypeService processTypeService;
    @Autowired
    private ProcessDefinitionConfigService pdConfigService;

    @Autowired
    MailReceiveService mailReceiveService;
    @Autowired
    ProcessAddsignerDao processAddsignerDao;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysOrganizationService sysOrganizationService;
    @Autowired
    private SysPositionService sysPositionService;

    @Autowired
    private ProcessDefinitionConfigDetailService pdcdService;

    @Autowired
    private MailSendService mailSendService;
    @Autowired
    private DiDatasourceService diDatasourceService;
    @Autowired
    private ProcessNodeListenerService processNodeListenerService ;
	@Autowired
	private SysAuthExpressionService sysAuthExpressionService;
	@Autowired
	private UserAuthorizationService userAuthorizationService;
	@Autowired
	private ProcessProxyDao processProxyDao;
	@Autowired
	private ProcessTaskTimerService processTaskTimerService;
	@Autowired
	private AgentBizDataService agentBizDataService;

    private final String lczxgly="lczxgly";

    @Override
    public BaseDao<T, String> getDao() {
        return processTaskDao;
    }

    /**
     * 我的代理审批任务
     */
	@Override
	public Map<String,Map<String,ProcessTaskDto>> getProxyTodoProcessTaskList(HttpServletRequest request,String pafKey,String byProxyUserId){
        //组织返回数据
        Map<String,Map<String,ProcessTaskDto>>  resultMap=new HashMap<String, Map<String,ProcessTaskDto>>();
        //取得代理任务数据
        Map<String,List<ProcessSearch>> mapProxyTaskList=new HashMap<String, List<ProcessSearch>>();
        Conditions conditions=new Conditions();
        conditions.setSize("5");
        //默认取5条
        if(byProxyUserId!=null&&!byProxyUserId.equals("")){
            byProxyUserId=sysUserService.getUserIdByUserName(byProxyUserId);
        }
        processSearchService.findPrcocessTaskListByUserId(ComUtils.getLoginNamePin(),2,pafKey,byProxyUserId,conditions,mapProxyTaskList);
        //处理任务数据
        for(Entry<String,List<ProcessSearch>> entry:mapProxyTaskList.entrySet()){
            Map<String,ProcessTaskDto> map=new HashMap<String, ProcessTaskDto>();
            String[] byProxyId=entry.getKey().split(",");
            //处理业务数据
            this.handlerMapToGetBusinessData(request,map, entry.getValue());
			this.handlerMapToGetBusinessDataForBizTask(map,entry.getValue());
            Map<String,Object> totalMap=null;
            totalMap=processSearchService.findTodoProcessTaskTotalNum(byProxyId[1]);
            resultMap.put(entry.getKey()+","+totalMap.get("total").toString(),map);
            for(Entry<String,ProcessTaskDto> subEntry:map.entrySet()){
                ProcessTaskDto dto=subEntry.getValue();
                totalMap=processSearchService.findTodoProcessTaskTotalNum(byProxyId[1],dto.getProcessDefinitionKey());
                if(totalMap!=null){
                    dto.setTotalNum(Integer.parseInt(totalMap.get("total").toString()));
                }
            }
        }
        return resultMap;

    }



    /**
     *我的待办任务，包括我的审批和加签给我的
     */
    @Override
    public Map<String,ProcessTaskDto> getTodoProcessTaskList(HttpServletRequest request,String taskType) {
        //返回的数据结构
        Map<String,ProcessTaskDto> map=new HashMap<String, ProcessTaskDto>();

        //取到待办任务的流程信息
        Conditions conditions=new Conditions();
        conditions.setSize("5");
        //我的审批任务
        List<ProcessSearch> processSearchs=null;
        if(taskType==null){
            //取全部任务
            processSearchs = processSearchService.findPrcocessTaskListByUserId(ComUtils.getLoginNamePin(),4,null,null,conditions);
        }else if(taskType.equals(SystemConstant.TASK_KUBU_ADDSIGNER)){
            //取加签任务
            processSearchs = processSearchService.findPrcocessTaskListByUserId(ComUtils.getLoginNamePin(),3,null,null,conditions);
        }else if(taskType.equals(SystemConstant.TASK_KUBU_OWNER)){
            //取我的任务
            processSearchs = processSearchService.findPrcocessTaskListByUserId(ComUtils.getLoginNamePin(),1,null,null,conditions);
        }
        //处理业务数据
		//处理流程中心正常任务
        this.handlerMapToGetBusinessData(request,map, processSearchs);
		//处理统一待办任务
		this.handlerMapToGetBusinessDataForBizTask(map,processSearchs);
        //获取每个流程的待办任务数目
        Map<String,Object> totalMap=null;
        for(Entry<String,ProcessTaskDto> entry:map.entrySet()){
            ProcessTaskDto dto=entry.getValue();
            if(taskType==null){
                //取全部任务总数
                totalMap=processSearchService.findTodoProcessTaskTotalNum(ComUtils.getLoginNamePin(),dto.getProcessDefinitionKey());
            }else if(taskType.equals(SystemConstant.TASK_KUBU_ADDSIGNER)){
                //取加签任务总数
                totalMap=processSearchService.findForwardProcessTaskTotalNum(ComUtils.getLoginNamePin(),dto.getProcessDefinitionId());
            }else if(taskType.equals(SystemConstant.TASK_KUBU_OWNER)){
            }
            if(totalMap!=null){
                dto.setTotalNum(Integer.parseInt(totalMap.get("total").toString()));
            }
        }
        return map;
    }
    /**
     * @decription 指定流程定义刷新待办任务
     */
    @Override
    public ProcessTaskDto getTodoProcessTaskList(HttpServletRequest request,String processDefinitionId,String processDefinitionName,
                                                 String processDefinitionKey,String tableName,List<String> processColumns,List<String> bussinessColumns,String taskType) {
        String contextPath = request.getSession().getServletContext().getRealPath("/");
        //设置查询条件
        Conditions conditions=new Conditions();
        conditions.setSize("5");

        //组织页面返回数据
        ProcessTaskDto processTaskDto=new ProcessTaskDto();
        processTaskDto.setProcessColumns(processColumns);
        processTaskDto.setBussinessColumns(bussinessColumns);
        processTaskDto.setTableName(tableName);
        processTaskDto.setProcessDefinitionId(processDefinitionId);
        processTaskDto.setProcessDefinitionKey(processDefinitionKey);
        processTaskDto.setProcessDefinitionName(processDefinitionName);

        List<ProcessSearch> processSearchs=null;
        if(taskType!=null&&taskType.equals(SystemConstant.TASK_KUBU_ADDSIGNER)){
            //只得到加签任务
            processSearchs=processSearchService.findPrcocessTaskListByUserId(ComUtils.getLoginNamePin(),3, processDefinitionKey,null,conditions);
            Map<String,Object> totalMap=processSearchService.findForwardProcessTaskTotalNum(ComUtils.getLoginNamePin(),processDefinitionId);
            if(totalMap!=null){
                processTaskDto.setTotalNum(Integer.parseInt(totalMap.get("total").toString()));
            }
        }else{
            Map<String,Object> totalMap=processSearchService.findTodoProcessTaskTotalNum(ComUtils.getLoginNamePin(),processDefinitionKey);
            if(totalMap!=null){
                processTaskDto.setTotalNum(Integer.parseInt(totalMap.get("total").toString()));
            }
            //取得所有待办和加签任务
            processSearchs=processSearchService.findPrcocessTaskListByUserId(ComUtils.getLoginNamePin(),4, processDefinitionKey,null,conditions);
        }
        Collections.sort(processSearchs,new Comparator<ProcessSearch>() {
            @Override
            public int compare(ProcessSearch o1, ProcessSearch o2) {
                if(o1.getCurrentTaskNoteInfo()!=null&&o2.getCurrentTaskNoteInfo()!=null){
                    return o2.getCurrentTaskNoteInfo().getStartTime().compareTo(o1.getCurrentTaskNoteInfo().getStartTime());
                }
                return o2.getLastTaskTime().compareTo(o1.getLastTaskTime());
            }
        });
		Map<String,ProcessTaskDto> map=new HashMap<String, ProcessTaskDto>();
		this.handlerMapToGetBusinessData(request,map,processSearchs);
		this.handlerMapToGetBusinessDataForBizTask(map,processSearchs);
		if(map.size()>0 && map.containsKey(processDefinitionId)){
			ProcessTaskDto newProcessTaskDto = (ProcessTaskDto) map.get(processDefinitionId);
			newProcessTaskDto.setTotalNum(processTaskDto.getTotalNum());
			return newProcessTaskDto;
		}else{
			return processTaskDto;
		}

    }
    /**
     * @author wangdongxing
     * 根据流程实例ID得到业务数据
     */
    @Override
    public Map<String,Object> getBussinessDataByProcessInstance(String processInstanceId,String submitUser,String taskOperateCode) {
        Map<String,Object> processBussinessData = null;
        //查询流程实例
        ProcessInstance processInstance=new ProcessInstance();
        processInstance.setProcessInstanceId(processInstanceId);
        List<ProcessInstance> processInstances=processTaskDao.findProcessInstances(processInstance);
        if(null==processInstances||processInstances.size()==0){
            return null;
        }
        //取得列表配置的显示项
//		ProcessDefinitionConfig config=new ProcessDefinitionConfig();
//		config.setProcessDefinitionId(processInstance.getProcessDefinitionId());
//		config.setConfigType("1");
//		List<ProcessDefinitionConfig> configs=processDefinitionConfigService.find(config);
        List<String> columns=new ArrayList<String>();
//		for(ProcessDefinitionConfig con:configs){
//			columns.add(con.getConfigItem());
//		}
        //取得Table名字
        String tableName=formService.getFormCodeByProDId(processInstances.get(0).getProcessDefinitionId());
        processBussinessData = formBusinessTableService.getBusinessData(tableName,columns,processInstances.get(0).getBusinessInstanceId()).get(0);

        processBussinessData.put("processDefinitionId", processInstances.get(0).getProcessDefinitionId());
        processBussinessData.put("processDefinitionName", processInstances.get(0).getProcessDefinitionName());
        processBussinessData.put("processInstanceId", processInstances.get(0).getProcessInstanceId());
        processBussinessData.put("processInstanceName", processInstances.get(0).getProcessInstanceName());
        processBussinessData.put("followCode", processInstances.get(0).getFollowCode());
        processBussinessData.put("starterId", processInstances.get(0).getStarterId());
		processBussinessData.put("startErp","");
		processBussinessData.put("startRealName","");
		String starterId = processInstances.get(0).getStarterId();
		if(StringUtils.isNotEmpty(starterId)){
			SysUser sysUser = sysUserService.get(starterId);
			if(sysUser != null){
				processBussinessData.put("startErp", sysUser.getUserName());
				processBussinessData.put("startRealName", sysUser.getRealName());
				processBussinessData.put("startCodeName", sysUser.getCodename());
				SysOrganization organization = sysOrganizationService.get(sysUser.getOrganizationId());
				processBussinessData.put("startOrg1Code",organization.getOrganization_1_Code());
				processBussinessData.put("startOrg1Name",organization.getOrganization_1_Name());
				processBussinessData.put("startOrg2Code",organization.getOrganization_2_Code());
				processBussinessData.put("startOrg2Name",organization.getOrganization_2_Name());
			}
		}
        processBussinessData.put("status", processInstances.get(0).getStatus());
        processBussinessData.put("beginTime", processInstances.get(0).getBeginTime());
        processBussinessData.put("endTime", new Date());
		//获取历史记录列表(每个审批节点最新的一条记录)
		List<ProcessTaskHistory> processTaskHistoryList = processTaskHistoryService.find("findNewerProcessTaskHistoryList",processInstances.get(0).getProcessInstanceId());
		for(ProcessTaskHistory history:processTaskHistoryList){
			SysUser sysUser = sysUserService.get(history.getUserId());

			if (sysUser!=null) {
				processBussinessData.put(history.getNodeId()+"_submitUser",sysUser.getUserName());
				processBussinessData.put(history.getNodeId()+"_submitUserRealName",sysUser.getRealName());
				processBussinessData.put(history.getNodeId()+"_submitUserCodeName",sysUser.getCodename());
				processBussinessData.put(history.getNodeId()+"_comment",history.getComment());
				SysOrganization organization = sysOrganizationService.get(sysUser.getOrganizationId());
				processBussinessData.put(history.getNodeId()+"_submitUserOrg1Code",organization.getOrganization_1_Code());
				processBussinessData.put(history.getNodeId()+"_submitUserOrg1Name",organization.getOrganization_1_Name());
				processBussinessData.put(history.getNodeId()+"_submitUserOrg2Code",organization.getOrganization_2_Code());
				processBussinessData.put(history.getNodeId()+"_submitUserOrg2Name",organization.getOrganization_2_Name());
			
			}
		}
		SysUser sysSubmitUser = sysUserService.getByUserName(submitUser);
		if (sysSubmitUser!=null) {
	        processBussinessData.put("submitUserRealName",sysSubmitUser.getRealName());
	        processBussinessData.put("submitUserCodeName",sysSubmitUser.getCodename());
	        processBussinessData.put("submitUser", submitUser);
			SysOrganization organization = sysOrganizationService.get(sysSubmitUser.getOrganizationId());
			processBussinessData.put("submitUserOrg1Code",organization.getOrganization_1_Code());
			processBussinessData.put("submitUserOrg1Name",organization.getOrganization_1_Name());
			processBussinessData.put("submitUserOrg2Code",organization.getOrganization_2_Code());
			processBussinessData.put("submitUserOrg2Name",organization.getOrganization_2_Name());
			processBussinessData.put("taskOperateCode",taskOperateCode);
		}
        return processBussinessData;
    }

    /**
     * WXJ
     */
    @Override
    public Map<String, Object> processTaskComplete(OaTaskInstance task) throws Exception {
        return this.processTaskComplete("", task);
    }

    /**
     * WXJ
     * @throws Exception
     */
    @Override
    public Map<String, Object> processTaskRejectToPrevious(OaTaskInstance task) throws Exception {
        return this.processTaskRejectToPrevious("", task);
    }

    /**
     * WXJ
     * @throws Exception
     */
    @Override
    public Map<String,Object> processTaskRejectToFirst(OaTaskInstance task) throws Exception{
        return this.processTaskRejectToFirst("", task);
    }

    /**
     *
     * @desc 加签任务
     * @author WXJ
     * @date 2014-2-7 下午04:43:00
     *
     * @param instance
     * @param userList
     * @return
     */
    @Override
    public Boolean addSign(OaTaskInstance instance, List<String> userList){
        if (oaPafService.addSign(instance.getProcessInstanceId(), instance.getId(), userList)) {

            //写入审批历史记录
            Date date = new Date();
            this.taskHistoryAdd(ComUtils.getLoginNamePin(), instance, "0", SystemConstant.TASK_STATUS_6, date);

            List<String > mails = new ArrayList<String>();
            List<String> users = new ArrayList<String>();
            //写入加签（加签）历史记录
            for(String userName : userList){
                SysUser u = sysUserService.getUserMailAndPhoneByUserName(userName, "1") ;
                boolean isNotNull = (u!=null);
                
                ProcessAddsigner pa = new ProcessAddsigner();
                pa.setProcessInstanceId(instance.getProcessInstanceId());
                pa.setNodeId(instance.getNodeId());
                pa.setTaskId(instance.getId());
                pa.setUserId(sysUserService.getUserIdByUserName(
                        ComUtils.getLoginNamePin(), "1"));
                if(isNotNull){
                	 pa.setAddsignUserId(u.getId());
                }
                pa.setStatus("1");
                pa.setYn(0);
                processAddsignerDao.insert(pa);

                //发送邮件
                if(isNotNull){
                    if(u.getEmail() != null && ! "".equals(u.getEmail()))
                        //mailSendService.sendSimpleMailByAsyn( new String[]{u.getEmail() } , "【京东企业门户-流程中心 】", u.getRealName() + "，有一个加签任务需要您审批，请尽快处理。申请单主题："+instance.getProcessInstanceName()+"，加签人:"+ComUtils.getLoginName()+"。");
                    	//发送邮件
                        mails.add(u.getEmail());
                        users.add(u.getUserName());
                }
            }

            sendMailToSomeone(ComUtils.getLoginNamePin() , instance.getProcessInstanceId(), instance.getProcessInstanceName(), instance.getBuinessInstanceId(),"7",mails,users);
            return true;
        }
        return false;
    }

    /**
     * 审批任务  yhb
     * @param task
     * @return
     */
    @Override
    public Map<String, Object> processTaskComplete(String submitUser, OaTaskInstance task) throws Exception {

        Map<String,Object> map = new HashMap<String,Object>();
        if (("").equals(submitUser))
            submitUser = ComUtils.getLoginNamePin();

		logger.info("[开始一次审批任务]:submitUser="+submitUser+"|oaTaskInstance="+JsonUtils.toJsonByGoogle(task));
        OaTaskInstance oti = null;
        Boolean canGo=Boolean.TRUE;
        oti = oaPafService.getTaskDetail(submitUser, task.getId());
        if (oti!=null) {
            if("owner".equals(task.getTaskType()) && "true".equals(oti.getUncompletedSubTaskExist())){
                canGo=Boolean.FALSE;
            }
            if(canGo) {         //加签人已审批  走正常审批流程
				logger.info("[非加签任务，开始正常审批流程]:submitUser="+submitUser);
                String isComplete="0";

                Date date= new Date();
                //确定审批人
                if (task.getIsProxy() != null && "1".equals(task.getIsProxy())){
                    submitUser = task.getAssignerId();
					logger.info("[确认为代理任务，使用代理人账号开始审批]:submitUser="+submitUser);
                }


                Map<String,Object> resulMap = findMapByProcess(submitUser,task.getProcessInstanceName(),task.getNodeId(),task.getProcessDefinitionId(),task.getBuinessInstanceId(),"1",task.getComments(),oti,task.getProcessInstanceId()) ;
                if (resulMap == null) {
                    throw new BusinessException("操作失败：获取流程变量失败。","获取流程变量失败。");
                }
				logger.info("[获取流程变量]:submitUser="+submitUser+"|variables="+ JsonUtils.toJsonByGoogle(resulMap));
                map.putAll(resulMap);



                Map<String,Object> businessObject = this.getBussinessDataByProcessInstance(task.getProcessInstanceId(),submitUser,SystemConstant.TASK_OPERATE_APPROVE);

                /**
                 * 流程开始事件调用
                 */
				logger.info("[审批操作开始之前，前置事件开始调用]:submitUser="+submitUser);
                List<ProcessNodeListener> beforeEvents = processNodeListenerService.getBeforeEvents(task.getProcessDefinitionId(), task.getNodeId());
                this.processEvents(beforeEvents, businessObject);
				logger.info("[审批操作开始之前，前置事件调用结束]:submitUser="+submitUser);
				logger.info("[PAF审批接口调用开始,参数]:submitUser="+submitUser+"|processInstanceId="+task.getProcessInstanceId()
				+"processInstanceName="+task.getProcessInstanceName());
                Boolean result= oaPafService.completeUserTask(submitUser, task.getId(), map) ;//false 审批失败
				logger.info("[PAF审批接口调用成功,调用结果:"+result+"]");


                if(!StringUtils.isEmpty(task.getComments())) {
                    oaPafService.addTaskComment(submitUser, task.getProcessInstanceId(),task.getId(),task.getComments()) ;
					logger.info("[存在审批意见，PAF审批意见接口调用]:submitUser="+submitUser+"|comments="+task.getComments());
                }
                if(result){


                    try{
                        OaProcessInstance instance = null;
                        instance = oaPafService.getTrackTaskByInstance(submitUser, task.getProcessInstanceId(),new Conditions());

                        OaProcessInstance oaProcessInstance = new OaProcessInstance();
                        oaProcessInstance.setLastTaskTime(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
                        oaProcessInstance.setProcessInstanceId(task.getProcessInstanceId());
                        oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_1);  //进行
                        if(StringUtils.isNotEmpty(instance.getCompleted())&&instance.getCompleted().equals("true")){
                            oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_5);  //归档
                            isComplete="1";

                            task.setEndTime(DateFormatUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
                            oaProcessInstance.setEndTime(DateFormatUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
                        }
                        if(StringUtils.isNotEmpty(task.getIsProxy())){
                            if(task.getIsProxy().equals("1")) {
                                this.processProxyTaskAdd(submitUser,task);
                            }
                        }
                        if(StringUtils.isNotEmpty(task.getTaskType())){
                            if(task.getTaskType().equals(SystemConstant.TASK_KUBU_ADDSIGNER)) {
                                this.processSignerAdd(submitUser,task)  ;
                            }
                        }
						logger.info("[审批结果成功，开始更新流程实例状态]:submitUser="+submitUser+"|processInstance"+ JsonUtils.toJsonByGoogle(oaProcessInstance));
                        processTaskDao.updateProcessInstance(oaProcessInstance);
						logger.info("[审批结果成功，更新流程状态成功]:submitUser"+submitUser+"|processInstanceId="+oaProcessInstance.getProcessInstanceId());
                        //历史库
						logger.info("[审批结果成功，更新流程实例状态成功，开始更新历史库]:submitUser"+submitUser+"|taskHistory"+ JsonUtils.toJsonByGoogle(task));
                        ProcessNode pn = processNodeService.findAddsignRule(task.getProcessDefinitionId(), task.getNodeId());
                        if ("1".equals(pn.getIsFirstNode())){
                            taskHistoryAdd(submitUser,task,isComplete,SystemConstant.TASK_STATUS_4,date) ;
                        }else{
                            if(StringUtils.isNotEmpty(task.getTaskType())){
                                if(task.getTaskType().equals(SystemConstant.TASK_KUBU_ADDSIGNER)) {
                                    taskHistoryAdd(submitUser,task,isComplete,SystemConstant.TASK_STATUS_5,date) ;
                                }else{
                                    taskHistoryAdd(submitUser,task,isComplete,SystemConstant.TASK_STATUS_1,date) ;
                                }
                            }else{
                                taskHistoryAdd(submitUser,task,isComplete,SystemConstant.TASK_STATUS_1,date) ;
                            }

                        }
						logger.info("[审批结果成功，更新流程实例状态成功，更新历史库成功]:submitUser"+submitUser);
                        /**
                         * 流程结束事件调用
                         */
						logger.info("[审批操作开始之前，后置事件开始调用]:submitUser="+submitUser);
                        List<ProcessNodeListener> endEvents = processNodeListenerService.getEndEvents(task.getProcessDefinitionId(), task.getNodeId());
                        processEvents(endEvents, businessObject);
						logger.info("[审批操作开始之前，后置事件调用结束]:submitUser="+submitUser);
                        
                        //定时器：再次获取活动任务，为定时器推进流程作准备 Add By WXJ At 2014-06-16 23:12:00
                        List<OaTaskInstance> taskList = null;
                        taskList = oaPafService.getActiveTaskByInstance(submitUser, task.getProcessInstanceId());
                        if (!isComplete.equals("1")) {
	                        if(taskList != null){
	                            for(OaTaskInstance t :taskList){
	                                OaTaskInstance ot = oaPafService.getTaskDetail(submitUser,t.getId());
	                                if(ot!=null){
	                                	ProcessNode processNode = processNodeService.findAddsignRule(ot.getProcessDefinitionId(), ot.getNodeId());
	                                	if (processNode!=null && processNode.getTimerDelay()>0){
	                                		ProcessTaskTimer entity = new ProcessTaskTimer();
	                                		entity.setProcessDefinitionId(ot.getProcessDefinitionId());
	                                		entity.setProcessInstanceId(ot.getProcessInstanceId());
	                                		entity.setProcessInstanceName(task.getProcessInstanceName());
	                                		entity.setBusinessInstanceId(task.getBuinessInstanceId());
	                                		entity.setNodeId(ot.getNodeId());
	                                		entity.setTaskId(ot.getId());
	                                		entity.setStatus("0");
	                                		//计算延时时间
	                                		int timerDelay = processNode.getTimerDelay();
	                                		Calendar c = Calendar.getInstance();
	                                	    c.setTime(new Date());
	                                	    c.add(Calendar.SECOND, timerDelay);
	                                		entity.setSoonTime(c.getTime());	                                		
	                                		processTaskTimerService.insert(entity);
											logger.info("[定时器任务执行]:submitUser="+submitUser+"|ProcessTaskTimer="+JsonUtils.toJsonByGoogle(entity));
	                                	}
	                                }
	                            }
	                        }
                        }
                        
                        //定时器：更新定时器为失效状态Add By WXJ At 2014-06-17 13:12:00
                        ProcessTaskTimer entity = new ProcessTaskTimer();
                		entity.setTaskId(task.getId());
                		entity.setStatus("0");
                		List<ProcessTaskTimer> timerList = processTaskTimerService.find(entity);
                        if (timerList!=null && timerList.size()>0) {
                        	for (ProcessTaskTimer timer : timerList) {
                        		timer.setStatus("2");
                        		processTaskTimerService.update(timer);
								logger.info("[更新定时器为失效状态]:submitUser="+submitUser+"|ProcessTaskTimer="+JsonUtils.toJsonByGoogle(entity));
                        	}
                        }
                        
                        //发送邮件给审批人
                        //如果整个审批流程完成，则发送给申请人邮件
						logger.info("[针对审批结果给申请人和审批人开始发送邮件]:submitUser="+submitUser+"|processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                        if("1".equals(isComplete))	{
                            sendMailToAssignee(submitUser,task.getProcessInstanceId(),
                            		task.getProcessInstanceName(),task.getBuinessInstanceId(),"5",true, false);
                        } else {
                            sendMailToAssignee(submitUser,task.getProcessInstanceId(),
                            		task.getProcessInstanceName(),task.getBuinessInstanceId(),"0",false, true);
                        }
						logger.info("[发送邮件成功]:submitUser="+submitUser);
                        //如果设置了限制职级，则自动提交，调用ProcessTaskComplete
                        if (!isComplete.equals("1")) {
	                        ProcessDefinition pd = processDefService.get(task.getProcessDefinitionId());
	                        if(pd != null){
	                            String levelLimit = pd.getTopLevelCode();
	                            if (StringUtils.isNotEmpty(levelLimit)) {
	                                //再次获取活动任务，为流程自动推进作准备
	                                if(taskList != null){
	                                    for(OaTaskInstance t :taskList){
	                                        OaTaskInstance ot = oaPafService.getTaskDetail(submitUser,t.getId());
	                                        if(ot!=null){
	                                            String usersStr = ot.getCandidateUsers();
	
	                                            boolean isAutoComplete = isTopperThanLevelLimit(usersStr,levelLimit);
	                                            if(isAutoComplete){
	                                                ot.setComments("已经过该流程设置的最高级别审批，此节点跳过。——流程中心。");
	                                                ot.setProcessInstanceName(task.getProcessInstanceName());
	                                                ot.setProcessInstanceId(task.getProcessInstanceId());
	                                                ot.setBuinessInstanceId(task.getBuinessInstanceId());
	                                                processTaskComplete(lczxgly, ot);
													logger.info("[审批人["+usersStr+"]职级大于"+levelLimit+",开始自动推进任务]:processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
	                                            }
	                                        }
	                                    }
	                                }
	                            }
	                        }
                        }
                        
                      //通过MQ向JME推送信息
					  logger.info("[开始向JME推送信息]:processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                      oaPafService.sendMessage2JmeByMQ(task.getProcessInstanceId());
                    }catch (Exception e){
                        logger.error(e.getMessage());
                        throw new BusinessException("操作失败",e);
                    }
                    map.put("message","操作成功!");
                }else{
                    map.put("message","操作失败!");
                }
                map.put("Success",result)  ;

            }else{   //加签人未审批则当前任不能审批
                map.put("Success",false)  ;
                map.put("message","请等待加签人进行审批!");
            }
        }else{
            map.put("Success",false)  ;
            map.put("message","获取当前活动任务失败");
        }
        return map;
    }

    /**
     * 拒绝  yhb
     * @param task
     * @return
     * @throws Exception
     */
	public Map<String, Object> processTaskRejectToPrevious(String submitUser, OaTaskInstance task) throws Exception {
        Map map = new HashMap<String,Object>();
        if (("").equals(submitUser))
            submitUser = ComUtils.getLoginNamePin();

		logger.info("[开始退回任务操作]:submitUser+"+submitUser+"processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
        OaTaskInstance oti = null;
        Boolean canGo=Boolean.TRUE;
        oti = oaPafService.getTaskDetail(submitUser, task.getId());
        if (oti!=null) {
            if("true".equals(oti.getUncompletedSubTaskExist())){
                canGo=Boolean.FALSE;
            }
//                }
            if(canGo){         //加签人已审批  走正常审批流程
                Date date = new Date();
                //确定审批人
                if (task.getIsProxy() != null && "1".equals(task.getIsProxy()))
                    submitUser = task.getAssignerId();
                
                //邮件服务切换,临时处理方案
                //Map<String,Object> resulMapMail = getMailVariablesByProcess(submitUser,task.getProcessInstanceName(),task.getNodeId(),task.getProcessDefinitionId(),task.getBuinessInstanceId(),"2",task.getComments(),oti,"") ;
                Map<String,Object> resulMapMail = this.getMailVariablesByProcessTemp();
                map.putAll(resulMapMail);
				logger.info("[退回任务操作，调用PAF接口]:submitUser+"+submitUser+"processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                Boolean result= oaPafService.rejectToPrevious(submitUser,task.getId(),map);
				logger.info("[退回任务操作，调用PAF接口成功,结果为"+result+"]:submitUser+"+submitUser+"processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                if(StringUtils.isNotEmpty(task.getComments())){
					logger.info("[退回任务操作，向PAF添加审批意见["+task.getComments()+"]]:submitUser+"+submitUser+"processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                    oaPafService.addTaskComment(submitUser, task.getProcessInstanceId(),task.getId(),task.getComments()) ;
                }
                if(result){
                    OaProcessInstance oaProcessInstance = new OaProcessInstance();
                    oaProcessInstance.setProcessInstanceId(task.getProcessInstanceId());
                    oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_2);  //Rejected
                    oaProcessInstance.setLastTaskTime(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
                    oaProcessInstance.setStartTime(DateFormatUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
                    try {
						logger.info("[退回任务操作成功，更新流程实例状态]:submitUser+"+submitUser+"processInstance="+JsonUtils.toJsonByGoogle(oaProcessInstance));
                        processTaskDao.updateProcessInstance(oaProcessInstance);
                        //历史库
						logger.info("[退回任务操作成功，插入历史记录]:submitUser+"+submitUser+"taskHistory="+JsonUtils.toJsonByGoogle(task));
                        taskHistoryAdd(submitUser,task,"0",SystemConstant.TASK_STATUS_2,date) ;
                        //
                        if(StringUtils.isNotEmpty(task.getIsProxy())){
                            if(task.getIsProxy().equals("1")) {
                                this.processProxyTaskAdd(submitUser,task);
                            }
                        }
                        if(StringUtils.isNotEmpty(task.getTaskType())){
                            if(task.getTaskType().equals(SystemConstant.TASK_KUBU_ADDSIGNER)) {
                                this.processSignerAdd(submitUser,task)  ;
                            }
                        }

                        //回调事件准备
						logger.info("[退回操作成功，上一节点回调事件开始调用]:submitUser="+submitUser);
                        List<OaTaskInstance> instanceList = null;
                        instanceList= oaPafService.getActiveTaskByInstance(submitUser, task.getProcessInstanceId());


                        /**
                         * 流程回调事件调用
                         */
                        if(instanceList != null){

                            for(OaTaskInstance instance :instanceList){
                                OaTaskInstance ot = oaPafService.getTaskDetail(submitUser,instance.getId());

                                Map<String,Object> businessObject = this.getBussinessDataByProcessInstance(instance.getProcessInstanceId(),submitUser,SystemConstant.TASK_OPERATE_REJECT);

                                List<ProcessNodeListener> callbackEvents = processNodeListenerService.getCallbackEvents(instance.getProcessDefinitionId(), ot.getNodeId());
                                processEvents(callbackEvents, businessObject);
                            }
                        }
                        //发送邮件给审批人，不发送给申请人
						logger.info("[退回任务操作成功，给审批人发送邮件]:submitUser+"+submitUser+"taskHistory="+JsonUtils.toJsonByGoogle(task));
                        sendMailToAssignee(submitUser,task.getProcessInstanceId(),task.getProcessInstanceName(),task.getBuinessInstanceId(),"2",false, false);
                        
                        //通过MQ向JME推送信息
						logger.info("[开始向JME推送信息]:processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                        oaPafService.sendMessage2JmeByMQ(task.getProcessInstanceId());
                        
                    }catch (Exception e) {
                        logger.error(e.getMessage());
                    }
                    map.put("message","操作成功!");
                }else{
                    map.put("message","操作失败!");
                }
                map.put("Success",result)  ;
            }else{   //加签人未审批则当前任不能审批
                map.put("Success",false)  ;
                map.put("message","请等待加签人进行审批!");
            }
        }else{
            map.put("Success",false)  ;
            map.put("message","获取当前活动任务失败");
        }
        return map;
    }
    /**
     * 驳回  yhb
     * @param task
     * @return
     * @throws Exception
     */
    public Map<String,Object> processTaskRejectToFirst(String submitUser, OaTaskInstance task) throws Exception{
        Map map = new HashMap<String,Object>();
        if (("").equals(submitUser))
            submitUser = ComUtils.getLoginNamePin();
		logger.info("[开始驳回任务操作]:submitUser+"+submitUser+"processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
        OaTaskInstance oti = null;
        Boolean canGo=Boolean.TRUE;
        oti = oaPafService.getTaskDetail(submitUser, task.getId());
        if (oti!=null) {
            if("true".equals(oti.getUncompletedSubTaskExist())){
                canGo=Boolean.FALSE;
            }
//            }
            if(canGo){         //加签人已审批  走正常审批流程
                Date date = new Date();
                //确定审批人
                if (task.getIsProxy() != null && "1".equals(task.getIsProxy()))
                    submitUser = task.getAssignerId();

                //邮件服务切换,临时处理方案
                //Map<String,Object> resulMapMail = getMailVariablesByProcess(submitUser,task.getProcessInstanceName(),task.getNodeId(),task.getProcessDefinitionId(),task.getBuinessInstanceId(),"2",task.getComments(),oti,"") ;
                Map<String,Object> resulMapMail = this.getMailVariablesByProcessTemp();
                map.putAll(resulMapMail);
				logger.info("[驳回任务操作，调用PAF接口]:submitUser+"+submitUser+"processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                Boolean result= oaPafService.rejectToFirst(submitUser,task.getId(),map);
				logger.info("[驳回任务操作，调用PAF接口成功，结果为"+result+"]:submitUser+"+submitUser+"processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                if(StringUtils.isNotEmpty(task.getComments())){
                    oaPafService.addTaskComment(submitUser, task.getProcessInstanceId(),task.getId(),task.getComments()) ;
                }
                if(result){
                    OaProcessInstance oaProcessInstance = new OaProcessInstance();
                    oaProcessInstance.setProcessInstanceId(task.getProcessInstanceId());
                    oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_3);  //overrule
                    oaProcessInstance.setStartTime(DateFormatUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
                    oaProcessInstance.setLastTaskTime(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
                    try {
						logger.info("[驳回任务操作成功，更新流程实例状态]:submitUser+"+submitUser+"processInstance="+JsonUtils.toJsonByGoogle(oaProcessInstance));
                        processTaskDao.updateProcessInstance(oaProcessInstance);
                        //历史库
						logger.info("[驳回任务操作成功，插入历史记录]:submitUser+"+submitUser+"taskHistory="+JsonUtils.toJsonByGoogle(task));
                        taskHistoryAdd(submitUser,task,"0",SystemConstant.TASK_STATUS_3,date) ;
                        if(StringUtils.isNotEmpty(task.getIsProxy())){
                            if(task.getIsProxy().equals("1")) {
                                this.processProxyTaskAdd(submitUser,task);
                            }
                        }
                        if(StringUtils.isNotEmpty(task.getTaskType())){
                            if(task.getTaskType().equals(SystemConstant.TASK_KUBU_ADDSIGNER)) {
                                this.processSignerAdd(submitUser,task)  ;
                            }
                        }

                        //回调事件准备
						logger.info("[驳回操作成功，上一节点回调事件开始调用]:submitUser="+submitUser);
                        List<OaTaskInstance> instanceList = null;
                        instanceList= oaPafService.getActiveTaskByInstance(submitUser, task.getProcessInstanceId());


                        /**
                         * 流程回调事件调用
                         */
                        if(instanceList != null){
                            for(OaTaskInstance instance :instanceList){
                                OaTaskInstance ot = oaPafService.getTaskDetail(submitUser,instance.getId());

                                Map<String,Object> businessObject = this.getBussinessDataByProcessInstance(instance.getProcessInstanceId(),submitUser,SystemConstant.TASK_OPERATE_REJECT);

                                List<ProcessNodeListener> callbackEvents = processNodeListenerService.getCallbackEvents(instance.getProcessDefinitionId(), ot.getNodeId());
                                processEvents(callbackEvents, businessObject);
                            }
                        }
                        
                        //发送邮件给审批人，不发送给申请人
						logger.info("[驳回任务成功，开始发送邮件]:processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                        sendMailToAssignee(submitUser,task.getProcessInstanceId(),task.getProcessInstanceName(),task.getBuinessInstanceId(),"3",false, false);
                        
                        //通过MQ向JME推送信息
						logger.info("[驳回任务成功，开始向JME推送信息]:processInstanceId="+task.getProcessInstanceId()+"|processInstanceName="+task.getProcessInstanceName());
                        oaPafService.sendMessage2JmeByMQ(task.getProcessInstanceId());

                    }catch (Exception e){
                        logger.error(e.getMessage());
                    }
                    map.put("message","操作成功!");
                }else{
                    map.put("message","操作失败!");
                }
                map.put("Success",result)  ;
            }else{
                map.put("Success",false)  ;
                map.put("message","请等待加签人进行审批!");
            }
        }else{
            map.put("Success",false)  ;
            map.put("message","获取当前活动任务失败");
        }
        return map;
    }
    /**
     * 获取按钮组
     * @param processInstanceId 流程实例ID
     * @param taskType 任务类型
     * @param processDefinitionId 流程定义ID
     * @param nodeId 节点ID
     * @return map
     */
    public HashMap<String, Object> getButtons(String processInstanceId, String taskType, String processDefinitionId, String nodeId, String taskId,String type){
    	if(type == null || type.trim().length() == 0){//原有的oa代行
    		return this.getButtons(processInstanceId, taskType, processDefinitionId, nodeId, taskId);
    	} else { // type等于unite 为统一待办
    		return this.getButtonsForUnite(processInstanceId, taskType, processDefinitionId, nodeId, taskId);
    	}
    }

    /**
     * 获取按钮组
     * @param processInstanceId 流程实例ID
     * @param taskType 任务类型
     * @param processDefinitionId 流程定义ID
     * @param nodeId 节点ID
     * @return map
     */
    private HashMap<String, Object> getButtons(String processInstanceId, String taskType, String processDefinitionId, String nodeId, String taskId){
        HashMap<String,Object> hashMap = new HashMap<String,Object>();

        //判断当前节点任务是否已经被审批 Add By WXJ
//        List<OaTaskInstance> otiList = oaPafService.getActiveTaskByInstance(processInstanceId);
//        boolean bln = false;
//        for (OaTaskInstance oti : otiList){
//        	if (oti.getUncompletedSubTaskExist().equals("true")){
//        		bln = true;
//        		break;
//        	}
//        	if (oti.getId().equals(taskId)){
//        		bln = true;
//        		break;
//        	}
//        }

        OaTaskInstance oti = null;
        oti = oaPafService.getTaskDetail(taskId);
        if (oti==null){
            hashMap.put("isFinishedFlag", true);
            return hashMap;
        }

        boolean submitButtonFlag = false;//提交
        boolean draftButtonFlag = false;//草稿
        boolean approveButtonFlag = false;//批准
        boolean rejectButtonFlag = false; //拒绝
        boolean rebackButtonFlag = false; //驳回
        boolean forwardButtonFlag = false;//加签
        boolean confirmButtonFlag = false;//确认

        if(processInstanceId == null || processInstanceId.equals("")){ //流程实例为空说明未创建流程实例
            submitButtonFlag = true;
            draftButtonFlag = true;
        }else{
            ProcessInstance processInstance = new ProcessInstance();
            processInstance.setProcessInstanceId(processInstanceId);
            processInstance =  processInstanceService.selectByProcessInstanceId(processInstance);
            String status = processInstance.getStatus();
            ProcessNode processNode = processNodeService.findAddsignRule(processDefinitionId,nodeId);
            if(status!=null && !status.equals("")){
                if(status.equals(SystemConstant.PROCESS_STATUS_3)){                                                     //驳回状态
                    submitButtonFlag = true;
                    draftButtonFlag = true;
                }else if(status.equals(SystemConstant.PROCESS_STATUS_1)){   //审批状态
                    //需要判断是否配置加签
                    String flag = processNode.getAddsignRule();
                    if(taskType.equals(SystemConstant.TASK_KUBU_ADDSIGNER)){                   //加签的
                        if(flag.equals("1")){
                            confirmButtonFlag = true;
                        }
                    }else if(taskType.equals(SystemConstant.TASK_KUBU_PROXY)){               //代理审批的任务不能出现加签
                        approveButtonFlag = true;           //批准
                        rejectButtonFlag = true;            //拒绝
                        rebackButtonFlag = true;            //驳回
                    }else{                                                                     //审批的与加签的
                        approveButtonFlag = true;
                        rejectButtonFlag = true;
                        rebackButtonFlag = true;
                        if(!flag.equals("0")){
                            forwardButtonFlag = true;
                        }
                    }
                }else if(status.equals(SystemConstant.PROCESS_STATUS_2)){       //拒绝状态
                    if(processNode.getIsFirstNode()!=null && processNode.getIsFirstNode().equals("1")){
                        submitButtonFlag = true;
                        draftButtonFlag = true;
                    }else{
                        //需要判断是否配置加签
                        String flag = processNode.getAddsignRule();
                        if(taskType.equals(SystemConstant.TASK_KUBU_ADDSIGNER)){                   //加签的
                            if(flag.equals("1")){
                                confirmButtonFlag = true;
                            }
                        }else{                                                                     //审批的与加签的
                            approveButtonFlag = true;
                            rejectButtonFlag = true;
                            rebackButtonFlag = true;
                            if(!flag.equals("0")){
                                forwardButtonFlag = true;
                            }
                        }
                    }
                }
            }
        }
        hashMap.put("submitButtonFlag", submitButtonFlag);
        hashMap.put("draftButtonFlag", draftButtonFlag);
        hashMap.put("approveButtonFlag", approveButtonFlag);
        hashMap.put("rejectButtonFlag", rejectButtonFlag);
        hashMap.put("rebackButtonFlag", rebackButtonFlag);
        hashMap.put("forwardButtonFlag", forwardButtonFlag);
        hashMap.put("confirmButtonFlag", confirmButtonFlag);
        return hashMap;
    }
    /**
     * 统一待办button逻辑处理
     * @return
     */
    private HashMap<String,Object> getButtonsForUnite(String processInstanceId, String taskType, String processDefinitionId, String nodeId, String taskId){
    	 boolean approveButtonFlag = false;           //批准
         boolean rejectButtonFlag = false;            //拒绝
         boolean isFinishedFlag = false;
         String message = "";
         HashMap<String,Object> resultDataMap = new HashMap<String,Object>();
         ProcessInstance processInstance = new ProcessInstance();
         processInstance.setProcessInstanceId(processInstanceId);
         processInstance =  processInstanceService.selectByProcessInstanceId(processInstance);
         String status = processInstance.getStatus();//流程实例状态
         if(status != null && !status.equals("")){
        	 if(status.equals(SystemConstant.PROCESS_STATUS_0)){//申请
        		 approveButtonFlag = true;
        		 rejectButtonFlag = true;
        	 } else if(status.equals(SystemConstant.PROCESS_STATUS_1)){//批准
            	 rejectButtonFlag = true;
            	 approveButtonFlag = true;
             } else if(status.equals(SystemConstant.PROCESS_STATUS_2)){//拒绝
            	 isFinishedFlag = true;
             } else if(status.equals(SystemConstant.PROCESS_STATUS_3)){//驳回
            	 isFinishedFlag = true;
             } else if(status.equals(SystemConstant.PROCESS_STATUS_5)){//归档
            	 isFinishedFlag = true;
             }
         }
         resultDataMap.put("approveButtonFlag", approveButtonFlag);
         resultDataMap.put("rejectButtonFlag", rejectButtonFlag);
         return resultDataMap;
    }

    /**
     * 获取流程实例运行轨迹
     * @param processInstanceId 流程实例ID
     * @return 流程实例详细信息
     */
    @Override
    public List<ProcessTaskHistory> getProcessTrajectory(String processInstanceId){
        ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
        processTaskHistory.setProcessInstanceId(processInstanceId);
        if(processInstanceId != null && !"".equals(processInstanceId)){
            return processTaskHistoryService.selectByCondition(processTaskHistory);
        } else {
            logger.error("获取流程实例运行轨迹失败！");
            return null;
        }
    }
    /**
     * 按照时间范围获取我的督办任务
     * @return
     */
    @Override
    public Map<String,ProcessTaskDto> getSuperviseProcessTaskList(HttpServletRequest request,int startDay,int endDay,String pafKey) {

        //组织返回数据
        Map<String,ProcessTaskDto> map=new HashMap<String, ProcessTaskDto>();

        Conditions conditions=new Conditions();
        conditions.setSize("5");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //计算查询时间点
        Calendar  cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE,-1*startDay);
        String startTimePoint=dateFormat.format(cal.getTime());  //当前时间远端
        cal.setTime(new Date());
        cal.add(Calendar.DATE,-1*endDay);
        String endTimePoint=dateFormat.format(cal.getTime());   //当前时间近端
        if(startDay!=-1){
            conditions.setCreateStartDate(startTimePoint);
        }
        conditions.setCreateEndDate(endTimePoint);
        //得到督办任务
        List<ProcessSearch> processSearchs=processSearchService.findPrcocessSuperviseTaskByUserId(ComUtils.getLoginNamePin(),"1",pafKey, conditions);
        //处理业务数据
        this.handlerMapToGetBusinessData(request,map, processSearchs);
        //获取每个流程的待办任务数目
        for(Entry<String,ProcessTaskDto> entry:map.entrySet()){
            ProcessTaskDto dto=entry.getValue();
            int totalNum=processSearchService.getProSuperviseTaskCountByUserId(ComUtils.getLoginNamePin(),dto.getProcessDefinitionId(), conditions);
            dto.setTotalNum(totalNum);
        }
        return map;
    }

	/**
	 * 得到业务数据并且组织数据返回页面  统一待办的业务数据
	 * @param map
	 * @param processSearchs
	 */
	private void handlerMapToGetBusinessDataForBizTask(Map<String,ProcessTaskDto> map,List<ProcessSearch> processSearchs){

		ProcessTaskDto dto;
		String processDefId;
		String processDefKey;
		String processDefName;
		if(processSearchs==null) return;
		//按时间倒排序
		Collections.sort(processSearchs,new Comparator<ProcessSearch>() {
			@Override
			public int compare(ProcessSearch o1, ProcessSearch o2) {
				if(o1.getCurrentTaskNoteInfo()!=null&&o2.getCurrentTaskNoteInfo()!=null){
					return o2.getCurrentTaskNoteInfo().getStartTime().compareTo(o1.getCurrentTaskNoteInfo().getStartTime());
				}
				return o2.getLastTaskTime().compareTo(o1.getLastTaskTime());
			}
		});

		//把杂乱的流程制作成以流程定义ID区分
		Map<String,List<ProcessSearch>> makePsListByPefIdMap=new HashMap<String, List<ProcessSearch>>();

		//相同流程定义ID的processSearch组织的业务实例ID
		Map<String,List<String>> makePsBusinessIdsMap=new HashMap<String, List<String>>();

		//暂存流程定义的业务字段
		Map<String,List<Map<String,String>>> pdfFormItemsMap=new HashMap<String, List<Map<String,String>>>();

		for(ProcessSearch search:processSearchs){
			if(search.getCurrentTaskNoteInfo()!=null&&search.getCurrentTaskNoteInfo().isFirstNode()) continue;
			if(search.getCurrentTaskNoteInfo()!=null&&!search.getCurrentTaskNoteInfo().getProcessDefinitionIsOuter().equals(SystemConstant.PROCESS_DEFINITION_BIZ)) continue;
			processDefId=search.getCurrentTaskNoteInfo()==null?search.getProcessDefinitionId():search.getCurrentTaskNoteInfo().getProcessDefinitionId();
			processDefKey=search.getCurrentTaskNoteInfo()==null?search.getPafProcessDefinitionId():search.getCurrentTaskNoteInfo().getProcessDefinitionKey();
			processDefName=search.getCurrentTaskNoteInfo()==null?search.getProcessDefinitionName():search.getCurrentTaskNoteInfo().getProcessDefinitionName();

			//组织processSearch的分类数据
			List<ProcessSearch> currPsList=null;
			if(!makePsListByPefIdMap.containsKey(processDefId)){
				makePsListByPefIdMap.put(processDefId,new ArrayList<ProcessSearch>());
			}
			currPsList=makePsListByPefIdMap.get(processDefId);
			currPsList.add(search);
			//组织同类的processSearch的ID
			List<String> currPsPdfIdList=null;
			if(!makePsBusinessIdsMap.containsKey(processDefId)){
				makePsBusinessIdsMap.put(processDefId,new ArrayList<String>());
			}
			currPsPdfIdList=makePsBusinessIdsMap.get(processDefId);
			currPsPdfIdList.add(search.getBusinessInstanceId());

			if(!map.containsKey(processDefId)){
				ProcessDefinition pdf=processDefService.get(processDefId);
				if(!pdf.getIsOuter().equals(SystemConstant.PROCESS_DEFINITION_BIZ)) continue; //是统一待办任务过滤掉
				//取得列表配置的显示项
				dto=new ProcessTaskDto();
				ProcessDefinitionConfig config=new ProcessDefinitionConfig();
				config.setProcessDefinitionId(processDefId);
				config.setConfigType("1");
				List<ProcessDefinitionConfig> configs=processDefinitionConfigService.find(config);
				List<Map<String,String>> formItemMapList = new ArrayList<Map<String, String>>();
				Map<String,String> formItemMap;
				for(ProcessDefinitionConfig c:configs){
					formItemMap = new LinkedHashMap<String, String>();
					String configItem = c.getConfigItem();
					String[] items = configItem.split(":");
					formItemMap.put("fieldChineseName",items[0]);
					formItemMap.put("fieldName",items[1]);
					dto.getProcessColumns().add(items[0]);
					dto.getBussinessColumns().add(items[1]);
					formItemMapList.add(formItemMap);
				}
				pdfFormItemsMap.put(processDefId,formItemMapList);
				map.put(processDefId,dto);
			}else{
				dto=map.get(processDefId);
			}
			dto.setProcessDefinitionId(processDefId);
			dto.setProcessDefinitionKey(processDefKey);
			dto.setProcessDefinitionName(processDefName);
		}

		//取业务数据
		for(Entry<String,List<ProcessSearch>> currItem:makePsListByPefIdMap.entrySet()){
			String pdfId=currItem.getKey();
			List<ProcessSearch> psList=currItem.getValue();
			ProcessTaskDto processTaskDto=map.get(pdfId);
			List<String>  pdfIds=makePsBusinessIdsMap.get(pdfId);
			List<Map<String,String>> myFormItems=pdfFormItemsMap.get(currItem.getKey());

			//填充业务数据
			List<Map<String,Object>> data= new ArrayList<Map<String, Object>>();
			Map<String,Map<String,Object>> data2Map=new HashMap<String, Map<String, Object>>();
			if(processTaskDto.getBussinessColumns().size()!=0){
				//获取业务数据
				for(String businessID:pdfIds){
					Map<String,Object> businessData = (Map<String,Object>) agentBizDataService.get(businessID);
					Map<String,Object> keyBusinessData = new HashMap<String, Object>();
					keyBusinessData.put("ID",businessID);
					for(int i=0;i<processTaskDto.getBussinessColumns().size();i++){
						keyBusinessData.put(processTaskDto.getBussinessColumns().get(i),businessData.get("COL"+(i+1)));
					}
					data.add(keyBusinessData);
				}
				if(data != null && data.size() > 0){
					for(int i=0;i<data.size();i++){
						if(data.get(i) !=null) data2Map.put(data.get(i).get("ID").toString(),data.get(i));
					}
				}
			}

			for(int i=0;i<psList.size();i++){
				if(psList.get(i) == null ) continue;
				Map<String,Object> currentDataMap=data2Map.get(psList.get(i).getBusinessInstanceId());
				if(currentDataMap !=null ){
					Map<String,Object> newDataMap = new HashMap<String, Object>();
					for(Map<String,String> item:myFormItems){
						Object valueObject = currentDataMap.get(item.get("fieldName"));
						if(valueObject!=null){
							String valueStirng = DataConvertion.object2String(valueObject);
							String[] values = valueStirng.split(":");
							if(values.length > 1){
								newDataMap.put(item.get("fieldName"),values[1]);
							}else{
								newDataMap.put(item.get("fieldName"),"");
							}
						}else{
							newDataMap.put(item.get("fieldName"),"");
						}
					}
					processTaskDto.addDataList(psList.get(i),newDataMap);
				}else{
					processTaskDto.addDataList(psList.get(i),null);
				}
			}

		}
	}

    /**
     * 得到业务数据并且组织数据返回页面
     * @param map
     * @param processSearchs
     */
    private void handlerMapToGetBusinessData(HttpServletRequest request,Map<String,ProcessTaskDto> map,List<ProcessSearch> processSearchs){
        ProcessTaskDto dto;
        String processDefId;
        String processDefKey;
        String processDefName;
        if(processSearchs==null) return;
        //按时间倒排序
        Collections.sort(processSearchs,new Comparator<ProcessSearch>() {
            @Override
            public int compare(ProcessSearch o1, ProcessSearch o2) {
                if(o1.getCurrentTaskNoteInfo()!=null&&o2.getCurrentTaskNoteInfo()!=null){
                    return o2.getCurrentTaskNoteInfo().getStartTime().compareTo(o1.getCurrentTaskNoteInfo().getStartTime());
                }
                return o2.getLastTaskTime().compareTo(o1.getLastTaskTime());
            }
        });
        //取项目上下文路径
        String contextPath = request.getSession().getServletContext().getRealPath("/");

        //把杂乱的流程制作成以流程定义ID区分
        Map<String,List<ProcessSearch>> makePsListByPefIdMap=new HashMap<String, List<ProcessSearch>>();

        //相同流程定义ID的processSearch组织的业务实例ID
        Map<String,List<String>> makePsBusinessIdsMap=new HashMap<String, List<String>>();

        //暂存流程定义的业务字段
        Map<String,List<FormItem>> pdfFormItemsMap=new HashMap<String, List<FormItem>>();

        for(ProcessSearch search:processSearchs){
            if(search.getCurrentTaskNoteInfo()!=null&&search.getCurrentTaskNoteInfo().isFirstNode()) continue;
			if(search.getCurrentTaskNoteInfo()!=null&&search.getCurrentTaskNoteInfo().getProcessDefinitionIsOuter().equals(SystemConstant.PROCESS_DEFINITION_BIZ)) continue;
            processDefId=search.getCurrentTaskNoteInfo()==null?search.getProcessDefinitionId():search.getCurrentTaskNoteInfo().getProcessDefinitionId();
            processDefKey=search.getCurrentTaskNoteInfo()==null?search.getPafProcessDefinitionId():search.getCurrentTaskNoteInfo().getProcessDefinitionKey();
            processDefName=search.getCurrentTaskNoteInfo()==null?search.getProcessDefinitionName():search.getCurrentTaskNoteInfo().getProcessDefinitionName();

            //组织processSearch的分类数据
            List<ProcessSearch> currPsList=null;
            if(!makePsListByPefIdMap.containsKey(processDefId)){
                makePsListByPefIdMap.put(processDefId,new ArrayList<ProcessSearch>());
            }
            currPsList=makePsListByPefIdMap.get(processDefId);
            currPsList.add(search);
            //组织同类的processSearch的ID
            List<String> currPsPdfIdList=null;
            if(!makePsBusinessIdsMap.containsKey(processDefId)){
                makePsBusinessIdsMap.put(processDefId,new ArrayList<String>());
            }
            currPsPdfIdList=makePsBusinessIdsMap.get(processDefId);
            currPsPdfIdList.add(search.getBusinessInstanceId());

            if(!map.containsKey(processDefId)){
                ProcessDefinition pdf=processDefService.get(processDefId);
				if(pdf.getIsOuter().equals(SystemConstant.PROCESS_DEFINITION_BIZ)) continue; //是统一待办任务过滤掉
                //取得列表配置的显示项
                ProcessDefinitionConfig config=new ProcessDefinitionConfig();
                config.setProcessDefinitionId(processDefId);
                config.setConfigType("1");
                List<ProcessDefinitionConfig> configs=processDefinitionConfigService.find(config);
                dto=new ProcessTaskDto(configs);
                List<FormItem> businessFormItems=new ArrayList<FormItem>();  //对字段进行转义操作
                for(ProcessDefinitionConfig c:configs){
                    FormItem formItem=new FormItem();
                    formItem.setFormId(pdf.getFormId());
                    formItem.setFieldName(c.getConfigItem());
                    List<FormItem> formItems=formItemService.find(formItem);
                    dto.addProcessColumns(formItems.get(0).getFieldChineseName());
                    businessFormItems.add(formItems.get(0));
                }
                pdfFormItemsMap.put(processDefId,businessFormItems);
                //取得Table名字
                String tableName=formService.getFormCodeByProDId(processDefId);
                dto.setTableName(tableName);
                map.put(processDefId,dto);
            }else{
                dto=map.get(processDefId);
            }
            dto.setProcessDefinitionId(processDefId);
            dto.setProcessDefinitionKey(processDefKey);
            dto.setProcessDefinitionName(processDefName);
        }

        FormUIUtil formUIUtil=new FormUIUtil();
        //取业务数据
        for(Entry<String,List<ProcessSearch>> currItem:makePsListByPefIdMap.entrySet()){

            String pdfId=currItem.getKey();
            List<ProcessSearch> psList=currItem.getValue();
            ProcessTaskDto processTaskDto=map.get(pdfId);
			if(processTaskDto == null) continue;
            List<String>  pdfIds=makePsBusinessIdsMap.get(pdfId);
            List<FormItem> myFormItems=pdfFormItemsMap.get(currItem.getKey());

            //填充业务数据
            List<Map<String,Object>> data= null;
            Map<String,Map<String,Object>> data2Map=new HashMap<String, Map<String, Object>>();
            if(processTaskDto.getBussinessColumns().size()!=0){
                String idTag="ID";
                processTaskDto.getBussinessColumns().add(idTag);
                data=formBusinessTableService.getBusinessData(processTaskDto.getTableName(),processTaskDto.getBussinessColumns(),pdfIds);
                processTaskDto.getBussinessColumns().remove(idTag);
                if(data != null && data.size() > 0){
                    for(int i=0;i<data.size();i++){
                        if(data.get(i) !=null) data2Map.put(data.get(i).get("ID").toString(),data.get(i));
                    }
                }
            }

            for(int i=0;i<psList.size();i++){
                if(psList.get(i) == null ) continue;
                Map<String,Object> currentDataMap=data2Map.get(psList.get(i).getBusinessInstanceId());
                if(currentDataMap !=null ){
                    for(FormItem item:myFormItems){
                        if(currentDataMap.get(item.getFieldName())!=null){
                            Map<String,String> businessMap=formUIUtil.buildMasterHashtableOfVerify(contextPath, item,currentDataMap.get(item.getFieldName()).toString());
                            currentDataMap.put(item.getFieldName(),businessMap.get(item.getFieldName()));
                        }
                    }
                    processTaskDto.addDataList(psList.get(i),currentDataMap);
                }else{
                    processTaskDto.addDataList(psList.get(i),null);
                }
            }

        }

    }


    @Override
    public void parseMailTask() throws Exception{
        System.out.println(DateUtils.datetimeFormat24(new Date())+":Worker:parseMailTask:Start");

        List<Message> mList = mailReceiveService.getUnreadMails();
        if (mList != null && mList.size()>0){
            for (Message m : mList){
                try {
                    //解析待办
//                    if (mailReceiveService.getSubject(m).indexOf("流程中心") > -1 || mailReceiveService.getSubject(m).indexOf("ProcessCenter") > -1){
                        String content = mailReceiveService.getContent(m);
                        if (content != null){//邮件内容
//							dealMailTask(content.replace("\r\n",""),mailReceiveService.getFrom(m));
                            dealMailTask(content,mailReceiveService.getFrom(m));
                            mailReceiveService.setMailFlag(m, MailFlag.SEEN);
                        }
//                    }

                } catch (IOException e) {
                    log.error(e);
                } catch (MessagingException e) {

                    log.error(e);
                }
            }
        }
        System.out.println(DateUtils.datetimeFormat24(new Date())+":Worker:parseMailTask:End");
    }

    public void dealMailTask(String content, String fromMail) throws Exception{
        Map<String, Object> mp = null;

        log.info("邮件审批内容:"+content);
        int pos1 = content.indexOf("审批意见");
        int pos2 = content.indexOf("[",pos1);
        int pos3 = content.indexOf("]",pos2);
        String comment = content.substring(pos2+1, pos3).trim();

        pos1 = content.indexOf("PID");
        if (pos1 < 0){//如果不包含PID,return
            throw new BusinessException("邮件审批异常-参数错误!");
        }

        pos2 = content.indexOf("[",pos1);
        pos3 = content.indexOf("]",pos2);
        String pid = content.substring(pos2+1, pos3).trim();
        com.jd.oa.common.utils.StringUtils pidStringUtils = new com.jd.oa.common.utils.StringUtils(pid);
        List<String> pidArray = pidStringUtils.split("/");
        if(pidArray == null || pidArray.size() == 0){
            throw new BusinessException("邮件审批异常-参数错误!");
        }
        //审批结果
        String result = pidArray.get(0);
        if(StringUtils.isEmpty(result)){
            throw new BusinessException("邮件审批异常-审批结果为空!");
        }

        //任务ID
        String taskId = pidArray.get(1);
        if(StringUtils.isEmpty(taskId)){
            throw new BusinessException("邮件审批异常-任务ID为空!");
        }


        //流程实例ID
        String processInstanceId = pidArray.get(2);
        if(StringUtils.isNotEmpty(processInstanceId)){
            processInstanceId = processInstanceId.replace("\r\n", "");
            processInstanceId = processInstanceId.replace("=", "");
        } else {
            throw new BusinessException("邮件审批异常-流程实例ID为空!");
        }
        //任务节点ID
        String taskDefinitionKey = pidArray.get(3);
        if(StringUtils.isEmpty(taskDefinitionKey)){
            throw new BusinessException("邮件审批异常-任务节点ID为空!");
        }

        //任务名
        StringBuilder taskNameSb = new StringBuilder("");
        for (int i=4;i<pidArray.size()-1;i++){
        	taskNameSb.append( pidArray.get(i) + "/");
        }
        String taskName = taskNameSb.toString();
        
		if(!StringUtils.isEmpty(taskName)){
        	taskName = taskName.substring(0,taskName.length()-1);
		}

        //任务创建时间
        String createTime = pidArray.get(pidArray.size()-1);
        if(StringUtils.isNotEmpty(createTime)){
            createTime = DateUtils.datetimeFormatCst(createTime.replace("\r\n ", " ").replace("\r\n", " "),"yyyy-MM-dd HH:mm:ss");
        } else {
            throw new BusinessException("邮件审批异常-任务创建时间为空!");
        }

        pos2 = fromMail.indexOf("<");
        pos3 = fromMail.indexOf(">",pos2);
        if (pos1!=-1 && pos2!=-1)
            fromMail = fromMail.substring(pos2+1, pos3).trim();

        String submitUser = sysUserService.getByEmailAndMobile(fromMail,null).getUserName();
		OaTaskInstance oaTaskInstance = oaPafService.getTaskDetail(submitUser,taskId);
		if(oaTaskInstance != null){
			taskName = oaTaskInstance.getName();
		}else{
			taskName = taskDefinitionKey;
		}
        OaTaskInstance task = getTask(taskId, processInstanceId, taskDefinitionKey, comment, taskName, createTime);

        if (task!=null){
            if ("1".equals(result))
                mp = this.processTaskComplete(submitUser,task);
            if ("2".equals(result))
                mp = this.processTaskRejectToPrevious(submitUser,task);
            if ("3".equals(result))
                mp = this.processTaskRejectToFirst(submitUser,task);
        }
    }

    @Override
    public Map parseSmsTask(String content, String fromMobile) throws Exception{


        Map<String, Object> mp = null;

//		int pos1 = content.indexOf("审批意见");
//		if (pos1 < 0) return null;
//		int pos2 = content.indexOf("[",pos1);
//		int pos3 = content.indexOf("]",pos2);
//		String comment = content.substring(pos2+1, pos3).trim();
//
//		pos1 = content.indexOf("审批结果");
//		if (pos1 < 0) return null;
//		pos2 = content.indexOf("[",pos1);
//		pos3 = content.indexOf("]",pos2);
//		String result = content.substring(pos2+1, pos3).trim();
//
//		pos1 = content.indexOf("任务ID");
//		if (pos1 < 0) return null;
//		pos2 = content.indexOf("[",pos1);
//		pos3 = content.indexOf("]",pos2);
//		String taskId = content.substring(pos2+1, pos3).trim();
//
//		pos1 = content.indexOf("流程实例ID");
//		if (pos1 < 0) return null;
//		pos2 = content.indexOf("[",pos1);
//		pos3 = content.indexOf("]",pos2);
//		String processInstanceId = content.substring(pos2+1, pos3).trim();
//
//		pos1 = content.indexOf("任务节点ID");
//		if (pos1 < 0) return null;
//		pos2 = content.indexOf("[",pos1);
//		pos3 = content.indexOf("]",pos2);
//		String taskDefinitionKey = content.substring(pos2+1, pos3).trim();
//
//        String submitUser = sysUserService.getByEmailAndMobile(null,fromMobile).getUserName();
//        OaTaskInstance task = getTask(taskId, processInstanceId, taskDefinitionKey, comment, taskName, createTime);
//
//		if ("1".equals(result))
//			mp = this.processTaskComplete(submitUser,task);
//		if ("2".equals(result))
//			mp = this.processTaskRejectToPrevious(submitUser,task);
//		if ("3".equals(result))
//			mp = this.processTaskRejectToFirst(submitUser,task);

        return mp;
    }

    /**
     * 获取流程变量-流程推进相关
     * @param nodeId
     * @param processDefinitionId
     * @param businessInstanceId
     * @return
     */
    @Override
    public Map<String,Object> findMapByProcess(String submitUser,String processInstanceName,String nodeId,String processDefinitionId,String businessInstanceId,String result,String comment,OaTaskInstance oti,String processInstanceId) throws Exception {
        Map<String,Object> map = getSubmitVariablesByProcess(submitUser,processInstanceName,nodeId,processDefinitionId,businessInstanceId,result,comment,oti,processInstanceId) ;
        if (map != null){
        	//邮件服务切换,临时处理方案
            //Map<String,Object> resulMapMail = getMailVariablesByProcess(submitUser,task.getProcessInstanceName(),task.getNodeId(),task.getProcessDefinitionId(),task.getBuinessInstanceId(),"2",task.getComments(),oti,"") ;
            Map<String,Object> resulMapMail = this.getMailVariablesByProcessTemp();
            map.putAll(resulMapMail);
        }
        return map;
    }

    private Map<String,Object> getSubmitVariablesByProcess(String submitUser,String processInstanceName,String nodeId,String processDefinitionId,String businessInstanceId,String result,String comment,OaTaskInstance oti,String processInstanceId) throws Exception {
        Map<String,Object>  map = null;

        Variables variables = new Variables();
        variables.setProcessInstanceName(processInstanceName);

        map= ComUtils.pojoToMap(variables);

        //办理人：上级经理
//        ProcessNode processNode = new ProcessNode();
//        processNode.setManagerNodeId(nodeId);
//        Integer managerNodeCount = processNodeService.findManagerNodeCount(processNode);
//        if(managerNodeCount > 0){
//            map.put(nodeId, submitUser);
//        }

        //办理人：职位关系（上级经理等）
        ProcessNode processNode = new ProcessNode();
        processNode.setManagerNodeId(nodeId);
        Integer managerNodeCount = processNodeService.findManagerNodeCount(processNode);
        if(managerNodeCount > 0){
            String managerUser = "";
            //Modify By WXJ At 2014-5-22
            //List<ProcessNode> pdList = processNodeService.findByManagerNode(nodeId);
            List<ProcessNode> pdList = processNodeService.findAllManagerNode(processDefinitionId);
            
            for (ProcessNode pn : pdList){
                if (submitUser.equals(lczxgly)) {
                    managerUser = lczxgly;
                }else{
                    if (pn.getManagerNodeId().endsWith("_assignee")){
                    	ProcessNode node = processNodeService.findAddsignRule(processDefinitionId, nodeId);
                    	if (node!=null) {
                    		if ("1".equals(node.getIsFirstNode())){
                    			managerUser = submitUser;
                    		}else{         
                    			//获取最近的节点审批人owner或proxy        			
                    			ProcessTaskHistory entity = new ProcessTaskHistory();
                    			entity.setProcessInstanceId(processInstanceId);
                    			entity.setNodeId(pn.getManagerNodeId().substring(0,pn.getManagerNodeId().indexOf("_assignee")));
                    			ProcessTaskHistory pth = processTaskHistoryService.getRecentHistoryByNode(entity);
                    			if (pth!=null) {
                    				String userId = pth.getUserId();
                    				SysUser su = sysUserService.get(userId);
                    				managerUser = su.getUserName();
                    			}else{
                    				return null;
                    			}
                    		}
                    	}else{
                    		managerUser = submitUser;
                    	}                    	                        
                    }else{
                        managerUser = getManagerUser(submitUser);
                    }
                }
                if (StringUtils.isNotEmpty(managerUser) && !map.containsKey(pn.getManagerNodeId())){
                	if (managerUser.split(",").length<=0) {                		
	                    ProcessDefinition pd = processDefService.get(processDefinitionId);
	                    if(pd != null){
	                        String levelLimit = pd.getTopLevelCode();
	                        if (StringUtils.isNotEmpty(levelLimit)){
	                            boolean isAutoComplete = isTopperThanLevelLimit(managerUser,levelLimit);
	                            if(isAutoComplete){
	                                managerUser = lczxgly;
	                            }
	                        }
	                    }
                	}                	
                    map.put(pn.getManagerNodeId(), managerUser);   
                    String[] mu = managerUser.split(",");
                    if (mu!=null && mu.length>0) {
	                    SysUser su = sysUserService.getByUserName(mu[0]);
	                    if (su != null && su.getLevelCode() != null)
	                    	map.put("userLevelCode", su.getLevelCode());   
	                    else
	                    	map.put("userLevelCode", ""); 
	                }else{
	                	SysUser su = sysUserService.getByUserName(managerUser);
	                	if (su != null && su.getLevelCode() != null)
	                    	map.put("userLevelCode", su.getLevelCode());   
	                    else
	                    	map.put("userLevelCode", ""); 
	                }
                }
            }
        }
        //办理人：变量  Add By WXJ At 2013-12-11
        Map<String,Object> businessData = getBusinessData(processDefinitionId, businessInstanceId);
        businessData.put("processInstanceName", processInstanceName);
        businessData.put("submitUser", submitUser);

        ProcessDefinitionConfig pdConfig = new ProcessDefinitionConfig();
        pdConfig.setConfigType("2");
        pdConfig.setProcessDefinitionId(processDefinitionId);
        pdConfig.setYn(10);
        List<ProcessDefinitionConfig> pdConfigList = pdConfigService.find(pdConfig);
        if (pdConfigList!= null && pdConfigList.size()>0){
            for (ProcessDefinitionConfig pdc : pdConfigList){
                //获得办理人变量
                String submitUsers = null;
                Boolean isDataSource = false;

                ProcessDefinitionConfigDetail pdcdTemp = new ProcessDefinitionConfigDetail();
                pdcdTemp.setConfigId(pdc.getId());
                List<ProcessDefinitionConfigDetail> pdcdList = pdcdService.find(pdcdTemp);

                if (pdcdList!=null && pdcdList.size()>0){
                    for (ProcessDefinitionConfigDetail pdcd : pdcdList) {
                        String valNo = pdcd.getValNo();
                        String[] valNoArray = valNo.split("#");
                        if (StringUtils.isNotEmpty(valNo) && valNoArray[0].equals("dataSource")) {
                            //数据源接口计算出变量值
                            List<Map<String,Object>> mapList = null;
                            DiDatasource dds = diDatasourceService.get(valNoArray[2]);
                            if (valNoArray[1].equals("0")) {
                                //数据库数据源
                                mapList = diDatasourceService.getDatasourceResultList(dds.getDataAdapter(), dds.getExecSql(), "0");
                            }else{
                                //接口数据源
                                mapList = diDatasourceService.getDatasourceResultList(dds.getDataAdapter(), 1, 1, businessData, "0");
                            }
                            if (mapList!=null && mapList.size()>0){
                                for (Entry<String,Object> entry : mapList.get(0).entrySet()){
                                    submitUsers = entry.getValue().toString();
                                    break;
                                }
                                isDataSource = true;
                                break;
                            }
                        }
                    }
                }

                if (!isDataSource) {
                    //表达式计算出变量值
                    submitUsers = pdcdService.getSubmitUsers(pdc.getId(),businessData);
                }
                if (submitUsers!=null){
                    map.put(pdc.getConfigItem(), submitUsers);
                }

            }
        }


        //表单项目
        // itemId为File_name字段
        List<ProcessNodeFormPrivilege> list = processTaskDao.findVariableByNodeId(processDefinitionId,nodeId);
        List<String> columns=new ArrayList<String>();

        if(processDefinitionId == null){
            throw new Exception("流程定义ID不允许为空!");
        }
        if(list.size()>0){
            Map<String,Object> addMap= new HashMap<String,Object> ();
            for (ProcessNodeFormPrivilege formNode:list){
                columns.add(formNode.getItemId());
                String keyTemp="E"+formNode.getFormId()+"_"+formNode.getItemId();
                addMap.put(keyTemp,"");
            }

            String tableName = formService.getFormCodeByProDId(processDefinitionId);
            List< Map<String,Object>> resultList = formBusinessTableService.getBusinessData(tableName,columns,businessInstanceId);
            if (null!=resultList&&resultList.size()!=0) {
                for(Map<String,Object> hashmap : resultList){
                    if(null!=hashmap) {
                        Iterator iterator=hashmap.entrySet().iterator();
                        String key="";
                        while(iterator.hasNext())   {
                            Entry e = (Entry) iterator.next();
                            for (ProcessNodeFormPrivilege formNode:list){
                                if(e.getKey().equals(formNode.getItemId())){
                                    key="E"+formNode.getFormId()+"_"+formNode.getItemId();
                                    addMap.put(key,hashmap.get(formNode.getItemId()));
                                }
                            }
                        }
                    }
                }
            }

            map.putAll(addMap);
        }
		//会签任务节点变量赋值
//		ProcessNode countSignNode = new ProcessNode();
//		countSignNode.setProcessDefinitionId(processDefinitionId);
//		countSignNode.setIsCountSignNode("1"); //会签节点此处值应为1
//		List<ProcessNode> countSignNodeList = processNodeService.find(countSignNode);
//		if(countSignNodeList !=null && countSignNodeList.size() > 0){
//			//当前流程存在会签节点
//			for(ProcessNode node : countSignNodeList){
//				String key = "assigneeList_" + node.getNodeId();
//				key = "assigneeList";
//				List<SysAuthExpression> authList = sysAuthExpressionService.findExpressionByBusinessIdAndBusinessType(SysBussinessAuthExpression.BUSSINESS_TYPE_NODE,node.getId());
//				List<SysUser> userList = userAuthorizationService.getUserListBySysAuthExpressionList(authList);
//				if(userList != null && userList.size() > 0){
//					String[] signerErps = new String[userList.size()];
//					for(int i=0;i<userList.size();i++){
//						signerErps[i] = userList.get(i).getUserName();
//					}
//					map.put(key,signerErps);
//					logger.info("处理流程[" + processDefinitionId + "],会签节点[" + node.getNodeId() + "],会签人[" + JsonUtils.toJsonByGoogle(signerErps) + "]");
//				}
//			}
//		}

        return map;

    }

    private Map<String,Object> getBusinessData(String processDefId, String businessInstanceId){
        String tableName=formService.getFormCodeByProDId(processDefId);
        Map<String,Object> data = null;
        List<String> columns = new ArrayList<String>();

        List<Map<String, Object>> businessDataList = formBusinessTableService.getBusinessData(tableName,columns,businessInstanceId);
        if (businessDataList!=null && businessDataList.size()>0 && businessDataList.get(0)!=null){
            data = businessDataList.get(0);
        }
        return data;
    }
    
    
    
    private Map<String,Object> getMailVariablesByProcessTemp() {
        Map<String,Object>  map = new HashMap<String,Object>();

        map.put("processDefinitionName", "");
        map.put("processInstanceName", "");
        map.put("mailSubject", "");
        map.put("mailHeader", "");
        map.put("mail1", "");
        map.put("mail2", "");
        
        map.put("mailBussinessData", "");
        map.put("mailSubmitHistory", "");
        map.put("mailComment", "");
        map.put("mailFooter", "");

        return map;
    }

    /**
     *
     * @desc 获取流程变量-邮件提醒相关
     * @author WXJ
     * @date 2013-11-29 下午05:31:36
     *
     * @param submitUser
     * @param processInstanceName
     * @param processDefinitionId
     * @param businessInstanceId
     * @param result
     * @param comment
     * @return
     * @throws Exception
     */
    @Override
    public Map<String,Object> getMailVariablesByProcess(String submitUser,String processInstanceName,String nodeId,String processDefinitionId,String businessInstanceId,String result,String comment,OaTaskInstance oti,String processInstanceId) throws Exception {
        Map<String,Object>  map = new HashMap<String,Object>();

        ProcessInstance processInstance = new ProcessInstance();

        String mailSubject = "";

        String startTime = "";
        String startId = "";
        ProcessInstance pi = new ProcessInstance();
        pi.setBusinessInstanceId(businessInstanceId);    
        List<ProcessInstance> processInstanceList = processInstanceService.find(pi);
        if (processInstanceList!=null && processInstanceList.size()>0){ //审批
            processInstance = processInstanceList.get(0);
            startTime = DateUtils.datetimeFormat(processInstance.getBeginTime());
            SysUser su = sysUserService.get(processInstance.getStarterId());
            startId = su.getUserName();   
        }else{ //申请提交
        	startTime = DateUtils.datetimeFormat(new Date());
        	
        	SysUser su = sysUserService.getByUserName(submitUser); 
        	processInstance.setStarterId(su.getId());
        	processInstance.setBusinessInstanceId(businessInstanceId); 
        	processInstance.setProcessInstanceId(processInstanceId);
        	
            startId = submitUser;
        }

        ProcessDefinition temp = processDefService.get(processDefinitionId);

//
//        if(subSheet != null)
//            map.put("subSheet",subSheet);

        String processDefinitionName = temp.getProcessDefinitionName();
        mailSubject = "流程中心_"+processDefinitionName+"："+processInstanceName+"";

        //
//		String taskId= "";
//		String taskName = "";
//		if (instanceList!=null && instanceList.size()>0){
//			taskId = instanceList.get(0).getId();
//			taskName = instanceList.get(0).getName();
//		}


        String mailHeader = "<html><body style=\"FONT-SIZE: 12px\">" +
                "<table border='0' style='FONT-SIZE: 12px;' width='100%'>" +
                "<tr><td align='center' colspan='10' style='font-size: 20px; font-weight: bold; color: #0095CD;'>"+processDefinitionName+"-"+processInstanceName+"</td></tr>" +
                "<tr><td align='left' colspan='10' style='font-size: 20px; font-weight: bold; color: #0095CD;'>" +
                "<hr style='BORDER-BOTTOM-STYLE: dotted; BORDER-LEFT-STYLE: dotted; BORDER-RIGHT-STYLE: dotted; BORDER-TOP-STYLE: dotted; color: gray;'></td></tr>";

        String realName = sysUserService.getRealName(startId);
        SysUser sysUser = sysUserService.getByUserName(startId);
        SysOrganization sysOrganization = sysOrganizationService.get(sysUser.getOrganizationId());
        String deptName = sysOrganization.getOrganizationFullname();
        String mail1 = "<tr><td align='right'>申请人:</td><td style='font-weight:bold;'>"+realName+"</td>" +
                "<td align='right'>所属部门:</td><td style='font-weight:bold;'>"+deptName+"</td></tr>" ;

        String mail2 = "<tr><td align='right'>申请日期:</td><td style='font-weight:bold;'>"+startTime+"</td>"+
                "<td align='right'>流程类别:</td><td style='font-weight:bold;'>"+processDefinitionName+"</td></tr>";

//		String mail3 = "<tr><td align='right'>当前环节:</td><td style='font-weight:bold;'>"+taskName+"</td>" +
//					   "<td align='right'>主题:</td><td style='font-weight:bold;'>"+processInstanceName+"</td></tr></table>" +
//						"<hr>" ;

        String mailBussinessData = this.getMailBussinessData(submitUser, processInstanceName, processInstance, processDefinitionId, businessInstanceId, result, comment, oti);
        String mailSubmitHistory = this.getMailSubmitHistory(submitUser, processInstanceName, processInstance, processDefinitionId, businessInstanceId, result, comment, oti);
        
        String mailBussinessDataSubSheet = this.verifySubSheetBusinessDataHtmlForMail(temp,businessInstanceId);
        
        //待办任务邮件内容(申请人邮件内容不需要这部分)
//		String mailParam = "mailParam=taskId|"+taskId+";processInstanceId|"+processInstanceId+";" +
//				"taskDefinitionKey|"+nodeId+";createTime|"+startTime+";taskName|"+taskName+"" ;
//		String result1 = "mailto:jd-oa@jd.com?subject=流程中心_审批任务："+processInstanceName+"(批准)&body=审批结果：[批准]%0A%0D%0A审批意见：[  ]%0A%0D%0A" +
//				"PID[1/"+taskId+"/"+processInstanceId+"/"+nodeId+"/"+taskName+"/"+startTime+"]";
//		String result2 = "mailto:jd-oa@jd.com?subject=流程中心_审批任务："+processInstanceName+"(拒绝)&body=审批结果：[拒绝]%0A%0D%0A审批意见：[  ]%0A%0D%0A" +
//				"PID[2/"+taskId+"/"+processInstanceId+"/"+nodeId+"/"+taskName+"/"+startTime+"]";
//		String result3 = "mailto:jd-oa@jd.com?subject=流程中心_审批任务："+processInstanceName+"(驳回)&body=审批结果：[驳回]%0A%0D%0A审批意见：[  ]%0A%0D%0A" +
//				"PID[3/"+taskId+"/"+processInstanceId+"/"+nodeId+"/"+taskName+"/"+startTime+"]";
//
//		String mailLink = "详情页面审批：<a title='' href='http://oa.jd.com/app/taskHandle_email?"+mailParam+"'>"+processInstanceName+"["+taskName+"]</a><BR>" +
//							"<BR>" +
//							"邮件快捷审批：<a style='font-weight:bold;font-size:16px;' href=\""+result1+"\">[批准]</a>&nbsp;&nbsp;<a style='font-weight:bold;font-size:16px;' href=\""+result2+"\">[拒绝]</a><BR><BR>" ;

        String mailComment = "<span style='font-size:12px;color:#FF0000;'>敬请注意:您在进行审批操作时，若发现邮件内容乱码，请及时联系运维人员进行邮件客户端升级。 </span>";

        //
        String mailFooter = "</body></html>";

//      String mailContent = mailHeader+mail1+mail2+mail3+mailBussinessData+mailSubmitHistory+mailLink+mailComment+mailFooter;
//      String mailContentInit = mailHeader+mail1+mail2+mail3+mailBussinessData+mailSubmitHistory+mailFooter;

        map.put("processDefinitionName", processDefinitionName);
        map.put("processInstanceName", processInstanceName);
        map.put("mailSubject", mailSubject);
        map.put("mailHeader", mailHeader);
        map.put("mail1", mail1);
        map.put("mail2", mail2);
        String mailBussinessDataAll = mailBussinessData + mailBussinessDataSubSheet;
        
        map.put("mailBussinessData", mailBussinessDataAll);
        map.put("mailSubmitHistory", mailSubmitHistory);
        map.put("mailComment", mailComment);
        map.put("mailFooter", mailFooter);

        return map;
    }

    /**
     * 为邮件内容拼凑子表数据的html片段
     * @param pd
     * @param businessObjectId
     * @return
     */
	private String verifySubSheetBusinessDataHtmlForMail(ProcessDefinition pd,String businessObjectId){
    	StringBuffer html = new StringBuffer();
    	Form form = formService.get(pd.getFormId());//获取主表form
    	List<FormBean> subSheetForms = formItemService.getFormAndItemsByFormId(form.getId(),SystemConstant.FORM_TYPE_S);//获取子表
    	if(subSheetForms  != null && subSheetForms.size() > 0){
    		FormUIUtil formUIUtil = new FormUIUtil();
    		for(int i = 0 ; i < subSheetForms .size() ; i++){    	        
    			FormBean subSheetForm = subSheetForms .get(i);
    			//取得列表配置的显示项
    			if(subSheetForm != null){
	    			Map<String,String> configsMap = new LinkedHashMap<String,String>();
	    	        List<ProcessDefinitionConfigDetail> configs = pdcdService.findByMap(pd.getId(),subSheetForm.getFormCode(),"3");
	    	        for (ProcessDefinitionConfigDetail pdcd : configs) { 
	    	        	configsMap.put(pdcd.getValNo(),pdcd.getId()); 
	    	        }
    	        
    			
    				List<FormItem> subSheetFormItems = subSheetForm.getListFormItems();//表结构
    				if(subSheetFormItems != null && subSheetFormItems.size() > 0){
    					html.append("<span style='padding: 5px; width: 100px; text-align: left; font-size: 14px; color: #E4393C'>");
    					html.append(subSheetForm.getFormName());
    					html.append("</span>");
    					html.append("<BR>");    			        
    					html.append("<table cellspacing='0' style='FONT-SIZE: 12px;' width='100%'>");
    					//表头
    					html.append("	<thead>\n");
    					html.append("		<tr align='left' style='background: #ccc; border-top: 1px solid #a5a5a5; border-bottom: 1px solid #a5a5a5;'>\n");
    					Map<String,String> tableHeadMapExt1 = new LinkedHashMap<String,String>();
    					Map<String,FormItem> tableHeadMapExt2 = new LinkedHashMap<String,FormItem>();
    					Map<String,FormItem> tableHeadMap = new LinkedHashMap<String,FormItem>();
    					
    					StringBuffer htmlTemp;
    					for(int j = 0 ; j < subSheetFormItems.size() ; j++){
    						FormItem subSheetFormItem = subSheetFormItems.get(j);
    						htmlTemp = new StringBuffer();
    						if(subSheetFormItem != null){
    							htmlTemp.append("<td style='font-weight:bold;'>\n");
    							htmlTemp.append(subSheetFormItem.getFieldChineseName());
    							htmlTemp.append("</td>\n");
    							tableHeadMapExt1.put(subSheetFormItem.getFieldName(), htmlTemp.toString());	
    							tableHeadMapExt2.put(subSheetFormItem.getFieldName(), subSheetFormItem);								
    						}
    					}	
    					
    					//取得列表配置的显示项
    					Iterator itor = configsMap.entrySet().iterator();
						while(itor.hasNext()){
							Entry entry = (Entry)itor.next();
							String key = entry.getKey().toString();
							
							html.append(tableHeadMapExt1.get(key));
							tableHeadMap.put(key, tableHeadMapExt2.get(key));
						}					
						    					
    					
    					html.append("		</tr>\n");
    					html.append("	</thead>\n");
    					//表格体
    					List<Map<String,Object>> businessDatas = formBusinessTableService.getBusinessDatas(subSheetForm.getFormCode(),businessObjectId);//业务数据
    					if(businessDatas != null && businessDatas.size() > 0){
    						html.append("	<tbody>\n");
    						for(int k = 0 ; k < businessDatas.size() ; k++){
    							Map<String,Object> data = businessDatas.get(k);
    							if(data != null){
    								html.append("       <tr style='background-color: #edf5ff;'>\n");
    								Iterator iterator = tableHeadMap.entrySet().iterator();
    								while(iterator.hasNext()){
    									Entry entry = (Entry)iterator.next();
    									String key = entry.getKey().toString();
    									
    									html.append("   <td>\n");
    									if(data.get(key) != null){
    										String value = formUIUtil.buildMasterHashtableOfVerify1(contextPath,tableHeadMap.get(key) ,data.get(key).toString());
        									html.append("      " + value);
    									} else {
    										html.append("&nbsp;");
    									}
    									
    									html.append("   </td>\n");
    							
    								}
    								html.append("       </tr>\n");
    							}
    						}
    						html.append("	</tbody>\n");
    					}
    					
    					html.append("</table>\n");
    				}
    			}
    		}
    	}
    	return html.toString();
    }
    /**
     * 审批成功后插入任务历史数据
     * @param instance
     * @return
     */
    private void taskHistoryAdd(String submitUser,OaTaskInstance instance,String isComplete,String result,Date date){
        String systemUserID=sysUserService.getUserIdByUserName(submitUser,"1");
        ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
        processTaskHistory.setId(IdUtils.uuid2());    //ID
        processTaskHistory.setUserId(systemUserID);    // 用户ID
        processTaskHistory.setComment(instance.getComments());    //审批意见
        processTaskHistory.setInstanceIscomplete(isComplete);    //是否完成
        processTaskHistory.setResult(result);                    //结果
        processTaskHistory.setNodeId(instance.getNodeId());     //节点ID
        processTaskHistory.setTaskName(instance.getName());     //任务名称
        processTaskHistory.setPafProcessDefinitionId(instance.getProcessDefinitionKey()); //流程定义key
        processTaskHistory.setProcessInstanceId(instance.getProcessInstanceId());  //流程实例ID
        if(StringUtils.isNotEmpty(instance.getStartTime())) {
            String time= instance.getStartTime().replace("T"," ");
            Date dt = DateFormatUtils.parseDate(time,"yyyy-MM-dd HH:mm:ss");
            if (dt == null) {
            	String dtStr = DateUtils.datetimeFormatCst(instance.getStartTime(), "yyyy-MM-dd HH:mm:ss");
            	dt = DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(dtStr);
            }
            processTaskHistory.setBeginTime(dt);  //开始时间
        }
        processTaskHistory.setTaskType(instance.getTaskType());
        if(SystemConstant.TASK_KUBU_PROXY.equals(instance.getTaskType())) {
            processTaskHistory.setAssignerId(instance.getAssignerId());        //原代理人
        }else if(SystemConstant.TASK_KUBU_ADDSIGNER.equals(instance.getTaskType())) {
            processTaskHistory.setAssignerId(instance.getAddSignerUserId());    //加签人
        }
        processTaskHistory.setAssignerId(instance.getAssignerId());
        processTaskHistory.setEndTime(date);      //结束时间
        processTaskHistory.setTaskId(instance.getId());            //任务ID
        processTaskHistory.setCreator(submitUser);
        processTaskHistory.setCreateTime(new Date());
        processTaskDao.taskHistoryAdd(processTaskHistory) ;
    }

    /**
     * 加签审批历史记录                              r
     * @param task
     */
    private void  processSignerAdd(String submitUser,OaTaskInstance task){
        ProcessAddsigner processAddsigner =new ProcessAddsigner();

        String systemUserID=sysUserService.getUserIdByUserName(submitUser,"1");
        processAddsigner.setAddsignUserId(systemUserID);   //加签人
        processAddsigner.setNodeId(task.getNodeId());       //节点ID
        processAddsigner.setProcessInstanceId(task.getProcessInstanceId());   //流程实例ID
        processAddsigner.setUserId(task.getAddSignerUserId());     //
        processAddsigner.setStatus("1");

        //取出前一个未完成的任务设置status 为2
        List<ProcessAddsigner> list=processAddsignerDao.find(processAddsigner);
        if(list!=null&&list.size()>0){
            ProcessAddsigner pa=list.get(0);
            pa.setStatus("2");
            processAddsignerDao.update(pa);
        }
        processAddsigner.setTaskId(task.getId());     //任务ID
        processAddsigner.setCreator(systemUserID);         //创建人
        processAddsigner.setCreateTime(new Date());       //创建时间
        processAddsigner.setId(IdUtils.uuid2());  //ID
        processAddsigner.setStatus("2");
        processAddsigner.setYn(0);
        processAddsignerDao.insert(processAddsigner) ;
    }

    /**
     * 代理审批历史记录
     * @param task
     */
    private void  processProxyTaskAdd(String submitUser, OaTaskInstance task){
        ProcessProxyTask processProxyTask = new ProcessProxyTask();
        processProxyTask.setAssignerId(task.getAssignerId());
        String systemUserID=sysUserService.getUserIdByUserName(submitUser,"1");
        processProxyTask.setProxyUserId(systemUserID);
        processProxyTask.setOperationTime(new Date());
        processProxyTask.setId(IdUtils.uuid2());
        processProxyTask.setProcessInstanceId(task.getProcessInstanceId());
        processProxyTask.setTaskId(task.getId());
        processTaskDao.processProxyTaskAdd(processProxyTask);
    }
    /**
     *获取加签状态
     * @param oaTaskInstance
     * @return
     */
    @Override
    public Map<String,Object> processTaskIsRead (OaTaskInstance oaTaskInstance){
        Map<String,Object> map = new HashMap<String,Object>();
        ProcessNode node = processNodeService.findAddsignRule(oaTaskInstance.getProcessInstanceId(),oaTaskInstance.getNodeId());
        map.put("isRead",node.getAddsignRule());
        return  map;
    }

    @Override
    public int findTodoProcessTaskTotalNum() {
        Map<String,Object> map=processSearchService.findTodoProcessTaskTotalNum(ComUtils.getLoginNamePin());
        int totalNum=0;
        if(map!=null&&map.get("total")!=null){
            totalNum=Integer.parseInt(map.get("total").toString());
        }
        return totalNum;
    }

    @Override
    public int findProxyProcessTaskTotalNum() {
        Map<String,Object> map=processSearchService.findProxyProcessTaskTotalNum(ComUtils.getLoginNamePin());
        int totalNum=0;
        if(map!=null&&map.get("total")!=null){
            totalNum=Integer.parseInt(map.get("total").toString());
        }
        return totalNum;
    }

    /**
	 *
	 * @desc 邮件内容1
	 * @author WXJ
	 * @date 2013-11-22 下午03:50:26
	 *
	 * @return
	 */
	private String getMailSubject(String processDefId, String startTime){
	    String processDefinitionName = processDefService.get(processDefId).getProcessDefinitionName();
	
	    String mailSubject = "<tr><td align='right'>申请日期:</td><td style='font-weight:bold;'>"+startTime+"</td>"+
	            "<td align='right'>流程类别:</td><td style='font-weight:bold;'>"+processDefinitionName+"</td></tr>" ;
	    return mailSubject;
	}

	@Override
	public int findSuperviseProcessTaskNum() {
        return processSearchService.getProSuperviseTaskCountByUserId(ComUtils.getLoginNamePin(),null,new Conditions());
    }

    @Override
    public int findForwardProcessTaskTotalNum() {
        Map<String,Object> map=processSearchService.findForwardProcessTaskTotalNum(ComUtils.getLoginNamePin(),null);
        int totalNum=0;
        if(map!=null&&map.get("total")!=null){
            totalNum=Integer.parseInt(map.get("total").toString());
        }
        return totalNum;
    }

    /**
     *
     * @desc 邮件内容2
     * @author WXJ
     * @date 2013-11-22 下午03:50:26
     *
     * @param startId
     * @return
     */
    private String getMailContent(String startId){
        String realName = sysUserService.getRealName(startId);
        SysUser sysUser = sysUserService.getByUserName(startId);
        SysOrganization sysOrganization = sysOrganizationService.get(sysUser.getOrganizationId());
        String deptName = sysOrganization.getOrganizationFullname();
        String mailContent = "<tr><td align='right'>申请人:</td><td style='font-weight:bold;'>"+realName+"</td><td align='right'>所属部门:</td><td style='font-weight:bold;'>"+deptName+"</td></tr>" ;
        return mailContent;
    }

    /**
     *
     * @desc 邮件内容3:业务数据
     * @author WXJ
     * @date 2013-11-20 下午07:15:40
     *
     * @param processDefId
     * @param businessInstanceId
     * @return
     */
    private String getMailBussinessData (String submitUser, String processInstanceName, ProcessInstance processInstance, String processDefId, String businessInstanceId, String result, String comment, OaTaskInstance oti){
//    	SysUser sysUser = null;
//    	if (oti!=null){
//    		submitUser = processInstance.getStarterId();
//    		sysUser = sysUserService.get(submitUser);
//        }else{
//        	sysUser = sysUserService.getByUserName(submitUser);
//        }
    	
//        SysOrganization sysOrganization = sysOrganizationService.get(sysUser.getOrganizationId());
//        String organizationFullname = sysOrganization.getOrganizationFullname();
        
        //取得列表配置的显示项
        ProcessDefinitionConfig config=new ProcessDefinitionConfig();
        config.setProcessDefinitionId(processDefId);
        config.setConfigType("1");
        List<ProcessDefinitionConfig> configs=processDefinitionConfigService.find(config);

        ProcessDefinition pdf = processDefService.get(processDefId);
        Map<String,FormItem> formItemName = new HashMap<String,FormItem>();  //对字段进行转义操作
        List<String> columns = new ArrayList<String>();

        for(ProcessDefinitionConfig con:configs){
            columns.add(con.getConfigItem());
            //
            FormItem formItem=new FormItem();
            formItem.setFormId(pdf.getFormId());
            formItem.setFieldName(con.getConfigItem());
            List<FormItem> formItems=formItemService.find(formItem);
            formItemName.put(con.getConfigItem(),formItems.get(0));
        }


        StringBuffer mailBussinessData = new StringBuffer();
        mailBussinessData.append("<span style='padding: 5px; width: 100px; text-align: left; font-size: 14px; color: #E4393C'>业务申请基本信息</span>");
        mailBussinessData.append("<BR>");
        mailBussinessData.append("<table cellspacing='0' style='FONT-SIZE: 12px;' width='100%'>");
        mailBussinessData.append("<tr align='left' style='background: #ccc; border-top: 1px solid #a5a5a5; border-bottom: 1px solid #a5a5a5;'>");
//        mailBussinessData.append("<td style=\"font-weight:bold;\" >申请人</td>");
        mailBussinessData.append("<td style=\"font-weight:bold;\" >主题</td>");
//        mailBussinessData.append("<td style=\"font-weight:bold;\" >部门</td>");

        //取得Table名字
        String tableName=formService.getFormCodeByProDId(processDefId);
        List<Map<String, Object>> businessDataList = formBusinessTableService.getBusinessData(tableName,columns,businessInstanceId);

        for (String itemCode : columns){
            mailBussinessData.append("<td style=\"font-weight:bold;\">");


            //转义表单项标题
            mailBussinessData.append((formItemName.get(itemCode)).getFieldChineseName());
            mailBussinessData.append("</td>");
        }

        mailBussinessData.append("</tr>");

        mailBussinessData.append("<tr style='background-color: #edf5ff;'>");



//        if (oti!=null){
//            mailBussinessData.append("<td >"+sysUserService.get(processInstance.getStarterId()).getRealName()+"</td>");
//        }else{
//            mailBussinessData.append("<td >"+sysUserService.getRealName(submitUser)+"</td>");
//        }

        mailBussinessData.append("<td >"+processInstanceName);
//        mailBussinessData.append("<td >"+organizationFullname+"</td>");
        //流水单号
        if(processInstance.getFollowCode() != null && ! "".equals(processInstance.getFollowCode()) ){
            mailBussinessData.append("[");
            mailBussinessData.append(processInstance.getFollowCode());
            mailBussinessData.append("]");
        }
        mailBussinessData.append("</td>");


        for (String itemCode : columns){
            mailBussinessData.append("<td >");
            if (businessDataList!=null && businessDataList.size()>0 && businessDataList.get(0)!=null){
                Map<String,Object> data = businessDataList.get(0);
                for (Entry entry : data.entrySet()){
                    if (entry.getKey().equals(itemCode)){
                        //转义表单项业务数据
                        FormUIUtil formUIUtil = new FormUIUtil();
                        Map<String,String> businessMap=formUIUtil.buildMasterHashtableOfVerify(contextPath, formItemName.get(entry.getKey()),entry.getValue().toString());
                        mailBussinessData.append(businessMap.get(entry.getKey()));
                    }
                }
            }
            mailBussinessData.append("</td>");
        }

        mailBussinessData.append("</tr>");
        mailBussinessData.append("</table>");


        return mailBussinessData.toString();
    }

    /**
     *
     * @desc 邮件内容4:审批历史记录
     * @author WXJ
     * @date 2013-12-18 下午07:15:40
     *
     * @param processDefId
     * @param businessInstanceId
     * @return
     */
    private String getMailSubmitHistory (String submitUser, String processInstanceName, ProcessInstance processInstance, String processDefId, String businessInstanceId, String result, String comment, OaTaskInstance oti){
        StringBuffer mailBussinessData = new StringBuffer();
        String processInstanceId = "";
        if (processInstance!=null)
            processInstanceId = processInstance.getProcessInstanceId();

//        if (oti!=null && StringUtils.isNotEmpty(processInstanceId)){

            List<ProcessTaskHistory> pthList = this.getProcessTrajectory(processInstanceId);

            if (pthList!=null && pthList.size()>0){
            	mailBussinessData.append("<hr style='BORDER-BOTTOM-STYLE: dotted; BORDER-LEFT-STYLE: dotted; BORDER-RIGHT-STYLE: dotted; BORDER-TOP-STYLE: dotted; color: gray;'>");
                mailBussinessData.append("<span style='padding: 5px; width: 100px; text-align: left; font-size: 14px; color: #E4393C'>历史审批记录</span>");
                mailBussinessData.append("<BR>");
                mailBussinessData.append("<table style='font-size: 12px; border-collapse: collapse; width: 100%; border: 1px solid #666; cellspacing: 0'>");
                mailBussinessData.append("<tr align='left' style='background: #ccc; border-top: 1px solid #a5a5a5; border-bottom: 1px solid #a5a5a5;'>");
                mailBussinessData.append("<td style=\"font-weight:bold;\" nowrap>任务名称</td>");
                mailBussinessData.append("<td style=\"font-weight:bold;\" nowrap>审批人</td>");
                mailBussinessData.append("<td style=\"font-weight:bold;\" nowrap>任务状态</td>");
                mailBussinessData.append("<td style=\"font-weight:bold;\" nowrap>审批意见</td>");
                mailBussinessData.append("<td style=\"font-weight:bold;\" nowrap>任务创建时间</td>");
                mailBussinessData.append("<td style=\"font-weight:bold;\" nowrap>任务结束时间</td>");
                mailBussinessData.append("</tr>");

                for(ProcessTaskHistory pth : pthList){
                    mailBussinessData.append("<tr style='background-color: #edf5ff;'>");
                    mailBussinessData.append("<td>" + pth.getTaskName() + "</td>");
                    mailBussinessData.append("<td>" + pth.getUserId() + "</td>");
                    mailBussinessData.append("<td>" + pth.getResult().replace("0","申请").replace("1","批准").replace("2","拒绝").replace("3","驳回").replace("4","重新申请").replace("5","加签确认").replace("6","加签") + "</td>");
                    String commentTemp = pth.getComment()==null||"null".equals(pth.getComment())?"":pth.getComment();
                    mailBussinessData.append("<td>" + commentTemp + "</td>");
                    if (pth.getBeginTime()!=null && !"".equals(pth.getBeginTime().toString()))
                        mailBussinessData.append("<td>" + DateUtils.datetimeFormatCst(pth.getBeginTime().toString(),"yyyy-MM-dd HH:mm:ss") + "</td>");
                    else
                        mailBussinessData.append("<td>" + DateUtils.datetimeFormatCst(pth.getEndTime().toString(),"yyyy-MM-dd HH:mm:ss") + "</td>");
                    mailBussinessData.append("<td>" + DateUtils.datetimeFormatCst(pth.getEndTime().toString(),"yyyy-MM-dd HH:mm:ss") + "</td>");
                    mailBussinessData.append("</tr>");
                }

                if (oti!=null){

                    mailBussinessData.append("<tr>");
                    mailBussinessData.append("<td>" + oti.getName() + "</td>");
                    mailBussinessData.append("<td>" + sysUserService.getRealName(submitUser) + "</td>");
                    if (result!=null)
                        mailBussinessData.append("<td>" + result.replace("0","申请").replace("1","批准").replace("2","拒绝").replace("3","驳回").replace("4","重新申请").replace("5","加签确认").replace("6","加签") + "</td>");
                    else
                        mailBussinessData.append("<td></td>");
                    if (comment!=null)
                        mailBussinessData.append("<td>" + comment + "</td>");
                    else
                        mailBussinessData.append("<td></td>");
                    mailBussinessData.append("<td>" + oti.getStartTime() + "</td>");
                    mailBussinessData.append("<td>" + DateUtils.datetimeFormat(new Date()) + "</td>");
                    mailBussinessData.append("</tr>");

                }
                mailBussinessData.append("</table>");
            }
//        }


        return mailBussinessData.toString();
    }

    /**
     *
     * @desc 邮件和短信解析过程中获得OaTaskInstance
     * @author WXJ
     * @date 2013-11-21 上午10:53:23
     *
     * @param taskId
     * @param processInstanceId
     * @param comment
     * @return
     */
    private OaTaskInstance getTask(String taskId, String processInstanceId, String taskDefinitionKey, String comment, String taskName, String createTime){
        OaTaskInstance task = new OaTaskInstance();
        try {
            task.setId(taskId);
            task.setProcessInstanceId(processInstanceId);
            task.setComments(comment);

            ProcessInstance piTemp = new ProcessInstance();
            piTemp.setProcessInstanceId(processInstanceId);

            ProcessInstance pi = processInstanceService.find(piTemp).get(0);

            task.setProcessInstanceName(pi.getProcessInstanceName());
            task.setNodeId(taskDefinitionKey);
            task.setProcessDefinitionId(pi.getProcessDefinitionId());
            task.setBuinessInstanceId(pi.getBusinessInstanceId());

            task.setName(taskName);
            task.setProcessDefinitionKey("");
            task.setStartTime(createTime);

            ProcessAddsigner pa = processAddsignerDao.getAddSignTask(processInstanceId, taskId, "");
            if (pa!=null)
                task.setTaskType(SystemConstant.TASK_KUBU_ADDSIGNER);
        }catch(Exception e){
            log.error(e);
            return null;
        }
        return task;
    }

    /**
     *
     * @desc 获得上级经理
     * @author WXJ
     * @date 2014-1-7 下午02:06:38
     *
     * @param submitUser
     * @return
     */
    @Override
    public String getManagerUser(String submitUser) {
        String managerUser = "";
        StringBuilder managerUserSb = new StringBuilder("");
        //获得当前用户信息
        SysUser su = sysUserService.getByUserName(submitUser);
        if (StringUtils.isNotEmpty(su.getPositionParentCode())){
            //获得当前用户职位信息
//			SysPosition sysPosition = new SysPosition();
//			sysPosition.setPositionCode(su.getPositionCode());
//			SysPosition sp = sysPositionService.find(sysPosition).get(0);

            //找出当前用户上级职位、所属机构对应的用户
            SysUser sysUser = new SysUser();
            //sysUser.setOrganizationId(su.getOrganizationId());
            sysUser.setPositionCode(su.getPositionParentCode());
            List<SysUser> suList = sysUserService.find(sysUser);
            if (suList!=null && suList.size()>0){
                for (SysUser suTemp : suList){
//                    managerUser = managerUser + "," + suTemp.getUserName();
                    managerUserSb.append("," + suTemp.getUserName());
                }
                managerUser = managerUserSb.toString();
                managerUser = managerUser.substring(1);
            }else{
				logger.info("[计算用户"+submitUser+"，上级Code为"+su.getPositionCode()+"，结果上级领导为空，继续递归计算]");
                managerUser = getManagerByPostion(su.getPositionCode());
//				//找出当前用户上级职位、上级机构对应的用户
//				SysOrganization so = sysOrganizationService.get(su.getOrganizationId());
//				if (StringUtils.isNotEmpty(so.getParentId())){
//					sysUser = new SysUser();
//					sysUser.setOrganizationId(so.getParentId());
//					sysUser.setPositionCode(su.getPositionParentCode());
//					suList = sysUserService.find(sysUser);
//					if (suList!=null && suList.size()>0){
//						for (SysUser suTemp : suList){
//							managerUser = managerUser + "," + suTemp.getUserName();
//						}
//						 = managerUser.substring(1);
//					}
//				}else{  //上级机构为空
//					managerUser = "";
//				}
            }
        }else{  //上级职位、所属机构为空
			logger.info("[计算用户"+submitUser+"，上级code为空]");
            managerUser = "";
        }
		logger.info("[最终计算用户"+submitUser+"，managerUser="+managerUser+"]");
        return managerUser;
    }
    /**
     * 直接查询sysuser的parentcode为空时，根据position表中查询其parentCode,递归查询上级经理
     * @param positionCode
     * @return
     */
    private String  getManagerByPostion(String positionCode){
        String managerUser="";
        StringBuilder managerUserSb = new StringBuilder("");
        SysPosition sysPosition = new SysPosition();
        sysPosition.setPositionCode(positionCode);
        SysPosition sp = sysPositionService.find(sysPosition).get(0);


//		SysOrganization so = sysOrganizationService.get(su.getOrganizationId());
        if (StringUtils.isNotEmpty(sp.getParentId())){
            SysUser sysUser = new SysUser();
//            sysUser = new SysUser();
//			sysUser.setOrganizationId(so.getParentId());
            sysUser.setPositionCode(sp.getParentId());
            List<SysUser> suList = sysUserService.find(sysUser);
            if (suList!=null && suList.size()>0){
                for (SysUser suTemp : suList){
//                    managerUser = managerUser + "," + suTemp.getUserName();
                    managerUserSb.append("," + suTemp.getUserName());
                }
                managerUser = managerUserSb.toString();
                managerUser = managerUser.substring(1);
            }else{
                managerUser =  getManagerByPostion(sp.getParentId());
            }
        }else{  //上级机构为空
            managerUser = "";
        }
		logger.info("[计算岗位（"+positionCode+"）的父级岗位（"+sp.getParentId()+"）]的managerUser（"+managerUser+"）]");
        return managerUser;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {

        this.contextPath = servletContext.getRealPath("/");
    }


    public void processEvents(List<ProcessNodeListener> events, Map<String,Object> bussinessObject){
        if(events == null) return ;


        for (ProcessNodeListener event : events) {
            if (StringUtils.isNotEmpty(event.getInterfaceName())) {
                DiDatasource datasource = diDatasourceService.get( event.getInterfaceName());

                //如果是接口
                if(2==datasource.getDataAdapterType()){
                    List<Map<String,Object>>  list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, bussinessObject, "1");
                }

                else if(1==datasource.getDataAdapterType()){
                    List<Map<String,Object>>  list2 = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), datasource.getExecSql(), "1");
                }
            }

        }
    }

    @Override
    public com.jd.oa.common.paf.model.ProcessInstance processAppSubmitInstance(String processDefinitionKey, String nodeId, String formId, String businessObjectId, String boTableName, String processInstanceName, String taskPriority, String processInstanceId,String submitUser,String followCode)  throws Exception{
        ProcessDefinition processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
        Map<String,Object> mp = null;
        String currentUser = sysUserService.getUserIdByUserName(submitUser, SystemConstant.ORGNAIZATION_TYPE_HR);
        ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
        processTaskHistory.setBeginTime(new Date());
        processTaskHistory.setEndTime(new Date());//


        /*
         * 开始事件 已提至controller层
         */

        com.jd.oa.common.paf.model.ProcessInstance result = oaPafService.createProcessInstance(processDefinitionKey, null,submitUser);
        if(result == null){
            throw new Exception("流程实例创建失败!");
        }else{
            String pid = result.getId();
            mp = this.findMapByProcess(submitUser,processInstanceName,nodeId,processDefinition.getId(),businessObjectId,null,null,null,pid);
            if (mp == null) {
                throw new BusinessException("操作失败：获取流程变量失败。","获取流程变量失败。");
            }
			logger.info("[流程实例创建成功，开始推进第一个人工任务]");
            result = oaPafService.submitHumanProcessInstance(pid, mp,submitUser);
            if(result == null){
//            	formBusinessTableService.deleteBusinessObjectById(boTableName,businessObjectId,formId);
                throw new Exception("流程实例推进失败!");
            }
                    
        }

        com.jd.oa.process.model.ProcessInstance oaProcessInstance = processInstanceService.get(processInstanceId);
        if(oaProcessInstance == null){//新增操作
            oaProcessInstance = new com.jd.oa.process.model.ProcessInstance();
            oaProcessInstance.setBusinessInstanceId(businessObjectId);
            oaProcessInstance.setProcessDefinitionId(processDefinition.getId());
            oaProcessInstance.setProcessInstanceName(processInstanceName);
			oaProcessInstance.setFollowCode(followCode);
            oaProcessInstance.setProcessInstanceId(result.getId());
            oaProcessInstance.setStarterId(currentUser);
            oaProcessInstance.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
            oaProcessInstance.setBeginTime(new Date());
            oaProcessInstance.setPriority(taskPriority);//任务优先级
            oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_1);
            oaProcessInstance.setLastTaskTime(new Date());
            processInstanceService.insert(oaProcessInstance);
        } else {
            oaProcessInstance.setProcessInstanceId(result.getId());
            oaProcessInstance.setProcessDefinitionId(processDefinition.getId());
            oaProcessInstance.setProcessInstanceName(processInstanceName);
			oaProcessInstance.setFollowCode(followCode);
            oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_1);
            oaProcessInstance.setBeginTime(new Date());
            oaProcessInstance.setLastTaskTime(new Date());
            processInstanceService.update(oaProcessInstance);
        }

        //插入流程任务历史
        processTaskHistory.setTaskName("申请");
        processTaskHistory.setProcessInstanceId(result.getId());
        processTaskHistory.setPafProcessDefinitionId(processDefinition.getPafProcessDefinitionId());
        processTaskHistory.setNodeId(nodeId);
        processTaskHistory.setUserId(currentUser);//erpid
        processTaskHistory.setResult(SystemConstant.TASK_STATUS_0);
        processTaskHistory.setCreateTime(new Date());
        processTaskHistory.setModifyTime(new Date());
        processTaskHistory.setModifier(submitUser);
        processTaskHistory.setCreator(submitUser);
        processTaskHistory.setId(IdUtils.uuid2());
        processTaskHistory.setInstanceIscomplete("0");
        processTaskHistoryService.insertProcessTask(processTaskHistory);
        
        //通过MQ向JME推送信息
        oaPafService.sendMessage2JmeByMQ(result.getId());
                      
        
      /*
      * 结束事件 已提至controller层
      */
        
      //1.定时器：再次获取活动任务，为定时器推进流程作准备 
      //2.发送邮件给审批人，并发送给申请人, 同时发送给代理审批人
      //3.如果设置了限制职级，则自动提交，调用ProcessTaskComplete
      //以上3步全部拆分至 processAppSubmitInstanceNextStep方法，由Controller调用
        
        return result;
    }

    private boolean isTopperThanLevelLimit(String users,String levelLimit){
		logger.info("[计算["+users+"]职级是否大于["+levelLimit+"]]");
        if(users != null){
            String[] erps = users.split(",");
            for(String erp:erps){
                if(erp!=null && ! "".equals(erp)){
                    if(erp.equals(lczxgly))
                        return true;
                    SysUser user = sysUserService.getByUserName(erp);
                    if(user != null){
                        String level = user.getLevelCode();
                        if(level == null || "".equals(level) || level.toLowerCase().indexOf("m")<0)
                            return false;
                        if(level.toLowerCase().compareTo(levelLimit.toLowerCase()) > 0) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return false ;
    }
    
    
    
    
    /**
     * 每次提交或审批表单时发送邮件给审批人
     * @param submitUser
     * @param processInstanceId
     * @param processInstanceName
     * @param businessInstanceId
     * @param flag   0[审批]  2[拒绝] 3[驳回] 7[加签] 8[督办], 8,9 没有审批连接
     * @param sendToStarter 是否给申请人发送邮件
     * @param sendToProxy 是否给代理人发邮件
     */
    @Override
    public void sendMailToAssignee(String submitUser, String processInstanceId,
    								String processInstanceName,String businessInstanceId,
    								String flag, boolean sendToStarter, boolean sendToProxy) {
    	//再次获取活动任务
        List<OaTaskInstance> taskList = oaPafService.getActiveTaskByInstance(submitUser, processInstanceId);
        for(OaTaskInstance task : taskList)	{
            OaTaskInstance oti = oaPafService.getTaskDetail(submitUser, task.getId());
            if(oti == null) continue;
            
            try{
                Map<String,Object> resulMapMail = getMailVariablesByProcess(
                			submitUser, processInstanceName, oti.getNodeId(), 
                			oti.getProcessDefinitionId(), businessInstanceId,
                			"3", oti.getComments(), null, "");

                if(sendToStarter){		//是否发送给申请人
                	String subj = getMailSubject(resulMapMail,"9");		//标志9代表给申请人发送，获取对应的邮件模版
                    String cont = getMailContent(resulMapMail, oti, processInstanceName,"9");
                    sendMailToStarter(processInstanceId, subj, cont);	//发送给申请人
                }
                
                String content = getMailContent(resulMapMail, oti, processInstanceName,flag);
                String subject = getMailSubject(resulMapMail,flag);
                for (String s : oti.getCandidateUsers().split(",")) {
                	SysUser assignee = sysUserService.getByUserName(s);
                    if(assignee != null && assignee.getEmail() != null && !"".equals(assignee.getEmail()))	{
                    	mailSendService.sendHtmlMailByAsyn(new String[]{assignee.getEmail()}, subject, content);
						logger.info("[发送邮件给审批人]:to="+assignee.getEmail()+",subject="+subject);
                    }
                    
                    if(sendToProxy)	{		//是否发送给代理人
                    	List<ProcessProxy> result = processProxyDao.findByAssigneeId(assignee.getId());
                    	for(ProcessProxy pp : result)	{
	                    	SysUser proxy = sysUserService.get(pp.getProxyId());
	                    	if(proxy != null && proxy.getEmail() != null && !"".equals(proxy.getEmail()))	{
	                    		mailSendService.sendHtmlMailByAsyn(new String[]{proxy.getEmail()}, subject, content);
								logger.info("[发送邮件给代理审批人]:to="+assignee.getEmail()+",subject="+subject);
	                    	}
                    	}
                    }
                }
            } catch (Exception e){
                log.error(e.getMessage(), e);
            }                	
        }
    }


    /**
     * 发送邮件给指定的人，与申请看到的相同
     * @param submitUser
     * @param processInstanceId
     * @param processInstanceName
     * @param businessInstanceId
     * @param flag   0[待审批]  2[拒绝] 3[驳回] 7[加签] 8[督办], 8,9 没有审批、拒绝链接
     * @param mails
     */
    @Override
    public void sendMailToSomeone(String submitUser, String processInstanceId,String processInstanceName,String businessInstanceId, String flag, List<String> mails,List<String> users) {
        //再次获取活动任务
        List<OaTaskInstance> taskList = null;
        taskList = oaPafService.getActiveTaskByInstance(submitUser, processInstanceId);

        if(taskList != null){
            for(OaTaskInstance task :taskList){
                OaTaskInstance oti = oaPafService.getTaskDetail(submitUser, task.getId());

                if (oti!=null) {
                    try{
                        Map<String,Object> resulMapMail = getMailVariablesByProcess(submitUser,processInstanceName,oti.getNodeId(),oti.getProcessDefinitionId(),businessInstanceId,"3",oti.getComments(),null,"") ;

//                        String content = getMailContent(resulMapMail, oti, processInstanceName,"9");
//                        String subject = getMailSubject(resulMapMail,"9");

                        String subject = getMailSubject(resulMapMail,flag);

                        String content = null;
                        if("7".equals(flag)){
                            getForwardMailContent(resulMapMail, oti, processInstanceName, flag,mails,users,subject);
                            return ;
                        }

                        else
                            content = getMailContent(resulMapMail, oti, processInstanceName,flag);


                        if(mails != null){
                            for(String email  : mails){
                                if(  ! "".equals(email))
                                    mailSendService.sendHtmlMailByAsyn(new String[]{email},subject,content);
								logger.info("[发送邮件成功给指定人]:to="+email+",subject="+subject);
                            }
                        }


                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    //发给申请人
    private void sendMailToStarter(String processInstanceId,String subject,String content){
        ProcessInstance p =  new ProcessInstance();
        p.setProcessInstanceId(processInstanceId);
        List<ProcessInstance> list = processInstanceService.find(p);
        
        if(!list.isEmpty() && list.get(0).getStarterId() != null){
            SysUser u = sysUserService.get(list.get(0).getStarterId());
            if(u != null && u.getEmail() != null){
            	mailSendService.sendHtmlMailByAsyn(new String[]{u.getEmail()},subject,content);
				logger.info("[发送邮件成功给申请人]:to="+u.getEmail()+",subject="+subject);
            }
        }
    }

    /**
     * 根据标志status设置不同的邮件Subject
     * @param mailVariables
     * @param status
     * @return
     */
    @Override
    public String getMailSubject(Map<String,Object> mailVariables,String status){
        StringBuilder sb = new StringBuilder();
        sb.append("流程中心_");
        sb.append(mailVariables.get("processDefinitionName"));

        if("0".equals(status))
            sb.append("[待审批]");
        else if("2".equals(status))
            sb.append("[拒绝]");
        else if("3".equals(status))
            sb.append("[驳回]");
        else if("5".equals(status))
            sb.append("[已完成]");
        else if("7".equals(status))
            sb.append("[加签]");
        else if("8".equals(status))
            sb.append("[督办]");
        sb.append(":");
        sb.append(mailVariables.get("processInstanceName"));
//    	return mailVariables.get("mailSubject").toString();
        return sb.toString();
    }

    /**
     * 根据标志flag设置不同的邮件内容
     * @param mailVariables
     * @param task
     * @param processInstanceName
     * @param flag
     * @return
     */
    @Override
    public String getMailContent(Map<String,Object> mailVariables, OaTaskInstance task,String processInstanceName,String flag){
    	String mailFrom = PlatformProperties.getProperty("mail.smtp.default.from");

        String result1 = "mailto:"+mailFrom+"?subject=流程中心_审批任务："+processInstanceName+"(批准)&body=审批结果：[批准]%0A%0D%0A审批意见：[  ]%0A%0D%0APID[1/"+task.getId()+"/"+task.getProcessInstanceId()+"/"+task.getTaskDefinitionKey()+"/"+task.getName()+"/"+task.getCreateTime()+"]";
    	String result2 = "mailto:"+mailFrom+"?subject=流程中心_审批任务："+processInstanceName+"(拒绝)&body=审批结果：[拒绝]%0A%0D%0A审批意见：[  ]%0A%0D%0APID[2/"+task.getId()+"/"+task.getProcessInstanceId()+"/"+task.getTaskDefinitionKey()+"/"+task.getName()+"/"+task.getCreateTime()+"]";
//    	String result3 = "mailto:"+mailFrom+"?subject=流程中心_审批任务："+processInstanceName+"(驳回)&body=审批结果：[驳回]%0A%0D%0A审批意见：[  ]%0A%0D%0APID[3/"+task.getId()+"/"+task.getProcessInstanceId()+"/"+task.getTaskDefinitionKey()+"/"+task.getName()+"/"+task.getCreateTime()+"]";
			
		String mailParam = "mailParam=taskId|"+task.getId()+";processInstanceId|"+task.getProcessInstanceId()+";" +
						"taskDefinitionKey|"+task.getTaskDefinitionKey()+";createTime|"+task.getCreateTime()+";taskName|"+task.getName()+"" ;
		String content = "";
		
		ProcessNode node = processNodeService.findAddsignRule(task.getProcessDefinitionId(),task.getNodeId());
		String isFirstNode = node.getIsFirstNode();
		
        StringBuilder sb = new StringBuilder();
        
        content = mailVariables.get("mailHeader").toString()+mailVariables.get("mail1").toString()+mailVariables.get("mail2").toString() +
//        "<tr><td align='right'>当前环节:</td><td style='font-weight:bold;'>"+task.getName()+"</td><td align='right'>主题:</td><td style='font-weight:bold;'>"+processInstanceName+"</td></tr>" +
        "<tr><td align='right'>当前环节:</td><td style='font-weight:bold;'>"+task.getName()+"</td><td align='right'></td><td style='font-weight:bold;'></td></tr>" +
        "<tr><td colspan='10'><hr style='BORDER-BOTTOM-STYLE: dotted; BORDER-LEFT-STYLE: dotted; BORDER-RIGHT-STYLE: dotted; BORDER-TOP-STYLE: dotted; color: gray;'></td></tr></table>" ;
        
        //邮件审批连接
		String mailLink = "<hr style='BORDER-BOTTOM-STYLE: dotted; BORDER-LEFT-STYLE: dotted; BORDER-RIGHT-STYLE: dotted; BORDER-TOP-STYLE: dotted; color: gray;'>" +
                "详情页面审批：<a title='' href='http://oa.jd.com/app/taskHandle_email?"+mailParam+"'>"+processInstanceName+"</a><BR><BR>" +
                "邮件快捷审批：<a style='font-weight:bold;font-size:16px; color: #0095CD;' href=\""+result1+"\">[批准]</a>&nbsp;&nbsp;<a style='font-weight:bold;font-size:16px; color: #E4393C;' href=\""+result2+"\">[拒绝]</a><BR><BR>" ;
        

        sb.append(content);
        sb.append(mailVariables.get("mailBussinessData").toString());
        
        //督办的审批人，和申请人都不会有审批连接mailLink
        if( !"8".equals(flag) &&  !"9".equals(flag) && !"1".equals(isFirstNode)){
            sb.append(mailLink);
            sb.append(mailVariables.get("mailComment").toString() );
        }
        
        sb.append( mailVariables.get("mailSubmitHistory").toString());
        
        sb.append(mailVariables.get("mailFooter").toString());
	
		
    	return sb.toString();
    }

    private void getForwardMailContent(Map<String,Object> mailVariables, OaTaskInstance task,String processInstanceName,String flag,List<String> mails,List<String> users,String subject){
        String mailFrom = PlatformProperties.getProperty("mail.smtp.default.from");

        Map<String ,String > userToMail = new HashMap<String, String>();
        if(mails == null || mails.size() <= 0) return ;
        for(int i = 0 ; i<mails.size();i++){
            userToMail.put(users.get(i),mails.get(i));
        }

        List<Object> subTaskList = task.getSubTaskList();
        if(subTaskList != null && subTaskList.size() >0){
            for(int i=0;i<subTaskList.size();i++){
                Object t = subTaskList.get(i);
                String json = t.toString() ;
                int idStartIndex = json.indexOf("id=");
                int idEndIndex = json.indexOf(",",idStartIndex);

                String taskId = json.substring(idStartIndex+3,idEndIndex);

                int assignStartIndex = json.indexOf("assignee=");
                int assignEndIndex = json.indexOf(",",assignStartIndex);

                String assignee = json.substring(assignStartIndex+9,assignEndIndex);

                if( taskId == null || "".equals(taskId)|| assignee == null || "".equals(assignee)) continue;

                String result1 = "mailto:"+mailFrom+"?subject=流程中心_审批任务："+processInstanceName+"(同意)&body=审批结果：[同意]%0A%0D%0A审批意见：[  ]%0A%0D%0APID[1/"+taskId+"/"+task.getProcessInstanceId()+"/"+task.getTaskDefinitionKey()+"/"+task.getName()+"/"+task.getCreateTime()+"]";
                String result2 = "mailto:"+mailFrom+"?subject=流程中心_审批任务："+processInstanceName+"(不同意)&body=审批结果：[不同意]%0A%0D%0A审批意见：[  ]%0A%0D%0APID[1/"+taskId+"/"+task.getProcessInstanceId()+"/"+task.getTaskDefinitionKey()+"/"+task.getName()+"/"+task.getCreateTime()+"]";
               
                String mailParam = "mailParam=taskId|"+taskId+";processInstanceId|"+task.getProcessInstanceId()+";" +
                        "taskDefinitionKey|"+task.getTaskDefinitionKey()+";createTime|"+task.getCreateTime()+";taskName|"+task.getName()+"" ;
                String content = "";
                
                ProcessNode node = processNodeService.findAddsignRule(task.getProcessDefinitionId(),task.getNodeId());
        		String isFirstNode = node.getIsFirstNode();
        		
                StringBuilder sb = new StringBuilder();
                

                //邮件审批连接
                String mailLink = "<hr style='BORDER-BOTTOM-STYLE: dotted; BORDER-LEFT-STYLE: dotted; BORDER-RIGHT-STYLE: dotted; BORDER-TOP-STYLE: dotted; color: gray;'>" +
                        "详情页面审批：<a title='' href='http://oa.jd.com/app/taskHandle_email?"+mailParam+"'>"+processInstanceName+"</a><BR><BR>" +
                        "邮件快捷审批：<a style='font-weight:bold;font-size:16px; color: #0095CD;' href=\""+result1+"\">[同意]</a>&nbsp;&nbsp;<a style='font-weight:bold;font-size:16px; color: #E4393C;' href=\""+result2+"\">[不同意]</a><BR><BR>" ;
                content = mailVariables.get("mailHeader").toString()+mailVariables.get("mail1").toString()+mailVariables.get("mail2").toString() +
//                    "<tr><td align='right'>当前环节:</td><td style='font-weight:bold;'>"+task.getName()+"</td><td align='right'>主题:</td><td style='font-weight:bold;'>"+processInstanceName+"</td></tr>" +
                        "<tr><td align='right'>当前环节:</td><td style='font-weight:bold;'>"+task.getName()+"</td><td align='right'></td><td style='font-weight:bold;'></td></tr>" +
                        "<tr><td colspan='10'><hr style='BORDER-BOTTOM-STYLE: dotted; BORDER-LEFT-STYLE: dotted; BORDER-RIGHT-STYLE: dotted; BORDER-TOP-STYLE: dotted; color: gray;'></td></tr></table>" ;

                sb.append(content);
                sb.append( mailVariables.get("mailBussinessData").toString());
                
                //督办的审批人，和申请人都不会有审批连接mailLink
                if( !"8".equals(flag) &&  !"9".equals(flag) && !"1".equals(isFirstNode)){
                    sb.append(mailLink);
                    sb.append(	 mailVariables.get("mailComment").toString() );
                }
                sb.append( mailVariables.get("mailSubmitHistory").toString());
                sb.append(mailVariables.get("mailFooter").toString());
                
                mailSendService.sendHtmlMailByAsyn(new String[]{userToMail.get(assignee)},subject,sb.toString());
            }
        }


    }

           
    /**
     * 
     * @desc 流程定时器执行,供worker使用
     * @author WXJ
     * @date 2014-6-16 下午11:32:42
     *
     * @param timer
     * @return
     */
    public Boolean processTaskTimer(ProcessTaskTimer timer) {
    	try {
		    OaTaskInstance ot = oaPafService.getTaskDetail(lczxgly,timer.getTaskId());
		    if(ot!=null){
	            ot.setComments("到达设定自动执行时间（"+timer.getSoonTime()+"），流程自动执行。——流程中心。");
	            ot.setProcessInstanceName(timer.getProcessInstanceName());
	            ot.setProcessInstanceId(timer.getProcessInstanceId());
	            ot.setBuinessInstanceId(timer.getBusinessInstanceId());
	            String submitUser = null;
	            if (StringUtils.isNotEmpty(ot.getCandidateUsers())) {
	            	submitUser = ot.getCandidateUsers().split(",")[0];
	            }else {
	            	if (StringUtils.isNotEmpty(ot.getAssignee()))
	            		submitUser = ot.getAssignee();
	            }
	            
	            processTaskComplete(submitUser, ot);
	            
	            //定时器：更新定时器为已执行状态
        		timer.setStatus("1");
        		processTaskTimerService.update(timer);
                	
		    }
		    return true;
    	} catch(Exception e) {
    		log.info("流程定时器方法processTaskTimer执行失败。"+e);
    		return false;
    	}
    }
    
    /**
     * 
     * @author zhengbing 
     * @desc 拆分processAppSubmitInstance方法 1.为定时器推进流程作准备   2.发邮件   3.限制职级自动提交
     * @date 2014年7月17日 下午3:40:37
     * @return void
     */
    public void processAppSubmitInstanceNextStep(String submitUser,String processInstanceName,String businessObjectId,String pafProcessInstanceId,ProcessDefinition processDefinition) throws Exception{
        //定时器：再次获取活动任务，为定时器推进流程作准备 Add By WXJ At 2014-06-17 17:58:00
        List<OaTaskInstance> taskList = null;
        taskList = oaPafService.getActiveTaskByInstance(submitUser, pafProcessInstanceId);
        if(taskList != null){
            for(OaTaskInstance t :taskList){
                OaTaskInstance ot = oaPafService.getTaskDetail(submitUser,t.getId());
                if(ot!=null){
                	ProcessNode processNode = processNodeService.findAddsignRule(ot.getProcessDefinitionId(), ot.getNodeId());
                	if (processNode!=null && processNode.getTimerDelay()>0){
                		ProcessTaskTimer entity = new ProcessTaskTimer();
                		entity.setProcessDefinitionId(ot.getProcessDefinitionId());
                		entity.setProcessInstanceId(ot.getProcessInstanceId());
                		entity.setProcessInstanceName(processInstanceName);
                		entity.setBusinessInstanceId(businessObjectId);
                		entity.setNodeId(ot.getNodeId());
                		entity.setTaskId(ot.getId());
                		entity.setStatus("0");
                		//计算延时时间
                		int timerDelay = processNode.getTimerDelay();
                		Calendar c = Calendar.getInstance();
                	    c.setTime(new Date());
                	    c.add(Calendar.SECOND, timerDelay);
                		entity.setSoonTime(c.getTime());	                                		
                		processTaskTimerService.insert(entity);
                	}
                }
            }
        }
        
        //发送邮件给审批人，并发送给申请人, 同时发送给代理审批人
        sendMailToAssignee(submitUser,pafProcessInstanceId,processInstanceName,businessObjectId,"0",true, true);
        
        //如果设置了限制职级，则自动提交，调用ProcessTaskComplete
        String levelLimit = processDefinition.getTopLevelCode();
        if (StringUtils.isNotEmpty(levelLimit)) {
            //再次获取活动任务
            if(taskList != null){
                for(OaTaskInstance t :taskList){
                    OaTaskInstance ot = oaPafService.getTaskDetail(submitUser,t.getId());
                    if(ot!=null){
                        String usersStr = ot.getCandidateUsers();

                        boolean isAutoComplete = isTopperThanLevelLimit(usersStr,levelLimit);
                        if(isAutoComplete){
                            ot.setComments("已经过该流程设置的最高级别审批，此节点跳过。——流程中心。");
                            ot.setProcessInstanceName(processInstanceName);
                            ot.setProcessInstanceId(pafProcessInstanceId);
                            ot.setBuinessInstanceId(businessObjectId);
                            processTaskComplete(lczxgly, ot);
                        }
                    }
                }
            }
        }
    }
    
    
    
}
