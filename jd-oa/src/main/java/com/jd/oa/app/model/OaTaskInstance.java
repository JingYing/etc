package com.jd.oa.app.model;

import java.util.ArrayList;
import java.util.List;

/**
 * OA任务实例
 * User: WXJ
 * Date: 13-11-12
 * Time: 下午3:00
 */
public class OaTaskInstance {

    /**
     * 任务实例ID
     */
    private String id;

    /**
     * 任务英文名称
     */
    private String taskDefinitionKey;

    /**
     * 任务中文名称
     */
    private String name;

    /**
     * 优先级
     */
    private String priority;

    /**
     * 所属流程实例ID
     */
    private String processInstanceId;
    /**
     * 主题名称
     */
    private String processInstanceName;
    /**
     * OA中流程定义ID
     */
    private String processDefinitionId;

    /**
     * OA中流程定义Name
     */
    private String processDefinitionName;
    
    /**
     * 流程定义Key
     */
    private String processDefinitionKey;

	/**
	 * 外部流程 oa:0,外部链接:1,统一待办:2
	 */
	private String processDefinitionIsOuter;
    
    /**
     * 流程表单
     */
    private String formId;
    
    /**
     * 单个办理人
     */
    private String assignee;
    
    /**
     * 节点ID
     */
    private String nodeId;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 启动时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;
    
    /**
     * 拥有人
     */
    private String owner;
    
    /**
     * 完成标记：true 完成；false 未完成。
     */
    private String completed;
    
    /**
     * 持续时间
     */
    private String duration;
    
    /**
     * 审批意见
     */
    private String comments;
    
    /**
     * 办理人
     */
    private String candidateUsers;
    
    /**
     * 是否存在未完成的加签子任务
     */
    private String uncompletedSubTaskExist;
    
    /**
     * 任务类别 
     */
    private String taskType;//自己的任务/代理的/转发的
    /**
     * 业务实例ID
     */
    private String buinessInstanceId;
    /**
     * 代办人
     */
    private String assignerId;
    /**
     * 加签人
     */
    private String addSignerUserId;
    
    /**
     * 当前节点是否首节点 true:是 false：否
     */
    private boolean isFirstNode;
    
    private int count;
    
    /**
     * 是否代理任务
     */
    private String isProxy;
    
    /**
     * pafHumanProcessNextTask
     */
    private String pafHumanProcessNextTask;
    
    List<Object> subTaskList = new ArrayList<Object>();

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriority() {
        return priority;
    }
    
    public boolean isFirstNode() {
		return isFirstNode;
	}

	public void setFirstNode(boolean isFirstNode) {
		this.isFirstNode = isFirstNode;
	}

	public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getAssignee() {
        return assignee;
    }
	
	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getProcessDefinitionKey() {
		return processDefinitionKey;
	}

	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}
	
	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getProcessDefinitionName() {
		return processDefinitionName;
	}

	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCompleted() {
		return completed;
	}

	public void setCompleted(String completed) {
		this.completed = completed;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
    public String getCandidateUsers() {
		return candidateUsers;
	}

	public void setCandidateUsers(String candidateUsers) {
		this.candidateUsers = candidateUsers;
	}

	public String getUncompletedSubTaskExist() {
		return uncompletedSubTaskExist;
	}

	public void setUncompletedSubTaskExist(String uncompletedSubTaskExist) {
		this.uncompletedSubTaskExist = uncompletedSubTaskExist;
	}

	public String getBuinessInstanceId() {
        return buinessInstanceId;
    }

    public void setBuinessInstanceId(String buinessInstanceId) {
        this.buinessInstanceId = buinessInstanceId;
    }
    
    public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
    public String getAssignerId() {
        return assignerId;
    }

    public void setAssignerId(String assignerId) {
        this.assignerId = assignerId;
    }

    public String getAddSignerUserId() {
        return addSignerUserId;
    }

    public void setAddSignerUserId(String addSignerUserId) {
        this.addSignerUserId = addSignerUserId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getProcessInstanceName() {
        return processInstanceName;
    }

    public void setProcessInstanceName(String processInstanceName) {
        this.processInstanceName = processInstanceName;
    }

	public String getIsProxy() {
		return isProxy;
	}

	public void setIsProxy(String isProxy) {
		this.isProxy = isProxy;
	}

	public String getPafHumanProcessNextTask() {
		return pafHumanProcessNextTask;
	}

	public void setPafHumanProcessNextTask(String pafHumanProcessNextTask) {
		this.pafHumanProcessNextTask = pafHumanProcessNextTask;
	}    
    
	public List<Object> getSubTaskList() {
		return subTaskList;
	}

	public void setSubTaskList(List<Object> subTaskList) {
		this.subTaskList = subTaskList;
	}

	public String getProcessDefinitionIsOuter() {
		return processDefinitionIsOuter;
	}

	public void setProcessDefinitionIsOuter(String processDefinitionIsOuter) {
		this.processDefinitionIsOuter = processDefinitionIsOuter;
	}
}
