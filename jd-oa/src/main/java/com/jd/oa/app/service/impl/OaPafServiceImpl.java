package com.jd.oa.app.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jd.oa.agent.model.AgentTodoList;
import com.jd.oa.agent.service.AgentTodoListService;
import com.jd.oa.process.service.ProcessDefService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.common.util.DateFormatUtils;
import com.jd.oa.api.service.MqProduceJmeService;
import com.jd.oa.app.model.Conditions;
import com.jd.oa.app.model.OaProcessInstance;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.mail.MailSendService;
import com.jd.oa.common.paf.model.FormProperties;
import com.jd.oa.common.paf.model.IdentityLinkResponse;
import com.jd.oa.common.paf.model.PafResult;
import com.jd.oa.common.paf.model.ProcessInstance;
import com.jd.oa.common.paf.model.TaskInstance;
import com.jd.oa.common.paf.model.processinstance.ProcessInstanceDomain;
import com.jd.oa.common.paf.model.processinstance.Task;
import com.jd.oa.common.paf.service.PafService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.process.dao.ProcessInstanceDao;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;

/**
 * OA工作流平台PAF服务实现
 * User: WXJ
 * Date: 13-10-11
 * Time: 下午8:18
 */
@Service
public class OaPafServiceImpl implements OaPafService{

    private final static Log LOG = LogFactory.getLog(OaPafServiceImpl.class);

	@Autowired
	private PafService pafService;
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private ProcessSearchService processSearchService;
	@Autowired
	private ProcessInstanceService processInstanceService;
    @Autowired
    private ProcessTaskHistoryService processTaskHistoryService;
    @Autowired
    private SysUserService sysUserService;
	@Autowired
	private ProcessNodeService processNodeService;
	@Autowired
	private ProcessTaskService processTaskService;
	@Autowired
	private ProcessInstanceDao processInstanceDao;
	@Autowired
	private MailSendService mailSendService;
	@Autowired
	private AgentTodoListService agentTodoListService;
	@Autowired
	private MqProduceJmeService mqProduceJmeService;
	
	@Override
	public ProcessInstance createProcessInstance(String processDefKey, String businessKey) {
		// TODO Auto-generated method stub
		PafResult<ProcessInstance> pafResult = pafService.createProcessInstance(ComUtils.getLoginNamePin(), processDefKey, businessKey);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return null;
	}

	@Override
	public ProcessInstance createProcessInstance(String processDefKey, String businessKey, String loginUser) {
		if(loginUser == null || loginUser.trim().length() == 0) return createProcessInstance(processDefKey,businessKey);
		PafResult<ProcessInstance> pafResult = pafService.createProcessInstance(loginUser, processDefKey, businessKey);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return null;
	}

	@Override
	public ProcessInstance submitHumanProcessInstance(String processInstanceId, Map<String, Object> variables) {
		// TODO Auto-generated method stub
		PafResult<ProcessInstance> pafResult = pafService.submitHumanProcessInstance(ComUtils.getLoginNamePin(), processInstanceId, variables);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return null;
		
	}

	@Override
	public ProcessInstance submitHumanProcessInstance(String processInstanceId, Map<String, Object> variables, String loginUser) {
		if(loginUser == null || loginUser.trim().length() == 0) return submitHumanProcessInstance(processInstanceId,variables);
		PafResult<ProcessInstance> pafResult = pafService.submitHumanProcessInstance(loginUser, processInstanceId, variables);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return null;
		
	}

	@Override
	public ProcessInstance submitHumanProcessInstance(String processDefKey, String businessKey, Map<String, Object> variables) {
		// TODO Auto-generated method stub
		PafResult<ProcessInstance> pafResult = pafService.submitHumanProcessInstance(ComUtils.getLoginNamePin(), processDefKey, businessKey, variables);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return null;
	}

	@Override
	public Boolean completeUserTask(String loginUser, String taskId, Map<String, Object> variables) {
		// TODO Auto-generated method stub
		
		LOG.info("OaPafServiceImpl.completeUserTask:variables:"+variables.toString());
		PafResult<Boolean> pafResult = pafService.completeUserTask(loginUser, taskId, variables);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return false;
	}

	@Override
	public List<OaTaskInstance> getTaskIncludePafAndBiz(String loginUser, String processDefinitionKey, Conditions conditions) {
		ProcessDefinition processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
		if(processDefinition.getIsOuter().equals(SystemConstant.PROCESS_DEFINITION_OA)){
			return getTask(loginUser,processDefinitionKey,conditions);
		}else if(processDefinition.getIsOuter().equals(SystemConstant.PROCESS_DEFINITION_BIZ)){
			Map<String,Object> map = getBizTaskAndAllTotal(loginUser,processDefinitionKey);
			return (List<OaTaskInstance>) map.get("taskList");
		}else{
			return null;
		}
	}


	@Override
	public List<OaTaskInstance> getTask(String loginUser, String processDefinitionKey, Conditions conditions) {
		// TODO Auto-generated method stub
		Map mp = null;
		//转换conditions为Map
		if (conditions == null){
			conditions = new Conditions();
		}
		mp = ComUtils.pojoToMap(conditions);
		
//		PafResult<List<TaskInstance>> pafResult = pafService.taskQuery(ComUtils.getLoginNamePin(), processDefinitionKey, mp);
		PafResult<List<TaskInstance>> pafResult = pafService.taskQuery(loginUser, processDefinitionKey, mp);
				
		if (pafResult.isSuccess()) {			
			return getOaTaskInstance(pafResult.getResult());
		} else {
			return null;
		}
			
	}

	@Override
	public List<OaTaskInstance> getTask(final String loginUser, List<String> processDefKeyList, final Conditions conditions) {
		// TODO Auto-generated method stub
		final List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();
		List<Thread> threads=new ArrayList<Thread>();
		if(processDefKeyList != null && processDefKeyList.size() > 0){
			for (final String pdKey : processDefKeyList){
				Thread thread=new Thread(new Runnable() {
					
					@Override
					public void run() {
						List<OaTaskInstance> tempList = getTask(loginUser, pdKey, conditions);
						if(null!=tempList){
							synchronized (otiList) {
								otiList.addAll(tempList);
							}
						}
					}
				});
				threads.add(thread);
				thread.start();
		
			}
			for(Thread t:threads){
				try {
					t.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return otiList;
		
	}
	
	@Override
	public List<OaTaskInstance> getTask(String loginUser, Conditions conditions) {
		// TODO Auto-generated method stub
		List<String> pdKeyList = processSearchService.findProcessDefAll();
		return getTask(loginUser, pdKeyList, conditions);
	}
	 
	@Override
	public List<OaTaskInstance> getCompleteTask(String loginUser, String completed, String processDefinitionKey, Conditions conditions) {
		// TODO Auto-generated method stub
		Map mp = null;
		//转换conditions为Map
		if (conditions == null){
			conditions = new Conditions();
		}
		mp = ComUtils.pojoToMap(conditions);
		
		List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();
		
		//过滤completed		
		PafResult<List<ProcessInstance>> pafPiResult = pafService.processInstQuery(loginUser, processDefinitionKey,null);		
		if (pafPiResult.isSuccess()) {			
			
			for (ProcessInstance pi : pafPiResult.getResult()){
				//获取流程实例详细信息
				ProcessInstanceDomain pio = pafService.getProcessInstanceDetail(loginUser, pi.getId()).getResult();
				if (!"all".equals(completed)){
					if (completed.equals(pio.getCompleted())){
						
					}					
				}else{					
					List<Task> tempList = pio.getTasks();		
					List<Task> resultList = new ArrayList<Task>();	
					
					if(null!=tempList){
						for (Task task : tempList){
							if ("true".equals(task.getCompleted())){					
								
							}							
						}				
					}
					otiList.addAll(getOaTaskInstanceByTask(pio.getProcessInstanceId(),resultList));
				}
				
			}	
		}		

		return otiList;
	}
	
	@Override
	public List<OaTaskInstance> getCompleteTask(String loginUser, String completed, List<String> processDefKeyList, Conditions conditions) {
		// TODO Auto-generated method stub
		List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();
		for (String pdKey : processDefKeyList){
			List<OaTaskInstance> tempList = getCompleteTask(loginUser, completed, pdKey, conditions);
			if(null!=tempList){
				otiList.addAll(tempList);
			}
		}
		return otiList;
	}
	
	@Override
	public List<OaTaskInstance> getCompleteTask(String loginUser, String completed, Conditions conditions) {
		// TODO Auto-generated method stub
		List<String> pdKeyList = processSearchService.findProcessDefAll();
		return getCompleteTask(loginUser, completed, pdKeyList, conditions);
	}

	@Override
	public OaProcessInstance getTrackTaskByInstance(String processInstId, Conditions conditions) {		
		return this.getTrackTaskByInstance("", processInstId, conditions);
	}
	@Override
	public OaProcessInstance getTrackTaskByInstance(String loginUser,String processInstId, Conditions conditions) {
		PafResult<ProcessInstanceDomain> pafResult = pafService.getProcessInstanceDetail(("").equals(loginUser)?ComUtils.getLoginNamePin():loginUser, processInstId);
		ProcessInstanceDomain pio = pafResult.getResult();
		OaProcessInstance opi = new OaProcessInstance();		
		if (pafResult.isSuccess()) {	
			opi.setBusinessKey(pio.getBusinessKey());
			opi.setCompleted(pio.getCompleted());
			opi.setProcessDefinitionId(pio.getProcessDefinitionId());
			opi.setProcessInstanceId(pio.getProcessInstanceId());
			opi.setStartActivityId(pio.getStartActivityId());
			opi.setStartTime(pio.getStartTime());
			opi.setStartUserId(pio.getStartUserId());
			
			opi.setTasks(getOaTaskInstanceByTask(opi.getProcessInstanceId(), pio.getTasks()));
			
		}else{
            return null;
        }
		return opi;
	}

	@Override
	public List<OaTaskInstance> getActiveTask(String processDefinitionKey, Conditions conditions) {
		// TODO Auto-generated method stub
		List<com.jd.oa.process.model.ProcessInstance> piList = processInstanceService.findProcessInstanceByKey(processDefinitionKey);
		//返回
		List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();		
		for (com.jd.oa.process.model.ProcessInstance pi : piList){
			List<OaTaskInstance> otiTempList = this.getActiveTaskByInstance(pi.getProcessInstanceId());
			otiList.addAll(otiTempList);
		}
		
		return otiList;
	}	
	
	@Override
	public List<OaTaskInstance> getActiveTask(List<String> processDefKeyList, final Conditions conditions) {
		final List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();
		List<Thread> threads=new ArrayList<Thread>();
		for (final String pdKey : processDefKeyList){
			Thread thread=new Thread(new Runnable() {
				
				@Override
				public void run() {
					List<OaTaskInstance> tempList = getActiveTask(pdKey, conditions);
					if(null!=tempList){
						synchronized (otiList) {
							otiList.addAll(tempList);
						}
					}
				}
			});
			threads.add(thread);
			thread.start();
	
		}
		for(Thread t:threads){
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return otiList;
	}	
	
	@Override
	public List<OaTaskInstance> getActiveTaskByInstance(String processInstId) {		
		return this.getActiveTaskByInstance("", processInstId);
	}	
	
	@Override
	public List<OaTaskInstance> getActiveTaskByInstance(String loginUser, String processInstId) {
		// TODO Auto-generated method stub
		List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();
		OaProcessInstance instance = this.getTrackTaskByInstance(("").equals(loginUser)?ComUtils.getLoginNamePin():loginUser, processInstId, null);
		if(instance!=null){
			List<OaTaskInstance> tempList = instance.getTasks();			
			if(null!=tempList){
				for (OaTaskInstance oti : tempList){
					if ("false".equals(oti.getCompleted())){					
						otiList.add(oti);
					}							
				}				
			}
		}
		
		return otiList;
	}


	    	
//	@Override
//	public List<Comment> getCommentListByInstance(String processInstId) {
//		// TODO Auto-generated method stub
//		List<OaTaskInstance> otiList = getTrackTaskByInstance(processInstId,null).getTasks();
//		List<Comment> cmList = new ArrayList<Comment>();
//		if(null!=otiList){
//			for (OaTaskInstance oti : otiList){
//				cmList.addAll(oti.getComments());
//			}
//		}
//		return cmList;
//		
//	}
	
	
//	@Override
//	public List<String> getUsers(String processInstId) {
//		// TODO Auto-generated method stub
//		List<OaTaskInstance> otiList = getActiveTaskByInstance(processInstId);
//		List<String> users = new ArrayList<String>();
//		
//		if(null!=otiList){
//			for (OaTaskInstance oti : otiList){
//				String usersStr = oti.getCandidateUsers();
//				String[] usersArray = usersStr.split(",");
//				for (int i=0;i<usersArray.length;i++){
//					users.add(usersArray[i]);
//				}				
//			}
//		}
//		return users;
//	}
	
	@Override
	public String getUsers(String taskId) {
		// TODO Auto-generated method stub
		String users = "";
		StringBuilder usersSb = new StringBuilder("");
		OaTaskInstance oti = this.getTaskDetail(taskId);
		if (oti != null){
			String usersStr = oti.getCandidateUsers();
			String[] usersArray = usersStr.split(",");
			
			for (int i=0;i<usersArray.length;i++){
				SysUser sysUser = sysUserService.getByUserName(usersArray[i]);
				String realName = "";
				if (sysUser!=null) 
					realName = sysUser.getRealName();	
				usersSb.append(realName + ",");
//				users = users + realName + ",";
			}
			users = usersSb.toString();
			users = users.substring(0,users.length() - 1);
			
		}
		return users;		
	}

	@Override
	public List<SysUser> getUsers(String loginUser,String taskId) {
		List<SysUser> users = new ArrayList<SysUser>();
		OaTaskInstance oti = this.getTaskDetail(loginUser,taskId);
		if (oti != null){
			String usersStr = oti.getCandidateUsers();
			String[] usersArray = usersStr.split(",");

			for (int i=0;i<usersArray.length;i++){
				SysUser sysUser = sysUserService.getByUserName(usersArray[i]);
				if(sysUser!=null) users.add(sysUser);
			}
		}
		return users;
	}
	
	@Override
	public Boolean addSign(String processInstId, String taskId, List<String> userAddedList) {
		// TODO Auto-generated method stub
		PafResult<Boolean> pafResult = pafService.createSubTask(ComUtils.getLoginNamePin(), taskId, userAddedList);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return false;
	}

	@Override
	public Boolean rejectToPrevious(String loginUser, String taskId, Map<String, Object> variables) {
		// TODO Auto-generated method stub
		
		PafResult<Boolean> pafResult = pafService.completeToPreviousUserTask(loginUser, taskId, variables);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return false;
		
	}

	@Override
	public Boolean rejectToFirst(String loginUser, String taskId, Map<String, Object> variables) {
		// TODO Auto-generated method stub
		
		PafResult<Boolean> pafResult = pafService.completeToFirstUserTask(loginUser, taskId, variables);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return false;
		
	}
	
	/**
	 * 通过MQ向JME推送信息
	 * 
	 * @param processInstId
	 */
	@Override
	public void sendMessage2JmeByMQ(String processInstId){
		com.jd.oa.process.model.ProcessInstance processInstance = 
				new com.jd.oa.process.model.ProcessInstance();
		processInstance.setProcessInstanceId(processInstId);
		List<com.jd.oa.process.model.ProcessInstance> processInstances = processInstanceService.find(processInstance);
		
		if(null != processInstances && !processInstances.isEmpty()){
			//得到流程申请人
			String starterId = processInstances.get(0).getStarterId();
			SysUser user = sysUserService.get(starterId);
			
			//申请时间
			String beginTime = DateUtils.datetimeFormat24(new Timestamp(processInstances.get(0).getBeginTime().getTime()));
			
			//申请单ID
			String reqId = processInstances.get(0).getBusinessInstanceId();
			
			//申请单主题
			String reqName = processInstances.get(0).getProcessInstanceName();
			
			//得到待办任务
			List<OaTaskInstance> tasks = this.getActiveTaskByInstance(processInstId);
			if(null != tasks && !tasks.isEmpty()){
				for(OaTaskInstance task:tasks){
					//得到所有的办理人
					String[] assignees = task.getCandidateUsers().split(",");
					for(int i=0; i<assignees.length; i++){
						//处理消息Json串
				    	Map jsonMap = new HashMap();
				    	jsonMap.put("processKey", task.getProcessDefinitionKey());    //流程key
				    	jsonMap.put("processName", task.getProcessDefinitionName());  //流程名称
				    	jsonMap.put("reqId", reqId);                                  //申请单Id
				    	jsonMap.put("taskId", task.getId());                          //待办任务Id
				    	jsonMap.put("taskName", task.getName());                      //待办任务名称
				    	jsonMap.put("assignee", assignees[i]);                        //待办人
				    	jsonMap.put("reqName", reqName);        //申请单主题
				    	if(null != user.getUserName() && !user.getUserName().equals("")){
				    		jsonMap.put("reqUserErp", user.getUserName());            //申请人ERP
				    		jsonMap.put("reqUserName", user.getRealName());           //申请人姓名
				    	}else{
				    		jsonMap.put("reqUserErp", "");
				    		jsonMap.put("reqUserName", "");
				    	}
				    	jsonMap.put("comments", task.getComments());                  //申请备注
				    	jsonMap.put("reqTime", beginTime);                            //申请时间
				    	//String createTime = task.getCreateTime();
				    	String startTime = task.getStartTime();
				    	jsonMap.put("startTime", startTime);                          //任务到达时间
				    	
				    	String textMessage = DataConvertion.object2Json(jsonMap);
				    	System.out.println(textMessage);
				    	//mqProduceJmeService.mqProducer(textMessage);
					}
				}
			}	
		}
	}

	@Override
	public Boolean addTaskComment(String processInstId, String taskId, String comment) {
		return this.addTaskComment("", processInstId, taskId, comment);
	}
	
	@Override
	public Boolean addTaskComment(String loginUser, String processInstId, String taskId, String comment) {
		// TODO Auto-generated method stub
		PafResult<Boolean> pafResult = pafService.addTaskComment(("").equals(loginUser)?ComUtils.getLoginNamePin():loginUser, taskId, processInstId, comment);
		if (pafResult.isSuccess())
			return pafResult.getResult();
		else
			return false;
	}

	
	/**
	 * paf待办任务列表转换成OA专用的待办任务列表
	 * @desc 将TaskInstance扩展为OaTaskInstance
	 * @author WXJ
	 * @date 2013-10-14 上午11:36:45
	 * @param tiList
	 * @return
	 */
	private List<OaTaskInstance> getOaTaskInstance(List<TaskInstance> tiList){
		LOG.info("[PAF待办任务列表转换成OA待办任务列表，传入size="+(tiList==null?0:tiList.size())+"]");
		List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();
		if (tiList != null){	
			for (TaskInstance ti : tiList){
				OaTaskInstance oti = new OaTaskInstance();
				try {
					ComUtils.copyProperties(ti, oti);
					
					//计算属性
					if (ti.getSubTaskList()!=null && ti.getSubTaskList().size()>0)
						oti.setUncompletedSubTaskExist("true");
					else
						oti.setUncompletedSubTaskExist("false");
					
					if (ti.getIdentityLinkList()!=null && ti.getIdentityLinkList().size()>0) {
						String candidateUsers = "";
						StringBuilder candidateUsersSb = new StringBuilder("");
						for (IdentityLinkResponse ilr : ti.getIdentityLinkList()) {
							if (StringUtils.isNotEmpty(ilr.getUserId()))
								candidateUsersSb.append(ilr.getUserId() + ",");
//								candidateUsers = candidateUsers + ilr.getUserId() + ","; 
						}
						candidateUsers = candidateUsersSb.toString();
						candidateUsers = candidateUsers.substring(0,candidateUsers.length() - 1);
						oti.setCandidateUsers(candidateUsers);
					}
					
					
					//扩展oti属性
					ProcessDefinition pd = processSearchService.findProcessDefByInstance(oti.getProcessInstanceId());
					if(pd!=null){
						oti.setProcessDefinitionId(pd.getId());
						oti.setProcessDefinitionKey(pd.getPafProcessDefinitionId());
						oti.setProcessDefinitionName(pd.getProcessDefinitionName());
						oti.setFormId(pd.getFormId());
						oti.setNodeId(oti.getTaskDefinitionKey());
						oti.setProcessDefinitionIsOuter(pd.getIsOuter());
	                    if(oti.getCreateTime()!=null && !oti.getCreateTime().equals("")){
	                        oti.setStartTime(oti.getCreateTime().replaceAll("CST","").replaceAll("T"," "));
	                    }
	                    
						otiList.add(oti);
					}else{
						LOG.info("[PAF待办任务列表转换成OA待办任务列表减少一条记录，因processInstanceId("+oti.getProcessInstanceId()+")对应ProcessDefintion找不到]");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		LOG.info("[PAF待办任务列表转换成OA待办任务列表，传出size="+otiList.size()+"]");
		return otiList;		
	}	
	
	/**
	 * 
	 * @desc 将Task扩展为OaTaskInstance
	 * @author WXJ
	 * @date 2013-10-15 上午11:36:45
	 *
	 * @param tiList
	 * @return
	 */
	private List<OaTaskInstance> getOaTaskInstanceByTask(String processInstId, List<Task> tiList){
		LOG.info("[PAF待办任务列表转换成OA待办任务列表，传入size="+(tiList==null?0:tiList.size())+"]");
		List<OaTaskInstance> otiList = new ArrayList<OaTaskInstance>();
		if (tiList != null){
			for (Task ti : tiList){
				OaTaskInstance oti = new OaTaskInstance();
				try {
					//Copy属性
					oti.setId(ti.getTaskId());
					oti.setName(ti.getTaskName());
					if (ti.getComments()!=null && ti.getComments().size()>0)
						oti.setComments(ti.getComments().get(0).getContent());
					//Task
					oti.setCompleted(ti.getCompleted());
					oti.setDuration(ti.getDuration());
                    if(ti.getEndTime()!=null && !ti.getEndTime().equals("")){
                        oti.setEndTime(ti.getEndTime().replaceAll("CST","").replaceAll("T"," "));
                    }
					oti.setOwner(ti.getOwner());
                    if(ti.getStartTime()!=null && !ti.getStartTime().equals("")){
                        oti.setStartTime(ti.getStartTime().replaceAll("CST","").replaceAll("T"," "));
                    }
                    if(ti.getStartTime()!=null && !ti.getStartTime().equals("")){
                        oti.setCreateTime(ti.getStartTime().replaceAll("CST","").replaceAll("T"," "));
                    }
                    oti.setCandidateUsers(ti.getCandidateUsers());
                    oti.setUncompletedSubTaskExist(ti.getUncompletedSubTaskExist());
					//Instance
					oti.setProcessInstanceId(processInstId);
								
					//扩展oti属性
					ProcessDefinition pd = processSearchService.findProcessDefByInstance(oti.getProcessInstanceId());
					if(pd!=null){
						oti.setProcessDefinitionId(pd.getId());
						oti.setProcessDefinitionKey(pd.getPafProcessDefinitionId());
						oti.setProcessDefinitionName(pd.getProcessDefinitionName());
						oti.setFormId(pd.getFormId());
						oti.setProcessDefinitionIsOuter(pd.getIsOuter());
						//NodeId
						oti.setNodeId(oti.getTaskDefinitionKey());
						
						otiList.add(oti);
					}else{
						LOG.info("[PAF待办任务列表转换成OA待办任务列表减少一条记录，因processInstanceId("+oti.getProcessInstanceId()+")对应ProcessDefintion找不到]");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		LOG.info("[PAF待办任务列表转换成OA待办任务列表，传出size="+otiList.size()+"]");
		return otiList;		
	}
	
	/**
	 * 
	 * @desc 将Conditions对象转换为PAF能识别的Map
	 * @author WXJ
	 * @date 2013-10-17 下午05:12:35
	 *
	 * @param conditions
	 * @return
	 */
	private Map convertPafMap(Conditions conditions){
		Map mp = ComUtils.pojoToMap(conditions);	
		if (conditions.getCreateStartDate()!=null){
			mp.remove("createStartDate");
			mp.put("createStartDate_>=", conditions.getCreateStartDate());
		}
		if (conditions.getCreateEndDate()!=null){
			mp.remove("createEndDate");
			mp.put("createEndDate_<=", conditions.getCreateEndDate());
		}	
		if (conditions.getMinDueDate()!=null){
			mp.remove("minDueDate");
			mp.put("minDueDate_>=", conditions.getMinDueDate());
		}
		if (conditions.getMaxDueDate()!=null){
			mp.remove("maxDueDate");
			mp.put("maxDueDate_>=", conditions.getMaxDueDate());
		}
		return mp;
	}

    /**
     * 查询流程实例已结案的任务历史  --赵明
     * @param loginUser 登陆用户
     * @param processDefinitionKey 流程定义KEY
     * @param conditions 查询条件实体类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectEndProcessTaskHistory(String loginUser, String processDefinitionKey, Conditions conditions) {
        ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
        processTaskHistory.setUserId(sysUserService.getUserIdByUserName(loginUser));
        processTaskHistory.setPafProcessDefinitionId(processDefinitionKey);                                             //流程定义key
        if (conditions != null){
            if(conditions.getProcessInstanceId() != null && !conditions.getProcessInstanceId().equals("")){
                processTaskHistory.setProcessInstanceId(conditions.getProcessInstanceId());                             //流程实例ID
            }
            if(conditions.getBeginTimeStart() != null && !conditions.getBeginTimeStart().equals("")){
                processTaskHistory.setBeginTimeStart(conditions.getBeginTimeStart());                                   //任务创建开始时间
            }
            if(conditions.getBeginTimeEnd() != null && !conditions.getBeginTimeEnd().equals("")){
                processTaskHistory.setBeginTimeEnd(conditions.getBeginTimeEnd());                                       //任务创建结束时间
            }
            if(conditions.getEndTimeStart() != null && !conditions.getEndTimeStart().equals("")){
                processTaskHistory.setEndTimeStart(conditions.getEndTimeStart());                                       //任务结束开始时间
            }
            if(conditions.getEndTimeEnd() != null && !conditions.getEndTimeEnd().equals("")){
                processTaskHistory.setEndTimeEnd(conditions.getEndTimeEnd());                                           //任务创建结束时间
            }

        }

        return processTaskHistoryService.selectEndProcessTaskHistory(processTaskHistory);
    }

    /**
     * 查询流程实例未结案的任务历史   --赵明
     * @param loginUser 登陆用户
     * @param processDefinitionKey 流程定义KEY
     * @param conditions 查询条件实体类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectProcessTaskHistory(String loginUser, String processDefinitionKey, Conditions conditions) {
        ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
        processTaskHistory.setUserId(sysUserService.getUserIdByUserName(loginUser));
        processTaskHistory.setPafProcessDefinitionId(processDefinitionKey);                                             //流程定义key
        if (conditions != null){
            if(conditions.getProcessInstanceId() != null && !conditions.getProcessInstanceId().equals("")){
                processTaskHistory.setProcessInstanceId(conditions.getProcessInstanceId());                             //流程实例ID
            }
            if(conditions.getBeginTimeStart() != null && !conditions.getBeginTimeStart().equals("")){
                processTaskHistory.setBeginTimeStart(conditions.getBeginTimeStart());                                   //任务创建开始时间
            }
            if(conditions.getBeginTimeEnd() != null && !conditions.getBeginTimeEnd().equals("")){
                processTaskHistory.setBeginTimeEnd(conditions.getBeginTimeEnd());                                       //任务创建结束时间
            }
            if(conditions.getEndTimeStart() != null && !conditions.getEndTimeStart().equals("")){
                processTaskHistory.setEndTimeStart(conditions.getEndTimeStart());                                       //任务结束开始时间
            }
            if(conditions.getEndTimeEnd() != null && !conditions.getEndTimeEnd().equals("")){
                processTaskHistory.setEndTimeEnd(conditions.getEndTimeEnd());                                           //任务创建结束时间
            }

        }

        return processTaskHistoryService.selectProcessTaskHistory(processTaskHistory);
    }

    /**
     * 获取我发起的流程实例信息    --赵明
     * @param loginUser 登陆用户
     * @return 流程实例列表
     */
    public List<com.jd.oa.process.model.ProcessInstance> getMyProcessInstance(String loginUser){
        List<com.jd.oa.process.model.ProcessInstance> processInstanceList = new ArrayList<com.jd.oa.process.model.ProcessInstance>();

        //获取当前申请人发起的待办任务
        Map<String, Object> conditions = new HashMap<String, Object>();
        conditions.put("init",loginUser);
        conditions.put("start","0");
        conditions.put("size","500");
        conditions.put("sort", "createTime"); 
        conditions.put("order", "desc");
        
        PafResult<List<TaskInstance>> pafResult = pafService.taskQueryAdvanced(loginUser, conditions);
        if(pafResult.isSuccess()){
            List<TaskInstance> taskInstancesList =  pafResult.getResult();
            for(TaskInstance taskInstance : taskInstancesList){
                com.jd.oa.process.model.ProcessInstance processInstance = new com.jd.oa.process.model.ProcessInstance();
                processInstance.setProcessInstanceId(taskInstance.getProcessInstanceId());
                //根据流程实例ID获取流程实例信息
                processInstance = processInstanceService.selectByProcessInstanceId(processInstance);
                
                if(processInstance != null){
                	List<OaTaskInstance> oaTaskInstanceList = new ArrayList<OaTaskInstance>();
                    //装载历史任务信息
                    ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
                    processTaskHistory.setProcessInstanceId(taskInstance.getProcessInstanceId());
                    List<ProcessTaskHistory> processTaskHistoryList = processTaskHistoryService.selectProcessTaskHistory(processTaskHistory);
                    for(ProcessTaskHistory taskHistory : processTaskHistoryList){
                        OaTaskInstance oaTaskInstance = new OaTaskInstance();
                        oaTaskInstance.setId(taskHistory.getTaskId());                                                      //任务ID
                        oaTaskInstance.setProcessInstanceId(taskHistory.getProcessInstanceId());                            //流程实例ID
                        oaTaskInstance.setName(taskHistory.getTaskName());                                                  //任务名称
                        oaTaskInstance.setAssignee(taskHistory.getUserId());                                                //审批人
                        oaTaskInstance.setComments(taskHistory.getComment());                                               //审批意见
						try {
							oaTaskInstance.setStartTime(DateFormatUtils.format(taskHistory.getBeginTime(),"yyyy-MM-dd HH:mm:ss"));     //任务创建时间
							oaTaskInstance.setEndTime(DateFormatUtils.format(taskHistory.getEndTime(),"yyyy-MM-dd HH:mm:ss"));         //任务结束时间
						} catch (Exception e) {
							LOG.info(e);
							e.printStackTrace();
						}
						oaTaskInstance.setCount(processTaskHistoryList.size());                                             //第几步
                        oaTaskInstance.setNodeId(taskHistory.getNodeId());
                        oaTaskInstanceList.add(oaTaskInstance);
                        break;
                    }
                    //装载待办任务信息
                    OaTaskInstance waitTaskInstance = new OaTaskInstance();
                    
                    waitTaskInstance.setId(taskInstance.getId());                                                           //任务ID
                    waitTaskInstance.setProcessInstanceId(taskInstance.getProcessInstanceId());                             //流程实例ID
                    waitTaskInstance.setName(taskInstance.getName());                                                       //任务名称
                    waitTaskInstance.setStartTime(taskInstance.getCreateTime());                                            //任务创建时间
                    waitTaskInstance.setCount(processTaskHistoryList.size()+1);                                             //第几步
                    //WXJ  2013-11-01        
                    waitTaskInstance.setTaskDefinitionKey(taskInstance.getTaskDefinitionKey());
                    waitTaskInstance.setNodeId(taskInstance.getTaskDefinitionKey());
                    //WXJ  2013-11-18
                    FormProperties fp = taskInstance.getFormProperties().get(0);
                    if (fp != null && ("PAF_HUMAN_PROCESS_NEXT_TASK").equals(fp.getName()))
                    	waitTaskInstance.setPafHumanProcessNextTask(fp.getValue());
                    
                    oaTaskInstanceList.add(waitTaskInstance);

                    //将任务处理轨迹装填到流程实例中
                    processInstance.setTasks(oaTaskInstanceList);
                    processInstance.setCurrentStep(processTaskHistoryList.size()+2);//当前节点步数
                    processInstanceList.add(processInstance);
                }
            }
        }
        return processInstanceList;
    }
    
    /**
     * 获取我发起的流程实例信息    --许林
     * @param loginUser 登陆用户
     * @return 流程实例列表
     */
    public List<com.jd.oa.process.model.ProcessInstance> getMyProcessInstanceOther(String loginUser) {

        List<com.jd.oa.process.model.ProcessInstance> processInstanceList = new ArrayList<com.jd.oa.process.model.ProcessInstance>();

        //获取当前申请人发起的待办任务
        Map<String, Object> conditions = new HashMap<String, Object>();
        conditions.put("init",loginUser);
        conditions.put("start","0");
        conditions.put("size","500");
        conditions.put("sort", "createTime");
        conditions.put("order", "desc");

        //PafResult<List<TaskInstance>> pafResult = pafService.taskQueryAdvanced(loginUser, conditions);   
        String starterId = sysUserService.getUserIdByUserName(loginUser);
        List<String> status = new ArrayList<String>();
//        status.add("0");
        status.add("1");
        status.add("2");
        status.add("3");
        List<String> processInstances = processInstanceService.findAllInstanceByStartId(starterId, status);
        PafResult<List<TaskInstance>> pafResult = pafService.getProcessCurrentUserTasks(loginUser, processInstances);
        
        if(pafResult.isSuccess()){
        	//一次性读取数据，避免在循环中多次读取数据库 许林
            List<String> processInstanceIds = new ArrayList<String>();
            List<TaskInstance> taskInstancesList =  pafResult.getResult();
            for(TaskInstance taskInstance : taskInstancesList){
            	processInstanceIds.add(taskInstance.getProcessInstanceId());
            }
            Map<String,com.jd.oa.process.model.ProcessInstance> tempMap = new HashMap<String, com.jd.oa.process.model.ProcessInstance>();
            Map<String,List<ProcessTaskHistory>> tempTaskHistory = null;
            if(processInstanceIds.size() > 0){
            	//获取流程实例详细信息
            	List<com.jd.oa.process.model.ProcessInstance> tempList = this.processInstanceService.selectByProcessInstanceIds(processInstanceIds);
            	if(tempList != null && tempList.size() > 0){
            		for(com.jd.oa.process.model.ProcessInstance bean:tempList){
            			tempMap.put(bean.getProcessInstanceId(), bean);
            		}
            	}
            	
            	//获取流程历史任务信息
            	tempTaskHistory = this.processTaskHistoryService.selectProcessTaskHistoryAll(processInstanceIds);
            }
            
            com.jd.oa.process.model.ProcessInstance processInstance = null;
            Map<String,String> mapCheck = new HashMap<String, String>();
            for(TaskInstance taskInstance : taskInstancesList){
                processInstance = tempMap.get(taskInstance.getProcessInstanceId());
                if(processInstance != null && !mapCheck.containsKey(processInstance.getProcessInstanceId())){
                	List<OaTaskInstance> oaTaskInstanceList = new ArrayList<OaTaskInstance>();
                	//装载历史任务信息
                    ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
                    processTaskHistory.setProcessInstanceId(taskInstance.getProcessInstanceId());
                    List<ProcessTaskHistory> processTaskHistoryList = new ArrayList<ProcessTaskHistory>();
                    if(tempTaskHistory != null){
                    	processTaskHistoryList = tempTaskHistory.get(taskInstance.getProcessInstanceId());
                    }
                    
                	boolean isFirstNode = false;
                	String currentStatus = processInstance.getStatus();
                	//判断当前节点是否是首节点
					ProcessNode node = this.processNodeService.findAddsignRule(processInstance.getProcessDefinitionId(), taskInstance.getTaskDefinitionKey());
					if(node!=null){
						if("1".equals(node.getIsFirstNode())){
							isFirstNode = true;
						} else {
							isFirstNode = false;
						}
					}
					ProcessTaskHistory taskHistory = null;
                    if(isFirstNode){
                    	//驳回状态或者拒绝到申请节点时只装载当前节点即申请节点
                    	//装载待办任务信息
                        OaTaskInstance waitTaskInstance = new OaTaskInstance();
                        
                        waitTaskInstance.setId(taskInstance.getId());                                                           //任务ID
                        waitTaskInstance.setProcessInstanceId(taskInstance.getProcessInstanceId());                             //流程实例ID
                        waitTaskInstance.setName(taskInstance.getName());                                                       //任务名称
                        waitTaskInstance.setStartTime(taskInstance.getCreateTime());                                            //任务创建时间
                        waitTaskInstance.setCount(processTaskHistoryList.size()+1);                                             //第几步
                        waitTaskInstance.setTaskDefinitionKey(taskInstance.getTaskDefinitionKey());
                        waitTaskInstance.setNodeId(taskInstance.getTaskDefinitionKey());
                        waitTaskInstance.setFirstNode(isFirstNode);
                        waitTaskInstance.setCandidateUsers(taskInstance.getAssignee());//当前节点处理人
                      
                        //WXJ  2013-11-18
                        if(taskInstance.getFormProperties() != null && taskInstance.getFormProperties().size() > 0){
                        	FormProperties fp = taskInstance.getFormProperties().get(0);
                            if (fp != null && ("PAF_HUMAN_PROCESS_NEXT_TASK").equals(fp.getName()))
                            	waitTaskInstance.setPafHumanProcessNextTask(fp.getValue());
                        } else {
                        	waitTaskInstance.setPafHumanProcessNextTask("未获取");
                        }
                        
                        oaTaskInstanceList.add(waitTaskInstance);
                        
                        processInstance.setCurrentStep(processTaskHistoryList.size()+1);//当前节点步数
                    } else if(!SystemConstant.PROCESS_STATUS_2.equals(currentStatus)
                    		&& (processTaskHistoryList != null && processTaskHistoryList.size() > 0)){
                    	if(processTaskHistoryList.size() == 1){
                    		//刚发起的流程且下级经理还未审批，此时还是可以取消申请的，新增一个状态6
                    		processInstance.setStatus(SystemConstant.PROCESS_STATUS_6);
                    	}
                    	//从历史节点中装载第一个节点即申请节点和最后一个节点即当前节点的前置节点
                    	for(int i=processTaskHistoryList.size()-1;i >= 0;i--){
                    		if(i==processTaskHistoryList.size()-1 || i==0){
                    			taskHistory = processTaskHistoryList.get(i);
                        		OaTaskInstance oaTaskInstance = new OaTaskInstance();
                                oaTaskInstance.setId(taskHistory.getTaskId());                                                      //任务ID
                                oaTaskInstance.setProcessInstanceId(taskHistory.getProcessInstanceId());                            //流程实例ID
                                if(SystemConstant.TASK_STATUS_4.equals(taskHistory.getResult())){
                                	oaTaskInstance.setName(SystemConstant.TASK_STATUS_NAME_4);
                                } else {
                                	oaTaskInstance.setName(taskHistory.getTaskName());                                              //任务名称
                                }
                                oaTaskInstance.setAssignee(taskHistory.getUserId());                                                //审批人
                                oaTaskInstance.setComments(taskHistory.getComment());                                               //审批意见
								try {
									oaTaskInstance.setStartTime(DateFormatUtils.format(taskHistory.getBeginTime(),"yyyy-MM-dd HH:mm:ss"));     //任务创建时间
									oaTaskInstance.setEndTime(DateFormatUtils.format(taskHistory.getEndTime(),"yyyy-MM-dd HH:mm:ss"));         //任务结束时间
								} catch (Exception e) {
									LOG.info(e);
									e.printStackTrace();
								}
								if(i==0){
                                	oaTaskInstance.setCount(processTaskHistoryList.size());                                         //当前节点前置
                                } else {
                                	oaTaskInstance.setCount(1);                                                                     //申请
                                }
                                oaTaskInstance.setNodeId(taskHistory.getNodeId());
                                oaTaskInstance.setCandidateUsers(taskHistory.getUserId());//任务处理人
                                oaTaskInstanceList.add(oaTaskInstance);
                    		}
                    	}
                        //装载待办任务信息
                        OaTaskInstance waitTaskInstance = new OaTaskInstance();
                        
                        waitTaskInstance.setId(taskInstance.getId());                                                           //任务ID
                        waitTaskInstance.setProcessInstanceId(taskInstance.getProcessInstanceId());                             //流程实例ID
                        waitTaskInstance.setName(taskInstance.getName());                                                       //任务名称
                        waitTaskInstance.setStartTime(taskInstance.getCreateTime());                                            //任务创建时间
                        waitTaskInstance.setCount(processTaskHistoryList.size()+1);                                             //第几步
                        waitTaskInstance.setTaskDefinitionKey(taskInstance.getTaskDefinitionKey());
                        waitTaskInstance.setNodeId(taskInstance.getTaskDefinitionKey());
                        waitTaskInstance.setFirstNode(false);
                        waitTaskInstance.setCandidateUsers(taskInstance.getAssignee());//当前节点处理人
                        
                        //WXJ  2013-11-18
                        if(taskInstance.getFormProperties() != null && taskInstance.getFormProperties().size() > 0){
                        	FormProperties fp = taskInstance.getFormProperties().get(0);
                            if (fp != null && ("PAF_HUMAN_PROCESS_NEXT_TASK").equals(fp.getName()))
                            	waitTaskInstance.setPafHumanProcessNextTask(fp.getValue());
                        } else {
                        	waitTaskInstance.setPafHumanProcessNextTask("未获取");
                        }
                        
                        oaTaskInstanceList.add(waitTaskInstance);
                        
                        processInstance.setCurrentStep(processTaskHistoryList.size()+1);//当前节点步数
                    } else {
                    	//拒绝的场合
                    	//从历史节点中装载第一个节点即申请节点,最后一个节点即当前节点的前置节点和倒数第二个节点也就是现在的节点
                    	if(processTaskHistoryList!=null){
                    		for(int i=processTaskHistoryList.size()-1;i >= 0;i--){
//                        		if(i==processTaskHistoryList.size()-1 || i==processTaskHistoryList.size()-2 || i==0){
                        		if(i==processTaskHistoryList.size()-1 || i==1 || i==0){
                        			taskHistory = processTaskHistoryList.get(i);
                            		OaTaskInstance oaTaskInstance = new OaTaskInstance();
                                    //oaTaskInstance.setId(taskHistory.getTaskId());                                                      //任务ID
                            		oaTaskInstance.setId(taskInstance.getId());                                                         //拒绝时二次产生的当前任务ID 
                                    oaTaskInstance.setProcessInstanceId(taskHistory.getProcessInstanceId());                            //流程实例ID
                                    if(SystemConstant.TASK_STATUS_4.equals(taskHistory.getResult())){
                                    	oaTaskInstance.setName(SystemConstant.TASK_STATUS_NAME_4);
                                    } else {
                                    	oaTaskInstance.setName(taskHistory.getTaskName());                                              //任务名称
                                    }
                                    oaTaskInstance.setAssignee(taskHistory.getUserId());                                                //审批人
                                    oaTaskInstance.setComments(taskHistory.getComment());                                               //审批意见
    								try {
    									oaTaskInstance.setStartTime(DateFormatUtils.format(taskHistory.getBeginTime(),"yyyy-MM-dd HH:mm:ss"));     //任务创建时间
    									oaTaskInstance.setEndTime(DateFormatUtils.format(taskHistory.getEndTime(),"yyyy-MM-dd HH:mm:ss"));         //任务结束时间
    								} catch (Exception e) {
    									LOG.info(e);
    									e.printStackTrace();
    								}
    								if(i==0){
                                    	oaTaskInstance.setCount(processTaskHistoryList.size()+2);                                       //当前节点前置
                                    } else if(i == processTaskHistoryList.size()-1) {
                                    	oaTaskInstance.setCount(1);                                                                     //申请
                                    } else {
                                    	oaTaskInstance.setCount(processTaskHistoryList.size()+1);
                                    }
                                    oaTaskInstance.setNodeId(taskHistory.getNodeId());
                                    oaTaskInstance.setCandidateUsers(taskHistory.getUserId());//当前节点处理人
                                    oaTaskInstanceList.add(oaTaskInstance);
                        		}
                        	}
                    	}
                    	
                    	processInstance.setCurrentStep(processTaskHistoryList.size()+2);//当前节点步数
                    }
                    
                    //将任务处理轨迹装填到流程实例中
                    processInstance.setTasks(oaTaskInstanceList);
                    mapCheck.put(processInstance.getProcessInstanceId(), processInstance.getProcessInstanceId());
                    processInstanceList.add(processInstance);
                }
            }
        }
        return processInstanceList;
    }

    /**
     * 删除流程实例信息
     * @param loginUser 登陆用户
     * @param processInstanceId 流程实例ID
     */
    public boolean cancelProcessInstance(String loginUser, String processInstanceId){
        boolean flag = true;
        PafResult<Boolean> pafResult = pafService.cancelProcessInstance(loginUser,processInstanceId);
        if(pafResult.isSuccess() && pafResult.getResult()){
            try{
                com.jd.oa.process.model.ProcessInstance processInstance = new com.jd.oa.process.model.ProcessInstance();
                processInstance.setProcessInstanceId(processInstanceId);
                processInstance.setStatus(SystemConstant.PROCESS_STATUS_4);
                processInstanceService.cancelByProcessInstanceId(processInstance);
            }catch(Exception e){
                LOG.error(e);
                flag = false;
            }
        }else{
            LOG.error(pafResult.getErrorCode() +" : " + pafResult.getLocalizedMessage());
            flag = false;
        }
        return flag;
    }

    /**
     * 获取流程实例图片
     * @param processInstanceId 流程实例ID
     * @return
     */
    public byte[] processInstanceFlowPictureQuery(String processInstanceId){
        PafResult<byte[]> pafResult = pafService.processInstanceFlowPictureQuery(ComUtils.getLoginNamePin(),processInstanceId);
        byte[] pictureBytes = null;
        if(pafResult.isSuccess()){
            pictureBytes = pafResult.getResult();
        }
        return pictureBytes;
    }

    /**
     * 获取任务的名称
     * @param loginUser 登陆人
     * @param processInstId 流程实例ID
     * @param taskInstanceId 任务实例ID
     * @return 任务创建时间
     */
    public String getTaskName(String loginUser, String processInstId, String taskInstanceId){
        String taskName = "";
        PafResult<ProcessInstanceDomain> pafResult = pafService.getProcessInstanceDetail(loginUser, processInstId);
        if(pafResult.isSuccess()){
            List<Task> tasks = pafResult.getResult().getTasks();
            for(Task task : tasks){
                if(task.getTaskId().equals(taskInstanceId)){
                    taskName = task.getTaskName();
                }
            }       
        }
        return taskName;
    }

	@Override
	public Map<String, Map<String, Object>> getTaskAndKeyTotal(String loginUser, Conditions conditions) {
		
		List<String> pdKeyList = processSearchService.findProcessDefAll();
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String,Object>>();
		for (String processDef : pdKeyList) {
			
		
				Map<String, Object> mp = null;
				//转换conditions为Map
				if (conditions == null){
					conditions = new Conditions();
				}
				mp = ComUtils.pojoToMap(conditions);

			    Map<String,Object> mapResult =  pafService.taskQueryWithTotal(loginUser,processDef, mp);	
			    PafResult<List<TaskInstance>> result =	(PafResult<List<TaskInstance>>)mapResult.get("pafResult");
			    if(result.isSuccess()){
		            List<TaskInstance> taskInstanceList = result.getResult();
		            mp.clear();
		            if( taskInstanceList != null && taskInstanceList.size() > 0){
			            mp.put("taskList", getOaTaskInstance(taskInstanceList));
			            mp.put("total", mapResult.get("total"));
		            }
		            
		           map.put(processDef, mp);
			    }
				
		}
		
		return map;
	}
	
	
	@Override
	public Map<String, Map<String, Object>> getTaskAndKeyTotalByProcessDef(String loginUser, Conditions conditions, String processDef) {
		
		Map<String, Map<String, Object>> map = new HashMap<String, Map<String,Object>>();

			
		
		Map<String, Object> mp = null;
		// 转换conditions为Map
		if (conditions == null) {
			conditions = new Conditions();
		}
		mp = ComUtils.pojoToMap(conditions);

		Map<String, Object> mapResult = pafService.taskQueryWithTotal(
				loginUser, processDef, mp);
		PafResult<List<TaskInstance>> result = (PafResult<List<TaskInstance>>) mapResult	.get("pafResult");
		if (result.isSuccess()) {
			List<TaskInstance> taskInstanceList = result.getResult();
			mp.clear();
			if (taskInstanceList != null && taskInstanceList.size() > 0) {
				mp.put("taskList", getOaTaskInstance(taskInstanceList));
				mp.put("total", mapResult.get("total"));
			}

			map.put(processDef, mp);
		}
				

		
		return map;
	}

	@Override
	public Map<String, Object> getTaskAndAllTotal(String loginUser,	Conditions conditions) {
        LOG.info("OaPafServiceImpl.getTaskAndAllTotal:loginUser:"+loginUser);
        
		// TODO Auto-generated method stub
		Map<String, Object> mp = null;
		//转换conditions为Map
		if (conditions == null){
			conditions = new Conditions();
		}
		mp = ComUtils.pojoToMap(conditions);
	    mp.put("TASK_PARTICIPATOR",loginUser);
	    Map<String,Object> mapResult =  pafService.taskQueryAdvancedWithTotal(loginUser, mp);	
	    PafResult<List<TaskInstance>> result =	(PafResult<List<TaskInstance>>)mapResult.get("pafResult");
	    if(result.isSuccess()){
            List<TaskInstance> taskInstanceList = result.getResult();
            mp.clear();
            if( taskInstanceList != null && taskInstanceList.size() > 0){
	            mp.put("taskList", getOaTaskInstance(taskInstanceList));
	            mp.put("total", mapResult.get("total"));
            }
            
            return mp;
	    }
		
		return null;
	}


	@Override
	public Map<String, Object> getTaskAndAllTotalIncludePafAndBiz(String loginUser, Conditions conditions) {
		Map<String,Object> allTaskMap = new HashMap<String, Object>();
		List<OaTaskInstance> allTaskList = new ArrayList<OaTaskInstance>();
		int total = 0 ;

		//获取Paf任务数据
		Map<String,Object> pafTaskMap = getTaskAndAllTotal(loginUser,conditions);
		if(pafTaskMap != null && pafTaskMap.size() > 0){
			allTaskList.addAll((List<OaTaskInstance>) pafTaskMap.get("taskList"));
			total  += (Integer) pafTaskMap.get("total");
		}
		//获取统一待办任务数据
		Map<String,Object> bizTaskMap = getBizTaskAndAllTotal(loginUser);
		if(bizTaskMap != null && bizTaskMap.size()>0){
			allTaskList.addAll((List<OaTaskInstance>) bizTaskMap.get("taskList"));
			total  += (Integer) bizTaskMap.get("total");
		}

		allTaskMap.put("taskList",allTaskList);
		allTaskMap.put("total",total);
		return allTaskMap;
	}

	@Override
	public Map<String, Object> getBizTaskAndAllTotal(String candidateUser) {
		Map<String,Object> bizTaskMap = new HashMap<String, Object>();
		if(candidateUser == null || candidateUser.equals("") ) return null;
		AgentTodoList agentTodoList = new AgentTodoList();
		agentTodoList.setCandidateUsers(candidateUser);
        agentTodoList.setTaskStatus("0");//task status == 0   为待办任务
		List<AgentTodoList> agentTodoLists = agentTodoListService.find(agentTodoList);
		List<OaTaskInstance> taskList  = getOaTaskInstanceByAgentTotoList(agentTodoLists);
		bizTaskMap.put("taskList",taskList);
		bizTaskMap.put("total",taskList.size());
		return bizTaskMap;
	}

	@Override
	public Map<String, Object> getBizTaskAndAllTotal(String candidateUser, String processDefinitionKey) {
		Map<String,Object> bizTaskMap = new HashMap<String, Object>();
		if(candidateUser == null || candidateUser.equals("") ) return null;
		AgentTodoList agentTodoList = new AgentTodoList();
		agentTodoList.setCandidateUsers(candidateUser);
		agentTodoList.setProcessKey(processDefinitionKey);
		agentTodoList.setTaskStatus("0");//task status == 0   为待办任务
		List<AgentTodoList> agentTodoLists = agentTodoListService.find(agentTodoList);
		List<OaTaskInstance> taskList  = getOaTaskInstanceByAgentTotoList(agentTodoLists);
		bizTaskMap.put("taskList", taskList);
		bizTaskMap.put("total",taskList.size());
		return bizTaskMap;
	}

	private List<OaTaskInstance> getOaTaskInstanceByAgentTotoList(List<AgentTodoList> agentTodoLists){
		List<OaTaskInstance> taskList = new ArrayList<OaTaskInstance>();
		for(AgentTodoList agentTodo:agentTodoLists){
			OaTaskInstance oaTaskInstance = new OaTaskInstance();
			com.jd.oa.process.model.ProcessInstance processInstance = new com.jd.oa.process.model.ProcessInstance();
			processInstance.setProcessInstanceId(agentTodo.getProcessInstanceId());
			
			processInstance = processInstanceService.selectByProcessInstanceId(processInstance);
			if(processInstance == null){
				continue;
			}
			ProcessDefinition processDefinition = processSearchService.findProcessDefByInstance(processInstance.getProcessInstanceId());
			//oaTaskInstance赋值
			oaTaskInstance.setProcessInstanceId(processInstance.getProcessInstanceId());
			oaTaskInstance.setTaskDefinitionKey(agentTodo.getTaskId());
			oaTaskInstance.setId(agentTodo.getTaskId());
			oaTaskInstance.setCandidateUsers(agentTodo.getCandidateUsers());
			oaTaskInstance.setName(agentTodo.getTaskName());
			oaTaskInstance.setProcessDefinitionKey(agentTodo.getProcessKey());
			oaTaskInstance.setProcessDefinitionId(processInstance.getProcessDefinitionId());
			oaTaskInstance.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
			oaTaskInstance.setProcessDefinitionIsOuter(processDefinition.getIsOuter());
			oaTaskInstance.setBuinessInstanceId(processInstance.getBusinessInstanceId());
			oaTaskInstance.setProcessInstanceName(processInstance.getProcessInstanceName());
			oaTaskInstance.setStartTime(DateFormatUtils.format(processInstance.getBeginTime(),"yyyy-MM-dd HH:mm:ss"));
			taskList.add(oaTaskInstance);
		}
		return taskList;
	}

	public List<OaTaskInstance> getProcessCurrentUserTasks(String loginUser, List<String> processInstances) {
		PafResult<List<TaskInstance>> pafResult = pafService.getProcessCurrentUserTasks(loginUser, processInstances);
		
		if (pafResult.isSuccess()) {			
			return getOaTaskInstance(pafResult.getResult());
		} else {
			return null;
		}
	}
	
	@Override
	public OaTaskInstance getTaskDetail(String taskId) {
		return this.getTaskDetail("", taskId);
	}
	
	@Override
	public OaTaskInstance getTaskDetail(String loginUser, String taskId) {
		PafResult<TaskInstance> pafResult = pafService.getTaskDetail(("").equals(loginUser)?ComUtils.getLoginNamePin():loginUser, taskId);
		
		if (pafResult.isSuccess()) {		
			List<TaskInstance> tiList = new ArrayList<TaskInstance>();
			tiList.add(pafResult.getResult());
			return getOaTaskInstance(tiList).get(0);
		} else {
			return null;
		}
	}

	@Override
	public Boolean reassignTask(String loginUser, String taskId,
			String origAssignee, String newAssignee, String assigneeType) {
		PafResult<Boolean> pafResult = pafService.reassignTask(loginUser, taskId, origAssignee, newAssignee, assigneeType);
		if (pafResult.isSuccess())	{
			emailToNewAssignee(loginUser, taskId, newAssignee);
			return pafResult.getResult();
		} else
			return false;
	}
	
	/**
	 * 向新办理人发邮件
	 * @param loginUser
	 * @param taskId
	 * @param newAssignee
	 */
	private void emailToNewAssignee(String loginUser, String taskId, String newAssignee)	{
		OaTaskInstance oti = getTaskDetail(loginUser, taskId);
		
        if (oti != null) {
        	com.jd.oa.process.model.ProcessInstance par = new com.jd.oa.process.model.ProcessInstance();
        	par.setProcessInstanceId(oti.getProcessInstanceId());
        	com.jd.oa.process.model.ProcessInstance pi = processInstanceDao.selectByProcessInstanceId(par);
        	
			try {
				Map<String, Object> resulMapMail = processTaskService.getMailVariablesByProcess(
						oti.getAssignee(), pi.getProcessInstanceName(), oti.getNodeId(),
						oti.getProcessDefinitionId(), pi.getBusinessInstanceId(), "3",
						oti.getComments(), null, "");
				
				String subject = processTaskService.getMailSubject(resulMapMail,"0");
				String content = processTaskService.getMailContent(resulMapMail, oti, pi.getProcessInstanceName(), "0"); 
                mailSendService.sendHtmlMailByAsyn(
	                	new String[]{sysUserService.getByUserName(newAssignee).getEmail()},
	            		subject, content);
                
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
        }
	}
	
}
