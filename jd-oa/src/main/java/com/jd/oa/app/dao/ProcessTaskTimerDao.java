package com.jd.oa.app.dao;

import com.jd.oa.app.model.ProcessTaskTimer;
import com.jd.oa.common.dao.BaseDao;

import java.util.List;

/**
 * 
 * @desc 
 * @author WXJ
 * @date 2014-6-16 下午10:09:07
 *
 */
public interface ProcessTaskTimerDao extends BaseDao<ProcessTaskTimer,String> {
   
}
