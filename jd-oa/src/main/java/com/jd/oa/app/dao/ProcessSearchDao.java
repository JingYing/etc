package com.jd.oa.app.dao;

import java.util.List;
import java.util.Map;

import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessInstance;

/** 
 * @Description: 前台-申请查询Dao
 * @author guoqingfu
 * @date 2013-10-11 上午11:14:32 
 * @version V1.0 
 */
public interface ProcessSearchDao extends BaseDao<ProcessSearch, String>{

	/**
     * @Description: 通过类别ID查询流程定义信息（前台用）
      * @param processTypeId
      * @return List<ProcessDefinition>
      * @author guoqingfu
      * @date 2013-10-11下午12:04:43 
      * @version V1.0
     */
    public List<ProcessSearch> findPrcocessListByTypeId(String processTypeId);
    
    /**
     * @Description: 根据申请人和状态查询流程信息（我的申请）
     * @param starterId
     * @param status 查询多个状态用,分割
     * @return List<ProcessInstance>
	 * @author 许林
	 * @date 2013-10-14上午10:04:43 
	 * @version V1.0
     */
    public List<ProcessInstance> selectProInsByStaIdAndStatus(String starterId,List<String> status);
    
    /**
     * @Description: 根据流程实例ID获取流程的详细信息
     * @param starterId
     * @param status 查询多个状态用,分割
     * @return List<ProcessInstance>
	 * @author 许林
	 * @date 2013-10-14上午10:04:43 
	 * @version V1.0
     */
    public List<ProcessSearch> getProcessInfoByInstanceId(List<String> processInstanceIds);
    
	/**
	 * 根据流程定义ID和最后更新时间查询流程实例
	 * @param processDefinitionId
	 * @param createStartDate
	 * @param createEndDate
	 * @return
	 */
	public List<ProcessSearch> findProcessInstanceByDefId(List<String> processDefinitionId,
			String createStartDate,String createEndDate);
	
	/**
     * @Description: 取消流程实例(状态是草稿)
      * @param processInstanceId
      * @return 
      * @author guoqingfu
      * @date 2013-10-23下午07:42:24 
      * @version V1.0
     */
    public boolean cancelApplyForDraft(String processInstanceKey);

    /**
     * 查询一级流程 -> 所有二级流程  ->所有流程
     * @param processType1Id
     * @return
     */
	public List<ProcessSearch> findByType1(String processType1Id);
}
