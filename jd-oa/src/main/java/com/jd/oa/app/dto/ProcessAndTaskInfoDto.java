/**
 * 许林
 * 2013年10月14号上午11点14分
 */
package com.jd.oa.app.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.jd.oa.app.model.OaTaskInstance;

public class ProcessAndTaskInfoDto implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -5340582764207898361L;

	private String processInstanceId;//流程实例ID
    
    private String processDefinitionId;//流程定义Id
    
    private String processInstanceName;//流程实例名称

	private String followCode; //流水号
    
    private String businessInstanceId;//流程业务Id
    
    private String processInsPrimayKey;//流程实例主键
    
    private String currentStatus;//当前流程状态
    
    private String processDifinitionName;//流程名称
    
    private String processDifinitionKey;//流程定义Key
    
    private String beginTime;
    
    /**
     * 流程PafKey
     */
    private String pafProcessDefinitionId;
    
    private int currentStep;//当前节点
    
    private boolean isFirstNode;//当前节点是否是首节点
    
	List<OaTaskInstance> oaTaskInstanceList;
    
    List<Map<String,String>> listStep;

	public List<Map<String, String>> getListStep() {
		return listStep;
	}

	public String getPafProcessDefinitionId() {
		return pafProcessDefinitionId;
	}

	public void setPafProcessDefinitionId(String pafProcessDefinitionId) {
		this.pafProcessDefinitionId = pafProcessDefinitionId;
	}

	public void setListStep(List<Map<String, String>> listStep) {
		this.listStep = listStep;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public String getProcessInsPrimayKey() {
		return processInsPrimayKey;
	}

	public void setProcessInsPrimayKey(String processInsPrimayKey) {
		this.processInsPrimayKey = processInsPrimayKey;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessDifinitionName() {
		return processDifinitionName;
	}

	public void setProcessDifinitionName(String processDifinitionName) {
		this.processDifinitionName = processDifinitionName;
	}

	public String getProcessDifinitionKey() {
		return processDifinitionKey;
	}

	public void setProcessDifinitionKey(String processDifinitionKey) {
		this.processDifinitionKey = processDifinitionKey;
	}

	public String getProcessInstanceName() {
		return processInstanceName;
	}
	
	public boolean isFirstNode() {
		return isFirstNode;
	}

	public void setFirstNode(boolean isFirstNode) {
		this.isFirstNode = isFirstNode;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}
	
	public void setProcessInstanceName(String processInstanceName) {
		this.processInstanceName = processInstanceName;
	}

	public String getBusinessInstanceId() {
		return businessInstanceId;
	}

	public void setBusinessInstanceId(String businessInstanceId) {
		this.businessInstanceId = businessInstanceId;
	}

	public List<OaTaskInstance> getOaTaskInstanceList() {
		return oaTaskInstanceList;
	}

	public void setOaTaskInstanceList(List<OaTaskInstance> oaTaskInstanceList) {
		this.oaTaskInstanceList = oaTaskInstanceList;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getFollowCode() {
		return followCode;
	}

	public void setFollowCode(String followCode) {
		this.followCode = followCode;
	}
}
