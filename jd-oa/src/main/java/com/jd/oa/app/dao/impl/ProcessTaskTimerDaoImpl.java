package com.jd.oa.app.dao.impl;

import org.springframework.stereotype.Repository;
import com.jd.oa.app.dao.ProcessTaskTimerDao;
import com.jd.oa.app.model.ProcessTaskTimer;
import com.jd.oa.common.dao.MyBatisDaoImpl;

/**
 * 
 * @desc 
 * @author WXJ
 * @date 2014-6-16 下午10:12:55
 *
 */
@Repository("processTaskTimerDao")
public class ProcessTaskTimerDaoImpl extends MyBatisDaoImpl<ProcessTaskTimer, String>
		implements ProcessTaskTimerDao {
	
}
