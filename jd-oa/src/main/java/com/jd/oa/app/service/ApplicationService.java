package com.jd.oa.app.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.process.model.ProcessDefinition;

public interface ApplicationService {
	/**
	 * 获取流程所绑定的电子表单内容
	 * @param request
	 * @param processDefinition
	 * @param form
	 * @param businessObjectId
	 * @param processNodeId
	 * @param processInstanceId
	 * @return
	 * @throws Exception
	 */
	public abstract String getExecuteWorklistBindReportFormContext(HttpServletRequest request,String processDefinitionKey,String businessObjectId,String processNodeId,String processInstanceId,String processInstanceStatus) throws Exception;
	/**
	 * 获取流程所绑定的form
	 * @param processDefinitionId 流程定义ID
	 * @return
	 * @throws Exception
	 */
	public abstract Form getFormByProcessDefinitionId(ProcessDefinition processDefinition) throws Exception;
	/**
	 * 根据FORM获取指定的业务表名称
	 * @param form
	 * @return
	 * @throws Exception
	 */
	public abstract String getBuninessOperationDataTableNameByForm(Form form) throws Exception;
	
	public abstract String getBuninessOperationToolbar();
	/**
	 * 保存主表业务数据
	 * @param boTableName
	 * @param datas
	 * @param businessObjectId
	 * @return
	 */
	public abstract String saveBusinessObject(String boTableName,String datas,String businessObjectId,String formId);
	
	/**
	 * 
	 * @desc 保存主表业务数据
	 * @author WXJ
	 * @date 2014-3-20 上午11:25:21
	 *
	 * @param boTableName
	 * @param datas
	 * @param businessObjectId
	 * @param formId
	 * @param processNodeId
	 * @return
	 */
	public abstract String saveBusinessObject(String boTableName,String datas,String businessObjectId,String formId,String processNodeId);
	
	/**
	 * 保存子表业务数据
	 * @param boTableName
	 * @param datas
	 * @param businessObjectId
	 * @return
	 */
	public abstract String saveBusinessObjectForSub(String boTableName,String datas,String businessObjectId,String formId);
	/**
	 * 
	 * @desc 保存子表业务数据
	 * @author WXJ
	 * @date 2014-3-20 下午03:06:28
	 *
	 * @param boTableName
	 * @param datas
	 * @param businessObjectId
	 * @param formId
	 * @param processNodeId
	 * @return
	 */
	public abstract String saveBusinessObjectForSub(String boTableName,String datas,String businessObjectId,String formId,String processNodeId);
	/**
	 * 获取子表业数据
	 * @param boTableName
	 * @param businessObjectId
	 * @return
	 */
	public List<Map<String,Object>> getBusinessDatas(String boTableName,String businessObjectId);
	
	/**
	 * 删除指定boTableName的业务数据
	 * @param boTableName
	 * @param id
	 * @return
	 */
	public String deleteBindReportData(String boTableName,String id);

	/**
	 * 删除boTableName指定parentId 的数据，主要使用于子表情况
	 * @param boTableName
	 * @param parentId
	 * @return
	 */
	public String deleteBusinessObjectByParentId(String boTableName,String parentId);
	public abstract String getDynamicQueryConditionsHtml(HttpServletRequest request,
			String processDefinitionKey);
	public abstract String getDynamicQueryConditionsHtml(HttpServletRequest request,
			String processDefinitionKey, String itemId);
	/**
	 * @author zhengbing 
	 * @desc 流程申请节点使用,获取整个流程表单
	 * @date 2014年7月30日 上午11:25:08
	 * @return String
	 */
	public String getExecuteWorklistBindReportFormContextForApply(String contextPath,String processDefinitionKey) throws BusinessException ;
}
