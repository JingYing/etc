package com.jd.oa.app.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.app.constant.AppConstant;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessType;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessTypeService;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;

/**
 * @Title: AppController.java
 * @Package com.jd.oa.app.controller
 * @Description: 流程应用首页controller
 * @author zhouhuaqi
 * @date 2013-10-12 上午10:58:32
 * @version V1.0
 */
@Controller
@RequestMapping(value = "/app")
public class AppController {

	private static final Logger logger = Logger.getLogger(AppController.class);

	@Autowired
	private ProcessTypeService processTypeService;
	@Autowired
	private DictDataService dictDataService;
	@Autowired
	private ProcessDefService processDefService;

	@Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserAuthorizationService userAuthorizationService;

	/**
	 * 流程应用主页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(String from,String enter,String processTypeId,String withMenu) throws Exception {
		ModelAndView mv = new ModelAndView();
		mv.addObject("from", (from==null||from.equals(""))?"oa":from);
		mv.addObject("enter",(enter==null||enter.equals(""))?"":enter);
		mv.addObject("withMenu",(withMenu==null||withMenu.equals(""))?"1":withMenu);
		mv.addObject("processTypeId",(processTypeId==null||processTypeId.equals(""))?"":processTypeId);
		mv.setViewName("app/app_index");
		return mv;
	}

	@RequestMapping(value = "/header")
	public ModelAndView header() {
		ModelAndView mav = new ModelAndView("app/app_header");		
		mav.addObject("displayName", ComUtils.getDisplayName());
		mav.addObject("displayTime", getDisplayTime());
		if(sysUserService.isAdministrator(ComUtils.getLoginNamePin()))
			mav.addObject("isAdministrator", "1");
		SysUser user = sysUserService.getByUserName(ComUtils.getLoginNamePin());
		if(user!=null)
			mav.addObject("sex", user.getSex());
		return mav;
	}
	
	/**
	 * 流程地图
	 * @return
	 */
	@RequestMapping(value = "/map")
	public ModelAndView map() {
		ModelAndView mav = new ModelAndView("app/app_map");
		
		// 获取所有的流程类别
		List<ProcessType> processTypes = processTypeService.find(null);
		
		// key为流程类别的一级类别ID，value为该一级流程类别包含的所有子类别的ID列表，Map<processType.id,List<processType.id>>
		Map<String,List<String>> processTypeIdMap= new LinkedHashMap<String, List<String>>();
		// key为流程列别ID，value为流程类别名称
		Map<String,String> processTypeIdNameMap = new  HashMap<String, String>();
		
		// 返回结果，key为一级流程类别名称，value为一级流程类别下的所有的流程定义
		Map<String,List<ProcessDefinition>> result = new LinkedHashMap<String,List<ProcessDefinition>>();
		for(ProcessType processType : processTypes){
			processTypeIdNameMap.put(processType.getId(), processType.getProcessTypeName());
			if(processType.getParentId()==null){
				if(!processTypeIdMap.containsKey(processType.getId())){
					List<String> idList = new ArrayList<String>();
					idList.add(processType.getId());
					processTypeIdMap.put(processType.getId(), idList);
				}
			}
			else{
				if(processTypeIdMap.containsKey(processType.getParentId())){
					List<String> idList = processTypeIdMap.get(processType.getParentId());
					idList.add(processType.getId());
					processTypeIdMap.put(processType.getParentId(), idList);
				}
				else{
					List<String> idList = new ArrayList<String>();
					idList.add(processType.getId());
					processTypeIdMap.put(processType.getParentId(), idList);
				}
			}
		}
		
		String sysUserId = sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin(), SystemConstant.ORGNAIZATION_TYPE_HR);
		// 获取该用户有权申请的流程定义ID
        List<SysBussinessAuthExpression> assignedProcessDefinitions = userAuthorizationService.getResourceByUserId(sysUserId, "2");
        List<String> assignedProcessDefinitionIds = new ArrayList<String>();
        for(SysBussinessAuthExpression sysBussinessAuthExpression: assignedProcessDefinitions){
        	assignedProcessDefinitionIds.add(sysBussinessAuthExpression.getBusinessId());
        }
        
        // 获取所有的已发布的流程
        List<ProcessDefinition> processDefinitions = processDefService.find("findAll", null);
        
        if(assignedProcessDefinitionIds!=null && processDefinitions!=null){
	        for(Map.Entry<String, List<String>> processTypeIdList : processTypeIdMap.entrySet()){
	        	List<ProcessDefinition> assignedProcessDefinitionList = new ArrayList<ProcessDefinition>();
	        	for(ProcessDefinition pf : processDefinitions){
	        		if(assignedProcessDefinitionIds.contains(pf.getId()) && processTypeIdList.getValue().contains(pf.getProcessTypeId())){
	        			assignedProcessDefinitionList.add(pf);
	        		}
	        	}
	        	if(assignedProcessDefinitionList.size()>0)
		        	result.put(processTypeIdNameMap.get(processTypeIdList.getKey()), assignedProcessDefinitionList);
	        }
        }

        //重新排序
        Map<String,List<ProcessDefinition>> result2 = new LinkedHashMap<String,List<ProcessDefinition>>();

        for(ProcessType processType : processTypes){
            String key = processType.getProcessTypeName() ;
            if(result.get(key) != null)
                result2.put(key,result.get(key));
        }


		mav.addObject("processTypeMap", result2);
		return mav;
	}
	

	@RequestMapping(value = "/left")
	public ModelAndView left(HttpServletRequest request) {
//		return new ModelAndView("app/app_left")
//			.addObject("pcList", dictDataService.findDictDataList(AppConstant.PROCESSCLASSIFICATION));
		
		// key为流程类别ID，value为流程类别名称
		Map<String,String> processTypeIdNameMap = new  HashMap<String, String>();
		// key为一级流程类别ID，value为二级流程类别列表
		Map<String,List<ProcessType>> processTypeMap = new LinkedHashMap<String,List<ProcessType>>();
		// key为一级流程类别名称，value为二级流程类别列表
		Map<String,List<ProcessType>> result = new LinkedHashMap<String,List<ProcessType>>();
		
		// 获得所有的流程类别
		List<ProcessType> processTypes = processTypeService.find(null);
		for(ProcessType processType: processTypes){
			processTypeIdNameMap.put(processType.getId(), processType.getProcessTypeName());
			if(processType.getParentId() == null || processType.getParentId().equals("")){
				if(!processTypeMap.containsKey(processType.getId())){
					processTypeMap.put(processType.getId(), new ArrayList<ProcessType>());
				}
			}
			else{
				if(!processTypeMap.containsKey(processType.getParentId())){
					List<ProcessType> children = new ArrayList<ProcessType>();
					children.add(processType);
					processTypeMap.put(processType.getParentId(), children);
				}
				else{
					processTypeMap.get(processType.getParentId()).add(processType);
				}
			}
		}
		
		for(Entry<String,List<ProcessType>> entry: processTypeMap.entrySet()){
			result.put(processTypeIdNameMap.get(entry.getKey()), entry.getValue());
		}
		ModelAndView mav = new ModelAndView("app/app_left");
		mav.addObject("processTypeMap", result);
		return mav;
	}

	@RequestMapping(value = "/info")
	public ModelAndView info(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("app/app_info");
		mav.addObject("rejectApplyNum", 0);
		mav.addObject("addSignApplyNum", 0);
		mav.addObject("proxyApplyNum", 0);
		return mav;
	}

	private String getDisplayTime() {
		String displayTime = "";
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if (hour >= 6 && hour < 8) {
			displayTime = "早上";
		} else if (hour >= 8 && hour < 11) {
			displayTime = "上午";
		} else if (hour >= 11 && hour < 13) {
			displayTime = "中午";
		} else if (hour >= 13 && hour < 18) {
			displayTime = "下午";
		} else {
			displayTime = "晚上";
		}
		return displayTime;
	}

}
