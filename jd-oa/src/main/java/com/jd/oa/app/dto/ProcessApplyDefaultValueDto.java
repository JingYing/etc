package com.jd.oa.app.dto;

/**
 * @author zhengbing
 * @desc  异步获取申请节点主表默认数据
 * @date 2014年7月30日 下午6:01:14
 */
public class ProcessApplyDefaultValueDto {
	
	private String defaultValue;
	private String inputType;
	private String fieldName;
	
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getInputType() {
		return inputType;
	}
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	

}
