package com.jd.oa.app.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.app.dto.ProcessTaskDto;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.mail.MailSendService;
import com.jd.oa.common.sms.SmsService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessSupervise;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.process.service.ProcessSuperviseService;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;

@Controller
@RequestMapping(value = "/app")
public class ProcessSuperviseController {
	private static final Logger logger = Logger.getLogger(ProcessSuperviseController.class);
	
	@Autowired
	private ProcessSuperviseService processSuperviseService;
	
    @Autowired
	private SmsService smsService;
    
    @Autowired
	private MailSendService mailSendService;
    
    @Autowired
    private ProcessSearchService processSearchService;
    
    @Autowired
    private ProcessNodeService processNodeService;
    
    @Autowired
    private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
    
    @Autowired
    private SysAuthExpressionService sysAuthExpressionService;
    
    @Autowired
    private UserAuthorizationService userAuthorizationService;
    @Autowired
    private ProcessTaskService processTaskService;
    
    @Autowired
    private SysUserService sysUserService;
    
    @Autowired
    private OaPafService oaPafService;
    
    @Autowired
    private ProcessInstanceService processInstanceService;
    
 // 创建一个可重用固定线程数的线程池
// 	ExecutorService threadPool = Executors.newFixedThreadPool(4);
    
	/**
	 * 我要督办页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/processSupervise_index", method=RequestMethod.GET)
	public ModelAndView processSuperviseIndex() throws Exception{
		ModelAndView mv = new ModelAndView();
		mv.setViewName("app/app_processSupervise_index");
		return mv;
	}
	/**
	 * 我的督办任务列表
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/processSupervise_list",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processSuperviseList(HttpServletRequest request,int startDay,int endDay){
		Map<String, ProcessTaskDto> map=processTaskService.getSuperviseProcessTaskList(request,startDay, endDay,null);
		return map;
	}
	
	/**
	 * 督办审批列表刷新
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/processSupervise_reload",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processSuperviseReload(HttpServletRequest request,int startDay,int endDay,String pafKey){
		Map<String, ProcessTaskDto> map=new HashMap<String, ProcessTaskDto>();
		if(pafKey!=null&&!pafKey.equals("")){
			map=processTaskService.getSuperviseProcessTaskList(request,startDay, endDay,pafKey);
		}
		return map;
	}
	
	//添加督办记录
	@RequestMapping(value = "/processSupervise_add", method=RequestMethod.GET)
	@ResponseBody
	public String processSuperviseAdd(ProcessSupervise processSupervise){
		
		
    	String currentLoginUserId = sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin() , "1");
		SysUser currentUser = sysUserService.get(currentLoginUserId) ;		
		//当前登陆人用户名 RealName
		String currentLoginName = "" ;
		if(currentUser != null )
			currentLoginName = currentUser.getRealName() ;
		
		int count = 0;
		
		//查询该节点的所有办理人
		List<OaTaskInstance> taskList = null ;
		//根据流程实例id获取任务列表
		if( processSupervise.getProcessInstanceId() != null )
			taskList = oaPafService.getActiveTaskByInstance( processSupervise.getProcessInstanceId() );
		

		if( taskList != null){
			for (OaTaskInstance task : taskList) {

				//获取流程实例以得到申请人、主题
				String pid = task.getProcessInstanceId() ;
				Map<String, String> map = new HashMap<String, String>() ;
				map.put("processInstanceId", pid ) ;
				List<ProcessInstance> list = processInstanceService.find("selectByProcessInstanceId", map ) ;
				
				
				//获取所有督办人
				String temp = task.getCandidateUsers();
				List<String> erpList = null;
				if(temp != null && ! "".equals(erpList))
					erpList = Arrays.asList( temp.split(","));
				if(erpList !=null && erpList.size() > 0){

                    List<String > mails = new ArrayList<String>();
					for (String erpName : erpList) {
						//督办人
						SysUser user = sysUserService.getByUserName(erpName);
						ProcessSupervise ps = new ProcessSupervise();
						if( user !=null  )
							
							//新增督办记录
							
							ps.setProcessInstanceId(processSupervise.getProcessInstanceId());
							ps.setNodeId(task.getNodeId());
							ps.setTaskId(task.getId());
							ps.setSuperviseUserId(currentLoginUserId);
							ps.setYn(0);
							
							String psId = processSuperviseService.insert(ps);

							if(psId != null){
								count = count + 1;
								if( list != null && list.size() > 0){
									String uid = list.get(0).getStarterId() ;
									SysUser u = sysUserService.get(uid) ;
									if(u != null ){
										//获得要申请人的RealName
										String	uName = u.getRealName() ;
											
										if(user.getEmail() != null && ! "".equals(user.getEmail()))
											//mailSendService.sendSimpleMailByAsyn( new String[]{user.getEmail() } , "【京东企业门户-流程中心】", user.getRealName() + "，有一个申请需要您审批，请尽快处理。申请单主题："+list.get(0).getProcessInstanceName()+"，申请人："+ uName+"。 " + currentLoginName);
                                            mails.add(user.getEmail());

										if(user.getMobile() != null && ! "".equals(user.getMobile()))
											smsService.sentMessageByAsyn( user.getMobile() , "【京东企业门户-流程中心 】" + user.getRealName() + "，有一个申请需要您审批，请尽快处理。申请单主题："+list.get(0).getProcessInstanceName()+"，申请人："+ uName+"。 " + currentLoginName);
									}
								}
								
								
							}
						}
                    if( list != null && list.size() > 0)
                        processTaskService.sendMailToSomeone(ComUtils.getLoginNamePin(), list.get(0).getProcessInstanceId(), list.get(0).getProcessInstanceName(), list.get(0).getBusinessInstanceId(),"8",mails,null);

				}
			}
		}
		
	
		return  Integer.valueOf(count).toString();
	}
	
	//根据流程实例id和nodeid获取办理人
		@RequestMapping(value = "/getUsersByNodeId", method=RequestMethod.GET)
		@ResponseBody
		public List<SysUser> getUsersByNodeId(String  processInstanceId, String nodeId){
			
			//渠道流程实例对应的流程定义id
			ProcessDefinition processDefinition = processSearchService.findProcessDefByInstance(processInstanceId);		
			if(processDefinition== null) return null;
			
			//根据流程定义id和节点Id获取节点，取的其唯一的主键
			Map map = new HashMap<String, String>();
    		map.put("nodeId", nodeId);
    		map.put("processDefinitionId", processDefinition.getId());
    		List<ProcessNode> list = processNodeService.find("findByMap",map);
    		if(list != null && list.size() > 0){
    			
    		    			
    			
    			//取到节点的表达式列表
    			List<SysAuthExpression> list2 = sysAuthExpressionService.findExpressionByBusinessIdAndBusinessType( SysBussinessAuthExpression.BUSSINESS_TYPE_NODE, list.get(0).getId());

    			

    	    	//获取所有用户
    			List<SysUser> userList = userAuthorizationService.getUserListBySysAuthExpressionList(list2);
    			
    			return userList;
//    			return JsonUtils.toJsonByGoogle(userList);
    			
    		}
			
			return null;
			
		}
	
	

    private void sendMailAndSmsByMap(Map<OaTaskInstance, List<SysUser>> map){
    	
    	for(OaTaskInstance task : map.keySet())   
    	{   
    		//以整个数组发送，加快速度
    		List<String>emailList = new ArrayList<String>(); 
    		List<String> phoneList = new ArrayList<String>();
    		List<SysUser> userList = map.get(task);
    		
    		if(userList != null && userList.size() > 0){
    			for(SysUser user :userList){
    				String email  = user.getEmail();
    				String phone = user.getMobile();
    				if(email != null && ! "".equals(email))
    					emailList.add(email);
    				if(phone != null && ! "".equals(phone))
    					phoneList.add(phone);
     			}
    		}
    		
    		if(emailList!=null && emailList.size() > 0)
    			mailSendService.sendSimpleMailByAsyn( emailList.toArray(new String[emailList.size()] ), "【京东OA】", "（审批人），请立刻办理该申请，申请单：主题XXXX，申请人XXX。XXX");
    		if(phoneList != null && phoneList.size() > 0)
    			smsService.massMessageByAsyn( phoneList , "（审批人），请立刻办理该申请，申请单：主题XXXX，申请人XXX。XXX");
    		//使用多线程发送
//    		MailAndSmsThread t = new MailAndSmsThread(taskName, emailList, phoneList);
//    		threadPool.execute(t);
    	} 
     }



    //邮件和短信线程
//	class MailAndSmsThread extends Thread {
//		
//		String taskName;
//		List<String>emailList ; 
//		List<String> phoneList ;
//		
//		public MailAndSmsThread( String taskName , List<String>emailList , List<String> phoneList ){
//			this.taskName = taskName;
//			this.emailList = emailList;
//			this.phoneList = phoneList;
//		}
//		
//	    @Override
//	    public void run() {
//	    	if(emailList!=null && emailList.size() > 0)
//				mailSendService.sendSimpleMail( emailList.toArray(new String[emailList.size()] ), "【京东OA 】", "【"+taskName+"】申请需要您审批：http://oa.jd.com/app");
//			if(phoneList != null && phoneList.size() > 0)
//				smsService.massMessage( phoneList , "【京东OA】【"+taskName+"】申请需要您审批 http://oa.jd.com/app");
//	    }
//	}

}

