package com.jd.oa.app.model;

import java.util.Date;

import com.jd.oa.common.model.IdModel;

/** 
 * @Title: ProcessSearch.java 
 * @Package com.jd.oa.app.model 
 * @Description: 前台-申请查询Model 
 * @author guoqingfu
 * @date 2013-10-12 上午10:35:56 
 * @version V1.0 
 */
public class ProcessSearch extends IdModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8119585096938875257L;
	//流程类别ID
	private String processTypeId;
	//流程类型名称
	private String processTypeName;
	//paf返回的流程定义ID
	private String pafProcessDefinitionId;
	//流程定义名称
	private String processDefinitionName;
	//流程定义ID 主键
	private String processDefinitionId;
	
	//逻辑删除 0:启用 1:删除
	private int yn;
	
	//paf返回的流程实例ID
	private String processInstanceId;
	//流程实例名称(主题)
	private String processInstanceName;
	//流程发起人ID
	private String starterId;
	//流程发起人ERP
	private String starterErp;
	//流程发起时间
	private Date beginTime;
	private Date endTime;
	//流程实例状态1:Pending 2:Rejected 3:Archived 4:Draft
	private String status;
	//业务实例ID
	private String businessInstanceId;
	//优先级（特急申请、紧急申请、标准申请），对应数据字典编码
	private String priority;
	
	//当前任务节点信息
	OaTaskInstance currentTaskNoteInfo;
	//当前节点名
	private String currentNoteName;
	
	//申请人姓名
	private String realName;
	//申请人部门
	private String organizationName;
	
	//最后任务处理时间
	private Date lastTaskTime;
	
	//加签人，即谁把流程给我转发过来的
	private String bySignerUserId;
	
	  /**
     * 是否代理任务
     */
    private String isProxy;
    
	private String assignee;//原办理人
	
	//流程完成标示
	private String isComplete;
	
	private String mark;
	 /**
     * 当前节点是否首节点 true:是 false：否
     */
    private boolean isFirstNode;

	/**
	 * 流水号
	 */
	private String followCode;
 
    public boolean getIsFirstNode() {
		return isFirstNode;
	}
	public void setIsFirstNode(boolean isFirstNode) {
		this.isFirstNode = isFirstNode;
	}
	
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public String getCurrentNoteName() {
		return currentNoteName;
	}
	
	public String getIsProxy() {
		return isProxy;
	}
	public void setIsProxy(String isProxy) {
		this.isProxy = isProxy;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public void setCurrentNoteName(String currentNoteName) {
		this.currentNoteName = currentNoteName;
	}

	public String getIsComplete() {
		return isComplete;
	}

	public void setIsComplete(String isComplete) {
		this.isComplete = isComplete;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public OaTaskInstance getCurrentTaskNoteInfo() {
		return currentTaskNoteInfo;
	}

	public void setCurrentTaskNoteInfo(OaTaskInstance currentTaskNoteInfo) {
		this.currentTaskNoteInfo = currentTaskNoteInfo;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcessInstanceName() {
		return processInstanceName;
	}

	public void setProcessInstanceName(String processInstanceName) {
		this.processInstanceName = processInstanceName;
	}

	public String getStarterId() {
		return starterId;
	}

	public void setStarterId(String starterId) {
		this.starterId = starterId;
	}

	public String getStarterErp() {
		return starterErp;
	}

	public void setStarterErp(String starterErp) {
		this.starterErp = starterErp;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBusinessInstanceId() {
		return businessInstanceId;
	}

	public void setBusinessInstanceId(String businessInstanceId) {
		this.businessInstanceId = businessInstanceId;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getProcessTypeId() {
		return processTypeId;
	}

	public void setProcessTypeId(String processTypeId) {
		this.processTypeId = processTypeId;
	}

	public String getProcessTypeName() {
		return processTypeName;
	}

	public void setProcessTypeName(String processTypeName) {
		this.processTypeName = processTypeName;
	}

	public String getPafProcessDefinitionId() {
		return pafProcessDefinitionId;
	}

	public void setPafProcessDefinitionId(String pafProcessDefinitionId) {
		this.pafProcessDefinitionId = pafProcessDefinitionId;
	}

	public String getProcessDefinitionName() {
		return processDefinitionName;
	}

	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}

	public int getYn() {
		return yn;
	}

	public void setYn(int yn) {
		this.yn = yn;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public Date getLastTaskTime() {
		return lastTaskTime;
	}

	public void setLastTaskTime(Date lastTaskTime) {
		this.lastTaskTime = lastTaskTime;
	}

	public String getBySignerUserId() {
		return bySignerUserId;
	}

	public void setBySignerUserId(String bySignerUserId) {
		this.bySignerUserId = bySignerUserId;
	}

	public String getFollowCode() {
		return followCode;
	}

	public void setFollowCode(String followCode) {
		this.followCode = followCode;
	}
}
