package com.jd.oa.app.controller;

import com.jd.oa.app.dto.ProcessAndTaskInfoDto;
import com.jd.oa.app.dto.ProcessTaskDto;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.service.ApplicationService;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.PlatformProperties;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.form.execute.plugins.imp2exp.excel.ImportExcel;
import com.jd.oa.form.model.FormAttachment;
import com.jd.oa.form.service.FormAttachmentService;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.system.service.SysUserService;

import net.sf.json.JSONArray;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/app")
public class ProcessApplyController {
    private static final Logger logger = Logger.getLogger(ProcessApplyController.class);

    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private ProcessSearchService processSearchService;
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private ProcessDefService processDefService;
    @Autowired
    private OaPafService oaPafService;
    @Autowired
    private FormAttachmentService formAttachmentService;

    /**
     * 我的申请页面
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/processApply_index", method = RequestMethod.GET)
    public ModelAndView myTaskApply() throws Exception {
        ModelAndView mv = new ModelAndView();

        try {
            //获得当前登录用户的申请流程详细信息
            String loginId = ComUtils.getLoginNamePin();
            List<ProcessAndTaskInfoDto> listApplyPro = this.processSearchService.getMyApplyProTaskInfo(loginId);
            //流程任务执行情况
            Map<String, String> mapStep = null;
            List<OaTaskInstance> oaTaskInstance = null;
            List<Map<String, String>> listStep = null;
            if (listApplyPro != null && listApplyPro.size() > 0) {
                for (int i = 0; i < listApplyPro.size(); i++) {
                    listStep = new ArrayList<Map<String, String>>();
                    ProcessAndTaskInfoDto dto = listApplyPro.get(i);
                    oaTaskInstance = dto.getOaTaskInstanceList();
                    
                    OaTaskInstance task = null;
                    if(SystemConstant.PROCESS_STATUS_0.equals(dto.getCurrentStatus())) {
                    	//提交节点
                       	mapStep = new HashMap<String, String>(2);
                        mapStep.put("StepNum", Integer.toString(1));
                        mapStep.put("StepText", "提交申请");
                        mapStep.put("StepMark", "草稿");
                		mapStep.put("StepStatus", "1");
                		listStep.add(mapStep);
                    }
                    
                    boolean endTask = false;
                    
                    if (oaTaskInstance != null && oaTaskInstance.size() > 0) {
                    	if(dto.isFirstNode()){
                        	//申请节点
                           	mapStep = new HashMap<String, String>(2);
                           	task = oaTaskInstance.get(0);
                            mapStep.put("StepNum", Integer.toString(1));
                            mapStep.put("StepText", task.getName());
                            if(SystemConstant.PROCESS_STATUS_2.equals(dto.getCurrentStatus())){
                        		mapStep.put("StepMark", "拒绝");
                        		mapStep.put("StepStatus", "1");
                        	} else if(SystemConstant.PROCESS_STATUS_3.equals(dto.getCurrentStatus())) {
                        		mapStep.put("StepMark", "驳回");
                        		mapStep.put("StepStatus", "1");
                        	} else {
                        		mapStep.put("StepMark", "");
                        		mapStep.put("StepStatus", "");
                        	}
                            listStep.add(mapStep);
                        } else {
                            //正常流转的流程 申请节点----当前节点前置节点-当前节点----完成节点
                            for(int j = 0; j < oaTaskInstance.size();j++ ){
                            	task = oaTaskInstance.get(j);
                            	mapStep = new HashMap<String, String>(2);
                                mapStep.put("StepNum", Integer.toString(task.getCount()));
                                mapStep.put("StepText", task.getName());
                                if(!SystemConstant.PROCESS_STATUS_2.equals(dto.getCurrentStatus())
                                		&& j==oaTaskInstance.size()-1){
                                	mapStep.put("StepMark", "等待处理");
                            		mapStep.put("StepStatus", "0");
                                } else if(SystemConstant.PROCESS_STATUS_2.equals(dto.getCurrentStatus())
                                		&& j==oaTaskInstance.size()-2){
                            		mapStep.put("StepMark", "拒绝");
                            		mapStep.put("StepStatus", "1");
                                } else {
                                	mapStep.put("StepMark", "");
                                    mapStep.put("StepStatus", "");
                                }
                                
                                listStep.add(mapStep);
                            }
                        }
                    	//当前节点的后继节点
                    	task = oaTaskInstance.get(oaTaskInstance.size()-1);
                        if(task.getPafHumanProcessNextTask()!=null && !"".equals(task.getPafHumanProcessNextTask())){
                        	mapStep = new HashMap<String, String>(2);
                            mapStep.put("StepNum", Integer.toString(dto.getCurrentStep()+1));
                            String taskName = task.getPafHumanProcessNextTask();
                            mapStep.put("StepMark", "");
                            mapStep.put("StepStatus", "");
                            listStep.add(mapStep);
                            if("End".equals(taskName)){
                            	endTask = true;
                            	taskName = "完成";
                            }
                            mapStep.put("StepText", taskName);
                        }
                    }
                    
                    //增加一个完成节点，补全
                    if(!endTask){
                    	 mapStep = new HashMap<String, String>(2);
                         mapStep.put("StepNum", Integer.toString(100));
                         mapStep.put("StepText", "完成");
                         mapStep.put("StepMark", "");
                         mapStep.put("StepStatus", "");
                         listStep.add(mapStep);
                    }
                    
                    dto.setListStep(listStep);
                }
				String listProcessTask = JSONArray.fromObject(listApplyPro).toString();
				String noEnterlistProcessTask = listProcessTask.replace("\\n","");//去除回车
                mv.addObject("listProcessTask",noEnterlistProcessTask );
            }
        } catch (Exception ex) {
            throw new BusinessException("我的申请页面加载失败！", ex);
        }
        mv.setViewName("app/app_processApply_index");
        return mv;
    }

    /**
     * 返回 流程图跟踪
     * @param instanceId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/processInstanceFlowPictureQuery", method = RequestMethod.GET)
    public void processInstanceFlowPictureQuery(String instanceId,HttpServletResponse response) throws Exception {
        byte[] data = oaPafService.processInstanceFlowPictureQuery(instanceId);
        response.setContentType("image/png");
        OutputStream stream = response.getOutputStream();
        stream.write(data);
        stream.flush();
        stream.close();
    }

    /**
     * 返回 流程图跟踪
     * @param instanceId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/processApply_detail", method = RequestMethod.GET)
    public ModelAndView processApplyDetail(String instanceId){
        byte[] data = oaPafService.processInstanceFlowPictureQuery(instanceId);
        String img = base64Encode(data);
        img = "data:image/x-icon;base64," + img;
        ModelAndView mv = new ModelAndView();
        mv.addObject("instanceId", instanceId);

        mv.addObject("img", img);
        mv.setViewName("app/app_processApply_view");
        return mv;
    }
    
    /**
     * 
     * @desc 获得任务办理人
     * @author WXJ
     * @date 2013-12-25 下午06:31:24
     *
     * @param request
     * @param taskId
     * @return
     */
    @RequestMapping(value="/processApply_getCandidateUsers",method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getCandidateUsers(HttpServletRequest request,String taskId){
    	Map<String, String> mp = new HashMap<String, String>();  
    	mp.put("candidateUsers", oaPafService.getUsers(taskId));
    	return mp;
	}
    
    /**
     * Base64编码.
     */

    public static String base64Encode(byte[] input) {

        return new String(Base64.encodeBase64(input));

    }
    /**
     * 文件列表页面
     * @param id
     * @return
     */
    @RequestMapping(value = "/process_FlexUpFileList",method=RequestMethod.GET)
    public ModelAndView getFileList(String id, String isRead){
    	 ModelAndView mv = new ModelAndView();
		 mv.setViewName("app/process_FlexUpFileList");
		 mv.addObject("id",id);
		 mv.addObject("isRead",isRead);
		 return mv;
    }
     /**
	  * 获取文件上传页面
	  * flag 上传功能的标识 imp:导入
	  * @return
	  */
	 @RequestMapping(value = "/process_FlexUpFile",method=RequestMethod.GET)
	 public ModelAndView getFlexUpFile(String tableName, String fieldName ,@CookieValue("erp1.jd.com") String cookie,String flag){
		 ModelAndView mv = new ModelAndView();
		 mv.setViewName("app/process_FlexUpFile");
		 mv.addObject("tableName",tableName);
		 mv.addObject("fieldName", fieldName);
		 mv.addObject("token",cookie);
		 mv.addObject("flag",flag == null ? "" : flag);
		 return mv;
	 }

	/**
	 * 文件上传
	 * @param file
	 * @param flag 上传类型标识 imp:流程定义导入
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 @RequestMapping(value = "/process_uploadFile",method=RequestMethod.POST)
	 @ResponseBody
	 public String flexFileupload(@RequestParam("Filedata") MultipartFile file,String tableName,String flag,HttpServletRequest request) throws Exception {
		 	String fileName = "";
		 	String uploadSucess = "";
            if(flag.equals("importExcel")){//子表数据导入
                 System.out.println("impExcel");
                String path = PlatformProperties.getProperty("jd-oa.jss.temp.directory");
                try{
                    File folder = new File(path);
                    if(!folder.exists()){
                        folder.mkdir();
                    }
                    File outFile = new File(folder.getAbsolutePath() + FileUtil.getSep() + file.getOriginalFilename());
                    if(outFile.exists()){
                        outFile.delete();
                    }
                    DataInputStream  myIn = new DataInputStream(file.getInputStream());
                    FileOutputStream fos = new FileOutputStream(outFile);
                    int len = 0;
                    byte[] b = new byte[1024 * 8];
                    while ((len = myIn.read(b)) != -1) {
                        fos.write(b, 0, len);
                    }
                    fos.flush();
                    fos.close();
                    myIn.close();

                }catch(Exception e){
                    e.printStackTrace();
                }
                uploadSucess = file.getOriginalFilename();
                ImportExcel.importExcelToFormSheet(file.getOriginalFilename(),path,tableName);
            } else 	if(flag.equals("imp")){//流程导入
		 		try{
		 			String path = PlatformProperties.getProperty("jd-oa.jss.temp.directory");
					File folder = new File(path);
					if(!folder.exists()){
						folder.mkdir();
					}
					File outFile = new File(folder.getAbsolutePath() + FileUtil.getSep() + file.getOriginalFilename());
					if(outFile.exists()){
						outFile.delete();
					}
					DataInputStream  myIn = new DataInputStream(file.getInputStream());
					FileOutputStream fos = new FileOutputStream(outFile);
					int len = 0;
					byte[] b = new byte[1024 * 8];
					while ((len = myIn.read(b)) != -1) {
						fos.write(b, 0, len);
					}
					fos.flush();
					fos.close();
					myIn.close();
					
				}catch(Exception e){
					e.printStackTrace();
				}
		 		uploadSucess = file.getOriginalFilename();
		 	} else {
		 		if(file != null){
			 		fileName = file.getOriginalFilename();
			 		uploadSucess = formAttachmentService.uploadFileToJssService(file);
			 	}
		 	}
		 	
		 	return uploadSucess;
	   }
	 
	 @RequestMapping(value = "/process_uploadFile_updatefileAttachment",method=RequestMethod.POST)
	 @ResponseBody
	 public String updateAttachment(String businessObjectId,String fieldName,String attachmentId){
		 StringUtils util = new StringUtils(attachmentId);
			List<String> utils = util.split("@@@@");
			for(int i = 0 ; i < utils.size() ; i++){
				String value = utils.get(i);
				FormAttachment formAttachment = formAttachmentService.get(value);
				if(formAttachment != null){
					 formAttachment.setBusinessId(businessObjectId);
					 formAttachmentService.update(formAttachment);
				}
			}
		 return "";
	 }

	/**
	 *  删除指定的文件
	 * @param attachmentId
	 * @return
	 */
	 @RequestMapping(value="/process_deleteFile",method=RequestMethod.GET)
	 @ResponseBody
	 public String deletefile(String attachmentId){
		 return String.valueOf(formAttachmentService.deleteFile(attachmentId));
	 }
	 /**
		 * 下载指定的文件
		 */
	@RequestMapping(value="/process_downloadfile",method=RequestMethod.GET)
	public void downloadfile(HttpServletResponse response,HttpServletRequest request,String id){
		formAttachmentService.downloadfile(request, response, id);
	}
	/**
	 * 导入导出公共页面
	 */
	@RequestMapping(value="/process_exportImportExcel",method=RequestMethod.GET)
	public ModelAndView exportAndImport(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("app/app_process_exportImportExcel");
		return mv;
	}
	 /**
	 * 下载指定的文件
	 */
	@RequestMapping(value="/process_exportExcel_formSheetModel",method=RequestMethod.GET)
	public void downloadFormSheetModelfile(HttpServletResponse response,HttpServletRequest request,String tableName){
		formAttachmentService.downloadExcelImportModel(request,response, tableName);
	}
	/**
	 * 获取附件列表
	 * @param businessId
	 * @param isRead
	 * @return
	 */
	@RequestMapping(value = "/process_getAttachmentList",method=RequestMethod.GET)
	@ResponseBody
	public Object getAttachmentList(String businessId, String isRead){			
			StringBuffer html = new StringBuffer();
			html.append("<table width='100%' id='attachment_flexUpload'><tr valign='top'><td width=100%>");
			html.append("<table width='100%'>");
			
			StringBuffer upfilevalue = new StringBuffer();
			
			FormAttachment formAttachment = new FormAttachment();
			formAttachment.setBusinessId(businessId);
			List<FormAttachment> faList = formAttachmentService.find(formAttachment);
			
			if (faList!=null && faList.size()>0) {				
				for(FormAttachment fa : faList){
					upfilevalue.append(fa.getBusinessId());
					upfilevalue.append("@@@@");
					
					String fileIcon = FileUtil.getFileIcon(fa.getAttachmentName());
					html.append("<tr valign='top'>");
					//文件下载
					html.append("<td align='left' width=70%>");
					html.append("<a href=\"javascript:void(0);\" onClick=\"parent.downloadfile('").append(fa.getId()).append("');return false;\"><img src='/static/fileupload/image/file/")
						.append(fileIcon).append("' border=0 />").append(fa.getAttachmentName());
					html.append("</a>");
					html.append("</td>");
					if (!"true".equals(isRead)) {
						//文件删除
						html.append("<td align='left' width=30% nowrap>");
						html.append("<a href=\"javascript:void(0);\" onClick=\"parent.deletefile('").append(fa.getId()).append("','").append(fa.getAttachmentName()).append("','');return false;\">删除");
						html.append("</a>");
						html.append("</td>");						
					}
					html.append("</tr>");
				}				
			}
			html.append("</table>");
			html.append("</td></tr>");
			html.append("</table>");
						
			Map<String, String> mp = new HashMap<String, String>();  
	    	mp.put("html", html.toString());
	    	String upfile = upfilevalue.toString();
	    	upfile = upfile.substring(0,upfile.indexOf("@@@@"));
	    	mp.put("upfile", upfile);
			return mp;
		 
	 }
	
}
class FileUploadReturnArgModel{
	private String attachmentId;
	private String fileName;
	public String getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}