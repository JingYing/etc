package com.jd.oa.form.design.style.impl;

import java.util.HashMap;
import java.util.Map;

import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.util.HtmlModelFactory;
import com.jd.oa.form.util.RepleaseKey;

/**
 * 通过表单日数据项的定义,结合表单指定模板生成表单项
 * @author liub
 *
 */
public class FormStyle_1_Impl extends FormStyleAbst{
	
	public FormStyle_1_Impl() {
		
	}
	@Override
	public String getWeb() {
		// TODO Auto-generated method stub
		Map<String,String> paramMapTags = new HashMap<String,String>();
		paramMapTags.put("PART1", getPart());
		paramMapTags.put("SUBSHEET", getSubSheetPart());//子表
		paramMapTags.put("FORMTITLE", getFormTemplateName() == null ? getForm().getFormName() : getFormTemplateName());
		String formTemplatePath = getHttpServletRequest().getSession().getServletContext().getRealPath("/");
		return RepleaseKey.replace(HtmlModelFactory.getModel(formTemplatePath,getHtmlName()), paramMapTags, "[@", "]");
	}
	private String getPart() {
		StringBuffer str = new StringBuffer();
		for(int i = 0 ; i < getFormItem().size() ; i++){
			FormItem item = getFormItem().get(i);
			if(item != null){
//				if( i % 2 == 0){
//					str.append(" <tr class = 'tr1'>\n");
//				}
////				str.append("<td height='25' align=right nowrap=\"nowrap\" width='15%' valign=\"middle\">")
//				str.append("<td height='25' align=right  width='15%' valign=\"middle\">")
//					.append(item.getFieldChineseName()).append(" : ")
//					.append("</td>")
////					.append("<td height='25' align=left nowrap=\"nowrap\" width='35%' valign=\"middle\">&nbsp;")
//					.append("<td height='25' align=left width='35%' valign=\"middle\">&nbsp;")
//					.append(super.getFormItemTags(item))
//					.append("</td>\n");
//				if( i % 2 == 1){
//					str.append("</tr>\n");
//				}
				
				str.append("<div style='width:49%;float:left;line-height:30px;height:40px;display:inline-table;' id='").append(item.getFieldName()).append("_div'>").append(getItemLabelAndFieldConent(item)).append("</div>");
			}
		}
		return str.toString();
	}
	private String getItemLabelAndFieldConent(FormItem item){
		StringBuffer str = new StringBuffer();
		str.append("<div style='width:30%;float:left;text-align:right'>").append(item.getFieldChineseName()).append("&nbsp;</div>");
		str.append("<div style='width:70%;float:right;text-align:left;'>").append(super.getFormItemTags(item)).append("</div>");
		return str.toString();
	}
	/**
	 * 子表
	 * @return
	 */
	private String getSubSheetPart(){
		StringBuffer str = new StringBuffer();
		if(getSubSheet() != null && getSubSheet().size() > 0){
			for(int i = 0 ; i < getSubSheet().size() ; i++){
				str.append(getSubSheet().get(i)).append("<br />");
			}
		}
		return str.toString();
	}
}
