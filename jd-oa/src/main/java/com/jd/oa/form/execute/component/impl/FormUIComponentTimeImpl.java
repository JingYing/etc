package com.jd.oa.form.execute.component.impl;

import java.util.Hashtable;

import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentTimeImpl extends FormUIComponentAbst {

	public FormUIComponentTimeImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}

	public String getModifyHtmlDefine(Hashtable params) {
		StringBuffer fieldHtml = new StringBuffer();
		fieldHtml.append("<input ").append(super.getHtmlInner()).append(" type='text'  onclick='showTimeWindowPro(this);'  readonly style='cursor:text' name='").append(getMetaDataMapModel().getFieldName()).append("' maxlength=8 class ='actionsoftInput' size='").append(getMetaDataMapModel().getLength()).append("' value='").append(getValue()).append("' >");
		if(Boolean.valueOf(getMetaDataMapModel().getIsNull()) && params.get("bindId") != null ){
			fieldHtml.append("<img src=notNull.gif alt='必须填写' />");
		}else{
			fieldHtml.append("<img src=null.gif alt='允许空' />");
		}	
		return fieldHtml.toString();
	}
	public String getReadHtmlDefine(Hashtable params) {
		StringBuffer html = new StringBuffer();
		html.append("<input type=hidden name='").append(getMetaDataMapModel().getFieldName()).append("' value=\"").append(getValue()).append("\">").append(getValue());
		return html.toString();
	}
	
    public String getValidateJavaScript(Hashtable params) {
        StringBuffer jsCheckvalue = new StringBuffer();
        return jsCheckvalue.toString();
    }

	@Override
	public String getTrueValue() {
		return getValue();
	}
	
}