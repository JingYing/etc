package com.jd.oa.form.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.FormAttachment;
import com.jd.oa.form.model.FormTemplate;

public interface FormAttachmentDao extends BaseDao<FormAttachment,String>{
	
}
