package com.jd.oa.form.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.IOUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.form.dao.FormTemplateDao;
import com.jd.oa.form.execute.RuntimeFormManager;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormTemplateService;

@Component
@CacheProxy
public class FormTemplateServiceImpl extends BaseServiceImpl<FormTemplate, String> implements FormTemplateService{

		private final String FORMTEMPLATEEXTFILENAME = ".htm";
		private final String FORMtEMPLATEJAVASCRIPTFILENAME = ".js";
	  	private static final Logger logger = Logger.getLogger(FormTemplateServiceImpl.class);
	    @Autowired
	    private FormTemplateDao formTemplateDao;
	    
	    @Autowired
		private JssService jssService;
		
		@Autowired
		private FormItemService formItemService;

	    public FormTemplateDao getDao() {
	        return formTemplateDao;
	    }

		@Override
		public FormTemplate getByFormId(String formId) {
			// TODO Auto-generated method stub
			return formTemplateDao.getByFormId(formId);
		}
		
		public String save(FormTemplate formTemplate,String templateValue){
			String fileId = IdUtils.uuid2();
			String fileKey = fileId + FORMTEMPLATEEXTFILENAME;
			jssService.uploadFile(SystemConstant.BUCKET, fileKey, templateValue);
			formTemplate.setTemplatePath(fileKey);
			return insert(formTemplate);
		}
		/**
		 * 保存javascript内容到jss存储服务器
		 * @param formId
		 * @param javascriptContext
		 */
		public void saveJavascript(String formId,String javascriptContext){
			String fileKey = formId + FORMtEMPLATEJAVASCRIPTFILENAME;
			jssService.deleteObject(SystemConstant.BUCKET, fileKey);
			jssService.uploadFile(SystemConstant.BUCKET, fileKey, javascriptContext);
		}
		/**
		 * 从jss存储服务器获取javascript内容
		 */
//		@CachePut(key = @CacheKey(template = "JDOA/FormTemplate/getJavascriptValue_${p0}"), expire = 7200)
		public String getJavascriptValue(String formId){
			String fileKey = formId + FORMtEMPLATEJAVASCRIPTFILENAME;
			String result = "";
			InputStream in = null;
			try{
				in = jssService.downloadFile(SystemConstant.BUCKET, fileKey);
				result = IOUtils.toString(in);
			} catch(IOException e){
				throw new BusinessException("获取表单脚本事件失败！",e);
			} finally {
	        	try{
	        		in.close();
	        	} catch(IOException e) {
	        		
	        	} catch(NullPointerException e){
	        		return result;
	        	}
	        }
			return result;
		}
		public int update(FormTemplate formTemplate,String templateValue){
			boolean isFormTemplateNull = (formTemplate!=null);
			if(isFormTemplateNull){
				String key = formTemplate.getTemplatePath();
				if(key != null && key.trim().length() > 0 && key.indexOf(FORMTEMPLATEEXTFILENAME) > 0){
					key = key.substring(0, key.indexOf(FORMTEMPLATEEXTFILENAME));
					jssService.deleteObject(SystemConstant.BUCKET, key);
				}
			}
			String fileId = IdUtils.uuid2();
			String fileKey = fileId + FORMTEMPLATEEXTFILENAME;
			jssService.uploadFile(SystemConstant.BUCKET, fileKey, templateValue);
			if(isFormTemplateNull){
				formTemplate.setTemplatePath(fileKey);
			}
			return update(formTemplate);
		}

		@Override
		public List<FormTemplate> find(String formId, String formTemplateType) {
			// TODO Auto-generated method stub
			FormTemplate formTemplate = new FormTemplate();
			formTemplate.setFormId(formId);
			if(!"".equals(formTemplateType)){
				formTemplate.setTemplateType(formTemplateType);
				formTemplate.setYn(0);
			}
			return find(formTemplate);
		}

		@Override
		public List<FormTemplate> find(String formId) {
			// TODO Auto-generated method stub
			return find(formId,"");
		}
		
//		@CachePut(key = @CacheKey(template = "JDOA/FormTemplate/getTemplateValue_${p0}"), expire = 7200)
		public String getTemplateValue(String templatePath) throws IOException{
			String result = null;
			InputStream in = null;
			try{
				in = jssService.downloadFile(SystemConstant.BUCKET, templatePath);
				result = IOUtils.toString(in);
			} catch(IOException e){
				throw new BusinessException("获取流程表单模板失败！",e);
			} finally {
	        	try{
	        		in.close();
	        	} catch(IOException e) {
	        		
	        	} catch(NullPointerException e){
	        		throw new BusinessException("获取流程表单模板失败！",e);
	        	}
	        }
			return result;
		}
		public int delete(String id){
			FormTemplate formTemplate = get(id);
			if(formTemplate != null){
				String key = formTemplate.getTemplatePath();
				if(key != null && key.trim().length() > 0 && key.indexOf(FORMTEMPLATEEXTFILENAME) > 0){
					key = key.substring(0, key.indexOf(FORMTEMPLATEEXTFILENAME));
					jssService.deleteObject(SystemConstant.BUCKET, key);
				}
			}
			return super.delete(id);
		}
		/**
		 * 根据formId，获取运行时刻表单数据
		 */
		public Map<String,String> getRuntimeForm(HttpServletRequest request,String formId){
			String contextPath = request.getSession().getServletContext().getRealPath("/");
			RuntimeFormManager runtime = new RuntimeFormManager(contextPath);
			List<FormItem> items = formItemService.getFormItemByFormIdAndType(formId,SystemConstant.FORM_TYPE_M);
			Map<String,String> uiTags = runtime.getRuntimeForm(contextPath , items);
			uiTags.putAll(runtime.getRuntimeFormSubSheet(formItemService.getFormAndItemsByFormId(formId,SystemConstant.FORM_TYPE_S),false));
	    	return uiTags;
		}
		public Map<String ,String> getRuntimeFormSubSheet(HttpServletRequest request,String formId){
			String contextPath = request.getSession().getServletContext().getRealPath("/");
			RuntimeFormManager runtime = new RuntimeFormManager(contextPath);
			return runtime.getRuntimeFormSubSheet(formItemService.getFormItemByFormIdAndType(formId,SystemConstant.FORM_TYPE_S),false);
		}
}
