package com.jd.oa.form.execute.expression.impl;

import java.util.Map;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;

public class OrganizationFullNameExpressionImpl extends ExpressionAbst{

	public OrganizationFullNameExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	private SysUserService sysUserService;
	private SysOrganizationService sysOrganizationService;
	public String expressionParse(String expression) {
		if(sysUserService == null){
			sysUserService = SpringContextUtils.getBean("sysUserService");
		}
		if(sysOrganizationService == null){
			sysOrganizationService = SpringContextUtils.getBean("sysOrganizationService");
		}
		SysUser user = (SysUser) sysUserService.getByUserName(ComUtils.getLoginNamePin());
		if(user != null){
			SysOrganization organization = sysOrganizationService.get(user.getOrganizationId());
			return organization.getOrganizationFullname();
		}
		return "";
	}
	
}
