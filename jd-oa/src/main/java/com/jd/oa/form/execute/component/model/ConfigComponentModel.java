package com.jd.oa.form.execute.component.model;

import java.lang.reflect.Constructor;

public class ConfigComponentModel {
	
	private String id;

	private String title;

	private String interfaceClass;

	private String implementsClass;

	private String desc;

	private String groupName;

	
	private boolean setAlt;

	private Constructor cons;
	//图片设置
	private String icon;
	
	
	public boolean isSetAlt() {
		return setAlt;
	}

	public void setSetAlt(boolean setAlt) {
		this.setAlt = setAlt;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @return Returns the cons.
	 */
	public Constructor getCons() {
		return cons;
	}

	/**
	 * @param cons
	 *   The cons to set.
	 */
	public void setCons(Constructor cons) {
		this.cons = cons;
	}


	/**
	 * @return Returns the groupName.
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName
	 *            The groupName to set.
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return Returns the desc.
	 */
	public String getDesc() {
		if (desc == null)
			desc = "";
		return desc;
	}

	/**
	 * @param desc
	 *            The desc to set.
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return Returns the id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return Returns the implementsClass.
	 */
	public String getImplementsClass() {
		if (implementsClass == null)
			implementsClass = "";
		return implementsClass;
	}

	/**
	 * @param implementsClass
	 *            The implementsClass to set.
	 */
	public void setImplementsClass(String implementsClass) {
		this.implementsClass = implementsClass;
	}

	/**
	 * @return Returns the interfaceClass.
	 */
	public String getInterfaceClass() {
		if (interfaceClass == null)
			interfaceClass = "";
		return interfaceClass;
	}

	/**
	 * @param interfaceClass
	 *            The interfaceClass to set.
	 */
	public void setInterfaceClass(String interfaceClass) {
		this.interfaceClass = interfaceClass;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		if (title == null)
			title = "";
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
