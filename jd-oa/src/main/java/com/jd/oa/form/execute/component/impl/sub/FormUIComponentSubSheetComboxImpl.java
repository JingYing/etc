package com.jd.oa.form.execute.component.impl.sub;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.form.execute.component.FormUISubSheetComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentSubSheetComboxImpl extends FormUISubSheetComponentAbst{

	public FormUIComponentSubSheetComboxImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
	private static final String COMBOX_ID = "value";
	private static final String COMBOX_TEXT = "text";
	public String getEditor(){
		return "editor:{type:'combobox',options:{valueField:'" + COMBOX_ID + "',textField:'" + COMBOX_TEXT + "',onSelect: function(rec){"+getCascadeFunction()+" "+getHtmlInner()+"},data:[" + getComboxDatas(getMetaDataMapModel().getInputTypeConfig()) + "]}}";
	}
	private String getComboxDatas(String inputTypeConfig){
		StringBuffer options = new StringBuffer();
		StringUtils util = new StringUtils(inputTypeConfig);
		
		
		if(util.startsWith("dataDict#")){
			String dictCode = util.replace("dataDict#", "");
			DictDataService dictDataService = SpringContextUtils.getBean("dictDataService");
			List<DictData> list = dictDataService.findDictDataList(dictCode);
			StringBuffer sb = new StringBuffer();
			for (DictData dictData : list) {
				sb.append(dictData.getDictCode());
				sb.append(":");
				sb.append(dictData.getDictName());
				sb.append(":");
				sb.append(dictData.getId());
				sb.append(":");
				sb.append(dictData.getParentId());
				sb.append("|");
			}
			String temp = sb.toString();
			if(temp != null && temp.length() > 0)
				temp = temp.substring(0, temp.length()-1);
			util = new StringUtils(temp);
		}else if (util.startsWith("dataSource#")) {
			DiDatasourceService diDatasourceService = SpringContextUtils.getBean("diDatasourceService");
//			DiDatasourceFieldMapService diDatasouceFieldMapService  = SpringContextUtils.getBean("diDatasouceFieldMapService");
//			diDatasouceFieldMapService.getDataGridDefinition(dataSourceId, f)
			DiDatasource datasource = diDatasourceService.get( util.replace("dataSource#", ""));
			List<Map<String,Object>>  list = null;
			if (datasource.getDataAdapterType()==1) {
				list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), datasource.getExecSql(), "0");
			} else if (datasource.getDataAdapterType()==2) {
				list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, null, "0");
			}
			
			StringBuffer sb = new StringBuffer();
			if(list!=null){
				for (Map<String, Object> map : list) {
					int length = map.values().size();
					Iterator iterator = map.values().iterator();
					if(length == 0){
						
					}else if(length == 1){
						
						Object temp = iterator.next();
						sb.append(temp);
						sb.append(":");
						sb.append(temp);
						sb.append("|");
					}else if(length == 2){
						Object temp = iterator.next();
						Object temp2=  iterator.next();
						sb.append(temp);
						sb.append(":");
						sb.append(temp2);
						sb.append("|");
					}else if(length == 3){
	                    Object temp = iterator.next();
	                    Object temp2=  iterator.next();
	                    Object temp3=  iterator.next();
	                    sb.append(temp);
	                    sb.append(":");
	                    sb.append(temp2);
	                    sb.append("&");
	                    sb.append(temp3);
	                    sb.append("|");
	                }else if(length >= 4){
	                    Object temp = iterator.next();
	                    Object temp2=  iterator.next();
	                    Object temp3=  iterator.next();
	                    Object temp4=  iterator.next();
	                    sb.append(temp);
	                    sb.append(":");
	                    sb.append(temp2);
	                    sb.append("&");
	                    sb.append(temp3);
	                    sb.append("&");
	                    sb.append(temp4);
	                    sb.append("|");
	                }
	            }
			}
			
			String temp = sb.toString();
			if(temp != null && temp.trim().length() > 0){
				temp = temp.substring(0, temp.length()-1);
			}
			util = new StringUtils(temp);
		}else{
			//静态字符串，则保持util不变
		}
		boolean isFather = false;
		
		List<String> v = util.split("|");
		for(int i = 0 ; i < v.size() ; i++){
			String tmpGetValue = v.get(i);
			String tmpGetName = "";
            String extra = "";
            String extra2 = "";
            String tmpGetId = "";
            String parentId = "";
			if(tmpGetValue.indexOf(":") != -1){
				StringUtils util2 = new StringUtils(tmpGetValue);
				List<String> v2 = util2.split(":");
				if(v2.size() == 2){
					if(v2.get(0) != null){
						tmpGetValue = v2.get(0);
					} else {
						tmpGetName = tmpGetValue;
					}
					if(v2.get(1) != null){
                        StringUtils utils3 = new StringUtils(v2.get(1));
                        List<String> v3 = utils3.split("&");
                        if(v3.size()==1)
						    tmpGetName = v3.get(0);
                        else if(v3.size()==2){
                            tmpGetName = v3.get(0);
                            extra = v3.get(1);
                        }else if(v3.size()==3){
                            tmpGetName = v3.get(0);
                            extra = v3.get(1);
                            extra2 = v3.get(2);
                        }
					}else {
						tmpGetName = tmpGetValue;
					}
				}else if(v2.size() == 4){//数据字典id取出，用于关联子select
					isFather = true;
					if(v2.get(0) != null){
						tmpGetValue = v2.get(0);
					} else {
						tmpGetName = tmpGetValue;
					}
					if(v2.get(1) != null){
						tmpGetName = v2.get(1);
					}else {
						tmpGetName = tmpGetValue;
					}
					if(isFather){//isFather
						if(v2.get(2) != null){
							tmpGetId = " ,dictDataId:'"+v2.get(2)+"' ";
						}
						if(v2.get(3) != null){
							parentId = " ,parentId:'"+v2.get(3)+"' ";
						}
					}
				}
			} else {
				tmpGetName = tmpGetValue;
			}
            options.append("{").append(COMBOX_ID).append(":'").append(tmpGetValue).append("',").append(COMBOX_TEXT).append(":'").append(tmpGetName).append("'").append(tmpGetId).append(parentId).append(",extra:'"+extra+"',extra2:'"+extra2+"'},");
		}
		if(options.length() > 0 ){
			options.setLength(options.length() - 1);
		}
		return options.toString();
	} 
	@Override
	public String getFormatter(){
		StringBuffer formate = new StringBuffer();
		formate.append("\n")
				.append(" var values = [").append(getComboxDatas(getMetaDataMapModel().getInputTypeConfig())).append("];\n")
				.append(" for(var i = 0; i< values.length; i++){\n")
				.append("    if (values[i].").append(COMBOX_ID).append(" == value){\n")
//				.append("         alert(values[i].").append(COMBOX_TEXT).append(");\n")
				.append("         return values[i].").append(COMBOX_TEXT).append(";\n")
				.append("    }\n")
				.append(" }\n")
				.append(" return value;\n");
		return formate.toString();
	}
	/**
	 * @author zhengbing 
	 * @desc 构造父combox级联函数
	 * @date 2014年7月11日 下午5:27:58
	 * @return String
	 */
	private String getCascadeFunction(){
		String function = "";
		try {
			FormBusinessField formBusinessField =  getMetaDataMapModel();
			Map<String,String> cascadeMap = formBusinessField.getCascadeComBox();
			String fieldName = formBusinessField.getFieldName();
			if(null!=cascadeMap&&cascadeMap.containsKey(fieldName)){
				String value = cascadeMap.get(fieldName).toString();
				if(null!=value&&value.contains(":")){
					String[] fieldNameAndCode = value.split(":");
					if(null!=fieldNameAndCode&&fieldNameAndCode.length==2)
					function = " sonTableCascadeSelect(rec,'"+formBusinessField.getTableName()+"','"+fieldName+"','"+fieldNameAndCode[0]+"','"+fieldNameAndCode[1]+"'); ";
				}
				
			}
		} catch (Exception e) {
			
		}
		return function;
	}
	
	
	
}
