package com.jd.oa.form.execute.component.impl.sub;

import java.util.List;

import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.form.execute.component.FormUISubSheetComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentSubSheetDateImpl extends FormUISubSheetComponentAbst{

	public FormUIComponentSubSheetDateImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
//	@Override
//	public String getFormate(){
//		return "";
//	}
	@Override
	public String getEditor(){
		StringBuffer editor = new StringBuffer();
		editor.append("editor:{type:'datebox',\n")
			   .append("	options:{\n")
			   .append("		formatter:function(date){\n")
			   .append("              var y = date.getFullYear();\n")
			   .append("              var m = date.getMonth()+1;")
			   .append("              var d = date.getDate();\n")
			   .append("              return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);\n")
			   .append("        },\n")
			   .append("        parser:function(s){\n")
			   .append("           if (!s) return new Date();\n")
			   .append("           		var ss = s.split('-');\n")
			   .append("           		var y = parseInt(ss[0],10);\n")
			   .append("           		var m = parseInt(ss[1],10);\n")
			   .append("           		var d = parseInt(ss[2],10);\n")
			   .append("           		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){\n")
			   .append("           			return new Date(y,m-1,d);\n")
			   .append("           		} else {\n")
			   .append("           			return new Date();\n")
			   .append("           		}\n")
			   .append("        }\n")
		      .append("}}");
		return editor.toString();
	}
}
