package com.jd.oa.form.dao.impl;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.form.dao.FormAttachmentDao;
import com.jd.oa.form.model.FormAttachment;
@Component
public class FormAttachmentDaoImpl extends MyBatisDaoImpl<FormAttachment, String> implements FormAttachmentDao{

}
