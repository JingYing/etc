package com.jd.oa.form.model;

import java.util.Date;
import java.util.List;

import com.jd.oa.common.model.IdModel;

public class FormBusinessTable extends IdModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1283760042190083657L;
	//数据表名
	private String tableChineseName;
	//数据库表名
    private String tableName;
    //表说明
    private String tableDesc;
    //表类型：m 主表；s 子表
    private String tableType;
    //主表ID
    private String parentId;
    //创建类型：0 自动；1 手动
    private String createType;
    //逻辑删除 0:启用 1:删除
    private int yn;
    //拥有者
    private String owner;
    //创建人
    private String creator;
    //创建时间
    private Date createTime;
    //最后修改人
    private String modifier;
    //最后修改时间
    private Date modifyTime;
    //主数据表名
    private String parentChineseName;
    //主数据库表名
    private String parentTableName;
    
    private List<FormBusinessTable> subTables;

	public String getParentChineseName() {
		return parentChineseName;
	}

	public void setParentChineseName(String parentChineseName) {
		this.parentChineseName = parentChineseName;
	}

	public String getParentTableName() {
		return parentTableName;
	}

	public void setParentTableName(String parentTableName) {
		this.parentTableName = parentTableName;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getTableChineseName() {
        return tableChineseName;
    }

    public void setTableChineseName(String tableChineseName) {
        this.tableChineseName = tableChineseName == null ? null : tableChineseName.trim();
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName == null ? null : tableName.trim();
    }

    public String getTableDesc() {
        return tableDesc;
    }

    public void setTableDesc(String tableDesc) {
        this.tableDesc = tableDesc == null ? null : tableDesc.trim();
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType == null ? null : tableType.trim();
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    public String getCreateType() {
        return createType;
    }

    public void setCreateType(String createType) {
        this.createType = createType == null ? null : createType.trim();
    }

    public int getYn() {
        return yn;
    }

    public void setYn(int yn) {
        this.yn = yn;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
    
    public List<FormBusinessTable> getSubTables() {
		return subTables;
	}

	public void setSubTables(List<FormBusinessTable> subTables) {
		this.subTables = subTables;
	}
}