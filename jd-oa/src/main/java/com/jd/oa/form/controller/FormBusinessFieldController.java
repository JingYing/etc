package com.jd.oa.form.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.form.constant.FormBusinessTableConstant;
import com.jd.oa.form.execute.component.FormUIFactory;
import com.jd.oa.form.execute.component.UIComponentInterface;
import com.jd.oa.form.execute.component.model.ConfigComponentModel;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormBusinessFieldService;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;

/**
 * @Description: 自定义表字段controller
 * @author guoqingfu
 * @date 2013-9-12 上午10:46:38
 * @version V1.0
 */
@Controller
@RequestMapping(value = "/form")
public class FormBusinessFieldController {

	private static final Logger logger = Logger
			.getLogger(FormBusinessFieldController.class);

	@Autowired
	private FormBusinessFieldService formBusinessFieldService;

	@Autowired
	private DictDataService dictDataService;
	@Autowired
	private FormItemService formItemService;
	@Autowired
	private FormBusinessTableService formBusinessTableService;

	/**
	 * @Description: 进入到查询自定义表字段列表页面
	 * @return
	 * @throws Exception
	 *             ModelAndView
	 * @author guoqingfu
	 * @date 2013-9-13上午09:45:23
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_index", method = RequestMethod.GET)
	public ModelAndView index(FormBusinessField formBusinessField)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = false;
		try{
			//判断此表是否已经有数据
			String tableIdInfo = formBusinessField.getId();
			if(StringUtils.isNotEmpty(tableIdInfo)){
				FormBusinessTable formBusinessTable = formBusinessTableService.getFormBusinessTableInfo(tableIdInfo);
				String tableName = formBusinessTable.getTableName();
				flag = formBusinessFieldService.isExistBusinessTable(tableName);
				if(flag){
				 	flag = formBusinessFieldService.isExistData(tableName);
					if(flag){
						map.put("operator", 1);
			            map.put("message", "此表已经存在数据，不允许修改表中内容");
					}
				}
			}
			
			ModelAndView mav = new ModelAndView("form/formBusinessField_index");
			mav.addObject("formBusinessFieldId", formBusinessField.getId());
			mav.addObject("tableId", formBusinessField.getTableId());
			mav.addObject("tableType", formBusinessField.getTableType());
			mav.addObject("flag", flag);
			return mav;
		}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("进入字段配置列表页面失败",e);
        }
	}

	/**
	 * @Description: 查询自定义表字段列表
	 * @param request
	 * @return
	 * @throws Exception
	 *             Object
	 * @author guoqingfu
	 * @date 2013-9-13上午09:52:42
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_findPage", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object findPage(HttpServletRequest request) throws Exception {
		try{
			// 创建pageWrapper
			PageWrapper<FormBusinessField> pageWrapper = new PageWrapper<FormBusinessField>(
					request);
			// 添加搜索条件
			String tableId = request.getParameter("tableId"); // 自定义表ID
			if (StringUtils.isNotEmpty(tableId)) {
				pageWrapper.addSearch("tableId", tableId);
			}

			formBusinessFieldService.find(pageWrapper.getPageBean(), "findByPage",
					pageWrapper.getConditionsMap());

			// 返回到页面的数据
			String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
			logger.debug(json);
			return json;	
		}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("进入字段配置列表页面失败",e);
        }
	}

	/**
	 * @Description: 进入到自定义表字段新增页面
	 * @param model
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13上午09:55:26
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_add", method = RequestMethod.GET)
	public String addFormBusinessField(Model model, String tableId) {

		// 数据表UI展示类型
		List<DictData> dictDataList = dictDataService
				.findDictDataList(FormBusinessTableConstant.InputTypeKey);

		// 数据表字段类型
		List<DictData> dictDataInfoList = dictDataService
				.findDictDataList(FormBusinessTableConstant.dsFiledTypeKey);

		model.addAttribute("tableId", tableId);
		model.addAttribute("dictDataList", dictDataList);
		model.addAttribute("dictDataInfoList", dictDataInfoList);
		return "form/formBusinessField_add";
	}

	/**
	 * @Description: 保存新增自定义表字段
	 * @param formBusinessField
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13上午10:07:11
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_addSave", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String saveAddFormBusinessField(FormBusinessField formBusinessField) {
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			// 获取当前操作人
			String userName = ComUtils.getLoginName();
			// formBusinessField.setTableId(tableId);
			formBusinessField.setCreator(userName);
			formBusinessField.setCreateTime(new Date());
			formBusinessField.setModifier(userName);
			formBusinessField.setModifyTime(new Date());
			formBusinessField.setYn(0); // 有效

			String primaryKey = formBusinessFieldService.insert(formBusinessField);
			if (StringUtils.isNotEmpty(primaryKey)) {
				map.put("operator", true);
				map.put("message", "添加成功");
				map.put("tableId", formBusinessField.getTableId());
			} else {
				map.put("operator", false);
				map.put("message", "添加失败");
				map.put("tableId", formBusinessField.getTableId());
			}

			return JSONObject.fromObject(map).toString();
		}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("保存新增数据库表字段失败",e);
        }
	}

	/**
	 * @Description: 进入查看自定义表字段页面
	 * @param formBusinessFieldId
	 * @param model
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13上午10:15:33
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_view", method = RequestMethod.GET)
	public String viewFormBusinessTable(String formBusinessFieldId, Model model) {
		// 根据ID查询自定义表字段信息
		FormBusinessField formBusinessField = formBusinessFieldService
				.getFormBusinessFieldInfo(formBusinessFieldId);
		// 数据表UI展示类型
		List<DictData> dictDataList = dictDataService
				.findDictDataList(FormBusinessTableConstant.InputTypeKey);

		// 数据表字段类型
		List<DictData> dictDataInfoList = dictDataService
				.findDictDataList(FormBusinessTableConstant.dsFiledTypeKey);
		model.addAttribute("dictDataList", dictDataList);
		model.addAttribute("dictDataInfoList", dictDataInfoList);
		model.addAttribute("formBusinessField", formBusinessField);
		return "form/formBusinessField_view";
	}

	/**
	 * @Description: 进入修改自定义表字段页面
	 * @param formBusinessFieldId
	 * @param model
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13上午10:21:49
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_update", method = RequestMethod.GET)
	public String updateFormBusinessField(String formBusinessFieldId,
			Model model) {
		// 根据ID查询自定义表字段信息
		FormBusinessField formBusinessField = formBusinessFieldService
				.getFormBusinessFieldInfo(formBusinessFieldId);
		// 数据表UI展示类型
		List<DictData> dictDataList = dictDataService
				.findDictDataList(FormBusinessTableConstant.InputTypeKey);

		// 数据表字段类型
		List<DictData> dictDataInfoList = dictDataService
				.findDictDataList(FormBusinessTableConstant.dsFiledTypeKey);

		model.addAttribute("dictDataList", dictDataList);
		model.addAttribute("dictDataInfoList", dictDataInfoList);
		model.addAttribute("formBusinessField", formBusinessField);
		return "form/formBusinessField_update";
	}

	/**
	 * @Description: 保存修改自定义表字段信息
	 * @param formBusinessField
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13上午10:27:02
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_updateSave", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String saveUpdateFormBusinessField(
			FormBusinessField formBusinessField) {
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			// 获取当前操作人
			String userName = ComUtils.getLoginName();
			formBusinessField.setModifier(userName);
			formBusinessField.setModifyTime(new Date());

			int i = formBusinessFieldService.update(formBusinessField);
			if (i > 0) {
				map.put("operator", true);
				map.put("message", "修改成功");
			} else {
				map.put("operator", false);
				map.put("message", "修改失败");
			}
			return JSONObject.fromObject(map).toString();
		}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("保存修改数据库表字段信息失败",e);
        }
	}

	/**
	 * @Description:  判断字段是否已被表单使用
	  * @param formBusinessFieldId
	  * @return String
	  * @author guoqingfu
	  * @date 2013-9-27上午11:41:48 
	  * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_isUsedByForm", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String isUsedByForm(String formBusinessFieldId){
		Map<String, Object> map = formBusinessFieldService.isUsedByForm(formBusinessFieldId);
		return JSONObject.fromObject(map).toString();
	}
	/**
	 * @Description: 删除自定义表字段
	 * @param ids
	 * @param length
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13上午11:19:41
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_delete", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String deleteFormBusinessField(String ids, Integer length) {
		List<String> formBusinessFieldIds = new ArrayList<String>();

		if (null == length || length.intValue() == 1) {
			formBusinessFieldIds.add(ids);
		} else {
			String[] batchIds = ids.split(",");
			CollectionUtils.addAll(formBusinessFieldIds, batchIds);
		}
		Map<String, Object> map = formBusinessFieldService
				.deleteFormBusinessField(formBusinessFieldIds);

		return JSONObject.fromObject(map).toString();
	}

	/**
	 * @Description: 判断是否存在字段名或是字段显示名(新增时)
	 * @param formBusinessField
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13下午02:12:43
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_isExistField", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String isExistField(FormBusinessField formBusinessField) {
		Map<String, Object> map = formBusinessFieldService
				.isExistTable(formBusinessField);
		return JSONObject.fromObject(map).toString();
	}

	/**
	 * @Description: 判断是否存在字段显示名(修改时)
	 * @param formBusinessField
	 * @return String
	 * @author guoqingfu
	 * @date 2013-9-13下午02:18:23
	 * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_isExistFieldChineseName", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String isExistFieldChineseName(FormBusinessField formBusinessField) {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag = formBusinessFieldService
				.isExistFieldChineseName(formBusinessField);
		if (flag) {
			map.put("operator", true);
			map.put("message", "存在字段显示名");
		} else {
			map.put("operator", false);
			map.put("message", "不存在字段显示名");
		}
		return JSONObject.fromObject(map).toString();
	}

	/**
	  * @Description: 创建物理表
	  * @param businessTableId
	  * @return String
	  * @author guoqingfu
	  * @date 2013-9-23上午10:08:08 
	  * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessField_createBusinessTable", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String createBusinessTable(String businessTableId) {
		Map<String, Object> map = formBusinessFieldService.createBusinessTable(businessTableId);
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * form ui 设计主页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/filed_item_ui_design_index", method = RequestMethod.GET)
	public ModelAndView getDesign(HttpServletRequest request,String fieldId,String inputType,String formId,Boolean isFormDesign) throws Exception {
		ModelAndView mv = new ModelAndView();
		if(inputType == null || inputType.trim().length() == 0){
			inputType = "单行";
		}
		mv.addObject("fieldId", fieldId);
		mv.addObject("inputType", inputType);
		mv.addObject("isFormDesign", isFormDesign);
		mv.addObject("formId", formId);
		mv.addObject("zNodes", getFormUIComponentJsonData(request));
		mv.setViewName("form/formBusinessFieldItemUIDesign_index");
		return mv;
	}

	/**
	 * ui tree node json data
	 * 
	 * @param request
	 * @throws Exception
	 */
//	@RequestMapping(value = "/getFormUItree_JsonData", method = RequestMethod.POST)
//	@ResponseBody
	public String getFormUIComponentJsonData(HttpServletRequest request) throws Exception {
		StringBuffer jsonStr = new StringBuffer();
		String contextPath = request.getSession().getServletContext().getRealPath("/");
		Map<String, Object> componentGroupList = FormUIFactory.getInstance(contextPath).getComponentGroupList();
		Map<Integer, ConfigComponentModel> componentSortList = FormUIFactory.getInstance(contextPath).getComponentSortList();
		for( int i = 0 ; i < componentGroupList.size() ; i++){
			String groupName = (String) componentGroupList.get(String.valueOf(i));
			jsonStr.append("{");
			jsonStr.append("name : '").append(groupName).append("',").append("open:true,isParent:true");
			jsonStr.append(",").append("children :") .append("[").append(getUITreeNodes(componentSortList,groupName)).append("]");
			jsonStr.append("}");
			
			jsonStr.append(",");
		}
		if(jsonStr.length() > 0){
			jsonStr.setLength(jsonStr.length() - 1);
		}
		return jsonStr.toString();
	}
	private String getUITreeNodes(Map<Integer, ConfigComponentModel> componentSortList,String groupName) {
		StringBuffer jsonStr = new StringBuffer();
		for (int i = 0; i < componentSortList.size(); i++) {// 分组信息
			ConfigComponentModel configComponentModel = (ConfigComponentModel) componentSortList.get(Integer.valueOf(i));
			if (configComponentModel != null&& configComponentModel.getGroupName().trim().length() > 0 && configComponentModel.getGroupName().equals(groupName)) {
				String name = configComponentModel.getTitle();
				jsonStr.append("{");
				jsonStr.append("name : '").append(name).append("',id:'").append(configComponentModel.getId()).append("'");
				jsonStr.append("}");
				
				jsonStr.append(",");
			}
		}
		if(jsonStr.length() > 0){
			jsonStr.setLength(jsonStr.length() - 1);
		}
		return jsonStr.toString();
	}

	@RequestMapping(value = "/filed_item_ui_design_display", method = RequestMethod.GET)
	public ModelAndView getDisplay(HttpServletRequest request,String fieldId,String inputType,String formId,boolean isFormDesign) throws Exception {
		ModelAndView mv = new ModelAndView();
		FormBusinessField metaDataMapModel = null;
		if(isFormDesign){// from form design
			FormItem formItem = formItemService.get(fieldId);
			if(formItem != null){
				metaDataMapModel = new FormBusinessField();
				ComUtils.copyProperties(formItem, metaDataMapModel);
			}
		} else {
			metaDataMapModel = new FormBusinessField();
			metaDataMapModel = formBusinessFieldService.get(fieldId);
		}
		if(inputType == null || inputType.trim().length() == 0){
			inputType = "单行";
		}
		if(metaDataMapModel == null || metaDataMapModel.getInputType() == null || !metaDataMapModel.getInputType().equals(inputType)){
			metaDataMapModel = new FormBusinessField();
			metaDataMapModel.setInputType(inputType);
			metaDataMapModel.setInputTypeConfig("");
		}
		String contextPath = request.getSession().getServletContext().getRealPath("/");
		UIComponentInterface uiComponent = FormUIFactory.getInstance(contextPath).getUIInstance(metaDataMapModel, "");
		Map<String, ConfigComponentModel> componentList = FormUIFactory.getInstance(contextPath).getComponentList();
		if(componentList == null || componentList.size() == 0){
			throw new Exception("获取表单UI组件配置列表失败!");
		}
		ConfigComponentModel configComponentModel = (ConfigComponentModel) componentList.get(inputType);
		// 如果已知的UIComponent不存在,返回
		if (configComponentModel == null) {
			mv.setViewName("form/formBusinessFieldItemUIDesign_error");
			return mv;
		} else {
			mv.addObject("htmlInner", metaDataMapModel.getHtmlInner() == null ? "" :  metaDataMapModel.getHtmlInner());
			mv.addObject("settingWeb", uiComponent.getSettingWeb());
			mv.addObject("formId", formId);
			mv.addObject("fieldId", fieldId);
			mv.setViewName("form/formBusinessFieldItemUIDesign_display");
		}
		return mv;
	}

	@RequestMapping(value = "/formBusinessFieldItem_edit")
	@ResponseBody 
	public String formBusinessFieldItem_edit(
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "fieldName", required = false) String fieldName,
			@RequestParam(value = "fieldChineseName", required = false) String fieldChineseName,
			@RequestParam(value = "dataType", required = false) String dataType,
			@RequestParam(value = "length", required = false) String length,
			@RequestParam(value = "defaultValue", required = false) String defaultValue,
			@RequestParam(value = "inputType", required = false) String inputType,
			@RequestParam(value = "sortNo", required = false) String sortNo){
		int num=0;
		 
		
		if(id!=null){
			FormBusinessField field = new FormBusinessField();
			field.setId(id);
			field.setFieldName(fieldName);
			field.setFieldChineseName(fieldChineseName);
			field.setDataType(dataType);
			field.setLength(length);
			field.setDefaultValue(defaultValue);
			field.setInputType(inputType);
			field.setSortNo(new Float(sortNo).floatValue());
			num = formBusinessFieldService.update(field);
			
		}
		
		return Integer.valueOf(num).toString();
	}
	
	//编辑单个记录下，jeditable
	//value=fd&id=&item_id=15c7a7ef0dbb45f29ed21dd458e116b3&item_column=field_name
//	@RequestMapping(value = "/formBusinessFieldItem_edit", method = RequestMethod.POST)
//	@ResponseBody 
//	public String formBusinessFieldItem_edit(
//			@RequestParam(value = "item_id", required = false) String item_id,
//			@RequestParam(value = "item_column", required = false) String item_column,
//			@RequestParam(value = "value", required = false) String item_value){
//		
//		
//		FormBusinessField field = new FormBusinessField();
//		field.setId(item_id);
//		
//		if("field_name".equals(item_column))
//			field.setFieldName(item_value);
//		if("field_chinese_name".equals(item_column))
//			field.setFieldChineseName(item_value);
//		if("sort_no".equals(item_column))
//			field.setSortNo((new Float(item_value)).floatValue());
//		if("data_type".equals(item_column))
//			field.setDataType(item_value);
//		if("input_type".equals(item_column))
//			field.setInputType(item_value);
//        if("length".equals(item_column))
//            field.setLength(item_value);
//        if("defaultValue".equals(item_column))
//            field.setDefaultValue(item_value);
//		
//		formBusinessFieldService.update(field);
//		
//		return item_value;
//	}
}
