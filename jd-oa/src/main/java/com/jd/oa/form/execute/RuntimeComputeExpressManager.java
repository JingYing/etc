package com.jd.oa.form.execute;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;

/**
 * 运行时刻 行列规则
 * @author birkhoff
 * @date   2013年10月30日
 *
 */
public class RuntimeComputeExpressManager {
	private RuntimeFormManager rfm;
	public RuntimeComputeExpressManager(RuntimeFormManager rfm){
		this.rfm = rfm;
	}
	/**
	 * 行规则 解析javascript
	 * formCode 子表 表名
	 * @return
	 */
	public String getRowOfRulesJavascript(List<FormItem> formItems,String formCode){
		StringBuffer javascript = new StringBuffer();
		javascript.append(", \n").append("onAfterEdit : function(rowIndex, rowData, changes){\n");
		//行规则计算
		if(formItems != null && formItems.size() > 0){
			javascript.append("<!--行规则计算-->\n");
			for(int i = 0 ; i < formItems.size() ; i++){
				FormItem formItem = formItems.get(i);
				if(formItem != null && formItem.getComputeExpress() != null && formItem.getComputeExpress().trim().length() > 0){
//					System.out.println(computeString(formItem.getComputeExpress()));
					javascript.append("try{\n");
					javascript.append("$('#").append(formCode).append("').datagrid('updateRow',{index:rowIndex,row:{")
							  .append(getComputeStringEqualSignLeft(formItem.getComputeExpress()))
							  .append(":fmoney2(").append(computeString(getComputeStringEqualSignRight(formItem.getComputeExpress()),formItems))
							  .append(",4)")
							  .append("}});\n");
					javascript.append("}catch(e){}\n");
				}
			}
			javascript.append("<!--end 行规则计算-->\n");
		}
		javascript.append("<!--列规则计算-->\n");
		javascript.append("updateComputeExpress('").append(formCode).append("');\n");
		javascript.append("<!--end 列规则计算-->\n");
		javascript.append("}\n");
		return javascript.toString();
	}
	/**
	 * 检测指定子表是否配置列计算规则
	 * @param formItems
	 * @param formCode
	 * @return
	 */
	public boolean checkRowOfRules(String formCode){
		if(this.rfm._items != null && this.rfm._items.size() > 0){
			for(int i = 0 ; i < this.rfm._items.size() ; i++){
				FormItem formItem = this.rfm._items.get(i);
				if(formItem != null && formItem.getComputeExpress() != null && formItem.getComputeExpress().trim().length() > 0){
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * 列规则 解析javascript
	 *  formCode 子表 表名
	 * @return
	 */
	private FormItemService formItemService;
	private FormService formService;
	public String getColOfRulesJavascript(FormItem formItem){
		StringBuffer javascript = new StringBuffer();
		//getChanges
		if(formItemService == null){
			formItemService = SpringContextUtils.getBean(FormItemService.class);
		}
		if(formService == null){
			formService = SpringContextUtils.getBean(FormService.class);
		}
		FormItem subFormItem = formItemService.get(formItem.getComputeExpress());
		if(subFormItem != null){
			Form form = formService.get(subFormItem.getFormId());
			if(form != null && form.getFormCode() != null && form.getFormCode().trim().length() > 0){
				javascript.append("if(formCode == '").append(form.getFormCode()).append("'){\n");
				javascript.append("try{\n");
				javascript.append(" var sum = 0;\n");
				javascript.append(" var datas = $('#' + formCode).datagrid('getData');\n");
				javascript.append(" var rows = datas.rows;\n");
				javascript.append(" for(var i = 0 ; i < rows.length ;i++){\n");
				javascript.append("    var row = rows[i];\n");
				javascript.append("    sum += Number(rmoney(row.").append(subFormItem.getFieldName()).append("));\n");
				javascript.append(" }\n");
				javascript.append(" document.getElementById('").append(formItem.getFieldName()).append("').value = fmoney(sum,4);\n");
				javascript.append("}catch(e){}\n");
				javascript.append("}\n");
			}
		}
		return javascript.toString();
	}
	/**
	 * 计算字符串四则运算表达式 
	 */
	private String computeString(String express,List<FormItem> formItems){
		Matcher matcher = null;   
        String temp = "";   
        int index = -1;   
        express = express.replaceAll("\\s", "");// 去除空格   
        return replaceComputeStirng(express,formItems);
	}
	/**
	 * 解析等号左侧内容
	 * @param express
	 * @return
	 */
	private static String getComputeStringEqualSignLeft(String express){
		String regexComputeStringEqualSignLeft = "=";
		String temp = "";   
        int index = -1;   
		// 解析等号左侧内容
        Pattern pattern = Pattern.compile(regexComputeStringEqualSignLeft);   
        Matcher matcher = null;   
        while (pattern.matcher(express).find()) {   
             matcher = pattern.matcher(express);   
             if (matcher.find()) {   
                  temp = matcher.group();   
                  index = express.indexOf(temp);   
                  express = express.substring(0, index);   
             }   
        }  
        return express;
	}
	/**
	 * 解析等号右侧内容
	 * @param express
	 * @return
	 */
	private static String getComputeStringEqualSignRight(String express){
		String regexComputeStringEqualSignLeft = "=";
		String temp = "";   
        int index = -1;   
		// 解析等号左侧内容
        Pattern pattern = Pattern.compile(regexComputeStringEqualSignLeft);   
        Matcher matcher = null;   
        while (pattern.matcher(express).find()) {   
             matcher = pattern.matcher(express);   
             if (matcher.find()) {   
                  temp = matcher.group();   
                  index = express.indexOf(temp);   
                  express = express.substring(index + temp.length());   
             }   
        }  
        return express;
	}
	/**
	 * 替换表达式
	 * @param express
	 * @return
	 */
    private static String replaceComputeStirng(String express,List<FormItem> formItems) {   
         if(formItems != null && formItems.size() > 0){
        	 for( int i = 0 ; i < formItems.size() ; i++){
        		 FormItem formItem = formItems.get(i);
        		 if(formItem != null){
        			 if(express.contains(formItem.getFieldName())){
        				 express = express.replaceAll(formItem.getFieldName(), replaceJavascript(formItem.getFieldName()));
        			 }
        		 }
        	 }
         }
         return express;   
    } 
    /**
     * 替换成javascript可执行语法
     * @param itemName
     * @return
     */
    private static String replaceJavascript(String itemName){
    	return "Number(rowData." + itemName + ")";
    }
	/**
	 * 判断是否是一个合法的表达式
	 */
	private boolean isCheckCompute(String express){
		String regexCheck = "[\\(\\)\\d\\+\\-\\*/\\.]*";// 是否是合法的表达式   
		if (!Pattern.matches(regexCheck, express)){
			return false;
		} else {
			return true;
		}
	}
}
