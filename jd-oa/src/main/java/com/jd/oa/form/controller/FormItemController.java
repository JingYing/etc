package com.jd.oa.form.controller;

import java.util.List;

import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormBusinessFieldService;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;
import com.jd.oa.process.service.ProcessDefinitionConfigDetailService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单字段配置
 * User: yujiahe
 * Date: 13-9-15
 * Time: 下午2:56
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/form")
public class FormItemController {
    private static final Logger logger = Logger.getLogger(FormBusinessTableController.class);
    @Autowired
    private FormItemService formItemService;
    @Autowired
    private FormService formService;
    @Autowired
    private ProcessDefinitionConfigService processDefinitionConfigService;
    @Autowired
    private ProcessDefinitionConfigDetailService processDefinitionConfigDetailService;
    @Autowired
    private FormBusinessTableService formBusinessTableService;
    @Autowired
    private FormBusinessFieldService formBusinessFieldService;

    
    /**
     * 表单库首页
     */

    @RequestMapping(value = "/formItem_index", method = RequestMethod.GET)
    public String formIndex(String formId, String createType, String tableId, Model model) {
        model.addAttribute("formId", formId);
        model.addAttribute("createType", createType);
        model.addAttribute("formItem_AdvancedModel", "formItem_advanced_set?formId=" + formId + "&tableId=" + tableId);
        model.addAttribute("formItem_EditItem", "formItem_EditItem?formId=" + formId+"&createType="+createType + "&tableId=" + tableId);
        model.addAttribute("formItem_EditRowRules", "formItem_EditRowRules?formId=" + formId);
        model.addAttribute("formItem_EditColumnRules", "formItem_EditColumnRules?formId=" + formId);
        model.addAttribute("formItem_EditTemplate", "formTemplate_edit?formId=" + formId);
        model.addAttribute("formItem_PrintTemplate", "formTemplate_edit?formId=" + formId + "&isPrint=true");
        model.addAttribute("formItem_JavascriptTemplate", "formTemplate_javascript?formId=" + formId);
        return "form/formItem_index";
    }
    
    /**
     * 判断表单对应的业务表是否创建了物理表且是否存在数据
     * @param tableId
     * @return false:表示存在业务数据，不允许修改除UI类型外的字段信息 true:表示不存在数据，可以修改
     */
    private boolean checkTable(String tableId){
    	//判断表是否被创建过
    	boolean result = false;
    	if(tableId!=null && !"".equals(tableId)){
    		FormBusinessTable table = this.formBusinessTableService.get(tableId);
    		if(table!=null){
    			boolean isCreate = this.formBusinessFieldService.isExistBusinessTable(table.getTableName());
            	if(isCreate){
            		//查询是否有数据
            		result = this.formBusinessFieldService.isExistData(table.getTableName());
            	}
    		}
    	}
    	
    	return !result;
    }
    
    /**
     * @Description: 查询表单字段配置列表/表单表达式查询分页
     * @author yujiahe
     */
    @RequestMapping(value = "/formItem_mainFormPage")
    @ResponseBody
    public Object page(HttpServletRequest request, @RequestParam String formId, @RequestParam String computeExpressDesc, FormItem formItem) {
        //创建pageWrapper
        PageWrapper<FormItem> pageWrapper = new PageWrapper<FormItem>(request);
        if (null != formId && !formId.equals("")) {
            pageWrapper.addSearch("formId", formId);
        }
        if (null != computeExpressDesc && !computeExpressDesc.equals("")) {
            pageWrapper.addSearch("computeExpressDesc", computeExpressDesc);
        }
        //后台取值
        formItemService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
        //返回到页面的数据
        String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug("result:" + json);
        return json;
    }

    @RequestMapping(value = "/formItem_mainFormItemCptExpPage")
    @ResponseBody
    public Object mainFormItemCptExpPage(HttpServletRequest request, @RequestParam String formId) {
        //创建pageWrapper
        PageWrapper<FormItem> pageWrapper = new PageWrapper<FormItem>(request);
        if (null != formId && !formId.equals("")) {
            pageWrapper.addSearch("formId", formId);
        }
        //后台取值
        formItemService.find(pageWrapper.getPageBean(), "findMainFormItemCptExpByMap", pageWrapper.getConditionsMap());
        //返回到页面的数据
        String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug("result:" + json);
        return json;
    }

    /**
     * @Description: 新增表单配置单条
     * @author yujiahe
     */

    @RequestMapping(value = "/formItem_addSave")
    @ResponseBody
    public String saveAddFormItem(FormItem formItem) {
        Map<String, Object> map = new HashMap<String, Object>();
        String primaryKey = formItemService.insert(formItem);
        if (StringUtils.isNotEmpty(primaryKey)) {
            map.put("operator", true);
            map.put("message", "添加成功");
            map.put("formItemId", formItem.getId());
        } else {
            map.put("operator", false);
            map.put("message", "添加失败");
            map.put("formItemId", formItem.getId());
        }

        return JSONObject.fromObject(map).toString();
    }

    /**
     * 打开表单库-表单项设置iframe
     *
     * @param formId
     * @return
     */
    @RequestMapping(value = "/formItem_EditItem")
    public ModelAndView getFormItem(HttpServletResponse response, String formId,String createType,String tableId ) {

        ModelAndView mv = new ModelAndView();
        Form sonForm = new Form();
        sonForm.setParentId(formId);
        List<Form> sonFormList = formService.find(sonForm); //根据parentId查找子表单列表
        mv.addObject("sonFormList", sonFormList);
        mv.addObject("formId", formId);
        mv.addObject("createType", createType);
        boolean check = this.checkTable(tableId);
        mv.addObject("isUpdateMark", Boolean.toString(check));
        mv.setViewName("form/formItem_EditItem");
        return mv;
    }

    /**
     * 在流程定义，配置表单显示列中使用
     */
    @RequestMapping(value = "/formItem_EditItemView")
    public ModelAndView getFormItemView(HttpServletResponse response, String formId,String processId) {
    	
    	ModelAndView mv = new ModelAndView();
    	List<Form> formlist = formService.findFormWithSubForm(formId);
    	List<String> formIds = new ArrayList<String>();
    	for(Form form:formlist){
    		formIds.add(form.getId());
    	}
    	List<FormItem> formItems = formItemService.findFromAndSubItems(formIds);
        
//        Form sonForm = new Form();
//        sonForm.setParentId(formId);
//        List<Form> sonFormList = formService.find(sonForm); //根据parentId查找子表单列表
//        mv.addObject("sonFormList", sonFormList);
    	
    	List<String> configColumns = new ArrayList<String>();
    	List<String> configSubViewColumns = new ArrayList<String>();
    	List<String> configSubColumns = new ArrayList<String>();
        List<ProcessDefinitionConfig> list = null;
        if(processId!=null && ! "".equals(processId)){
        	//主表字段
	        ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
	    	pdc.setProcessDefinitionId(processId);
	    	pdc.setConfigType("1");
	    	
	    	list = processDefinitionConfigService.find(pdc);
	    	for (ProcessDefinitionConfig c : list) {
				configColumns.add(c.getConfigItem());
			}
	    	
	    	//子表字段
	    	ProcessDefinitionConfig pdcsub = new ProcessDefinitionConfig();
	    	pdcsub.setProcessDefinitionId(processId);
	    	pdcsub.setConfigType("3");
	    	List<ProcessDefinitionConfig> sublist = processDefinitionConfigService.find(pdcsub);
	    	for(ProcessDefinitionConfig config: sublist){
	    		ProcessDefinitionConfigDetail detail = new ProcessDefinitionConfigDetail();
	    		detail.setConfigId(config.getId());
	    		List<ProcessDefinitionConfigDetail> detailList = processDefinitionConfigDetailService.find(detail);
	    		for(ProcessDefinitionConfigDetail configDetail: detailList){
	    			configSubColumns.add(config.getConfigItem()+":"+configDetail.getValNo());
	    			configSubViewColumns.add(configDetail.getValNo());
	    		}
	    	}
	    	
        }
        
        mv.addObject("configColumns", JsonUtils.toJsonByGoogle(configColumns));
        mv.addObject("configSubViewColumns", JsonUtils.toJsonByGoogle(configSubViewColumns));
        mv.addObject("configSubColumns", JsonUtils.toJsonByGoogle(configSubColumns));
        mv.addObject("formlist", formlist);
    	mv.addObject("formItems",formItems);
        mv.addObject("formId", formId);
        mv.setViewName("form/formItem_EditItemView");
        return mv;
    }
    
    /**
     * 打开表单库-表单行规则设置iframe
     *
     * @param formId
     * @return
     */
    @RequestMapping(value = "/formItem_EditRowRules")
    public ModelAndView getFormRowRules(HttpServletResponse response, String formId) {
        ModelAndView mv = new ModelAndView();
        Form sonForm = new Form();
        sonForm.setParentId(formId);
//        sonForm.setTableId("haveIntItem");
        List<Form> sonFormList = formService.find(sonForm); //根据parentId查找子表单列表
        mv.addObject("sonFormList", sonFormList);
        mv.addObject("formId", formId);
        mv.setViewName("form/formItem_EditRowRules");
        return mv;
    }  //formItem_EditColumnRules

    /**
     * 打开表单库-表单列规则设置iframe
     *
     * @param formId
     * @return
     */
    @RequestMapping(value = "/formItem_EditColumnRules")
    public ModelAndView getFormEditColumnRules(HttpServletResponse response, String formId) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("formId", formId);
        mv.setViewName("form/formItem_EditColumnRules");
        return mv;
    }

    /**
     * 编辑主表单分页中单个Item
     *
     * @param item_id
     * @param item_column
     * @param item_value
     * @return
     */
    @RequestMapping(value = "/formItem_itemEdit", method = RequestMethod.POST)
    @ResponseBody
    public String formBusinessFieldItem_edit(
            @RequestParam(value = "item_id", required = false) String item_id,
            @RequestParam(value = "item_column", required = false) String item_column,
            @RequestParam(value = "value", required = false) String item_value) {


        FormItem formItem = new FormItem();
        formItem.setId(item_id);
        //排序
        if ("sort_no".equals(item_column))
            formItem.setSortNo(new Float(item_value));
        //表单项显示名称
        if ("item_name".equals(item_column))
            formItem.setFieldChineseName(item_value);
        //表单项
        if ("item_code".equals(item_column))
            formItem.setFieldName(item_value);
        //表单项
        if ("data_type".equals(item_column))
            formItem.setDataType(item_value);

        formItemService.update(formItem);

        return item_value;
    }    //formItem_addEmptyItem  formItem_findNumItem

    @RequestMapping(value = "/formItem_getFormItemByFormId")
    @ResponseBody
    public String getFormItemById(FormItem formItem) {
//        Map<String, Object> map = new HashMap<String, Object>();
//        FormItem formItem = new FormItem();
//        formItem.setFormId(formId);
        List<FormItem> FormItem = formItemService.find(formItem);
        String json = JsonUtils.toJsonByGoogle(FormItem);
        return json;
    }

    /**
     * @Description:删除表单项支持批量
     * @author yujiahe
     */
    @RequestMapping(value = "/formItem_delete")
    @ResponseBody
    public String formItemDelete(String ids) {
        Map<String, Object> map = new HashMap<String, Object>();
        formItemService.mutidelete(ids);//批量删除
        map.put("operator", true);
        map.put("message", "删除成功");
        return JSONObject.fromObject(map).toString();
    }

    @RequestMapping(value = "/formItem_cptExpAdd")
    @ResponseBody
    public String formItemCptExpAdd(FormItem formItem) {
        Map<String, Object> map = new HashMap<String, Object>();
        formItemService.update(formItem);
        map.put("operator", true);
        map.put("message", "修改成功");
        return JSONObject.fromObject(map).toString();
    }

    /**
     * 列规则编辑
     *
     * @param formItem
     * @param oldItemId
     * @return
     */
    @RequestMapping(value = "/formItem_columnRolesEdit")
    @ResponseBody
    public String formItemColumnRolesEdit(FormItem formItem, String oldItemId) {
        Map<String, Object> map = new HashMap<String, Object>();
        formItemService.update(formItem);
        String[] args = oldItemId.split(",");
        for (String updateItemId : args) {
            logger.debug(updateItemId+"updateItemId");
            FormItem oldFormItem = new FormItem();
            oldFormItem.setId(updateItemId);
            oldFormItem.setComputeExpress("");
            oldFormItem.setComputeExpressDesc("");
            formItemService.update(oldFormItem);
        }
        map.put("operator", true);
        map.put("message", "修改成功");
        return JSONObject.fromObject(map).toString();
    } //formItem_delFormColumnRules




    /**
     * @Description: 查询列规则分页
     * @author yujiahe
     */
    @RequestMapping(value = "/formItem_columnRulesPage")
    @ResponseBody
    public Object formColumnRulesPage(HttpServletRequest request, @RequestParam String formId) {
        //创建pageWrapper
        PageWrapper<FormItem> pageWrapper = new PageWrapper<FormItem>(request);
        if (null != formId && !formId.equals("")) {
            pageWrapper.addSearch("formId", formId);
        }
        //后台取值
        formItemService.find(pageWrapper.getPageBean(), "findColumnRulesByMap", pageWrapper.getConditionsMap());
        //返回到页面的数据
        String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug("result:" + json);
        return json;
    }

    @RequestMapping(value = "/formItem_getSonFormByParentId")
    @ResponseBody
    public String getSonFormByParentId(String formId) {
//        Map<String, Object> map = new HashMap<String, Object>();
        Form sonForm = new Form();
        sonForm.setParentId(formId);
        sonForm.setTableId("haveIntItem");
        List<Form> sonFormList = formService.find(sonForm); //根据parentId查找子表单列表
        String json = JsonUtils.toJsonByGoogle(sonFormList);
        return json;
    }
    @RequestMapping(value = "/formItem_edit")
    @ResponseBody
    public String formItem_edit(FormItem formItem){
        int num=0;
        if(formItem.getId()==null||formItem.getId().equals("")){
             formItem.setId(null);
             formItemService.insert(formItem);
        } else{
            formItemService.update(formItem);
        }

        return Integer.valueOf(num).toString();
    }

    /**
     * 查找某个字段配置的字段名是否唯一
     * @param formItem
     * @return
     */
    @RequestMapping(value = "/formItem_getItemNumOfForm")
    @ResponseBody
    public String getItemNumOfForm(FormItem formItem) {
//        Map<String, Object> map = new HashMap<String, Object>();
        List<FormItem> FormItemList = formItemService.selectItemByFormIdAndFieldName(formItem.getFormId(), formItem.getFieldName());
       int num=FormItemList.size();
        return Integer.valueOf(num).toString();
    }

}
