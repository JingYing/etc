package com.jd.oa.form.execute.component.impl.sub;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.execute.component.FormUISubSheetComponentAbst;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormAttachment;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormAttachmentService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
/**
 * 附件上传
 * @author birkhoff
 *
 */
public class FormUIComponentSubSheetFileImpl extends FormUISubSheetComponentAbst{

	public FormUIComponentSubSheetFileImpl(FormBusinessField metaDataMapModel,String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
	private FormItemService formItemService;
	private FormService formService;
	public String getEditor(){
		if(formItemService == null){
			formItemService = SpringContextUtils.getBean(FormItemService.class);
		}
		if(formService == null){
			formService = SpringContextUtils.getBean(FormService.class);
		}
		String tableName = "";
		FormItem formItem = formItemService.get(getMetaDataMapModel().getId());
		if(formItem != null){
			Form form = formService.get(formItem.getFormId());
			if(form != null){
				tableName = form.getFormCode();
			}
		}
		return "editor:{type:'file',options:{fieldName:'" + getFieldId() + "',tableName:'" + tableName + "'}}";
	}
	
	public String getFormatter(){
		if(formItemService == null){
			formItemService = SpringContextUtils.getBean(FormItemService.class);
		}
		if(formService == null){
			formService = SpringContextUtils.getBean(FormService.class);
		}
		String tableName = "";
		FormItem formItem = formItemService.get(getMetaDataMapModel().getId());
		if(formItem != null){
			Form form = formService.get(formItem.getFormId());
			if(form != null){
				tableName = form.getFormCode();
			}
		}
		StringBuffer formate = new StringBuffer();
		formate.append(" var values = \"<div style='margin:0 auto;padding:0 auto;text-align:center;width:'><img style='cursor:pointer' onClick=\\\"upfileOfSubSheet('"+tableName+"','true');return false;\\\" title='点击进行附件操作' border='0' src='/static/common/img/search.png'></div>\"; \n");
		formate.append(" return values; \n");
		return formate.toString();
	}
	
}
