package com.jd.oa.form.execute;

public class RuntimeFormToolbar {
	
	private String submitButton(){
		StringBuffer button = new StringBuffer();
		button.append("<input type=button id='saveButton' class='btn' onClick='submitProcess();return false;' value='提交'>");
		return button.toString();
	}
	private String closeButton(){
		StringBuffer button = new StringBuffer();
		button.append("<input type=button id='close' class='btn' value='关闭'>");
		return button.toString();
	}
	private String saveDraft(){
		StringBuffer button = new StringBuffer();
		button.append("<input type=button id='save' class='btn' onClick='saveDraft();return false;' value='暂存'>");
		return button.toString();
	}
	
	public String toString(){
		StringBuffer toolBar = new StringBuffer();
		toolBar.append("<div class=\"navbar\">\n");
		toolBar.append("<div class=\"navbar-inner\">\n");
		toolBar.append("<table width='300px' style='margin-left:40%;'>\n");
		toolBar.append("<tr>");
		toolBar.append("<td width='30%'>").append(submitButton()).append("</td>");
		toolBar.append("<td width='30%'>").append(saveDraft()).append("</td>");
		toolBar.append("<td width='30%'>").append(closeButton()).append("</td>");
		toolBar.append("</tr></table>\n");
		toolBar.append("</div>\n");
		toolBar.append("</div>\n");
		return toolBar.toString();
	}
}
