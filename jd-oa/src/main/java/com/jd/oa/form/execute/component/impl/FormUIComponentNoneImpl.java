package com.jd.oa.form.execute.component.impl;

import java.util.Hashtable;

import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;



/**
 * 尚未定义的组件
 * 
 */
public class FormUIComponentNoneImpl extends FormUIComponentAbst {
	/**
	 * @param value
	 * @param metaDataMapModel
	 * 
	 */
	public FormUIComponentNoneImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}

	public String getModifyHtmlDefine(Hashtable params) {
		return "尚未实现的UI组件类型";
	}

	/**
	 * 
	 */
	public String getReadHtmlDefine(Hashtable params) {
		return "尚未实现的UI组件类型";
	}

	/**
	 * 获取当前组件的客户端JavaScript校验代码，包括字段超长、是否允许为空、常规值校验
	 * 
	 * @param params
	 * @return
	 * 
	 */
	public String getValidateJavaScript(Hashtable params) {
		return "";
	}

	/**
	 * 获取当前组件值的客户端JavaScript代码
	 * 
	 * @param params
	 * @return
	 * 
	 */
	public String getValueJavaScript(Hashtable params) {
		return "";
	}
	public String getSettingWeb(){
		return "尚未实现的UI组件类型";
	}

	@Override
	public String getTrueValue() {
		return "尚未实现的UI组件类型";
	}
}
