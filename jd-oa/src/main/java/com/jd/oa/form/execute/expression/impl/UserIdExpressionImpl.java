package com.jd.oa.form.execute.expression.impl;

import java.util.Map;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;

public class UserIdExpressionImpl extends ExpressionAbst{

	public UserIdExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	private SysUserService sysUserService;
	public String expressionParse(String expression) {
		if(sysUserService == null){
			sysUserService = SpringContextUtils.getBean(SysUserService.class);
		}
		SysUser user = (SysUser) sysUserService.getByUserName(ComUtils.getLoginNamePin());
		if(user != null){
			return user.getUserName();
		}
		return "";
	}
}
