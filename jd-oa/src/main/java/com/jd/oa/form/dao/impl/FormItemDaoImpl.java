package com.jd.oa.form.dao.impl;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.form.dao.FormItemDao;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单项DaoImpl
 * User: 许林
 * Date: 13-9-12
 * Time: 下午12:55
 * To change this template use File | Settings | File Templates.
 */
@Component("formItemDao")
public class FormItemDaoImpl extends MyBatisDaoImpl<FormItem,String> implements FormItemDao {
	
	/**
	 * 根据formId获取表单详细项目信息
	 */
	public List<FormItem> selectByFormId(String formId){
		List<FormItem> formItem = null;
		try {
			formItem =  this.getSqlSession().selectList("com.jd.oa.form.model.FormItem.selectByFormId",formId);
		} catch (DataAccessException e) {
			throw e;
		}
		return formItem;
	}
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2014-3-20 下午04:02:13
	 *
	 * @param formId
	 * @return
	 */
	public List<FormItem> selectEditItemByFormIdAndNodeId(Map<String, Object> map){
		List<FormItem> formItem = null;
		try {
			formItem =  this.getSqlSession().selectList("com.jd.oa.form.model.FormItem.selectEditItemByFormIdAndNodeId",map);
		} catch (DataAccessException e) {
			throw e;
		}
		return formItem;
	}

    public List<FormItem> selectItemByFormIdAndFieldName(Map<String, Object> map){
        List<FormItem> formItem = null;
        try {
            formItem =  this.getSqlSession().selectList("com.jd.oa.form.model.FormItem.selectItemByFormIdAndFieldName",map);
        } catch (DataAccessException e) {
            throw e;
        }
        return formItem;
    }
    
    public List<FormItem> findFromAndSubItems(Map<String, Object> map){
    	List<FormItem> formItem = null;
    	try {
			formItem =  this.getSqlSession().selectList("com.jd.oa.form.model.FormItem.findFormAndSubItems",map);
		} catch (DataAccessException e) {
			throw e;
		}
    	return formItem;
    }

}
