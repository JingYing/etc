/**
 * 
 */
package com.jd.oa.form.dao;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormItem;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单项Dao
 * User: 许林
 * Date: 13-9-12
 * Time: 上午11:57
 * To change this template use File | Settings | File Templates.
 */
public interface FormItemDao extends BaseDao<FormItem,String> {
	
	/**
	 * 根据formId获取表单详细项目信息
	 */
	public List<FormItem> selectByFormId(String formId);
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2014-3-20 下午04:04:23
	 *
	 * @param map
	 * @return
	 */
	public List<FormItem> selectEditItemByFormIdAndNodeId(Map<String, Object> map);

    public List<FormItem> selectItemByFormIdAndFieldName(Map<String, Object> map);
    
    public List<FormItem> findFromAndSubItems(Map<String, Object> map);
}
