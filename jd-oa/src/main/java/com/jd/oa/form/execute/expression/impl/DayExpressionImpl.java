package com.jd.oa.form.execute.expression.impl;

import java.sql.Timestamp;
import java.util.Map;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
/**
 * 天表达式
 * @author birkhoff
 *
 */
public class DayExpressionImpl extends ExpressionAbst{

	public DayExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	public String expressionParse(String expression) {
		return DateUtils.dayFormat(new Timestamp(System.currentTimeMillis()));
	}
}
