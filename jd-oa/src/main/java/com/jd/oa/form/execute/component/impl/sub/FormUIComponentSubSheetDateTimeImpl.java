package com.jd.oa.form.execute.component.impl.sub;

import java.util.List;

import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.form.execute.component.FormUISubSheetComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentSubSheetDateTimeImpl extends FormUISubSheetComponentAbst{

	public FormUIComponentSubSheetDateTimeImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
//	@Override
//	public String getFormate(){
//		return "";
//	}
	@Override
	public String getEditor(){
		return "editor:'datetimebox'";
	}
}
