package com.jd.oa.form.constant;

/**
 * Created with IntelliJ IDEA.
 * User: dbdongjian
 * Date: 13-9-16
 * Time: 下午5:28
 * To change this template use File | Settings | File Templates.
 */
public class FormBusinessTableConstant {

    /**
     * 数据表创建类型
     */
    public static final String TableTypeKey = "tableType";

    /**
     * 数据库表UI展现类型
     */
    public static final String InputTypeKey = "formControlType";

    /**
     * 数据库表字段类型
     */
    public static final String dsFiledTypeKey = "dsFiledType";

}
