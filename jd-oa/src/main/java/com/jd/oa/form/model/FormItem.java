package com.jd.oa.form.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import net.sf.ehcache.search.impl.SearchManager;

public class FormItem implements Serializable{
	private static final long serialVersionUID = 1L;

    /**
     * T_JDOA_FORM_ITEM.ID
     * 表单字段ID
     */
    private String id;
    /**
     * T_JDOA_FORM_ITEM.FORM_ID
     * 表单字段对应表单定义表T_JDOA_FORM.ID
     */
    private String formId;
    /**
     * T_JDOA_FORM_ITEM.FIELD_NAME
     * 表单字段编码
     */
    private String fieldName;
    /**
     * T_JDOA_FORM_ITEM.FIELD_CHINESE_NAME
     * 表单字段名称
     */
    private String fieldChineseName;
    /**
     * T_JDOA_FORM_ITEM.TABLE_TYPE
     * TABLE在表单中的应用类型：m 主表；s 子表
     */
    private String tableType;
    /**
     * T_JDOA_FORM_ITEM.FIELD_ID
     * 字段ID
     */
    private String fieldId;
    /**
     * T_JDOA_FORM_ITEM.DATA_TYPE
     * 字段ID
     */
    private String dataType;
    /**
     * T_JDOA_FORM_ITEM.LENGTH
     * 长度
     */
    private String length;
    /**
     * T_JDOA_FORM_ITEM.DEFAULT_VALUE
     * 默认值
     */
    private String defaultValue;
    /**
     * T_JDOA_FORM_ITEM.INPUT_TYPE
     * 输入类型
     */
    private String inputType;
    /**
     * T_JDOA_FORM_ITEM.INPUT_TYPE_CONFIG
     * 输入类型详细配置
     */
    private String inputTypeConfig;
    /**
     * T_JDOA_FORM_ITEM.DICT_TYPE_CODE
     * 字典类型编码
     */
    private String dictTypeCode;
    /**
     * T_JDOA_FORM_ITEM.SORT_NO
     * 序号
     */
    private Float sortNo;
    /**
     * T_JDOA_FORM_ITEM.VALIDATE_RULE
     * 校验规则
     */
    private String validateRule;
    /**
     * T_JDOA_FORM_ITEM.YN
     * 逻辑删除标志位
     */
    private int yn;
    /**
     * T_JDOA_FORM_ITEM.COMPUTE_EXPRESS
     * 计算表达式
     */
    private String computeExpress;
    /**
     * T_JDOA_FORM_ITEM.COMPUTE_EXPRESS_DESC
     * 计算表达式
     */
    private String computeExpressDesc;
	/**
     * T_JDOA_FORM_ITEM.CREATOR
     * 创建者
     */
    private String creator;
    /**
     * T_JDOA_FORM_ITEM.CREATE_TIME
     * 创建时间
     */
    private Date createTime;
    /**
     * T_JDOA_FORM_ITEM.MODIFIER
     * 最近修改者
     */
    private String modifier;
    /**
     * T_JDOA_FORM_ITEM.MODIFY_TIME
     * 最近修改时间
     */
    private Date modifyTime;
    
    private List<FormItem> subtables;

    private String isHidden;
    private String isEdit;
    private String isNull;
    private String isVariable;
    private String processNodeFormPrivilegeId;
    
    private String htmlInner;
    
    public String getHtmlInner() {
		return htmlInner;
	}

	public void setHtmlInner(String htmlInner) {
		this.htmlInner = htmlInner;
	}

	public String getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(String hidden) {
        isHidden = hidden;
    }

    public String getIsEdit() {
        return isEdit;
    }

    public void setIsEdit(String edit) {
        isEdit = edit;
    }

    public String getIsNull() {
        return isNull;
    }

    public void setIsNull(String aNull) {
        isNull = aNull;
    }

    public String getIsVariable() {
        return isVariable;
    }

    public void setIsVariable(String variable) {
        isVariable = variable;
    }

    public String getProcessNodeFormPrivilegeId() {
        return processNodeFormPrivilegeId;
    }

    public void setProcessNodeFormPrivilegeId(String processNodeFormPrivilegeId) {
        this.processNodeFormPrivilegeId = processNodeFormPrivilegeId;
    }

    public List<FormItem> getSubtables() {
		return subtables;
	}
	public void setSubtables(List<FormItem> subtables) {
		this.subtables = subtables;
	}

	public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    public String getFormId() {
        return formId;
    }


    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }

    public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldChineseName() {
		return fieldChineseName;
	}

	public void setFieldChineseName(String fieldChineseName) {
		this.fieldChineseName = fieldChineseName;
	}

	public String getTableType() {
        return tableType;
    }


    public void setTableType(String tableType) {
        this.tableType = tableType == null ? null : tableType.trim();
    }


    public String getFieldId() {
        return fieldId;
    }


    public void setFieldId(String fieldId) {
        this.fieldId = fieldId == null ? null : fieldId.trim();
    }


    public String getLength() {
        return length;
    }

    public String getDataType() {
		return dataType;
	}


	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public void setLength(String length) {
        this.length = length;
    }

    public String getDefaultValue() {
        return defaultValue;
    }


    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue == null ? null : defaultValue.trim();
    }


    public String getInputType() {
        return inputType;
    }


    public void setInputType(String inputType) {
        this.inputType = inputType == null ? null : inputType.trim();
    }


    public String getInputTypeConfig() {
        return inputTypeConfig;
    }


    public void setInputTypeConfig(String inputTypeConfig) {
        this.inputTypeConfig = inputTypeConfig == null ? null : inputTypeConfig
                .trim();
    }


    public String getDictTypeCode() {
        return dictTypeCode;
    }


    public void setDictTypeCode(String dictTypeCode) {
        this.dictTypeCode = dictTypeCode == null ? null : dictTypeCode.trim();
    }


    public Float getSortNo() {
        return sortNo;
    }


    public void setSortNo(Float sortNo) {
        this.sortNo = sortNo;
    }


    public String getValidateRule() {
        return validateRule;
    }


    public void setValidateRule(String validateRule) {
        this.validateRule = validateRule == null ? null : validateRule.trim();
    }


    public int getYn() {
        return yn;
    }


    public void setYn(int yn) {
        this.yn = yn;
    }

    public String getComputeExpress() {
        return computeExpress;
    }


    public void setComputeExpress(String computeExpress) {
        this.computeExpress = computeExpress == null ? null : computeExpress
                .trim();
    }

    public String getComputeExpressDesc() {
  		return computeExpressDesc;
  	}

  	public void setComputeExpressDesc(String computeExpressDesc) {
  		this.computeExpressDesc = computeExpressDesc == null ? null : computeExpressDesc
                .trim();
  	}
  	
    public String getCreator() {
        return creator;
    }


    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }


    public Date getCreateTime() {
        return createTime;
    }


    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String getModifier() {
        return modifier;
    }


    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }


    public Date getModifyTime() {
        return modifyTime;
    }


    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}