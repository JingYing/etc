package com.jd.oa.form.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jd.oa.common.utils.SpringContextUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.SqlType;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.form.dao.FormBusinessFieldDao;
import com.jd.oa.form.dao.FormBusinessTableDao;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.service.FormBusinessFieldService;

/**
 * @author guoqingfu
 * @version V1.0
 * @Description: 自定义表字段 Service实现
 * @date 2013-9-12 上午10:49:56
 */
@Service("formBusinessFieldService")
public class FormBusinessFieldServiceImpl extends BaseServiceImpl<FormBusinessField, String>
        implements FormBusinessFieldService {

    private static final Logger logger = Logger.getLogger(FormBusinessFieldServiceImpl.class);

    @Autowired
    private FormBusinessFieldDao formBusinessFieldDao;

    @Autowired
    private FormBusinessTableDao formBusinessTableDao;

    JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");

    public FormBusinessFieldDao getDao() {
        return formBusinessFieldDao;
    }

    /**
     * @param formBusinessFieldId
     * @return FormBusinessField
     * @Description: 根据ID查询自定义表字段信息
     * @author guoqingfu
     * @date 2013-9-13上午10:11:33
     * @version V1.0
     */
    public FormBusinessField getFormBusinessFieldInfo(String formBusinessFieldId) {
        try {
            return formBusinessFieldDao.getFormBusinessFieldInfo(formBusinessFieldId);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据ID查询自定义表字段信息失败", e);
        }
    }

    /**
     * @param formBusinessFieldIds
     * @return Map<String,Object>
     * @Description: 删除自定义表字段
     * @author guoqingfu
     * @date 2013-9-13上午11:03:25
     * @version V1.0
     */
    @Transactional
    public Map<String, Object> deleteFormBusinessField(List<String> formBusinessFieldIds) {
        Map<String, Object> map = new HashMap<String, Object>();
        boolean flag = false;
        map.put("formBusinessFieldIds", formBusinessFieldIds);
        try {
            flag = formBusinessFieldDao.isUsedByForm(map);
            if (flag) {
                map.put("operator", false);
                map.put("message", "已被表单引用,删除失败");
                return map;
            }
            flag = formBusinessFieldDao.deleteFormBusinessField(map);
            if (flag) {
                map.put("operator", true);
                map.put("message", "删除成功");
            } else {
                map.put("operator", false);
                map.put("message", "删除失败");
            }
            return map;
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("删除失败", e);
        }
    }

    /**
     * @param bussinessFieldId
     * @return Map<String,Object>
     * @Description: 判断字段是否已被表单使用
     * @author guoqingfu
     * @date 2013-9-27上午11:37:46
     * @version V1.0
     */
    public Map<String, Object> isUsedByForm(String bussinessFieldId) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<String> formBusinessFieldIds = new ArrayList<String>();
        formBusinessFieldIds.add(bussinessFieldId);
        map.put("formBusinessFieldIds", formBusinessFieldIds);
        try {
            boolean flag = formBusinessFieldDao.isUsedByForm(map);
            if (flag) {
                map.put("operator", false);
                map.put("message", "字段已被表单引用,不可修改");
                return map;
            } else {
                map.put("operator", true);
                map.put("message", "字段可以被修改");
                return map;
            }
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询字段是否可被修改失败", e);
        }

    }

    /**
     * @param formBusinessField
     * @return boolean
     * @Description: 判断是否存在自定义表字段名
     * @author guoqingfu
     * @date 2013-9-13下午01:39:41
     * @version V1.0
     */
    public boolean isExistFieldName(FormBusinessField formBusinessField) {
        try {
            return formBusinessFieldDao.isExistFieldName(formBusinessField);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询是否存在字段名失败", e);
        }
    }

    /**
     * @param formBusinessField
     * @return boolean
     * @Description: 判断是否存在自定义表字段显示名
     * @author guoqingfu
     * @date 2013-9-13下午02:07:28
     * @version V1.0
     */
    public boolean isExistFieldChineseName(FormBusinessField formBusinessField) {
        try {
            return formBusinessFieldDao.isExistFieldChineseName(formBusinessField);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询是否存在字段名失败", e);
        }
    }

    /**
     * @param formBusinessField
     * @return Map<String,Object>
     * @Description: 判断是否存在字段名或是字段显示名
     * @author guoqingfu
     * @date 2013-9-13下午01:35:03
     * @version V1.0
     */
    public Map<String, Object> isExistTable(FormBusinessField formBusinessField) {
        Map<String, Object> map = new HashMap<String, Object>();
        boolean flag = false;
        //判断是否存在字段名
        flag = this.isExistFieldName(formBusinessField);
        if (flag) {
            map.put("operator", true);
            map.put("message", "字段名称已存在");
            return map;
        }
        //判断是否存在字段显示名
        flag = this.isExistFieldChineseName(formBusinessField);
        if (flag) {
            map.put("operator", true);
            map.put("message", "字段显示名称已存在");
            return map;
        }
        map.put("operator", false);
        map.put("message", "不存在字段名和字段显示名");
        return map;
    }

    /**
     * @param businessTableId
     * @return Map<String,Object>
     * @Description: 创建物理表
     * @author guoqingfu
     * @date 2013-9-17下午01:11:25
     * @version V1.0
     */
    @Transactional
    public Map<String, Object> createBusinessTable(String businessTableId) {
        Map<String, Object> map = new HashMap<String, Object>();
        SqlAdapter adapter = null;
        try {
            if (StringUtils.isEmpty(businessTableId)) {
                map.put("operator", false);
                map.put("message", "表ID不能为空");
                return map;
            }
            //根据业务表ID查询表和字段信息
            List<FormBusinessField> businessTableList = formBusinessFieldDao.findTableFieldList(businessTableId);

            if (businessTableList == null || businessTableList.size() <= 0) {
                map.put("operator", false);
                map.put("message", "表字段不能为空");
                return map;
            }

            //判断当前表是否已被创建过,如果创建过要删除
            String tableName = businessTableList.get(0).getTableName();
            boolean flag = isExistBusinessTable(tableName);
            if (flag) {
                List<FormBusinessField> addBusinessTableList = new ArrayList<FormBusinessField>();
                List<FormBusinessField> updateBusinessTableList = new ArrayList<FormBusinessField>();
                if (isExistData(tableName)) {
//                    map.put("operator", false);
//                    map.put("message", "该表已经存在数据,不允许再次生成");
                    StringBuffer sqlQuery = new StringBuffer();

                    sqlQuery.append(" select column_name from information_schema.`COLUMNS`");
                    sqlQuery.append("  where  ");
                    sqlQuery.append("  TABLE_SCHEMA='jdoa'");
                    sqlQuery.append("  and TABLE_NAME='" + tableName + "'");
                    List<Map<String, Object>> rows = jdbcTemplate.queryForList(sqlQuery.toString());
                    if (rows != null && rows.size() > 0) {
                        for (FormBusinessField fbf : businessTableList) {
                            boolean existFlag = false;

                            Map columnMap = (Map) rows.get(0);
                            for (Map<String, Object> m : rows) {
                                if (m.get("column_name").toString().equals(fbf.getFieldName())) {
                                    existFlag = true;
                                    updateBusinessTableList.add(fbf);
                                }
                            }
                            if (!existFlag) {
                                addBusinessTableList.add(fbf);
                            }
                        }
                        updateBusinessTable(tableName, addBusinessTableList, updateBusinessTableList);
//                        String columnName = map.get("column_name").toString();

                    }


                    return map;
                }
                adapter = new MySqlAdapter();
                adapter.setSqlType(SqlType.DDL_DROP_TABLE).addTable(tableName);
                sql(adapter);
            }

            //创建数据表Begin
            adapter = new MySqlAdapter();
            adapter.setSqlType(SqlType.DDL_CREATE_TABLE).addTable(tableName);
            adapter.addColumn(null, "ID", DataType.STRING, "32", Boolean.TRUE, Boolean.FALSE, null, "主键");
            //如果是子表，增加父表ID字段
            String tableType = businessTableList.get(0).getTableType();
            if (StringUtils.isNotEmpty(tableType) && "s".equalsIgnoreCase(tableType)) {
                adapter.addColumn(null, "PARENT_ID", DataType.STRING, "32", Boolean.FALSE, Boolean.TRUE, null, "父表ID");
            }

            for (int i = 0; i < businessTableList.size(); i++) {
                FormBusinessField formBusinessField = businessTableList.get(i);
                String fieldName = formBusinessField.getFieldName();
                String fieldChineseName = formBusinessField.getFieldChineseName();
                String dataType = formBusinessField.getDataType();
                String isNull = formBusinessField.getIsNull();
                String length = formBusinessField.getLength();
                //长度都放在页面验证，去掉后台默认值，组内审核时要求
                if (StringUtils.isNotEmpty(length) && !"null".equalsIgnoreCase(length)) {
                    length = length.replaceAll(" ", "");
                }
//				else{
//					length = "1";
//				}
                //页面中的默认值指的是表单的的默认值，不是建表的默认值
                //String defaultValue = formBusinessField.getDefaultValue();

                adapter.addColumn(null, fieldName, this.getDataType(dataType), length, Boolean.FALSE, this.getIsNull(isNull), null, fieldChineseName);
            }
            sql(adapter);
            //创建数据表End
            map.put("operator", true);
            map.put("message", "创建物理表成功");
            return map;
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("创建物理表失败", e);
        }
    }

    /**
     * @param businessTableId
     * @return Map<String,Object>
     * @Description: 创建物理表(包括子表)
     * @author guoqingfu
     * @date 2013-9-25上午11:15:37
     * @version V1.0
     */
    @Transactional
    public Map<String, Object> createTableIncludeChild(String businessTableId) {
        Map<String, Object> map = new HashMap<String, Object>();
        SqlAdapter adapter = null;
        try {
            if (StringUtils.isEmpty(businessTableId) || "null".equalsIgnoreCase(businessTableId)) {
                map.put("operator", false);
                map.put("message", "表ID不能为空");
                return map;
            }
            //根据业务表ID查询表和字段信息(包括子表)
            Map<String, List<FormBusinessField>> fieldMap = this.findFieldListIncludeChild(businessTableId);

            if (fieldMap == null || fieldMap.size() <= 0) {
                map.put("operator", false);
                map.put("message", "表字段不能为空");
                return map;
            }

            for (Iterator<String> ite = fieldMap.keySet().iterator(); ite.hasNext(); ) {
                String tableName = ite.next();
                List<FormBusinessField> businessTableList = fieldMap.get(tableName);

                //判断当前表是否已被创建过,如果创建过要删除
                boolean flag = isExistBusinessTable(tableName);
                if (flag) {
                    if (isExistData(tableName)) {
                        map.put("operator", false);
                        map.put("message", "该表已经存在数据,不允许再次生成");
                        return map;
                    }
                    adapter = new MySqlAdapter();
                    adapter.setSqlType(SqlType.DDL_DROP_TABLE).addTable(tableName);
                    sql(adapter);
                }
                //创建数据表Begin
                adapter = new MySqlAdapter();
                adapter.setSqlType(SqlType.DDL_CREATE_TABLE).addTable(tableName);
                adapter.addColumn(null, "ID", DataType.STRING, "32", Boolean.TRUE, Boolean.FALSE, null, "主键");
                //如果是子表，增加父表ID字段
                String tableType = businessTableList.get(0).getTableType();
                if (StringUtils.isNotEmpty(tableType) && "s".equalsIgnoreCase(tableType)) {
                    adapter.addColumn(null, "PARENT_ID", DataType.STRING, "32", Boolean.FALSE, Boolean.TRUE, null, "父表ID");
                }
                for (int i = 0; i < businessTableList.size(); i++) {
                    FormBusinessField formBusinessField = businessTableList.get(i);
                    String fieldName = formBusinessField.getFieldName();
                    String fieldChineseName = formBusinessField.getFieldChineseName();
                    String dataType = formBusinessField.getDataType();
                    String isNull = formBusinessField.getIsNull();
                    String length = formBusinessField.getLength();
                    if (StringUtils.isNotEmpty(length) && !"null".equalsIgnoreCase(length)) {
                        length = length.replaceAll(" ", "");
                    } else {
                        length = "1";
                    }
                    //页面中的默认值指的是表单的的默认值，不是建表的默认值
                    //String defaultValue = formBusinessField.getDefaultValue();

                    adapter.addColumn(null, fieldName, this.getDataType(dataType), length, Boolean.FALSE, this.getIsNull(isNull), null, fieldChineseName);
                }
                sql(adapter);
                //创建数据表End
            }
            map.put("operator", true);
            map.put("message", "创建物理表成功");
            return map;
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("创建物理表失败", e);
        }
    }

    /**
     * @param businessTableId
     * @return Map<String,List<FormBusinessField>>
     * @Description: 根据业务表ID查询表和字段信息(包括子表), 同时按表名进行分组
     * @author guoqingfu
     * @date 2013-9-25上午11:26:58
     * @version V1.0
     */
    private Map<String, List<FormBusinessField>> findFieldListIncludeChild(String businessTableId) {
        //根据业务表ID查询表和字段信息(包括子表)
        try {
            List<FormBusinessField> fieldList = formBusinessFieldDao.findFieldListIncludeChild(businessTableId);
            Iterator<FormBusinessField> ite = fieldList.iterator();
            Map<String, List<FormBusinessField>> map = new HashMap<String, List<FormBusinessField>>();
            while (ite.hasNext()) {
                FormBusinessField formBusinessField = ite.next();
                String tableName = formBusinessField.getTableName();
                if (map.containsKey(tableName)) {
                    map.get(tableName).add(formBusinessField);
                } else {
                    List<FormBusinessField> list = new ArrayList<FormBusinessField>();
                    list.add(formBusinessField);
                    map.put(tableName, list);
                }
            }
            return map;
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据业务表ID查询表和字段信息(包括子表)失败", e);
        }
    }

    /**
     * @param tableName
     * @return boolean
     * @Description: 判断当前表是否已被创建过
     * @author guoqingfu
     * @date 2013-9-23下午08:31:04
     * @version V1.0
     */
    public boolean isExistBusinessTable(String tableName) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("tableSchema", SystemConstant.TABLESCHEMA);
            map.put("tableName", tableName);
            return formBusinessFieldDao.isExistBusinessTable(map);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询是否已创建业务表失败", e);
        }
    }

    /**
     * @param tableNameList
     * @return List<String>
     * @Description: 查询被创建过的业务表名
     * @author guoqingfu
     * @date 2013-10-22下午05:56:41
     * @version V1.0
     */
    public List<String> findCreatedTable(List<String> tableNameList) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("tableSchema", SystemConstant.TABLESCHEMA);
            map.put("tableNameList", tableNameList);
            return formBusinessFieldDao.findCreatedTable(map);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询被创建过的业务表名失败", e);
        }
    }

    /**
     * @param tableName
     * @return boolean
     * @Description: 根据表名查看该表是否已经有数据
     * @author guoqingfu
     * @date 2013-9-27上午10:42:27
     * @version V1.0
     */
    public boolean isExistData(String tableName) {
        try {
            return formBusinessFieldDao.isExistData(tableName);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据表名查看该表是否已经有数据失败", e);
        }
    }

    /**
     * @param isNull
     * @return boolean
     * @Description: 判断是否可以为空
     * @author guoqingfu
     * @date 2013-9-25下午05:48:59
     * @version V1.0
     */
    private boolean getIsNull(String isNull) {
        if ("1".equals(isNull)) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    /**
     * @param dataType
     * @return DataType
     * @Description: 封装数据类型
     * @author guoqingfu
     * @date 2013-9-23上午11:03:11
     * @version V1.0
     */
    private DataType getDataType(String dataType) {
        if ("NUMBER".equalsIgnoreCase(dataType)) {
            return DataType.NUMBER;
        } else if ("DATE".equalsIgnoreCase(dataType)) {
            return DataType.DATE;
        } else if ("TIME".equalsIgnoreCase(dataType)) {
            return DataType.TIME;
        } else if ("DATETIME".equalsIgnoreCase(dataType)) {
            return DataType.DATETIME;
        } else {
            //如果都不匹配，默认返回文本类型
            return DataType.STRING;
        }
    }

    /**
     * @param businessTableId
     * @return List<FormBusinessField>
     * @Description: 高级模式创建表单时 根据自定义表ID查询未选择的字段信息
     * @author 许林
     * @date 2013-9-22下午01:54:17
     * @version V1.0
     */
    public List<FormBusinessField> findTableFieldListNotSelect(String businessTableId) {
        return this.formBusinessFieldDao.findTableFieldListNotSelect(businessTableId);
    }

    /**
     * @param tableId
     * @return List<FormBusinessField>
     * @Description: 根据tableId查询字段信息
     * @author xulin
     * @date 2013-9-25上午10:44:15
     * @version V1.0
     */
    public List<FormBusinessField> selectTableFieldsByTableId(String tableId) {
        return this.formBusinessFieldDao.selectTableFieldsByTableId(tableId);
    }

    /**
     * 对于已经有数据的业务表添加 或者修改 字段
     *
     * @param tableName
     * @param addFieldsList
     * @param updateFieldsList
     * @return
     */
    public Map<String, Object> updateBusinessTable(String tableName, List<FormBusinessField> addFieldsList, List<FormBusinessField> updateFieldsList) {
        Map<String, Object> map = new HashMap<String, Object>();
        SqlAdapter adapter = null;
        String addLog = "";
        StringBuilder addLogSb = new StringBuilder("");
        String updateLog = "";
        StringBuilder updateLogSb = new StringBuilder("");
        try {
            if (StringUtils.isNotBlank(tableName)) {//数据库表同步数据库的时候
                if (addFieldsList.size() > 0) {
                    adapter = new MySqlAdapter();
                    adapter.setSqlType(SqlType.DDL_ADD_COLUMN).addTable(tableName);
                    for (FormBusinessField af : addFieldsList) {
                        adapter.addColumn(null, af.getFieldName(), this.getDataType(af.getDataType()), af.getLength(), Boolean.FALSE, this.getIsNull(af.getIsNull()), null, af.getFieldChineseName());
//                        addLog+=af.getFieldChineseName()+"("+af.getFieldName()+") ";
                        addLogSb.append(af.getFieldChineseName()+"("+af.getFieldName()+") ");
                    }
                    sql(adapter);
                    logger.info(adapter.toSql());
                }
                if (updateFieldsList.size() > 0) {
                    adapter = new MySqlAdapter();
                    adapter.setSqlType(SqlType.DDL_CHANGE_COLUMN).addTable(tableName);
                    for (FormBusinessField uf : updateFieldsList) {
                        adapter.addColumn(uf.getFieldName(), uf.getFieldName(), this.getDataType(uf.getDataType()), uf.getLength(), Boolean.FALSE, this.getIsNull(uf.getIsNull()), null, uf.getFieldChineseName());
//                        updateLog +=uf.getFieldChineseName()+"("+uf.getFieldName()+") ";
                        updateLogSb.append(uf.getFieldChineseName()+"("+uf.getFieldName()+") ");
                    }
                    sql(adapter);
                    logger.info(adapter.toSql());
                }

            } else {//非高级模式
                if (addFieldsList.size() > 0) {
                    for (FormBusinessField af : addFieldsList) {
                        adapter = new MySqlAdapter();
                        adapter.setSqlType(SqlType.DDL_ADD_COLUMN).addTable(formBusinessTableDao.get(af.getTableId()).getTableName());
                        adapter.addColumn(null, af.getFieldName(), this.getDataType(af.getDataType()), af.getLength(), Boolean.FALSE, this.getIsNull(af.getIsNull()), null, af.getFieldChineseName());
                        sql(adapter);
                        logger.info(adapter.toSql());
//                        addLog+=af.getFieldChineseName()+"("+af.getFieldName()+") ";
                        addLogSb.append(af.getFieldChineseName()+"("+af.getFieldName()+") ");
                    }
                }
                if (updateFieldsList.size() > 0) {
                    for (FormBusinessField uf : updateFieldsList) {
                        adapter = new MySqlAdapter();
                        adapter.setSqlType(SqlType.DDL_CHANGE_COLUMN).addTable(formBusinessTableDao.get(uf.getTableId()).getTableName());
                        adapter.addColumn(uf.getFieldName(), uf.getFieldName(), this.getDataType(uf.getDataType()), uf.getLength(), Boolean.FALSE, this.getIsNull(uf.getIsNull()), null, uf.getFieldChineseName());
                        sql(adapter);
                        logger.info(adapter.toSql());
//                        updateLog +=uf.getFieldChineseName()+"("+uf.getFieldName()+") ";
                        updateLogSb.append(uf.getFieldChineseName()+"("+uf.getFieldName()+") ");
                    }
                }

            }
            map.put("operator", true);
            addLog =  addLogSb.toString();
            updateLog = updateLogSb.toString();
            
            if(addLog.equals("")&&updateLog.equals("")){
                map.put("message", "无表单项需要同步");
            }else{
                String message = "更改物理表成功";
                if(!addLog.equals("")){
                    message+="新增列："+addLog;
                }
                if(!updateLog.equals("")){
                    message+="更新列："+updateLog;
                }
                map.put("message", message);
            }

            return map;
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("更改物理表失败", e);
        }
    }
}
