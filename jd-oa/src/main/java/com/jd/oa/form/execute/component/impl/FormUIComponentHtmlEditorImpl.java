package com.jd.oa.form.execute.component.impl;

import java.util.Map;

import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.util.Html;

public class FormUIComponentHtmlEditorImpl extends FormUIComponentAbst{
	public FormUIComponentHtmlEditorImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}
	@Override
	public String getReadHtmlDefine(Map<String,Object> params) {
		StringBuffer fieldHtml = new StringBuffer();
		String inputTypeConfig = "";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() >0){
			inputTypeConfig =getMetaDataMapModel().getInputTypeConfig();
		}
		fieldHtml.append("<textarea id='").append(getMetaDataMapModel().getFieldName()).append("' style='width:").append(inputTypeConfig).append("' onkeypress='if(event.keyCode==13||event.which==13){return false;}' >");
		fieldHtml.append(Html.escape(getValue()));
		fieldHtml.append("</textarea>");
		fieldHtml.append(" <script type=\"text/javascript\">\n");
		fieldHtml.append("$(function() {\n");
		fieldHtml.append("KindEditor.ready(function(K) {\n");
		fieldHtml.append("editor = K.create('textarea[id=\"").append(getMetaDataMapModel().getFieldName()).append("\"]', {\n");
		fieldHtml.append(" resizeType : 0,\n");
		fieldHtml.append(" filterMode : false\n");
		fieldHtml.append("});\n");
		fieldHtml.append(" });\n");
		fieldHtml.append("});\n");
		fieldHtml.append("</script> \n");
		return fieldHtml.toString();
	}
	@Override
	public String getModifyHtmlDefine(Map<String,Object> params) {
		StringBuffer fieldHtml = new StringBuffer();
		String inputTypeConfig = "";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() >0){
			inputTypeConfig =getMetaDataMapModel().getInputTypeConfig();
		}
		fieldHtml.append("<textarea id='").append(getMetaDataMapModel().getFieldName()).append("' style='width:").append(inputTypeConfig).append("' onkeypress='if(event.keyCode==13||event.which==13){return false;}' >");
		fieldHtml.append(Html.escape(getValue()));
		fieldHtml.append("</textarea>");
		if(!isNull()){
			fieldHtml.append("<font color=red>*</font>");
		}
		fieldHtml.append(" <script type=\"text/javascript\">\n");
		fieldHtml.append("$(function() {\n");
		fieldHtml.append("KindEditor.ready(function(K) {\n");
		fieldHtml.append("editor = K.create('textarea[id=\"").append(getMetaDataMapModel().getFieldName()).append("\"]', {\n");
		fieldHtml.append(" resizeType : 0,\n");
		fieldHtml.append(" filterMode : false\n");
		fieldHtml.append("});\n");
		fieldHtml.append(" });\n");
		fieldHtml.append("});\n");
		fieldHtml.append("</script> \n");
		return fieldHtml.toString();
	}
	public String getSettingWeb() {
		StringBuffer settingHtml = new StringBuffer();
		String displayValue = getMetaDataMapModel().getInputTypeConfig();
		settingHtml.append("<input type=text id='htmlHeight' name='htmlHeight' size=30 title='请指定HTML排版的高度' onkeypress='if(event.keyCode==13||event.which==13){return false;}'  value='").append(displayValue).append("'/>指定HTML排版的高度");
		settingHtml.append("").append("\n");
		settingHtml.append("").append("\n");
		
		settingHtml.append("<script>");
		settingHtml.append("function returnSettingConfig(){\n");
		settingHtml.append("	return document.getElementById('htmlHeight').value;\n");
	    settingHtml.append("}\n"); 
	    settingHtml.append("</script>\n"); 
		return settingHtml.toString();
	}
	@Override
	public String getTrueValue() {
		return getValue();
	}
}
