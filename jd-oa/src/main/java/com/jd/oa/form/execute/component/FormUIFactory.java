package com.jd.oa.form.execute.component;

import java.lang.reflect.Constructor;
import java.util.*;

import net.sf.ehcache.CacheManager;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.ReflectionUtils;
import com.jd.oa.form.execute.component.impl.FormUIComponentNoneImpl;
import com.jd.oa.form.execute.component.model.ConfigComponentModel;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIFactory {
	private static Map<String,ConfigComponentModel> _componentList = null;

	private static Map<Integer,ConfigComponentModel> _componentSortList = null;

	private static Map<String,Object> _componentGroup = null;

	private static String contextPath;
	
	private static FormUIFactory _instance;
	public static FormUIFactory getInstance(String contextPath){
		if(_instance == null){
			_instance = new FormUIFactory(contextPath);
		}
		return _instance;
	}
	private FormUIFactory(String contextPath){
		this.contextPath = contextPath;
		loadComponent();
	}

	/**
	 * 根据UI组件类型返回其UI组件实例
	 * 
	 * @param UIType
	 * 
	 */
	public UIComponentInterface getUIInstance(FormBusinessField metaDataMapModel, String value) {
		ConfigComponentModel configComponentModel = (ConfigComponentModel) _componentList.get(metaDataMapModel.getInputType());
		if (configComponentModel != null) {
			Constructor cons = null;
			UIComponentInterface ui;
			try {
				cons = configComponentModel.getCons();
				if (cons != null) {
					Object[] params = { metaDataMapModel, value };
					ui = (UIComponentInterface) cons.newInstance(params);
					return ui;
				}
			} catch (InstantiationException ie) {
				System.err.println("在FormUIFactory中,定义的组件类[" + configComponentModel.getImplementsClass() + "]构造实例时出错!");
			} catch (java.lang.IllegalAccessException le) {
				System.err.println("在FormUIFactory中,定义的组件类[" + configComponentModel.getImplementsClass() + "]抛出IllegalAccessException!");
			} catch (java.lang.reflect.InvocationTargetException invoke) {
				System.err.println("在FormUIFactory中,定义的组件类[" + configComponentModel.getImplementsClass() + "]抛出InvocationTargetException!");
			} catch (Exception e) {
				System.err.println("在FormUIFactory中,定义的组件类[" + configComponentModel.getImplementsClass() + "]抛出Exception!");
			}
		}

		// 尚未实现的新UI组件类型或加载异常默认显示的控件
		return new FormUIComponentNoneImpl(metaDataMapModel, value);
	}

	/**
	 * 重新加载UI组件库
	 * 
	 * 
	 */
	public static void reloadComponent() {
		loadComponent();
	}

	/**
	 * 获取表单的组件对象
	 * 
	 * 
	 * @return
	 */
	public Map<String,ConfigComponentModel> getComponentList() {
		return _componentList;
	}

	/**
	 * 获取表单的组件对象(按xml排序)
	 * 
	 * 
	 * @return
	 */
	public Map<Integer,ConfigComponentModel> getComponentSortList() {
		return _componentSortList;
	}

	/**
	 * 获取组件分类列表
	 * @return
	 */
	public Map<String,Object> getComponentGroupList() {
		return _componentGroup;
	}
	/**
	 * 获取组件模型Model
	 * @param componentId
	 */
	public ConfigComponentModel getComponentSortModel(String componentId){
		ConfigComponentModel configComponentModel=null;
		for(int i=0;i<_componentSortList.size();i++){
			 configComponentModel = (ConfigComponentModel) _componentSortList.get(Integer.valueOf(i));
			 if(configComponentModel!=null&&configComponentModel.getId().equals(componentId)){
				 return configComponentModel;
			 }
		}
		return null;
	}
	// 从eform-component.xml加载组件对象
	static void loadComponent() {
		_componentList = new HashMap<String,ConfigComponentModel>();
		_componentSortList = new HashMap<Integer,ConfigComponentModel>();
		_componentGroup = new HashMap<String,Object>();
		Class[] parameterTypes = { FormBusinessField.class, String.class };
		String xml = "eform-component.xml";
		SAXReader saxreader = new SAXReader();
		Document doc = DocumentFactory.getInstance().createDocument();
		try {
			doc =saxreader.read(contextPath + "WEB-INF" + System.getProperties().getProperty("file.separator") + "classes/" + xml);
			Iterator it = doc.getRootElement().elementIterator();
//			Hashtable hs = new Hashtable();
			it = doc.getRootElement().elementIterator("component");
			int i = 0;
			while (it.hasNext()) {
				Element element = (Element) it.next();
				if (element.getName().equals("component")) {
					Iterator iit = element.elementIterator();
					ConfigComponentModel configComponentModel = new ConfigComponentModel();
					while (iit.hasNext()) {
						Element ielement = (Element) iit.next();
						if (ielement.getName().equals("id"))
							configComponentModel.setId(ielement.getText());
						if (ielement.getName().equals("title"))
							configComponentModel.setTitle(ielement.getText());
						if (ielement.getName().equals("interface-class"))
							configComponentModel.setInterfaceClass(ielement.getText());
						if (ielement.getName().equals("implements-class"))
							configComponentModel.setImplementsClass(ielement.getText());
						if (ielement.getName().equals("desc"))
							configComponentModel.setDesc(ielement.getText());
						if (ielement.getName().equals("groupname"))
							configComponentModel.setGroupName(ielement.getText());
						if (ielement.getName().equals("seticon"))
							configComponentModel.setIcon(ielement.getText());
						if (ielement.getName().equals("setAltText")) {
							configComponentModel.setSetAlt(Boolean.parseBoolean(ielement.getText()));
						}
					}
					// 建立反射
					Constructor cons = null;
					try {
						cons = ReflectionUtils.getConstructor(configComponentModel.getImplementsClass(), parameterTypes);
					} catch (ClassNotFoundException ce) {
						System.err.println(" ClassLoader>>在FormUIFactory中,定义的组件类[" + configComponentModel.getImplementsClass() + "]没有找到!");
						// ce.printStackTrace(System.err);
					} catch (NoSuchMethodException ne) {
						System.err.println(" ClassLoader>>在FormUIFactory中,定义的组件类[" + configComponentModel.getImplementsClass() + "]构造方法不匹配!");
					} catch (Exception e) {
						System.err.println(" ClassLoader>>在FormUIFactory中,定义的组件类[" + configComponentModel.getImplementsClass() + "]构造方法不匹配!");
						
					}
					// 设置构造组件的构造函数对象
					configComponentModel.setCons(cons);
					if (cons == null && !configComponentModel.getImplementsClass().equals("")) {
						
					}
					_componentList.put(configComponentModel.getId(), configComponentModel);
					_componentSortList.put(Integer.valueOf(_componentSortList.size()), configComponentModel);
					// 是否属于一个新组
					if (!_componentGroup.containsValue(configComponentModel.getGroupName())) {
						_componentGroup.put(String.valueOf(_componentGroup.size()), configComponentModel.getGroupName());
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	public static void main(String[] args){
		loadComponent();
	}
}
