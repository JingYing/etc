package com.jd.oa.form.model.bean;

import java.io.Serializable;

public class FormItemBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7935542487236212318L;
	
	private String formId;//表单ID
	private String formName;//表单名称
	private String tableId;//对应的业务表ID
	private String fieldId;//表中的字段ID
	private String tableType;//表类型 M：主表 S：子表
	private String fieldName;//字段中文名称
	
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getFieldId() {
		return fieldId;
	}
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getTableType() {
		return tableType;
	}
	public void setTableType(String tableType) {
		this.tableType = tableType;
	}
}