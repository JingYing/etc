package com.jd.oa.form.execute.plugins.imp2exp.excel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCountCallbackHandler;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.plugins.imp2exp.ImpExpUtil;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;

/**
 * @author Administrator
 * 
 *         该类用来为表单提供Excel模板和导出功能
 * @preserve 声明此方法不被JOC混淆.
 */
public class ExportExcel {
	
	/**
	 * 获取指定表单子表的数据导入模板
	 * 
	 * @param me
	 * @param bindId
	 *            当前工作流实例id，触发器用
	 * @param sheetId
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public static String getExcelModelOfSubSheet(HttpServletRequest request, String processNodeId,String formId,String subSheetCode) {
		String fileName = "";
		FormBean subSheetFormBean = getSubSheetFormBean(formId, subSheetCode, processNodeId);
		if (subSheetFormBean.getListFormItems() != null && subSheetFormBean.getListFormItems().size() > 0) {
			HSSFWorkbook wb = new HSSFWorkbook();
			String formName = subSheetFormBean.getFormName();
			String formCode = subSheetFormBean.getFormCode();
			HSSFSheet sheet = wb.createSheet(formName);
			HSSFCell cell = null;
			HSSFRow row = null;
			HSSFFont font = wb.createFont();

			// 模板标题
			short rowIndex = 0;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			HSSFCellStyle aws_head_cell = wb.createCellStyle();
			font.setFontHeight((short) 400);
			aws_head_cell.setFont(font);
			cell.setCellStyle(aws_head_cell);
			cell.setCellValue("Excel数据导入模板");

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("数据源:" + formCode + "::" + formName);

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("导入动作:追加");

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("创建日期:" + (DateUtils.datetimeFormat(new Date())));

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("创建人:" + ComUtils.getLoginName());

			font = wb.createFont();
			
			HSSFCellStyle styleHead = wb.createCellStyle();
			
			styleHead.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
			styleHead.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			styleHead.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			styleHead.setBottomBorderColor(HSSFColor.BLACK.index);
			styleHead.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			styleHead.setLeftBorderColor(HSSFColor.BLACK.index);
			styleHead.setBorderRight(HSSFCellStyle.BORDER_THIN);
			styleHead.setRightBorderColor(HSSFColor.BLACK.index);
			styleHead.setBorderTop(HSSFCellStyle.BORDER_THIN);
			styleHead.setTopBorderColor(HSSFColor.BLACK.index);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.WHITE.index);
			styleHead.setFont(font);

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);
			cell.setCellStyle(styleHead);
			
			createExcelModelColumn(subSheetFormBean.getListFormItems(), sheet, cell, row, styleHead);
			fileName = createExcelModelFile(request,wb);
			
		}
		return fileName;
	}
	/**
	 * 获取指定表单子表的数据导入模板
	 * 
	 * @param me
	 * @param bindId
	 *            当前工作流实例id，触发器用
	 * @param sheetId
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public static String getExcelModelOfTable(HttpServletRequest request, String tableName) {
		String fileName = "";
		JdbcTemplate jdbcTemplate = SpringContextUtils.getBean("jdbcTemplate");
		String sql = "select * from "+ tableName;  
		RowCountCallbackHandler rcch = new RowCountCallbackHandler();  
		jdbcTemplate.query(sql, rcch);  
		String[] coloumnNames = rcch.getColumnNames();  
		if (coloumnNames != null && coloumnNames.length > 0) {
			//转换成List<FormItem>
			List<FormItem> formItems = new ArrayList<FormItem>();
			for(int i = 0 ; i < coloumnNames.length ; i++){
				FormItem formItem = new FormItem();
				if(coloumnNames[i].toLowerCase().equals("id")){
					continue;
				}
				formItem.setFieldName(coloumnNames[i]);
				formItem.setFieldChineseName(coloumnNames[i]);
				formItems.add(formItem);
				
			}
			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet(tableName);
			HSSFCell cell = null;
			HSSFRow row = null;
			HSSFFont font = wb.createFont();

			// 模板标题
			short rowIndex = 0;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			HSSFCellStyle aws_head_cell = wb.createCellStyle();
			font.setFontHeight((short) 400);
			aws_head_cell.setFont(font);
			cell.setCellStyle(aws_head_cell);
			cell.setCellValue("Excel数据导入模板");

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("数据源:" + tableName + "::" + tableName);

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("导入动作:追加");

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("创建日期:" + (DateUtils.datetimeFormat(new Date())));

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);

			cell.setCellValue("创建人:" + ComUtils.getLoginName());

			font = wb.createFont();
			
			HSSFCellStyle styleHead = wb.createCellStyle();
			
			styleHead.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
			styleHead.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			styleHead.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			styleHead.setBottomBorderColor(HSSFColor.BLACK.index);
			styleHead.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			styleHead.setLeftBorderColor(HSSFColor.BLACK.index);
			styleHead.setBorderRight(HSSFCellStyle.BORDER_THIN);
			styleHead.setRightBorderColor(HSSFColor.BLACK.index);
			styleHead.setBorderTop(HSSFCellStyle.BORDER_THIN);
			styleHead.setTopBorderColor(HSSFColor.BLACK.index);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(HSSFColor.WHITE.index);
			styleHead.setFont(font);

			rowIndex++;
			row = sheet.createRow(rowIndex);
			cell = row.createCell((short) 0);
			cell.setCellStyle(styleHead);
			
			createExcelModelColumn(formItems, sheet, cell, row, styleHead);
			fileName = createExcelModelFile(request,wb);
			
		}
		return fileName;
	}
	/**
	 * 导出离线模板文件
	 * @param request
	 * @param wb
	 * @return
	 */
	private static String createExcelModelFile(HttpServletRequest request,HSSFWorkbook wb){
		return ImpExpUtil.createExcelTmpFile(request,wb);
	}
	/**
	 * 导出离线模板column
	 * @param subSheetFormBean
	 * @param sheet
	 * @param cell
	 * @param row
	 * @param styleHead
	 */
	private static void createExcelModelColumn(List<FormItem> formItems,HSSFSheet sheet,HSSFCell cell,HSSFRow row,HSSFCellStyle styleHead){
		short column = 0;
		for (int i = 0; i < formItems.size(); i++) {
			FormItem model = formItems.get(Integer.valueOf(i));
			cell = row.createCell(column);
			sheet.setColumnWidth(column, 50*256);

			cell.setCellValue(model.getFieldName() + "<" + model.getFieldChineseName() + ">");
			cell.setCellStyle(styleHead);
			column++;
		}
	}
	/**
	 * 获取子表的formItem列
	 * @param formId
	 * @param subSheetCode
	 * @param processNodeId
	 * @return
	 */
	public static FormBean getSubSheetFormBean(String formId,String subSheetCode,String processNodeId){
		FormService formService = SpringContextUtils.getBean(FormService.class);
		FormItemService formItemService = SpringContextUtils.getBean(FormItemService.class);
//		Form subSheetForm = formService.get(formId);
		List<FormBean> subSheets = formItemService.getFormAndItemsByFormId(formId,SystemConstant.FORM_TYPE_S);//表单子表
		if(subSheets != null && subSheets.size() > 0){
			for(int i = 0 ; i < subSheets.size() ; i++){
				FormBean formBean = subSheets.get(i);
				if(formBean != null){
					if(formBean.getFormCode().equals(subSheetCode)){
						return formBean;
					}
				}
			}
		}
		return null;
	}
	/**
     * 获取节点表单权限配置
     * @param processDefinitionId
     * @param nodeId
     * @param formId
     * @return
     */
    private List<ProcessNodeFormPrivilege> getProcessNodeFormPrivileges(String processDefinitionId,String nodeId,String formId){
    	ProcessNodeFormPrivilege processNodeFormPrivilege = new ProcessNodeFormPrivilege();
    	processNodeFormPrivilege.setProcessDefinitionId(processDefinitionId);
    	processNodeFormPrivilege.setNodeId(nodeId);
    	ProcessNodeFormPrivilegeService processNodeFormPrivilegeService = SpringContextUtils.getBean(ProcessNodeFormPrivilegeService.class);
    	return processNodeFormPrivilegeService.selectByCondition(processNodeFormPrivilege);
    }
	
//	/**
//	 * 获取指定表单子表的数据
//	 * 
//	 * @param me
//	 * @param sheetId
//	 * @return
//	 * @preserve 声明此方法不被JOC混淆.
//	 */
//	public static String getExcelDataOfSheet(UserContext me, int bindId, int sheetId, int meId) {
//		StringBuffer sb = new StringBuffer();
//		String fileURL = "";
//		SheetModel sheetModel = (SheetModel) SheetCache.getModel(sheetId);
//		FormModel formModel = (FormModel) FormCache.getModel(sheetModel._formId);
//		MetaDataModel metaDataModel = (MetaDataModel) MetaDataCache.getModel(sheetModel._metaDataId);
//		HeadMessageModel headModel = (HeadMessageModel) HeadMessageDaoFactory.createHeadMessage().getInstance(bindId);
//		HSSFWorkbook wb = new HSSFWorkbook();
//		HSSFSheet sheet = wb.createSheet(metaDataModel._entityName);
//		HSSFRow row = null;
//		HSSFCell cell = null;
//		HSSFFont font = wb.createFont();
//
//		// 模板标题
//		short rowIndex = 0;
//		row = sheet.createRow(rowIndex);
//		cell = row.createCell((short) 0);
//
//		HSSFCellStyle aws_head_cell = wb.createCellStyle();
//		font.setFontHeight((short) 400);
//		aws_head_cell.setFont(font);
//		cell.setCellStyle(aws_head_cell);
//		cell.setCellValue(formModel._reportTitle + "::" + sheetModel._sheetTitle);
//
//		rowIndex++;
//		row = sheet.createRow(rowIndex);
//		cell = row.createCell((short) 0);
//
//		cell.setCellValue("创建日期:" + (UtilDate.datetimeFormat(new Date())));
//
//		rowIndex++;
//		row = sheet.createRow(rowIndex);
//		cell = row.createCell((short) 0);
//
//		cell.setCellValue("制表信息:" + me.getDepartmentModel()._departmentFullNameOfCache + "/" + me.getUserModel()._userName + " 标题：" + headModel._title);
//
//		// 输出表格内容
//		exportSheetData(me, true, wb, sheet, rowIndex, (short) 0, bindId, formModel, sheetModel, metaDataModel, meId);
//
//		// 看看是否有自己的filter 事件
//		if (bindId > 0) {
//			HeadMessageModel instanceModel = HeadMessageDaoFactory.createHeadMessage().getInstance(bindId);
//			WorkFlowStepModel stepModel = (WorkFlowStepModel) WorkFlowStepCache.getModelOfStepNo(instanceModel._wfId, instanceModel._wfsNo == 0 ? 1 : instanceModel._wfsNo);
//			if (metaDataModel != null && stepModel != null) {
//				ExcelDownFilterRTClassA filterAdapter = RTClassEvent.getExcelFilterAdapter(me, RTClassTypeConst.RUNTIME_TYPE_EXCEL_DATA_DOWN, bindId, stepModel._id, metaDataModel._entityName);
//
//				try {
//					if (filterAdapter != null)
//						wb = filterAdapter.fixExcel(wb);
//					if (wb == null)
//						return "数据下载被拒绝";
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			} else {
//				System.err.println("Export metaDataModel==null||stepModel==null");
//			}
//		}
//		String fileName = ImpExpUtil.createExcelTmpFile(wb);
//		fileURL = "downfile.wf?flag1=Excel&flag2=_&sid=" + me.getSessionId() + "&rootDir=tmp&filename=" + fileName;
//
//		return "<html><title></title><script> window.location=encodeURI('" + fileURL + "');</script></html>";
//	}
//
//	/**
//	 * 获取指定表单(Form)的Excel报表输出
//	 * 
//	 * @param me
//	 * @param bindId
//	 * @param formId
//	 * @return
//	 * @preserve 声明此方法不被JOC混淆.
//	 */
//	public static String getExcelDataOfAWSForm(UserContext me, int bindId, int formId) {
//		StringBuffer html = new StringBuffer();
//		FormModel formModel = (FormModel) FormCache.getModel(formId);
//		SheetModel sheetModel = (SheetModel) SheetCache.getMastSheetModel(formId);
//		// 获取当前表单的所有字段
//		Map metaDataMapList = MetaDataMapCache.getListOfMetaData(sheetModel._metaDataId);
//		MetaDataModel metaDataModel = (MetaDataModel) MetaDataCache.getModel(sheetModel._metaDataId);
//
//		HSSFWorkbook writeBook = new HSSFWorkbook();
//		FileInputStream fs = null;
//		String excelModelName = "../template/eform/excel/" + formModel._excelModel;
//
//		if (metaDataMapList != null) {
//			// 读配套的Excel模板数据
//			try {
//				// 读表单数据从数据库中
//				Vector data = (Vector) FormDaoFactory.createRuntimeForm().getInstanceOfBind(metaDataModel, bindId).get(0);
//
//				// 读模板数据到readBook
//				fs = new FileInputStream(excelModelName);
//				writeBook = new HSSFWorkbook(fs);
//				// 创建对工作表的引用。
//				HSSFSheet writeSheet = writeBook.getSheetAt(0);
//				// 删除其他Sheet
////				while (writeBook.getNumberOfSheets() > 1) {
////					writeBook.removeSheetAt(1);
////				}
//
//				// 循环Excel行
//				for (int rowIndex = 0; rowIndex < writeSheet.getLastRowNum(); rowIndex++) {
//					HSSFRow writeRow = writeSheet.getRow(rowIndex);
//					if (writeRow != null) {
//						// 循环Excel列
//						for (short columnIndex = 0; columnIndex < writeRow.getLastCellNum(); columnIndex++) {
//							HSSFCell writeCell = writeRow.getCell(columnIndex);
//							String value = "";
//							if (writeCell != null) {
//								value = getCellValue(writeCell);
//								
//								// 如果excel模板中的某个单元有[@XXX]标签
//								if (value != null && value.indexOf("[@") > -1) {
//									if (value.indexOf("[@SubReport") > -1) {
//										// 这个位置是个子表
//										// 计算这个子表的SheetId
//										int sheetIndex = 0;
//										try {
//											sheetIndex = Integer.parseInt(value.substring(value.lastIndexOf("[@SubReport") + 11, value.length() - 1));
//										} catch (Exception e) {
//											e.printStackTrace(System.err);
//										}
//										if (sheetIndex > 1)
//											sheetIndex = sheetIndex - 1;
//										Hashtable subSheetList = SheetCache.getSubSheetList(formId);
//										SheetModel currentSheetModel = (SheetModel) (SheetModel) subSheetList.get(new Integer(sheetIndex));
//										// 发现了这个子表描述模型
//										if (currentSheetModel != null) {
//											MetaDataModel currentMetaDataModel = (MetaDataModel) MetaDataCache.getModel(currentSheetModel._metaDataId);
//											// 输出表格内容
//											// 创建对工作表的引用。
//											HSSFSheet currentSheet = writeBook.createSheet("SubReport" + (sheetIndex + 1));
//											exportSheetData(me, true, writeBook, writeSheet, rowIndex, columnIndex, bindId, formModel, currentSheetModel, currentMetaDataModel, 0);
//											// exportSheetData(true,writeBook,
//											// currentSheet, 0, columnIndex,
//											// bindId, formModel,
//											// currentSheetModel,
//											// currentMetaDataModel);
//											// writeSheet.createRow(1);
//											// writeSheet.createRow(1);
//											// writeSheet.createRow(1);
//											// 重新设置Cell值
//											writeCell.setCellValue(formModel._reportTitle + "::" + currentSheetModel._sheetTitle);
//										}
//									} else {
//										// 循环替换可能存在的标签
//										for (int i = 0; i < metaDataMapList.size(); i++) {
//											MetaDataMapModel metaDataMapModel = (MetaDataMapModel) metaDataMapList.get(new Integer(i));
//											String targetValue = (String) data.get(i);
//											value = new UtilString(value).replace("[@" + metaDataMapModel._fieldName.toUpperCase() + "]", targetValue);
//										}
//										// 重新设置Cell值
//										if (writeCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
//											try {
//												writeCell.setCellValue(Double.parseDouble(value));
//											} catch (Exception ee) {
//											}
//										} else {
//											writeCell.setCellValue(value);
//										}
//									}
//
//								}
//
//							}
//						}
//					} else {// 空行
//
//					}// end if
//				}// end for
//				String fileName = ImpExpUtil.createExcelTmpFile(writeBook);
//				html.append("<html><title></title><script> window.location=encodeURI('downfile.wf?flag1=Excel&flag2=_&sid=").append(me.getSessionId()).append("&rootDir=tmp&filename=").append(fileName).append("');</script></html>");
//			} catch (Exception e) {
//				e.printStackTrace(System.err);
//				// html.append("<script>alert(\"导出Excel报表失败!").append(e.toString()).append("\");</script>");
//				html.append("<script>alert(\"导出Excel报表失败!\\n没有找到该报表的数据！").append("\");window.close();</script>");
//			} finally {
//				try {
//					fs.close();
//				} catch (Exception e) {
//				}
//			}
//		}
//		return html.toString();
//	}
//
//	private static String getCellValue(HSSFCell cell) {
//		int type = cell.getCellType();
//		switch (type) {
//		case HSSFCell.CELL_TYPE_BOOLEAN:
//			return String.valueOf(cell.getBooleanCellValue());
//		case HSSFCell.CELL_TYPE_NUMERIC:
//			return Double.toString(cell.getNumericCellValue());
//		case HSSFCell.CELL_TYPE_FORMULA:
//			return cell.getCellFormula();
//		case HSSFCell.CELL_TYPE_STRING:
//			return cell.getRichStringCellValue() == null ? null : cell.getRichStringCellValue().getString();
//		default:
//			break;
//		}
//		return null;
//	}
//
//	/**
//	 * 将指定的子表数据输出到指定的Excel工作区间指定的相对位置
//	 * 
//	 * @param isFriendly
//	 * @param wb
//	 * @param sheet
//	 * @param rowIndex
//	 * @param columnIndex
//	 * @param bindId
//	 * @param formModel
//	 * @param sheetModel
//	 * @param metaDataModel
//	 * @preserve 声明此方法不被JOC混淆.
//	 */
//	private static void exportSheetData(UserContext me, boolean isFriendly, HSSFWorkbook wb, HSSFSheet sheet, int rowIndex, short columnIndex, int bindId, FormModel formModel, SheetModel sheetModel, MetaDataModel metaDataModel, int meId) {
//		HeadMessageModel headMessageModel = HeadMessageDaoFactory.createHeadMessage().getInstance(bindId);
//		WorkFlowStepModel workflowStepModel = (WorkFlowStepModel) WorkFlowStepCache.getModelOfStepNo(headMessageModel._wfId, headMessageModel._wfsNo);
//		Map h = MetaDataMapCache.getListOfMetaData(sheetModel._metaDataId);
//		HSSFFont font = wb.createFont();
//		HSSFCellStyle styleHead = wb.createCellStyle();
//		styleHead.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
//		styleHead.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//		styleHead.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//		styleHead.setBottomBorderColor(HSSFColor.BLACK.index);
//		styleHead.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//		styleHead.setLeftBorderColor(HSSFColor.BLACK.index);
//		styleHead.setBorderRight(HSSFCellStyle.BORDER_THIN);
//		styleHead.setRightBorderColor(HSSFColor.BLACK.index);
//		styleHead.setBorderTop(HSSFCellStyle.BORDER_THIN);
//		styleHead.setTopBorderColor(HSSFColor.BLACK.index);
//		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
//		font.setColor(HSSFColor.WHITE.index);
//		styleHead.setFont(font);
//
//		short column = columnIndex;
//		HSSFRow row = sheet.createRow(rowIndex);
//		for (int i = 0; i < h.size(); i++) {
//			// metaDataModel
//			MetaDataMapModel model = (MetaDataMapModel) h.get(new Integer(i));
//			if (FormUtil.isFnDisplay(me, workflowStepModel, sheetModel, model)) {
//				HSSFCell cell = row.createCell(column);
//				/**
//				 * 2代表行数 (short)1 代表第几个字段 3代表合并到第几行 (short)0代表合并到第几列
//				 */
//				if (isFriendly) {// 是否友好的显示列标题
//					cell.setCellValue(model._fieldTitle);
//				} else {
//					cell.setCellValue(model._fieldName + "<" + model._fieldTitle + ">");
//				}
//				cell.setCellStyle(styleHead);
//				column++;
//			}
//		}
//
//		// 数据记录
//		HSSFCellStyle styleData = wb.createCellStyle();
//		styleData.setFillForegroundColor(HSSFColor.WHITE.index);
//		styleData.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//		styleData.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//		styleData.setBottomBorderColor(HSSFColor.BLACK.index);
//		styleData.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//		styleData.setLeftBorderColor(HSSFColor.BLACK.index);
//		styleData.setBorderRight(HSSFCellStyle.BORDER_THIN);
//		styleData.setRightBorderColor(HSSFColor.BLACK.index);
//		styleData.setBorderTop(HSSFCellStyle.BORDER_THIN);
//		styleData.setTopBorderColor(HSSFColor.BLACK.index);
//
//		MetaDataModel mdm = new MetaDataModel();
//		mdm.setModel(metaDataModel);
//		mdm.pageNum = 0;
//		Vector datas = null;
//		if (FormUtil.isFieldSubSheet(metaDataModel)) {
//			datas = FormDaoFactory.createRuntimeForm().getInstanceOfBindAndWorkFlowId(metaDataModel, bindId, null, meId, null);
//		} else {
//			datas = FormDaoFactory.createRuntimeForm().getInstanceOfBind(mdm, bindId);
//		}
//
//		// 为子表行移动预留的空间
//		for (int i = 0; i < datas.size(); i++) {
//			Vector data = (Vector) datas.get(i);
//			String id = (String) data.get(h.size()); // h变量是用户自定义字段列表h.size()是第一个系统默认字段在data列表的位置
//			// append a rows
//			rowIndex++;
//			row = sheet.createRow(rowIndex);
//			column = columnIndex;
//			for (int ii = 0; ii < h.size(); ii++) { // 每一行记录
//				// modify zhanghf @ 2008-05-12 将下面两行原有的注释去掉，以便判断是否在表单UI设计时是否隐藏字段
//				MetaDataMapModel model = (MetaDataMapModel) h.get(new Integer(ii));
//				// <-----------------@modify
//				// highsun---------------------------->
//				if (FormUtil.isFnDisplay(me, workflowStepModel, sheetModel, model)) {// 判断字段是否显示和UI设计时是否隐藏字段，实现只导出显示的字段
//					String currentData = (String) data.get(ii);
//					// append a column
//					HSSFCell cell = row.createCell(column);
//
//					if (model._fieldType.equals(MetaDataMapConst.MAP_TYPE_NUMBER)) {
//						try {
//							cell.setCellValue(Double.parseDouble(currentData));
//						} catch (Exception e) {
//						}
//					} else if(model._displayType.equals(MetaDataMapConst.MAP_DISPLAY_FILE)){  //附件
//						currentData=currentData.replaceAll("@@@@",",");
//						cell.setCellValue(currentData);
//					} else if (model._displayType.equals(MetaDataMapConst.MAP_DISPLAY_MULTI_LINE) || model._displayType.equals(MetaDataMapConst.MAP_DISPLAY_HTML_EDIT) || model._displayType.equals(MetaDataMapConst.MAP_DISPLAY_HTML_ADVANCE_EDIT)) {
//						if (currentData.toLowerCase().indexOf("<br/>") > -1) {
//							currentData = currentData.replaceAll("<br/>","\r\n");
//						}
//						if (currentData.toLowerCase().indexOf("__eol__") > -1) {
//							currentData = currentData.replaceAll("__eol__","\r\n");
//						}
//						CreationHelper helper = wb.getCreationHelper();
//						RichTextString str = helper.createRichTextString(currentData);
//						cell.setCellValue(str);
//					} else {
//						cell.setCellValue(currentData);
//					}
//					cell.setCellStyle(styleData);
//					column++;
//				}
//			}
//		}
//	}
//
//	/**
//	 * 从指定的位置向下移动n行
//	 * 
//	 * @param wb
//	 * @param sheet
//	 * @param rowIndex
//	 * @preserve 声明此方法不被JOC混淆.
//	 */
//	private static void moveNextRow(HSSFWorkbook wb, HSSFSheet sheet, int rowIndex, int step) {
//		for (int row = sheet.getLastRowNum(); row > rowIndex; row--) {
//			HSSFRow sourceRow = sheet.getRow(row);
//			HSSFRow newRow = sheet.createRow(row + step);
//			short col = 0;
//			for (col = 0; col < sourceRow.getLastCellNum(); col++) {
//				HSSFCell sourceCell = sourceRow.getCell(col);
//				HSSFCell newCell = newRow.getCell(col);
//				newCell = sourceCell;
//			}
//			newRow = sourceRow;
//		}
//	}
}