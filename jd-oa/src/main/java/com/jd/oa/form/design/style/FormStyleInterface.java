package com.jd.oa.form.design.style;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.form.execute.component.FormUIFactory;
import com.jd.oa.form.execute.component.UIComponentInterface;
import com.jd.oa.form.execute.component.model.ConfigComponentModel;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormItem;

public interface FormStyleInterface {
	
	public abstract Object getWeb();
	
	public abstract List<FormItem> getFormItem();
	
	public abstract void setFormItem(List<FormItem> items);
	
	public abstract void setForm(Form form);
	
	public abstract Form getForm();
	
	public abstract void setFormTemplateName(String formTemplateName);
	
	public abstract String getFormTemplateName();
	
	public abstract HttpServletRequest getHttpServletRequest();
	
	public abstract void setHttpServletRequest(HttpServletRequest request);
	
	public abstract void setHtmlName(String htmlName);
	
	public abstract String getHtmlName();
	
	public abstract void setSubSheets(List<Map<String,List<FormItem>>> subSheetItems);
	
}
