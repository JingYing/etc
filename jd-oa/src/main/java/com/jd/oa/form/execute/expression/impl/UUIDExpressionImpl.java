package com.jd.oa.form.execute.expression.impl;

import java.util.Map;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.system.model.SysUser;

public class UUIDExpressionImpl extends ExpressionAbst{

	public UUIDExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	public String expressionParse(String expression) {
		return IdUtils.uuid2();
	}
}
