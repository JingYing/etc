package com.jd.oa.form.execute.component.impl;

import java.util.Hashtable;
import java.util.Map;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.util.Html;

public class FormUIComponentDateImpl extends FormUIComponentAbst {
	
	public FormUIComponentDateImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}
	@Override
	public String getReadHtmlDefine(Map<String,Object> params) {
		// TODO Auto-generated method stub
		StringBuffer html = new StringBuffer();
		String value =  getValue();
//		if(value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
		html.append("<input type=text readonly name='").append(getMetaDataMapModel().getFieldName()).append("'  id='").append(getMetaDataMapModel().getFieldName()).append("' value=\"").append(Html.escape(value)).append("\">");
		return html.toString();
	}
	@Override
	public String getModifyHtmlDefine(Map<String,Object> params) {
		StringBuffer fieldHtml = new StringBuffer();
		String value =  getValue();
		String inputTypeConfig = "yyyy-MM-dd";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() >0){
			inputTypeConfig =getMetaDataMapModel().getInputTypeConfig();
		}
			
		fieldHtml.append("<input readonly id=\"").append(getMetaDataMapModel().getFieldName()).append("\" ></input>");
		
		if(!isNull()){
			fieldHtml.append("<font color=red>*</font>");
		}
		
		fieldHtml.append(" <script type=\"text/javascript\">\n");
		fieldHtml.append("$(function(){\n");
		fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()).append("').datebox({\n");
		fieldHtml.append("formatter:formatD_").append(getMetaDataMapModel().getFieldName()).append("\n");
		fieldHtml.append("});\n");
//		设置值
		fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()).append("').datebox('setValue','").append(DateUtils.dateFormat(value, inputTypeConfig)).append("');\n");
		fieldHtml.append("function formatD_").append(getMetaDataMapModel().getFieldName()).append("(date){\n");
		fieldHtml.append("	var date = new Date(date);\n");
		fieldHtml.append("	return date.formatDate('").append(inputTypeConfig).append("');\n");
		fieldHtml.append("}\n");
		fieldHtml.append("})\n");
		fieldHtml.append("</script> \n");
		return fieldHtml.toString();
	}

	@Override
    public String getValidateJavaScript(Map<String,Object> params) {
    	FormBusinessField metaDataMapModel = getMetaDataMapModel();
		StringBuffer jsCheckvalue = new StringBuffer();
//		$('#dds').datebox('getValue')
		jsCheckvalue.append(" var ").append(getMetaDataMapModel().getFieldName()).append(" = ").append("$('#").append(getMetaDataMapModel().getFieldName()).append("').datebox('getValue');");
		// 非空校验
		if (!isNull()) {
			jsCheckvalue.append("\n try{\n")
						.append("     if(").append(getMetaDataMapModel().getFieldName()).append(".length == 0)").append("{\n")
						.append(" 	    alert ('请给字段:[").append(metaDataMapModel.getFieldChineseName()).append("]输入一个值，该字段值不允许为空！');\n")
						.append("	    try{ \n")
						.append(getErrorTip(metaDataMapModel.getFieldName()))
						.append("	   }catch(e){}\n")
						.append("	   return false; \n")
						.append("    }\n ")
						.append("  }catch(e){}\n\n ");
		}

		// 基本类型校验
		if (metaDataMapModel.getInputType().equals("日期")) {
			jsCheckvalue.append("if(!IsDate(").append(getMetaDataMapModel().getFieldName()).append("))").append("{ \n")
						.append("\n try{\n")
						.append("           alert ('字段:[").append(metaDataMapModel.getFieldChineseName()).append("]输入的值是一个非法的日期格式！');\n")
						.append(getErrorTip(metaDataMapModel.getFieldName()))
						.append("   }catch(e){}\n\n ")
						.append("      		return false; \n")
						.append("        }\n");
		}
		return jsCheckvalue.toString();
    }
    public String getSettingWeb(){
    	String value = getMetaDataMapModel().getInputTypeConfig();
    	StringBuffer settingHtml = new StringBuffer();
    	settingHtml.append("<table>");
		settingHtml.append("<tr>");
		settingHtml.append("<td>格式</td>");
		settingHtml.append("<td>");
		settingHtml.append("<select id='selectData'>");
		settingHtml.append("<option value='yyyy-MM-dd' ").append("yyyy-MM-dd".equals(value) ? "selected" : "").append(">yyyy-MM-dd");
//		settingHtml.append("<option value='MM-dd-yyyy' ").append("MM-dd-yyyy".equals(value) ? "selected" : "").append(">MM-dd-yyyy");
//		settingHtml.append("<option value='yyyy年MM月dd日' ").append("yyyy年MM月dd日".equals(value) ? "selected" : "").append(">yyyy年MM月dd日");
		settingHtml.append("</select>");
		settingHtml.append("<td>");
		settingHtml.append("</tr>");
		settingHtml.append("</table>");
		
		settingHtml.append("<script>");
		settingHtml.append("function returnSettingConfig(){\n");
		settingHtml.append("	return getSelectValue('selectData')\n");
	    settingHtml.append("}\n"); 
	    settingHtml.append("</script>\n"); 
    	return settingHtml.toString();
    }
	@Override
	public String getTrueValue() {
		return getValue();
	}
	@Override
	public String getQueryHtmlDefine(Map<String,Object> params) {
		StringBuffer fieldHtml = new StringBuffer();
//		String value =  getValue();
		String inputTypeConfig = "yyyy-MM-dd";
//		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() >0){
//			inputTypeConfig =getMetaDataMapModel().getInputTypeConfig();
//		}
			
		fieldHtml.append("<input readonly id=\"").append(getMetaDataMapModel().getFieldName()+"_BeginDate").append("\" ></input> 至 ");
		fieldHtml.append("<input readonly id=\"").append(getMetaDataMapModel().getFieldName()+"_EndDate").append("\" ></input>");
		
//		if(!isNull()){
//			fieldHtml.append("<font color=red>*</font>");
//		}
		
//		fieldHtml.append(" <script type=\"text/javascript\">\n");
//		
//		
//		
//		fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()+"_BeginDate").append("').datebox('setValue','").append(DateUtils.dateFormat(value, inputTypeConfig)).append("');\n");
//		fieldHtml.append("function formatD_").append(getMetaDataMapModel().getFieldName()+"_BeginDate").append("(date){\n");
//		fieldHtml.append("	var date = new Date(date);\n");
//		fieldHtml.append("	return date.formatDate('").append(inputTypeConfig).append("');\n");
//		fieldHtml.append("}\n");
//		
//		fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()+"_EndDate").append("').datebox('setValue','").append(DateUtils.dateFormat(value, inputTypeConfig)).append("');\n");
//		fieldHtml.append("function formatD_").append(getMetaDataMapModel().getFieldName()+"_EndDate").append("(date){\n");
//		fieldHtml.append("	var date = new Date(date);\n");
//		fieldHtml.append("	return date.formatDate('").append(inputTypeConfig).append("');\n");
//		fieldHtml.append("}\n");
//		
//		
////		fieldHtml.append("$(function(){\n");
//		fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()+"_BeginDate").append("').datebox({\n");
//		fieldHtml.append("formatter:formatD_").append(getMetaDataMapModel().getFieldName()+"_BeginDate").append("\n");
//		fieldHtml.append("});\n");
//		
//		fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()+"_EndDate").append("').datebox({\n");
//		fieldHtml.append("formatter:formatD_").append(getMetaDataMapModel().getFieldName()+"_EndDate").append("\n");
//		fieldHtml.append("});\n");
//		
//		
////		设置值
//		
//		
//		
////		fieldHtml.append("})\n");
//		fieldHtml.append("</script> \n");
		return fieldHtml.toString();
	}
    
}