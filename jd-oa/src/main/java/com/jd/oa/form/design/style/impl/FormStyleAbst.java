package com.jd.oa.form.design.style.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.form.design.style.FormStyleInterface;
import com.jd.oa.form.execute.RuntimeDefaultFormUIComponent;
import com.jd.oa.form.execute.component.FormUIFactory;
import com.jd.oa.form.execute.component.UIComponentInterface;
import com.jd.oa.form.execute.component.model.ConfigComponentModel;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormItem;

public abstract class FormStyleAbst implements FormStyleInterface{
	/**
	 * 一个字段模型的描述
	 */
	private List<FormItem> items;
	
	private List<Map<String,List<FormItem>>> subSheetItems;
	
	private Form form;
	
	private String formTemplateName;
	
	private HttpServletRequest request;
	
	private String htmlName;
	
	public abstract String getWeb();
	
	
	@Override
	public  List<FormItem> getFormItem() {
		// TODO Auto-generated method stub
		return addProcessFormDefaultField(items);
	}
	
	public void setFormItem(List<FormItem> items){
		this.items = items;
	}
	
	public void setSubSheets(List<Map<String,List<FormItem>>> subSheetItems){
		this.subSheetItems = subSheetItems;
	}
	
	public void setForm(Form form){
		this.form = form;
	}
	public Form getForm(){
		return this.form;
	}
	public void setFormTemplateName(String formTemplateName){
		this.formTemplateName = formTemplateName;
	}
	
	public String getFormTemplateName(){
		return this.formTemplateName;
	}
	
	public HttpServletRequest getHttpServletRequest(){
		return this.request;
	}
	
	public void setHttpServletRequest(HttpServletRequest request){
		this.request = request;
	}
	
	public void setHtmlName(String htmlName){
		this.htmlName = htmlName;
	}
	
	public String getHtmlName(){
		return this.htmlName;
	}
	public String getFormItemTags(FormItem item){
		return "[@" + item.getFieldName() + "]";
	}
	public List getSubSheet(){
		List<String> sheets = new ArrayList();
		if(this.subSheetItems != null && this.subSheetItems.size() > 0){
			for(int i = 0 ; i < this.subSheetItems.size();i++){
				Map<String,List<FormItem>> subSheet = this.subSheetItems.get(i);
				if(subSheet != null){
					Iterator iterator = subSheet.entrySet().iterator(); 
					int j = 0;
					while(iterator.hasNext()) { 
					    Entry entry = (Entry)iterator.next(); 
					    String key = entry.getKey().toString();
					    List<FormItem> formItems = subSheet.get(key); 
					    if(formItems != null){
							sheets.add("[@SUBSHEET" + j + "]");
							j++;
					    }
					} 
				}
			}
		}
		return sheets;
	}
	public List<FormItem>  addProcessFormDefaultField(List<FormItem> items){
		List<FormItem> formItems = new ArrayList<FormItem>();
		formItems.addAll(RuntimeDefaultFormUIComponent.getFormDefaultField(null,null));
		formItems.addAll(items);
		return formItems;
	}
}
