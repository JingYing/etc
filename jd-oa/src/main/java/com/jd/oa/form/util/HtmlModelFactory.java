package com.jd.oa.form.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

import com.jd.oa.common.exception.BusinessException;

public class HtmlModelFactory {
	static String encoding = "utf-8";
	
	public static String getModel(String templatePath,String htmName){
		File myFile = new File( templatePath + "WEB-INF/views/form/template/" + htmName + ".htm");
		String str = "";
		if(!myFile.exists()){
			return htmName + "不存在";
		}else {
			InputStreamReader reader = null;
			StringWriter writer = new StringWriter();
			try {
				if (encoding == null || "".equals(encoding.trim())) {
					reader = new InputStreamReader(new FileInputStream(myFile));
				} else {
					reader = new InputStreamReader(new FileInputStream(myFile),encoding);
				}
				// 将输入流写入输出流
				char[] buffer = new char[1024];
				int n = 0;
				while (-1 != (n = reader.read(buffer))) {
					writer.write(buffer, 0, n);
				}
			} catch (Exception e) {
				throw new BusinessException("读取表单模板失败：" + e.getMessage(),e);
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						throw new BusinessException("读取表单模板失败：" + e.getMessage(),e);
					}
			}
			// 返回转换结果
			
			return writer.toString();
			
		}
	}
}
