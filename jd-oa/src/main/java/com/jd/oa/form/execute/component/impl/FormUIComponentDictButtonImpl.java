package com.jd.oa.form.execute.component.impl;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;
/**
 * 动态数据映射实现
 * @author liub
 *
 */
public class FormUIComponentDictButtonImpl extends FormUIComponentAbst{
	
	private DiDatasourceService diDatasourceService;
	
	public FormUIComponentDictButtonImpl(FormBusinessField metaDataMapModel,String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getTrueValue() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getModifyHtmlDefine(Map<String,Object> params) {
		return null;
	}
	@Override
	public String getSettingWeb(){
		StringBuffer settingHtml = new StringBuffer();
		String value = "";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() > 0){
			value = getMetaDataMapModel().getInputTypeConfig();
		}
		String inputLength = "";
		String dataSourceId = "";
		String dataSourceName = "";
		if(value.trim().length() > 0){
			JSONObject jsonObject = JSONObject.fromObject(value);
			if(jsonObject != null){
				if(jsonObject.get("inputLength") != null){
					inputLength = jsonObject.get("inputLength").toString();
				}
				if(jsonObject.get("dataSourceId") != null){
					dataSourceId = jsonObject.get("dataSourceId").toString();
				}
//				if(jsonObject.get("dataSourceName") != null){
//					dataSourceName = jsonObject.get("dataSourceName").toString();
//				}
			}
		}
		settingHtml.append("<table>")
					.append("<tr>")
					.append("<td align='right'>录入长度:</td>")
					.append("<td colspan='2'>&nbsp;<input type='text' id='inputLength' name='inputLength' style='width:100px' data-rule='required;tableLength' value='").append(inputLength).append("'></td>")
					.append("</tr>")
					.append("<tr>")
					.append("<td align='right'>数据源:</td>")
					.append("<td>");
		settingHtml.append("<input id='selectDataSource' style='width:150px'></input>");
		settingHtml.append("</td>")
					.append("<td><span style='cursor:pointer' onClick='openDataSourceMappingDialog();return false;'>&nbsp;<img src='../static/common/img/set.png' border='0' />&nbsp;配置映射</span></td>")
					.append("</tr>")
					.append("</table>");
		
		settingHtml.append("<script type='text/javascript'>\n");
		settingHtml.append("$(function(){\n");
		settingHtml.append(" $('#selectDataSource').combogrid('setValue','").append(dataSourceId).append("');\n");
		settingHtml.append("});\n");
		settingHtml.append(" document.getElementById('inputLength').focus();\n");
		settingHtml.append("function returnSettingConfig(){\n");
		settingHtml.append("var dataSource = $('#selectDataSource').combogrid('grid').datagrid('getSelected');\n");
		settingHtml.append("var jsonData = {\n");
		settingHtml.append("		inputLength : document.getElementById('inputLength').value,\n");
		settingHtml.append("		dataSourceId : dataSource.id,\n");
		settingHtml.append("		dataSourceName : dataSource.name\n");
		settingHtml.append("	};\n");
		settingHtml.append("return JSON.stringify(jsonData);\n");
		settingHtml.append("}\n");
		settingHtml.append("</script>\n");
		return settingHtml.toString();
	}

}
