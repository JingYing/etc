package com.jd.oa.form.execute.component;

import java.util.Map;

import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.form.model.FormBusinessField;
/**
 * 抽取子表组件抽象部分代码
 * @author liub
 *
 */
public  class FormUISubSheetComponentAbst extends FormUIComponentAbst {

	public FormUISubSheetComponentAbst(FormBusinessField metaDataMapModel,String value) {
		super(metaDataMapModel, value);
	}
	protected String getFieldId(){
		return getMetaDataMapModel().getFieldName();
	}
	protected String getTitle(){
		if(!isNull()){
			return getMetaDataMapModel().getFieldChineseName() + "<font color='red'>*</font>";
		}
		return getMetaDataMapModel().getFieldChineseName();
	}
	protected String getWidth(){
		return "150";
	}
	protected String getIsNullContext(){
		if(!isNull()){
			return ",require:true";
		}
		return "";
	}
	protected String getIsHiddenContext(){
		//隐藏时，带有编辑
		if(isHidden()){
			if(isEdit()){
				return ",hidden:true";
			} else {
				return ",hidden:true," + getEditor();
			}
		}
		return "";
	}
	protected String getEditorContext(){
		if(isEdit()){
			return "," + getEditor();
		}
		return "";
	}
	public String getEditor(){
		return "editor:{type:'text',options:{fieldName:'" + getFieldId() + "'}}";
	}
	/**
	 * 获取column 格式化的子类实现
	 * @return
	 */
	public String getFormatter(){
		return "";
	}
	protected String getFormatterContext(){
		String formatterString=getFormatter();
		if(formatterString != null && formatterString.trim().length() > 0){
			StringBuffer formatter = new StringBuffer();
			formatter.append(",").append("formatter : function(value){\n");
			formatter.append(formatterString);
			formatter.append("}");
			return formatter.toString();
		}
		return "";
	}
	public String toString(){
		StringBuffer str = new StringBuffer();
		str.append("{field: \"").append(getFieldId()).append("\"").append(",")
			.append(" title: \"").append(getTitle()).append("\"").append(",")
			.append(" width:").append(getWidth())
			.append(getIsNullContext())//是否允许为空
			.append(getIsHiddenContext())//是否隐藏
			.append(getEditorContext())//是否允许编辑
			.append(getFormatterContext())//格式化列显示
			.append("}");
		return str.toString();
	}
	@Override
	public String getTrueValue() {
		return null;
	}
}
