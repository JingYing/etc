package com.jd.oa.form.dao;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.bean.FormItemBean;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单Dao
 * User: yujiahe
 * Date: 13-9-12
 * Time: 上午11:57
 * To change this template use File | Settings | File Templates.
 */
public interface FormDao extends BaseDao<Form,String> {
    /**
     * @param formName
     * @return String
     * @Description: 根据表单名称查看表单是否存在
     * @author yujiahe
     */
    boolean isExisForm(Form form);
    
    /**
	  * @Description: 根据ID查询信息
	  * @param id
	  * @return Form
	  * @author xulin
	  * @date 2013-9-16下午01:49:54 
	  * @version V1.0
	 */
	public Form selectByPrimaryKey(String id);
	
	/**
	 * 
	 * @param tableId
	 * @author xulin
	 * @return
	 */
	public List<FormItemBean> findFormItemsByTableId(String tableId);
	
	/**
	 * 
	 * @param processDefintonId
	 * @author xulin
	 * @return
	 */
	public String getFormCodeByProDId(String processDefintonId);
	
	public Map<String,Object> getList(String sql,String type);
	
	/**
	 * 查询表单包括其子表
	 * @param form
	 * @return
	 */
	public List<Form> findFormWithSubForm(Form form);
}
