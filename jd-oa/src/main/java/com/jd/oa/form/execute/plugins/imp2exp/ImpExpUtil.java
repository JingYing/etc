package com.jd.oa.form.execute.plugins.imp2exp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddressList;

import com.jd.oa.form.util.TmpFile;

public class ImpExpUtil {
	/**
	 * 创建Excel临时文件,返回文件名
	 * 
	 * @param chart
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String createExcelTmpFile(HttpServletRequest request,HSSFWorkbook wb) {
		String fileName = TmpFile.getExcelTmpFilePath(request,"xls");
		FileOutputStream out = null;
		String fn = TmpFile.getExcelTmpDirectoryPath(request) + "/" + fileName;
		try {
			out = new FileOutputStream(fn);
			wb.write(out);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			try {
				out.close();
			} catch (Exception e2) {
			}
		}
		return fileName;
	}

	/**
	 * 创建pdf临时文件,返回文件名
	 * 
	 * @param chart
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String createPdfTmpFile(HttpServletRequest request,FileInputStream srcPdf) {

		String fileName = TmpFile.getPDFTmpFilePath(request,"pdf");
		FileOutputStream out = null;
		String fn = TmpFile.getPDFTmpDirectoryPath(request) + "/" + fileName;
		try {
			out = new FileOutputStream(fn);
			byte[] btSrcPdf = new byte[16];
			while (srcPdf.read(btSrcPdf) > 0) {
				out.write(btSrcPdf);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Exception e2) {
			}
		}
		return fileName;
	}

	/**
	 * 创建Word临时文件,返回文件名
	 * 
	 * @param chart
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String createWordTmpFile(HttpServletRequest request,HSSFWorkbook wb) {
		String fileName = TmpFile.getExcelTmpFilePath(request,"doc");
		FileOutputStream out = null;
		String fn = TmpFile.getExcelTmpDirectoryPath(request) + "/" + fileName;
		try {
			out = new FileOutputStream(fn);
			wb.write(out);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			try {
				out.close();
			} catch (Exception e2) {
			}
		}
		return fileName;
	}

//	/**
//	 * 返回一个准备上传导入文件的页面
//	 * 
//	 * @return 返回一个准备上传文件的页面描述
//	 */
//	public static String getUpFilePage(UserContext me, String type, String id, String rootDir) {
//
//		String sid = "<input type=hidden name=sid value=" + me.getSessionId() + ">\n";
//		Hashtable hashTags = new Hashtable(3);
//		hashTags.put("flag1", type);
//		hashTags.put("flag2", id);
//		hashTags.put("sid", sid);
//		hashTags.put("rootDir", rootDir);
//
//		return RepleaseKey.replace(HtmlModelFactory.getHtmlModel("upFile.htm"), hashTags);
//	}

	/**
	 * 
	 * 方法名称：SetDataValidation
	 * 
	 * 内容摘要：设置数据有效性 字符少于255
	 * 
	 * @param firstRow
	 *            起始行
	 * 
	 * @param firstCol
	 *            起始列
	 * 
	 * @param endRow
	 *            终止行
	 * 
	 * @param endCol
	 *            终止列
	 */
	public static HSSFDataValidation setDataValidation(HSSFWorkbook wb, int firstRow, int firstCol, int endRow, int endCol, String[] textList) {
		if (textList == null) {
			return null;
		}
		StringBuffer textStr = new StringBuffer();
		for (int i = 0; i < textList.length; i++) {
			textStr.append(textList[i]);
		}
		// 大于255 字符
		if (textStr.length() > 255) {
			return setDataValidation(wb, firstRow, firstCol, endRow, endCol, textList, null);
		}
		// 加载下拉列表内容

		DVConstraint constraint = DVConstraint.createExplicitListConstraint(textList);

		// 设置数据有效性加载在哪个单元格上。

		// 四个参数分别是：起始行、终止行、起始列、终止列

		CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);

		// 数据有效性对象

		HSSFDataValidation data_validation = new HSSFDataValidation(regions, constraint);

		return data_validation;

	}

	/**
	 * 
	 * 方法名称：SetDataValidation
	 * 
	 * 内容摘要：设置数据有效性 自动产生sheet 字典
	 * 
	 * @param firstRow
	 *            起始行
	 * 
	 * @param firstCol
	 *            起始列
	 * 
	 * @param endRow
	 *            终止行
	 * 
	 * @param endCol
	 *            终止列
	 * @param fieldName
	 *            列名称
	 */
	public static HSSFDataValidation setDataValidation(HSSFWorkbook wb, int firstRow, int firstCol, int endRow, int endCol, String[] textList, String fieldName) {
		if (textList == null) {
			return null;
		}
		if (fieldName == null) {
			return null;
		}
		if (fieldName.trim().length() == 0) {
			fieldName = String.valueOf(System.currentTimeMillis());
		}
		HSSFSheet sheet = wb.getSheet(fieldName + "_字典");
		sheet = sheet == null ? wb.createSheet(fieldName + "_字典") : sheet;
		for (int i = 0; i < textList.length; i++) {
			HSSFRow row = sheet.createRow(i);
			HSSFCell cell = row.createCell(0);
			cell.setCellValue(textList[i]);
		}
		wb.setSheetHidden(wb.getSheetIndex(fieldName + "_字典"), true);
		DVConstraint dvConstraint = DVConstraint.createFormulaListConstraint("'" + fieldName + "_字典'!$A$1:$A$" + textList.length);
		CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
		// 数据有效性对象
		HSSFDataValidation data_validation = new HSSFDataValidation(regions, dvConstraint);
		return data_validation;
	}
}
