package com.jd.oa.form.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.FormBusinessField;

/** 
 * @Description: 自定义表字段 Service
 * @author guoqingfu
 * @date 2013-9-12 上午10:49:56 
 * @version V1.0 
 */
public interface FormBusinessFieldService extends BaseService<FormBusinessField, String>{

	/**
	  * @Description: 根据ID查询自定义表字段信息
	  * @param formBusinessFieldId
	  * @return FormBusinessField
	  * @author guoqingfu
	  * @date 2013-9-13上午10:11:33 
	  * @version V1.0
	 */
	public FormBusinessField getFormBusinessFieldInfo(String formBusinessFieldId);
	/**
	  * @Description: 删除自定义表字段
	  * @param formBusinessFieldIds
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-13上午11:03:25 
	  * @version V1.0
	 */
	public Map<String,Object> deleteFormBusinessField(List<String> formBusinessFieldIds);
	/**
	  * @Description: 判断是否存在字段名或是字段显示名
	  * @param formBusinessField
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-13下午01:35:03 
	  * @version V1.0
	 */
	public Map<String,Object> isExistTable(FormBusinessField formBusinessField);
	/**
	  * @Description: 判断是否存在自定义表字段显示名
	  * @param formBusinessField
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-13下午02:07:28 
	  * @version V1.0
	 */
	public boolean isExistFieldChineseName(FormBusinessField formBusinessField);
	/**
	  * @Description: 创建物理表
	  * @param businessTableId
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-17下午01:11:25 
	  * @version V1.0
	 */
	public Map<String,Object> createBusinessTable(String businessTableId);
	
	/**
	  * @Description: 高级模式创建表单时 根据自定义表ID查询未选择的字段信息
	  * @param businessTableId
	  * @return List<FormBusinessField>
	  * @author xulin
	  * @date 2013-9-22上午01:54:17 
	  * @version V1.0
	 */
	public List<FormBusinessField> findTableFieldListNotSelect(String businessTableId);
	/**
	 * @Description: 创建物理表(包括子表)
	  * @param businessTableId
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-25上午11:15:37 
	  * @version V1.0
	 */
	public Map<String,Object> createTableIncludeChild(String businessTableId);
	
	/**
	 * @Description: 根据tableId查询字段信息
	 * @param tableId
	 * @return List<FormBusinessField>
	 * @author xulin
	 * @date 2013-9-25上午10:44:15 
	 * @version V1.0
	 */
	public List<FormBusinessField> selectTableFieldsByTableId(String tableId);
	/**
	 * @Description: 判断字段时候已被表单使用
	  * @param bussinessFieldId
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-27上午11:37:46 
	  * @version V1.0
	 */
	public Map<String,Object> isUsedByForm(String bussinessFieldId);
	
	/**
	 * @Description: 判断当前表是否已被创建过
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-23下午08:31:04 
	  * @version V1.0
	 */
	public boolean isExistBusinessTable(String tableName);
	/**
	 * @Description: 根据表名查看该表是否已经有数据
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-27上午10:42:27 
	  * @version V1.0
	 */
	public boolean isExistData(String tableName);
	/**
	 * @Description: 查询被创建过的业务表名
	  * @param tableNameList
	  * @return List<String>
	  * @author guoqingfu
	  * @date 2013-10-22下午05:56:41 
	  * @version V1.0
	 */
	public List<String> findCreatedTable(List<String> tableNameList);

    /**
     *  对于已经有数据的业务表添加 或者修改 字段
     * @param businessTableId
     * @param addFieldsList
     * @param updateFieldsList
     * @return
     */
    public  Map<String,Object> updateBusinessTable(String businessTableId , List<FormBusinessField> addFieldsList,List<FormBusinessField> updateFieldsList);
}
