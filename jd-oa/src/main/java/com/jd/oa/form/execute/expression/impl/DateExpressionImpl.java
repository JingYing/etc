package com.jd.oa.form.execute.expression.impl;

import java.sql.Timestamp;
import java.util.Map;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
/**
 * 日期表达式
 * @author birkhoff
 *
 */
public class DateExpressionImpl extends ExpressionAbst{

	public DateExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	public String expressionParse(String expression) {
		return DateUtils.dateFormat(new Timestamp(System.currentTimeMillis()));
	}

}
