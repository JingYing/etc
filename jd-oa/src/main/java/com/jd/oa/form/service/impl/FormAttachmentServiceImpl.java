package com.jd.oa.form.service.impl;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.file.FileStreamType;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.IOUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.form.dao.FormAttachmentDao;
import com.jd.oa.form.execute.plugins.imp2exp.excel.ExportExcel;
import com.jd.oa.form.model.FormAttachment;
import com.jd.oa.form.service.FormAttachmentService;
import com.jd.oa.form.util.TmpFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;

@Service
public class FormAttachmentServiceImpl extends BaseServiceImpl<FormAttachment, String> implements FormAttachmentService{
	 @Autowired
	private JssService jssService;
	@Autowired
	private FormAttachmentDao formAttachmentDao;
	@Override
	public BaseDao<FormAttachment, String> getDao() {
		// TODO Auto-generated method stub
		return formAttachmentDao;
	}

	@Override
	public String uploadFileToJssService(MultipartFile uploadFile) throws BusinessException{
		String fileId = IdUtils.uuid2();
		String fileName = uploadFile.getOriginalFilename();
    	String fileExtName = FileUtil.getFileExtensionName(uploadFile.getOriginalFilename());
		String fileKey = fileId + fileExtName;
		//上传到JSS
		try {
			jssService.uploadFile(SystemConstant.BUCKET, fileKey, uploadFile.getInputStream());
		} catch (IOException e) {
			throw new BusinessException("文件上传到JSS服务失败!",e);
		}
		
		FormAttachment formAttachment = new FormAttachment();
		formAttachment.setAttachmentExt(fileExtName);
		formAttachment.setAttachmentName(fileName);
		formAttachment.setAttachmentSize(uploadFile.getSize());
		formAttachment.setAttachmentKey(fileKey);
		formAttachment.setBucket(SystemConstant.BUCKET);
		formAttachment.setAttachmentType("1");
		
		return insert(formAttachment);
	}

	@Override
	public String uploadFileToJssService(FormAttachment formAttachment, InputStream inputStream) throws BusinessException {
		try {
			jssService.uploadFile(SystemConstant.BUCKET, formAttachment.getAttachmentKey(),inputStream);
		} catch (Exception e) {
			throw new BusinessException("文件上传到JSS服务失败!",e);
		}
		formAttachment.setBucket(SystemConstant.BUCKET);
		formAttachment.setAttachmentType("1");

		return insert(formAttachment);
	}

	/**
	 * 删除附件
	 */
	public int deleteFile(String id){
		FormAttachment formAttachment = get(id);
		if(formAttachment != null){
			String key = formAttachment.getAttachmentKey();
			if(key != null && key.trim().length() > 0){
				jssService.deleteObject(SystemConstant.BUCKET, key);
			}
		}
		return super.delete(id,true);
		
	}
	/**
	 * 下载文件
	 */
	public void downloadfile(HttpServletRequest request,HttpServletResponse response,String id){
		response.setCharacterEncoding("UTF-8");  
		String contentType = "application/octet-stream";
		String ext = "";
		String agent = request.getHeader("USER-AGENT");
		FormAttachment formAttachment = get(id);
		if(formAttachment == null){
			throw new BusinessException("下载的文件不存在!");
		}
		String fileName = formAttachment.getAttachmentName();
		if (fileName.lastIndexOf(".") > 0 && fileName.lastIndexOf(".") < fileName.length()) {
			ext = FileUtil.getFileExtensionName(fileName);
			contentType = FileStreamType.getStreamType(ext);
		}
		response.setContentType(contentType);
		response.setHeader("Content-disposition", "attachment;filename=" + getFileName(fileName, agent));
		InputStream fileBody = jssService.downloadFile(SystemConstant.BUCKET, formAttachment.getAttachmentKey());//读取JSS文件流
		if(fileBody != null){
			BufferedInputStream bis = null;
			try{
//				in = fileBody.getBinaryStream();
				bis = new BufferedInputStream(fileBody);
				IOUtils.copy(bis, response.getOutputStream());
			}catch(IOException e){
				throw new BusinessException("文件下载失 败!" ,e);
			} finally{
				try {
					if(bis!=null){
						bis.close();
					}
				} catch (IOException e) {
					throw new BusinessException("文件下载失 败!",e);
				}
			}
		}else {
			try {
				response.getWriter().write("下载文件错误!");
			} catch (IOException e) {
				throw new BusinessException("文件下载失 败!",e);
			}
		}
	}
	/**
	 * 
	 */
	public void downloadExcelImportModel(HttpServletRequest request,HttpServletResponse response, String tableName){
		String fileName = ExportExcel.getExcelModelOfTable(request, tableName);
		response.setCharacterEncoding("UTF-8");  
		String contentType = "application/octet-stream";
		String ext = "";
		String agent = request.getHeader("USER-AGENT");
		if (fileName.lastIndexOf(".") > 0 && fileName.lastIndexOf(".") < fileName.length()) {
			ext = FileUtil.getFileExtensionName(fileName);
			contentType = FileStreamType.getStreamType(ext);
		}
		response.setContentType(contentType);
		response.setHeader("Content-disposition", "attachment;filename=" + getFileName(fileName, agent));
		File tmpFile = new File(TmpFile.getExcelTmpDirectoryPath(request) + "/" +  fileName);
		InputStream fileBody = null;
		try {
			fileBody = new FileInputStream(tmpFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		if(fileBody != null){
			BufferedInputStream bis = null;
			try{
//				in = fileBody.getBinaryStream();
				bis = new BufferedInputStream(fileBody);
				IOUtils.copy(bis, response.getOutputStream());
			}catch(IOException e){
				throw new BusinessException("文件下载失 败!" ,e);
			} finally{
				try {
					if(bis!=null){
						bis.close();
					}
				} catch (IOException e) {
					throw new BusinessException("文件下载失 败!",e);
				}
			}
		}else {
			try {
				response.getWriter().write("下载文件错误!");
			} catch (IOException e) {
				throw new BusinessException("文件下载失 败!",e);
			}
		}
	}
	private String getFileName(String fileName, String agent) {
		String codedfilename = fileName;
		try {
			if(agent != null){
				if (agent.indexOf("Mozilla") != -1 && agent.indexOf("Android") != -1) {// Mozilla..
					try {
						codedfilename = URLEncoder.encode(fileName, "UTF-8");
					} catch (Exception e) {
						codedfilename = fileName;
					}
				} else if (agent.toLowerCase().indexOf("safari") != -1) {// Safari浏览器，只能采用ISO编码的中文输出
					codedfilename = new String(codedfilename.getBytes("UTF-8"), "ISO8859-1");
				} else if (agent.toLowerCase().indexOf("chrome") != -1) {// Chrome浏览器，只能采用MimeUtility编码或ISO编码的中文输出
					codedfilename = MimeUtility.encodeText(codedfilename, "UTF8", "B");
				}
					// FireFox浏览器，可以使用MimeUtility或filename*或ISO编码的中文输出
				else if (agent.toLowerCase().indexOf("mozilla") != -1 && agent.toLowerCase().indexOf("firefox") != -1) {// ff,mozilla..
					codedfilename = MimeUtility.encodeText(fileName, "GBK", "B");
				} else {// ie,opera..
					if (Charset.defaultCharset().name().indexOf("GBK") != -1) {
						codedfilename = new String(fileName.getBytes(), "ISO8859_1");
					} else {
						try {
							codedfilename = URLEncoder.encode(fileName, "utf-8");
						} catch (Exception e) {
							
						}
					}
				}
			}
				
		} catch (Exception e) {
				
		}
		return codedfilename;
	}	
}
