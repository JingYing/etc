package com.jd.oa.form.execute.plugins.imp2exp.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Hashtable;
import java.util.Vector;

import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.StringUtils;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.jd.oa.system.model.SysUser;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author Administrator
 * 
 *         该类用来为表单提供数据导入功能
 * 
 * @preserve 声明此方法不被JOC混淆.
 */
public class ImportExcel {

	/**
	 * 由用户上传Excel数据后，执行导入操作 如果导入失败，提示一个MessaggBox，如果导入成功，刷新表单页面
	 *
	 * @param fileName
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public static String importExcelToFormSheet(String fileName,String path,  String formCode) {
        JdbcTemplate jdbcTemplate = SpringContextUtils.getBean("jdbcTemplate");
		HSSFWorkbook workbook = null;
		FileInputStream fs = null;
		String fullFileName = "";
		if (!fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase().equals("xls")) {
            throw new BusinessException("文件[" + fileName + "]，扩展名非xls");
		}
        File file = new File(path + fileName);
		try {
			fs = new FileInputStream(file);
		} catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException("文件[" + fileName + "]不存在");
		}

		try {
			workbook = new HSSFWorkbook(fs);
		} catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException("文件[" + fileName + "]不符合模板标准，请下载正确的Excle 模版并按照正确方式填写");
		}
//
		// 创建对工作表的引用。
		HSSFSheet sheet = workbook.getSheetAt(0);
		// 第3行定义了导入动作
		int rowIndex = 2;
		HSSFRow row = sheet.getRow(rowIndex);
		HSSFCell cell = row.getCell(0);
		String action = cell.getStringCellValue();
		if (action == null || action.length() < 5 || action.indexOf("导入") != 0) {
			throw new BusinessException("文件[" + fileName + "]不符合模板标准，请下载正确的Excle 模版并按照正确方式填写");
		}
//
		action = action.substring(action.length() - 2).equals("更新") ? "更新" : "追加";
//
		// 第5行默认为是字段行
		rowIndex = 5;
		row = sheet.getRow(rowIndex);
		Hashtable fields = new Hashtable();
		int lastCellNum = row.getLastCellNum();
		for (int i = 0; i < lastCellNum; i++) {
			cell = row.getCell(i);
			String fieldName = "";
			if (cell.getStringCellValue().indexOf("<") > 0) {
				fieldName = cell.getStringCellValue().substring(0, cell.getStringCellValue().indexOf("<"));
			} else {
				fieldName = cell.getStringCellValue();
			}
			fields.put(Integer.valueOf(i), fieldName);
		}
		// 循环excel的数据
		rowIndex++;
		int i = 0;
        SqlAdapter adapter = null;
		try {
			for (i = rowIndex; i <= sheet.getLastRowNum(); i++) {
				// 循环列
                adapter = new MySqlAdapter();

                adapter.addTable(formCode);
                adapter.setSqlType(SqlAdapter.SqlType.DML_INSERT);
                adapter.addColumn("ID",IdUtils.uuid2(),SqlAdapter.DataType.STRING);
				row = sheet.getRow(i);
				for (int column = 0; column < lastCellNum; column++) {
					cell = row.getCell(column);
					Object ofieldName = fields.get(new Integer(column));
					if (ofieldName == null)
						continue;
					String fieldName = ((String) ofieldName).toUpperCase();
					String value = "";
					if (cell == null || cell.getCellType() == HSSFCell.CELL_TYPE_BLANK) {

					} else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						if (cell.getCellStyle().getDataFormat() == 14) {// 日期
							value = DateUtils.dateFormat(cell.getDateCellValue());
                            adapter.addColumn(fieldName, value, SqlAdapter.DataType.DATE);
						} else {
							BigDecimal bd = BigDecimal.valueOf(cell.getNumericCellValue());
							value = bd.toPlainString();
							// 小数位数
							if (value.endsWith(".0")) {
								value = value.substring(0, value.length() - 2);
							}
                            adapter.addColumn(fieldName, value, SqlAdapter.DataType.NUMBER);
						}
					} else if (cell.getCellType() == HSSFCell.CELL_TYPE_BOOLEAN) {
						value = String.valueOf(cell.getBooleanCellValue());
					} else if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
						value = cell.getStringCellValue();
						value = value == null ? "" : value;
                        value = new StringUtils(value).replace("\n", "__eol__");
                        value = value.trim();
                        adapter.addColumn(fieldName, value, SqlAdapter.DataType.STRING);
                    }
				}
                // add id column
                //执行sql
                jdbcTemplate.execute(adapter.toSql());
			}
		} catch (Exception e) {

		} finally {

		}
//
		try {
			fs.close();
			// // 删除临时解密文件
			File delFile = new File(path + fileName);
			delFile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
//
        return "导入成功!";
	}
//	private static String getShowMsg(String msg, boolean success, String formType) {
//		String html = "<script language=JavaScript>parent.pageFrame.refData(parent.pageFrame.document.frmMain,'WorkFlow_Execute_Worklist_BindReport_Refresh');</script>";
//		if (Integer.parseInt(formType) == FormUIUtil.FORM_TYPE_SUB) {
//			html = "<script language=JavaScript>parent.refData(parent.document.frmMain);</script>";
//		} else if (Integer.parseInt(formType) == FormUIUtil.FORM_TYPE_REF) {
//			html = "<script language=JavaScript>parent.pageFrame.AWS_SUB_SHEET_DlgPage.refData(parent.pageFrame.AWS_SUB_SHEET_DlgPage.document.frmMain);</script>";
//		}
//
//		String msgbox = "<script language=JavaScript>alert(\"" + msg + "\");</script>";
//		return success ? html + msgbox : msgbox;
//	}
//
//	/**
//	 * 由用户上传Excel数据后，执行导入操作 如果导入失败，提示一个MessaggBox，如果导入成功，刷新表单页面
//	 *
//	 * @param me
//	 * @param floderName
//	 * @param fileName
//	 * @return
//	 * @preserve 声明此方法不被JOC混淆.
//	 */
//	public static String importExcelToForm(UserContext me, String floderName, String fileName) {
//		String msg = "导入失败,可能格式不符合AWS的Excel数据源标准!";
//		String html = "";
//		HSSFWorkbook workbook = null;
//		FileInputStream fs = null;
//		String fullFileName = "";
//		// 存放@标签对象
//		Vector labelList = new Vector();
//		try {
//			int workflowInstanceId = Integer.parseInt(floderName.substring(0, floderName.indexOf("-")));
//			int formId = Integer.parseInt(floderName.substring(floderName.indexOf("-") + 1));
//			if (fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase().equals("xls")) {
//				// 解密excel文件
//				fullFileName = AWFConfig._awfServerConf.getDocumentPath() + "tmp/groupImportFormData/file" + floderName + "/" + fileName;
//				EncryptFileUtil.decryptFile(fullFileName);
//
//				// 读解密后导入的文件记录
//				fs = new FileInputStream(fullFileName + ".dec");
//				workbook = new HSSFWorkbook(fs);
//
//				// ----------------------------------------------------------
//				// ----获取上传的模板标签及对应数据区域的位置，构造到AWSFormLabelModel
//				// ----提示：系统默认第2页为带@标签的模板
//				HSSFSheet sheetLabel = workbook.getSheetAt(1);
//				HSSFSheet sheetData = workbook.getSheetAt(0);
//				for (int rowIndex = 0; rowIndex < sheetLabel.getLastRowNum(); rowIndex++) {
//					HSSFRow currentRow = sheetLabel.getRow(rowIndex);
//					if (currentRow == null)
//						continue;
//					for (int colIndex = 0; colIndex < currentRow.getLastCellNum(); colIndex++) {
//						HSSFCell currentCell = currentRow.getCell((short) colIndex);
//						if (currentCell == null)
//							continue;
//						String currentValue = null;
//						if (currentCell.getCellType() != HSSFCell.CELL_TYPE_NUMERIC) {
//							currentValue = currentCell.getStringCellValue();
//						}
//						if (currentValue == null)
//							currentValue = "";
//						currentValue = currentValue.trim();
//						if (currentValue.indexOf("[@") == 0 && currentValue.indexOf("]") > 1) {
//							// 发现一个有效标签
//							AWSFormLabelModel formLabelModel = new AWSFormLabelModel();
//							String key = new UtilString(currentValue).matchValue("[@", "]");
//							formLabelModel.setLabelName(key.toUpperCase());
//							formLabelModel.setCol((short) colIndex);
//							formLabelModel.setRow(rowIndex);
//
//							// 获取该位置对应Sheet(0)的值
//							HSSFRow dataSheetRow = sheetData.getRow(rowIndex);
//							HSSFCell dataCell = dataSheetRow.getCell((short) colIndex);
//							if (dataCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
//								currentValue = Double.toString(dataCell.getNumericCellValue());
//								// 排除excel的幂
//								if (currentValue != null) {
//									if (currentValue.toUpperCase().indexOf("E") > 0) {
//										BigDecimal big = new BigDecimal(currentValue);
//										currentValue = big.toString();
//									}
//								}
//							} else {
//								currentValue = dataCell.getStringCellValue();
//								if (dataCell.getCellType() == HSSFCell.CELL_TYPE_FORMULA) {
//									msg = "警告:部分值包含了Excel公式，例如:=SUM(C6:C8)+SUM(C11:C19)，这些公式不能被AWS数据源识别!";
//								}
//							}
//							formLabelModel.setValue(currentValue == null ? "" : currentValue);
//							labelList.add(formLabelModel);
//						}
//					}
//				}
//				// ----------------------已经准备好标签、位置及对应的值---------------
//				if (labelList.size() > 0) {
//					// ----------------------------------------------------------
//					// ----保存数据到当前表单
//					FormModel formModel = (FormModel) FormCache.getModel(formId);
//					if (formModel != null) {
//						SheetModel sheetModel = (SheetModel) SheetCache.getMastSheetModel(formModel._id);
//						// 对应的MetaDataModel
//						MetaDataModel metaDataModel = (MetaDataModel) MetaDataCache.getModel(sheetModel._metaDataId);
//						int boid = DBSql.getInt("select id from " + metaDataModel._entityName + " where bindid=" + workflowInstanceId, "id");
//						RuntimeFormManager runtimeFormManager = new RuntimeFormManager(me, workflowInstanceId, 0, TaskStateConst.WORK_TYPE_TRANSACT, boid, formId);
//						// 表单数据
//						StringBuffer bindData = new StringBuffer();
//						for (int i = 0; i < labelList.size(); i++) {
//							AWSFormLabelModel labelModel = (AWSFormLabelModel) labelList.get(i);
//							bindData.append("_").append(labelModel.getLabelName()).append("{").append(labelModel.getValue()).append("}").append(labelModel.getLabelName()).append("_ ");
//						}
//						int r = runtimeFormManager.saveData(bindData.toString());
//						if (r > 0) {
//							html = "<script language=JavaScript>parent.pageFrame.refData(parent.pageFrame.frmMain,'WorkFlow_Execute_Worklist_BindReport_Refresh');</script>";
//							// 标志成功
//							msg = "\\n\\n导入成功!";
//						} else {
//							msg = "导入失败，数据格式与数据库格式不匹配或溢出!要排除错误，请查看系统日志记录";
//						}
//					} else {
//						msg = "没有发现对应的表单模型,导入失败!";
//					}
//				} else {
//					msg = "文件[" + fileName + "]不符合AWS规定的Excel数据源格式标准，没有发现[@???]标签!";
//				}
//			} else {
//				msg = msg + "文件[" + fileName + "]后缀不是.xls";
//			}
//		} catch (Exception e) {
//			try {
//				fs.close();
//				// 删除临时解密文件
//				File delFile = new File(fullFileName + ".dec");
//				delFile.delete();
//			} catch (Exception ee) {
//			}
//			e.printStackTrace(System.err);
//			msg = msg + e.toString();
//		}
//		String msgbox = "<script language=JavaScript>alert(\"" + msg + "\");</script>";
//		return html + msgbox;
//	}
//
//}
//
//class AWSFormLabelModel {
//	private String labelName;
//
//	private short col;
//
//	private int row;
//
//	private String value;
//
//	/**
//	 * @return Returns the col.
//	 */
//	public short getCol() {
//		return col;
//	}
//
//	/**
//	 * @param col
//	 *            The col to set.
//	 */
//	public void setCol(short col) {
//		this.col = col;
//	}
//
//	/**
//	 * @return Returns the labelName.
//	 */
//	public String getLabelName() {
//		return labelName;
//	}
//
//	/**
//	 * @param labelName
//	 *            The labelName to set.
//	 */
//	public void setLabelName(String labelName) {
//		this.labelName = labelName;
//	}
//
//	/**
//	 * @return Returns the row.
//	 */
//	public int getRow() {
//		return row;
//	}
//
//	/**
//	 * @param row
//	 *            The row to set.
//	 */
//	public void setRow(int row) {
//		this.row = row;
//	}
//
//	/**
//	 * @return Returns the value.
//	 */
//	public String getValue() {
//		return value;
//	}
//
//	/**
//	 * @param value
//	 *            The value to set.
//	 */
//	public void setValue(String value) {
//		this.value = value;
//	}
}