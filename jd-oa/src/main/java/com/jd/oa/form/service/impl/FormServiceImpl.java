package com.jd.oa.form.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jd.common.context.PagedListBean;
import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.service.ProcessDefService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.form.dao.FormBusinessFieldDao;
import com.jd.oa.form.dao.FormBusinessTableDao;
import com.jd.oa.form.dao.FormDao;
import com.jd.oa.form.dao.FormItemDao;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormItemBean;
import com.jd.oa.form.service.FormBusinessFieldService;
import com.jd.oa.form.service.FormService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单ServiceImpl
 * User: yujiahe
 * Date: 13-9-12
 * Time: 上午11:56
 * To change this template use File | Settings | File Templates.
 */
// Spring Service Bean的标识.
@Service("formService")
// 默认将类中的所有函数纳入事务管理.
@Transactional
public class FormServiceImpl extends BaseServiceImpl<Form, String> implements FormService {
    private static final Logger logger = Logger.getLogger(FormServiceImpl.class);
    @Autowired
    private FormDao formDao;
    @Autowired
    private FormItemDao formItemDao;
    @Autowired
    private FormBusinessTableDao formBusinessTableDao;
    @Autowired
    private FormBusinessFieldDao formBusinessFieldDao;
    @Autowired
    private FormBusinessFieldService formBusinessFieldService;
    private FormBusinessFieldServiceImpl formBusinessFieldServiceImpl;
    //    @Autowired
//    private FormService formService;
    @Autowired
    private ProcessDefService processDefService;

    public FormDao getDao() {
        return formDao;
    }

    /**
     * @param form
     * @return String
     * @Description: 根据表单名称查看表单是否存在
     * @author yujiahe
     */
    @Override
    public boolean isExistForm(Form form) {
        try {
            return formDao.isExisForm(form);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查看类别是否存在失败", e);
        }
    }

    /**
     * @param owner
     * @param ids
     * @return String
     * @Description: 保存变更所有人
     * @author yujiahe
     */
    @Override
    public Map<String, Object> updateOwner(String owner, String ids) {
        Map<String, Object> map = new HashMap<String, Object>();
        String[] args = ids.split(",");
        boolean flag = true; //默认没有重名
        String allFormName = "";
        StringBuilder allFormNameSb = new StringBuilder("");
        for (String id : args) {
            Form formCheck = new Form();
            //拿到 要变更所有人的表单的 名称
            formCheck.setFormName(formDao.get(id).getFormName());
            //查询一下 该所有人名下是否有这个名称的表单
            formCheck.setOwner(owner);
            if (isExistForm(formCheck)) {  //如果存在 则属于重名
                flag = false;//有重名
//                allFormName += "[" + formCheck.getFormName() + "] ";//记录表单名称
                allFormNameSb.append("[" + formCheck.getFormName() + "] ");
            }
        }
        allFormName = allFormNameSb.toString();
        if (flag) {//如果没有重名
            for (String id : args) {
                Form form = new Form();
                form.setOwner(owner); //查询现有id 的表单的名字 在 拥有人的表单里面是否重名
                form.setId(id);
                formDao.update(form);
            }
            map.put("operator", false);
            map.put("message", "变更所有人成功");
            return map;
        } else { //有重名 就返回提示语句
            map.put("operator", true);
            map.put("message", "该用户下已存在表单名称为" + allFormName + "的表单，请修改表单名称后再变更所有人！");
            return map;
        }

    }

    /**
     * @Description:删除表单支持批量
     * @author yujiahe
     */
    @Override
    @Transactional
    public void mutidelete(String ids, String formType) {
        String[] args = ids.split(",");
        String message = "";
        try {
            for (String id : args) {
                //查询在流程定义中是否引用此表单
                ProcessDefinition processDefinition = new ProcessDefinition();
                processDefinition.setFormId(id);
                int num = processDefService.find(processDefinition).size();
                if (num > 0) {
                    message = "此表单已经被流程引用，无法删除";
                    throw new BusinessException(message);
                } else {
                    message = "删除表单失败";
                    Form form = formDao.get(id);
                    if (formType.equals("M")) {  //如果为主表则需要删除其下面的子表
                        operateFormAndFormItem(id);//删除子表
                    }
                    String createType = form.getCreateType();
                    //高级模式 只删除 form 相关数据
                    formDao.delete(id);//删除表单
                    formItemDao.delete(formItemDao.selectByFormId(id)); //删除表单项
                    if (null != createType && !createType.equals("advancedModule")) { //标准模式、复制模式删除数据库表及业务数据表等
                        if (formType.equals("M")) {//如果删除主表则删除删除其下子表的数据库表及业务表
                            Form sonForm = new Form();
                            sonForm.setParentId(id);
                            List<Form> sonFormList = formDao.find(sonForm);
                            if (sonFormList.size() != 0) {
                                for (Form delSonForm : sonFormList) {
                                    delBusinessTableAndPhysicalTable(delSonForm);
                                }
                            }

                        }
                        delBusinessTableAndPhysicalTable(form);
                    }

                }
            }
        } catch (Exception e) {
            throw new BusinessException(message, e);
        }
    }

    /**
     * 删除数据库表及业务表失败
     *
     * @param form
     */
    private void delBusinessTableAndPhysicalTable(Form form) {
        try {
            if (form.getTableId() != null && !form.getTableId().equals("")) {
                FormBusinessTable formBusinessTable = formBusinessTableDao.get(form.getTableId());
                formBusinessTableDao.delete(formBusinessTable);//删除数据库表
                formBusinessFieldDao.delete(formBusinessFieldDao.selectTableFieldsByTableId(formBusinessTable.getId()));//删除数据表项
                //物理删除业务数据表
                String tableName = formBusinessTable.getTableName();
                if (null != tableName && !tableName.equals("")) {
                    if (formBusinessFieldService.isExistBusinessTable(tableName)) {
                        SqlAdapter adapter = new MySqlAdapter();
                        adapter.setSqlType(SqlAdapter.SqlType.DDL_DROP_TABLE).addTable(formBusinessTable.getTableName());
                        sql(adapter);
                    }
                }
            }

        } catch (Exception e) {
            throw new BusinessException("删除数据库表及业务表失败", e);
        }

    }

    /**
     * @param id
     * @return Form
     * @Description: 根据ID查询信息
     * @author xulin
     * @date 2013-9-16下午01:49:54
     * @version V1.0
     */
    public Form selectByPrimaryKey(String id) {
        return this.formDao.selectByPrimaryKey(id);
    }

    /**
     * @param tableId
     * @return
     * @author xulin
     */
    public List<FormItemBean> findFormItemsByTableId(String tableId) {
        return this.formDao.findFormItemsByTableId(tableId);
    }

    /**
     * 高级模式下更新表单和表单明细项
     *
     * @param mainFormModel
     * @param subFormModelList
     * @param formItemList
     */
    public void operateFormAndFormItem(Form mainFormModel, List<Form> subFormModelList, List<FormItem> formItemList) {
        //删除当前表单下的子表单及其表单明细项
        String formId = mainFormModel.getId();
        List<FormItem> oldFormItemList = new ArrayList<FormItem>();
        List<Form> subFormForDel = new ArrayList<Form>();
        List<Form> subFormForAdd = new ArrayList<Form>();
        List<FormItem> tempList = null;
        tempList = this.formItemDao.selectByFormId(formId);//已有的主表表单项
        if (tempList != null && tempList.size() > 0) {
            for (FormItem item : tempList) {
                oldFormItemList.add(item);
            }
        }
        List<Form> oldSubForm = mainFormModel.getSubTables();//已有的子表表单项
        boolean hasOldSubForm = (oldSubForm!=null);
        if (hasOldSubForm) {
            for (Form form : oldSubForm) {
                formId = form.getId();
                tempList = this.formItemDao.selectByFormId(formId);
                if (tempList != null && tempList.size() > 0) {
                    for (FormItem item : tempList) {
                        oldFormItemList.add(item);
                    }
                }
            }
        }
        List<FormItem> itemsForAdd = new ArrayList<FormItem>();
        List<FormItem> itemsForDel = new ArrayList<FormItem>();
        for (FormItem o : oldFormItemList) {
            boolean flag = false;
            for (FormItem n : formItemList) {
                if (n.getFieldId().equals(o.getFieldId())) {
                    flag = true;
                }
            }
            if (!flag) {
                itemsForDel.add(o);
            }
        }
        for (FormItem n : formItemList) {
            boolean flag = false;
            for (FormItem o : oldFormItemList) {
                if (n.getFieldId().equals(o.getFieldId())) {
                    flag = true;
                }
            }
            if (!flag) {
                itemsForAdd.add(n);
            }
        }
        if(hasOldSubForm){
        	for(Form o :oldSubForm){
                boolean flag = false;
                for(Form n:subFormModelList ){
                    if(n.getTableId().equals(o.getTableId())&&n.getFormCode().equals(o.getFormCode())){
                        flag = true;
                    }
                }
                if(!flag){

                    subFormForDel.add(o);
                }
            }
        }
        
        for(Form n:subFormModelList){
            boolean flag = false;
            for(Form o:oldSubForm ){
                if(n.getTableId().equals(o.getTableId())&&n.getFormCode().equals(o.getFormCode())){
                    flag = true;
                }
            }
            if(!flag){
                subFormForAdd.add(n);
            }
        }


        //删除子表单
        this.formDao.delete(subFormForDel);// old 中有 new 中没有的
        //删除子表单明细项
        this.formItemDao.delete(itemsForDel);
        //更新主表单
        this.formDao.update(mainFormModel);
        //插入子表单
        this.formDao.insert(subFormForAdd);//old中没有 new中有的
        //插入表单明细项
        this.formItemDao.insert(itemsForAdd);
    }

    /**
     * 高级模式下删除表单下的子表单和表单明细项
     *
     * @param formId
     * @author xulin
     */
    public void operateFormAndFormItem(String formId) {
        //删除当前表单下的子表单及其表单明细项
        List<FormItem> oldFormItemList = new ArrayList<FormItem>();
        List<FormItem> tempList = null;
        tempList = this.formItemDao.selectByFormId(formId);
        Form mainForm = this.formDao.get(formId);
        if (tempList != null && tempList.size() > 0) {
            for (FormItem item : tempList) {
                oldFormItemList.add(item);
            }
        }
        List<Form> oldSubForm = mainForm.getSubTables();
        if (oldSubForm != null && oldSubForm.size() > 0) {
            for (Form form : oldSubForm) {
                formId = form.getId();
                tempList = this.formItemDao.selectByFormId(formId);
                if (tempList != null && tempList.size() > 0) {
                    for (FormItem item : tempList) {
                        oldFormItemList.add(item);
                    }
                }
            }
        }

        //删除子表单
        this.formDao.delete(oldSubForm);
        //删除子表单明细项
        this.formItemDao.delete(oldFormItemList);
    }

    /**
     * @param tableId
     * @param resultFormTable
     * @param resultTableFields
     * @Description: 自动生成业务表及其下属字段
     * @author xulin
     * @date 2013-9-25下午06:22:13
     * @version V1.0
     */
    @Transactional
    public Map<String, Object> createBusinessTableAuto(String tableId, List<FormBusinessTable> resultFormTable,
                                                       List<FormBusinessField> resultTableFields, List<Form> resultForm, boolean check) {
        //删除主表子表及其下属字段
        List<FormBusinessTable> oldTablaList = new ArrayList<FormBusinessTable>();
        List<FormBusinessField> oldFieldsList = new ArrayList<FormBusinessField>();
        List<FormBusinessField> tempList = null;
        FormBusinessTable oldTable = this.formBusinessTableDao.get(tableId);
        if (oldTable != null) {
            oldTablaList.add(oldTable);
            tempList = this.formBusinessFieldDao.selectTableFieldsByTableId(tableId);
            if (tempList != null && tempList.size() > 0) {
                for (FormBusinessField fieldModel : tempList) {
                    oldFieldsList.add(fieldModel);
                }
            }
            for (FormBusinessTable subTable : oldTable.getSubTables()) {
                oldTablaList.add(subTable);
                tempList = this.formBusinessFieldDao.selectTableFieldsByTableId(subTable.getId());
                if (tempList != null && tempList.size() > 0) {
                    for (FormBusinessField fieldModel : tempList) {
                        oldFieldsList.add(fieldModel);
                    }
                }
            }
        }
        Map<String, Object> result;
        if (check) {//没有业务数据的情况
            //根据TableId删除已创建过的业务表
            if (oldTablaList.size() > 0) {
                this.formBusinessTableDao.delete(oldTablaList, true);
            }
            if (oldFieldsList.size() > 0) {
                this.formBusinessFieldDao.delete(oldFieldsList, true);
            }
            //更新表单对应的tableId
            this.formDao.update(resultForm);
            //生成新的业务表
            this.formBusinessTableDao.insert(resultFormTable);
            this.formBusinessFieldDao.insert(resultTableFields);

            //依据新生成的业务表创建物理表
            result = this.formBusinessFieldService.createTableIncludeChild(tableId);
        } else {//有业务数据的情况
            //找到需要更新 和需要新增的
            List<FormBusinessField> updateFieldsList = new ArrayList<FormBusinessField>();
            List<FormBusinessField> addFieldsList = new ArrayList<FormBusinessField>();
            for (FormBusinessField n : resultTableFields) {
                boolean flag = false;
                for (FormBusinessField o : oldFieldsList) {
                    if (n.getTableId().equals(o.getTableId()) && n.getFieldName().equals(o.getFieldName())) {
                        n.setId(o.getId());
                        flag = true;
                        if (StringUtils.isNotEmpty(n.getLength())&&StringUtils.isNotEmpty(o.getLength())){
                            if(n.getLength().indexOf(",")>0&&o.getLength().indexOf(",")>0
                                    &&(Integer.parseInt(n.getLength().split(",")[0])>Integer.parseInt(o.getLength().split(",")[0]))
                                     &&(Integer.parseInt(n.getLength().split(",")[1])>Integer.parseInt(o.getLength().split(",")[1]))){//都是decimal  且 都大于
                                updateFieldsList.add(n);
                            }else if(n.getLength().indexOf(",")<0&&o.getLength().indexOf(",")<0 &&( Integer.parseInt(n.getLength()) > Integer.parseInt(o.getLength()))){//都是int 且大于之前的
                                updateFieldsList.add(n);
                            }else if(n.getLength().indexOf(",")>0&&o.getLength().indexOf(",")<0){//int 转decimal 全部放过
                                updateFieldsList.add(n);
                            }

                        }
                    }
                }
                if (!flag) {
                    addFieldsList.add(n);
                }
            }
            result = formBusinessFieldService.updateBusinessTable("", addFieldsList, updateFieldsList);
            if(result.get("operator").equals(true)){
                try{
                this.formBusinessFieldDao.update(updateFieldsList);
                this.formBusinessFieldDao.insert(addFieldsList);
                }catch (Exception e){
                    logger.error(e);
                    throw new BusinessException("同步业务表失败", e);
                }
            }

        }
        return result;
    }

    /**
     * @param newFormList
     * @param newFormItemList
     * @Description: 复制模式时创建表单及其表单项目
     * @author xulin
     * @date 2013-9-26下午02:09:13
     * @version V1.0
     */
    public void createFormAndFormItem(List<Form> newFormList, List<FormItem> newFormItemList) {

        //插入表单
        this.formDao.insert(newFormList);
        if (newFormItemList != null && newFormItemList.size() > 0) {
            //插入表单明细项
            this.formItemDao.insert(newFormItemList);
        }
    }

    /**
     * @param processDefintonId
     * @return
     * @author xulin
     */
    public String getFormCodeByProDId(String processDefintonId) {
        return this.formDao.getFormCodeByProDId(processDefintonId);
    }

    public Map<String, Object> getList(String sql, String type) {
        return this.formDao.getList(sql, type);
    }
    
    public List<Form> findFormWithSubForm(String formId) {
    	Form form = new Form();
    	form.setId(formId);
    	return this.formDao.findFormWithSubForm(form);
    }
}
