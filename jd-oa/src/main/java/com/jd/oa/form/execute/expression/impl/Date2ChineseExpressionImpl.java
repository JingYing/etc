package com.jd.oa.form.execute.expression.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
/**
 * 日期转换成中文日期表达式
 * @author birkhoff
 *
 */
public class Date2ChineseExpressionImpl extends ExpressionAbst{
	public Date2ChineseExpressionImpl(Map<String,Object> paramMaps, String expressionValue) {
		super(paramMaps, expressionValue);
	}

	public String expressionParse(String expression) {
		String date = getParameter(expression, 1).trim();
		return date2Upper(new Date(DateUtils.getTimes(date)));
	}

	// 日期转化为大小写
	public static String date2Upper(Date date) {
		Calendar ca = Calendar.getInstance();
		ca.setTime(date);
		int year = ca.get(Calendar.YEAR);
		int month = ca.get(Calendar.MONTH) + 1;
		int day = ca.get(Calendar.DAY_OF_MONTH);
		return numToUpper(year) + "年" + monthToUppder(month) + "月" + dayToUppder(day) + "日";
	}

	// 将数字转化为大写
	private static String numToUpper(int num) {
		String u[] = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
		char[] str = String.valueOf(num).toCharArray();
		String rstr = "";
		for (int i = 0; i < str.length; i++) {
			rstr = rstr + u[Integer.parseInt(str[i] + "")];
		}
		return rstr;
	}

	// 月转化为大写
	private static String monthToUppder(int month) {
		if (month < 10) {
			return numToUpper(month);
		} else if (month == 10) {
			return "十";
		} else {
			return "十" + numToUpper(month - 10);
		}
	}

	// 日转化为大写
	private static String dayToUppder(int day) {
		if (day < 20) {
			return monthToUppder(day);
		} else {
			char[] str = String.valueOf(day).toCharArray();
			if (str[1] == '0') {
				return numToUpper(Integer.parseInt(str[0] + "")) + "十";
			} else {
				return numToUpper(Integer.parseInt(str[0] + "")) + "十" + numToUpper(Integer.parseInt(str[1] + ""));
			}
		}
	}
}
