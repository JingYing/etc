package com.jd.oa.form.dao;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.FormBusinessTable;

/** 
 * @Description: 自定义表Dao
 * @author guoqingfu
 * @date 2013-9-12 上午10:49:56 
 * @version V1.0 
 */
public interface FormBusinessTableDao extends BaseDao<FormBusinessTable, String>{

	/**
	  * @Description: 查询主表列表
	  * @return List<FormBusinessTable>
	  * @author guoqingfu
	  * @date 2013-9-12下午01:53:44 
	  * @version V1.0
	 */
	public List<FormBusinessTable> findParentTableList();
	/**
	  * @Description:根据ID查询数据表信息
	  * @param formBusinessTableId
	  * @return FormBusinessTable
	  * @author guoqingfu
	  * @date 2013-9-12下午02:44:03 
	  * @version V1.0
	 */
	public FormBusinessTable getFormBusinessTableInfo(String formBusinessTableId);
	/**
	  * @Description: 判断是否有字段或是已被表单引用
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午03:59:31 
	  * @version V1.0
	 */
	public boolean isUsed(Map<String, Object> map);
	/**
	  * @Description: 删除自定义表
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午04:27:02 
	  * @version V1.0
	 */
	public boolean deleteFormBusinessTable(Map<String, Object> map);
	
	/**
	  * @Description: 判断是否存在数据表名
	  * @param tableChineseName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:00:24 
	  * @version V1.0
	 */
	public boolean isExistTableChineseName(FormBusinessTable formBusinessTable);
	/**
	  * @Description: 判断是否存在数据库表名
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:33:41 
	  * @version V1.0
	 */
	public boolean isExistTableName(FormBusinessTable formBusinessTable);
	
	/**
	 * @Description:根据表ID列表查询表名称列表
	  * @param map
	  * @return List<String>
	  * @author guoqingfu
	  * @date 2013-10-23上午10:53:43 
	  * @version V1.0
	 */
	public List<String> findTableNameList(Map<String,Object> map);
}
