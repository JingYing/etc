package com.jd.oa.form.execute;

import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.form.execute.expression.EXPParser;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessNodeExtendButton;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.service.ProcessNodeExtendButtonService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuntimeFormManager {
	private static final String TAG_HEAD = "[@";

	private static final String TAG_TAIL = "]";
	
	
	public Form _form;//表单ID
	
	public String _processNodeId;//流程节点ID
	
	public String _businessObjectId;//业务数据ID
	
	public String _businessTableName;//业务数据表名称
	
	public List<FormItem> _items;
	
	public Map<String,Object> _businessData;
	
	private List<ProcessNodeFormPrivilege> _processNodeFormPrivileges;
	
	public ProcessDefinition _processDefinition;
	
	public ProcessInstance _processInstance;
	
	public Map<String,Object> _processNodeFormPrivilege = new HashMap<String,Object>();
	
	public List<FormBean> _subSheets;
	
	public String _contextPath ;
	
	private String extendButtonJavascriptContext;
	
	private ProcessNodeExtendButtonService processNodeExtendButtonService;//流程节点上扩展按钮service
	
	public RuntimeFormManager(String contextPath){
		this._contextPath = contextPath;
		if(processNodeExtendButtonService == null){
			processNodeExtendButtonService = SpringContextUtils.getBean(ProcessNodeExtendButtonService.class);
		}
	}
	
	/**
	 * 
	 * @param items
	 * @param processNodeFormPrivileges
	 * @param businessData
	 * @param processDefinition
	 * @param form
	 * @param processNodeId
	 * @param processInstance
	 * @param businessTableName
	 * @param businessObjectId
	 */
	public RuntimeFormManager(String contextPath,List<FormItem> items,List<ProcessNodeFormPrivilege> processNodeFormPrivileges,Map<String,Object> businessData,ProcessDefinition processDefinition,Form form,List<FormBean> subSheets,String processNodeId,ProcessInstance processInstance,String businessTableName,String businessObjectId){
		this._items = items;
		this._processNodeFormPrivileges = processNodeFormPrivileges;
		this._businessData = businessData;
		this._businessObjectId = businessObjectId;
		this._businessTableName = businessTableName;
		this._processDefinition = processDefinition;
		this._processInstance = processInstance;
		this._subSheets = subSheets;
		this._processNodeId = processNodeId;
		this._contextPath = contextPath;
		if(processNodeExtendButtonService == null){
			processNodeExtendButtonService = SpringContextUtils.getBean(ProcessNodeExtendButtonService.class);
		}
	}
	public RuntimeFormManager(String contextPath,List<FormItem> items,ProcessDefinition processDefinition){
		this._items = items;
		this._contextPath = contextPath;
		this._processDefinition = processDefinition;
		if(processNodeExtendButtonService == null){
			processNodeExtendButtonService = SpringContextUtils.getBean(ProcessNodeExtendButtonService.class);
		}
	}
	/**
	 * 表单内容
	 * @param request
	 * @return
	 */
	public Map<String,String> getFormPage(String contextPath,boolean isEdit,boolean isApply){
		if (this._businessObjectId != null && this._businessObjectId.trim().length() > 0) {
			
		}
    	Map<String , ProcessNodeFormPrivilege> maps = getProcessNodeFormPrivilegeItemListToMap(this._processNodeFormPrivileges);
    	for(int i = 0 ; i < this._items.size() ; i++){//主表表单项
    		FormItem formItem = this._items.get(i);
    		if(formItem != null && formItem.getFormId() != null){
    			ProcessNodeFormPrivilege processNodeFormPrivilege = maps.get(formItem.getId());
    			if(processNodeFormPrivilege != null){//设置流程节点表单字段个性化必填，只读，隐藏权限
    				if(formItem.getIsEdit() == null || formItem.getIsEdit().trim().length() == 0 || formItem.getIsEdit().equals("0")){
    					formItem.setIsHidden(processNodeFormPrivilege.getHidden());
    					formItem.setIsEdit(processNodeFormPrivilege.getEdit());
    					formItem.setIsNull(processNodeFormPrivilege.getNull());
    				}
    			}
    		}
    	}
    	Map<String,String> uiTags = null;
    	if(isApply){//申请节点，进缓存
    		 uiTags = getRuntimeFormForApply(contextPath,this._items,isEdit);
    	}else{
    		uiTags = getRuntimeForm(contextPath,this._items,isEdit);
    	}
		uiTags.putAll(getRuntimeFormSubSheet(this._subSheets,isEdit));
		return uiTags;
	}
	/**
	 * 运行时刻 表单数据UI生成
	 * @param request
	 * @param items List
	 * @return
	 */
	public Map<String,String> getRuntimeForm(String contextPath,List<FormItem> items){
		return getRuntimeForm(contextPath,items,true);
	}
	/**
	 * 运行时刻 表单数据UI生成
	 * @param request
	 * @param items
	 * @param isEdit true:可编辑，false:只读
	 * @return
	 */
	public Map<String,String> getRuntimeForm(String contextPath,List<FormItem> items,boolean isEdit){
		return new FormUIUtil(this).buildMasterHashtableOfVerify(contextPath,addProcessFormDefaultField(items),isEdit);
	}
	
	/**
	 * 运行时刻 申请节点表单数据UI生成
	 * @param request
	 * @param items
	 * @param isEdit true:可编辑，false:只读
	 * @return
	 */
	public Map<String,String> getRuntimeFormForApply(String contextPath,List<FormItem> items,boolean isEdit){
		return new FormUIUtil(this).buildMasterHashtableOfVerifyForApply(contextPath,addProcessFormDefaultField(items),isEdit);
	}
	/**
	 * 查询条件动态增加代码返回
	 * @param contextPath
	 * @param items
	 * @return
	 */
	public String getDynamicQueryConditions(){
		return new FormUIUtil(this).getHtmlForQueryCondition(this._contextPath, this._items);
	}
	public List<FormItem>  addProcessFormDefaultField(List<FormItem> items){
		List<FormItem> formItems = new ArrayList<FormItem>();
		formItems.addAll(RuntimeDefaultFormUIComponent.getFormDefaultField(this._processDefinition,this._processInstance));
		formItems.addAll(items);
		return formItems;
	}
	
	/**
	 * 转换formItem 的 list对象为map对象 key为formItem id
	 * @param items
	 * @return
	 */
	public Map<String,ProcessNodeFormPrivilege> getProcessNodeFormPrivilegeItemListToMap(List<ProcessNodeFormPrivilege> processNodeFormPrivileges){
		Map<String ,ProcessNodeFormPrivilege> maps = new HashMap<String,ProcessNodeFormPrivilege>();
		if(processNodeFormPrivileges != null && processNodeFormPrivileges.size() > 0){
			for(int i = 0 ; i < processNodeFormPrivileges.size() ; i++){
				ProcessNodeFormPrivilege processNodeFormPrivilege = processNodeFormPrivileges.get(i);
				if(processNodeFormPrivilege != null){
					maps.put(processNodeFormPrivilege.getItemId(), processNodeFormPrivilege);
				}
			}
		}
		return maps;
	}
	/**
	 * 运行时刻 子表数据表格生成
	 * @param formId
	 * @param subSheets
	 * @return
	 */
	public Map<String,String> getRuntimeFormSubSheet(List<FormBean> subSheets,boolean isEdit){
		Map<String,String> mapSubSheet = new HashMap<String,String>();
		int j = 0;
		for( int i = 0 ; i < subSheets.size() ; i++){
			FormBean formBean = subSheets.get(i);
			if(formBean != null){
				List<FormItem> formItems = setProcessNodePrivilege(formBean.getListFormItems());
				boolean isAllColumnEditAc = checkSubSheetIsEditAc(formItems);//是否所有列都不允许编辑
				boolean isAllColumnHiddenAc = checkSubSheetIsHiddenAc(formItems);//是否有列隐藏
				StringBuffer sheetStr = new StringBuffer();
				if(isAllColumnHiddenAc){//判断该子表的列是否全部隐藏，如果有一列显示，则显示该子表,否则不显示该子表
					String subSheetHtml = getSubSheetBody(formBean.getFormCode(),formBean.getFormId(),formBean.getFormName(), formBean.getListFormItems(),isEdit);;
					sheetStr.append("<table width=\"100%\" border=\"0\">\n");
					sheetStr.append("<tr>\n");
					sheetStr.append(" <td colspan=\"3\" align=\"left\" style='color:green;font-weight:bold;'><img style='width:16px;height:16px;' src='../static/app/image/data-table.png'/> 明细表_").append(formBean.getFormName()).append("</td>\n");
					sheetStr.append("</tr>\n");
					if(isEdit){
						sheetStr.append("<tr>\n");
						sheetStr.append(" <td  align=\"left\">").append(getSubSheetToolbar(formBean.getFormCode(),formBean.getFormId(),formBean.getFormName(),isAllColumnEditAc,false)).append("</td>\n");
						sheetStr.append("</tr>\n");
					}
					sheetStr.append("<tr>\n");
					sheetStr.append(" <td>").append(subSheetHtml).append("</td>\n");
					sheetStr.append(" </tr>\n");
					//表格下面再加一排动态按钮
					if(isEdit){
						sheetStr.append("<tr>\n");
						sheetStr.append(" <td  align=\"left\">").append(getSubSheetToolbar(formBean.getFormCode(),formBean.getFormId(),formBean.getFormName(),isAllColumnEditAc,true)).append("</td>\n");
						sheetStr.append("</tr>\n");
					}
					
					sheetStr.append("</table>\n");
				}
				mapSubSheet.put("SUBSHEET" + j, sheetStr.toString());
				j++;
			}
		}
		return mapSubSheet;
	}
	/**
	 * 检查子表是否可编辑
	 * @param formItems
	 * @return
	 */
	private boolean checkSubSheetIsHiddenAc(List<FormItem> formItems){
		for(int i = 0 ; i < formItems.size() ; i++){
			FormItem formItem = formItems.get(i);
			if(formItem != null){
				if(formItem.getIsHidden() == null || formItem.getIsHidden().equals("0")){
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * 检查子表是否可编辑
	 * @param formItems
	 * @return
	 */
	private boolean checkSubSheetIsEditAc(List<FormItem> formItems){
		for(int i = 0 ; i < formItems.size() ; i++){
			FormItem formItem = formItems.get(i);
			if(formItem != null){
				if(formItem.getIsEdit() != null && formItem.getIsEdit().equals("1")){//0：只读，1：可编辑
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * 根据流程节点表单个性化设置字段是否隐藏，可编辑，必填
	 * @param formItems
	 */
	private List<FormItem> setProcessNodePrivilege(List<FormItem> formItems){
		Map<String , ProcessNodeFormPrivilege> maps = getProcessNodeFormPrivilegeItemListToMap(this._processNodeFormPrivileges);
		for(int i = 0 ; i < formItems.size() ; i++){
    		FormItem metaDataMap = formItems.get(i);
    		if(metaDataMap != null && metaDataMap.getFormId() != null){
    			ProcessNodeFormPrivilege processNodeFormPrivilege = maps.get(metaDataMap.getId());
    			if(processNodeFormPrivilege != null){//设置流程节点表单字段个性化必填，只读，隐藏权限
    				if(metaDataMap.getIsEdit() == null || metaDataMap.getIsEdit().trim().length() == 0 || metaDataMap.getIsEdit().equals("0")){
    					metaDataMap.setIsHidden(processNodeFormPrivilege.getHidden());
    					metaDataMap.setIsEdit(processNodeFormPrivilege.getEdit());
    					metaDataMap.setIsNull(processNodeFormPrivilege.getNull());
    				}
    			}
    		}
		}
		return formItems;
	}
	/**
	 * 子表toolbar
	 * @param formCode 
	 * @param formId
	 * @param formName
	 * @param isAllColumnEditAc 是否所有列都允许编辑
	 * @param isAllowOneColumnEditAc 是否只有一列允许编辑
	 * @param isBootomButton 是否表格底部按钮
	 * @return
	 */
	private String getSubSheetToolbar(String formCode,String formId,String formName,boolean isAllColumnEditAc,boolean isBootomButton){
		StringBuffer toolbar = new StringBuffer();
		toolbar.append("<table border=\"0\">\n");
		toolbar.append(" <tr>\n");
//		if(isAllColumnEditAc){//所有列都允许编辑，显示" "
//			toolbar.append("  <td><button type=\"button\" onClick=\"subSheetNew_").append(formCode).append("('").append(formCode).append("');return false;\" class=\"btn\" title='新增'>新增</button></td>\n");
//			toolbar.append(" <td><button type=\"button\"  onClick=\"subSheetDelete('").append(formCode).append("');return false;\" class=\"btn\" title='删除'>删除</button></td>\n");
//			toolbar.append(" <td><button type=\"button\"  onClick=\"subSheetSave('").append(formCode).append("');return false;\" class=\"btn\" title='保存'>保存</button></td>\n");
//		}
//		//控制子表按钮权限:只有一列可编辑，显示保存。如果是所有列可编辑，显示保存，新培，删除
//		
//		toolbar.append(" <td><button type=\"button\"  onClick=\"subSheetRefresh('").append(formCode).append("');return false;\" class=\"btn\" title='刷新'>刷新</button></td>\n");
		//动态按钮
		toolbar.append(" <td  align=\"left\">").append(this.getProcessNodeExtendButtons(formId,isAllColumnEditAc,isBootomButton)).append("</td>\n");
//		if(isAllColumnEditAc){
//			RuntimeComputeExpressManager cptExp = new RuntimeComputeExpressManager(this);
//			if(cptExp.checkRowOfRules(formCode)){
//				toolbar.append(" <td><button type=\"button\"  onClick=\"subSheetSum('").append(formCode).append("');return false;\" class=\"btn\" title='汇总'>汇总</button></td>\n");
//			}
//		}
		toolbar.append(" </tr>\n");
		toolbar.append("  </table>");
		return toolbar.toString();
	}
	/**
	 * 获取流程节点子表的动态按钮
	 * @param processId
	 * @param nodeId
	 * @param formId
	 * @param formName
	 * @return
	 */
	private String getProcessNodeExtendButtons(String formId,boolean isAllColumnEditAc, boolean isBootomButton){
		StringBuffer extendButtons = new StringBuffer();
		ProcessNodeExtendButton entity = new ProcessNodeExtendButton();
		entity.setFormId(formId); 
		entity.setProcessDefinitionId(this._processDefinition.getId());
		entity.setNodeId(this._processNodeId);
		if(isBootomButton){
			entity.setIsExtendButton(0);
		}
		List<ProcessNodeExtendButton> list = this.processNodeExtendButtonService.selectByCondition(entity);
				//find(entity);
		if(list != null && list.size() > 0){
			for(int i = 0 ; i < list.size() ; i++){
				ProcessNodeExtendButton processNodeExtendButton = list.get(i);
				if(processNodeExtendButton != null){
					if(processNodeExtendButton.getIsExtendButton() == 0){//系统内置按钮
						if(isAllColumnEditAc){
							extendButtons.append("<button type=\"button\"  onClick=\"")
										 .append(processNodeExtendButton.getFunctionName())
										 .append("\" class=\"btn\" title='")
										 .append(processNodeExtendButton.getButtonExegesis())
										 .append("'>")
										 .append(processNodeExtendButton.getButtonName())
										 .append("</button>&nbsp;");
						} else {
							continue;
						}
					} else {
						extendButtons.append("<button type=\"button\"  onClick=\"")
									 .append(processNodeExtendButton.getFunctionName())
									 .append("\" class=\"btn\" title='")
									 .append(processNodeExtendButton.getButtonExegesis())
									 .append("'>")
									 .append(processNodeExtendButton.getButtonName())
									 .append("</button>&nbsp;");
					}
				}
			}
		}
		return extendButtons.toString();
	}
	/**
	 * 子表 定义
	 * @param formCode
	 * @param formId
	 * @param formName
	 * @param items
	 * @return
	 */
	private String getSubSheetBody(String formCode,String formId,String formName,List<FormItem> items,boolean isEdit){
		StringBuffer sheetBody = new StringBuffer();
		sheetBody.append("<!--").append(formName).append(" 子表定义-->\n");
		sheetBody.append("<table style=\"height:auto;overflow:scroll;\" singleSelect=\"true\" idField=\"a1\" id=\"").append(formCode).append("\"></table>\n");
		sheetBody.append("<!-- end ").append(formName).append(" 子表定义-->\n");
		sheetBody.append("<!-- ").append(formName).append(" 子表script定义-->\n");
		sheetBody.append("<script type='text/javascript'>\n");
		sheetBody.append("var lastIndex;\n");
		sheetBody.append("$(document).ready(function () {\n");
		sheetBody.append(" try{\n");
		sheetBody.append("$('#").append(formCode).append("').datagrid({ \n");
		sheetBody.append("singleSelect:true,\n");
		sheetBody.append("nowrap:false,\n");
		sheetBody.append("loadMsg:'',\n");
		sheetBody.append("url:springUrl + '/app/processApp_get_bindReportDataJson?boTableName=").append(formCode).append("&businessObjectId=").append(this._businessObjectId).append("',\n");
		sheetBody.append("frozenColumns:[[\n");
		sheetBody.append("                {field:'ck',checkbox:true}\n");
		sheetBody.append("			]],\n");
		sheetBody.append("			fitColumns: false,\n");
		sheetBody.append("    columns: [[");
		sheetBody.append("\n {field: \"id\", title: \"id\",width:120,hidden:true},");
		
		RuntimeSubSheetFormUIComponent subSheet = new RuntimeSubSheetFormUIComponent(this);
		sheetBody.append(subSheet.buildSubSheetOfVerify(items,formCode));
		
		if(isEdit){
			sheetBody.append(",onClickRow:function(rowIndex){\n");
			sheetBody.append("if (lastIndex != rowIndex){\n");
			sheetBody.append("		$('#").append(formCode).append("').datagrid('endEdit', lastIndex);\n");
			sheetBody.append("	}\n");
			sheetBody.append("	$('#").append(formCode).append("').datagrid('beginEdit', rowIndex);\n");
			sheetBody.append("	lastIndex = rowIndex;\n");
			sheetBody.append("}\n");
		}
		sheetBody.append(",onLoadSuccess:function(data){\n");
		sheetBody.append("try{parent.iFrameHeight();}catch(e){}\n");
		sheetBody.append("try{onLoadSuccessCallback();}catch(e){}\n");	
//		sheetBody.append("	if(data=null || data.total==0){\n");
//		sheetBody.append("	subSheetNew_"+formCode+"('"+formCode+"');\n");
//		sheetBody.append("	}\n");
//		sheetBody.append("	if(data.total==0){\n");
//		sheetBody.append("		$('#").append(formCode).append("').datagrid('insertRow',{\n");
//		sheetBody.append("	     row: {\n");
//		sheetBody.append("	     }\n");
//		sheetBody.append("	   });\n");
//		sheetBody.append("	  $(\"tr[datagrid-row-index='0']\").css({\"visibility\":\"hidden\"});\n");
//		sheetBody.append("	}\n");
		sheetBody.append("}\n");
		//行列规则解析
		RuntimeComputeExpressManager cptExp = new RuntimeComputeExpressManager(this);
		sheetBody.append(cptExp.getRowOfRulesJavascript(items, formCode));
		
		sheetBody.append("});\n");
		sheetBody.append("//easyui表格滚动\n");
		sheetBody.append("var dv2 = $(\".datagrid-view2\");\n");
		sheetBody.append("dv2.children(\".datagrid-body\").html(\"<div style='width:1500px;border:solid 0px;height:1px;'></div>\");\n");
		sheetBody.append("}catch(e){}\n");
		sheetBody.append("});\n");
		sheetBody.append(getAppendNewDataFunction(formCode,items));
		sheetBody.append("</script>\n");
		sheetBody.append("<!-- end").append(formName).append("script定义-->\n");
		return sheetBody.toString();
	}
	/*
	 * 新增数据
	 */
	private String getAppendNewDataFunction(String formCode,List<FormItem> items){
		StringBuffer js = new StringBuffer();
		js.append("function subSheetNew_").append(formCode).append("(tableId){\n");
		js.append("$('#' + tableId).datagrid('endEdit', lastIndex);\n");
		js.append("$('#' + tableId).datagrid('appendRow',{\n");
		for(int i = 0 ; i < items.size() ; i++){
			FormItem item = items.get(i);
			if(item != null){
				String defaultValue = item.getDefaultValue();
				if(defaultValue != null && defaultValue.trim().length() > 0){
					defaultValue = convertMacrosValue(defaultValue);
				}
				js.append("\n ").append(item.getFieldName()).append(": '").append(defaultValue).append("',");
			}
		}
		js.setLength(js.length() - 1);
		js.append("});\n");
		js.append("lastIndex = $('#' + tableId).datagrid('getRows').length-1;\n");
		js.append("$('#' + tableId).datagrid('selectRow', lastIndex);\n");
		js.append("$('#' + tableId).datagrid('beginEdit', lastIndex);\n");
		js.append("try{parent.iFrameHeight();}catch(e){}");
		js.append("}\n");
		return js.toString();
	}
	/**
	 * ProcessNodeFormPrivilege list 转换成  Map<itemId,ProcessNodeFormPrivilege>
	 * @param processNodeFormPrivileges
	 */
	private void transformListToMapForProcessNodePrivilege(List<ProcessNodeFormPrivilege> processNodeFormPrivileges){
		if(processNodeFormPrivileges != null){
			for(int i = 0 ; i < processNodeFormPrivileges.size() ; i++){
				ProcessNodeFormPrivilege processNodeFormPrivilege = processNodeFormPrivileges.get(i);
				if(processNodeFormPrivilege != null){
					this._processNodeFormPrivilege.put(processNodeFormPrivilege.getItemId(), processNodeFormPrivilege);
				}
			}
		}
	}
	/**
	 * 将宏变量转换成默认的值
	 * 
	 * @param defaultValue
	 */
	public String convertMacrosValue(String defaultValue) {
		// 排除\+符号，+号为@命令的连接符，如果
		// 在@命令中要包含+号，可使用\+来实现
		String sign = "_MACROS%C1%F5%BD%F0%D6%F9MACROS_";
		String formula = new StringUtils(defaultValue).replace("\\+", sign);
		formula = new StringUtils(formula).replace("+", "");
		formula = new StringUtils(formula).replace(sign, "+");
		String result = EXPParser.getInstance(this._contextPath).executeRule(formula, this);
		return new StringUtils(result).replace(sign, "+");
	}

	public String getExtendButtonJavascriptContext() {
		return extendButtonJavascriptContext;
	}

	public void setExtendButtonJavascriptContext(
			String extendButtonJavascriptContext) {
		this.extendButtonJavascriptContext = extendButtonJavascriptContext;
	}
	
}
