package com.jd.oa.form.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.form.dao.FormTemplateDao;
import com.jd.oa.form.model.FormTemplate;

@Component
public class FormTemplateDaoImpl extends MyBatisDaoImpl<FormTemplate,String> implements FormTemplateDao{


	@Override
	public FormTemplate getByFormId(String formId) {
		// TODO Auto-generated method stub
		return getSqlSession().selectOne(sqlMapNamespace + ".getByFormId",formId);
	}
	
}
