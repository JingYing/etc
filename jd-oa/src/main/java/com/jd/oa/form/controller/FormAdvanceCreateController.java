package com.jd.oa.form.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.reflect.TypeToken;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormItemBean;
import com.jd.oa.form.service.FormBusinessFieldService;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.system.service.SysUserService;

/**
 * @Description: 自定义表字段controller
 * @author 许林
 * @date 2013-9-22 下午10:46:38
 * @version V1.0
 */
@Controller
@RequestMapping(value = "/form")
public class FormAdvanceCreateController {

	private static final Logger logger = Logger
			.getLogger(FormAdvanceCreateController.class);

	@Autowired
	private FormBusinessFieldService formBusinessFieldService;

	@Autowired
	private FormBusinessTableService formBusinessTableService;
	
	@Autowired
	private FormService formService;
	
	@Autowired
	private FormItemService formItemService;
	
	@Autowired
	private SysUserService sysUserService;
	
	 /**
     * 高级模式表单项目配置
     * xulin
     */
    @RequestMapping(value = "/formItem_advanced_set", method=RequestMethod.GET,produces = "application/json")
    public String formAdvancedSet(String formId,String tableId,Model model) {
        model.addAttribute("formId",formId);
        model.addAttribute("tableId",tableId);
        return "form/form_advanced_add";
    }
    
	/**
	  * @Description: 获取所有主业务表信息 只显示手动创建的
	  * @param model
	  * @return String
	  * @author xulin
	  * @date 2013-9-17下午02:28:14 
	  * @version V1.0
	 */
	 @RequestMapping(value = "/formBusinessTable_advanced",method = RequestMethod.GET)
	 @ResponseBody
	public void getMainTableInfos(HttpServletResponse response) {
		 
		 try {
			 //用于新增页面显示可以关联的主表
			 List<FormBusinessTable> parentTableList = formBusinessTableService.findParentTableList();
			 List<FormBusinessTable> result = new ArrayList<FormBusinessTable>();
			 
			 List<String> paramList = null;
			 Map<String,FormBusinessTable> tempMap = null;
			 if(parentTableList!=null&&parentTableList.size()>0){
				 paramList = new ArrayList<String>(parentTableList.size());
				 tempMap = new HashMap<String, FormBusinessTable>(parentTableList.size());
				 for(FormBusinessTable table:parentTableList){
					 paramList.add(table.getTableName());
					 tempMap.put(table.getTableName(), table);
				 }
				 
				 //过滤还未生成物理表的数据
				 List<String> alreadyCreateTable = this.formBusinessFieldService.findCreatedTable(paramList);
				 if(alreadyCreateTable!=null&&alreadyCreateTable.size()>0){
					 for(String tableName:alreadyCreateTable){
						 result.add(tempMap.get(tableName));
					 }
				 }
			 }
			 
			 response.getWriter().write(
						JsonUtils.toJsonByGoogle(result));
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
   }
	 
	/**
	 * @Description: 根据tableID获取其主表和下属子表的所有未被选择的字段信息
	 * @param model
	 * @return String
	 * @author xulin
	 * @date 2013-9-22上午10:13:14
	 * @version V1.0
	 */
	@RequestMapping(value = "/selectAllFieldsByTableId", method = RequestMethod.GET)
	@ResponseBody
	public void selectAllFieldsByTableId(HttpServletRequest request,
			@RequestParam(value = "tableId", required = true) String tableId,
			HttpServletResponse response) {

		try {
			List<FormItemBean> result = new ArrayList<FormItemBean>();
			FormItemBean itemBean = null;
			//根据TableId获取其主表和下属子表
			FormBusinessTable formBusinessTable = this.formBusinessTableService.get(tableId);
			//主表下字段详细信息
			if(formBusinessTable!=null){
				itemBean = new FormItemBean();
				itemBean.setFieldId(tableId);
				itemBean.setFieldName("主表".concat("-").concat(formBusinessTable.getTableChineseName()));
				result.add(itemBean);
				List<FormBusinessField> mainTableFields = this.formBusinessFieldService.findTableFieldListNotSelect(tableId);
				if(mainTableFields!=null&&mainTableFields.size()>0){
					for(FormBusinessField field:mainTableFields){
						itemBean = new FormItemBean();
						itemBean.setFieldId("M".concat("-").concat(tableId).concat("-").concat(field.getId()));
						itemBean.setFieldName(field.getFieldChineseName()==null?"":field.getFieldChineseName());
						result.add(itemBean);
					}
				}
				
				//子表下字段详细信息
				List<FormBusinessTable> subTables = formBusinessTable.getSubTables();
				List<FormBusinessField> subTableFields = null;
				if(subTables!=null&&subTables.size()>0){
					for(FormBusinessTable subTable:subTables){
						//子表下字段详细信息
						subTableFields = this.formBusinessFieldService.findTableFieldListNotSelect(subTable.getId());
						itemBean = new FormItemBean();
						itemBean.setFieldId(subTable.getId());
						itemBean.setFieldName("子表".concat("-").concat(subTable.getTableChineseName()));
						result.add(itemBean);
						for(FormBusinessField field:subTableFields){
							itemBean = new FormItemBean();
							itemBean.setFieldId("S".concat("-").concat(subTable.getId()).concat("-").concat(field.getId()));
							itemBean.setFieldName(field.getFieldChineseName()==null?"":field.getFieldChineseName());
							result.add(itemBean);
						}
					}
				}
			}
			
			response.getWriter().write(
					JsonUtils.toJsonByGoogle(result));
		} catch (Exception e1) {
			logger.error(e1.getMessage(), e1);
		}
	}
	
	/**
	 * @Description: 根据tableID获取已经选择的主表和子表下的字段信息
	 * @param model
	 * @return String
	 * @author xulin
	 * @date 2013-9-22上午10:13:14
	 * @version V1.0
	 */
	@RequestMapping(value = "/selectedFieldsByTableId", method = RequestMethod.GET)
	@ResponseBody
	public void selectedFieldsByTableId(HttpServletRequest request,
			@RequestParam(value = "tableId", required = true) String tableId,
			HttpServletResponse response) {
		try {
			List<FormItemBean> result = new ArrayList<FormItemBean>();
			FormItemBean itemBean = null;
			//根据TableId获取其主表和下属子表
			FormBusinessTable formBusinessTable = this.formBusinessTableService.get(tableId);
			if(formBusinessTable!=null){
				itemBean = new FormItemBean();
				itemBean.setFieldId(formBusinessTable.getId());
				itemBean.setFieldName("主表".concat("-").concat(formBusinessTable.getTableChineseName()));
				result.add(itemBean);
				List<FormItemBean> tempItem = this.formService.findFormItemsByTableId(tableId);
				String tempFieldId = null;
				if(tempItem!=null&&tempItem.size()>0){
					for(FormItemBean item:tempItem){
						tempFieldId = item.getFieldId();
						item.setFieldId("M".concat("-").concat(tableId).concat("-").concat(tempFieldId));
						result.add(item);
					}
				}
				//主表下的子表信息
				List<FormBusinessTable> subTables = formBusinessTable.getSubTables();
				if(subTables!=null&&subTables.size()>0){
					for(FormBusinessTable subTable:subTables){
						//子表下字段详细信息
						tempItem = this.formService.findFormItemsByTableId(subTable.getId());
						itemBean = new FormItemBean();
						itemBean.setFieldId(subTable.getId());
						itemBean.setFieldName("子表".concat("-").concat(subTable.getTableChineseName()));
						result.add(itemBean);
						if(tempItem!=null&&tempItem.size()>0){
							for(FormItemBean item:tempItem){
								tempFieldId = item.getFieldId();
								item.setFieldId("S".concat("-").concat(subTable.getId()).concat("-").concat(tempFieldId));
								result.add(item);
							}
						}
					}
				}
			}
			
			response.getWriter().write(
					JsonUtils.toJsonByGoogle(result));
		} catch (Exception e1) {
			logger.error(e1.getMessage(), e1);
		}
	}

    /**
     * 高级模式下 选择数据库表下字段 保存触发
     * @param data  //字段数据  如： {"tableType":"M","tableId":"9434e6ab1f0e44fd997bef9b8cfceb29","fieldId":"a68b6251c6674a799a3d8517b0f32392"}
     * @param formId//form表ID
     * @return
     */
	@RequestMapping(value = "/formAdvanced_addSave",method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String formAdvanced_addSave(String data,@RequestParam(value = "formId", required = true) String formId) {
		List<FormItemBean> formItemBeanList = 
			JsonUtils.fromJsonByGoogle(data, new TypeToken<List<FormItemBean>>() {});
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			if(formItemBeanList!=null&&formItemBeanList.size()>0){
				List<Form> subFormModelList = new ArrayList<Form>();
				Form mainFormModel = null;
				List<FormItem> formItemList = new ArrayList<FormItem>();
				Map<String,Form> mapTables = new HashMap<String, Form>();
				String tableId,filedId;
				FormBusinessTable table = null;
				Form form = null;
				FormBusinessField tableField = null;
				FormItem formField = null;
				
				//首先整理表单主表信息
				for(FormItemBean itemBean:formItemBeanList){
					tableId = itemBean.getTableId();
					filedId = itemBean.getFieldId();
					if(!mapTables.containsKey(tableId)){
						table = this.formBusinessTableService.get(tableId);
						form = this.getFormModel(table, formId);
						mapTables.put(tableId, form);
						if(SystemConstant.FORM_TYPE_M.equals(table.getTableType().toUpperCase())){
							mainFormModel = form;
						} else {
							subFormModelList.add(form);
						}
					}
				}
				//整理表单明细项信息
				for(FormItemBean itemBean:formItemBeanList){
					filedId = itemBean.getFieldId();
					tableField = this.formBusinessFieldService.getFormBusinessFieldInfo(filedId);
					formField = this.getFormItemModel(tableField, mapTables);
					formItemList.add(formField);
				}
				
				//更新数据库
				this.formService.operateFormAndFormItem(mainFormModel, subFormModelList, formItemList);
			} else {
				//删除该表单下的子表单和明细项
				this.formService.operateFormAndFormItem(formId);
			}
			map.put("check", true);
		} catch (Exception e1) {
			logger.error(e1.getMessage(), e1);
			map.put("check", false);
		}
		
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 表单主表model配置
	 * @param table
	 * @param formId
	 * @return Form
	 */
	private Form getFormModel(FormBusinessTable table,String formId){
		Form form = null;
		//手动控制主键的生成，因为在后面生成子表单和表单明细项时需要关联
		if(SystemConstant.FORM_TYPE_M.equals(table.getTableType().toUpperCase())){
			form = this.formService.get(formId);
		} else {
          Form formForQuery = new Form();
            formForQuery.setParentId(formId);
            formForQuery.setTableId(table.getId());
            if(this.formService.find(formForQuery).size()==1){
                form =  this.formService.find(formForQuery).get(0);
            }else{
                form = new Form();
                form.setId(IdUtils.uuid2());
                //表单名称
                form.setFormName(table.getTableChineseName());
                //父表单
                form.setParentId(formId);
                //表单所有者
                form.setOwner(this.sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin(), SystemConstant.ORGNAIZATION_TYPE_HR));
            }


		}
		//FormCode
		form.setFormCode(table.getTableName());
		
		//创建类型
		form.setCreateType(SystemConstant.FORM_TYPE_AM);
		//表单类型 主表或子表
		form.setFormType(table.getTableType().toUpperCase());
		//对应的业务表ID
		form.setTableId(table.getId());
		//逻辑删除标志
		form.setYn(0);
		
		return form;
	}
	
	/**
	 * 表单详细项model配置
	 * @param tableField
	 * @param mapTables
	 * @return
	 */
	private FormItem getFormItemModel(FormBusinessField tableField,Map<String,Form> mapTables){
		FormItem formItem = new FormItem();
		Form form = mapTables.get(tableField.getTableId());
		//对应表单主表ID
		formItem.setFormId(form.getId());
		//表单定义数据项编码
		formItem.setFieldName(tableField.getFieldName());
		//表单定义数据项名称
		formItem.setFieldChineseName(tableField.getFieldChineseName());
		formItem.setTableType(form.getFormType());
		formItem.setFieldId(tableField.getId());
		formItem.setDataType(tableField.getDataType());
		formItem.setLength(tableField.getLength());
		formItem.setDefaultValue(tableField.getDefaultValue());
		formItem.setInputType(tableField.getInputType());
		formItem.setInputTypeConfig(tableField.getInputTypeConfig());
		formItem.setDictTypeCode(tableField.getDictTypeCode());
		formItem.setSortNo(tableField.getSortNo());
		formItem.setValidateRule("");//TODO
		formItem.setYn(0);
		
		return formItem;
	}
	
	 /**
     * 同步表单信息到业务表并基于业务表创建物理存储表
     * xulin
     */
    @RequestMapping(value = "/synchronization_formInfo_businessTable", method=RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public String createBusinessTableAndLogicTable(String formId,String tableId) {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	try{
    		List<FormBusinessTable> resultFormTable = new ArrayList<FormBusinessTable>();
    		List<FormBusinessField> resultTableFields = new ArrayList<FormBusinessField>();
    		List<Form> resultForm = new ArrayList<Form>();
    		
    		FormBusinessTable tempTableModel = null;
    		FormBusinessField tempTableFieldsModel = null;
    		
    		Form form = this.formService.get(formId);
    		String mainTableUUID = null;
    		if(tableId!=null&&!"".equals(tableId)&&!"null".equals(tableId)){
    			mainTableUUID = tableId;
    		} else {
    			mainTableUUID = IdUtils.uuid2();
    			tableId = mainTableUUID;
    		}
    		//根据form的信息生成一条businessTable
    		tempTableModel = this.createFormBusinessTable(form, mainTableUUID,null);
    		resultFormTable.add(tempTableModel);
    		
    		//更新form对应的tableId
    		form.setTableId(mainTableUUID);
    		resultForm.add(form);
    		
    		//主表单及其下属字段信息
    		List<FormItem> mainFormItems = this.formItemService.selectByFormId(formId);
    		if(mainFormItems != null && mainFormItems.size() > 0){
    			for(FormItem formItem:mainFormItems){
    				tempTableFieldsModel = this.createBusinessTableField(formItem, mainTableUUID);
    				resultTableFields.add(tempTableFieldsModel);
    			}
    		}
    		
    		//子表单及其下属字段信息
    		List<Form> subForms = form.getSubTables();
    		List<FormItem> subFormItems = null;
    		if(subForms != null && subForms.size() > 0){
    			String subTableUUID = null;
    			for(Form subForm:subForms){
    				subTableUUID = IdUtils.uuid2();
    				
    				//更新form对应的tableId
    				subForm.setTableId(subTableUUID);
    	    		resultForm.add(subForm);
    	    		
    				tempTableModel = this.createFormBusinessTable(subForm, mainTableUUID, subTableUUID);
    				resultFormTable.add(tempTableModel);
    				subFormItems = this.formItemService.selectByFormId(subForm.getId());
    				if(subFormItems != null && subFormItems.size() > 0){
    	    			for(FormItem formItem:subFormItems){
    	    				tempTableFieldsModel = this.createBusinessTableField(formItem, subTableUUID);
    	    				resultTableFields.add(tempTableFieldsModel);
    	    			}
    	    		}
    			}
    		}
    		
    		//判断能否传输数据到业务表
    		boolean check = true;
    		if(resultTableFields.size() == 0){//判断是否有字段信息
    			check = false;
    			map.put("operator", false);
				map.put("message", "表字段不能为空");
    		} else {
    			//判断是否创建过 且是否存在数据
    			boolean temp = true;
    			for(Form bean:resultForm){
    				temp = this.formBusinessFieldService.isExistBusinessTable(bean.getFormCode());
    				if(temp){
    					temp = this.formBusinessFieldService.isExistData(bean.getFormCode());
    					if(temp){
    						check = false;
//    						map.put("operator", false);
//    						map.put("message", "该表已经存在数据,不允许再次生成");
    						break;


    					}
    				}
    			}
    		}
    		
//    		if(check){
    			//根据TableId删除已创建过的业务表 &&&& 更新表单对应的tableId  &&&&生成新的业务表
        		map = this.formService.createBusinessTableAuto(tableId, resultFormTable, resultTableFields, resultForm,check);
//    		}
    		
    	} catch (Exception e1) {
			logger.error(e1.getMessage(), e1);
			map.put("operator", false);
		}
    	
    	return JSONObject.fromObject(map).toString();
    }
    
    /**
     * 创建业务表主表
     * @param form
     * @param mainTableUUID
     * @return
     */
    private FormBusinessTable createFormBusinessTable(Form form,String mainTableUUID,String subTableUUID){
    	FormBusinessTable model = new FormBusinessTable();
    	
    	if(SystemConstant.FORM_TYPE_M.equals(form.getFormType().toUpperCase())){
			//主表
    		model.setId(mainTableUUID);
		} else {
			//子表
			model.setId(subTableUUID);
			model.setParentId(mainTableUUID);
		}
    	//中文名称
    	model.setTableChineseName(form.getFormName());
    	//表名
    	model.setTableName(form.getFormCode());
    	//表说明
    	model.setTableDesc(form.getFormDesc());
    	//表类型
    	model.setTableType(form.getFormType().toLowerCase());
    	//创建类型
    	model.setCreateType(SystemConstant.TABLE_CREATE_TYPE_0);
    	//删除标志
    	model.setYn(0);
    	//所有者
    	model.setOwner(ComUtils.getLoginNamePin());
    	model.setModifier(ComUtils.getLoginName());
    	model.setModifyTime(new Date());
    	
    	return model;
    }
    
    /**
     * 创建业务表字段详情表
     * @param formItem
     * @param tableId
     * @return
     */
    private FormBusinessField createBusinessTableField(FormItem formItem,String tableId){
    	FormBusinessField model = new FormBusinessField();
    	
    	//TableId
    	model.setTableId(tableId);
    	//字段名
    	model.setFieldName(formItem.getFieldName());
    	//字段中文名
    	model.setFieldChineseName(formItem.getFieldChineseName());
    	//数据库类型
    	model.setDataType(formItem.getDataType());
    	//该字段是否为空
    	model.setIsNull(formItem.getIsNull());
    	//字段长度
    	model.setLength(formItem.getLength());
    	//默认值
    	model.setDefaultValue(formItem.getDefaultValue());
    	//UI展示输入类型
    	model.setInputType(formItem.getInputType());
    	//输入配置项
    	model.setInputTypeConfig(formItem.getInputTypeConfig());
    	//下来列表时 对应的数据项
    	model.setDictTypeCode(formItem.getDictTypeCode());
    	//排序
    	model.setSortNo(formItem.getSortNo()==null?0:formItem.getSortNo());
    	//是否可用标志
    	model.setYn(0);
    	
    	return model;
    }
}
