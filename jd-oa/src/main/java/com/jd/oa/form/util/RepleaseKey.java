package com.jd.oa.form.util;

import java.util.Iterator;
import java.util.Map;

public class RepleaseKey {

	public static String replace(String paramString, Map<String,String> paramMapTags) {
		return replace(paramString, paramMapTags, "${", "}");
	}

	public static String replace(String paramString1, Map<String,String> paramMapTags,String paramString2, String paramString3) {
		if(paramMapTags != null && paramMapTags.size() > 0){
			Iterator it = paramMapTags.keySet().iterator();
			while(it.hasNext()){
				String key = (String) it.next();
				String replaceKey = paramString2 + key + paramString3;
				if(paramString1.indexOf(replaceKey) != -1 && paramMapTags.get(key) != null){
					paramString1 = paramString1.replace(replaceKey, paramMapTags.get(key));
				}
			} 
		}
		return paramString1;
	}
}