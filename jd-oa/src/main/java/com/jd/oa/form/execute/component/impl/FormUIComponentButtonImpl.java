package com.jd.oa.form.execute.component.impl;

import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentButtonImpl extends FormUIComponentAbst {

	public FormUIComponentButtonImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}

	@Override
	public String getTrueValue() {
		return getValue();
	}
	
}