package com.jd.oa.form.execute;

import java.util.ArrayList;
import java.util.List;

import com.jd.common.util.StringUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormSequenceService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessInstance;
/**
 * 默认的表单UI
 * @author birkhoff
 * @date   2013年10月22日
 *
 */
public class RuntimeDefaultFormUIComponent {
	/**
	 * 数据库字段数据类型
	 * 
	 */
	public static final String DEFAULT_FIELDNAME_PROCESSINSTANCENAME = "processInstanceName";//主题
	
	public static final String DEFAULT_FIELDNAME_TASKPRIORITY = "taskPriority";

	public static final String DEFAULT_FIELDNAME_FOLLOWCODE = "followCode";
	
	public static List<FormItem> getFormDefaultField(ProcessDefinition processDefinition,ProcessInstance processInstance){
		List<FormItem> items = new ArrayList<FormItem>();
		FormItem item = new FormItem();
		String processInstanceNameValue = "";
		String processTaskPriortyValue = "0";
		String processFollowCodeValue=null;
		String isEdit = "1";
		if(processInstance != null){
			processInstanceNameValue = processInstance.getProcessInstanceName() != null ? processInstance.getProcessInstanceName() : "";
			processFollowCodeValue = processInstance.getFollowCode();
			processTaskPriortyValue = processInstance.getPriority() != null ? processInstance.getPriority() : "0";
			if(!processInstance.getStatus().equals("0")){
				isEdit = "0";
			}
		}

		//主题
		item.setFieldName("processInstanceName");
		item.setFieldChineseName("主题");
		item.setDefaultValue(processInstanceNameValue);
		item.setInputType("单行");
		item.setIsEdit(isEdit);
        item.setLength("50");
		item.setIsNull("0");
		item.setDataType("string");
		items.add(item);

		//任务优先级
		item = new FormItem();
		item.setFieldChineseName("任务优先级");
		item.setFieldName("taskPriority");
		item.setInputType("单选按纽组");
		item.setDataType("string");
//		item.setIsEdit(isEdit);
		item.setIsHidden("1");
		item.setDefaultValue(processTaskPriortyValue);
		item.setInputTypeConfig("2:特急|1:紧急|0:标准");
		items.add(item);

		//流程流水号
		item = new FormItem();
		item.setFieldChineseName("流水单号");
		item.setFieldName("followCode");
		item.setInputType("单行");
		item.setDataType("string");
		if(processDefinition != null){
			if(processInstance == null){
				FormSequenceService formSequenceService = SpringContextUtils.getBean(FormSequenceService.class);
				item.setDefaultValue(formSequenceService.getProcessSequenceCode(processDefinition.getFollowCodePrefix()));
			}else{
				item.setDefaultValue(processFollowCodeValue);
			}
		}
		items.add(item);
		return items;
	}
}
