package com.jd.oa.di.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.common.utils.PageJDBC;
import com.jd.oa.di.model.DiDatasource;

import java.util.List;
import java.util.Map;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
public interface DiDatasourceService extends BaseService<DiDatasource,String> {

	/**
	 * 删除数据源
	 * @param ids 数据源ID数组
	 */
	public void deleteDatasource(String[] ids);

	/**
	 * 验证数据库类型数据源是否可用
	 * @param dataAdapterId  数据源适配的连接ID
	 * @param sql  需要验证的sql
	 */
	public List<String> verifyDBDatasource(String dataAdapterId,String sql);

	/**
	 * 验证接口类型数据源是否可用
	 * @param dataAdapterId  数据源适配的连接ID
	 */
	public List<String>  verifyInterfaceDatasource(String dataAdapterId);

	/**
	 * 接口类型数据源取回数据
	 * @param dataAdapter   数据源适配的连接ID
	 * @param pageNum  页码
	 * @param pageSize  页面大小
	 * @param pageSize  isService 1:服务接口;0:查询数据接口
	 */
	public List<Map<String,Object>>  getDatasourceResultList(String dataAdapter,int pageNum,int pageSize,Map<String,Object> params,String isService);

	/**
	 * 数据库类型数据源取回数据
	 * @param dataAdapter  数据源适配的连接ID
	 * @param pageNum  页码
	 * @param pageSize  页面大小
	 * @param sql 需要执行的sql语句且用于区分数据源是否是数据库连接类型
	 */
	public PageJDBC getDatasourceResultListForPage(String dataAdapter,int pageNum,int pageSize,String sql);
		
	/**
	 * 获取数据库类型数据源返回的数据或执行数据库脚本
	 * @param dataAdapter  数据源适配的连接ID
	 * @param sql 需要执行的sql语句且用于区分数据源是否是数据库连接类型
	 * @param isService 是否服务
	 */
	public List<Map<String,Object>> getDatasourceResultList(String dataAdapter,String sql,String isService);
	
	
	
}
