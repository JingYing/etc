package com.jd.oa.di.dao.impl;

import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.di.dao.DiDatasourceFieldMapDao;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.model.DiDatasourceFieldMap;

@Repository
public class DiDatasourceFieldMapDaoImpl extends MyBatisDaoImpl<DiDatasourceFieldMap,String>  implements DiDatasourceFieldMapDao{

}
