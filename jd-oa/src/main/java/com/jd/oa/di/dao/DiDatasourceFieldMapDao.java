package com.jd.oa.di.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.di.model.DiDatasourceCondition;
import com.jd.oa.di.model.DiDatasourceFieldMap;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
public interface DiDatasourceFieldMapDao extends BaseDao<DiDatasourceFieldMap,String>{

}
