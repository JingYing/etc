package com.jd.oa.di.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.di.model.DiCcDb;

public interface DiCcDbService extends BaseService<DiCcDb	, String> {
	/**
	 * 
	 * @desc 根据唯一索引获得对象
	 * @author WXJ
	 * @date 2014-2-21 上午11:30:40
	 *
	 * @param dbName
	 * @return
	 */
	public DiCcDb getByUnique(String dbName);
	
}
