package com.jd.oa.di.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.di.dao.DiDatasourceDao;
import com.jd.oa.di.model.DiDatasource;
import org.springframework.stereotype.Repository;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Repository("diDatasourceDao")
public class DiDatasourceDaoImpl extends MyBatisDaoImpl<DiDatasource,String> implements DiDatasourceDao  {
	
}
