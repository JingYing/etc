package com.jd.oa.di.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.di.dao.DiDatasourceConditionDao;
import com.jd.oa.di.model.DiDatasourceCondition;
import org.springframework.stereotype.Repository;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Repository("diDatasourceConditionDao")
public class DiDatasourceConditionDaoImpl extends MyBatisDaoImpl<DiDatasourceCondition,String> implements DiDatasourceConditionDao {
}
