/**
 * 
 */
package com.jd.oa.di.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.model.DiCcInterface;

/**
 * @author yujiahe
 * @date 2014-2-20
 *
 */
public interface DiCcInterfaceService extends BaseService<DiCcInterface,String> {
	/**
	 * 
	 * @desc 根据唯一索引获得对象
	 * @author WXJ
	 * @date 2014-2-21 上午11:30:40
	 *
	 * @param interfaceName
	 * @return
	 */
	public DiCcInterface getByUnique(String interfaceName);
	/**删除
	 * @param ids
	 */
	void mutidelete(String ids);

}
