package com.jd.oa.di.service;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.saf.SafClient;
import com.jd.oa.common.utils.PageJDBC;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.webservice.*;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.model.DiCcInterface;
import org.apache.commons.collections.map.LinkedMap;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DatasourceUtils {

	private static final Logger logger = Logger.getLogger(DatasourceUtils.class);
	private static ConnectionPool connectionPool;
	
	/**
	 * 
	 * @desc 取得Connection
	 * @author WXJ
	 * @date 2014-2-21 下午01:58:32
	 *
	 * @param dbId
	 * @return
	 */
	public static Connection getConnection(String dbId) {
		if (connectionPool!=null){
			try {
				return connectionPool.getConnection();
			} catch (SQLException e) {
				throw new BusinessException("接口服务异常-DB-getConnection",e);
			}
		} else{
			DiCcDbService diCcDbService = SpringContextUtils.getBean("diCcDbService");
			DiCcDb diCcDb = diCcDbService.get(dbId);
			if (diCcDb!=null){
				ConnectionParam param = new ConnectionParam();
				param.setDriver(diCcDb.getDriver());
				param.setUrl(diCcDb.getUrl());
				param.setUser(diCcDb.getUsername());
				param.setPassword(diCcDb.getPassword());
				param.setMaxConnection(5);
				param.setMinConnection(2);
				param.setIncrementalConnections(1);
				
				connectionPool = new ConnectionPool(param);
				try {
					connectionPool.createPool();
					return connectionPool.getConnection();
				} catch (Exception e) {
					connectionPool = null;
					throw new BusinessException("接口服务异常-DB-getConnection",e);
				}
			}
		}
		
		return null;
	}	
	
	/**
	 * 
	 * @desc 取得DataBase Result
	 * @author WXJ
	 * @date 2014-2-21 下午01:58:47
	 *
	 * @param dbId
	 * @param sql
	 * @return
	 */
	public static List<Map<String,Object>> getDbResult(String dbId, String sql) {
		List<Map<String,Object>> dbResultList = null;

		JdbcTemplate jdbcTemplate = SpringContextUtils.getBean("jdbcTemplate");
//		Connection conn = getConnection(dbId);
		Connection conn = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
		} catch (SQLException e) {
			throw new BusinessException("接口服务异常-DB",e);
		}
		if (conn!=null){
			Statement stmt = null;
	        ResultSet rs = null;
			try {
				stmt = conn.createStatement();								
				
				rs = stmt.executeQuery(sql);
				
				ResultSetMetaData md = rs.getMetaData();
				int num = md.getColumnCount();
								
				dbResultList = new ArrayList<Map<String,Object>>();
				while (rs.next()) {
					Map<String,Object> mapOfColValues = new LinkedMap(num);
					for (int i = 1; i <= num; i++) {
						mapOfColValues.put(md.getColumnLabel(i), rs.getObject(i));
					}
					dbResultList.add(mapOfColValues);
				}
			} catch (Exception e) {
				throw new BusinessException("接口服务异常-DB",e);
			} finally {
				 try {
//					  connectionPool.returnConnection(conn);
					  if(rs != null){
						  rs.close();
				          if(stmt!=null){
				        	  stmt.close();
							  conn.close();
				          }			         
					  }
				  }catch (SQLException e) {
					  throw new BusinessException("接口服务异常-DB",e);
				  }		
			}
		}
		
		return dbResultList;	
	}
	/**
	 * 
	 * @desc 取得DataBase Result
	 * @author WXJ
	 * @date 2014-2-21 下午01:58:47
	 *
	 * @param dbId
	 * @param sql
	 * @return
	 */
	public static PageJDBC getDbResultForPage(String dbId, int pageNo, int pageSize, String sql) {
		PageJDBC page = new PageJDBC();
		List<Map<String,Object>> dbResultList = null;
		JdbcTemplate jdbcTemplate = SpringContextUtils.getBean("jdbcTemplate");
//		Connection conn = getConnection(dbId);
		Connection conn = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (conn!=null){
			Statement stmt = null;
	        ResultSet rs = null;
			try {
				stmt = conn.createStatement();
				ResultSet rsTotal = stmt.executeQuery(sql);
				if(rsTotal != null){
					page.setTotal(rsTotal.getFetchSize());
				}
				page.setPageNo(pageNo + 1);
				page.setPageSize(pageSize);
				
				//设置Total
				String sqlTotal = " SELECT count(1) total FROM ( " + sql + ") tempDb";
				rs = stmt.executeQuery(sqlTotal);
				int total = 0;
				while (rs.next()) {
					total = rs.getInt("total");
				}
				page.setTotal(total);
				
				DiCcDbService diCcDbService = SpringContextUtils.getBean("diCcDbService");
				DiCcDb diCcDb = diCcDbService.get(dbId);
				
				//Mysql
				int offset = (pageNo - 1)   * pageSize ;
				if("1".equals(diCcDb.getDbType())){
					sql +=" limit " + String.valueOf(offset) +","+pageSize;
				}
				//Oracle
				else if("2".equals(diCcDb.getDbType())){
					sql = "SELECT *    FROM ( 	SELECT tempDb.*, ROWNUM rn  FROM ( "+sql + ") tempDb  WHERE ROWNUM <= "+String.valueOf(offset+pageSize)+") WHERE rn >= "+(offset);
				}					
				
				rs = stmt.executeQuery(sql);
				
				ResultSetMetaData md = rs.getMetaData();
				int num = md.getColumnCount();
								
				dbResultList = new ArrayList<Map<String,Object>>();
				while (rs.next()) {
					Map<String,Object> mapOfColValues = new HashMap<String,Object>(num);
					for (int i = 1; i <= num; i++) {
						mapOfColValues.put(md.getColumnLabel(i), rs.getObject(i));
					}
					dbResultList.add(mapOfColValues);
				}
			} catch (Exception e) {
				throw new BusinessException("接口服务异常-DB",e);
			} finally {
				 try {
//					  connectionPool.returnConnection(conn);
					  if(rs != null){
						  rs.close();
				          if(stmt!=null){
				        	  stmt.close();
//							  if(conn!=null){
								  conn.close();
//							  }
				          }			         
					  }
				  }catch (SQLException e) {
					  throw new BusinessException("接口服务异常-DB",e);
				  }		
			}
		}
		page.setRows(dbResultList);
		return page;	
	}

	/**
	 * 
	 * @desc 取得DataBase Columns
	 * @author WH
	 * @date 2014-2-24 下午08:37:49
	 *
	 * @param dbId
	 * @param sql
	 * @return
	 */
	public static List<String> getDbMap(String dbId,String sql){

		List<String> columnList=new ArrayList<String>();
		JdbcTemplate jdbcTemplate = SpringContextUtils.getBean("jdbcTemplate");
//		Connection conn = getConnection(dbId);
		Connection conn = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
		} catch (SQLException e) {
			throw new BusinessException("接口服务异常-DB",e);
		}
		if (conn!=null){
			Statement stmt = null;
			ResultSet rs = null;
			try {
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				ResultSetMetaData md = rs.getMetaData();
				int num = md.getColumnCount();
				for (int i = 1; i <= num; i++) {
					columnList.add(md.getColumnLabel(i));
				}
			} catch (Exception e) {
				throw new BusinessException("接口服务异常-DB",e);
			} finally {
				try {
//					connectionPool.returnConnection(conn);
					if(rs != null){
						rs.close();
						if(stmt!=null){
							stmt.close();
//							if(conn!=null){
								conn.close();
//							}
						}						
					}
				}catch (SQLException e) {
					throw new BusinessException("接口服务异常-DB",e);
				}
			}
		}
		return  columnList;
	}

	/**
	 * soap webservice 请求
	 * @param diCcInterface 接口定义
	 * @param soapHeaderTag 定义头信息的tag
	 * @param soapHeader  头信息
	 * @param inParamter 请求参数
	 * @return
	 */
	public static  Object requestSoapWebserviceResult(DiCcInterface diCcInterface,String soapHeaderTag, Map<String,Object> soapHeader,Map<String,Object> inParamter){
		Object result = null;
		CXFDynamicClient wsclient=new CXFDynamicClient(diCcInterface.getUrl());
		if(soapHeaderTag!=null && !soapHeaderTag.equals("")){
			if(soapHeader == null) soapHeader=new HashMap<String, Object>();
			wsclient.addSoapHeader(soapHeaderTag,soapHeader);
		}
		List<Class<?>> paramClassList=wsclient.getParamterClass(diCcInterface.getMethod());
		try {
			Object[] obj =null;
			if (inParamter!=null && inParamter.size() >0){
				if(inParamter.values().toArray()[0] instanceof Map){
					obj = new Object[inParamter.size()];
					int i=0;
					for (Object v : inParamter.values()) {
						obj[i] = wsclient.newInstance(paramClassList.get(i),(Map<String,Object>) v);
						i++;
					}
				}else{
					obj = new Object[inParamter.size()];
					int i=0;
					for (Object v : inParamter.values()) {
						obj[i] = v;
						i++;
					}
				}
			}
			Object[] res = wsclient.invoke(RESULT_DATA_TYPE.MAP,diCcInterface.getMethod(),obj);
			if(res !=  null && res.length>0){
				result = res[0];
			}
		} catch (Exception e) {
			throw new BusinessException("接口服务异常-远程接口-Webservice请求",e);
		}finally {
			if(wsclient != null){
				wsclient.destroy();
				wsclient = null;
			}
		}
		return  result;
	}

	/**
	 * rest webservice 请求
	 * @param diCcInterface 接口定义
	 * @param inParamter 参数
	 * @return
	 */
	public static Object requestRestfulWebserviceResult(DiCcInterface diCcInterface,Map<String,Object> requestTypeMap,Map<String,Object> inParamter){
		Object result = null;
		try {
			CXFRestfulClient client = new CXFRestfulClient(diCcInterface.getUrl());
			String type = (String) requestTypeMap.get("RequestType");
			if(type.toLowerCase().equals("get")){
				result = client.Get(RESULT_DATA_TYPE.MAP,diCcInterface.getMethod(),inParamter);
			}else if(type.toLowerCase().equals("post")){
				result = client.Post(RESULT_DATA_TYPE.MAP,diCcInterface.getMethod(),inParamter);
			}else{
				throw new BusinessException("接口服务异常-远程接口-Rest请求","Restful Webservice请求只支持GET/POST方式");
			}
		}catch(Exception e){
			throw new BusinessException("接口服务异常-远程接口-Rest请求",e);
		}	
		return result;
	}

	/**
	 * saf 接口请求数据
	 * @param diCcInterface 接口定义
	 * @param inParamter 参数
	 * @return
	 */
	public  static Object requestSafResult(DiCcInterface diCcInterface,Map<String,Object> safConfMap,Map<String,Object> inParamter){
		Object result = null;
		try {
			Map<String,String> newSafConfMap = new HashMap<String, String>();
			for(Map.Entry<String,Object> entry:safConfMap.entrySet()){
				newSafConfMap.put(entry.getKey(),(String) entry.getValue());
			}
			SafClient safClient  = new SafClient(newSafConfMap);
			String[] paramTypes = null;
			Object[] params =  null;
			if (inParamter!=null && inParamter.size() >0){
				paramTypes = new String[inParamter.size()];
				params = new Object[inParamter.size()];
				int i = 0;
				for (Object v : inParamter.values()) {
					//Saf传递包装类型要转成基本类型
					if(v instanceof Long){
						paramTypes[i] = "long";
					}else if(v instanceof Integer){
						paramTypes[i] = "int";
					}else if(v instanceof Float){
						paramTypes[i] = "float";
					}else if(v instanceof Double){
						paramTypes[i] = "double";
					}else if(v instanceof Character){
						paramTypes[i] = "char";
					}else if(v instanceof Byte){
						paramTypes[i] = "byte";
					}else{
						paramTypes[i] = v.getClass().getName();
					}
					params[i] = v;
					i++;
				}
			}
			result = safClient.invoke(diCcInterface.getUrl(), diCcInterface.getMethod(), paramTypes, params);
			safClient.destory();
			safClient = null;
		}catch(Exception e){
			throw new BusinessException("接口服务异常-远程接口-Saf请求",e);
		}
		return result;
	}

	/**
	 * http 接口请求
	 * @param diCcInterface
	 * @param inParamter
	 * @return
	 */
	public  static Object requestHttpResult(DiCcInterface diCcInterface,Map<String,Object> requestTypeMap,Map<String,Object> inParamter){
		Object result = null;
		try {
			Map<String,String> newMap = new HashMap<String, String>();
			for(Map.Entry<String,Object> entry:inParamter.entrySet()){
				newMap.put(entry.getKey(),(String) entry.getValue());
			}
			HttpClientUtil httpClientUtil = HttpClientUtil.getInstance();
			String url = diCcInterface.getUrl();
			if(diCcInterface.getMethod() != null && diCcInterface.getMethod().trim().length()>0){
				url += "/"+diCcInterface.getMethod();
			}
			byte[] bytes;
			String type = (String) requestTypeMap.get("RequestType");
			if(type.toLowerCase().equals("get")){
				bytes=httpClientUtil.request(HttpClientUtil.REQUEST_TYPE.GET,url,newMap);
			}else if(type.toLowerCase().equals("post")){
				bytes=httpClientUtil.request(HttpClientUtil.REQUEST_TYPE.POST,url,newMap);
			}else{
				throw new BusinessException("接口服务异常-远程接口-Http请求","Http请求只支持GET/POST方式");
			}
			result = DataConvertion.convertData(RESULT_DATA_TYPE.MAP,new String(bytes));
		}catch(Exception e){
			throw new BusinessException("接口服务异常-远程接口-Http请求",e);
		}
		return result;
	}

	/**
	 * 内部类接口请求
	 * @param diCcInterface
	 * @param inParamter
	 * @return
	 */
	public  static Object requestInnerClassResult(DiCcInterface diCcInterface,Map<String,Object> inParamter){
		Object result = null;
		Class<?> c = null;
		try {
			c = Class.forName(diCcInterface.getUrl());
			Object[] params;
			if (inParamter != null) {
				params = new Object[inParamter.size()];
				int i = 0;
				for (Object v : inParamter.values()) {
					params[i] = v;
					i++;
				}

				Method[] methods = c.getMethods();
				for (Method method : methods) {
					if (method.getName().equals(diCcInterface.getMethod())) {
						result = method.invoke(c.newInstance(),params);
						break;
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException("接口服务异常-内部类",e);
		}
		return result;
	}

	/**
	 * 
	 * @desc 取得Interface Result
	 * @author WXJ
	 * @date 2014-2-21 下午02:40:14
	 *
	 * @param inParamter
	 * @return
	 */
	public static Object getInterfaceResult(String interfaceId,String soapHeaderTag, Map<String,Object> soapHeader,Map<String,Object> inParamter) {
		Object result= null;
		try {
			DiCcInterfaceService diCcInterfaceService = SpringContextUtils.getBean("diCcInterfaceService");
			DiCcInterface diCcInterface = diCcInterfaceService.get(interfaceId);
	
			if (diCcInterface!=null){			
					//输入日志，但不影响原来业务操作
					logger.info("调用接口服务:["+interfaceId+"#"+diCcInterface.getInterfaceName()+"]");
					logger.info("调用接口服务头信息:["+DataConvertion.object2Json(soapHeader)+"]");
					logger.info("调用接口服务入参:["+DataConvertion.object2Json(inParamter)+"]");
					
				String interfaceType = diCcInterface.getInterfaceType();				
				if (interfaceType.equals("1")){
					result = requestSoapWebserviceResult(diCcInterface, soapHeaderTag, soapHeader, inParamter);
				}else if(interfaceType.equals("2")){//restful webservice 调用
					result = requestRestfulWebserviceResult(diCcInterface,soapHeader,inParamter);
				}else if(interfaceType.equals("3")){ //saf类的调用
					result = requestSafResult(diCcInterface,soapHeader,inParamter);
				}else if(interfaceType.equals("4")){ //Http调用
	//				result = requestRestfulWebserviceResult(diCcInterface,inParamter);
					result = requestHttpResult(diCcInterface,soapHeader,inParamter);
				}else if (interfaceType.equals("5")){//内部类的调用
					result = requestInnerClassResult(diCcInterface,inParamter);
				}
			}
		
			logger.info("调用接口服务结果:["+DataConvertion.object2Json(result)+"]");
		} catch (Exception e) {
			throw new BusinessException("接口服务异常-远程接口",e);
		}
		return result;
	}


}

