package com.jd.oa.di.service.impl;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.di.dao.DiCcInterfaceDao;
import com.jd.oa.di.dao.DiTransforLogDao;
import com.jd.oa.di.model.DiTransforLog;
import com.jd.oa.di.service.DiTransforLogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Service("diTransforLogService")
public class DiTransforLogServiceImpl extends BaseServiceImpl<DiTransforLog,String> implements DiTransforLogService {
	@Autowired
	private DiTransforLogDao diTransforLogDao;
	
	@Override
	public BaseDao<DiTransforLog, String> getDao() {
		// TODO Auto-generated method stub
		return diTransforLogDao;
	}

	
	
}
