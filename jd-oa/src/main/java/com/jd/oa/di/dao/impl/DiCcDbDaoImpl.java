package com.jd.oa.di.dao.impl;

import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.di.dao.DiCcDbDao;
import com.jd.oa.di.model.DiCcDb;


@Repository("diCcDbDao")
public class DiCcDbDaoImpl extends MyBatisDaoImpl<DiCcDb, String> implements DiCcDbDao {

}
