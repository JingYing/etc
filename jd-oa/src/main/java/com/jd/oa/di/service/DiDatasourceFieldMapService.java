package com.jd.oa.di.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.common.utils.PageJDBC;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.di.model.DiDatasourceCondition;
import com.jd.oa.di.model.DiDatasourceFieldMap;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
public interface DiDatasourceFieldMapService extends BaseService<DiDatasourceFieldMap,String> {
	public abstract String save(String datas,String formId,String dataSourceId,String formItemId);
	
	/**
	 * 表格定义
	 * @param dataSourceId
	 * @param formId
	 * @return
	 */
	public abstract String getDataGridDefinition(String dataSourceId,String formId,String formItemId,boolean isSubSheet);
	/**
	 * 分页json数据
	 * @param pageWrapper
	 * @param dataSourceId
	 */
	public abstract PageJDBC getPageJsonData(String dataSourceId,String rows,String page,String condition);
	/**
	 * 查询条件区域
	 * @param dataSourceId
	 * @param formId
	 * @return
	 */
	public abstract String getQueryConditionDefinition(HttpServletRequest request,String dataSourceId,String formId,String formItemId,String targetName,String likeConditionValue);
	/**
	 * 快速检索
	 * @param dataSourceId
	 * @param rows
	 * @param page
	 * @param condition
	 * @return
	 */
	public abstract PageJDBC getQuickSearchJsonData(String dataSourceId,String formId,String formItemId,String fieldName,String condition);
}

