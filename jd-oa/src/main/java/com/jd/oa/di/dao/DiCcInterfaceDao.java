/**
 * 
 */
package com.jd.oa.di.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.di.model.DiCcInterface;

/**
 * @author yujiahe
 * @date 2014-2-20
 *
 */
public interface DiCcInterfaceDao extends BaseDao<DiCcInterface,String> {

}
