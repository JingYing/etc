package com.jd.oa.di.model;

import java.util.Date;

import com.jd.oa.common.model.IdModel;

public class DiDatasourceFieldMap extends IdModel{
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String formId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ITEM_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String formItemId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.NAME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.TARGET_NAME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String targetName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FIELD_TYPE
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String fieldType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.DISPALY
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String dispaly;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.WIDTH
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private Integer width;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FILTER
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private int filter;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.isHidden
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private int ishidden;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.DATASOURCE_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String datasourceId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.YN
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private Boolean yn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATOR
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String creator;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATE_TIME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFIER
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private String modifier;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFY_TIME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    private Date modifyTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.ID
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.ID
     *
     * @param id the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ID
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getFormId() {
        return formId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ID
     *
     * @param formId the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ITEM_ID
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ITEM_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getFormItemId() {
        return formItemId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ITEM_ID
     *
     * @param formItemId the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.FORM_ITEM_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setFormItemId(String formItemId) {
        this.formItemId = formItemId == null ? null : formItemId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.NAME
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.NAME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.NAME
     *
     * @param name the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.NAME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.TARGET_NAME
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.TARGET_NAME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getTargetName() {
        return targetName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.TARGET_NAME
     *
     * @param targetName the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.TARGET_NAME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setTargetName(String targetName) {
        this.targetName = targetName == null ? null : targetName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FIELD_TYPE
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.FIELD_TYPE
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getFieldType() {
        return fieldType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FIELD_TYPE
     *
     * @param fieldType the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.FIELD_TYPE
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setFieldType(String fieldType) {
        this.fieldType = fieldType == null ? null : fieldType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.DISPALY
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.DISPALY
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getDispaly() {
        return dispaly;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.DISPALY
     *
     * @param dispaly the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.DISPALY
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setDispaly(String dispaly) {
        this.dispaly = dispaly == null ? null : dispaly.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.WIDTH
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.WIDTH
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.WIDTH
     *
     * @param width the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.WIDTH
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FILTER
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.FILTER
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public int getFilter() {
        return filter;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.FILTER
     *
     * @param filter the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.FILTER
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setFilter(int filter) {
        this.filter = filter;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.isHidden
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.isHidden
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public int getIshidden() {
        return ishidden;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.isHidden
     *
     * @param ishidden the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.isHidden
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setIshidden(int ishidden) {
        this.ishidden = ishidden;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.DATASOURCE_ID
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.DATASOURCE_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getDatasourceId() {
        return datasourceId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.DATASOURCE_ID
     *
     * @param datasourceId the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.DATASOURCE_ID
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setDatasourceId(String datasourceId) {
        this.datasourceId = datasourceId == null ? null : datasourceId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.YN
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.YN
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public Boolean getYn() {
        return yn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.YN
     *
     * @param yn the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.YN
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setYn(Boolean yn) {
        this.yn = yn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATOR
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATOR
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATOR
     *
     * @param creator the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATOR
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATE_TIME
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATE_TIME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATE_TIME
     *
     * @param createTime the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.CREATE_TIME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFIER
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFIER
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFIER
     *
     * @param modifier the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFIER
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFY_TIME
     *
     * @return the value of T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFY_TIME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFY_TIME
     *
     * @param modifyTime the value for T_JDOA_DI_DATASOURCE_FIELD_MAP.MODIFY_TIME
     *
     * @mbggenerated Wed Feb 19 16:43:39 GMT+08:00 2014
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}