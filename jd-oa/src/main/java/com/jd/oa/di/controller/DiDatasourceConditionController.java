package com.jd.oa.di.controller;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.model.DiDatasourceCondition;
import com.jd.oa.di.service.DatasourceUtils;
import com.jd.oa.di.service.DiCcDbService;
import com.jd.oa.di.service.DiDatasourceConditionService;
import com.jd.oa.di.service.DiDatasourceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangdongxing on 14-2-20.
 * Email : wangdongxing@jd.com
 */
@Controller
@RequestMapping("/di/datasource")
public class DiDatasourceConditionController {

	@Autowired
	private DiDatasourceConditionService diDatasourceConditionService;

	@Autowired
	private DiDatasourceService diDatasourceService;

	@Autowired
	private DiCcDbService diCcDbService;

	@RequestMapping(value = "/condition")
	public ModelAndView conditions(String id){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("di/datasource/ds_condition");
		mav.addObject("datasourceId",id);
		return mav;
	}


	/**
	 * author wangdongxing
	 * description 数据源查询条件的分页展示
	 * param request
	 * @return json数据
	 */
	@RequestMapping(value="/condition_page",method= RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object conditionPage(HttpServletRequest request){
		PageWrapper<DiDatasourceCondition> pageWrapper=new PageWrapper<DiDatasourceCondition>(request);
		String datasourceId=request.getParameter("datasourceId");
		pageWrapper.addSearch("datasourceId",datasourceId);
		diDatasourceConditionService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
		return pageWrapper.getResult();
	}

	/**
	 * author wangdongxing
	 * description 添加数据查询条件
	 * param request
	 * @return json数据
	 */
	@RequestMapping(value="/condition_add",method= RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object conditionAdd(String id,String fieldName,String fieldTitle,String fieldType,
							   String compareType,String defaultValue,String uiType,String isHidden,String datasourceId){
		DiDatasourceCondition datasourceCondition = null;
		if(id != null && !id.equals("")){
			datasourceCondition = diDatasourceConditionService.get(id);
		}
		if(datasourceCondition == null){
			datasourceCondition = new DiDatasourceCondition();
		}
		datasourceCondition.setFieldName(fieldName);
		datasourceCondition.setFieldTitle(fieldTitle);
		datasourceCondition.setFieldType(fieldType);
		datasourceCondition.setCompareType(compareType);
		datasourceCondition.setDefaultValue(defaultValue);
		datasourceCondition.setUiType(uiType);
		datasourceCondition.setIsHidden(Integer.parseInt(isHidden));
		datasourceCondition.setDatasourceId(datasourceId);

		try {
			if(datasourceCondition.getId() != null && !datasourceCondition.getId().equals("")){
				diDatasourceConditionService.update(datasourceCondition);
			}else{
				diDatasourceConditionService.insert(datasourceCondition);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
		Map<String,Object> resultMap=new HashMap<String, Object>();
		resultMap.put("operator","success");
		return resultMap;
	}

	/**
	 * author wangdongxing
	 * description 删除数据源查询条件
	 * @return json数据
	 */
	@RequestMapping(value="/condition_delete",method = RequestMethod.POST)
	@ResponseBody
	public Object datasourceDelete(String idstr){
		String[] ids= JsonUtils.fromJsonByGoogle(idstr, new TypeToken<String[]>() {
		});
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			for(String id:ids){
				diDatasourceConditionService.delete(id,true);
			}
			resultMap.put("operator", true);
			resultMap.put("message","删除成功");
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message",e.getMessage());
		}
		return resultMap;
	}

}
