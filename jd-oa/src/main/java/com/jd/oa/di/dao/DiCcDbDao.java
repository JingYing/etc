package com.jd.oa.di.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.di.model.DiCcDb;

public interface DiCcDbDao extends BaseDao<DiCcDb, String> {

}
