/**
 * 
 */
package com.jd.oa.di.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.di.dao.DiCcInterfaceDao;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.service.DiCcInterfaceService;

/**
 * @author yujiahe
 * @date 2014-2-20
 * 
 */
@Service("diCcInterfaceService")
public class DiCcInterfaceServiceImpl extends
		BaseServiceImpl<DiCcInterface, String> implements DiCcInterfaceService {
	@Autowired
	private DiCcInterfaceDao diCcInterfaceDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jd.oa.common.service.BaseServiceImpl#getDao()
	 */
	@Override
	public BaseDao<DiCcInterface, String> getDao() {
		// TODO Auto-generated method stub
		return diCcInterfaceDao;
	}

	/**
	 * @return the diCcInterfaceDao
	 */
	public DiCcInterfaceDao getDiCcInterfaceDao() {
		return diCcInterfaceDao;
	}

	/**
	 * @param diCcInterfaceDao
	 *            the diCcInterfaceDao to set
	 */
	public void setDiCcInterfaceDao(DiCcInterfaceDao diCcInterfaceDao) {
		this.diCcInterfaceDao = diCcInterfaceDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jd.oa.di.service.DiCcInterfaceService#mutidelete(java.lang.String)
	 */
	@Override
	public void mutidelete(String ids) {
		// TODO Auto-generated method stub
		String[] args = ids.split(",");
		try {
			for (String id : args) {
				diCcInterfaceDao.delete(id);
			}
		} catch (Exception e) {
			throw new BusinessException("删除接口失败", e);
		}
	}

	@Override
	public DiCcInterface getByUnique(String interfaceName) {
		DiCcInterface entity = new DiCcInterface();
		entity.setInterfaceName(interfaceName);
		List<DiCcInterface> diCcInterfaceList = this.find(entity);
		if (diCcInterfaceList!=null && diCcInterfaceList.size()>0){
			return diCcInterfaceList.get(0);
		}
		// TODO Auto-generated method stub
		return null;
	}	
}
