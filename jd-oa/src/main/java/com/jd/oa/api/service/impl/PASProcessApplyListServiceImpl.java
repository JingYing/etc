/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.service.impl;

import com.jd.common.util.StringUtils;
import com.jd.oa.api.dto.ProcessApplyDto;
import com.jd.oa.api.service.PASProcessApplyListService;
import com.jd.oa.app.dto.ProcessAndTaskInfoDto;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.system.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("pasProcessApplyListServiceImpl")
public class PASProcessApplyListServiceImpl implements PASProcessApplyListService {

	@Autowired
	private ProcessSearchService processSearchService;
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private OaPafService oaPafService;

	private static final Map<String,String> STATUS_MAP = new HashMap<String, String>();

	static {
		STATUS_MAP.put(SystemConstant.PROCESS_STATUS_0,"草稿");
		STATUS_MAP.put(SystemConstant.PROCESS_STATUS_1,"审批中");
		STATUS_MAP.put(SystemConstant.PROCESS_STATUS_2,"退回");
		STATUS_MAP.put(SystemConstant.PROCESS_STATUS_3,"驳回");
		STATUS_MAP.put(SystemConstant.PROCESS_STATUS_4,"取消");
		STATUS_MAP.put(SystemConstant.PROCESS_STATUS_5,"已完成");
		STATUS_MAP.put(SystemConstant.PROCESS_STATUS_6,"草稿");
	}
	@Override
	public List<ProcessApplyDto> getApplyList(String userErp) {
		//返回的结果
		List<ProcessApplyDto> myProcessApplyDtoList = new ArrayList<ProcessApplyDto>();
		//获取我的申请单数据
		List<ProcessAndTaskInfoDto> applys = processSearchService.getMyApplyProTaskInfo(userErp);
		//遍历组合数据
		ProcessApplyDto processApplyDto;
		for(ProcessAndTaskInfoDto processAndTaskInfoDto:applys){
			processApplyDto = new ProcessApplyDto();
			processApplyDto.setProcessCode(processAndTaskInfoDto.getProcessDefinitionId());
			processApplyDto.setProcessKey(processAndTaskInfoDto.getProcessDifinitionKey());
			processApplyDto.setProcessName(processAndTaskInfoDto.getProcessDifinitionName());
			processApplyDto.setReqId(processAndTaskInfoDto.getProcessInstanceId());
			processApplyDto.setReqTime(processAndTaskInfoDto.getBeginTime());
			processApplyDto.setReqName(processAndTaskInfoDto.getProcessInstanceName());
			processApplyDto.setStatusCode(processAndTaskInfoDto.getCurrentStatus());
			processApplyDto.setStatusText(PASProcessApplyListServiceImpl.STATUS_MAP.get(processApplyDto.getStatusCode()));
			if(processAndTaskInfoDto.getOaTaskInstanceList()!=null && processAndTaskInfoDto.getOaTaskInstanceList().size()>0){
				OaTaskInstance oaTaskInstance  =processAndTaskInfoDto.getOaTaskInstanceList().get(processAndTaskInfoDto.getOaTaskInstanceList().size()-1);
				if(oaTaskInstance != null){
					processApplyDto.setCurrentTaskId(oaTaskInstance.getId());
					processApplyDto.setCurrentTaskName(oaTaskInstance.getName());
					List<SysUser> userList = oaPafService.getUsers(userErp,oaTaskInstance.getId());
					String userNames = "";
					String realNames = "";
					if(userList != null && userList.size() > 0){
						for(SysUser user:userList){
							userNames+="," + user.getUserName() ;
							realNames +="," + user.getRealName();
						}
					}
					if(userNames.length() > 0){
						userNames = userNames.substring(1);
					}
					if(realNames.length() > 0){
						realNames = realNames.substring(1);
					}
					processApplyDto.setCurrentTaskAssigneerErp(userNames);
					processApplyDto.setCurrentTaskAssigneerName(realNames);
				}
			}
			myProcessApplyDtoList.add(processApplyDto);
		}
		return myProcessApplyDtoList;
	}

	@Override
	public List<ProcessApplyDto> getApplyList(String userErp, Map<String, String> queryMap) {
		Date queryStartDate = null;
		String queryStartDateString = queryMap.get("queryStartDate");
		if(StringUtils.isNotEmpty(queryStartDateString)){
			queryStartDate = DateUtils.convertFormatDateString_yyyy_MM_dd(queryStartDateString);
		}
		Date queryEndDate = null;
		String queryEndDateString = queryMap.get("queryEndDate");
		if(StringUtils.isNotEmpty(queryEndDateString)){
			queryEndDate = DateUtils.convertFormatDateString_yyyy_MM_dd(queryEndDateString);
		}

		String processCode = queryMap.get("processCode");
		String processTypeCode = queryMap.get("processTypeCode");
		//根据流程定义过滤数据
		StringBuilder processCodeSb = new StringBuilder();
		if(StringUtils.isNotEmpty(processCode)){
			processCodeSb.append(processCode).append("#");
		}
		if(StringUtils.isNotEmpty(processTypeCode)){
			List<ProcessDefinition> processDefinitionList = processDefService.findByProcessType(processTypeCode);
			for(ProcessDefinition processDefinition:processDefinitionList){
				processCodeSb.append(processDefinition.getId()).append("#");
			}
		}
		String processCodeContainerString = processCodeSb.toString();

		String queryStatusCode = queryMap.get("queryStatusCode");

		List<ProcessApplyDto> dtos = getApplyList(userErp);
		List<ProcessApplyDto> removeDtos = new ArrayList<ProcessApplyDto>();
		for(ProcessApplyDto dto:dtos){
			//根据流程定义过滤数据
			if(StringUtils.isNotEmpty(processCodeContainerString)){
				if(!processCodeContainerString.contains(dto.getProcessCode())){
					removeDtos.add(dto);
				}
			}
			if(StringUtils.isNotEmpty(queryStatusCode)){
				if(!queryStatusCode.equals(dto.getStatusCode())){
					removeDtos.add(dto);
				}
			}
			Date date = DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(dto.getReqTime());
			if(queryStartDate != null && queryStartDate.compareTo(date) > 0){
				removeDtos.add(dto);
			}
			if(queryEndDate != null && queryEndDate.compareTo(date) < 0){
				removeDtos.add(dto);
			}
		}
		dtos.removeAll(removeDtos);
		return dtos;
	}
}
