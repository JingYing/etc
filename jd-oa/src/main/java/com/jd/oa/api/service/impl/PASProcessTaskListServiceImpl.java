/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.service.impl;

import com.jd.common.util.DateFormatUtils;
import com.jd.common.util.StringUtils;
import com.jd.oa.api.dto.ProcessApplyDto;
import com.jd.oa.api.dto.ProcessTodoTaskDto;
import com.jd.oa.api.service.PASProcessTaskListService;
import com.jd.oa.app.model.Conditions;
import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.service.ProcessDefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("pasProcessTaskListService")
public class PASProcessTaskListServiceImpl implements PASProcessTaskListService {

	@Autowired
	private ProcessSearchService processSearchService;
	@Autowired
	private ProcessDefService processDefService;


	@Override
	public List<ProcessTodoTaskDto> getTodoTaskList(String userErp) {
		Conditions conditions = new Conditions();
		conditions.setSize("500");
		conditions.setOrder("desc");
		conditions.setSort("createTime");
		return getTodoTaskList(userErp,conditions);
	}

	@Override
	public List<ProcessTodoTaskDto> getTodoTaskList(String userErp,Map<String,String> queryMap) {
		Conditions conditions = new Conditions();
		conditions.setSize("500");
		conditions.setOrder("desc");
		conditions.setSort("createTime");

		Date queryStartDate = null;
		String queryStartDateString = queryMap.get("queryStartDate");
		if(StringUtils.isNotEmpty(queryStartDateString)){
			queryStartDate = DateUtils.convertFormatDateString_yyyy_MM_dd(queryStartDateString);
		}
		Date queryEndDate = null;
		String queryEndDateString = queryMap.get("queryEndDate");
		if(StringUtils.isNotEmpty(queryEndDateString)){
			queryEndDate = DateUtils.convertFormatDateString_yyyy_MM_dd(queryEndDateString);
		}

		String processCode = queryMap.get("processCode");
		String processTypeCode = queryMap.get("processTypeCode");
		//根据流程定义过滤数据
		StringBuilder processCodeSb = new StringBuilder();
		if(StringUtils.isNotEmpty(processCode)){
			processCodeSb.append(processCode).append("#");
		}
		if(StringUtils.isNotEmpty(processTypeCode)){
			List<ProcessDefinition> processDefinitionList = processDefService.findByProcessType(processTypeCode);
			for(ProcessDefinition processDefinition:processDefinitionList){
				processCodeSb.append(processDefinition.getId()).append("#");
			}
		}
		String processCodeContainerString = processCodeSb.toString();

		//过滤申请人
		String queryApplyer = queryMap.get("queryApplyer");
		List<ProcessTodoTaskDto> processTodoTaskDtoList =  getTodoTaskList(userErp,conditions);
		List<ProcessTodoTaskDto> removeDtos = new ArrayList<ProcessTodoTaskDto>();

		for(ProcessTodoTaskDto dto:processTodoTaskDtoList){
			//根据流程定义过滤数据
			if(StringUtils.isNotEmpty(processCodeContainerString)){
				if(!processCodeContainerString.contains(dto.getProcessCode())){
					removeDtos.add(dto);
				}
			}

			if(StringUtils.isNotEmpty(queryApplyer) && !dto.getReqUserErp().contains(queryApplyer) &&
					! dto.getReqUserName().contains(queryApplyer)){
				removeDtos.add(dto);
			}
			Date date = DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(dto.getTaskTime());
			if(queryStartDate != null && queryStartDate.compareTo(date) > 0){
				removeDtos.add(dto);
			}
			if(queryEndDate != null && queryEndDate.compareTo(date) < 0){
				removeDtos.add(dto);
			}
		}
		processTodoTaskDtoList.removeAll(removeDtos);
		return  processTodoTaskDtoList;
	}

	private List<ProcessTodoTaskDto> getTodoTaskList(String userErp,Conditions conditions) {
		List<ProcessTodoTaskDto> myTodoTaskDtoList = new ArrayList<ProcessTodoTaskDto>();
		List<ProcessSearch> processSearchs = processSearchService.findPrcocessTaskListByUserId(userErp,4,null,null,conditions);
		ProcessTodoTaskDto processTodoTaskDto;
		for(ProcessSearch search:processSearchs){
			processTodoTaskDto = new ProcessTodoTaskDto();
			//流程定义信息
			processTodoTaskDto.setProcessCode(search.getProcessDefinitionId());
			processTodoTaskDto.setProcessKey(search.getPafProcessDefinitionId());
			processTodoTaskDto.setProcessName(search.getProcessDefinitionName());
			//流程类型信息
			processTodoTaskDto.setProcessTypeCode(search.getProcessTypeId());
			processTodoTaskDto.setProcessTypeName(search.getProcessTypeName());
			//待办任务信息
			processTodoTaskDto.setTaskId(search.getCurrentTaskNoteInfo().getId());
			processTodoTaskDto.setTaskName(search.getCurrentTaskNoteInfo().getName());
			processTodoTaskDto.setTaskTime(search.getCurrentTaskNoteInfo().getStartTime());
			//申请单信息
			processTodoTaskDto.setReqId(search.getProcessInstanceId());
			processTodoTaskDto.setReqName(search.getProcessInstanceName());
			processTodoTaskDto.setReqFollowCode(search.getFollowCode());
			processTodoTaskDto.setReqTime(DateFormatUtils.format(search.getBeginTime(), "yyyy-MM-dd HH:mm:ss"));
			processTodoTaskDto.setReqUserErp(search.getStarterErp());
			processTodoTaskDto.setReqUserName(search.getRealName());
			processTodoTaskDto.setNodeId(search.getCurrentTaskNoteInfo().getNodeId());
			processTodoTaskDto.setTaskType(search.getCurrentTaskNoteInfo().getTaskType());
			myTodoTaskDtoList.add(processTodoTaskDto);
		}
		//按照时间倒序排列
		Collections.sort(myTodoTaskDtoList, new Comparator<ProcessTodoTaskDto>() {
			@Override
			public int compare(ProcessTodoTaskDto o1, ProcessTodoTaskDto o2) {
				if (o1.getTaskTime() != null && o2.getTaskTime() != null) {
					return o2.getTaskTime().compareTo(o1.getTaskTime());
				}
				return o2.getTaskTime().compareTo(o1.getTaskTime());
			}
		});
		return myTodoTaskDtoList;
	}
}
