/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author yujiahe 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : yujiahe@jd.com
 */
package com.jd.oa.api.service;

public interface PASProcessTaskApprovalService {
    public Object  doProcessApproval(String processInstanceId, String id, String submitUserErp, String submitUserName, String startTime, String submitResult, String submitComments,String taskName,String nodeId,String taskType);
}
