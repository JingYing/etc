package com.jd.oa.api.dto;

/**
 * 申请单审批历史
 * 
 * @author qiuyang
 *
 */
public class ProcessTaskHistoryDto {
	
	private String taskName;
	
	private String submitUserName;
	
	private String startTime;
	
	private String endTime;
	
	private String submitResult;
	
	private String submitComments;

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getSubmitUserName() {
		return submitUserName;
	}

	public void setSubmitUserName(String submitUserName) {
		this.submitUserName = submitUserName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getSubmitResult() {
		return submitResult;
	}

	public void setSubmitResult(String submitResult) {
		this.submitResult = submitResult;
	}

	public String getSubmitComments() {
		return submitComments;
	}

	public void setSubmitComments(String submitComments) {
		this.submitComments = submitComments;
	}

}
