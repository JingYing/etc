/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.service;

import com.jd.oa.api.dto.ProcessApplyDto;
import com.jd.oa.process.model.ProcessApply;

import java.util.List;
import java.util.Map;

public interface PASProcessApplyListService {
	/**
	 * 得到我的申请任务列表
	 * @param userErp
	 * @return
	 */
	public List<ProcessApplyDto> getApplyList(String userErp);

	/**
	 * 查询我的申请列表任务
	 * @param userErp 查询人ERP
	 * @param queryMap 筛选条件
	 * @return
	 */
	public List<ProcessApplyDto> getApplyList(String userErp,Map<String,String> queryMap);
}
