package com.jd.oa.api.controller;

import com.jd.oa.api.dto.ProcessApplyDto;
import com.jd.oa.api.dto.ProcessInstanceDetailDto;
import com.jd.oa.api.dto.ProcessTaskHistoryDto;
import com.jd.oa.api.dto.ProcessTodoTaskDto;
import com.jd.oa.api.service.PASProcessApplyListService;
import com.jd.oa.api.service.PASProcessInstanceDetailService;
import com.jd.oa.api.service.PASProcessTaskApprovalService;
import com.jd.oa.api.service.PASProcessTaskHistoryService;
import com.jd.oa.api.service.PASProcessTaskListService;
import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 申请单审批历史接口
 *
 * @author qiuyang
 */
@Controller
@RequestMapping(value = "/app/service/pas")
public class PASProcessTaskController {
	private static final Logger logger = Logger.getLogger(PASProcessTaskController.class);

    @Autowired
    private PASProcessApplyListService pasProcessApplyListService;
    @Autowired
    private PASProcessTaskListService pasProcessTaskListService;
    @Autowired
    private PASProcessTaskHistoryService pasProcessTaskHistoryService;
    @Autowired
    private PASProcessTaskApprovalService pasProcessTaskApprovalService;
    @Autowired
    private PASProcessInstanceDetailService pasProcessInstanceDetailService;

	/**
	 * request请求 封装成queryMap
	 * @param request
	 * @return
	 */
	private Map<String,String> parseRequest2QueryMap(HttpServletRequest request){
		Map<String,String> queryMap = new HashMap<String, String>();
		Enumeration enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()){
			String key = (String) enumeration.nextElement();
			queryMap.put(key,request.getParameter(key));
		}
		return queryMap;
	}

	/**
	 * 获取我的申请列表
	 */
	@RequestMapping(value = "/getApplyList", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getProcessApplyList(HttpServletRequest request) {
		CallerInfo info = Profiler.registerInfo("wfp_jmeapplylist",false,true); //方法性能监控
		List<ProcessApplyDto> list = new ArrayList<ProcessApplyDto>();
		try {
			String reqUserErp = request.getParameter("reqUserErp");
			Map<String,String> queryMap = parseRequest2QueryMap(request);
			list = pasProcessApplyListService.getApplyList(reqUserErp,queryMap);
			return list;
		} catch (Exception e) {
			logger.error("jme_getApplyList error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return list;
		} finally {
			Profiler.registerInfoEnd(info);
		}
	}

	/**
	 * 获取我的待办任务列表
	 * @return
	 */
	@RequestMapping(value = "/getTaskList", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getProcessTaskList(HttpServletRequest request) {
		CallerInfo info = Profiler.registerInfo("wfp_jmetasklist",false,true); //方法性能监控
		List<ProcessTodoTaskDto> list = new ArrayList<ProcessTodoTaskDto>();
		try {
			String reqUserErp = request.getParameter("reqUserErp");
			Map<String,String> queryMap = parseRequest2QueryMap(request);
			list = pasProcessTaskListService.getTodoTaskList(reqUserErp,queryMap);
			return list;
		} catch (Exception e) {
			logger.error("jme_getTaskList error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return list;
		} finally {
			Profiler.registerInfoEnd(info);
		}
	}


	/**
	 * 得到申请单审批历史信息
	 * @return
	 */
	@RequestMapping(value = "/getTaskHistory", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getTaskHistory(HttpServletRequest request) {
		CallerInfo info = Profiler.registerInfo("wfp_jmetaskhistory",false,true); //方法性能监控
		List<ProcessTaskHistoryDto> processTaskHistorys = new ArrayList<ProcessTaskHistoryDto>();
		try {
			String processInstanceId = request.getParameter("reqId");
			if (processInstanceId == null || processInstanceId.equals("")) return null;
			processTaskHistorys = pasProcessTaskHistoryService
					.getProcessTaskHistory(processInstanceId);
			return processTaskHistorys;
		} catch (Exception e) {
			logger.error("jme_getTaskHistory error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return processTaskHistorys;
		} finally {
			Profiler.registerInfoEnd(info);
		}
	}

    /**
     * 审批操作
     *
     */
    @RequestMapping(value = "/doProcessApproval", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object processTaskApproval(HttpServletRequest request) {
    	CallerInfo info = Profiler.registerInfo("wfp_jmedoprocessapproval",false,true); //方法性能监控
    	Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
			String processInstanceId = request.getParameter("reqId");
			String id = request.getParameter("taskId");
			String submitUserErp = request.getParameter("submitUserErp");
			String submitUserName = request.getParameter("submitUserName");
			String startTime = request.getParameter("submitTime");
			String submitResult = request.getParameter("submitResult");
			String submitComments = request.getParameter("submitComments");
			String taskName = request.getParameter("taskName");
			String nodeId = request.getParameter("nodeId");
			String taskType = request.getParameter("taskType");
			resultMap = (Map<String, Object>)pasProcessTaskApprovalService.doProcessApproval(processInstanceId, id, submitUserErp, submitUserName, startTime, submitResult, submitComments,taskName,nodeId,taskType);
			return resultMap;
        } catch (Exception e) {
			logger.error("jme_processTaskApproval error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return resultMap;
		} finally {
			Profiler.registerInfoEnd(info);
		}
    }
    
    /**
	 * 查询申请单详情
	 * @return
	 */
	@RequestMapping(value="/getProcessInstanceDetail",method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object findProcessInstanceDetail(HttpServletRequest request,String reqId){
		CallerInfo info = Profiler.registerInfo("wfp_jmeprocessinstancedetail",false,true); //方法性能监控
		ProcessInstanceDetailDto processInstanceDetailDto = new ProcessInstanceDetailDto();
		try {
			processInstanceDetailDto = pasProcessInstanceDetailService.findProcessInstanceDetail(request,reqId);
			return processInstanceDetailDto;
		} catch (Exception e) {
			logger.error("jme_getProcessInstanceDetail error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return null;
		} finally {
			Profiler.registerInfoEnd(info);
		}
	}
}

