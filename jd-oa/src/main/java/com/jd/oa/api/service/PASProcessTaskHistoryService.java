package com.jd.oa.api.service;

import java.util.List;

import com.jd.oa.api.dto.ProcessTaskHistoryDto;

public interface PASProcessTaskHistoryService {
	
	/**
	 * 根据申请单ID得到审批历史信息
	 * 
	 * @param processInstanceId
	 * @return
	 */
	public List<ProcessTaskHistoryDto> getProcessTaskHistory(String processInstanceId);

}
