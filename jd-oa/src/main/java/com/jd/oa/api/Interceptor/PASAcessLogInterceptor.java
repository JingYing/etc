/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.Interceptor;

import com.jd.oa.api.auth.PASAuthInfo;
import com.jd.oa.api.auth.UserAuthTools;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.system.model.SysUser;
import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PASAcessLogInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = Logger.getLogger(PASAcessLogInterceptor.class);
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod method = (HandlerMethod) handler;
		//处理用户验证
		PASAuthInfo pasAuthInfo = new PASAuthInfo(request);
		SysUser sysUser = UserAuthTools.auth(pasAuthInfo.getSystemId(),pasAuthInfo.getErp(), pasAuthInfo.getToken());
		request.setAttribute("sysUser",sysUser);
		request.setAttribute("pasAuthInfo",pasAuthInfo);
		//打印接口访问日志
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[Request:")
				.append(pasAuthInfo.getSystemId()).append("||")
				.append(sysUser.getUserName()+"_"+sysUser.getRealName()).append("||")
				.append(request.getRemoteAddr()).append("||")
				.append(request.getRequestURI()).append("||")
				.append(method.getBean().getClass().getName()).append(".")
				.append(method.getMethod().getName()).append("||")
				.append(JsonUtils.toJsonByGoogle(request.getParameterMap()))
				.append("]");
		logger.info(stringBuilder.toString());
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	}
}
