/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.dto;

public class ProcessTodoTaskDto {

	/**
	 * 流程类别
	 */
	private String processTypeCode;
	/**
	 * 流程类别名称
	 */
	private String processTypeName;

	/**
	 * 流程定义Key
	 */
	private String processKey;
	/**
	 * 流程定义Code
	 */
	private String processCode;
	/**
	 * 流程名称
	 */
	private String processName;

	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 任务名称
	 */
	private String taskName;
	/**
	 * 任务到达时间
	 */
	private String taskTime;

	/**
	 * 申请单实例ID
	 */
	private String reqId;
	/**
	 * 申请单主题
	 */
	private String reqName;
	/**
	 * 申请单流水号
	 */
	private String reqFollowCode;
	/**
	 * 申请单申请人erp
	 */
	private String reqUserErp;
	/**
	 * 申请单申请人名称
	 */
	private String reqUserName;
	/**
	 * 申请单-创建时间
	 */
	private String reqTime;

	private String nodeId;

	private String taskType;

	public String getProcessTypeCode() {
		return processTypeCode;
	}

	public void setProcessTypeCode(String processTypeCode) {
		this.processTypeCode = processTypeCode;
	}

	public String getProcessTypeName() {
		return processTypeName;
	}

	public void setProcessTypeName(String processTypeName) {
		this.processTypeName = processTypeName;
	}

	public String getProcessKey() {
		return processKey;
	}

	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskTime() {
		return taskTime;
	}

	public void setTaskTime(String taskTime) {
		this.taskTime = taskTime;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getReqName() {
		return reqName;
	}

	public void setReqName(String reqName) {
		this.reqName = reqName;
	}

	public String getReqFollowCode() {
		return reqFollowCode;
	}

	public void setReqFollowCode(String reqFollowCode) {
		this.reqFollowCode = reqFollowCode;
	}

	public String getReqUserErp() {
		return reqUserErp;
	}

	public void setReqUserErp(String reqUserErp) {
		this.reqUserErp = reqUserErp;
	}

	public String getReqUserName() {
		return reqUserName;
	}

	public void setReqUserName(String reqUserName) {
		this.reqUserName = reqUserName;
	}

	public String getReqTime() {
		return reqTime;
	}

	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
}
