package com.jd.oa.upgrade.biz;

import java.util.List;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeExtendButton;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessNodeExtendButtonService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.upgrade.core.AutoUpgradeBizServerPackAbst;

public class AutoUpgradeBizServerPackImp_201406171629_UpgradeAddExtendButton extends AutoUpgradeBizServerPackAbst{

	@Override
	public String getServerPackName() {
		// TODO Auto-generated method stub
		return _PACK_INFO + "表单子表系统内置按钮补丁" + _PACK_INFO;
	}

	@Override
	public boolean forPlatform() {
		// TODO Auto-generated method stub
		ProcessDefService processDefService = SpringContextUtils.getBean(ProcessDefService.class);
		ProcessNodeService processNodeService = SpringContextUtils.getBean(ProcessNodeService.class);
		FormService formService = SpringContextUtils.getBean(FormService.class);
		FormItemService formItemService = SpringContextUtils.getBean(FormItemService.class);
		ProcessNodeExtendButtonService processNodeExtendButtonService = SpringContextUtils.getBean(ProcessNodeExtendButtonService.class);
		//遍历所有的流程定义
		List<ProcessDefinition> processDefinitions = processDefService.find(null);
		if(processDefinitions != null && processDefinitions.size() > 0){
			for(int i = 0 ; i < processDefinitions.size() ; i++){
				ProcessDefinition processDefinition = processDefinitions.get(i);
				if(processDefinition != null){
					//根据流程定义获取form
					Form form = formService.get(processDefinition.getFormId());
					if(form == null){
						continue;
					}
					//根据form Id获取子表
					List<FormBean> subSheets = formItemService.getFormAndItemsByFormId(form.getId(),SystemConstant.FORM_TYPE_S);
					if(subSheets == null || subSheets.size() == 0){
						continue;
					}
					ProcessNode nodeCondition = new ProcessNode();
					nodeCondition.setProcessDefinitionId(processDefinition.getId());
					List<ProcessNode> processNodes = processNodeService.find(nodeCondition);
					if(processNodes != null && processNodes.size() > 0){
						for(int j = 0 ; j < processNodes.size() ; j++){
							ProcessNode processNode = processNodes.get(j);
							if(processNode != null){
								for(int k = 0 ; k < subSheets.size() ; k++){
									FormBean subSheet = subSheets.get(k);
									if(subSheet != null){
										insertNewButtonRecord(processNodeExtendButtonService, subSheet.getFormId(), subSheet.getFormCode(),processNode.getNodeId(), processDefinition.getId());
										insertDeleteButtonRecord(processNodeExtendButtonService, subSheet.getFormId(), subSheet.getFormCode(),processNode.getNodeId(), processDefinition.getId());
										insertSaveButtonRecord(processNodeExtendButtonService, subSheet.getFormId(), subSheet.getFormCode(),processNode.getNodeId(), processDefinition.getId());
										insertRefreshButtonRecord(processNodeExtendButtonService, subSheet.getFormId(), subSheet.getFormCode(),processNode.getNodeId(), processDefinition.getId());
									}
								}
							}
						}
					}
				}
			}
		}
		return true;
	}
	/**
	 * 插入新建BUTTON记当
	 * @param processNodeExtendButtonService
	 * @param formId
	 * @param formCode
	 * @param nodeId
	 * @param processDefinitionId
	 */
	private void insertNewButtonRecord(ProcessNodeExtendButtonService processNodeExtendButtonService,String formId,String formCode,String nodeId,String processDefinitionId){
		if(isHad(processNodeExtendButtonService, formId, formCode, nodeId, processDefinitionId, "新建") > 0){
			return ;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("新建");
		record.setButtonExegesis("新建");
		record.setFunctionName("subSheetNew_" + formCode + "('" + formCode + "');return false;");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processDefinitionId);
		record.setFormId(formId);
		record.setStatus("0");
		record.setOrderNo(1);
		processNodeExtendButtonService.insert(record);
	}
	/**
	 * 插入删除BUTTON记录
	 * @param processNodeExtendButtonService
	 * @param formId
	 * @param formCode
	 * @param nodeId
	 * @param processDefinitionId
	 */
	private void insertDeleteButtonRecord(ProcessNodeExtendButtonService processNodeExtendButtonService,String formId,String formCode,String nodeId,String processDefinitionId){
		if(isHad(processNodeExtendButtonService, formId, formCode, nodeId, processDefinitionId, "删除") > 0){
			return ;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("删除");
		record.setButtonExegesis("删除");
		record.setFunctionName("subSheetDelete('" + formCode + "')");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processDefinitionId);
		record.setFormId(formId);
		record.setStatus("0");
		record.setOrderNo(3);
		processNodeExtendButtonService.insert(record);
	}
	/**
	 * 插入保存BUTTON记录
	 * @param processNodeExtendButtonService
	 * @param formId
	 * @param formCode
	 * @param nodeId
	 * @param processDefinitionId
	 */
	private void insertSaveButtonRecord(ProcessNodeExtendButtonService processNodeExtendButtonService,String formId,String formCode,String nodeId,String processDefinitionId){
		if(isHad(processNodeExtendButtonService, formId, formCode, nodeId, processDefinitionId, "保存") > 0){
			return ;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("保存");
		record.setButtonExegesis("保存");
		record.setFunctionName("subSheetSave('" + formCode + "')");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processDefinitionId);
		record.setFormId(formId);
		record.setStatus("0");
		record.setOrderNo(2);
		processNodeExtendButtonService.insert(record);
	}
	/**
	 * 插入刷新BUTTON记录
	 * @param processNodeExtendButtonService
	 * @param formId
	 * @param formCode
	 * @param nodeId
	 * @param processDefinitionId
	 */
	private void insertRefreshButtonRecord(ProcessNodeExtendButtonService processNodeExtendButtonService,String formId,String formCode,String nodeId,String processDefinitionId){
		if(isHad(processNodeExtendButtonService, formId, formCode, nodeId, processDefinitionId, "刷新") > 0){
			return ;
		}
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setId(IdUtils.uuid2());
		record.setButtonName("刷新");
		record.setButtonExegesis("刷新");
		record.setFunctionName("subSheetRefresh('" + formCode + "')");
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processDefinitionId);
		record.setFormId(formId);
		record.setStatus("0");
		record.setOrderNo(4);
		processNodeExtendButtonService.insert(record);
	}
	
	private int isHad(ProcessNodeExtendButtonService processNodeExtendButtonService ,String formId,String formCode,String nodeId,String processDefinitionId,String buttonName){
		ProcessNodeExtendButton record = new ProcessNodeExtendButton();
		record.setNodeId(nodeId);
		record.setProcessDefinitionId(processDefinitionId);
		record.setFormId(formId);
		record.setButtonName(buttonName);
		List<ProcessNodeExtendButton> records = processNodeExtendButtonService.selectByCondition(record);
		if(records == null){
			return 0;
		}
		return records.size();
	}
	
}
