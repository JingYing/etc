package com.jd.oa.upgrade.core;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;

import org.springframework.jdbc.core.JdbcTemplate;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.upgrade.biz.AutoUpgradeBizServerPackImp_201406171315_UpgradeAddExtendButton;
import com.jd.oa.upgrade.biz.AutoUpgradeBizServerPackImp_201406171629_UpgradeAddExtendButton;
import com.jd.oa.upgrade.biz.AutoUpgradeBizServerPackImp_201407171629_UpgradeUpdateProcessNodeCandUserType;
import com.jd.oa.upgrade.db.AutoUpgradeDBServerPackImp_201406171515_test;

public class AutoUpgradeUtil {
	private static Hashtable _list = new Hashtable();

	static {
		// 按顺序注册要执行的补丁包程序
		// 注意历史顺序一定不要混淆，新的补丁包的注册按顺序排在最后面
		putServerPack(new AutoUpgradeBizServerPackImp_201406171315_UpgradeAddExtendButton());
		putServerPack(new AutoUpgradeDBServerPackImp_201406171515_test());
		putServerPack(new AutoUpgradeBizServerPackImp_201406171629_UpgradeAddExtendButton());
		putServerPack(new AutoUpgradeBizServerPackImp_201407171629_UpgradeUpdateProcessNodeCandUserType());
	}

	public AutoUpgradeUtil() {
		super();
	}
	private JdbcTemplate jdbcTemplate;
	public AutoUpgradeUtil(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
		autoUpgrade();
	}
	/**
	 * 
	 * 
	 * @preserve 声明此方法不被JOC混淆.
	 */
	private void autoUpgrade() {

		Hashtable dbPack = new Hashtable();//存放DB的pack，优先级最高
		Hashtable bizPack = new Hashtable();//存放Biz的pack，优先级次于DB
		
		// 获取当前数据库的版本号
		int count = 1;
		try {
			count = jdbcTemplate.queryForInt("select count(id) from T_JDOA_SYS_PUB_VERSION");
		} catch (Exception e) {
			return;
		}
		for (int i = (count - 1); i < _list.size(); i++) {
			Object obj = _list.get(Integer.valueOf(i));
			if(obj instanceof AutoUpgradeDBServerPackAbst) {
				AutoUpgradeDBServerPackAbst dbServerpack = (AutoUpgradeDBServerPackAbst) _list.get(new Integer(i));
				dbPack.put(String.valueOf(count), dbServerpack);
			} else {
				AutoUpgradeBizServerPackAbst bizServerpack = (AutoUpgradeBizServerPackAbst) _list.get(new Integer(i));
				bizPack.put(String.valueOf(count), bizServerpack);
			}
			count++;
		}
		
		try {
			StringBuffer log = new StringBuffer();
			
			boolean dbError = false;
			
			String packVersionKey = "";
			Object[] dbKeys = dbPack.keySet().toArray();
			Arrays.sort(dbKeys);
			// 按顺序循环已经注册的数据库补丁程序
			for (int i = 0; i < dbKeys.length; i++) {
				dbError = false;
				packVersionKey = dbKeys[i].toString();
				AutoUpgradeDBServerPackAbst dbServerpack = (AutoUpgradeDBServerPackAbst) dbPack.get(packVersionKey);
				if(checkPackIsExecute(dbServerpack.getClass().getName(), "db")){
					continue;
				}
				System.out.println(dbServerpack.getServerPackName());
				boolean spCheck = dbServerpack.execute(jdbcTemplate);
				
				log.setLength(0);
				log.append(DateUtils.datetimeFormat(new Date())).append(",");
				log.append("系统执行升级包:").append(dbServerpack.getClass().getName()).append(",");
				log.append("升级包信息:");
				log.append(new StringUtils(dbServerpack.getServerPackName()).replace(AutoUpgradeDBServerPackAbst._PACK_INFO, "").replaceAll("'", "`")).append(",");
				log.append("执行状态:").append(spCheck ? "成功" : "失败");
				System.err.println(log);
				
				if (!spCheck) {
					System.out.println("!!!这个补丁没有执行成功:" + dbServerpack.getClass().getName());
					System.err.println("!!!这个补丁没有执行成功:" + dbServerpack.getClass().getName());
					dbError = true;
					break;
				}
				//DB执行过程中出现错误，退出升级过程
				if (dbError) {
					return;
				}
				
				StringBuffer sql = new StringBuffer();
				sql.append("INSERT INTO T_JDOA_SYS_PUB_VERSION ")
				.append("(ID, SERVERPACKCLASSNAME, STATUS, SERVERPACKNAME,TYPE)")
				.append("VALUES(")
				.append("'").append(IdUtils.uuid2()).append("',")
				.append("'").append(dbServerpack.getClass().getName()).append("',")
				.append("'1'").append(",")
				.append("'").append(dbServerpack.getServerPackName()).append("',")
				.append("'db'")
				.append(")");
				jdbcTemplate.execute(sql.toString());
			}
			
			packVersionKey = "";
			Object[] bizKeys = bizPack.keySet().toArray();
			Arrays.sort(bizKeys);
			// 按顺序循环已经注册的数据库补丁程序
			for (int i = 0; i < bizKeys.length; i++) {
				packVersionKey = bizKeys[i].toString();
				AutoUpgradeBizServerPackAbst bizServerpack = (AutoUpgradeBizServerPackAbst) bizPack.get(packVersionKey);
				if(checkPackIsExecute(bizServerpack.getClass().getName(), "biz")){
					continue;
				}
				System.out.println(bizServerpack.getServerPackName());
				boolean spCheck = bizServerpack.execute(jdbcTemplate);
				
				log.setLength(0);
				log.append(DateUtils.datetimeFormat(new Date())).append(",");
				log.append("系统执行升级包:").append(bizServerpack.getClass().getName()).append(",");
				log.append("升级包信息:");
				log.append(new StringUtils(bizServerpack.getServerPackName()).replace(AutoUpgradeBizServerPackAbst._PACK_INFO, "")).append(",");
				log.append("执行状态:").append(spCheck?"成功":"失败");
				System.err.println(log);
				if (!spCheck) {
					System.out.println("!!!这个补丁没有执行成功:" + bizServerpack.getClass().getName());
					System.err.println("!!!这个补丁没有执行成功:" + bizServerpack.getClass().getName());
					break;
				}
				StringBuffer sql = new StringBuffer();
				sql.append("INSERT INTO T_JDOA_SYS_PUB_VERSION ")
					.append("(ID, SERVERPACKCLASSNAME, STATUS, SERVERPACKNAME,TYPE)")
					.append("VALUES(")
					.append("'").append(IdUtils.uuid2()).append("',")
					.append("'").append(bizServerpack.getClass().getName()).append("',")
					.append("'1'").append(",")
					.append("'").append(bizServerpack.getServerPackName()).append("',")
					.append("'biz'")
					.append(")");
				jdbcTemplate.execute(sql.toString());
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
		}
		dbPack.clear();
		dbPack = null;
		bizPack.clear();
		bizPack = null;
	}
	private boolean checkPackIsExecute(String packClassName,String type){
		int flag = jdbcTemplate.queryForInt("select count(id) from T_JDOA_SYS_PUB_VERSION where SERVERPACKCLASSNAME='" + packClassName + "' AND TYPE='" + type + "'");
		if(flag == 1 ){
			return true;
		}
		return false;
	}
	/**
	 * 将系统所有的ServerPack装载到列表中
	 * 
	 * @param dbServerpack
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public static void putServerPack(AutoUpgradeDBServerPackAbst dbServerpack) {
		_list.put(Integer.valueOf(_list.size()), dbServerpack);
	}

	/**
	 * 将系统所有的ServerPack装载到列表中
	 * 
	 * @param bizServerpack
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public static void putServerPack(AutoUpgradeBizServerPackAbst bizServerpack) {
		_list.put(Integer.valueOf(_list.size()), bizServerpack);
	}
}
