package com.jd.oa.upgrade.core;

import org.springframework.jdbc.core.JdbcTemplate;

public interface UpgradeServerPackInterface {
	/**
	 * 补丁包名称介绍
	 * @return
	 */
	public abstract String getServerPackName();
	/**
	 * 执行升级程序
	 * @return
	 */
	public abstract boolean execute(JdbcTemplate jdbcTemplate);

}
