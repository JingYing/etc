package com.jd.oa.upgrade.core;

import org.springframework.jdbc.core.JdbcTemplate;

public abstract class AutoUpgradeBizServerPackAbst implements UpgradeServerPackInterface {

	public final static String _PACK_INFO = "\n******** Platform PACK Info*************\n";
	/**
	 * 升级包机制,只针对平台级的升级，不对数据库操作的升级补丁继承该抽象类
	 */
	public boolean execute(JdbcTemplate jdbcTemplate) {
		return forPlatform();
	}

	public abstract boolean forPlatform();

}
