package com.jd.oa.common.dao.dialect;

import java.util.LinkedHashMap;

public interface Dialect {

    public boolean supportsLimit();

    public String getLimitString( String sql, boolean hasOffset, int offset, int limit );

    public String getLimitString( String sql, int offset, int limit );
    
    public abstract String getOrderString(String sql, LinkedHashMap<String, String> orderItems);
}
