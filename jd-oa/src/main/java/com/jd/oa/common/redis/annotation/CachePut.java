package com.jd.oa.common.redis.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: dbcuizhen
 * @time: 13-8-13  下午5:32
 * @version: 1.0
 * @since: 1.0
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CachePut {
    boolean cacheNull() default false;

    /**
     * Spring Expression Language (SpEL) attribute used for conditioning the cache update.
     * <p>Default is "", meaning the method result is always cached.
     */
    String condition() default "";

    int expire();

    String expireTime() default "";

    /**
     * Spring Expression Language (SpEL) attribute for computing the key dynamically.
     * <p>Default is "", meaning all method parameters are considered as a key.
     */
    CacheKey key();

    String type() default "";

    /**
     * Spring Expression Language (SpEL) attribute used to veto the cache update.
     * <p>Unlike {@link #condition()}, this expression is evaluated after the method
     * has been called and can therefore refer to the {@code result}. Default is "",
     * meaning that caching is never vetoed.
     *
     * @since 3.2
     */
    String unless() default "";

    /**
     * Name of the caches in which the update takes place.
     * <p>May be used to determine the target cache (or caches), matching the
     * qualifier value (or the bean name(s)) of (a) specific bean definition.
     */
    //String[] value();
}

