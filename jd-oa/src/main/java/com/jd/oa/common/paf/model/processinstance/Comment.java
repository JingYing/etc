package com.jd.oa.common.paf.model.processinstance;

/**
 * 审批意见(查询流程实例专用)
 * User: zhaoming
 * Date: 13-10-11
 * Time: 下午8:21
 * To change this template use File | Settings | File Templates.
 */
public class Comment {

    private String taskId;
    private String userId;
    private String messageTime;
    private String content;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
