/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-4-15
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.common.saf;

import com.alibaba.dubbo.rpc.service.GenericService;
import com.jd.bk.saf.config.SAFReferenceConfig;
import com.jd.bk.saf.config.SAFRegistryConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SafClient {
	//saf configure propery alias
	public final  static  String SAF_CLIENT_VERSION = "version";
	public final  static  String SAF_CLIENT_GROUP = "group";
	public final  static  String SAF_CLIENT_TOKEN = "token";
	public final  static  String SAF_REGISTRY_PROTOCOL = "registryProtocol";
	public final  static  String SAF_CLIENT_PROTOCOL = "clientProtocol";
	public final  static  String SAF_CLIENT_TIMEOUT= "timeout";
	public final  static  String SAF_CLIENT_ZKADDRESS = "zkAddress";
	public final  static  String SAF_CLIENT_SAFTOKENFILTER= "safTokenFilter";

	//saf客户端配置信息
	private Map<String,String> safConfMap;
	//saf注册中心
	private SAFRegistryConfig safRegistry;
	// 引用远程服务（必须）
	SAFReferenceConfig<GenericService> referenceConfig;

	//无参构造函数
	public SafClient(){}

	//形参构造函数
	public SafClient(Map<String,String>safConfMap){
		this.safConfMap = safConfMap;
		this.init();
	}

	public void init(){

		//注册中心实现
		this.safRegistry = new SAFRegistryConfig();
		this.safRegistry.setAddress(this.safConfMap.get(SAF_CLIENT_ZKADDRESS));
		this.safRegistry.setProtocol(this.safConfMap.get(SAF_REGISTRY_PROTOCOL));
		List<SAFRegistryConfig> registries = new ArrayList<SAFRegistryConfig>();
		registries.add(safRegistry);

		// 引用远程服务
		referenceConfig = new SAFReferenceConfig<GenericService>();
		referenceConfig.setRegistries(registries);
		referenceConfig.setGroup(this.safConfMap.get(SAF_CLIENT_GROUP));
		referenceConfig.setVersion(this.safConfMap.get(SAF_CLIENT_VERSION));
		referenceConfig.setGeneric(true); // 声明为泛化接口
		referenceConfig.setProtocol(this.safConfMap.get(SAF_CLIENT_PROTOCOL));
		referenceConfig.setTimeout(Integer.parseInt(this.safConfMap.get(SAF_CLIENT_TIMEOUT)));
		referenceConfig.setFilter(this.safConfMap.get(SAF_CLIENT_SAFTOKENFILTER));
	}

	/**
	 * saf 方法调用
	 * @param method 方法
	 * @param paramTypes 参数类型数组
	 * @param params 参数
	 * @return 返回结果
	 */
	public Object invoke(String url,String method,String[] paramTypes,Object[] params){
		this.referenceConfig.setInterface(url); // 弱类型接口名
		GenericService genericService = this.referenceConfig.get();
		Object result = genericService.$invoke(method,paramTypes,params);
		return result;
	}

	/**
	 * 销毁会话
	 */
	public void destory(){
		if(this.referenceConfig != null){
			try {
				this.referenceConfig.destroy();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
