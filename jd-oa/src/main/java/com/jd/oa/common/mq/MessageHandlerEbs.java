package com.jd.oa.common.mq;

import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jd.activemq.consumer.TextMessageListener;
import com.jd.oa.agent.service.UnifyAgentReceiveService;
import com.jd.oa.agent.service.impl.UnifyAgentReceiveServiceImpl;
import com.jd.oa.common.utils.SpringContextUtils;
/**
 * ebs
 * @author liubo
 *
 */
public class MessageHandlerEbs extends TextMessageListener{
	
	private static final Log logger = LogFactory.getLog(MessageHandlerEbs.class);
	
	private UnifyAgentReceiveService unifyAgentReceiveService;
	
	@Override
	protected void onMessage(String message) throws Exception {
		// TODO Auto-generated method stub
		System.out.println(message);
		String txt = message;
		try {
            String gbk = new String(txt.getBytes("ISO-8859-1"), "GBK");
            System.out.println(gbk);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
		logger.info(message);
		if(this.unifyAgentReceiveService == null){
			this.unifyAgentReceiveService = SpringContextUtils.getBean(UnifyAgentReceiveService.class);
		}
		this.unifyAgentReceiveService.receive(message);
	}
	
}
