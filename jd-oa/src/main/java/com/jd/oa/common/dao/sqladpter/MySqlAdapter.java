package com.jd.oa.common.dao.sqladpter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * mysql适配器
 * 
 * @author zhouhuaqi
 * 
 */
public class MySqlAdapter extends BaseSqlAdapter {

	private static Logger logger = LoggerFactory.getLogger(MySqlAdapter.class);

	/**
	 * dll操作需要添加的列信息
	 * 
	 * @param oldClumnName
	 *            老的列名
	 * @param columnName
	 *            列名
	 * @param dataType
	 *            数据类型
	 * @param dataLength
	 *            数据长度
	 * @param isPK
	 *            是否主键
	 * @param isNull
	 *            是否为NULL
	 * @param defaultValue
	 *            默认值
	 * @param comment
	 *            注释
	 * @return
	 */
	public BaseSqlAdapter addColumn(String oldColumnName, String columnName,
			DataType dataType, String dataLength, boolean isPK, boolean isNull,
			String defaultValue, String comment) {
		StringBuffer sql = new StringBuffer();
		int length;
		sql.append((oldColumnName != null ? "`" + oldColumnName + "` " : "")
				+ "`" + columnName + "` ");
		switch (dataType) {
		case STRING:
			length = !dataLength.contains(",") ? Float.valueOf(dataLength)
					.intValue() : 0;
			if (length == 1) {
				sql.append("CHAR(1)");
			} else if (length > 2000) {
				sql.append("TEXT");
			} else {
				sql.append("VARCHAR(" + length + ")");
			}
			break;
		case NUMBER:
			if (dataLength != null) {
				if (!dataLength.contains(",")) {
					length = Integer.valueOf(dataLength).intValue();
					if (length == 1)
						sql.append("TINYINT(1)");
					else if (length > 1 && length <= 10)
						sql.append("INT");
					else if (length > 10)
						sql.append("BIGINT");
				} else {
					sql.append("DECIMAL"
							+ ("(" + dataLength.split(",")[0] + ","
									+ dataLength.split(",")[1] + ")"));
				}
			} else {
				sql.append("INT");
			}
			break;
		case DATETIME:
			sql.append("DATETIME");
			break;
		default:
			sql.append(dataType.name());
			break;
		}
		sql.append((isNull ? " NULL" : " NOT NULL"));
		if (StringUtils.isNotEmpty(defaultValue) && !"null".equalsIgnoreCase(defaultValue)) {
			if (dataType.equals(DataType.NUMBER)) {
				sql.append(" DEFAULT " + defaultValue);
			} else if (dataType.equals(DataType.DATETIME)) {
				if (defaultValue.equals("NOW")) {
					sql.append(" DEFAULT " + defaultValue + "()");
				} else {
					sql.append(" DEFAULT '" + defaultValue + "'");
				}
			} else {
				sql.append(" DEFAULT '" + defaultValue + "'");
			}
		}
		if (comment != null && !"".equals(comment)) {
			sql.append(" COMMENT '" + comment + "'");
		}
		if (isPK)
			columnMap.put("PK_" + columnName, sql.toString());
		else
			columnMap.put(columnName, sql.toString());
		return this;
	}

	/**
	 * 目前仅支持 Oracle, MySQL ; 日期查询生成 其他数据库方式请根据需要自行扩展
	 */
	protected String getDateTimeQuery(String columnName, RelationalOperator ro,String columnValue, DataType dataType) {
		switch (dataType) {
		case DATE:
			return getColumnSql(columnName).concat(ro.toString()).concat("'").concat(columnValue).concat("'");
		case TIME:
			return getColumnSql(columnName).concat(ro.toString()).concat("'").concat(columnValue).concat("'");
		case DATETIME:
			return getColumnSql(columnName).concat(ro.toString()).concat("'").concat(columnValue).concat("'");
		default:
			break;
		}

		return getColumnSql(columnName).concat(" ").concat(ro.toString())
				.concat(" \"").concat(columnValue).concat("\"");
	}

	/**
	 * main()
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * 创建数据表
		 */
		SqlAdapter adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DDL_CREATE_TABLE)
				.addTable("T_BO_TEST")
				.addColumn(null, "ID", DataType.STRING, "32", Boolean.FALSE,
						Boolean.FALSE, null, "")
				.addColumn(null, "NAME", DataType.STRING, "50", Boolean.FALSE,
						Boolean.TRUE, null, "")
				.addColumn(null, "BIRTHDAY", DataType.DATE, null,
						Boolean.FALSE, Boolean.TRUE, "2012-09-10", "")
				.addColumn(null, "AGE", DataType.NUMBER, null, Boolean.FALSE,
						Boolean.TRUE, null, "")
				.addColumn(null, "DESC", DataType.STRING, "1000",
						Boolean.FALSE, Boolean.TRUE, null, "");
		logger.info(adapter.toSql());

		/**
		 * 增加字段
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DDL_ADD_COLUMN)
				.addTable("T_BO_TEST")
				.addColumn(null, "BIRTHDAY", DataType.DATE, null,
						Boolean.FALSE, Boolean.TRUE, null, "出生日期");
		logger.info(adapter.toSql());

		/**
		 * 修改字段
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DDL_CHANGE_COLUMN)
				.addTable("T_BO_TEST")
				.addColumn("NAME", "NAME", DataType.STRING, "150",
						Boolean.FALSE, Boolean.TRUE, null, "");
		logger.info(adapter.toSql());

		/**
		 * 查询
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DML_SELECT)
				.addTable("base_user")
				.addTable("base_group")
				.addColumn("base_user.user_name")
				.addColumn("base_group.user_age")
				.addAndCondition("id", RelationalOperator.EQ, "xxxx",
						DataType.STRING)
				.addAndInCondition("base_group.user_age", "'x','x','x','x','x'")
				.addOrLikeCondition("xxx", "yyyy")
				.addGroupBy("base_user.user_name")
				.addAscOrderBy("base_user.create_time")
				.addDescOrderBy("base_user.id");
		logger.info(adapter.toSql());
		
		/**
		 * 更新
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DML_UPDATE);
		adapter.addTable("Test");
		adapter.addColumn("ID", "1234567890-", DataType.STRING);
		adapter.addColumn("SEND_TIME","2013-10-18 13:00:11", DataType.DATETIME);
		adapter.addAndCondition("ID", RelationalOperator.EQ, "1234567890-", DataType.STRING);
		adapter.addAndCondition("BIRTHDAY", RelationalOperator.EQ, "1983-10-18", DataType.DATE);
		adapter.addAndCondition("SEND_TIME", RelationalOperator.EQ, "2013-10-18 13:00:11", DataType.DATETIME);
		adapter.addAndCondition("TIME1", RelationalOperator.EQ, "13:00:11", DataType.TIME);
		logger.info(adapter.toSql());
		
		/**
		 * 插入
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DML_INSERT);
		adapter.addTable("Test");
		adapter.addColumn("ID", "1234567890-", DataType.STRING);
		adapter.addColumn("SEND_TIME","2013-10-18 13:00:11 0", DataType.DATETIME);
		adapter.addColumn("BIRTHDAY","2012-10-19", DataType.DATE);
		adapter.addColumn("TIME1", "13:00:11", DataType.TIME);
		logger.info(adapter.toSql());
		
	}
}
