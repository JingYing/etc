package com.jd.oa.common.mail.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimePart;
import javax.mail.internet.MimeUtility;
import javax.mail.search.FlagTerm;
import javax.mail.search.SearchTerm;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.jd.oa.common.mail.MailAuthenticator;
import com.jd.oa.common.mail.MailReceiveService;
import com.sun.mail.imap.IMAPBodyPart;

public class MailReceiveServiceImpl implements MailReceiveService {

	private static final Logger logger = Logger
			.getLogger(MailReceiveServiceImpl.class);

	/** The default protocol: 'pop3' */
	public static final String DEFAULT_PROTOCOL = "pop3";

	/** The default port: -1 */
	public static final int DEFAULT_PORT = 110;

	private Session session;

	private String protocol = DEFAULT_PROTOCOL;

	private String host;

	private int port = DEFAULT_PORT;

	private String username;

	private String password;

	private boolean validate;

	// 与邮件服务器连接后得到的邮箱
	private Store store;
	// 收件箱
	private Folder folder;

	// 保存邮件的路径
	private String attachmentDir;
	private String emailDir;
	private String emailFileSuffix = ".eml";

	public List<Message> getAllMails() {
		return getMails(MailFlag.ALL);
	}

	public List<Message> getNewMails() {
		return getMails(MailFlag.NEW);
	}

	public List<Message> getUnreadMails() {
		return getMails(MailFlag.UNREAD);
	}

	@Override
	public List<Message> searchMail(SearchTerm searchTerm) {
		try {
			 Message[] messages = getFolder().search(searchTerm);
			 return Arrays.asList(messages);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean setMailFlag(Message message, MailFlag flag) {
		try {
			openFolder(message);
			switch(flag){
			case DELETED:
					message.setFlag(Flags.Flag.DELETED, true);
					logger.info("邮件[" + message.getSubject() + "]被删除");
					break;
			case SEEN:
					message.setFlag(Flags.Flag.SEEN, true);
					logger.info("邮件[" + message.getSubject() + "]状态更新为已读");
					break;
			case UNREAD:
				message.setFlag(Flags.Flag.SEEN, false);
				logger.info("邮件[" + message.getSubject() + "]状态更新为未读");
				break;
			default:
				break;
			}
			return true;
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		finally{
			closeFolder(message, true);
		}
		return false;
	}

	public int getMessageCount() {
		try {
			return getFolder().getMessageCount();
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return 0;
	}

	public int getNewMessageCount() {
		try {
			return getFolder().getNewMessageCount();
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return 0;
	}

	public int getUnreadMessageCount() {
		try {
			return getFolder().getUnreadMessageCount();
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return 0;
	}
	
	public int getDeletedMessageCount() {
		try {
			return getFolder().getDeletedMessageCount();
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return 0;
	}

	/**
	 * 获得邮件的收件人，抄送，和密送的地址和姓名，根据所传递的参数的不同 "to"----收件人 "cc"---抄送人地址 "bcc"---密送人地址
	 */
	public String getTOAddress(Message message) {
		return getMailAddress("TO", message);
	}

	public String getCCAddress(Message message) {
		return getMailAddress("CC", message);
	}

	public String getBCCAddress(Message message) {
		return getMailAddress("BCC", message);
	}

	public String getFrom(Message message) {
		String fromaddr = null;
		try {
			openFolder(message);
			InternetAddress[] address;
			address = (InternetAddress[]) message.getFrom();

			// 获得发件人的邮箱
			String from = address[0].getAddress();
			if (from == null) {
				from = "";
			}
			// 获得发件人的描述信息
			String personal = address[0].getPersonal();
			if (personal == null) {
				personal = "";
			}
			// 拼成发件人完整信息
			fromaddr = personal + "<" + from + ">";
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		finally{
			closeFolder(message, false);
		}
		return fromaddr;
	}

	public String getSubject(Message message) {
		String subject = null;
		// 将邮件主题解码
		try {
			openFolder(message);
			subject = MimeUtility.decodeText(message.getSubject());
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		finally{
			closeFolder(message, false);
		}
		return subject;
	}

	public Date getSentDate(Message message) {
		try {
			openFolder(message);
			return message.getSentDate();
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		finally{
			closeFolder(message, false);
		}
		return null;
	}

	public boolean getReplySign(Message message) {
		boolean replysign = false;
		String needreply[];
		try {
			openFolder(message);
			needreply = message.getHeader("Disposition-Notification-To");
			if (needreply != null) {
				replysign = true;
			}
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		finally{
			closeFolder(message, false);
		}
		return replysign;
	}

	public boolean isContainAttach(Part part) {
		boolean attachflag = false;
		try {
			getFolder();
			if (part.isMimeType("multipart/*")) {
				// 如果邮件体包含多部分
				Multipart mp = (Multipart) part.getContent();
				// 遍历每部分
				for (int i = 0; i < mp.getCount(); i++) {
					// 获得每部分的主体
					BodyPart bodyPart = mp.getBodyPart(i);
					String disposition = bodyPart.getDisposition();
					if ((disposition != null)
							&& ((disposition.equals(Part.ATTACHMENT)) || (disposition
									.equals(Part.INLINE)))) {
						attachflag = true;
					} else if (bodyPart.isMimeType("multipart/*")) {
						attachflag = isContainAttach((Part) bodyPart);
					} else {
						String contype = bodyPart.getContentType();
						if (contype.toLowerCase().indexOf("application") != -1) {
							attachflag = true;
						}
						if (contype.toLowerCase().indexOf("name") != -1) {
							attachflag = true;
						}
					}
				}
			} else if (part.isMimeType("message/rfc822")) {
				attachflag = isContainAttach((Part) part.getContent());
			}
			getFolder().close(false);
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return attachflag;
	}

	public String getContent(Message message) {
		try {
			openFolder(message);
			Object content;
			content = message.getContent();
			// 处理多部分邮件
			if (content instanceof Multipart) {
				return handleMultipart((Multipart) content);
			} else {
				return handlePart(message, false);
			}
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		finally{
			closeFolder(message, false);
		}
		return null;
	}

	/**
	 * 获取邮件列表
	 * 
	 * @param flag
	 *            是否只获取新邮件（未读邮件）
	 * @return
	 */
	private List<Message> getMails(MailFlag flag) {
		List<Message> result = new ArrayList<Message>();
		try {
			SearchTerm searchTerm;
			Flags flags;
			switch (flag) {
			case NEW:
				flags = getFolder().getPermanentFlags();
				flags.add(Flags.Flag.RECENT);
				searchTerm = new FlagTerm(flags, true);
				Message[] newMessages = getFolder().search(searchTerm);
				result = Arrays.asList(newMessages);
				break;
			case UNREAD:
				flags = getFolder().getPermanentFlags();
				flags.add(Flags.Flag.SEEN);
				searchTerm = new FlagTerm(flags, false);
				Message[] unReadMessages = getFolder().search(searchTerm);
				result = Arrays.asList(unReadMessages);
				break;
			default:
				Message[] allMessages = getFolder().getMessages();
				result = Arrays.asList(allMessages);
				break;
			}
			getFolder().close(false);
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * 获得邮件地址
	 * 
	 * @param type
	 *            类型，如收件人、抄送人、密送人
	 * @param message
	 *            邮件消息
	 * @return
	 * @throws Exception
	 */
	private String getMailAddress(String type, Message message) {
		String mailaddr = "";
		StringBuilder mailaddrSb = new StringBuilder("");
		try {
			String addtype = type.toUpperCase();
			InternetAddress[] address = null;
			openFolder(message);
			if (addtype.equals("TO") || addtype.equals("CC")
					|| addtype.equals("BCC")) {
				if (addtype.equals("TO")) {

					address = (InternetAddress[]) message
							.getRecipients(Message.RecipientType.TO);

				} else if (addtype.equals("CC")) {
					address = (InternetAddress[]) message
							.getRecipients(Message.RecipientType.CC);
				} else {
					address = (InternetAddress[]) message
							.getRecipients(Message.RecipientType.BCC);
				}
				if (address != null) {
					for (int i = 0; i < address.length; i++) {
						// 先获取邮件地址
						String email = address[i].getAddress();
						if (email == null) {
							email = "";
						} else {
							email = MimeUtility.decodeText(email);
						}
						// 再取得个人描述信息
						String personal = address[i].getPersonal();
						if (personal == null) {
							personal = "";
						} else {
							personal = MimeUtility.decodeText(personal);
						}
						// 将个人描述信息与邮件地址连起来
						String compositeto = personal + "<" + email + ">";
						// 多个地址时，用逗号分开
//						mailaddr += "," + compositeto;
						mailaddrSb.append("," + compositeto);
					}
					mailaddr = mailaddrSb.toString();
					mailaddr = mailaddr.substring(1);
				}
			} else {
				logger.error("错误的地址类型！");
			}
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}
		finally{
			closeFolder(message, false);
		}
		return mailaddr;
	}

	private String getMessageId(Message message) throws MessagingException {
		return ((MimeMessage) message).getMessageID();
	}

	/**
	 * 邮件是否是新邮件
	 * 
	 * @param message
	 * @return
	 * @throws MessagingException
	 */
	private boolean isNew(Message message) throws MessagingException {
		boolean isnew = false;
		Flags flags = message.getFlags();
		Flags.Flag[] flag = flags.getSystemFlags();
		for (int i = 0; i < flag.length; i++) {
			if (flags.contains(Flags.Flag.ANSWERED)) {
				isnew = true;
				logger.info("Flags.Flag.ANSWERED");
				break;
			} else if (!flags.contains(Flags.Flag.DRAFT)) {
				isnew = true;
				logger.info("Flags.Flag.DRAFT");
				break;
			} else if (!flags.contains(Flags.Flag.FLAGGED)) {
				isnew = true;
				logger.info("Flags.Flag.FLAGGED");
				break;
			} else if (!flags.contains(Flags.Flag.RECENT)) {
				isnew = true;
				logger.info("Flags.Flag.RECENT");
				break;
			} else if (!flags.contains(Flags.Flag.USER)) {
				isnew = true;
				logger.info("Flags.Flag.USER");
				break;
			} else if (flags.contains(Flags.Flag.SEEN)) {
				isnew = true;
				logger.info("Flags.Flag.SEEN");
				break;
			}
		}
		return isnew;
	}

	private boolean openFolder(Message message) {
		try {
			if (!message.getFolder().isOpen()) {
				message.getFolder().open(Folder.READ_WRITE);
			}
			return true;
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}
	
	private boolean closeFolder(Message message, boolean closeFlag) {
		try {
			if (message.getFolder().isOpen()) {
				message.getFolder().close(closeFlag);;
			}
			return true;
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}

	/**
	 * 保存邮件源文件
	 */
	private void saveMessageAsFile(Message message) {
		try {
			// 将邮件的ID中尖括号中的部分做为邮件的文件名
			String oriFileName = getInfoBetweenBrackets(this.getMessageId(
					message).toString());
			// 设置文件后缀名。若是附件则设法取得其文件后缀名作为将要保存文件的后缀名，
			// 若是正文部分则用.htm做后缀名
			String emlName = oriFileName;
			String fileNameWidthExtension = getEmailDir() + oriFileName
					+ getEmailFileSuffix();
			File storeFile = new File(fileNameWidthExtension);
			for (int i = 0; storeFile.exists(); i++) {
				emlName = oriFileName + i;
				fileNameWidthExtension = getEmailDir() + emlName
						+ getEmailFileSuffix();
				storeFile = new File(fileNameWidthExtension);
			}
			logger.debug("邮件消息的存储路径: " + fileNameWidthExtension);
			// 将邮件消息的内容写入ByteArrayOutputStream流中
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			message.writeTo(baos);
			// 读取邮件消息流中的数据
			StringReader in = new StringReader(baos.toString());
			// 存储到文件
			saveFile(fileNameWidthExtension, in);
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * 解析Multipart
	 */
	private String handleMultipart(Multipart multipart) {
		String result = null;
		try {
			for (int i = 0, n = multipart.getCount(); i < n; i++) {
				result = handlePart(multipart.getBodyPart(i), false);
				break;
			}
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

	/*
	 * 解析指定part,从中提取文件
	 */
	private String handlePart(Part part, boolean isSave) {
		String result = null;
		// 获得邮件的内容输入流
		InputStreamReader sbis = null;
		try {
			sbis = new InputStreamReader(part.getInputStream());

			if (isSave) {
				String disposition = part.getDisposition();
				String contentType = part.getContentType();
				String fileNameWidthExtension = "";

				String oriFileName = getInfoBetweenBrackets(this.getMessageId(
						(Message) part).toString());
				// 设置文件后缀名。若是附件则设法取得其文件后缀名作为将要保存文件的后缀名，
				// 若是正文部分则用.htm做后缀名
				String emailName = oriFileName;
				File storeFile = new File(fileNameWidthExtension);
				for (int i = 0; storeFile.exists(); i++) {
					emailName = oriFileName + i;

				}
				// 没有附件的情况
				if (disposition == null) {
					if ((contentType.length() >= 10)
							&& (contentType.toLowerCase().substring(0, 10)
									.equals("text/plain"))) {
						fileNameWidthExtension = getAttachmentDir() + emailName
								+ ".txt";
					} else if ((contentType.length() >= 9) // Check if html
							&& (contentType.toLowerCase().substring(0, 9)
									.equals("text/html"))) {
						fileNameWidthExtension = getAttachmentDir() + emailName
								+ ".html";
					} else if ((contentType.length() >= 9) // Check if html
							&& (contentType.toLowerCase().substring(0, 9)
									.equals("image/gif"))) {
						fileNameWidthExtension = getAttachmentDir() + emailName
								+ ".gif";
					} else if ((contentType.length() >= 11)
							&& contentType.toLowerCase().substring(0, 11)
									.equals("multipart/*")) {
						// System.out.println("multipart body: " + contentType);
						handleMultipart((Multipart) part.getContent());
					} else { // Unknown type
						// System.out.println("Other body: " + contentType);
						fileNameWidthExtension = getAttachmentDir() + emailName
								+ ".txt";
					}
					// 存储内容文件
					logger.debug("保存邮件内容到：" + fileNameWidthExtension);
					saveFile(fileNameWidthExtension, sbis);
				}

				// 各种有附件的情况
				String name = "";
				if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
					name = getFileName(part);
					// System.out.println("Attachment: " + name + " : "
					// + contentType);
					fileNameWidthExtension = getAttachmentDir() + name;
				} else if (Part.INLINE.equalsIgnoreCase(disposition)) {
					name = getFileName(part);
					// System.out.println("Inline: " + name + " : "
					// + contentType);
					fileNameWidthExtension = getAttachmentDir() + name;
				} else {
					// System.out.println("Other: " + disposition);
				}
				// 存储各类附件
				if (!fileNameWidthExtension.equals("")) {
					logger.debug("保存邮件附件到：" + fileNameWidthExtension);
					saveFile(fileNameWidthExtension, sbis);
				}
			}
			
			InputStream is = part.getInputStream();
			String contentType = StringUtils.lowerCase(((MimePart)part).getContentType());
			if(contentType.contains("gb2312") || contentType.contains("gbk"))
				result = IOUtils.toString(is,"GBK");
			else
				result = IOUtils.toString(is);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
        	try{
        		if(sbis!=null){
        			sbis.close();
        		}
        	} catch(IOException e) {
        	}
        }
		return result;
	}

	private String getFileName(Part part) throws MessagingException,
			UnsupportedEncodingException {
		String fileName = part.getFileName();
		fileName = MimeUtility.decodeText(fileName);
		String name = fileName;
		if (fileName != null) {
			int index = fileName.lastIndexOf("/");
			if (index != -1) {
				name = fileName.substring(index + 1);
			}
		}
		return name;
	}

	/**
	 * 保存文件内容
	 * 
	 * @param fileName
	 *            文件名
	 * @param input
	 *            输入流
	 * @throws IOException
	 */
	private void saveFile(String fileName, Reader input) throws IOException {

		// 为了放置文件名重名，在重名的文件名后面天上数字
		File file = new File(fileName);
		// 先取得文件名的后缀
		int lastDot = fileName.lastIndexOf(".");
		String extension = fileName.substring(lastDot);
		fileName = fileName.substring(0, lastDot);
		for (int i = 0; file.exists(); i++) {
			// 　如果文件重名，则添加i
			file = new File(fileName + i + extension);
		}
		// 从输入流中读取数据，写入文件输出流
		FileWriter fos = new FileWriter(file);
		BufferedWriter bos = new BufferedWriter(fos);
		BufferedReader bis = new BufferedReader(input);
		int aByte;
		while ((aByte = bis.read()) != -1) {
			bos.write(aByte);
		}
		// 关闭流
		bos.flush();
		bos.close();
		bis.close();
	}

	/**
	 * 获得尖括号之间的字符
	 * 
	 * @param str
	 * @return
	 * @throws Exception
	 */
	private String getInfoBetweenBrackets(String str) throws Exception {
		int i, j; // 用于标识字符串中的"<"和">"的位置
		if (str == null) {
			str = "error";
			return str;
		}
		i = str.lastIndexOf("<");
		j = str.lastIndexOf(">");
		if (i != -1 && j != -1) {
			str = str.substring(i + 1, j);
		}
		return str;
	}

	private Properties getJavaMailProperties() {
		Properties p = new Properties();
		if (getProtocol().equals("pop3")) {
			p.put("mail.pop3.host", this.host);
			p.put("mail.pop3.port", this.port);
			p.put("mail.pop3.auth", validate ? "true" : "false");
		} else {
			p.put("mail.imap.host", this.host);
			p.put("mail.imap.port", this.port);
			p.put("mail.imap.auth", validate ? "true" : "false");
		}
		return p;
	}

	public synchronized void setSession(Session session) {
		Assert.notNull(session, "Session must not be null");
		this.session = session;
	}

	/**
	 * Return the JavaMail <code>Session</code>, lazily initializing it if
	 * hasn't been specified explicitly.
	 */
	public synchronized Session getSession() {
		if (this.session == null) {
			// 判断是否需要身份认证
			if (isValidate()) {
				// 如果需要身份认证，则创建一个密码验证器
				MailAuthenticator authenticator = new MailAuthenticator(
						getUsername(), getPassword());
				// 创建session
				session = Session.getInstance(getJavaMailProperties(),
						authenticator);
			} else {
				session = Session.getInstance(getJavaMailProperties());
			}
		}
		return this.session;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}

	public synchronized Store getStore() {
		try {
			if (this.store == null) {
				Store tmpStore = getSession().getStore(getProtocol());
				tmpStore.connect();
				return tmpStore;
			}
		} catch (NoSuchProviderException e) {
			logger.error(e.getMessage(), e);
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return this.store;
	}

	public synchronized void setStore(Store store) {
		this.store = store;
	}

	public synchronized Folder getFolder() {
		try {
			if (this.folder == null) {
				folder = getStore().getFolder("INBOX");
				// 读写
				if (!folder.isOpen())
					folder.open(Folder.READ_WRITE);
			}
			else {
				if (!folder.isOpen())
					folder.open(Folder.READ_WRITE);
			}
			return folder;
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	public synchronized void setFolder(Folder folder) {
		this.folder = folder;
	}

	public String getAttachmentDir() {
		return attachmentDir;
	}

	public void setAttachmentDir(String attachmentDir) {
		this.attachmentDir = attachmentDir;
	}

	public String getEmailDir() {
		return emailDir;
	}

	public void setEmailDir(String emailDir) {
		this.emailDir = emailDir;
	}

	public String getEmailFileSuffix() {
		return emailFileSuffix;
	}

	public void setEmailFileSuffix(String emailFileSuffix) {
		this.emailFileSuffix = emailFileSuffix;
	}
}