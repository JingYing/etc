package com.jd.oa.common.mail.impl;

import java.io.File;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.jd.oa.common.mail.MailAuthenticator;
import com.jd.oa.common.mail.MailSendService;

/**
 * 邮件发送服务
 * 
 * @author zhouhuaqi
 * 
 */
@Service("mailSendService")
public class MailSendServiceImpl implements MailSendService {

	private static final Logger logger = Logger
			.getLogger(MailSendServiceImpl.class);

	/**
	 * 默认邮件的发送者
	 */
	private String defaultFrom;

	/**
	 * velocity引擎
	 */
	private VelocityEngine velocityEngine;

	/**
	 * 邮件内容模板
	 */
	private String defaultTemplate;

	/**
	 * JavaMail发送器
	 */
	private JavaMailSenderImpl mailSender;

	/**
	 * 异步执行器
	 */
	private TaskExecutor taskExecutor;

	@Override
	public void sendSimpleMail(String[] to, String subject, String content) {
		// 建立邮件消息
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(to);
		mailMessage.setFrom(defaultFrom);
		mailMessage.setSubject(subject);
		mailMessage.setText(content);

		addAuthenticator();
		// 发送邮件
		mailSender.send(mailMessage);
		logger.debug("Send simple mail: [Subject: " + mailMessage.getSubject()
				+ ", To: " + mailMessage.getReplyTo() + "]");
	}

	public void sendSimpleMailByAsyn(final String[] to, final String subject,
			final String content) {
		taskExecutor.execute(new Runnable() {
			public void run() {
				sendSimpleMail(to, subject, content);
			}
		});
	}

	public void sendHtmlMail(String[] to, String subject, String content) {
		try {
			// 建立html邮件消息
			MimeMessage mailMessage = mailSender.createMimeMessage();
			MimeMessageHelper messageHelper = new MimeMessageHelper(
					mailMessage, true, "UTF-8");

			// 设置收件人，寄件人、主题
			messageHelper.setTo(to);
			messageHelper.setFrom(defaultFrom);
			messageHelper.setSubject(subject);
			// true 表示启动HTML格式的邮件
			messageHelper.setText(content, true);

			addAuthenticator();
			// 发送邮件
			mailSender.send(mailMessage);
			logger.debug("Send html mail: [Subject: "
					+ mailMessage.getSubject() + ", To: "
					+ mailMessage.getReplyTo() + "]");
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void sendHtmlMailByAsyn(final String[] to, final String subject,
			final String content) {
		taskExecutor.execute(new Runnable() {
			public void run() {
				sendHtmlMail(to, subject, content);
			}
		});
	}

	public void sendAttachedFileMail(String[] to, String subject,
			String content, File[] attachedFiles) {
		try {
			// 建立邮件消息
			MimeMessage mailMessage = mailSender.createMimeMessage();
			// 注意这里的boolean,等于真的时候才能嵌套图片，在构建MimeMessageHelper时候，所给定的值是true表示启用，
			// multipart模式为true时发送附件 可以设置html格式
			MimeMessageHelper messageHelper;

			messageHelper = new MimeMessageHelper(mailMessage, true, "utf-8");

			for (File attachedFile : attachedFiles) {
				FileSystemResource file = new FileSystemResource(attachedFile);
				messageHelper.addAttachment(file.getFilename(), file);
			}

			// 设置收件人，寄件人、主题
			messageHelper.setTo(to);
			messageHelper.setFrom(defaultFrom);
			messageHelper.setSubject(subject);
			// true 表示启动HTML格式的邮件
			messageHelper.setText(content, true);

			addAuthenticator();
			// 发送邮件
			mailSender.send(mailMessage);
			logger.debug("Send attached file mail: [Subject: "
					+ mailMessage.getSubject() + ", To: "
					+ mailMessage.getReplyTo() + "]");
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}

	}

	public void sendAttachedFileMailByAsyn(final String[] to,
			final String subject, final String content,
			final File[] attachedFiles) {
		taskExecutor.execute(new Runnable() {
			public void run() {
				sendAttachedFileMail(to, subject, content, attachedFiles);
			}
		});
	}

	public void sendTemplateMail(String[] to, String subject,
			String templateFilePath, Map<String, Object> model,
			File[] attachedFiles) {
		try {
			// 建立邮件消息
			MimeMessage mailMessage = mailSender.createMimeMessage();
			// 注意这里的boolean,等于真的时候才能嵌套图片，在构建MimeMessageHelper时候，所给定的值是true表示启用，
			// multipart模式为true时发送附件 可以设置html格式
			MimeMessageHelper messageHelper;

			messageHelper = new MimeMessageHelper(mailMessage, true, "utf-8");

			for (File attachedFile : attachedFiles) {
				FileSystemResource file = new FileSystemResource(attachedFile);
				messageHelper.addAttachment(file.getFilename(), file);
			}

			// 设置收件人，寄件人、主题
			messageHelper.setTo(to);
			messageHelper.setFrom(defaultFrom);
			messageHelper.setSubject(subject);

			String text = null;
			text = renderText(model, templateFilePath);
			messageHelper.setText(text, true);

			addAuthenticator();
			// 发送邮件
			mailSender.send(mailMessage);
			logger.debug("Send attached file mail: [Subject: "
					+ mailMessage.getSubject() + ", To: "
					+ mailMessage.getReplyTo() + "]");
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void sendTemplateMailByAsyn(final String[] to, final String subject,
			final String content, final Map<String, Object> model,
			final File[] attachedFiles) {
		taskExecutor.execute(new Runnable() {
			public void run() {
				sendTemplateMail(to, subject, content, model, attachedFiles);
			}
		});
	}

	/**
	 * 使用提供的数据，套入velocity模板，生成最后文本信息。
	 * 
	 * @param model
	 * @param templateFilePath
	 * @return
	 */
	private String renderText(Map<String, Object> model, String templateFilePath) {
		// 防止乱码，需设置为GBK
		return VelocityEngineUtils.mergeTemplateIntoString(getVelocityEngine(),
				templateFilePath != null ? templateFilePath
						: getDefaultTemplate(), "GBK", model);
	}

	private void addAuthenticator() {
		if (mailSender.getSession().getProperties()
				.containsKey("mail.smtp.auth")) {
			if (Boolean.parseBoolean(mailSender.getSession().getProperties()
					.getProperty("mail.smtp.auth"))) {
				// 添加验证
				MailAuthenticator auth = new MailAuthenticator(
						mailSender.getUsername(), mailSender.getPassword());
				Session session = Session.getDefaultInstance(mailSender
						.getSession().getProperties(), auth);
				mailSender.setSession(session);
			}
		}
	}

	public String getDefaultFrom() {
		return defaultFrom;
	}

	public void setDefaultFrom(String defaultFrom) {
		this.defaultFrom = defaultFrom;
	}

	public VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public String getDefaultTemplate() {
		return defaultTemplate;
	}

	public void setDefaultTemplate(String defaultTemplate) {
		this.defaultTemplate = defaultTemplate;
	}

	public JavaMailSenderImpl getMailSender() {
		return mailSender;
	}

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	public TaskExecutor getTaskExecutor() {
		return taskExecutor;
	}

	public void setTaskExecutor(TaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}
}