/*
 * Created on 2011-2-23
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.jd.oa.common.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;

/**
 * @author
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class OtherDbUtil {
	
	private static Logger logger = LoggerFactory.getLogger(OtherDbUtil.class);
	
	private static String driver = "oracle.jdbc.driver.OracleDriver";
	
	private static String strUrl = "jdbc:oracle:thin:@172.17.33.15:1521:erpdb";//正试环境
	private static String usernName = "OA";
	private static String password = "OA_20130829";
	
//	private static String strUrl = "jdbc:oracle:thin:@localhost:1521:orcl";//测试
//	private static String usernName = "ehr";
//	private static String password = "root";

	/**
	 * 获取合部用户数据
	 * @return
	 */
	public static List<SysUser> getAllUserFromEHR(){
		List<SysUser> result = new ArrayList<SysUser>();
		
        
       /* String driver = "com.mysql.jdbc.Driver";
        String strUrl = "jdbc:mysql://192.168.193.52:3306/jdoa?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&autoReconnect=true&failOverReadOnly=false&maxReconnects=10";
        String usernName = "root";
        String password = "123456";*/
        
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(strUrl, usernName, password);
            //创建一个Statement对象
            stmt = conn.createStatement();
            //执行SQL声明
            //创建SQL串，传送到DBMS并执行SQL语句
            StringBuffer buffer = new StringBuffer();
            /*
             * select 
					t.HR_ORG_ORG_ID as orgId,
					t.HR_PERSON_PERSON_ID as user_code,
					t.HR_PERSON_ERPID as user_name,
					t.HR_PERSON_PERSONNAME as real_name,
					t.HR_POSITION_POS_ID as position_code,
					decode(t.POS_POSSERIES_NAME,null,t.POS_JOBLEVEL,t.POS_JOBLEVEL || '-' || t.POS_POSSERIES_NAME) as levelcode,
					t.HR_PERSON_QYYX as email,
					t.HR_PERSON_GRLXDHO as mobile,
					t.HR_PERSON_SEX as sex, 
					t.HR_PERSON_BIRTHDAY as bierthday,
					t.A04_A0401 as age,
					'0' as status,
					t.HR_ORG_RANK as org_full_path,
					t.HR_ORG_FULL_NAME as org_full_name,
					t.HR_POSITION_PARENT_ID	as position_parent_id
				from JDHR.V_PUBLIC t 
				where t.CTH440_CODEID not in ('301', '302');
             * */
            buffer.append(" select ");
            buffer.append(" t.HR_ORG_ORG_ID as orgId, ");//组织ID
            buffer.append(" t.HR_PERSON_PERSON_ID as user_code, "); //唯一标示
            buffer.append(" t.HR_PERSON_ERPID as user_name, "); //ERPID
            buffer.append(" t.HR_PERSON_PERSONNAME as real_name, "); //姓名
            buffer.append(" t.HR_POSITION_POS_ID as position_code, "); //职位ID
            buffer.append(" decode(t.POS_POSSERIES_NAME, ");
            buffer.append(" null, ");
            buffer.append(" t.POS_JOBLEVEL, ");
            buffer.append(" t.POS_JOBLEVEL || '-' || t.POS_POSSERIES_NAME) as levelcode, "); //级别
            buffer.append(" t.HR_PERSON_QYYX as email, "); //企业油箱
            buffer.append(" t.HR_PERSON_GRLXDHO as mobile, "); //手机号
            buffer.append(" t.HR_PERSON_SEX as sex, "); //性别
            buffer.append(" t.HR_PERSON_BIRTHDAY as bierthday, "); //生日
            buffer.append(" t.A04_A0401 as age, "); //年龄
            buffer.append(" t.CTH440_CODEID as status, "); //状态
            buffer.append("t.HR_ORG_RANK as org_full_path,"); //所在部门全路径ID
            buffer.append("t.HR_ORG_FULL_NAME as org_full_name,"); //所在部门全路径名称
            buffer.append("t.HR_POSITION_PARENT_ID	as position_parent_code,"); //上级岗位ID
         // 主体公司代码及名称 CTH148_CODEID,CTH148_CODENAME

            buffer.append("t.CTH148_CODENAME as codename,"); //上级岗位ID
            buffer.append("t.CTH148_CODEID	as codeid"); //上级岗位ID
            
            buffer.append(" from JDHR.V_PUBLIC t ");
//            buffer.append(" from V_PUBLIC t ");//test
//            buffer.append(" where t.CTH440_CODEID not in ('301', '302') ");//101:正常在岗 102:试用期 103:实习期 104:劳务用工 105:临时工 301:辞职 302:辞退 304:其他人员
            
            /*String sql = "SELECT t.ORGANIZATION_ID,t.USER_CODE,t.USER_NAME,t.REAL_NAME,t.POSITION_CODE,t.LEVEL_CODE,t.EMAIL,";
            sql = sql + "t.MOBILE,t.SEX,t.BIRTHDAY,t.AGE,t.STATUS  FROM T_JDOA_SYS_USER t limit 10";*/
       
            rs = stmt.executeQuery(buffer.toString());
            
            //处理每一个数据行，直到不再有数据行
            if(rs != null){
            	
	        	while(rs.next()){
	        		SysUser userBean  = new SysUser();
	        	    //将列值保存到SysUser Model中
	                String orgId = rs.getString(1);
	                String userCode = rs.getString(2);
	                String userName = rs.getString(3);
	                String realName = rs.getString(4);
	                String positionCode = rs.getString(5);
	                String levelCode = rs.getString(6);
	                String email = rs.getString(7);
	                String mobile = rs.getString(8);
	                String sex = rs.getString(9);
	                String temBirthday = rs.getString(10);
	                Date birthday = null;
	                if(temBirthday != null && !"".equals(temBirthday)){
	                	birthday = DateUtils.convertFormatDateString_yyyy_MM_dd(temBirthday);
	                }
	                int age = 0;
	                if(rs.getString(11) != null && !"".equals( Integer.toString(rs.getInt(11)))){
	                	age = Integer.parseInt(rs.getString(11));
	                }
	                String status = rs.getString(12);
	                
	                // add 组织信息
//	                String orgFullPath = rs.getString("org_full_path");
//	                String orgFullName = rs.getString("org_full_name");
	                String positionParentCode = rs.getString("position_parent_code");
	             // 主体公司代码及名称
	                String codeid = rs.getString("codeid");
	                String codename = rs.getString("codename");
	                
	                userBean.setOrganizationId(orgId);
	                userBean.setUserCode(userCode);
	                userBean.setUserName(userName);
	                userBean.setRealName(realName);
	                userBean.setPositionCode(positionCode);
	                userBean.setLevelCode(levelCode);
	                userBean.setEmail(email);
	                userBean.setMobile(mobile);
	                userBean.setSex(sex);
	                userBean.setBirthday(birthday);
	                userBean.setAge(age);
	                userBean.setStatus(status);
	                userBean.setPositionParentCode(positionParentCode);
	                // 主体公司代码及名称
	                userBean.setCodeid(codeid);
	                userBean.setCodename(codename);
	                result.add(userBean);
	        	}
            }
        }catch (Exception e) {
        	logger.error(e.getMessage(), e);
        }finally{
      	  try {
      		  if(rs != null){
      			  rs.close();
    	          if(stmt!=null){
    	        	  stmt.close();
    	          }
    	          if(conn!=null){
    	        	  conn.close();
    	          }
      		  }
      	  }catch (SQLException e) {
      		logger.error(e.getMessage(), e);
  	      }
        }
        //modify by birkhoff
        //modify cause:oa用户数据中存在两条相同的数据(usercode 相同).咨询人力，多条相同的数据，有两种情况，一种是最高学历有区别，另一种完全相同，只取第一条
        //解决思路 ： 以userCode为key,进行重复性过滤
        
//        return result;
        return removeDuplicateSysUser(result);
    }
	/**
	 * 去除重复的用户信息
	 * @param result
	 * @return
	 */
	private static List<SysUser> removeDuplicateSysUser(List<SysUser> result){
		List<SysUser> sysUsers = new ArrayList<SysUser>();
		Map<String ,SysUser> mapSysUser = new HashMap<String,SysUser>();
		if(result != null && result.size() > 0){
			for(int i = 0 ; i < result.size() ; i++){
				SysUser sysUser = result.get(i);
				if(sysUser != null && sysUser.getUserCode() != null && sysUser.getUserCode().trim().length() > 0){
					mapSysUser.put(sysUser.getUserCode(), sysUser);
				}
			}
		}
		Iterator iterator = mapSysUser.entrySet().iterator(); 
		while(iterator.hasNext()) { 
		    Entry entry = (Entry)iterator.next(); 
		    String key = entry.getKey().toString();
		    SysUser sysUser = mapSysUser.get(key);
		    sysUsers.add(sysUser);
		} 
		return sysUsers;
	}
	/**
	 * 获取合部组织数据
	 * @return
	 */
	public static List<SysOrganization> getOrganizationFromEHR(){
		List<SysOrganization> result = new ArrayList<SysOrganization>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(strUrl, usernName, password);
            stmt = conn.createStatement();
            StringBuffer buffer = new StringBuffer();
            buffer.append(" select *");
            buffer.append(" from JDHR.V_PUBLIC_ORG t");
//            buffer.append(" from V_PUBLIC_ORG t ");
            buffer.append(" order by t.hr_org_org_id");
            
            rs = stmt.executeQuery(buffer.toString());
            while(rs.next()){
            	SysOrganization org = new SysOrganization();
            	org.setOrganizationCode(rs.getString("HR_ORG_ORG_ID"));
            	org.setOrganizationName(rs.getString("HR_ORG_NAME"));
            	org.setOrganizationFullname(rs.getString("HR_ORG_FULL_NAME"));
            	org.setOrganizationFullPath(rs.getString("HR_ORG_RANK"));
            	org.setParentId(rs.getString("HR_ORG_PARENT_ID"));
            	org.setStatus("0");
            	result.add(org);
            }
        }catch (Exception e) {
        	logger.error(e.getMessage(), e);
        }finally{
      	  try {
      		  if(rs != null){
      			  rs.close();
    	          if(stmt!=null){
    	        	  stmt.close();
    	          }
    	          if(conn!=null){
    	        	  conn.close();
    	          }
      		  }
      	  }catch (SQLException e) {
      		logger.error(e.getMessage(), e);
  	      }
        }
      
        return result;
    }
	/**
	 * 获取ehr数据的岗位信息数据
	 * @return
	 */
	public static List<SysPosition> getPositionFromEHR(){
		List<SysPosition> result = new ArrayList<SysPosition>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(strUrl, usernName, password);
            stmt = conn.createStatement();
            StringBuffer buffer = new StringBuffer();
            //where fromtime <= sysdate and totime >= sysdate
            buffer.append(" select  *  ");
            buffer.append("  from JDHR.HR_POSITION t");
//            buffer.append("  from HR_POSITION t ");
            buffer.append("  where t.fromtime <= sysdate and t.totime >= sysdate");
            buffer.append("  order by t.OID");
            
            rs = stmt.executeQuery(buffer.toString());
            while(rs.next()){
            	SysPosition position = new SysPosition();
            	position.setPositionCode(rs.getString("POS_ID"));
            	position.setPositionName(rs.getString("NAME"));
            	position.setOrganizationId(rs.getString("ORG_ID"));
            	position.setParentId(rs.getString("PARENT_ID"));
            	position.setStatus("0");
            	result.add(position);
            }
        }catch (Exception e) {
        	logger.error(e.getMessage(), e);
        }finally{
      	  try {
      		  if(rs != null){
      			  rs.close();
    	          if(stmt!=null){
    	        	  stmt.close();
    	          }
    	          if(conn!=null){
    	        	  conn.close();
    	          }
      		  }
      	  }catch (SQLException e) {
      		logger.error(e.getMessage(), e);
  	      }
        }
      
        return result;
	}
	
}
