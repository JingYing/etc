package com.jd.oa.common.mq;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.activemq.consumer.TextMessageListener;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.utils.JsonUtils;

public class MessageHandler extends TextMessageListener {
	private static final Log logger = LogFactory.getLog(MessageHandler.class);

	@Autowired
	private ProcessTaskService processTaskService;

	@Override
	protected void onMessage(String textMessage) throws Exception {
		SmsContent smsContent = JsonUtils.fromJson(textMessage, SmsContent.class);
		logger.info("短信回复：" + textMessage);
		processTaskService.parseSmsTask(smsContent.getMsgContent(), smsContent.getMobileNum());
	}

	public class SmsContent {

		/**
		 * 电话号码
		 */
		private String mobileNum;
		/**
		 * 短信内容
		 */
		private String msgContent;
		/**
		 * arrivedTime
		 */
		private Date arrivedTime;

		public String getMobileNum() {
			return mobileNum;
		}

		public void setMobileNum(String mobileNum) {
			this.mobileNum = mobileNum;
		}

		public String getMsgContent() {
			return msgContent;
		}

		public void setMsgContent(String msgContent) {
			this.msgContent = msgContent;
		}

		public Date getArrivedTime() {
			return arrivedTime;
		}

		public void setArrivedTime(Date arrivedTime) {
			this.arrivedTime = arrivedTime;
		}
	}
}
