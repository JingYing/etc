package com.jd.oa.common.webservice;

import org.apache.ws.security.WSPasswordCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;
import java.util.Map;

/**
 * Created by wangdongxing on 14-3-13.
 * Email : wangdongxing@jd.com
 */
public class ServerPasswordCallback implements CallbackHandler{
	private Map<String, String> passwords;

	public ServerPasswordCallback(Map<String,String> passwordMap) {
		this.passwords = passwordMap;
	}

	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		WSPasswordCallback pc = (WSPasswordCallback)callbacks[0];
		if(passwords.containsKey(pc.getIdentifier())){
			String realPassworld = passwords.get(pc.getIdentifier());
			pc.setPassword(realPassworld);
		}else{
			throw  new SecurityException("无效的用户名！");
		}
	}
}
