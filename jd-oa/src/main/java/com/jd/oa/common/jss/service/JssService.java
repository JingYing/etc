package com.jd.oa.common.jss.service;

import com.jcloud.jss.JingdongStorageService;
import com.jcloud.jss.domain.Bucket;
import com.jcloud.jss.domain.ObjectListing;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

/**
 * Jss云存储接口
 * User: zhaoming
 * Date: 13-8-28
 * Time: 下午4:49
 * To change this template use File | Settings | File Templates.
 */
public interface JssService {

    /**
     * 获取Jss云存储服务类
     * @return
     */
    public JingdongStorageService getJssService();

    /**
     * 创建桶
     * @param bucketName 桶名
     */
    public void createBucket(String bucketName);

    /**
     * 获取全部桶
     * @return 桶列表
     */
    public List<Bucket> getBuckets() ;

    /**
     * 删除桶
     * @param bucketName 桶名
     */
    public void deleteBucket(String bucketName);

    /**
     * 以文本形式上传到云端
     * @param bucketName 桶名
     * @param objKey 对象的KEY
     * @param content 文本内容
     */
    public void uploadFile(String bucketName, String objKey, String content);

    /**
     * 以流形式上传到云端
     * @param bucketName 桶名
     * @param objKey 对象的KEY
     * @param inputStream 文本内容
     */
    public void uploadFile(String bucketName, String objKey, InputStream inputStream);

    /**
     * 下载文件
     * @param bucketName 桶名
     * @param objKey 对象的KEY
     */
    public InputStream downloadFile(String bucketName, String objKey);

    /**
     * 获取文件对象
     * @param bucketName 桶名
     * @param objKey 对象的KEY
     */
    public File getFile(String bucketName, String objKey);

    /**
     * 查找所有对象(默认返回前1000个ObjectSummary)
     * @param bucketName 桶名
     * @return 对象列表
     */
    public ObjectListing findObjects(String bucketName);

    /**
     * 判断KEY是否存在
     * @param bucketName 桶名
     * @param key 对象的KEY
     */
    public boolean exist(String bucketName,String key);

    /**
     * 删除对象
     * @param bucketName 桶名
     * @param key 对象的KEY
     */
    public void deleteObject(String bucketName,String key);

	/**
	 * 获取jss文件的外部链接
	 * @param bucketName 桶名
	 * @param key 对象的KEY
	 * @return 对象外链URI
	 */
	public URI getObjectUri(String bucketName,String key);
}
