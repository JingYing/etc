package com.jd.oa.common.paf.model;

/**
 * 流程实例Domain
 * User: zhaoming
 * Date: 13-7-18
 * Time: 下午6:14
 */
public class ProcessInstance {

    /**
     * 流程实例ID
     */
    private String id;
    /**
     * 流程实例创建时间
     */
    private String startTime;
    /**
     * 业务主键ID
     */
    private String businessKey;
    /**
     * 流程定义ID
     */
    private String processDefinitionId;
    /**
     * 流程发起者
     */
    private String startUserId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }
}
