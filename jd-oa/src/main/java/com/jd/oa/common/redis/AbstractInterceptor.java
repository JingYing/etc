package com.jd.oa.common.redis;

import com.jd.oa.common.redis.annotation.*;
import net.sf.cglib.proxy.MethodProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Stack;

public abstract class AbstractInterceptor implements CacheInterceptor {

    private static final ThreadLocal<Stack<Boolean>> firstInvoke = new ThreadLocal<Stack<Boolean>>() {
        @Override
        protected Stack<Boolean> initialValue() {
            return new Stack<Boolean>();
        }
    };
    Logger logger = LoggerFactory.getLogger(AbstractInterceptor.class);
    public Object doCache(Cache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        CacheKey key = annotation.key();
        String cacheKey = CacheUtils.parseCacheKey(key, args);
        boolean cacheNull = annotation.cacheNull();
        int expire = CacheUtils.getExpire(annotation.expireTime(), annotation.expire());
        return simpleCache(cacheKey, cacheNull, expire, obj, annotation.type(), method, args, proxy);
    }

    public Object doListCache(ListedCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        CacheKey key = annotation.key();
        String cacheKey = CacheUtils.parseCacheKey(key, args);
        Integer offset = (Integer) getParameterValue(annotation.offsetIndex(), args);
        Integer limit = (Integer) getParameterValue(annotation.limitIndex(), args);
        List<?> result = null;
        try {
            logger.debug(String.format("try load list cache %s:%s,%s", cacheKey, offset, limit));
            result = loadListFromCache(cacheKey, offset, limit, Object.class, annotation.type());
            logger.debug("local list cache:" + result.size());
        } catch (Exception e) {
            logger.info("load list cache error:" + e.getCause());
        }
        if (result != null && result.size() == limit) {
            return result;
        } else {
            if (result != null) {
                offset = offset + result.size();
                limit = limit - result.size();
            }
            args[annotation.offsetIndex()] = offset;
            args[annotation.limitIndex()] = limit;
            List dbResult = (List) proxy.invokeSuper(obj, args);
            if (result == null) {
                return dbResult;
            }
            result.addAll(dbResult);
        }
        return result;
    }

    public Object doSimpleCache(SimpleCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        String key = annotation.key();
        boolean cacheNull = annotation.cacheNull();
        int expire = CacheUtils.getExpire(annotation.expireTime(), annotation.expire());
        return simpleCache(key, cacheNull, expire, obj, annotation.type(), method, args, proxy);
    }

    public void removeCache(RemoveCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        CacheKey[] keys = annotation.key();
        for (CacheKey key : keys) {
            String cacheKey = CacheUtils.parseCacheKey(key, args);
            int expire = CacheUtils.getExpire(annotation.expireTime(), annotation.expire());
            if (annotation.markAsNull()) {
                writeToCache(cacheKey, new NullCachedObject(), expire, false, annotation.type());
            } else {
                removeFromCache(cacheKey, annotation.type());
            }
        }
    }

    public Object writeCache(WriteCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        CacheKey key = annotation.key();
        String cacheKey = CacheUtils.parseCacheKey(key, args);
        int expire = CacheUtils.getExpire(annotation.expireTime(), annotation.expire());
        try {
            if (annotation.writeParameter()) {
                Object value = getParameterValue(annotation.parameterIndex(), args);
                writeToCache(cacheKey, value, expire, annotation.cacheNull(), annotation.type());
            }
        } catch (Exception e) {
            logger.info("write listed parm cache error:" + e.getCause());
        }
        Object returnVal = proxy.invokeSuper(obj, args);
        try {
            if (annotation.writeReturn()) {
                writeToCache(cacheKey, returnVal, expire, annotation.cacheNull(), annotation.type());
            }
        } catch (Exception e) {
            logger.info("write listed return cache error:" + e.getCause());
        }
        return returnVal;
    }

    public Object writeListCache(WriteListCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        CacheKey key = annotation.key();
        String cacheKey = CacheUtils.parseCacheKey(key, args);
        try {
            if (annotation.writeParameter()) {
                Object value = getParameterValue(annotation.parameterIndex(), args);
                writeListToCache(cacheKey, value, annotation.append(), annotation.max(), annotation.type());
            }
        } catch (Exception e) {
            logger.info("write parm cache error:" + e.getCause());
        }
        Object returnVal = proxy.invokeSuper(obj, args);
        try {
            if (annotation.writeReturn()) {
                writeListToCache(cacheKey, returnVal, annotation.append(), annotation.max(), annotation.type());
            }
        } catch (Exception e) {
            logger.info("write return cache error:" + e.getCause());
        }
        return returnVal;
    }

    public abstract void writeListToCache(String cacheKey, Object value, boolean append, int max, String cacheType);

    public abstract void writeToCache(String cacheKey, Object value, int expire, boolean cacheNull, String cacheType);

    public abstract List<?> loadListFromCache(String key, int offset, int limit, Class<?> c, String cacheType);

    protected Object getParameterValue(int index, Object[] parameters) {
        return parameters[index];
    }

    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {

        boolean pushed;
        pushed = !(firstInvoke.get().isEmpty() || firstInvoke.get().peek());
        firstInvoke.get().push(pushed);
        try {
            Annotation[] annotations = method.getAnnotations();
            RemoveCache delayedAnnotation = null;
            CacheEvict delayAnnotation = null;
            Object returned = null;
            boolean methodInvoked = false;
            for (Annotation annotation : annotations) {
                if (annotation instanceof CachePut) {
                    CachePut cachePut = (CachePut) annotation;
                    //TODO:
                    returned = doCachePut(cachePut, obj, method, args, proxy);
                    methodInvoked = true;
                } else if (annotation instanceof CacheEvict) {
                    //TODO:
                    if (((CacheEvict) annotation).beforeInvocation()) {
                        evictCache((CacheEvict) annotation, obj, method, args, proxy);
                    } else {
                        delayAnnotation = (CacheEvict) annotation;
                    }
                } else if (annotation instanceof SimpleCache) {
                    SimpleCache a = (SimpleCache) annotation;
                    returned = doSimpleCache(a, obj, method, args, proxy);
                    methodInvoked = true;
                } else if (annotation instanceof Cache) {
                    Cache a = (Cache) annotation;
                    returned = doCache(a, obj, method, args, proxy);
                    methodInvoked = true;
                } /*else if (annotation instanceof ListedCache) {
                    returned = doListCache((ListedCache) annotation, obj, method, args, proxy);
                    methodInvoked = true;
                } else if (annotation instanceof WriteListCache) {
                    returned = writeListCache((WriteListCache) annotation, obj, method, args, proxy);
                    methodInvoked = true;
                } else if (annotation instanceof WriteCache) {
                    returned = writeCache((WriteCache) annotation, obj, method, args, proxy);
                    methodInvoked = true;
                }*/ else if (annotation instanceof RemoveCache) {
                    if (((RemoveCache) annotation).invokeBefore()) {
                        removeCache((RemoveCache) annotation, obj, method, args, proxy);
                    } else {
                        delayedAnnotation = (RemoveCache) annotation;
                    }
                }
            }
            if (!methodInvoked)
                returned = proxy.invokeSuper(obj, args);
            if (delayedAnnotation != null) {
                removeCache(delayedAnnotation, obj, method, args, proxy);
            }
            if (delayAnnotation != null) {
                evictCache(delayAnnotation, obj, method, args, proxy);
            }
            return returned;
        } finally {
            firstInvoke.get().pop();
        }


    }

    public abstract Object loadFromCache(String key, Class<?> c, String cacheType);


//    private Object invokeSuper(Object obj, Object[] args, MethodProxy proxy) throws Throwable {
//        return proxy.invokeSuper(obj, args);
//    }

    public abstract void removeFromCache(String key, String cacheType);

    protected void evictCache(CacheEvict annotation, Object obj, Method method, Object[] args, MethodProxy proxy) {
        String cacheKey = annotation.key();
//        String cacheKey = CacheUtils.parseCacheKey(key, args);
        int expire = CacheUtils.getExpire(annotation.expireTime(), annotation.expire());
        if (annotation.markAsNull()) {
            writeToCache(cacheKey, new NullCachedObject(), expire, false, annotation.type());
        } else {
            removeFromCache(cacheKey, annotation.type());
        }
    }

    private Object doCacheEvict(CacheEvict cacheEvict, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        return null;
    }

    private Object doCachePut(CachePut cachePut, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        /*String key = cachePut.key();
        boolean cacheNull = cachePut.cacheNull();
        int expire = CacheUtils.getExpire(cachePut.expireTime(), cachePut.expire());
        return simpleCache(key, cacheNull, expire, obj, cachePut.type(), method, args, proxy);*/
        CacheKey key = cachePut.key();
        String cacheKey = CacheUtils.parseCacheKey(key, args);
        boolean cacheNull = cachePut.cacheNull();
        int expire = CacheUtils.getExpire(cachePut.expireTime(), cachePut.expire());
        return simpleCache(cacheKey, cacheNull, expire, obj, cachePut.type(), method, args, proxy);
    }

    private Object simpleCache(String key, boolean cacheNull, int expire, Object obj, String cacheType, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        Object cached = null;
        try {
            logger.debug("try load  [{}] from cache", key);
            cached = loadFromCache(key, method.getReturnType(), cacheType);
        } catch (Exception e) {
            logger.info("load from cache error:" + e.getCause());
        }
        if (cached == null) {
            logger.debug("load cache [{}] fail,invoke super", key);
            cached = proxy.invokeSuper(obj, args);
            try {
                Object cacheObj = cached;
                if (cached == null) {
                    if (cacheNull) {
                        cacheObj = new NullCachedObject();
                    }
                }
                logger.debug("write cache [{}]", key);
                writeToCache(key, cacheObj, expire, false, cacheType);
            } catch (Exception e) {
                logger.info("set to cache error:" + e.getCause());
            }
        }
        if (cached instanceof NullCachedObject) {
            cached = null;
            logger.debug("cached [{}] is null", key);
        }

        return cached;
    }
}
