package com.jd.oa.common.paf.service;

import org.apache.cxf.endpoint.Client;

/**
 * PAF客户端服务接口
 * User: zhaoming
 * Date: 13-7-22
 * Time: 上午11:58
 * To change this template use File | Settings | File Templates.
 */
public interface PafClientService {

    /**
     * 获取Paf客户端
     * @return
     */
    public Client getClient();
}
