package com.jd.oa.common.paf.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 任务实例Domain
 * User: zhaoming
 * Date: 13-7-19
 * Time: 下午3:00
 */
public class TaskInstance {

    /**
     * 任务实例ID
     */
    private String id;

    /**
     * 任务英文名称
     */
    private String taskDefinitionKey;

    /**
     * 任务中文名称
     */
    private String name;

    /**
     * 优先级
     */
    private String priority;

    /**
     * 所属流程实例ID
     */
    private String processInstanceId;

    /**
     * 处理人
     */
    private String assignee;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 启动时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;
    
    /**
     * 子属性
     */
    private List<FormProperties> formProperties;
    

    //Add By WXJ At 2013-12-23
    String description;

    String dueDate;

    String executionId;

    String owner;

    String parentTaskId;

    String processDefinitionId;

    String formResourceKey;

    List<Object> subTaskList = new ArrayList<Object>();

    List<IdentityLinkResponse> identityLinkList = new ArrayList<IdentityLinkResponse>();

    //getter/setter
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getAssignee() {
        return assignee;
    }
		
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

	public List<FormProperties> getFormProperties() {
		return formProperties;
	}

	public void setFormProperties(List<FormProperties> formProperties) {
		this.formProperties = formProperties;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getFormResourceKey() {
		return formResourceKey;
	}

	public void setFormResourceKey(String formResourceKey) {
		this.formResourceKey = formResourceKey;
	}

	public List<Object> getSubTaskList() {
		return subTaskList;
	}

	public void setSubTaskList(List<Object> subTaskList) {
		this.subTaskList = subTaskList;
	}

	public List<IdentityLinkResponse> getIdentityLinkList() {
		return identityLinkList;
	}

	public void setIdentityLinkList(List<IdentityLinkResponse> identityLinkList) {
		this.identityLinkList = identityLinkList;
	}

}
