package com.jd.oa.common.paf.service;

import com.jd.oa.common.paf.model.PafResult;
import com.jd.oa.common.paf.model.ProcessInstance;
import com.jd.oa.common.paf.model.TaskInstance;
import com.jd.oa.common.paf.model.processinstance.ProcessInstanceDomain;

import java.util.List;
import java.util.Map;

/**
 * PAF工作流平台服务接口
 * User: zhaoming
 * Date: 13-7-18
 * Time: 下午5:18
 */
public interface PafService {

    /**
     * 创建流程实例
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefKey 流程定义Key
     * @param businessKey 业务ID
     * @return result
     */
    public PafResult<ProcessInstance> createProcessInstance(String loginUser, String processDefKey, String businessKey);
    
    /**
     * 推进第一个人工任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processInstanceId 流程实例
     * @param variables 流程变量
     * @return result
     */
    public PafResult<ProcessInstance> submitHumanProcessInstance(String loginUser, String processInstanceId, Map<String, Object> variables);
    
    /**
     * 创建流程实例并推进第一个人工任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefKey 流程定义Key
     * @param businessKey 业务ID
     * @param variables 流程变量
     * @return result
     */
    public PafResult<ProcessInstance> submitHumanProcessInstance(String loginUser, String processDefKey, String businessKey, Map<String, Object> variables);

    /**
     * 提交任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 需要完成的任务ID
     * @param variables  流程变量
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeUserTask(String loginUser, String taskId, Map<String, Object> variables);

    /**
     * 回退到上一个人工任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 任务实例ID
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeToPreviousUserTask(String loginUser, String taskId, Map<String, Object> variables);

    /**
     * 回退到第一个人工任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 任务实例ID
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeToFirstUserTask(String loginUser, String taskId, Map<String, Object> variables);


    /**
     * 加签
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param parentTaskId 任务ID
     * @param users 办理人列表
     * @return 是否正确完成
     */
    public PafResult<Boolean> createSubTask(String loginUser, String parentTaskId, List<String> users);


    /**
     * 查询某一用户的待办任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 任务列表
     */
    public PafResult<List<TaskInstance>> taskQuery(String loginUser, String processDefinitionKey, Map<String, Object> conditions) ;
    
    /**
     * 查询某一用户的待办任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return  total和pafresult
     */
    public Map<String, Object> taskQueryWithTotal(String loginUser, String processDefinitionKey,  Map<String, Object> conditions);

    /**
     * 历史任务查询
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 任务列表
     */
    public PafResult<List<TaskInstance>> historyTaskQuery(String loginUser, String processDefinitionKey, Map<String, Object> conditions) ;

    /**
     * 流程实例查询
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 流程实例列表
     */
    public PafResult<List<ProcessInstance>> processInstQuery(String loginUser, String processDefinitionKey, Map<String, Object> conditions) ;

    /**
     * 记录审批意见
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId  审批意见所属任务ID
     * @param processInstId  任务所属流程实例ID
     * @param message  审批意见的内容，业务中审批通过与否可以记录在这里
     */
    public PafResult<Boolean> addTaskComment(String loginUser, String taskId, String processInstId, String message);

    /**
     * 重新分配任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId 任务ID
     * @param origAssignee 当前的任务分配用户
     * @param newAssignee 新的任务分配用户，必须为小写
     * @param assigneeType 用户类型，目前只支持“user”
     * @return 重新分配结果
     */
    public PafResult<Boolean> reassignTask(String loginUser, String taskId, String origAssignee, String newAssignee, String assigneeType);

    /**
     * 流程实例详细信息查询
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstId 流程实例ID
     * @return 流程实例详细信息
     */
    public PafResult<ProcessInstanceDomain> getProcessInstanceDetail(String loginUser, String processInstId);

    /**
     * 取消流程实例
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstanceId 流程实例ID
     * @return 是否成功
     */
    public PafResult<Boolean> cancelProcessInstance(String loginUser, String processInstanceId);

    /**
     * 获取任务详细信息(暂未开放)
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId 任务ID
     * @return 任务详细信息
     */
    public PafResult<TaskInstance> getTaskDetail(String loginUser, String taskId);

    /**
     * 获取流程实例图片
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstanceId 流程实例ID
     * @return
     */
    public PafResult<byte[]> processInstanceFlowPictureQuery(String loginUser, String processInstanceId);

    /***************************************************************************************************
     *  OA专用
     ***************************************************************************************************/

    /**
     * 查询当前租户下所有的待办任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param conditions 流程变量集合
     * @return 待办任务列表
     */
    public PafResult<List<TaskInstance>> taskQueryAdvanced(String loginUser,  Map<String, Object> conditions);



    /**
     * 查询当前租户下所有的待办任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param conditions 流程变量集合
     * @return total和pafresult
     */
    public Map<String,Object> taskQueryAdvancedWithTotal(String loginUser,  Map<String, Object> conditions);
    
    
    /**
     * 
     * @desc 根据多个流程实例返回当前任务状态
     * @author WXJ
     * @date 2013-11-19 下午02:43:05
     *
     * @param loginUser
     * @param processInstances
     * @return
     */
    public PafResult<List<TaskInstance>> getProcessCurrentUserTasks(String loginUser, List<String> processInstances);
 


}
