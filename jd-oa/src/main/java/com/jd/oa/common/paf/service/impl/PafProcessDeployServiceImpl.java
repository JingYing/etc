package com.jd.oa.common.paf.service.impl;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.paf.service.PafProcessDeployService;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程发布服务实现类
 * User: zhaoming
 * Date: 13-9-25
 * Time: 下午6:21
 * To change this template use File | Settings | File Templates.
 */
public class PafProcessDeployServiceImpl implements PafProcessDeployService {
    private static final Logger logger = Logger.getLogger(PafProcessDeployServiceImpl.class);
    private String url;

    /**
     * 发布流程图
     * @param file 流程文件
     */
    public boolean deploy(File file){
        boolean flag = false;
        InputStream inputStream = null;
        try {
            List providers = new ArrayList();
            providers.add(new JacksonJsonProvider());
            WebClient sweWebClient = WebClient.create(url, providers);
            sweWebClient.type("multipart/form-data");
            Response response = sweWebClient.post(file);

            inputStream = (InputStream) response.getEntity();
            StringBuffer buffer = new StringBuffer();
            byte[] bytes = new byte[inputStream.available()];
            while(inputStream.read(bytes) != -1){
                buffer.append(new String(bytes,"utf-8"));
            }
            if(buffer.toString()!=null && !buffer.toString().equals("") && buffer.toString().equals("true")){
                flag = true;
            }else{
            	throw new BusinessException("流程发布失败:"+buffer.toString());
            }
        } catch (Exception e) {
        	throw new BusinessException("流程发布失败:"+e.toString());
        } finally {
        	try{
        		if(inputStream!=null){
        			inputStream.close();
        		}
        	} catch(IOException e) {
        	}
        }
        return flag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
