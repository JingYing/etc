package com.jd.oa.common.constant;

import com.jd.oa.common.utils.PlatformProperties;

/**
 * 系统常量类
 * User: zhaoming
 * Date: 13-9-4
 * Time: 下午7:15
 * To change this template use File | Settings | File Templates.
 */
public class SystemConstant {
	
    /**
     * 桶名
     */
    public static final String BUCKET = PlatformProperties.getProperty("jd-oa.jss.bucket");

    /**
     * 数据库名,用在表单创建业务表时判断是否存在相同的表名
     */
    public static final String TABLESCHEMA = "jdoa";

    /** 表单创建类型 */
	public static final String FORM_TYPE = "formType";
	/** 表单创建类型-标准模式 */
	public static final String FORM_TYPE_SM = "standardModule";
	/** 表单创建类型-高级模式 */
	public static final String FORM_TYPE_AM = "advancedModule";
	/** 表单创建类型-复制模式 */
	public static final String FORM_TYPE_CM = "copyModule";
	
	public static final String FORM_TYPE_M = "M";
	
	public static final String FORM_TYPE_S = "S";
	
	/** 业务表创建类型 */
	public static final String TABLE_CREATE_TYPE_0 = "0";
	public static final String TABLE_CREATE_TYPE_1 = "1";
	
	/** 数据库类型 */
	public static final String T_JDOA_ = "T_JDOA_";
	
	/** 流程状态*/
	public static final String PROCESS_STATUS_0 = "0";//草稿
	public static final String PROCESS_STATUS_1 = "1";//审批中
	public static final String PROCESS_STATUS_2 = "2";//拒绝
	public static final String PROCESS_STATUS_3 = "3";//驳回
	public static final String PROCESS_STATUS_4 = "4";//取消
    public static final String PROCESS_STATUS_5 = "5";//归档
    
    public static final String PROCESS_STATUS_6 = "6";//已申请但上级经理还未审批的状态，逻辑状态
	public static final String PROCESS_STATUS_7 = "7";//他人草稿查看（不能编辑）

	
	/** 多组织机构分类 */
	public static final String ORGNAIZATION_TYPE_HR = "1";//人力资源
	public static final String ORGNAIZATION_TYPE_FI = "2";//财务
	public static final String ORGNAIZATION_TYPE_MO = "3";//采销
	
	/** 代办任务类型 */
	public static final String TASK_KUBU_OWNER = "owner";//本人的
	public static final String TASK_KUBU_PROXY = "proxy";//代理的
	public static final String TASK_KUBU_ADDSIGNER = "addsigner";//转发的
	public static final String TASK_KUBU_SUPERVISE = "supervise";//督办的
	/** 流程优先级*/
	public static final String PROCESS_PRIORITY_0 = "0"; //标准申请
	public static final String PROCESS_PRIORITY_1 = "1"; //紧急申请
	public static final String PROCESS_PRIORITY_2 = "2"; //特急申请
	
	/** 流程完成状态 流程审批任务历史表中的INSTANCE_ISCOMPLETE */
	public static final String PROCESS_COMPLETE_0 = "0";//未完成
	public static final String PROCESS_COMPLETE_1 = "1";//已完成
	
    /**任务操作result*/
    public static final String TASK_STATUS_0 = "0";//提交申请
    public static final String TASK_STATUS_1 = "1";//批准
    public static final String TASK_STATUS_2 = "2";//拒绝
    public static final String TASK_STATUS_3 = "3";//驳回
    public static final String TASK_STATUS_4 = "4";//重新申请
    public static final String TASK_STATUS_NAME_4 = "重新申请";
    public static final String TASK_STATUS_5 = "5";//转发确认
    public static final String TASK_STATUS_6 = "6";//转发

	public static final String PROCESS_DEFINITION_OA="0";
	public static final String PROCESS_DEFINITION_LINK="1";
	public static final String PROCESS_DEFINITION_BIZ="2";

	public static final String TASK_OPERATE_APPROVE="1"; //批转操作
	public static final String TASK_OPERATE_REJECT="-1"; //驳回和退回操作
	
	public static final String JDOA_APPLY_FORM_JSS_KEY_FIX = "JDOAAPPLYFORMJSSKEYFIX";//流程定义整个申请表单JSSkey后缀

}
