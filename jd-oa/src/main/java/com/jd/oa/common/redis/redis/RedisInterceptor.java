package com.jd.oa.common.redis.redis;

import com.jd.cachecloud.driver.jedis.JedisOperation;
import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.oa.common.redis.AbstractInterceptor;
import com.jd.oa.common.redis.NullCachedObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class RedisInterceptor extends AbstractInterceptor {
    Logger logger = LoggerFactory.getLogger(RedisInterceptor.class);
    /*private RedisCommands redisCommands;*/
    private ShardedXCommands redisCommands;
    private int defaultExpire = 43200;//默认十二小时有效期

    public RedisInterceptor(ShardedXCommands redisCommands) {
        this.redisCommands = redisCommands;
    }

    public ShardedXCommands getRedisCommands() {
        return redisCommands;
    }

    public void setRedisCommands(ShardedXCommands redisCommands) {
        this.redisCommands = redisCommands;
    }

    protected Object getParameterValue(int index, Object[] parameters) {

        return parameters[index];
    }

    @Override
    public List<?> loadListFromCache(String key, int offset, int limit, Class<?> c, String cacheType) {
        return redisCommands.lrange(key, offset, offset + limit);
    }

    @Override
    public void writeToCache(String cacheKey, Object value, int expire, boolean cacheNull, String cacheType) {
        if (value != null) {
            write(cacheKey, value, expire);
        } else {
            if (cacheNull) {
                write(cacheKey, new NullCachedObject(), expire);
            }
        }
    }

    private void write(String key, Object value, int expire) {
        try {
            redisCommands.setex(RedisCacheSerializer.serialize(key), expire,
                    RedisCacheSerializer.serialize(value));
            logger.debug("add [" + key + "] to cache");
        } catch (IOException e) {
            logger.error("调用缓存服务器put操作出错:", e);
        } catch (IllegalStateException e) {
            logger.error("put key error", e);
        }
    }

    @Override
    public void writeListToCache(String cacheKey, Object value, boolean append, int max, String cacheType) {
        write(cacheKey, value, defaultExpire);
    }

    @Override
    public Object loadFromCache(String key, Class<?> c, String cacheType) {
        byte[] buf;
        Object returnValue = null;
        try {
            buf = redisCommands.get(RedisCacheSerializer.serialize(key));
            if (buf == null) {
                logger.warn("没有查找到key:[" + key + "]");
                return buf;
            }
            returnValue = RedisCacheSerializer.deSerialize(buf);
        } catch (Exception e) {
            logger.error("调用缓存服务器get操作出错:", e);
        }
        return returnValue;
    }

    @Override
    public void removeFromCache(final String key, String cacheType) {
        if (key.indexOf("*") < 0)
            redisCommands.del(key);
        else {
            final Set<byte[]> bytesKey = new HashSet<byte[]>();
            redisCommands.execute(new JedisOperation<Void>() {
                @Override
                public Void call(Jedis jedis) throws Throwable {
                    bytesKey.addAll(jedis.keys("*".getBytes()));
                    return null;
                }
            });
            for (byte[] s : bytesKey) {
                try {
                    String allKeys = (String) RedisCacheSerializer.deSerialize(s);
                   String _tempKey= key.replace("*", "");
                    if (allKeys.indexOf(_tempKey) >=0){
                        redisCommands.del(s);
                        logger.debug("remove [" + allKeys + "] from cache");
                    }
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }

}
