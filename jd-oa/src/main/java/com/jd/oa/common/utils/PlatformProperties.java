package com.jd.oa.common.utils;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jd.ump.profiler.proxy.Profiler;

public class PlatformProperties {
	private static final Logger logger = Logger.getLogger(PlatformProperties.class);
	private static ResourceBundle platform6 = null;

	
	private PlatformProperties(String path) {
		try {
			//参数key为该监控点在统一监控平台系统中注册的唯一标识符
			Profiler.InitHeartBeats("wfp_running");   //系统存活监控
			Profiler.registerJVMInfo("wfp_jvm");      //JVM监控
			
			loadProperties(path);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * 从给定的配置文件中加载参数配置
	 * @param file
	 * @throws Exception
	 */
	private static void loadProperties(String file) throws Exception {
		platform6 = ResourceBundle.getBundle(file, Locale.getDefault());
		printResouceBundle(platform6);
		
	}

	/**
	 * 输出资源参数文件的内容
	 * 
	 * @param rb
	 */
	private static void printResouceBundle(ResourceBundle rb) {
		logger.info("开始加载系统参数信息");
		if (rb != null) {
			Enumeration<String> keys = rb.getKeys();
			while (keys.hasMoreElements()) {
				String k = keys.nextElement();
				logger.info(k + " = " + rb.getString(k));
			}
		}
		logger.info("完成加载系统参数信息");
	}

	/**
	 * 从配置文件取信息
	 * 
	 * @param key
	 * @return String
	 */
	public static String getProperty(String key) {
		String value = null;
		try {
			value = platform6.getString(key).trim();
		} catch (Exception ex) {
			logger.error("从platform6配置文件取" + key + "错误:");
			logger.error(ex.getMessage(), ex);
		}
		return value;
	}
	

}
