package com.jd.oa.common.redis.annotation;

import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface ListedCache {
    CacheKey key();
    String type() default "";
//    String[] parameterNames();
    int offsetIndex() default 1;
    int limitIndex() default 2;
//    public boolean layerd() default false;
}
