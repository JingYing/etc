package com.jd.oa.common.redis.annotation;


import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Cache {
    //    String k
    CacheKey key();

    String type() default "";

    String expireTime() default "";

    int expire() default 0;

    boolean cacheNull() default false;
}
