package com.jd.oa.common.redis.annotation;

import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface WriteListCache {
    CacheKey key();
    String type() default "";
//    String[] parameterNames();
    int parameterIndex() default 0;
    boolean writeParameter() default false;
    boolean writeReturn() default true;
    int max() default 1000;
    boolean append() default  false;
//    int expire();
//    boolean cacheNull() default false;
}
