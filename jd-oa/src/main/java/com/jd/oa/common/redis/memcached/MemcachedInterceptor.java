package com.jd.oa.common.redis.memcached;/*
package com.jd.jdcache.memcached;

import com.jd.jdcache.AbstractInterceptor;
import com.jd.jdcache.NullCachedObject;
import com.jd.jdcache.annotation.CacheEvict;
import com.jd.jdcache.annotation.ListedCache;
import com.jd.jdcache.annotation.WriteListCache;
import net.spy.memcached.MemcachedClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.List;


public class MemcachedInterceptor extends AbstractInterceptor {
    protected MemcachedClient memcachedClient;
    Logger logger = LoggerFactory.getLogger(MemcachedInterceptor.class);

    public MemcachedInterceptor(MemcachedClient client) {
        this.memcachedClient = client;
    }

    public Object doListCache(ListedCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<?> loadListFromCache(String key, int offset, int limit, Class<?> c, String cacheType) {
        return null;
    }

    @Override
    public void writeToCache(String cacheKey, Object value, int expire, boolean cacheNull, String cacheType) {
        if (value != null) {
            memcachedClient.set(cacheKey, expire, value);
        } else {
            if (cacheNull) {
                memcachedClient.set(cacheKey, expire, new NullCachedObject());
            }
        }
    }

    @Override
    public void writeListToCache(String cacheKey, Object value, boolean append, int max, String cacheType) {

    }

    @Override
    public Object loadFromCache(String key, Class<?> c, String cacheType) {
        return memcachedClient.get(key);
    }

    @Override
    public void removeFromCache(String key, String cacheType) {
        memcachedClient.delete(key);
    }

    @Override
    protected void evictCache(CacheEvict annotation, Object obj, Method method, Object[] args, net.sf.cglib.proxy.MethodProxy proxy) {


    }

    public Object writeListCache(WriteListCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        throw new UnsupportedOperationException();
    }


}
*/
