package com.jd.oa.common.paf.service;

import java.io.File;

/**
 * 流程发布服务接口类
 * User: zhaoming
 * Date: 13-9-25
 * Time: 下午6:21
 * To change this template use File | Settings | File Templates.
 */
public interface PafProcessDeployService {

    /**
     * 发布流程图
     * @param file 流程文件
     */
    public boolean deploy(File file);
}
