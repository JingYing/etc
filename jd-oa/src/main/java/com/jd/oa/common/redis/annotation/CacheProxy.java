package com.jd.oa.common.redis.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheProxy {
    String type() default "";
}
