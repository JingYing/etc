package com.jd.oa.common.service;

import java.io.Serializable;
import java.util.List;

import com.jd.oa.common.dao.Page;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;

/**
 * 封装原生API的Service泛型接口.
 * 
 * @param <T> Service操作的对象类型
 * @param <PK> 主键类型
 * 
 * @author zhouhq
 */
public interface BaseService<T, PK extends Serializable> {
	/**
	 * 保存新增对象.
	 */
	public PK insert(final T entity);
	
	/**
	 * 保存新增对象列表.
	 */
	public List<PK> insert(final List<T> entitys);
	
	/**
	 * 保存新增对象.
	 */
	public Object insert(final String ql, final Object... values);
	
	/**
	 * 根据ID删除对象.
	 * 
	 * @param id 对象的ID属性.
	 */
	public int delete(PK id);
	
	/**
	 * 根据ID删除对象.
	 * 
	 * @param id 对象的ID属性.
	 * @param isPhysicalDelete 是否物理删除.
	 */
	public int delete(PK id, final boolean isPhysicalDelete);
	
	/**
	 * 删除对象.
	 * 
	 * @param entity 对象必须是session中的对象或含id属性的transient对象.
	 */
	public int delete(final T entity, final boolean... isPhysicalDelete);
	
	/**
	 * 删除对象列表.
	 * 
	 * @param entity 对象必须是session中的对象或含id属性的transient对象.
	 * @param isPhysicalDelete 是否物理删除.
	 */
	public int delete(final List<T> entitys, final boolean... isPhysicalDelete);
	
	/**
	 * 删除对象.
	 * 
	 * @param ql 删除的sql语句
	 * @param values 删除语句中的参数.
	 */
	public int delete(final String ql, final Object... values);
	
	/**
	 * 保存修改的对象.
	 */
	public int update(final T entity);

	/**
	 * 保存修改的对象列表.
	 */
	public int update(final List<T> entitys);
	
	/**
	 * 保存修改的对象.
	 */
	public int update(final String ql, final Object... values);
	
	/**
	 * 按id获取对象.
	 */
	public T get(final PK id);
	
	/**
	 * 获取对象.
	 */
	public Object get(final String ql, final Object... values);


	/**
	 * 查询对象列表.
	 * @return List<T> 查询结果对象列表
	 * 
	 * @param T 参数对象.
	 */
	public List<T> find(T entity);
	
	/**
	 * 查询对象列表的数量.
	 * @return 查询结果的数量
	 * 
	 * @param T 参数对象.
	 */
	public long findCount(T entity);
	
	/**
	 * 查询对象列表.
	 * @return List<X> 查询结果对象列表
	 * 
	 * @param ql 
	 * @param values 参数对象.
	 */
	public <X> List<X> find(final String ql, final Object... values);

	/**
	 * 查询对象列表的数量.
	 * @return 查询结果的数量
	 * 
	 * @param ql 
	 * @param values 参数对象.
	 */
	public long findCount(final String ql, final Object... values);

	/**
	 * 分页查询对象列表.
	 * @return Page<T> 查询结果的分页对象
	 * 
	 * @param page 分页参数对象 
	 * @param values 查询参数对象.
	 */
	public Page<T> find(Page<T> page, final T entity);
	
	/**
	 * 分页查询对象列表.
	 * @return Page<T> 查询结果的分页对象
	 * 
	 * @param page 分页参数对象 
	 * @param ql
	 * @param value 查询参数对象.
	 */
	public Page<T> find(final Page<T> page, final String ql, final Object value);
	
	/**
	 * 分页查询对象列表.
	 * @return Page<T> 查询结果的分页对象
	 * @author yujiahe
	 * @param page 分页参数对象 
	 * @param sql 查询分页的主sql语句
	 * @param value 查询参数对象.
	 * @param operateType 标示是否为动态SQL查询分页。1：是      0：否
	 */
	public Page<T> find(final Page<T> page, final String sql, final Object value, final int operateType);
	
	/**
	 * 分页查询对象列表.
	 * @return Page<Object> 查询结果的分页对象
	 * 
	 * @param page 分页参数对象 
	 * @param ql
	 * @param values 查询参数对象.
	 */
	public Page<Object> find(Page<Object> page, final String ql, final Object... values);
	
	/**
	 * 执行sql语句
	 * @param sqlAdapter
	 * @return
	 */
	public Object sql(SqlAdapter sqlAdapter);
}

