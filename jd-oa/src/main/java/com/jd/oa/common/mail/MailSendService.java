package com.jd.oa.common.mail;

import java.io.File;
import java.util.Map;

import javax.mail.MessagingException;

public interface MailSendService {

	/**
	 * 发送简单邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件文本内容
	 */
	public void sendSimpleMail(String[] to, String subject, String content);
	
	/**
	 * 异步发送简单邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件文本内容
	 */
	public void sendSimpleMailByAsyn(final String[] to, final String subject, final String content);

	/**
	 * 发送HTML邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件HTML内容
	 */
	public void sendHtmlMail(String[] to, String subject, String content);
	
	/**
	 * 异步发送HTML邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件HTML内容
	 */
	public void sendHtmlMailByAsyn(final String[] to, final String subject, final String content);

	/**
	 * 发送带附件的邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件内容
	 * @param attachedFiles
	 *            邮件附件列表
	 */
	public void sendAttachedFileMail(String[] to, String subject,
			String content, File[] attachedFiles);
	
	/**
	 * 异步发送带附件的邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件内容
	 * @param attachedFiles
	 *            邮件附件列表
	 */
	public void sendAttachedFileMailByAsyn(final String[] to, final String subject,
			final String content, final File[] attachedFiles);

	/**
	 * 发送velocity模板邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param templateFilePath
	 *            模板文件路径
	 * @param model
	 *            模板中的变量值
	 * @param attachedFiles
	 *            邮件附件列表
	 * @throws MessagingException
	 */
	public void sendTemplateMail(String[] to, String subject,
			String templateFilePath, Map<String, Object> model,
			File[] attachedFiles);
	
	/**
	 * 异步发送velocity模板邮件
	 * 
	 * @param to
	 *            邮件接收者
	 * @param subject
	 *            邮件主题
	 * @param templateFilePath
	 *            模板文件路径
	 * @param model
	 *            模板中的变量值
	 * @param attachedFiles
	 *            邮件附件列表
	 * @throws MessagingException
	 */
	public void sendTemplateMailByAsyn(final String[] to, final String subject,
			final String content, final Map<String, Object> model,
			final File[] attachedFiles);
}
