package com.jd.oa.common.redis.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface WriteCache {
    CacheKey key();
    String type() default "";
//    String[] parameterNames();
    int expire() default 0;
    String expireTime() default "";
    boolean cacheNull() default false;
    int parameterIndex() default 0;
    boolean writeParameter() default false;
    boolean writeReturn() default true;
//    public boolean layerd() default true;
}
