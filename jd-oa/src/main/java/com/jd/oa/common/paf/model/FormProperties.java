package com.jd.oa.common.paf.model;

import java.util.Map;

/**
 * 
 * @desc 待办任务子属性对象
 * @author WXJ
 * @date 2013-11-18 上午10:39:05
 *
 */
public class FormProperties {
    /**
     * name
     */
    private String name;

    /**
     * value
     */
    private String value;

    /**
     * id
     */
    private String id;

    /**
     * type
     */
    private String type;

    /**
     * readable
     */
    private String readable;

    /**
     * writable
     */
    private String writable;

    /**
     * required
     */
    private String required;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReadable() {
		return readable;
	}

	public void setReadable(String readable) {
		this.readable = readable;
	}

	public String getWritable() {
		return writable;
	}

	public void setWritable(String writable) {
		this.writable = writable;
	}

	public String getRequired() {
		return required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

}
