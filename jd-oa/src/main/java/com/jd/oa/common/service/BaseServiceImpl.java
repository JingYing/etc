package com.jd.oa.common.service;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.dao.Page;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;

/**
 * BaseService的默认实现类.
 * 
 * @param <T>
 *            Service操作的对象类型
 * @param <PK>
 *            主键类型
 * 
 * @author zhouhq
 */
// 默认将类中的所有函数纳入事务管理.
@Transactional
public abstract class BaseServiceImpl<T, PK extends Serializable> implements
		BaseService<T, PK> {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	public abstract BaseDao<T, PK> getDao();

	public PK insert(T entity) {
		return getDao().insert(entity);
	}

	public List<PK> insert(List<T> entitys) {
		return getDao().insert(entitys);
	}

	public Object insert(String ql, Object... values) {
		return getDao().insert(ql, values);
	}

	public int delete(PK id) {
		return getDao().delete(id);
	}
	
	public int delete(PK id, boolean isPhysicalDelete) {
		return getDao().delete(id, isPhysicalDelete);
	}
	
	public int delete(T entity, boolean... isPhysicalDelete) {
		return getDao().delete(entity, isPhysicalDelete);
	}

	public int delete(List<T> entitys, boolean... isPhysicalDelete) {
		return getDao().delete(entitys, isPhysicalDelete);
	}

	public int delete(String ql, Object... values) {
		return getDao().delete(ql, values);
	}

	public int update(T entity) {
		return getDao().update(entity);
	}

	public int update(List<T> entitys) {
		return getDao().update(entitys);
	}

	public int update(String ql, Object... values) {
		return getDao().update(ql, values);
	}

	public T get(PK id) {
		return getDao().get(id);
	}

	public Object get(String ql, Object... values) {
		return getDao().get(ql, values);
	}

	public List<T> find(T entity) {
		return getDao().find(entity);
	}

	public long findCount(T entity) {
		return getDao().findCount(entity);
	}

	public <X> List<X> find(String ql, Object... values) {
		return getDao().find(ql, values);
	}

	public long findCount(String ql, Object... values) {
		return getDao().findCount(ql, values);
	}

	public Page<T> find(Page<T> page, T entity) {
		return getDao().find(page, entity);
	}

	public Page<T> find(Page<T> page, String ql, Object value) {
		return getDao().find(page, ql, value);
	}
	
	public Page<T> find(Page<T> page, String sql, Object value, int operateType) {
		if(operateType == 0){
			return	 find( page, sql, value);
		}
		return getDao().find(page, sql, value ,operateType);
	}
	
	public Page<Object> find(Page<Object> page, String ql, Object... values) {
		return getDao().find(page, ql, values);
	}
	
	public Object sql(SqlAdapter sqlAdapter) {
		return getDao().sql(sqlAdapter);
	}
}
