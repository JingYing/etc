package com.jd.oa.common.paf.service.impl;

import com.jd.oa.common.paf.service.PafClientService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

/**
 * PAF客户端服务实现类
 * User: zhaoming
 * Date: 13-7-22
 * Time: 下午12:00
 * To change this template use File | Settings | File Templates.
 */
public class PafClientServiceImpl implements PafClientService {

    private Client client;

    private String wsdl;

    private void init(){
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        this.client = dcf.createClient(wsdl);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getWsdl() {
        return wsdl;
    }

    public void setWsdl(String wsdl) {
        this.wsdl = wsdl;
    }
}
