package com.jd.oa.common.paf.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: zhaoming
 * Date: 13-7-18
 * Time: 下午5:44
 * To change this template use File | Settings | File Templates.
 */
public class PafResult<T> implements Serializable {

    private static final long serialVersionUID = 588661676563845067L;

    /**
     * 执行状态
     */
    private boolean success;

    /**
     * 错误编码
     */
    private String errorCode;

    /**
     * 本地化的信息,对应错误编码的本地化说明
     */
    private String localizedMessage;

    /**
     * 错误信息栈
     */
    private String errorStack;

    /**
     * 返回值对象
     */
    private T result;

    
    /**
     * Total 任务总数，包括未返回的
     */
    private int total;
    
    public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public PafResult(boolean success) {
        this.success = success;
    }

    public PafResult(T result) {
        this.success = true;
        this.result = result;
    }

    public PafResult(boolean success, T result) {
        this.success = success;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorStack() {
        return errorStack;
    }

    public void setErrorStack(String errorStack) {
        this.errorStack = errorStack;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }

    public void setLocalizedMessage(String localizedMessage) {
        this.localizedMessage = localizedMessage;
    }

}

