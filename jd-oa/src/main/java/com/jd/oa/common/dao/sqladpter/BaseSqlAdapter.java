package com.jd.oa.common.dao.sqladpter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.DateUtils;

/**
 * 基础sql适配器对象
 * 
 * @author zhouhuaqi
 *
 */
public abstract class BaseSqlAdapter implements SqlAdapter{

	private static Logger logger = LoggerFactory.getLogger(BaseSqlAdapter.class);

	/**
	 * 日期格式化字符串 如:2013-06-20
	 */
	public static final String FMT_DATE = "yyyy-MM-dd";

	/**
	 * 时间格式化字符串 如:13:01:01
	 */
	public static final String FMT_TIME = "HH:mm:ss";

	/**
	 * 日期时间格式化字符串 如:2013-06-20 13:01:01
	 */
	public static final String FMT_DATETIME = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 内置表别名前缀
	 */
	private final String TABLE_ALIAS_NAME = "TEMP_TABLE_ALIAS_";

	/**
	 * 内置表别名索引
	 */
	private int TABLE_ALIAS_INDEX = 0;
	
	/**
	 * sql类型
	 */
	private SqlType sqlType;

	/**
	 * 是否去重
	 */
	private boolean isDistinct = false;

	/**
	 * 表名及别名集合 key : table & view name | value : alias name
	 * <p>
	 * <b>注意:</b>目前不支持重复表查询
	 * </p>
	 */
	private Map<String, String> tableMap = new HashMap<String, String>();

	/**
	 * 待查列名集合 key : column name value : | alias name
	 */
	protected Map<String, String> columnMap = new LinkedHashMap<String, String>();

	/**
	 * 查询条件集合 key : and & or | value 条件值
	 */
	private Map<String, String> conditionMap = new IdentityHashMap<String, String>();

	/**
	 * 排序条件集合
	 */
	private Map<String, String> orderMap = new HashMap<String, String>();

	/**
	 * 分组集合
	 */
	private List<String> groupList = new ArrayList<String>();
	
	/**
	 * 直传sql
	 */
	protected String sql;

	/**
	 * sql生成结果
	 */
	private StringBuilder sqlResult = new StringBuilder();

	public String toSql() {
		sqlResult = new StringBuilder();
		if (tableMap.isEmpty()) {
			throw new BusinessException("Failure", "未发现表或视图");
		}
		
		switch (getSqlType()) {
		case DDL_CREATE_TABLE:
			this.append(getSqlType().toString());
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
				this.append(",");
			}
			this.clearEndChar();
			this.append("(");
			// 增加固定的5个字段信息
			this.addColumn(null, "YN", DataType.NUMBER, "1", Boolean.FALSE, Boolean.TRUE, null, "逻辑删除 0:启用 1:删除");
			this.addColumn(null, "CREATOR", DataType.STRING, "32", Boolean.FALSE, Boolean.TRUE, null, "创建人");
			this.addColumn(null, "CREATE_TIME", DataType.DATETIME, null, Boolean.FALSE, Boolean.TRUE, null, "创建时间");
			this.addColumn(null, "MODIFIER", DataType.STRING, "32", Boolean.FALSE, Boolean.TRUE, null, "修改人");
			this.addColumn(null, "MODIFY_TIME", DataType.DATETIME, null, Boolean.FALSE, Boolean.TRUE, null, "修改时间");
			for (String key : columnMap.keySet()) {
				this.append(columnMap.get(key));
				this.append(",");
			}
			this.clearEndChar();
			for (String key : columnMap.keySet()) {
				if(key.indexOf("PK_")==0)
					this.append(", PRIMARY KEY (`" + key.substring(key.indexOf("PK_")+3) + "`)");
			}
			this.append(")");
			break;
		case DDL_DROP_TABLE:
			this.append(getSqlType().toString());
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
				this.append(",");
			}
			this.clearEndChar();
			break;
		case DDL_ADD_COLUMN:
			this.append("ALTER TABLE");
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
				this.append(",");
			}
			this.clearEndChar();
			for (String key : columnMap.keySet()) {
				this.append(getSqlType().toString());
				this.append(columnMap.get(key));
				this.append(",");
			}
			this.clearEndChar();
			for (String key : columnMap.keySet()) {
				if(key.indexOf("PK_")==0)
					this.append(", DROP PRIMARY KEY, ADD PRIMARY KEY (`" + key.substring(key.indexOf("PK_")+3) + "`)");
			}
			break;
		case DDL_DROP_COLUMN:
			this.append("ALTER TABLE");
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
				this.append(",");
			}
			this.clearEndChar();
			for (String key : columnMap.keySet()) {
				this.append(getSqlType().toString());
				this.append(columnMap.get(key));
				this.append(",");
			}
			this.clearEndChar();
			break;
		case DDL_CHANGE_COLUMN:
			this.append("ALTER TABLE");
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
				this.append(",");
			}
			this.clearEndChar();
			for (String key : columnMap.keySet()) {
				this.append(getSqlType().toString());
				this.append(columnMap.get(key));
				this.append(",");
			}
			this.clearEndChar();
			for (String key : columnMap.keySet()) {
				if(key.indexOf("PK_")==0)
					this.append(", DROP PRIMARY KEY, ADD PRIMARY KEY (`" + key.substring(key.indexOf("PK_")+3) + "`)");
			}
			break;
		case DML_SELECT:
			this.append(getSqlType().toString());
			if (isDistinct) {
				this.append("DISTINCT");
			}

			if (columnMap.isEmpty()) {
				this.append("*");
			} else {
				for (String key : columnMap.keySet()) {
					this.append(getColumnSql(key));
					if (!StringUtils.isBlank(columnMap.get(key))) {
						this.append("AS").append(columnMap.get(key));
					}
					this.append(",");
				}
				this.clearEndChar();
			}

			this.append("FROM");

			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
				if (!StringUtils.isBlank(tableMap.get(key))) {
					this.append("AS").append(tableMap.get(key));
				}
				this.append(",");
			}
			this.clearEndChar().append(" ");

			if (!groupList.isEmpty()) {
				this.append("GROUP BY");
				for (String field : groupList) {
					this.append(getColumnSql(field)).append(",");
				}
				this.clearEndChar();
			}

			if (!conditionMap.isEmpty()) {
				if (!groupList.isEmpty()) {
					this.append("HAVING");
				} else {
					this.append("WHERE");
				}
				this.append("1 = 1");
				for (Entry<String, String> entry : conditionMap.entrySet()) {
					this.append(entry.getKey()).append(entry.getValue());
				}
			}

			if (!orderMap.isEmpty()) {
				this.append("ORDER BY");
				for (String key : orderMap.keySet()) {
					this.append(getColumnSql(key)).append(orderMap.get(key))
							.append(",");
				}
				this.clearEndChar();
			}
			break;
		case DML_INSERT:
			this.append(getSqlType().toString());
			this.append("INTO");
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
			}
			this.append("(");
			if (!columnMap.isEmpty()) {
				for (String key : columnMap.keySet()) {
					this.append(key);
					this.append(",");
				}
				this.clearEndChar();
			}
			this.append(") VALUES (");
			if (!columnMap.isEmpty()) {
				for (String key : columnMap.keySet()) {
					this.append(columnMap.get(key));
					this.append(",");
				}
				this.clearEndChar();
			}
			this.append(")");
			break;
		case DML_UPDATE:
			this.append(getSqlType().toString());
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
			}
			this.append("SET");
			if (!columnMap.isEmpty()) {
				for (String key : columnMap.keySet()) {
					this.append(columnMap.get(key));
					this.append(",");
				}
				this.clearEndChar();
			}
			if (!conditionMap.isEmpty()) {
				this.append("WHERE");
				this.append("1 = 1");
				for (Entry<String, String> entry : conditionMap.entrySet()) {
					this.append(entry.getKey()).append(entry.getValue());
				}
			}
			break;
		case DML_DELETE:
			this.append(getSqlType().toString());
			this.append("FROM");
			for (String key : tableMap.keySet()) {
				this.append("`" + key + "`");
			}
			if (!conditionMap.isEmpty()) {
				this.append("WHERE");
				this.append("1 = 1");
				for (Entry<String, String> entry : conditionMap.entrySet()) {
					this.append(entry.getKey()).append(entry.getValue());
				}
			}
			break;
		default:
			break;
		}

		if (logger.isDebugEnabled()) {
			logger.trace(sqlResult.toString().replace(" ,", ",")
					.replace("  ", " ").replace(" 1 = 1 AND", "")
					.replace(" 1 = 1 OR", ""));
		}

		return sqlResult.toString().replace(" ,", ",").replace("  ", " ")
				.replace(" 1 = 1 AND", "").replace(" 1 = 1 OR", "");
	}

	/**
	 * 设置 是否Distinct查询
	 * 
	 * @param isDistinct
	 */
	public void setDistinct(boolean isDistinct) {
		this.isDistinct = isDistinct;
	}

	public SqlAdapter addTable(String tableName) {
		tableMap.put(tableName, TABLE_ALIAS_NAME + TABLE_ALIAS_INDEX++);
		return this;
	}

	public SqlAdapter addTable(String tableName, String aliasName) {
		if (aliasName.contains(TABLE_ALIAS_NAME)) {
			throw new BusinessException("Failure",
					"不能使用这个别名前缀:" + TABLE_ALIAS_NAME);
		}
		tableMap.put(tableName, aliasName);
		return this;
	}
	
	public abstract SqlAdapter addColumn(String oldColumnName, String columnName, DataType dataType, String dataLength, boolean isPK, boolean isNull, String defaultValue, String comment);

	public SqlAdapter addColumn(String columnName) {
		return this.addColumn(columnName, "");
	}

	public SqlAdapter addColumn(String columnName, String aliasName) {
		columnMap.put(columnName, aliasName);
		return this;
	}

	public SqlAdapter addColumn(String columnName, String columnValue,
			DataType dataType) {
		switch (getSqlType()) {
		case DML_UPDATE:
			switch (dataType) {
			case STRING:
				columnMap
						.put(columnName,
								getColumnSql(columnName).concat("='").concat(handleSingleQuotes(columnValue))
										.concat("'"));
				break;
			case NUMBER:
				columnMap.put(columnName,
						getColumnSql(columnName).concat("=").concat(columnValue));
				break;
			default:
				columnMap.put(columnName, this.getDateTimeQuery(columnName,
						RelationalOperator.EQ, columnValue, dataType));
				break;
			}
			break;
		case DML_INSERT:
			switch (dataType) {
			case STRING:
				columnMap
						.put(getColumnSql(columnName), ("'").concat(handleSingleQuotes(columnValue)).concat("'"));
				break;
			case NUMBER:
				columnMap.put(getColumnSql(columnName), columnValue);
				break;
			case DATE:
				columnMap.put(getColumnSql(columnName), ("'").concat(DateUtils.dateFormat(new Date(DateUtils.getTimes(columnValue)))).concat("'"));
				break;
			case TIME:
				columnMap.put(getColumnSql(columnName), ("'").concat(columnValue).concat("'"));
				break;
			case DATETIME:
				try {
					columnMap.put(getColumnSql(columnName),  ("'").concat(DateUtils.datetimeFormat24(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(columnValue))).concat("'"));
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
				}
				break;
			default:
				columnMap
				.put(getColumnSql(columnName), ("'").concat(columnValue).concat("'"));
				break;
			}
			break;
		default:
			break;
		}
		return this;
	}
	
	private String handleSingleQuotes(String s)	{
		return s.replaceAll("'", "''");
	}

	/**
	 * 添加查询条件
	 * 
	 * <p>
	 * <span>针对日期类型查询的说明</span> <div>
	 * 日期查询必须将查询值按照本类提供的格式化字符串进行格式化,并指定相应的ValueType </div>
	 * </p>
	 * 
	 * @param logicalOperator
	 * @param columnName
	 * @param relationalOperator
	 * @param columnValue
	 * @param dataType
	 * @return
	 */
	private SqlAdapter addCondition(LogicalOperator logicalOperator,
			String columnName, RelationalOperator relationalOperator,
			String columValue, DataType dataType) {
		switch (dataType) {
		case STRING:
			conditionMap.put(
					logicalOperator.toString(),
					getColumnSql(columnName).concat(" ")
							.concat(relationalOperator.toString()).concat(" '")
							.concat(columValue).concat("'"));
			break;
		case NUMBER:
			conditionMap.put(
					logicalOperator.toString(),
					getColumnSql(columnName).concat(" ")
							.concat(relationalOperator.toString()).concat(" ")
							.concat(columValue));
			break;
		default:
			conditionMap.put(logicalOperator.toString(), this.getDateTimeQuery(
					columnName, relationalOperator, columValue, dataType));
			break;

		}
		return this;
	}

	/**
	 * 目前仅支持 Oracle, MySQL ; 日期查询生成 其他数据库方式请根据需要自行扩展
	 */
	protected abstract String getDateTimeQuery(String columnName, RelationalOperator ro,
			String columnValue, DataType dataType);

	private SqlAdapter addLikeCondition(LogicalOperator logicalOperator,
			String columnName, String columnValue) {
		if (columnValue.contains("%")) {
			conditionMap.put(
					logicalOperator.toString(),
					getColumnSql(columnName).concat(" LIKE \'")
							.concat(columnValue).concat("\'"));
		} else {
			conditionMap.put(
					logicalOperator.toString(),
					getColumnSql(columnName).concat(" LIKE \'%")
							.concat(columnValue).concat("%\'"));
		}
		return this;
	}

	private SqlAdapter addInCondition(LogicalOperator logicalOperator,
			String columnName, String columnValue) {
		conditionMap.put(logicalOperator.toString(), getColumnSql(columnName)
				.concat(" IN ( ").concat(columnValue).concat(" )"));
		return this;
	}

	private SqlAdapter addNotInCondition(LogicalOperator logicalOperator,
			String columnName, String columnValue) {
		conditionMap.put(logicalOperator.toString(), getColumnSql(columnName)
				.concat(" NOT IN ( ").concat(columnValue).concat(" )"));
		return this;
	}

	private SqlAdapter addOrderBy(String orderField, SortMethod sortMethod) {
		orderMap.put(orderField, sortMethod.toString());
		return this;
	}

	public SqlAdapter addGroupBy(String groupField) {
		groupList.add(groupField);
		return this;
	}

	public SqlAdapter addAndCondition(String columnName,
			RelationalOperator relationalOperator, String columnValue,
			DataType dataType) {
		return this.addCondition(LogicalOperator.AND, columnName,
				relationalOperator, columnValue, dataType);
	}

	public SqlAdapter addOrCondition(String columnName,
			RelationalOperator relationalOperator, String columnValue,
			DataType dataType) {
		return this.addCondition(LogicalOperator.OR, columnName,
				relationalOperator, columnValue, dataType);
	}

	public SqlAdapter addAndLikeCondition(String columnName, String columnValue) {
		return this.addLikeCondition(LogicalOperator.AND, columnName,
				columnValue);
	}

	public SqlAdapter addOrLikeCondition(String columnName, String columnValue) {
		return this.addLikeCondition(LogicalOperator.OR, columnName,
				columnValue);
	}

	public SqlAdapter addAndInCondition(String columnName, String columnValue) {
		return this
				.addInCondition(LogicalOperator.AND, columnName, columnValue);
	}

	public SqlAdapter addOrInCondition(String columnName, String columnValue) {
		return this.addInCondition(LogicalOperator.OR, columnName, columnValue);
	}

	public SqlAdapter addAndNotInCondition(String columnName, String columnValue) {
		return this.addNotInCondition(LogicalOperator.AND, columnName,
				columnValue);
	}

	public SqlAdapter addOrNotInCondition(String columnName, String columnValue) {
		return this.addNotInCondition(LogicalOperator.OR, columnName,
				columnValue);
	}

	public SqlAdapter addAscOrderBy(String orderField) {
		return this.addOrderBy(orderField, SortMethod.ASC);
	}

	public SqlAdapter addDescOrderBy(String orderField) {
		return this.addOrderBy(orderField, SortMethod.DESC);
	}

	public SqlAdapter clearTable() {
		tableMap.clear();
		return this;
	}

	public SqlAdapter clearColumn() {
		columnMap.clear();
		return this;
	}

	public SqlAdapter clearCondition() {
		conditionMap.clear();
		return this;
	}

	public SqlAdapter clearOrder() {
		orderMap.clear();
		return this;
	}

	public SqlAdapter clearGroup() {
		groupList.clear();
		return this;
	}
	
	public SqlAdapter append(String string) {
		sqlResult.append(string).append(" ");
		return this;
	}

	public SqlAdapter clearEndChar() {
		sqlResult.delete(sqlResult.length() - 2, sqlResult.length());
		return this;
	}

	/**
	 * 把column的包含的表名转换成别名
	 * @param columnName
	 * @return
	 */
	protected String getColumnSql(String columnName) {
		StringBuffer columnSql = new StringBuffer();
		if (columnName.split("\\.").length > 1)
			columnSql.append("`" + tableMap.get(columnName.split("\\.")[0]) + "`")
					.append(".").append("`" + columnName.split("\\.")[1] + "`");
		else
			//columnSql.append(tableMap.get(tableMap.keySet().iterator().next()) + "." + columnName);
			columnSql.append("`" + columnName + "`");
		return columnSql.toString();
	}
	
	public String getSql() {
		return sql;
	}

	public SqlAdapter setSql(String sql) {
		this.sql = sql;
		return this;
	}
	
	public SqlType getSqlType(){
		return sqlType;
	}
	
	public SqlAdapter setSqlType(SqlType sqlType){
		this.sqlType = sqlType;
		return this;
	}
}