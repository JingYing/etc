package com.jd.oa.common.dao.dialect.impl;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.jd.oa.common.dao.dialect.Dialect;

/**
 * Mysql Dialet
 * 
 * 提供mysql的分页处理
 * 
 * @author zhouhq
 */
public class MySQLDialect implements Dialect {
	protected static final String SQL_END_DELIMITER = ";";

	public String getLimitString(String sql, int offset, int limit) {
		sql = StringUtils.trim(sql);
		StringBuffer sb = new StringBuffer(sql.length() + 20);
		sb.append(sql);
		if (offset > 0) {
			sb.append(" limit ").append(offset).append(',').append(limit)
					.append(SQL_END_DELIMITER);
		} else {
			sb.append(" limit ").append(limit).append(SQL_END_DELIMITER);
		}
		return sb.toString();
	}

	@Override
	public boolean supportsLimit() {
		return true;
	}

	@Override
	public String getLimitString(String sql, boolean hasOffset, int offset,
			int limit) {
		return new StringBuffer(sql.length() + 20).append(trim(sql))
				.append(hasOffset ? " limit ?,?" : " limit ?")
				.append(SQL_END_DELIMITER).toString();
	}
	
	@Override
	public String getOrderString(String originalSql, LinkedHashMap<String, String> orderItems) {
		String ex = "";
        StringBuilder exSb = new StringBuilder("");
		if (orderItems != null && orderItems.size() > 0) {
			Iterator<Entry<String, String>> it = orderItems.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> entry = it.next();
//				ex = ex + "," + entry.getKey() + " " + entry.getValue();
                if(entry.getKey().contains("TIME")||entry.getKey().contains("SORT")){
//                    ex = ex + "," + entry.getKey() + " " + entry.getValue();
                    exSb.append("," + entry.getKey() + " " + entry.getValue());
                }else{
//                    ex=ex + ", CONVERT( " + entry.getKey() + " USING gbk ) COLLATE gbk_chinese_ci " + entry.getValue();
                    exSb.append(", CONVERT( " + entry.getKey() + " USING gbk ) COLLATE gbk_chinese_ci " + entry.getValue());
                }
			}
		}
		ex = exSb.toString();
		if (ex.trim().length() > 0) {
			if (ex.startsWith(",")) {
				ex = ex.substring(1);
			}
			return originalSql + " order by "+ex;
		}

		return originalSql;
	}

	private String trim(String sql) {
		sql = sql.trim();
		if (sql.endsWith(SQL_END_DELIMITER)) {
			sql = sql.substring(0,
					sql.length() - 1 - SQL_END_DELIMITER.length());
		}
		return sql;
	}

}
