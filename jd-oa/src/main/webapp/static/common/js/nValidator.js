//nValidator = {};

//绑定容器后，则其内部的输入框都接受验证
function bindValidate(container,config){
	if(config!=null){
		container.validator(config);
	}else{
		container.validator({timely:2});
	}
};

function validate2(container){
	var bol = true;
	$("input,select,textarea",container).each(function(){
		$(this).trigger('validate');
		if(!$(this).isValid()){
			bol = false;
		}
	});
	
	return bol;
}