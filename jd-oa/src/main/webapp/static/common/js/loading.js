var url = window.location.href;  //当前url
var host= window.location.host; //当前域名
url = url.replace("http://"+host,""); //请求路径
//loading的HTML
var processBarHtml='<div id="background" class="background" style="display: none; "></div>'+
	'<div id="progressBar" class="progressBar" style="display: none; ">'
		+'<img src="'+springUrl+'/static/common/img/waiting.gif" width="30" height="30" style="margin-right:8px;" align="absmiddle" />正在响应，请稍候...'
		+'</div>';
//定义当前frame
var selfFrame=document;
if(url.indexOf("/app") == 0){
    //如果是前端
    if(selfFrame.getElementById("mainFrame")==null ){
    	try{
    		selfFrame=parent.document;
    		if(selfFrame.getElementById("mainFrame")==null){
    			selfFrame=parent.parent.document;
    			if(selfFrame.getElementById("mainFrame")==null){
    				selfFrame=parent.parent.parent.document;
    			}
    		}
    	}catch(e){
    	}
    }
}
var processBar=$("#progressBar",selfFrame);
if(processBar.html()==null){
		$("body",selfFrame).append(processBarHtml);
		processBar=$("#progressBar",selfFrame);
}
processBar=$("#background,#progressBar",selfFrame);
var bar=$("#progressBar",selfFrame);
var currentAjaxNum = 0;
$(document).ajaxStart(function () {
    if(url.indexOf("/app") == 0){
        bar.css("top",( (window.screen.availHeight- bar.height() -100)/2) + "px" );
    }else{
        bar.css("top",( (window.screen.availHeight- bar.height() -100-300)/2) + "px" );
    }
    bar.css("left",(selfFrame.documentElement.scrollLeft + ($(selfFrame).width() - bar.width()) / 2) + "px" );
    processBar.show();
    currentAjaxNum++;
}).ajaxStop(function () {
        currentAjaxNum--;
        if(currentAjaxNum <= 0){
            processBar.hide();
        }
    }).ajaxError(function(){
        if(currentAjaxNum <= 0){
            processBar.hide();
        }
    }).ajaxComplete(function(){
        if(currentAjaxNum <= 0){
            processBar.hide();
        }
    });
