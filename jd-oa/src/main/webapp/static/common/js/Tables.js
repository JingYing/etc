//author wangdongxing@jd.com
//date:2013-08-27
if(Table==null){
	var Table={
			tableSelectedTrs:null,
			baseCompent:{
				defaultCols:function(tableId){
					var tt='<input type="checkbox" name="all_chk" onclick="selectAll($(this),\''+tableId+'\')" />';
					var cols=[
					            {"bSortable":false,"sTitle":tt,"sWidth":20,"fnRender":function(obj){
					            	return '<input type="checkbox" name="chk" onclick="changRowState(\''+tableId+'\',$(this));" value="'+obj.aData.id+'" />';
					            }}
					        ];
					return cols;
				},
				defaultBtns:function(tableId){
					var btns=[
						{
						    "sExtends":    "text",
						    "sButtonText": "全选",
							"sToolTip": "",
							"fnClick": function ( nButton, oConfig, oFlash ) {
						        $("#"+tableId).find("input[name='chk']").each(function(){
									$(this).attr("checked",true);
									$(this).parent().parent().addClass("active");
								});
						    }
						},
						{
						    "sExtends":    "text",
						    "sButtonText": "反选",
							"sToolTip": "",
							"fnClick": function ( nButton, oConfig, oFlash ) {
						        $("#"+tableId).find("input[type='checkbox']").each(function(){
									var checked=$(this).attr("checked");
									if(checked){
										$(this).attr("checked",false);
									}else{
										$(this).attr("checked",true);
									}
									changRowState($(this).parent().parent());
								});
						    }
						},
						{
						    "sExtends":    "text",
						    "sButtonText": "不选",
							"sToolTip": "",
							"fnClick": function ( nButton, oConfig, oFlash ) {
						        $("#"+tableId).find("input[type='checkbox']").each(function(){
									$(this).attr("checked",false);
									$(this).parent().parent().removeClass("active");
								});
						    }
						}
						];
					
					return btns;
				}
			},
			
			//table的配置项目
			options:{
				pageUrl:null, //后台地址
				useCheckbox:null,
				isPaging:null,
				scrollX:null,
				scrollY:null,
				defaultSort:null,
				sendData:null,//前台向后台传处理
				columns:null, //页面的列表
				btns:null, //自定义的按钮处理
				moveRow:null, //上下行交换
				callback_fn:null,
				remember_status:null
			},
			currentPage:null,
			dataTable:function(tableId,options){
				if(options.remember_status==null || options.remember_status==true ){
					Table.tableSelectedTrs[tableId]=new Array();
				}
//				var tableBtns=$.merge(Table.baseCompent.defaultBtns(tableId),options.btns);
				var tableBtns=options.btns;
				//支持checkbox操作
				if(options.useCheckbox==null||options.useCheckbox==true){
					var tableCols=$.merge(Table.baseCompent.defaultCols(tableId),options.columns);
					var tableTool={
							"aButtons":tableBtns
					};
				}else{ //不支持checkbox操作
					var tableCols=options.columns;
					var tableTool={
							"sRowSelect": "single", //多行的选中
							"aButtons":tableBtns
					};
				}
				if(options.defaultSort==null){
					options.defaultSort=[];
				}
				if(options.isPaging==null){
					options.isPaging=true;
				}
				var sDom="<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>";
				if(options.isPaging==false){
					if(tableBtns==null||tableBtns.length==0){
						sDom="Rt";
					}else{
						sDom="<'row-fluid'<'span12'T>>Rt";
					}
				}
				if(options.scrollY==null){
					options.scrollY="450px";
				}
				//alert(1);
				var oTable = $('#'+tableId).dataTable( {
			        "bProcessing": false,
					"bServerSide":true,
					"sPaginationType": "full_numbers",
					"sAjaxSource":options.pageUrl,
					"sServerMethod": "POST",
					 "bAutoWidth": false,
					 "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
					 "sScrollY":options.scrollY,
				     "bScrollCollapse": true,
				     "bPaginate":options.isPaging, 
					"oLanguage": {
			            "sLengthMenu": "每页显示 _MENU_ 条记录",
			            "sZeroRecords": "抱歉， 没有找到",
			            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
			            "sInfoEmpty": "没有数据",
			            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
			            "oPaginate": {
			            "sFirst": "首页",
			            "sPrevious": "前页",
			            "sNext": "后页",
			            "sLast": "尾页"}
			        },
					//"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
					"sDom": sDom,
			        "sPaginationType": "bootstrap",
			        "bJQueryUI": true,
					"bFilter":false,
					"fnServerData":function(sSource, aoData, fnCallback){
						options.sendData(sSource, aoData, fnCallback);
					},
					"fnDrawCallback": function( oSettings ){
						if(Table.tableSelectedTrs[tableId]!=null&&Table.tableSelectedTrs[tableId].length>0){
							$.each(Table.tableSelectedTrs[tableId],function(i,n){
								Table.selectRow($("#"+tableId).find("input[name='chk'][value='"+n+"']").parent().parent());
							});
						}
//						top.autoHeight($(document).height());
						if(options.callback_fn!=null){
							options.callback_fn();
						}
					},
					"aaSorting":options.defaultSort,
			        "aoColumns":tableCols,
					"oTableTools":tableTool
			    } );
				//alert(2);
				if(options.moveRow!=null&&options.moveRow==true){
					oTable.rowReordering();
				}
				//alert(3);
				return oTable;
			},
			//重新绘制数据
			render:function(table){
				if(table!=null){
					table.fnDraw();
				}
			},
			//获取选中行的数据,返回结果是数组
			getSelectedRows:function(table){
				var datas=new Array();
				table.find("tr.active").each(function(){
					var data = table.fnGetData( this );
					datas.push(data);
				});
				return datas;
			},
			//获取选中行的ID数组
			getSelectedRowsIDs:function(table){
				var IDs=new Array();
				table.find("tr.active").each(function(){
					var data = table.fnGetData( this );
					IDs.push(data.id);
				});
				return IDs;
			},
			getAllSelectedRowIds:function(tableId){
				return Table.tableSelectedTrs[tableId];
			},
			setSelectedRowsIds:function(tableId,ids){
                if(Table.tableSelectedTrs[tableId]!=null){
                	if(ids=="")  {
                		ids="[]" ;
                	}
                	Table.tableSelectedTrs[tableId]=JSON.parse(ids);
                }
			},
			selectRow:function(tr){
				tr.find("input[name='chk']").attr("checked",true);
				tr.addClass("active");
			},
			unselectRow:function(tr){
				tr.find("input[name='chk']").attr("checked",false);
				tr.removeClass("active");
			}
	};
	Table.tableSelectedTrs={};
	function selectAll(cb,tableId){
		if(cb.attr("checked")=="checked"){
			$("#"+tableId).find("input[name='chk']").each(function(){
				$(this).attr("checked",true);
				if(Table.tableSelectedTrs[tableId]!=null){
					if(array_exists(Table.tableSelectedTrs[tableId],$(this).val())){
						Table.tableSelectedTrs[tableId].push($(this).val());
					}
				}
				$(this).parent().parent().addClass("active");
			});
		}else{
			$("#"+tableId).find("input[name='chk']").each(function(){
				$(this).attr("checked",false);
				if(Table.tableSelectedTrs[tableId]!=null){
					var pos=array_pos(Table.tableSelectedTrs[tableId],$(this).val());
					if(pos!=-1){
						Table.tableSelectedTrs[tableId]=Table.tableSelectedTrs[tableId].deleteIndex(pos);
					}
				}
				$(this).parent().parent().removeClass("active");
			});
		}
	}
	function changRowState(tableId,tr){
		var id=tr.val();
		tr=tr.parent().parent();
		if(tr.hasClass("active")){
			if(Table.tableSelectedTrs[tableId]!=null){
				var pos=array_pos(Table.tableSelectedTrs[tableId],id);
				if(pos!=-1){
					Table.tableSelectedTrs[tableId]=Table.tableSelectedTrs[tableId].deleteIndex(pos);
				}
			}
			tr.removeClass("active");
			if($("#"+tableId+"_wrapper")!=null){
				$("#"+tableId+"_wrapper").find("input[type='checkbox'][name='all_chk']").attr("checked",false);
			}
		}else{
			if(Table.tableSelectedTrs[tableId]!=null){
				Table.tableSelectedTrs[tableId].push(id);
			}
			tr.addClass("active");
		}
	}
	Array.prototype.deleteIndex = function(index) {
		if(index < 0){
			return this;
		}else{
			return this.slice(0,index).concat(this.slice(index + 1,this.length));
		}
	};  
	function array_exists(array1,m){
		var notExists=true;
		for(i=0;i<array1.length;i++){
			if(array1[i]==m){
				notExists=false;
				return notExists;
			}
		}
		return notExists;
	}
	function array_pos(array1,m){
		var pos=-1;
		for(i=0;i<array1.length;i++){
			if(array1[i]==m){
				pos=i;
				return pos;
			}
		}
		return pos;
	}	
}