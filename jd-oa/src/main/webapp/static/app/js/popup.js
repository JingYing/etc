// 点击弹出窗口
(function($){
    /*
     * 弹出窗口
     */
    $.fn.extend({
        popup : function(){
            // 先隐藏其它窗口
            $(".window").hide();
            $(this).show();
            $(".popup-wrap").css("top",$(document).scrollTop()).show(200);
        }
    });
    /*
     * 隐藏弹出窗口
     */
    $.extend({
        popdown : function(){
            $(".popup-wrap").hide(100);
            $(".window").hide();
        }
    });
})(jQuery);
