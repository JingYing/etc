var orgTree;
//根节点 folders 传入参数
var zNodes ={
	    "id":"single",
	    "name":"组织机构",
	    "isParent":true,
	    "iconSkin":"diy1"
	};
var setting = {
	async : {
		enable : true,
		url :springUrl+"/auth/expression_orgTreeLoad",
		autoParam : ["id"]
	},
	check: {
		enable: false
	},
	 callback : {
        beforeClick : beforeClick
    },
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false,
		expandSpeed: "fast"
	}
};
/**
 * 初始化操作
 */
$(function() {
	//加载组织机构树
	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");

});	
		
function beforeClick(treeId, treeNode) {
	if(treeNode.id!="single" && treeNode.id!="multiple"){
		$("#box-right").show();
        $("#box-right").load(springUrl+"/system/sysOrganization_view?id="+treeNode.props.id);
        return true;
    }
}
