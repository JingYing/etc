//初始化分页加载
$(document).ready(
	function() {
		var options = Table.options;
		options = {
			pageUrl : "userRolePage",
			useCheckbox : false,
			defaultSort : [],
			sendData : function(sSource, aoData, fnCallback) {
				var roleId = $("#roleId").val();
				aoData.push({
					"name" : "realName",
					"value" : ""
				}, {
					"name" : "roleId",
					"value" : roleId
				}, {
					"name" : "orgId",
					"value" : ""
				});
				jQuery.ajax({
					type : "POST",
					url : "userRolePage",
					dataType : "json",
					data : aoData,
					success : function(resp) {
						fnCallback(resp);
					}
				});
			},
			columns : [
					{
						"mData" : "realName",
						"sTitle" : "用户姓名"
					},
					{
						"mData" : "organizationName",
						"sTitle" : "部门",
						"bSortable" : true
					},
					{
						"mData" : "id",
						"sTitle" : "操作",
						"bSortable" : false,
						"sWidth" : 200,
						"mRender" : function(data, type, full) {
							var delHtml = "<a href='#' class='btn' title='删除' onclick='javascript:del(\""
									+ data
									+ "\")'><img src='"
									+ springUrl
									+ "/static/common/img/del.png' alt='删除'/></a>";
							return delHtml;
						}
					} ],
			btns : []
		};
	table = Table.dataTable("userdetail", options);
});
function del(userId) { // 删除某角色下面的用户
	var roleId = $("#roleId").val();
//	console.log(roleId + "roleId");
	var a = {
		"roleId" : roleId,
		"userId" : userId
	};
	Dialog.del('sysUserRoles_deletebyUR', a, function(result) {
		Table.render(table);
	});
}
function cancel() {
	Dialog.hide();
}
