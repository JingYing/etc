var posTree;
//根节点 folders 传入参数
var zNodes ={
	    "id":"single",
	    "name":"职位",
	    "isParent":true,
	    "iconSkin":"diy1"
	};
var setting = {
	async : {
		enable : true,
		url :springUrl+"/auth/expression_posTreeLoad",
		autoParam : ["id"]
	},
	check: {
		enable: false
	},
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false,
		expandSpeed: "fast"
	}
};
/**
 * 初始化操作
 */
$(function() {
	//加载职位树
	jQuery.fn.zTree.init($("#posTree"), setting, zNodes);
	posTree = jQuery.fn.zTree.getZTreeObj("posTree");

});	


	

