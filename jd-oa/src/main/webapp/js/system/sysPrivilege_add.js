function save_insert(){
	var obj = document.getElementById("operationName");
	var data = {
		url : document.getElementById('url').value,
		operationCode : document.getElementById('operationCode').value,
		operationId : document.getElementById("operationId").value,
		resourceId : resourceId,
		name : document.getElementById("name").value,
		code : document.getElementById("code").value
	};
	saveAjax(data);
}
function save_update(){
	var obj = document.getElementById("operationName");
	var data = {
		url : document.getElementById('url').value,
		operationCode : document.getElementById('operationCode').value,
		operationId : document.getElementById("operationId").value,
		resourceId : resourceId,
		name : document.getElementById("name").value,
		code : document.getElementById("code").value,
		id : document.getElementById("id").value
	};
	saveAjax(data);
}

function selectChange(obj){
	var index = obj.selectedIndex;
	var text = obj.options[index].text;
	var value = obj.options[index].value;
	var operationCode  = obj.item(index).getAttribute('operationCode'); 
	document.getElementById("operationId").value = value;
	document.getElementById("operationCode").value = operationCode;
	
	document.getElementById("code").value = resourceCode + "_" + operationCode;
	document.getElementById("name").value = resourceName + "_" + text;
}

function saveAjax(data){
	jQuery.ajax({
        type : "POST",
        cache : false,
        url : baseUrl + "/system/sysPrivilege_save",
        data : data,
        success:function (msg) {
        	 Table.render(privilegeTable);
        	 Dialog.hide();
        },
        error : function(msg){
        	
        }
    });
}

$(document).ready(function(){//页面初始化设置

    bindValidate($("#addPrivilege"));// 绑定验证
    bindValidate($("#updatePrivilege"));// 绑定验证
});