var table;
var selectRoleId;
function setSelectOrgUserData(orgUserDataTable) {
    var objs = orgUserDataTable;
    var userids = '';
    for (var i = 0; i < objs.length; i++) {
        var obj = objs[i];
        userids = userids + obj.id;
        if (i != objs.length - 1) {
            userids = userids + ",";
        }
    }
    saveUserRoles(userids, selectRoleId);
}

function saveUserRoles(userIds, roleId) {
    $.ajax({
        type: "GET",
        cache: "false",
        url: "sysUserRoles_updateSave",
        dataType: "json",
        data: "roleId=" + roleId + "&userIds=" + userIds,
        success: function (json) {
            if (json != null) {
                Dialog.hide();
                Dialog.alert("成功", "角色用户分配成功!");
            }
        }
    });
}

/**
 * 分配用户
 * @param roleId
 */
function remoteOrgUserInfo(roleId) {
    selectRoleId = roleId;
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function () {
                callBack();
            }
        }
    ];
    Dialog.openRemote('分配用户', baseUrl + '/system/sysAddress_getOrgUser?addressConfig=addressConfig&sysRoleId=' + roleId, 800, 550, buttons);
}

// 页面初始化加载分页
$(document).ready(
    function () {
        var options = Table.options;
        options = {
        		
            pageUrl: "sysRole_page",
            useCheckbox: false,
            defaultSort: [[2,"desc"]],
            sendData: function (sSource, aoData, fnCallback) {
                var roleName = $("input#roleName").val();
                var Type = $("select#roleType").val();
                aoData.push({
                    "name": "roleName",
                    "value": roleName
                }, {
                    "name": "roleType",
                    "value": Type
                });
                jQuery.ajax({
                    type: "POST",
                    url: "sysRole_page",
                    dataType: "json",
                    data: aoData,
                    success: function (resp) {
                        fnCallback(resp);
                    }
                });
            },
            // 分页列设置
            columns: [
                {
                    "mData": "roleName",
                    "sTitle": "名称",
                    "bSortable":true,
                    "sWidth":300

                },
                {
                    "mData": "roleType",
                    "bSortable":true,
                    "sTitle": "类别",
                    "sWidth":100,
                    "mRender": function (data) {
                        if (data == 0) {
                            return '系统用户';
                        } else {
                            return '用户组';
                        }
                    }
                },
                { "mData": "modifyTime", "sTitle": "最后更新", "bSortable": true,"sWidth":100,"mRender": function (data) {
                    return new Date(data).format("yyyy-MM-dd");
                }},
                {
                    "mData": "id",
                    "sTitle": "操作",
                    "fnRender": function (obj) {
//                        var nameHtml = "<a href='#'  title='编辑' onclick='javascript:view(\""
//                            + obj.aData.id
//                            + "\")'>"
//                            + obj.aData.roleName
//                            + "</a>&nbsp;&nbsp;";
                        var a = '{"id":"' + obj.aData.id + '"}';
                        var viewHtml = '<a href="#" class="btn" title="查看详情" onclick="view(\'' + obj.aData.id + '\')"><img src="'
                            + springUrl
                            + '/static/common/img/view_obj.gif" alt="修改"/></a>&nbsp;&nbsp;';
                        var editHtml = '<a href="#" class="btn" title="编辑" onclick="editRole(\'' + obj.aData.id + '\',\'' + obj.aData.roleType + '\')"><img src="'
                            + springUrl
                            + '/static/common/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;';
                        var delHtml = "<a href='#' class='btn' title='删除' onclick='javascript:del(\""
                            + obj.aData.id + "\",\"" + obj.aData.roleType
                            + "\")'><img src='"
                            + springUrl
                            + "/static/common/img/del.png' alt='删除'/></a>";
                        var acHtml = '&nbsp;&nbsp;<a href="#" class="btn" title="功能授权" onclick="ac(\'' + obj.aData.id + '\')"><img src="'
                            + springUrl
                            + '/static/common/img/shouquan.png" alt="功能授权"/></a>&nbsp;&nbsp;';
                        var userHtml = '<a href="#" class="btn" title="分配用户" onclick="remoteOrgUserInfo(\''
                            + obj.aData.id
                            + '\');"><img src="'
                            + springUrl
                            + '/static/common/img/enjoy.png" alt="分配用户"/></a>&nbsp;&nbsp;';
                        return viewHtml+editHtml + delHtml + acHtml
                            + userHtml;
                    }
                }
            ],
            // 分页按钮设置
            btns: [
                {
                    "sExtends": "text",
                    "sButtonText": "新增",
                    "sButtonClass": "btn btn-success",
                    "sToolTip": "",
                    "fnClick": function (nButton, oConfig, oFlash) {
                        addRole();
                        //Dialog.openRemote('新增角色', 'sysRole_add', 500, 200);
                    }
                }
            ]
        };

        table = Table.dataTable("hello", options);
        $('#search').click(function () {
            Table.render(table);
        });
    });

/**
 * 新增角色
 * @param data
 */
function addRole() {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function () {
                if(!validate2($("#addRole"))){
                    return false;
                }else{
                    return addRoles();
                }
            }
        }
    ];
    Dialog.openRemote('新增角色', 'sysRole_add', 550, 300, buttons);
}

/**
 * 更新角色
 * @param roleId,roleType
 */
function editRole(roleId, roleType) {
    if (roleType == 0) {//系统角色不能修改
        Dialog.alert("提示", "该角色属于系统角色，不能修改");
    } else {
        var buttons = [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function () {
                }
            },
            {
                "label": "保存",
                "class": "btn-success",
                "callback": function () {
                    if(!validate2($("#editRole"))){
                        return false;
                    }else{
                        return save();
                    }

                }
            }
        ];
        Dialog.openRemote('编辑角色', 'sysRole_update?sysroleId=' + roleId, 550, 300, buttons);
    }
}

/**
 * 功能授权
 * @param data
 */
function ac(data) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "授权",
            "class": "btn-success",
            "callback": function () {
                saveAc();
            }
        }
    ];
    Dialog.openRemote('功能授权', baseUrl + '/system/sysRolePrivilege_index?sysRoleId=' + data, 600, 550, buttons);
}

/**
 * 删除角色,传入角色ID
 */
function del(roleId, roleType) {
    if (roleType == 0) { //系统角色不能删除
        Dialog.alert("提示", "该角色属于系统角色，不能删除");
    } else {
        var a = {
            "id": roleId
        };
        $.ajax({
            type: "GET",
            cache: "false",
            url: "sysUserRole_deleteCheck",
            dataType: "json",
            data: "roleId=" + roleId,
            success: function (data) { // 查询该角色在用户角色配置表中是否已有记录，返回为记录条数
                if (data > 0) { // 已有记录则不能删除
                    Dialog.alert("提示", "该角色下有用户，不能删除");
                } else {
                    // 无记录继续删除
                    Dialog.del('sysRole_delete', a, function (result) {
                        Dialog.alert("系统提示","删除成功");
                        Table.render(table);
                    });
                }
            }
        });
    }
}
/**
 * 角色详情页面
 * @param roleId
 */
function view(roleId) {
    Dialog.openRemote('角色详情', 'sysRole_view?sysroleId=' + roleId, 700, 400);
}

/**
 * 刷新用户信息列表
 */
function refreshUserTable() {
    Table.render(table);
}