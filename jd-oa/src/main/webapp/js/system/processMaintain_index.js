/**
 * @Description: 维护流程页面JS
 * @author:yujiahe
 * 
 */
var PMTable;
$(function() {
	var pmTable_options = {
		pageUrl : springUrl + "/system/processMaintain_page",
		useCheckbox : true,
		defaultSort : [],
		isPaging : true,
		bAutoWidth : true,
		scrollY : "500px",
		callback_fn : function() {
		},
		sendData : function(sSource, aoData, fnCallback) {
			
			 var processInstanceName = $("input#processInstanceName").val();
			 var processDefinitionName = $("input#processDefinitionName").val();
             var status = $("select#status").val();
             var processInstanceId = $("input#processInstanceId").val();
             aoData.push({
                 "name": "processInstanceName",
                 "value": processInstanceName
             }, {
                 "name": "processDefinitionName",
                 "value": processDefinitionName
             }, {
                 "name": "status",
                 "value": status
             }, {
                 "name": "processInstanceId",
                 "value": processInstanceId
             });
			jQuery.ajax({
				type : "POST",
				url : sSource,
				dataType : "json",
				data : aoData,
				success : function(resp) {
					pmTableData = resp;
					fnCallback(resp);
				},
				error : function(msg) {
					Dialog.alert("操作失败", msg.responseText);
				}
			});
		},
		columns : [
				{
					"mDataProp" : "processInstanceId",
					"sTitle" : "流程实例ID",
					"sClass" : "my_class",
					"sWidth" : "200"
				},
				{
					"mDataProp" : "processInstanceName",
					"sTitle" : "流程实例名称",
					"sClass" : "my_class",
					"sWidth" : "100"
				},
				{
					"mDataProp" : "processDefinitionName",
					"sTitle" : "流程定义名称",
					"sClass" : "my_class",
					"sWidth" : "100"
				},
				
				{
					"sTitle" : "流程实例状态",
					"sClass" : "my_class",
					"sWidth" : "100",
					"fnRender" : function(obj) {
						var status = obj.aData.status;
						if (status == 1) {
							return "审批中";
						} else if (status == 2) {
							return "拒绝";
						} else if (status == 3) {
							return "驳回";
						} else {
							return "wrong";
						}
					}
				},
				
				{
					"bSortable" : false,
					"sWidth" : "300",
					"sTitle" : "修改当前节点审批人",
					"fnRender" : function(obj) {
						var id = obj.aData.id;
						var processInstanceName = obj.aData.processInstanceName;
						var html = "<a href='#' class='btn' title='修改当前节点审批人' onclick='edit(\""
								+ id
								+ "\",\""
								+ processInstanceName
								+ "\");'><img src='"
								+ springUrl
								+ "/static/common/img/edit.png' alt='修改当前节点审批人'></a>&nbsp;";
						return html;
					}
				} ],
		btns : [ ]
	};
	PMTable = Table.dataTable("processMaintain_table", pmTable_options);
	 $('#search').click(function () {
         Table.render(PMTable);
     });
});
/**
 * 弹出修改页面 --修改当前流程实例的审批人
 * @param id
 * @param processInstanceName
 */
function edit(id,processInstanceName){ 
    Dialog.openRemote("流程实例【"+processInstanceName+"】修改当前节点审批人",springUrl+"/system/processMaintain_edit?id="+id, 1000,600,
        [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function() {}
            }
        ]);
}
