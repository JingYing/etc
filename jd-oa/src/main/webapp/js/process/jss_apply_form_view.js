function fmoney2(s, n) //s:传入的float数字 ，n:希望返回小数点几位
{ 
	if(s==undefined || !s || s=='') return '';
	 n = n > 0 && n <= 20 ? n : 2; 
	 s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
	 var l = s.split(".")[0].split("").reverse(), 
	 r = s.split(".")[1]; 
	 t = ""; 
	 for(i = 0; i < l.length; i ++ ) 
	 { 
	    t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "" : ""); 
	 } 
	 return t.split("").reverse().join("") + "." + r; 
}

function fmoney(s, n) //s:传入的float数字 ，n:希望返回小数点几位
{ 
	if(s==undefined || !s || s=='') return '';
	 n = n > 0 && n <= 20 ? n : 2; 
	 s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + ""; 
	 var l = s.split(".")[0].split("").reverse(), 
	 r = s.split(".")[1]; 
	 t = ""; 
	 for(i = 0; i < l.length; i ++ ) 
	 { 
	    t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : ""); 
	 } 
	 return t.split("").reverse().join("") + "." + r; 
}



function rmoney(s) 
{  
	if(s==undefined || !s || s=='') return '';
    return parseFloat((s + "").replace(/[^\d\.-]/g, "")); 
}


$(function () {
    $('#close').click(function () {
        window.opener = null;
        self.close();
    });
    $.extend($.fn.datagrid.defaults.editors, {
        datetimebox: {//datetimebox就是你要自定义editor的名称
            init: function (container, options) {
                var input = $('<input class="easyuidatetimebox" style="width:' + container.width() + '">').appendTo(container);
                var obj = input.datetimebox({
                    formatter: function (date) {
                        return new Date(date).formatDate("yyyy-MM-dd hh:mm:ss");
                    },
                    parser: function (s) {
                        if (!s) return new Date();
                        s = new Date(s).formatDate("yyyy-MM-dd hh:mm:ss");
                        var ss = s.split('-');
                        var y = parseInt(ss[0], 10);
                        var m = parseInt(ss[1], 10);
                        var d = parseInt(ss[2], 10);
                        if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
                            return new Date(y, m - 1, d);
                        } else {
                            return new Date();
                        }
                    }
                });
                return obj;
            },
            getValue: function (target) {
                return $(target).parent().find('input.combo-value').val();
            },
            setValue: function (target, value) {
                $(target).datetimebox("setValue", value);
            },
            resize: function (target, width) {
                var input = $(target);
                input.parent().find('span:first').width(width);
                input.parent().find('span').find('input').width(width - 20);
                if ($.boxModel == true) {
                    input.width(width - (input.outerWidth() - input.width()));
                } else {
                    input.width(width);
                }
            }
        }
    });
    //数据字典扩展
    $.extend($.fn.datagrid.defaults.editors, {
    	dictionary: {//dictionary就是你要自定义editor的名称
            init: function (container, options) {
                var input = $('<input class="easyui-searchbox" style="width:' + container.width() + '">').appendTo(container);
                var obj = input.searchbox({
                	 searcher:function(value,name){
                		 openDictionary1(options.dataSourceId,options.dataSourceName,options.formItemId,true,value,options.formItemName);
                	 }
                });
                return obj;
            },
            getValue: function (target) {
                return $(target).parent().find('input.searchbox-text').val();
            },
            setValue: function (target, value) {
                $(target).searchbox("setValue", value);
            },
            resize: function (target, width) {
                var input = $(target);
                input.parent().find('span:first').width(width);
                input.parent().find('span').find('input').width(width - 20);
                if ($.boxModel == true) {
                    input.width(width - (input.outerWidth() - input.width()));
                } else {
                    input.width(width);
                }
            }
        }
    });
   
    //附件扩展
    $.extend($.fn.datagrid.defaults.editors, {
    	file: { 
            init: function (container, options) {            	
                $('<div style="margin:0 auto;padding:0 auto;text-align:center;width:' + container.width() + '"><img style="cursor:pointer" onClick="upfileOfSubSheet(\''+options.tableName+'\',\'false\');return false;" title="点击进行附件操作" border="0" src="/static/fileupload/image/file/attach1.gif"></div>').appendTo(container);
                //首先要判断一下该条记录ID是否为空，如果不空，方才允许上传，如果为空，提示先保存
                //return obj;
            },
            getValue: function (target) {
               // return $(target).parent().find('input.searchbox-text').val();
            },
            setValue: function (target, value) {
              //  $(target).searchbox("setValue", value);
            },
            resize: function (target, width) {
//                var input = $(target);
//                input.parent().find('span:first').width(width);
//                input.parent().find('span').find('input').width(width - 20);
//                if ($.boxModel == true) {
//                    input.width(width - (input.outerWidth() - input.width()));
//                } else {
//                    input.width(width);
//                }
            }
        }
    });
});

function upfileOfSubSheet(tableName,isRead){
	//springUrl
	var dialogId = tableName + '_dialog';
	var currentRow = $('#'+tableName).datagrid('getSelected');	
	var id = currentRow.ID;
	$('body').append("<div id='" + dialogId + "'></div>");
	if (isRead=='false') {
		$('#' + dialogId).dialog({
			title: '子表附件列表-对话框',
			width: 400,
			maximizable : false,
			closable : false,
			collapsible : false,
			minimizable : false,
			resizable : false,
			cache : false,
			modal: true,
			shadow: false,
			content:"<iframe src=" + springUrl + "'/app/process_FlexUpFileList?id=" + id + "&isRead=" + isRead + "&j=" + Math.random() + "' scrolling='no' id='flexUpfileList' name='flexUpfileList' width=100% height=150 frameBorder=0></iframe>",
			closed: false,
			height: 240,
			buttons: [{
				text:'添加文件',
				handler:function(fieldName){
					upfile(fieldName);
				}
			},{
				text:'关闭',
				handler:function(){
					$('#' + dialogId).dialog('close');
					//$('#' + dialogId).remove();
				}
			}]
		}).dialog('open');
	} else {
		$('#' + dialogId).dialog({
			title: '子表附件列表-对话框',
			width: 400,
			maximizable : false,
			closable : false,
			collapsible : false,
			minimizable : false,
			resizable : false,
			cache : false,
			modal: true,
			shadow: false,
			content:"<iframe src=" + springUrl + "'/app/process_FlexUpFileList?id=" + id + "&isRead=" + isRead + "&j=" + Math.random() + "' scrolling='no' id='flexUpfileList' name='flexUpfileList' width=100% height=150 frameBorder=0></iframe>",
			closed: false,
			height: 240,
			buttons: [{
				text:'关闭',
				handler:function(){
					$('#' + dialogId).dialog('close');
					//$('#' + dialogId).remove();
				}
			}]
		}).dialog('open');
	}
	
}

/**
 * 
 * @param tableName
 * @param attachmentId
 */
function upfileOfSubSheetSet(tableName,attachmentId){
	//springUrl
	var dialogId = tableName + '_dialog';
	var currentRow = $('#'+tableName).datagrid('getSelected');	
	
}

/**
 * 仅仅保存业务数据，供审批任务使用
 * @return
 */
function saveBusinessData() {
    if (!checkValue('3')) {
        return false;
    }
    if(!checkRowsAdd()){//判断子表是否有数据
	     return false;
	}
    var jsonDatas = getBindValue();
    var valueDatas = getDatas(jsonDatas);
    $('#processInstanceStatus').val(1);
    if (typeof(valueDatas) != 'undefined') {
        return saveAjaxBusinessData(valueDatas, "0");
    }

}
function saveBusinessDataNoCheckValue(){
    var jsonDatas = getBindValue();
    var valueDatas = getDatas(jsonDatas);
    $('#processInstanceStatus').val(1);
    if (typeof(valueDatas) != 'undefined') {
        return saveAjaxBusinessData(valueDatas, "0");
    }
}


/**
 * 保存业务数据，并创建流程实例
 * @return
 */
function submitProcess(modal) {
	parent.$('#saveDraftButton').attr('disabled',true);//btn_loading
	
    if (!checkValue('1')) {
    	parent.$('#saveDraftButton').attr('disabled',false);//btn_loading
        return false;
    }
    if(!checkRowsAdd()){//判断子表是否有数据
		 parent.$('#saveDraftButton').attr('disabled',false);//btn_loading
	     return false;
	}
   
    if(confirm("提交后将无法修改，确定提交申请？")){
    	var jsonDatas = getBindValue();
	    var valueDatas = getDatas(jsonDatas);
	    $('#processInstanceStatus').val(1);
	    if (typeof(valueDatas) != 'undefined') {
	    	 saveAjaxBusinessData(valueDatas, "1",modal);
	    }
    }
//    parent.Dialog.confirm("系统消息", "", "确定", "取消", function (result) {//提交前确认
//    	if(result){
//    		
//    	}
//    });
}
function submitNoValidate(flag,modal){
    parent.$('#saveDraftButton').attr('disabled',true);
    var jsonDatas = getBindValue();
    var valueDatas = getDatas(jsonDatas);
    $('#processInstanceStatus').val(1);
    if (typeof(valueDatas) != 'undefined') {
        saveAjaxBusinessData(valueDatas, flag,modal);
    }
}
/**
 * 保存草稿
 * @return
 */
function saveDraft(modal) {
	parent.$('#saveDraftButton').attr('disabled',true);//btn_loading
    if (!checkValue('0')) {
    	parent.$('#saveDraftButton').attr('disabled',false);
        return ;
    }
    $('#processInstanceStatus').val(0);//草稿状态
    var jsonDatas = getBindValue();
    var valueDatas = getDatas(jsonDatas);
    if (typeof(valueDatas) != 'undefined') {
//       return saveAjaxBusinessData(valueDatas, "1");
       saveAjaxBusinessData(valueDatas, "1",modal);
    } else {
    	return false;
    }
}

/**
 * 保存业务数据
 * @param jsonData
 * @param isSubmitProcess 是否提交流程
 */
function saveAjaxBusinessData(jsonData, isStart,modal) {
	var checkSub = true;
	var check = true;
	var isAsync = typeof(modal) != 'undefined';
    $.ajax({
        type: "POST",
        url: springUrl + "/app/processApp_save_bindReport",
        async: isAsync,
        data: "boTableName=" + $('#businessTableName').val() + "&businessObjectId=" + $('#businessObjectId').val() + "&datas=" + encodeURIComponent(JSON.stringify(jsonData)).toString() + '&formId=' + $('#formId').val() + '&processNodeId=' + $('#processNodeId').val(),
        success: function (businessObjectId) {
            $('#businessObjectId').val(businessObjectId);
            saveFileAttachment(businessObjectId);//更新附件表
            if (businessObjectId != '') {
                if ($("#subSheetStr").length > 0) {//保存子表
                    var subSheetNameArray = $("#subSheetStr").val().split(',');
                    for (var i = 0; i < subSheetNameArray.length; i++) {
                        var subSheetName = subSheetNameArray[i];
                        if (subSheetName != '') {
                            if (!subSheetSave(subSheetName)) {
                            	checkSub = false;
                            	break;
                            }                            	
                        }
                    }
                }
                if (checkSub && isStart == "1") {
                    if ($('#processInstanceStatus').val() != 0) {//提交流程
//                        check = startProcessTask(businessObjectId);//启动流程
                        startProcessTask(businessObjectId,modal);//启动流程
                    } else {//否则保存草稿
                        //					如果processInstanceKey 存在，不再保存草稿
                        saveDraftProcessInstance(businessObjectId,modal);
//                        if ($('#processInstanceKey').val() == '') {
//                            saveDraftProcessInstance(businessObjectId,modal);
//                        } else {
//                            parent.Dialog.alert("提示", "【" + $("#processInstanceName").val() + "】保存草稿成功!");
//                            parent.Dialog.hideModal(modal);
//                        }
                    }
                }
                parent.$('#saveDraftButton').attr('disabled',false);//btn_loading
            }
        },
        error: function (msg) {
        	parent.$('#saveDraftButton').attr('disabled',false);
            alert("【" + $("#processInstanceName").val() + "】业务数据保存失败!");
//            check = false;
            check = false;
        }
    });
    return check;
}
/**
 * 更新附件表
 * @param businessObjectId
 * @param fieldName
 */
function updateFileAttachment(businessObjectId,fieldName,attachmentId){
	$.ajax({
        type: "POST",
        url: springUrl + "/app/process_uploadFile_updatefileAttachment",
        async: false,
        data: "businessObjectId=" + businessObjectId + "&fieldName=" + fieldName + "&attachmentId=" + attachmentId,
        success: function (data) {
            
        },
        error: function (msg) {
           alert("附件更新失败!");
        }
    });
}
/**
 * 保存草稿
 * @param businessObjectId
 */
function saveDraftProcessInstance(businessObjectId,modal) {
	var check = true;
    jQuery.ajax({
        type: "POST",
        cache: false,
        async: true,
        url: springUrl + "/app/processApp_saveDraft",
        data: {
            processDefinitionKey: $("#processDefinitionKey").val(),
            businessObjectId: businessObjectId,
            processInstanceName: $("#processInstanceName").val(),
            followCode: $("#followCode").val(),
            taskPriority: getRadioValue('taskPriority'),
            boTableName: $("#businessTableName").val()
        },
        success: function (msg) {//为了解决IE下不弹窗问题  勉强用这种方法替代
            if(msg == null || msg ==''){
                alert("【" + $("#processInstanceName").val() + "】保存草稿失敗!");
                return;
            }
            if(msg != '1'){
                $('#processInstanceKey').val(msg);
            }
        	parent.$('#saveDraftButton').attr('disabled',true);
           alert("【" + $("#processInstanceName").val() + "】保存草稿成功!");
//            setTimeout(function () {
//                parent.Dialog.hide();
//            }, 2000);
//           try{
//        	   parent.window.frames['mainFrame'].refershTable();
//           }catch(e){}
//           try{
//        	   parent.window.frames['mainFrame'].load('apply', springUrl + '/app/processApply_index', true)
//           }catch(e){}
           //parent.Dialog.hideModal(modal);
        },
        error: function (data) {
        	parent.$('#saveDraftButton').attr('disabled',true);
            alert("保存草稿失败");
            check = false;
        }
    });
    return check;
}
/**
 * 启动流程实例
 * @param businessObjectId
 */
function startProcessTask(businessObjectId,modal) {
	var check = true;
    jQuery.ajax({
        type: "POST",
        cache: false,
        async: true,
        dataType: 'json',
        url: springUrl + "/app/processApp_submitInstance",
        data: {
            processDefinitionKey: $("#processDefinitionKey").val(),
            businessObjectId: businessObjectId,
            processInstanceName: $("#processInstanceName").val(),
            followCode: $("#followCode").val(),
            taskPriority: getRadioValue('taskPriority'),
            nodeId: $("#processNodeId").val(),
            boTableName: $("#businessTableName").val(),
            formId: $("#formId").val(),
            processInstanceId: $('#processInstanceKey').val()
        },
        success: function (data) {
           alert("【" + $("#processInstanceName").val() + "】流程任务创建成功!");

            try{
               $(".modal-footer").hide();
            }catch(e){}
            try{
               parent.$(".modal-footer").hide();
            }catch(e){}
            try{
         	   parent.window.frames['mainFrame'].refershTable();
            }catch(e){}
            try{
                parent.parent.window.frames['mainFrame'].refershTable();
            }catch(e){}
            try{
                $('#submitApplyInfoDiv').dialog('close');
                window.close();
            }catch(e){}
            try{
                parent.window.frames['mainFrame'].load('apply', springUrl + '/app/processApply_index', true);
            }catch(e){}
            try{
               parent.parent.window.frames['mainFrame'].load('apply', springUrl + '/app/processApply_index', true);
            }catch(e){}
            try{
                Dialog.hideModal(modal);
            }catch(e){}
           try{
               parent.Dialog.hideModal(modal);
           }catch(e){}
            try{
                parent.parent.Dialog.hideModal(modal);
            }catch(e){}
            try{
                parent.parent.parent.Dialog.hideModal(modal);
            }catch(e){}
            try{
                parent.parent.Dialog.hideAll();
            }catch(e){}
        },
        error: function (data) {
            alert("【" + $("#processInstanceName").val() + "】流程任务创建失败!");
            saveDraftProcessInstance(businessObjectId);
        }
    });
    return check;
}
/**
 * 获取表单填写的内容值
 * @return
 */
function getDatas() {
    var jsonDatas = getBindValue();
    for (var i = 0; i < jsonDatas.length; i++) {
        var jsonData = jsonDatas[i];
        var value = '';
        var fieldType = jsonData.fieldType;
        var fieldInputType = jsonData.fieldInputType;
        if (fieldType == 'DATE') {
            value = getDateValue(jsonData.fieldName);
        } else if (fieldType == 'DATETIME') {
            value = getDateTimeValue(jsonData.fieldName);
        } else if (fieldType == 'NUMBER') {
            if (fieldInputType == '复选框') {
                value = getCheckboxValue(jsonData.fieldName);
            } else if (fieldInputType == '单选按纽组') {
                value = getRadioValue(jsonData.fieldName);
            } else if (fieldInputType == '列表') {
                value = getSelectValue(jsonData.fieldName);
            } else if (fieldInputType == '多行') {
                value = getTextAreaValue(jsonData.fieldName);
            } else if (fieldInputType == '单行' || fieldInputType == '附件') {//单行
                value = getInputValue(jsonData.fieldName);
            }
            value = rmoney(value);
            if (value == '') {
                value = 0;
            }
            if (!IsNumber(value)) {
                return;
            }
        } else {
            if (fieldInputType == '复选框') {
                value = getCheckboxValue(jsonData.fieldName);
            } else if (fieldInputType == '单选按纽组') {
                value = getRadioValue(jsonData.fieldName);
            } else if (fieldInputType == '列表') {
                value = getSelectValue(jsonData.fieldName);
            } else if (fieldInputType == '多行') {
                value = getTextAreaValue(jsonData.fieldName);
            } else if (fieldInputType == '单行' || fieldInputType == '附件') {//单行
                value = getInputValue(jsonData.fieldName);
            }
        }
        if (typeof(value) == 'undefined') {
            value = '';
        }
        jsonData.value = value;
    }
    return jsonDatas;
}
function getInputValue(fieldId) {
    return $('#' + fieldId).val();
}
function getDateValue(fieldId) {
    try {
        return $("#" + fieldId).datebox("getValue");
    } catch (e) {
        return getInputValue(fieldId);
    }
}
function getDateTimeValue(fieldId) {
    try {
        return $("#" + fieldId).datetimebox("getValue");
    } catch (e) {
        return getInputValue(fieldId);
    }
}
function getSelectValue(fieldId) {
    var obj = document.getElementById(fieldId);
    if(obj == null || typeof(obj) == 'undefined'){
    	return '';
    }
    return obj.value;
}
function getTextAreaValue(fieldId) {
    return $('#' + fieldId).val();
}
function getRadioValue(fieldId) {
    var v = "";
    var r = document.getElementsByName(fieldId);
    for (var i = 0; i < r.length; i++) {
        if (r[i].checked) {
            v = r[i].value;
            break;
        }
    }
    return v;
}
function getCheckboxValue(fieldId) {
    var v = "";
    var r = document.getElementsByName(fieldId);
    if (typeof r.length == 'undefined') {
        v = r.value;
    } else {
        for (var i = 0; i < r.length; i++) {
            if (r[i].checked) {
                if (v == "") {
                    v = r[i].value;
                } else {
                    v = v + '|' + r[i].value;
                }
            }
        }
    }
    return v;
}
//判断一个串实际长度（1个中文2个单位）
function length2(txtValue) {
    var cArr = txtValue.match(/[^\x00-\xff]/ig);
    return txtValue.length + (cArr == null ? 0 : cArr.length);
}
/**
 IsNumber: 用于判断一个数字型字符串是否为数值型，
 还可判断是否是正数或负数，返回值为true或false
 string: 需要判断的字符串
 sign: 若要判断是正负数是使用，是正用'+'，负'-'，不用则表示不作判断
 */
function IsNumber(string, sign) {
    var number;
    if (string == null) {
        return false;
    }
    if ((sign != null) && (sign != '-') && (sign != '+')) {
        alert('IsNumber(string,sign)的参数出错：\nsign为null或"-"或"+"');
        return false;
    }
    number = new Number(rmoney(string));
    if (isNaN(number)) {
        return false;
    } else if ((sign == null) || (sign == '-' && number < 0) || (sign == '+' && number > 0)) {
        return true;
    } else {
        return false;
    }
}
/**
 IsDate: 用于判断一个字符串是否是日期格式的字符串

 返回值：
 true或false

 参数：
 DateString： 需要判断的字符串
 Dilimeter ： 日期的分隔符，缺省值为'-'
 Author: PPDJ
 sample:
 var date = '1999-1-2';
 if (IsDate(date))
 {
 alert('You see, the default separator is "-");
 }
 date = '1999/1/2";
 if (IsDate(date,'/'))
 {
 alert('The date\'s separator is "/");
 }
 */
//IsDate('2003-09-09');
function IsDate(DateString, Dilimeter) {
    if (DateString == null || DateString == '') {
        return true;
    }
    if (Dilimeter == '' || Dilimeter == null) {
        Dilimeter = '-';
    }
    var tempy = '';
    var tempm = '';
    var tempd = '';
    var tempArray;
    if (DateString.length < 8 && DateString.length > 10) {
        return false;
    }
    tempArray = DateString.split(Dilimeter);
    if (tempArray.length != 3) {
        return false;
    }
    if (tempArray[0].length == 4) {
        tempy = tempArray[0];
        tempd = tempArray[2];
    } else {
        tempy = tempArray[2];
        tempd = tempArray[1];
    }
    tempm = tempArray[1];
    var tDateString = tempy + '/' + tempm + '/' + tempd + ' 8:0:0';//加八小时是因为我们处于东八区
    var tempDate = new Date(tDateString);
    if (tempd.substring(0, 1) == '0') {
        tempd = tempd.substring(1);
    }
    if (tempm.substring(0, 1) == '0') {
        tempm = tempm.substring(1);
    }

    if (isNaN(tempDate))
        return false;
    if (((tempDate.getUTCFullYear()).toString() == tempy) && (tempDate.getMonth() == parseInt(tempm) - 1) && (tempDate.getDate() == parseInt(tempd))) {
        return true;
    } else {
        return false;
    }
}
//判断日期类型是否为YYYY-MM-DD hh:mm:ss格式的类型    
function IsDateTime(DatetimeString) {
    if (DatetimeString.length != 0) {
        var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
        var r = DatetimeString.match(reg);
        if (r == null) {
            return false;
        } else {
            return true;
        }
    }
    return true;
}

//===============子表=======================
/**
 * 子表数据 删除
 */
function subSheetDelete(obj) {
	 var row = $('#' + obj).datagrid('getSelected');
	    if (row) {
	        var index = $('#' + obj).datagrid('getRowIndex', row);
	        $.ajax({
	            type: "POST",
	            url: springUrl + "/app/processApp_delete_bindReport_data",
	            async: false,
	            data: "boTableName=" + obj + "&id=" + row.ID,
	            success: function (businessObjectId) {
	                $('#' + obj).datagrid('deleteRow', index);
	                setTimeout(function(){
	                	try{
		                	updateComputeExpress(obj);//触发列规则计算
		                }catch(e){
		                	
		                }
	                },1000);
	            },
	            error: function (msg) {
	                alert('业务数据删除失败!');
	                return false;
	            }
	        });
	    }
	
    return;
}
/**
 * 子表 保存
 * @param obj
 */
function subSheetSave(obj) {
    //$('#businessObjectId').val('12345678');
    if ($('#businessObjectId').val() == '') {
        alert('请先保存主表数据！');
        return false;
    } else {
        var datagrid = $('#' + obj);
        if (datagrid.length == 0) {
            return true;
        }
        endEdit(datagrid);
        var rows = datagrid.datagrid('getChanges');
        if (rows.length > 0) {
            $.ajax({
                type: "POST",
                url: springUrl + "/app/processApp_save_bindReport_s",
                async: false,
                data: "boTableName=" + obj + "&businessObjectId=" + $('#businessObjectId').val() + "&datas=" + encodeURIComponent(JSON.stringify(rows)) + '&formId=' + $('#formId').val() + '&processNodeId=' + $('#processNodeId').val(),
                success: function (businessObjectId) {
                    $('#' + obj).datagrid('acceptChanges');
                    $('#' + obj).datagrid({'url':springUrl + '/app/processApp_get_bindReportDataJson?boTableName='+obj+'&businessObjectId=' + $('#businessObjectId').val()});
                	subSheetRefresh(obj);
                    if ($('#businessObjectId').val() == null || $('#businessObjectId').val() == '' || $('#businessObjectId').val() == '0') {

                    }
                },
                error: function (msg) {
                    alert('业务数据保存失败!');
                    return false;
                }
            });
        }
        return true;
    }
}
//汇总确定按钮
function subSheetSum(obj){
	var datagrid = $('#' + obj);
    if (datagrid.length == 0) {
        return true;
    }
    endEdit(datagrid);
}
/**
 * 子表数据非空校验
 */
function checkSubSheetValue() {
	//为了解决easyui datagrid edit 最后一个单元格数据无法回添，保存
	try{
		document.getElementById('processInstanceName').focus();
	}catch(e){
		
	}
    var subSheetNameArray = $("#subSheetStr").val().split(',');
    for (var i = 0; i < subSheetNameArray.length; i++) {
        var obj = subSheetNameArray[i];
        if (obj != '') {
            var datagird = $('#' + obj);
            if (datagird.length == 0) {
                return true;
            }
            endEdit(datagird);
            var rows = datagird.datagrid('getRows');
            for (var k = 0; k < rows.length; k++) {//遍历修改过的行
                var row = rows[k];
                var index = datagird.datagrid('getRowIndex', row);
                datagird.datagrid('endEdit', index);
                var columnFields = datagird.datagrid('getColumnFields');
                for (var j = 0; j < columnFields.length; j++) {//遍历所有的字段
                    var columnField = columnFields[j];
                    var columnOption = datagird.datagrid('getColumnOption', columnField);//获取列的option属性
                    if (columnOption.require) {
                    	var value = row[columnField];
                        if (typeof(value) == 'undefined' || value.length == 0) {
                            alert('请给字段:[' + subStr(columnOption.title) + ']输入一个值，该字段值不允许为空！');
                            return false;
                        }
                    }
                }
            }
        }
    }
    try{
        if(checkSubSheetValueCallback){
            return checkSubSheetValueCallback();
        }
    }catch(e){
    }
    return true;
}
function endEdit(objDatagrid) {
    var rows = objDatagrid.datagrid('getRows');
    for (var i = 0; i < rows.length; i++) {
        objDatagrid.datagrid('endEdit', i);
    }
}
/**
 * 栽取字符串(<font color='red'>*</font>))
 * @param title
 * @returns
 */
function subStr(title) {
    if (title.indexOf("<font color='red'>*</font>") != -1) {
        return title.substr(0, title.indexOf("<font color='red'>*</font>"));
    } else {
        return title;
    }
}
/**
 * 子表刷新
 * @param obj
 */
function subSheetRefresh(obj) {
    $('#' + obj).datagrid('load');
}
//===============end子表=======================
Date.prototype.formatDate = function (format) {
    var date = this;
    if (!format) {
        format = "yyyy-MM-dd";
    }
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    var year = date.getFullYear();
    format = format.replace("MM", month.toString());
    if (format.indexOf("yyyy") > -1) {
        format = format.replace("yyyy", year.toString());
    } else if (format.indexOf("yy") > -1) {
        format = format.replace("yy", year.toString().substr(2, 2));
    }
    var day = date.getDate();
    if (day < 10) {
        day = "0" + day;
    }
    format = format.replace("dd", day.toString());
    var hours = date.getHours();
    if (format.indexOf("t") > -1) {
        if (hours > 11) {
            format = format.replace("t", "pm");
        } else {
            format = format.replace("t", "am");
        }
    }
    if (format.indexOf("HH") > -1) {
        format = format.replace("HH", hours.toString());
    }
    if (format.indexOf("hh") > -1) {
        if (hours > 12) {
            hours - 12;
        }
        if (hours == 0) {
            hours = 12;
        }
        format = format.replace("hh", hours.toString());
    }
    var minutes = date.getMinutes();
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (format.indexOf("mm") > -1) {
        format = format.replace("mm", minutes.toString());
    }
    var seconds = date.getSeconds();
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    if (format.indexOf("ss") > -1) {
        format = format.replace("ss", seconds.toString());
    }
    return format;

}
/**
 * 文件上传
 */
function upfile(fieldName) {
    var frameObj = document.getElementById("flexUpfileList");
	if (frameObj != null) {
        frameObj =  $(document.getElementById("flexUpfileList").contentWindow.document.body);
		var businessId = $(frameObj).find("#businessId").val();
		if (businessId!="undefined" && businessId!=null && businessId!="") {
					
		} else {
			alert("请先保存子表数据后再上传附件。");
			return false;
		}
	}
	upfileAction(fieldName);
}
var wind;
function upfileAction(fieldName) {
    wind = new FlexWindow(fieldName);
    wind.open();
}
/**
 * 上传成功后，服务器返回处理
 * @param uploadAttachmentId
 * @param fileName
 * @param fieldName
 */
function returnUploadSucess(uploadAttachmentId,fileName,fieldName){
    var frameObj = document.getElementById("flexUpfileList");
    if (frameObj != null) {
		returnUploadSucessSub(uploadAttachmentId,fileName,fieldName);	
	} else {
		var upfilevalue = $('#' + fieldName).val();
		if(upfilevalue.indexOf(uploadAttachmentId) == -1){
			if(upfilevalue == ''){
				$('#' + fieldName).val(uploadAttachmentId);
			} else {
				$('#' + fieldName).val(upfilevalue + '@@@@' + uploadAttachmentId);
			}
			var context = "<tr id='" + uploadAttachmentId + "'>" +
					"<td width='100%'><a href=\"javascript:void(0);\" onClick=\"downloadfile('" + uploadAttachmentId + "')\">"+ fileName +"</a></td>" +
					"<td width='20' nowrap><a onclick=\"deletefile('" + uploadAttachmentId + "','" + fileName + "','" + fieldName + "');return false;\" href=\"javascript:void(0);\">删除</a></td>" +
					"</tr>";
			$('#' + fieldName + '_flexUpload').append(context);
		}
	}	
}

function returnUploadSucessSub(uploadAttachmentId,fileName,fieldName){	
	var frameObj = $(document.getElementById("flexUpfileList").contentWindow.document.body);
	var fieldNameObj = $(frameObj).find('#fieldName');
	var upfilevalue = $(fieldNameObj).val();
	if(upfilevalue.indexOf(uploadAttachmentId) == -1){
		if(upfilevalue == ''){
			$(fieldNameObj).val(uploadAttachmentId);
		} else {
			$(fieldNameObj).val(upfilevalue + '@@@@' + uploadAttachmentId);
		}
		
		var businessId = $(frameObj).find("#businessId").val();
		var attachmentId = $(fieldNameObj).val();
		if (businessId!="undefined" && businessId!=null && businessId!="") {
			//上传附件
			updateFileAttachment(businessId,fieldName,attachmentId);
			//设置附件id到业务表
			//upfileOfSubSheetSet(,attachmentId);
			//刷新附件列表
			returnUploadSucessLoad();
		}			
	}
}


/**
 * 查看附件列表
 * @param uploadAttachmentId
 * @param fileName
 * @param fieldName
 */
function returnUploadSucessLoad(){	
	var frameObj = $(document.getElementById("flexUpfileList").contentWindow.document.body);
	var businessId = $(frameObj).find("#businessId").val();
	var isRead = $(frameObj).find("#isRead").val();
	if (businessId!="undefined" && businessId!=null && businessId!="") {
		$.ajax({
	        type: "GET",
	        url: springUrl + "/app/process_getAttachmentList",
	        async: false,  
	        data: "businessId=" + businessId + "&isRead=" + isRead,
	        success: function (context) {
	        	var obj = $(frameObj).find("#fileList");
	        	$(obj).empty();
	        	$(obj).append(context.html);
	        	var fieldNameObj = $(frameObj).find('#fieldName');
	        	$(fieldNameObj).val(context.upfile);
	        },
	        error: function (msg) {
	            return false;
	        }
	    });
	}
}

function deletefile(uploadAttachmentId,fileName,fieldName){
	if(window.confirm("确定要删除【" + fileName + "】附件吗?")){
		 $.ajax({
	         type: "GET",
	         url: springUrl + "/app/process_deleteFile",
	         async: false,
	         data: "attachmentId=" + uploadAttachmentId,
	         success: function (businessObjectId) {
	        	var frameObj = document.getElementById("flexUpfileList");
	        	if (frameObj!=null) {
        			returnUploadSucessSub(uploadAttachmentId,fileName,fieldName);	
	        		} else {
		             $('#' + uploadAttachmentId).remove();
		             //处理业务附件值
		             var files = $('#' + fieldName).val();
		             if(files.indexOf('@@@@')){
		            	 var filesArray = files.split('@@@@');
		            	 for(var i = 0 ; i < filesArray.length; i++){
		            		 var fileId = filesArray[i];
		            		 if(fileId == uploadAttachmentId){
		            			 filesArray.remove(i);
		            		 }
		            	 }
		            	 var newFiels = '';
		            	 for(var i = 0 ; i < filesArray.length; i++){
		            		 if(newFiels == ''){
		            			 newFiels = filesArray[i];
		            		 }else {
		            			 newFiels += '@@@@' + filesArray[i];
		            		 }
		            		 
		            	 }
		            	 $('#' + fieldName).val(newFiels);
		             }
	        	}
	         },
	         error: function (msg) {
	             alert('文件删除失败!');
	             return false;
	         }
	     });
	}
}
Array.prototype.remove = function(dx){
  if(isNaN(dx)||dx>this.length){
	  return false;
  }
  for(var i=0,n=0;i<this.length;i++){
      if(this[i]!=this[dx]){
          this[n++]=this[i]
      }
  }
  this.length-=1;
}

/**
 * 文件上传
 */
function ajaxFileUpload(fileElementId){
	if(document.getElementById(fileElementId + '_file').value != ''){
		$.ajaxFileUpload({
			url : springUrl + '/app/process_uploadFile',
			secureuri : false,
			fileElementId : fileElementId + '_file',
			dataType : 'json',//返回值类型 一般设置为json
			success : function(data) {
				var resulstIndex = data.responseText.indexOf('<pre>') + '<pre>'.length;
				var lastIndex = data.responseText.indexOf('</pre>') - '</pre>'.length + 1;
				var resulst = data.responseText.substr(resulstIndex,lastIndex);
				var resultJson = JSON.parse(resulst);
				alert('附件【' + resultJson.fileName + '】上传成功!');
				document.getElementById(fileElementId).value = resultJson.attachmentId;
			},
			error : function(data, status, e) {
				alert("上传失败");
			}
		});
	}
}
function downloadfile(id){
//	 $.ajax({
//         type: "GET",
//         url: springUrl + "/app/process_downloadfile",
//         async: false,
//         data: "id=" + id,
//         success: function (businessObjectId) {
//             
//         },
//         error: function (msg) {
//             alert('文件下载失败!');
//             return false;
//         }
//     });
	 var url = springUrl + "/app/process_downloadfile?id=" + id;
	 window.location.href = encodeURI(url);
}
function getCookie(){
	var COOKIE_NAME = 'test_cookie';
	alert($.cookie(COOKIE_NAME))
	return $.cookie(COOKIE_NAME);
}
/*打开动态数据源进行数据选择*/
function openDictionary(dataSourceId,dataSourceName,formItemId,isSubSheet,targetName){
	if(typeof(isSubSheet) == 'undefined'){
		isSubSheet = false;
	}
	// new easyui dialog
    if($('#dictionaryDialog').length == 0){
        $("<div id=\"dictionaryDialog\"> </div>").html("<iframe id='dictionaryIframe' name='dictionaryIframe' style='width: 786px; height: 325px;overflow: hidden;' frameborder='0'></iframe>").appendTo("body");
    }
    
    var likeConditionValue = '';
	if(typeof(targetName) != 'undefined'){
		likeConditionValue = document.getElementById(targetName).value;
	}
    var formId = document.getElementById('formId').value;
    var dialog =  $('#dictionaryDialog').dialog({
        title :  dataSourceName,
        width : 800,
        height:  400,
        top : 50,
        closed :  false,
        cache :  false,
//        href :
        modal:  false  ,
       buttons:[{
           text:'确定',
           handler:function(){
               try{
                   document.getElementById('dictionaryIframe').contentWindow.callBack();
               }catch(e){

               }
               $('#dictionaryDialog').dialog('close');
           }
       },{
           text:'取消',
           handler:function(){
               $('#dictionaryDialog').dialog('close');
           }
       }]
    });
    $('#dictionaryDialog').dialog('open');
    document.getElementById('dictionaryIframe').src =  encodeURI(springUrl + '/di/datasource/mapping_getBusiness?dataSourceId=' + dataSourceId + '&formId=' + formId + '&formItemId=' + formItemId + '&isSubSheet=' + isSubSheet + '&targetName=' + targetName + '&likeConditionValue=' + likeConditionValue);
}

/*打开动态数据源进行数据选择*/
function openDictionary1(dataSourceId,dataSourceName,formItemId,isSubSheet,likeConditionValue,targetName){
	if(typeof(isSubSheet) == 'undefined'){
		isSubSheet = false;
	}
	// new easyui dialog
    if($('#dictionaryDialog').length == 0){
        $("<div id=\"dictionaryDialog\"> </div>").html("<iframe id='dictionaryIframe' name='dictionaryIframe' style='width: 786px; height: 325px;overflow: hidden;' frameborder='0'></iframe>").appendTo("body");
    }
    
    var formId = document.getElementById('formId').value;
    var dialog =  $('#dictionaryDialog').dialog({
        title :  dataSourceName,
        width : 800,
        height:  400,
        top : 50,
        closed :  false,
        cache :  false,
//        href :
        modal:  false  ,
       buttons:[{
           text:'确定',
           handler:function(){
               try{
                   document.getElementById('dictionaryIframe').contentWindow.callBack();
               }catch(e){

               }
               $('#dictionaryDialog').dialog('close');
           }
       },{
           text:'取消',
           handler:function(){
               $('#dictionaryDialog').dialog('close');
           }
       }]
    });
    $('#dictionaryDialog').dialog('open');
    document.getElementById('dictionaryIframe').src =  encodeURI(springUrl + '/di/datasource/mapping_getBusiness?dataSourceId=' + dataSourceId + '&formId=' + formId + '&formItemId=' + formItemId + '&isSubSheet=' + isSubSheet + '&targetName=' + targetName + '&likeConditionValue=' + likeConditionValue);
}

/*打开附件上传组件*/
function openAttrUpload(isSubSheet){
	if(typeof(isSubSheet) == 'undefined'){
		isSubSheet = false;
	}
	var buttons = [
        {
            "label": "取消",
            "id" : "saveDraftButton",
            "class": "active1",
            "callback": function (modal) {
                return true;
            }
        },
        {
            "label": "确定",
            "id" : "submitProcessButton",
            "class": "active1",
            "callback": function (modal) {
            	
                return true;
            }
        }
    ];	
    alert("OK");
}

//radio控件点击事件
function radioClick(event){
//    console.log("radio.click.")
    var e = window.event || event ;
    var element = e.target || e.srcElement ;


    if(typeof radioClickCallback === "function"){
        var r = radioClickCallback(event);
        if(r)  {
//            $("input:radio",$(element).parent().parent()).next().removeClass('label').removeClass('label-warning');
//            $(element).next().addClass('label').addClass('label-warning');
            $("input:radio",$(element).parent().parent()).each(function(){
                if (this==element) $(this).next().addClass('label').addClass('label-warning');
                else
                    $(this).next().removeClass('label').removeClass('label-warning');
            });
        }
        return r;
    }

    else{
//        $("input:radio",$(element).parent().parent()).next().removeClass('label').removeClass('label-warning');
//        $(element).next().addClass('label').addClass('label-warning');
        $("input:radio",$(element).parent().parent()).each(function(){
            if (this==element) $(this).next().addClass('label').addClass('label-warning');
            else
                $(this).next().removeClass('label').removeClass('label-warning');
        });
        return true;
    }


}



