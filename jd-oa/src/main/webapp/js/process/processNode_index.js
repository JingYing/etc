var processNodeTable ;
var	processTypeId="";

$(function(){


//datatable属性
var options = {
		pageUrl:springUrl+"/process/processNode_view",
		useCheckbox:false,
		defaultSort:[],
		sendData:function ( sSource, aoData, fnCallback ) {
			
			//！！！！！！！！！！！！！！！！！！指定某个processId,以后要改成获取
			//！！！！！！！！！！！！！！！！！！！！！！！
			aoData.push( 
					{ "name": "processDefinitionId", "value":"1" }		);   
			
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(resp) {
	                    	tableData=resp;
	                            fnCallback(resp);
	                    }
		    });
		},
		columns: [
		            { "mDataProp": "processDefinitionId","sTitle":"流程ID","sWidth":10},
		            { "mDataProp": "nodeId","sTitle":"节点ID","sWidth":20},
		            { "mDataProp": "addsignRule","sTitle":"加签规则","sWidth":10},
//		            { "mDataProp": "modifyTime","sTitle":"更新时间",
//		            	"fnRender":function(obj){
//		            		var modifyTime = obj.aData.modifyTime;
//		            		var d = new Date(modifyTime);
//		            		return d.toLocaleDateString() + " " + d.toLocaleTimeString();	     
//		            	}
//		            },
		            {"sTitle":"属性设置","sWidth":10,
		            	"fnRender":function(obj){
						var id = obj.aData.nodeId;
						return "<a href='#' class='btn' title='加签' onclick='processNodeAddsign(\""+id+"\");'><img src='"+springUrl+"/static/common/img/edit.png' alt='加签'></a>";
		            	}
		            }
		            
		        ],
		        btns:[]
		
	
};

processNodeTable = Table.dataTable("processNode",options);




}); //end of jquery



function processNodeAddsign(id){
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){}
	},
	{
		"label": "保存",
		"class": "btn-success",
		"callback": function(){
			//获取加签结果
			
			//获取加签规则
			var addsignRule = $("input[name='processNodeRule']:checked").val();
			
			//是否同步到所有节点
			var allUpdate = $("input[name='processNodeRuleForAll']:checked").val();
			
//			console.log("rule: "+addsignRule+", allUpdate: "+allUpdate);
			
			//添加发送Ajax数据
			var ao=[];
			
			ao.push({ "name": "nodeId", "value":id });
			
			if(addsignRule!=undefined && addsignRule!=null && addsignRule!='')
				ao.push({ "name": "addsignRule", "value":addsignRule });
			
			if(allUpdate!=undefined && allUpdate!=null && allUpdate!='')
				ao.push({ "name": "allUpdate", "value":allUpdate });
			
//			console.log("ao: "+ ao);
			
			//return false;
			
			var bol = false; //process_insert(id);
			
			jQuery.ajax( {
				async: false ,
                type: "POST", 
                url:"processNode_addSignUpdate", 
                data: ao, 
                success: function(resp) {
                        if(parseInt(resp)>=1)
                        	
                        
                        	bol=true;
                }
		    });
			
			if(bol){
				Dialog.alert("提示信息","保存成功！");
				Table.render(processNodeTable);
				return true;
			}else{
				Dialog.alert("提示信息","保存失败！");
				return false;
			}
			}			     							
	}];



Dialog.openRemote('节点加签',encodeURI('processNode_addSign?nodeId='+id),550,250,buttons);
}


//增加
function process_insert(id){
	var name = $.trim($("#insert_processTypeName").val());
	var sortno =$.trim( $("#insert_sort_no").val());
	var bol = false;
	if(name==''  ||  sortno=='') return;
	
	
	var ao=[];
	ao.push(
		{ "name": "processTypeName", "value":name }
		,{ "name": "sortNo", "value":sortno}
		,{"name":"modifier","value":"wanghui"});
	
	if(id!=undefined && id!=null && id!='')
		ao.push({ "name": "processTypeId", "value":id });
	
	var bol= false;
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:baseUrl+"processTypeAjax_add", 
       // dataType: "json",   //！注意！！！这个属性写了以后，会对格式进行严格检查，返回只会走error函数
        data: ao, 
        success: function(resp) {
//        	console.log('success'+ resp);
        	if(resp=='ok') bol = true;
        },
		error: function(msg){
//			console.log(msg);
		}
	});
	

	return bol;

	
}

//删除
function processTypeDel(processTypeId){
	var ao=[];
	ao.push(
		{ "name": "processTypeId", "value":processTypeId}) ;
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:"processType_delete", 
        //dataType: "json",
        data: ao, 
        success: function(resp) {
//        	console.log('success:'+resp);
        	Dialog.alert("提示消息","删除成功！");
        },
		error: function(msg){
//			console.log(msg);
		}
	});
}

//打开修改页面，跟新增一样，但要发送ajax请求数据
function processTypeEdit(id){
	
	var buttons = [{
			"label": "取消",
			"class": "btn-success",
			"callback": function(){}
		},
		{
			"label": "保存",
			"class": "btn-success",
			"callback": function(){
				var bol = process_insert(id);
				if(bol){
					Dialog.alert("提示信息","保存成功！");
					Table.render(processTypeTable);
					return true;
				}else{
					Dialog.alert("提示信息","保存失败！");
					return false;
				}
				}			     							
		}];
	
	
	processTypeId=id;
	
	Dialog.openRemote('修改流程分类',encodeURI('processType_add?processTypeId='+processTypeId),600,250,buttons);

}



