var processTypeTable ;
var	processTypeId="";

$(function(){


//datatable属性
var options = {
		pageUrl:springUrl+"/process/processType_view",
		useCheckbox:true,
		defaultSort:[],
		sendData:function ( sSource, aoData, fnCallback ) {
			var processTypeName=$("input#processTypeName").val();
			if(processTypeName==null) processTypeName="";
			aoData.push( 
					{ "name": "processTypeName", "value":processTypeName }		);   
			
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(resp) {
	                            fnCallback(resp);
	                    }
		    });
		},
/*		columns: [
            { "mDataProp": "authExpressionName","sTitle":"名称","sClass": "my_class","sWidth":200},
			{"bSortable":false,"sTitle":"表达式","fnRender":function(obj){
				return describeExpressMap(obj.aData);
			}},
            { "sWidth":20,"sTitle":"操作","fnRender":function(obj){
				var id = obj.aData.id;
				return "<a href='#' class='btn' title='修改' onclick='getExpression4Edit(\""+id+"\");'><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>"
						   +"<a href='#' class='btn' title='删除' onclick='getExpression4Edit(\""+id+"\");'><img src='"+springUrl+"/static/common/img/del.png' alt='修改'></a>;
		}}
        ],*/
		columns: [
		            { "mDataProp": "processTypeCode","sTitle":"编号"},
		            { "mDataProp": "processTypeName","sTitle":"类别名称"},
		            { "mDataProp": "sortNo","sTitle":"显示顺序"},
		            { "mDataProp": "modifier","sTitle":"操作人","fnRender":function(obj){
		            	var modifier = obj.aData.modifier;
		            	var creator = obj.aData.creator;
//		            	console.log(modifier);
		            	if(modifier == undefined) 
		            		return creator;
		            	else
		            		return modifier;
		         
		            }},
		            { "mDataProp": "modifyTime","sTitle":"更新时间",
		            	"fnRender":function(obj){
		            		var modifyTime = obj.aData.modifyTime;
		            		var createTime = obj.aData.createTime;
		            		if(modifyTime == undefined) {
			            		var d = new Date(createTime);
			            		return d.toLocaleDateString() + " " + d.toLocaleTimeString();
		            		}else{
		            			var d = new Date(modifyTime);
			            		return d.toLocaleDateString() + " " + d.toLocaleTimeString();
		            		}
		     
		            	}
		            },
		            {"sTitle":"操作","bSortable":false,
		            	"fnRender":function(obj){
						var id = obj.aData.id;
						return "<a href='#' class='btn' title='修改' onclick='processTypeEdit(\""+id+"\");'><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>"
									+ "&nbsp;&nbsp;<a href='#' class='btn' title='删除' onclick='processTypeDel(\""+id+"\",true);'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'></a>";
		            	}
		            }
		            
		        ],
		
		
		btns:[
			{
					    "sExtends":    "text",
					    "sButtonText": "新增",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
								var buttons = [{
				     				"label": "取消",
				     				"class": "btn-success",
				     				"callback": function(){}
					     		},
					     		{
				     				"label": "保存",
				     				"class": "btn-success",
				     				"callback": function(){
				     					//if(!$("#insert_processTypeName").isValid() || !$("#insert_sort_no").isValid()){
				     					if(!validate2($('#processTypeAddForm'))){
				     						//Dialog.alert("提示信息","请按提示填写正确数据！");
				     						return false;
				     					}
				     						
				     					
					     					var bol = process_insert();
					     					if(bol=='1'){
					     						Dialog.alert("提示信息","保存成功！","确定");
					     						Table.render(processTypeTable);
					     						return true;
					     					}else if(bol =='100'){
					     						Dialog.alert("提示信息","保存成功！但该一级流程类别下有流程定义，建议将该一级类别下的流程定义修改到二级类别下，否则流程定义在前台将无法显示！");
					     						Table.render(processTypeTable);
					     						return true;
					     					}else if(bol=='0'){
					     						Dialog.alert("提示信息","保存失败！已经存在该流程类别！","确定");
					     						return false;
					     					}else{
					     						Dialog.alert("提示信息","保存失败！","确定");
					     						return false;
					     					}
				     					}			     							
					     		}];
								//该变量有可能在单条数据修改时被赋值，不置空，则会在新增时请求数据
								processTypeId="";
								Dialog.openRemote('新增流程分类',encodeURI('processType_add'),600,440,buttons);
								//return false;
					    }
			},		
			{
					    "sExtends":    "text",
					    "sButtonText": "删除",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							var ids=Table.getSelectedRowsIDs(processTypeTable);
							if(ids.length==0){
								Dialog.alert("消息提示","没有选中项","确定");
							}else{
								//Dialog.alert("消息提示",ids[0],"确定");
								processTypeDel(ids, false);
							}
					    }
			}
		]
};

processTypeTable = Table.dataTable("processType",options);

$('#processTypeSearch').click(function() {
		Table.render(processTypeTable);
}); 

$('#processTypeName').keydown(function(e) {
	if(e.keyCode == 13){
		Table.render(processTypeTable);
		return false;
	}
} );






}); //end of jquery


//增加
function process_insert(id){
	var name = $.trim($("#insert_processTypeName").val());
	var sortno =$.trim( $("#insert_sort_no").val());
	var parentId = "";
	if($("input[name=processTypeOptions]").eq(1).attr("checked") == "checked"){
		parentId = $("#insert_processTypeNameParent").val();
	}
	
	if(name==''  ||  sortno=='') return;
	var ao=[];
	ao.push(
		{ "name": "processTypeName", "value":name }
		,{ "name": "sortNo", "value":sortno}
		,{"name":"parentId","value":parentId});
	
	if($("#is_outer").prop('checked')){
		ao.push({ "name": "isOuter", "value": "1"});
		ao.push({ "name": "linkUrl", "value": $("#link_url").val()});
	}else{
		ao.push({ "name": "isOuter", "value": ""});
		ao.push({ "name": "linkUrl", "value": ""});
	}
	
	if(id!=undefined && id!=null && id!='')
		ao.push({ "name": "processTypeId", "value":id });
	
	var bol= -1;
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:baseUrl+"processTypeAjax_add", 
       // dataType: "json",   //！注意！！！这个属性写了以后，会对格式进行严格检查，返回只会走error函数
        data: ao, 
        success: function(resp) {
//        	console.log('success'+ resp);
        	 bol = resp;
        },
		error: function(msg){
//			console.log(msg);
		}
	});
	
//	Dialog.openSimple('提示','保存成功！',300,200, [{
//			"label": "确定",
//				"class": "btn-success",
//				"callback": function(){}
// 		}]);
	
	return bol;
//	Table.render(processTypeTable);
	
}

//删除,isSingle表示是单个删除还是批量
function processTypeDel(processTypeId,isSingle){
	
	Dialog.confirm("删除警告！","确定要删除选中数据吗？","确定","取消",function(result){
		if(result)
			processTypeDelCall(processTypeId,isSingle);
	});
	
	
}



function processTypeDelCall(processTypeId,isSingle){
	var ao=[];
	if(isSingle)
		ao.push(
			{ "name": "processTypeId", "value":processTypeId}) ;
	else
		ao.push(
				{ "name": "processTypeIds", "value":processTypeId}) ;
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:"processType_delete", 
        //dataType: "json",
        data: ao, 
        success: function(resp) {
//        	console.log('success:'+resp);
        	
        	if(parseInt(resp)>0){
        		Dialog.alert("提示消息","删除成功！");
        		Table.render(processTypeTable);
        	}
        	else
        		Dialog.alert("提示消息","删除失败！存在被引用的流程类别！");
        	
        }
	});
}

//打开修改页面，跟新增一样，但要发送ajax请求数据
function processTypeEdit(id){
	
	var buttons = [{
			"label": "取消",
			"class": "btn-success",
			"callback": function(){}
		},
		{
			"label": "保存",
			"class": "btn-success",
			"callback": function(){
				
				if(!validate2($('#processTypeAddForm'))){
						//Dialog.alert("提示信息","请按提示填写正确数据！");
						return false;
				}
				
				var bol = process_insert(id);
				if(bol=='1'){
					Dialog.alert("提示信息","保存成功！");
					Table.render(processTypeTable);
					return true;
				}else if (bol=='0'){
					Dialog.alert("提示信息","保存失败！流程类别被子流程类别或流程定义引用！");
					return false;
				}
				}			     							
		}];
	
	
	processTypeId=id;
	
	Dialog.openRemote('修改流程分类',encodeURI('processType_add?processTypeId='+processTypeId),600,440,buttons);

}
