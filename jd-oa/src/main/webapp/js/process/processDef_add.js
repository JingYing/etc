$(function(){
	getProcessDefType("processTypeId");
	$("select[name='processTypeId']").attr("onchange","getChildrenProcessType('processTypeId','childProcessTypeId')");
	getChildrenProcessType('processTypeId','childProcessTypeId');
	getOrganizationType("organizationType");
	if(processDef.id!=null){
		$("input[name='id']").val(processDef.id);
		$("input[name='processDefinitionName']").val(processDef.processDefinitionName);
		$("input[name='sortNo']").val(processDef.sortNo);
		getParentTypeId(processDef.processTypeId);
		$("select[name='organizationType']").find("option[value='"+processDef.organizationType+"']").attr("selected",true);
		$("select[name='organizationType']").attr("disabled",true);
		$("#isOuter").children("option").each(function(){
           var temp_value = $(this).val();
           if(temp_value == processDef.isOuter){
              $(this).attr("selected","selected");
           }
      });
		
		if (processDef.isOuter=="1"){
			$("#linkUrl").val(processDef.linkUrl);
			$("#linkUrlDiv").attr("style","display:block");
//			$("#orgDiv").attr("style","display:none");
		}else if(processDef.isOuter=="2"){
			
			$("#systemIdDiv").attr("style","display:block");
//			$("#orgDiv").attr("style","display:none");
		}else if(processDef.isOuter=="0"){
			$("#orgDiv").attr("style","display:block");
		}
		
	}else{
		$("#orgDiv").attr("style","display:block");
		$("#templeteDiv").show();
		getProcessTemplate();
	}
	bindValidate($('#validateForm'));
	loadSystemId();
});
//保存记录
function addProcessDef(type) {
//	var bool = false;
    var obj={};
	var id = $("input[name='id']").val();
	var processTypeId = $("select[name='childProcessTypeId']").val();
	if(processTypeId==""){
		processTypeId = $("select[name='processTypeId']").val();
	}
	var processDefinitionName = $("input[name='processDefinitionName']").val();
	var organizationType = $("select[name='organizationType']").val();
	var processDefId = $("select[name='processDefId']").val();
	var sortNo = $("input[name='sortNo']").val();
	var isOuter = $('#isOuter').val();
	var linkUrl = $('#linkUrl').val();
	var systemId = '';
	if(isOuter=='2'){
		systemId = $("select[name='systemId']").val();
	}
	
	var data = {
			"id" : id,
			"processTypeId" : processTypeId,
			"processDefinitionName" : processDefinitionName,
			"processDefId" : processDefId,
			"organizationType" : organizationType,
			"sortNo" : sortNo,
			"isOuter" : isOuter,
			"linkUrl" : linkUrl,
			"systemId" :systemId
	};
	
	Dialog.post(springUrl + "/process/processDef_addSave", data, function(
			result) {
	//	bool = result.operator;
//		Dialog.alert("操作" + result.operator, result.processId);
        obj.bool=result.operator;
        obj.message=  result.message;
        obj.processId= result.processId;
        obj.isOuter= result.isOuter;
	});
	return obj;
}
//获取流程模板
function getProcessTemplate(){
	jQuery.ajax({
		type : "post",
		async:false,
		url : springUrl + "/process/processDef_getTemplete",
		dataType : "json",
		success : function(result) {
			var options = '<option value="" onclick="changeType()">不使用模板</option>';
			$.each(result, function(i, def) {
				options += '<option value="' + def.id + '" onclick="changeType(\''+def.processTypeId+'\')">'
				+ def.processDefinitionName + '</option>';
			});
			$("select[name='processDefId']").html(options);
		},
        error:function(msg){
        	Dialog.alert("操作失败",msg.responseText);
        }
	});
}
function changeType(typeName){
	if(typeName==null || typeName ==""){
		$("select[name='processTypeId2']").hide();
	}else{
		var options="<option>"+typeName+"</option>";
		$("select[name='processTypeId2']").show();
		$("select[name='processTypeId2']").html(options);
	}
}
//根据子类型ID得到父类型ID
function getParentTypeId(id){
	var data={
		"childId":id	
	};
	jQuery.ajax({
		type : "post",
		async:false,
		url : springUrl + "/process/processParentIdByIdAjax_view",
		data:data,
		success : function(result) {
			if(result==null||result==''){
				$("select[name='processTypeId']").find("option[value='"+id+"']").attr("selected",true);
				getChildrenProcessType('processTypeId','childProcessTypeId');
			}else{
				$("select[name='processTypeId']").find("option[value='"+result+"']").attr("selected",true);
				getChildrenProcessType('processTypeId','childProcessTypeId');
				$("select[name='childProcessTypeId']").find("option[value='"+id+"']").attr("selected",true);
			}
		},
        error:function(msg){
        	Dialog.alert("操作失败",msg.responseText);
        }
	});
}


$('#isOuter').change(function() {
	var value = $('#isOuter').val();
	if (value==1){//外部流程
		$("#linkUrlDiv").attr("style","display:block");
		$("#templeteDiv").attr("style","display:none");
		$("#orgDiv").attr("style","display:none");
		$("#systemIdDiv").attr("style","display:none");
	}else if(value==2){//集成流程
		$("#linkUrlDiv").attr("style","display:none");
		$("#templeteDiv").attr("style","display:none");
		$("#orgDiv").attr("style","display:none");
		$("#systemIdDiv").attr("style","display:block");
	}else {//内部流程
		$("#systemIdDiv").attr("style","display:none");
		$("#linkUrlDiv").attr("style","display:none");
		if(processDef.id==null){
			$("#templeteDiv").attr("style","display:block");
		}
		$("#orgDiv").attr("style","display:block");
	}
}); 

/**
 * 加载系统标识
 */
function loadSystemId() {
	var dictCode = '';
	try {
		if(processDef.id!=null&&processDef.isOuter=='2'){
			dictCode = processDef.systemId;
		}
	} catch (e) {
	}
    jQuery.ajax({
        type: "post",
        cache: false,
        dataType: 'json',
        url: springUrl + "/dict/dictData_findDictDataList?dictTypeCode=processDefSystemId",
        success: function (data) {
        	if(dictCode!=''){
        		for (var i = 0; i < data.length; i++) {
        			if(dictCode==data[i].dictCode){
        				 $("<option value='" + data[i].dictCode + "' selected>" + data[i].dictName + "</option>").appendTo($("#systemId"));
        			}else{
        				 $("<option value='" + data[i].dictCode + "'>" + data[i].dictName + "</option>").appendTo($("#systemId"));
        			}
                }
        	}else{
        		for (var i = 0; i < data.length; i++) {
                    $("<option value='" + data[i].dictCode + "'>" + data[i].dictName + "</option>").appendTo($("#systemId"));
                }
        	}
            
        },
        error: function (data) {
            Dialog.alert("失败!", "加载系统标识失败");
        }
    });
}
