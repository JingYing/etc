$(function(){
	$('#configItems').blur(function() {
		validateConfigItemSize();
	});
	
	$('#configItems').focus(function() {
		$("#isConfigItemSize").html("");
	});
	
	bindValidate($('#validateForm'));
});

function validateConfigItemSize(){
	var configItems = $("input[name='configItems']").val();
	var configItem = new Array();
	configItem = configItems.split(",");
	var size = configItem.length;
	if(size > 10){
		 if(configItem[10] != ""){
			var str = "不得超过10个！";
			$("#isConfigItemSize").html(str).css("color","red"); 
		 }
	}
}

function addSettingBusinessConfig(type) {
	var obj={};
	var processDefId = $("input[name='processDefId']").val();
	var configItems = $("input[name='configItems']").val();
	var dataSourceId = $("select[name='dataAdapter']").val();
	var rejectDataSourceId = $("select[name='dataAdapter2']").val();
	var data = {
			"processDefId" : processDefId,
			"configItems" : configItems,
			"dataSourceId" : dataSourceId,
			"rejectDataSourceId" :rejectDataSourceId
	};
	Dialog.post(springUrl + "/process/processDef_addSettingBusinessConfig", data, function(
			result) {
        obj.bool=result.operator;
	});
	return obj;
}