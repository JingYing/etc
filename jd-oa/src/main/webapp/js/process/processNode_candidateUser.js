  var nodeIdSelect ;
$(function(){	
	nodeIdSelect = getUrlParam("nodeId");
	setTimeout('loadEvent()',500); 
});
String.prototype.endWith=function(str){
	if(str==null||str==""||this.length==0||str.length>this.length)
	  return false;
	if(this.substring(this.length-str.length)==str)
	  return true; 
	else
	  return false;
	return true;
}

function loadEvent(){	
	//t=1:指定节点上级 2:指定节点办理人 3:流程变量 4:特定范围权限表达式 5:自定义
	var t = $("#candidateUserType").val();
	var m = $("#managerNodeId").val();
	var c = $("#candidateUser").val();
	
	if (t=='1' || t=='2'){
		$("input[name='processNodeRule']").eq(0).click();	
		
		if (m.endWith("_assignee")) {
			$("input[name='posSetRule']").eq(0).click();	
			$("#nodeSelect").val(m.substring(0,m.indexOf("_assignee")));
		}else if (m.endWith("_manager")) {
			$("input[name='posSetRule']").eq(2).click();	
			$("#nodeSelect").val(m.substring(0,m.indexOf("_manager")));
		}else {
			$("input[name='posSetRule']").eq(1).click();	
			$("#nodeSelect").val(m);
		}		
	}
	if (t=='3'){
		$("input[name='processNodeRule']").eq(2).click();
		$("#variableSelect").val(c);		
	}
	if (t=='4'){
		$("input[name='processNodeRule']").eq(1).click();
	}
	if (t=='5'){	
		$("input[name='processNodeRule']").eq(3).click();
		$("#customUser").val(c);	
	}
}

function clickPosSetRule(obj){
	alert($(obj).checked);
	if ($(obj).checked)
		$("#levelSelect").attr("style","display:block");
	else
		$("#levelSelect").attr("style","display:none");
}

function clickManager(){
	$("#manager").attr("style","display:block");
	$("#variable").attr("style","display:none");
	$("#expression").attr("style","display:none");
	$("#custom").attr("style","display:none");	

	var tasks = window.parent.allUserTask;
	var arr = [];
	
	arr.push('<option value="">请选择</option>');
	$.each(eval(tasks),function(index,value){
		if(value.id != nodeIdSelect)
			arr.push('<option value="'+value.id+'">'+value.name+'</option>');
	});
	$("#nodeSelect").html(arr.join());
}

function clickVariable(){
	$("#manager").attr("style","display:none");
	$("#variable").attr("style","display:block");
	$("#expression").attr("style","display:none");
	$("#custom").attr("style","display:none");	
	
	var variables = "";
	var arr = [];
	var data={
    		"processDefId":window.parent.tab_processId	
    };
	Dialog.post(springUrl+"/process/processDef_getVariables",data,function(result){
		variables=JSON.stringify(result);
	});
	
	arr.push('<option value="">请选择</option>');
	$.each(eval(variables),function(index,value){
		arr.push('<option value="'+value.configItem+'">'+value.configItem+'</option>');
	});
	$("#variableSelect").html(arr.join());
}

function clickCustom(){
	$("#manager").attr("style","display:none");
	$("#variable").attr("style","display:none");
	$("#expression").attr("style","display:none");
	$("#custom").attr("style","display:block");	
}


var expressionId;
function getExpressionId(){
	return expressionId;
}
function clickExpression(){
	$("#manager").attr("style","display:none");
	$("#variable").attr("style","display:none");
	$("#custom").attr("style","display:none");	
	
	var data={
			"id":window.parent.tab_processNodeId,
			"businessType":"node"
	};
	Dialog.post(springUrl+"/process/processDef_findAuthIds",data,function(result){
		expressionId=JSON.stringify(result);
	});
	$("#expression").load(springUrl + "/auth/authExpression_select");
	$("#expression").attr("style","display:block");
}

function candidateUserSave(){
	var processNodeRule=$("input[name='processNodeRule']:checked").val();
	
	if(processNodeRule==0){
		var ao=[];
		var managerNodeIdVal = "";
		var posSetRule=$("input[name='posSetRule']:checked").val();
		var candidateUserType = "1";
		if(posSetRule==1){
			managerNodeIdVal = $("#nodeSelect").val();
			candidateUserType = "1";
		}else if(posSetRule==0){
			managerNodeIdVal = $("#nodeSelect").val()+"_assignee";
			candidateUserType = "2";
		}
				
		ao.push({ "name": "managerNodeId", "value":managerNodeIdVal});
		ao.push({ "name": "processId", "value":window.parent.tab_processId });
		ao.push({ "name": "nodeId", "value":window.parent.tab_nodeId });
		ao.push({ "name": "candidateUser", "value":""});
		ao.push({ "name": "candidateUserType", "value":candidateUserType});
		
		jQuery.ajax( {
			async: false ,
	        url:"processNode_addSignUpdate", 
	        data: ao, 
	        success: function(resp) {
	                if(parseInt(resp)>=1)              	              
	                	bol=true;
	        }
	    });
//		parent.setCandidateUsers(window.parent.tab_nodeId,"${identityMgr.getUserManager("+$("#nodeSelect").val()+")}");		
		parent.setCandidateUsers(window.parent.tab_nodeId,"${"+managerNodeIdVal+"}");
	}
	if(processNodeRule==1){
		var authIds=selectOk();
		var resIds=new Array();
		resIds.push(window.parent.tab_processNodeId);
		if(!assginAll(resIds,authIds,"node")){
			 return false;
		}	
		
		var users=getUsersByBusiness("node",window.parent.tab_processNodeId);		
		if (users != null){
			parent.setCandidateUsers(window.parent.tab_nodeId,users.usersErpid);
		}else{
			parent.setCandidateUsers(window.parent.tab_nodeId,"");
		}
		
		//删除上级经理节点、流程变量
		var ao=[];
		
		ao.push({ "name": "managerNodeId", "value":""});
		ao.push({ "name": "candidateUser", "value":""});
		ao.push({ "name": "processId", "value":window.parent.tab_processId });
		ao.push({ "name": "nodeId", "value":window.parent.tab_nodeId });
		ao.push({ "name": "candidateUserType", "value":"4"});
		
		jQuery.ajax( {
			async: false ,
	        url:"processNode_addSignUpdate", 
	        data: ao, 
	        success: function(resp) {
	                if(parseInt(resp)>=1)              	              
	                	bol=true;
	        }
	    });	
	
	}
	if(processNodeRule==2){
		var ao=[];
		
		ao.push({ "name": "managerNodeId", "value":""});
		ao.push({ "name": "processId", "value":window.parent.tab_processId });
		ao.push({ "name": "nodeId", "value":window.parent.tab_nodeId });
		ao.push({ "name": "candidateUser", "value":$("#variableSelect").val()});
		ao.push({ "name": "candidateUserType", "value":"3"});
		
		jQuery.ajax( {
			async: false ,
	        url:"processNode_addSignUpdate", 
	        data: ao, 
	        success: function(resp) {
	                if(parseInt(resp)>=1)              	              
	                	bol=true;
	        }
	    });
		parent.setCandidateUsers(window.parent.tab_nodeId,"${"+$("#variableSelect").val()+"}");
	}
	if(processNodeRule==3){
		var ao=[];
		
		ao.push({ "name": "managerNodeId", "value":""});
		ao.push({ "name": "processId", "value":window.parent.tab_processId });
		ao.push({ "name": "nodeId", "value":window.parent.tab_nodeId });
		ao.push({ "name": "candidateUser", "value":$("#customUser").val()});
		ao.push({ "name": "candidateUserType", "value":"5"});
		
		jQuery.ajax( {
			async: false ,
	        url:"processNode_addSignUpdate", 
	        data: ao, 
	        success: function(resp) {
	                if(parseInt(resp)>=1)              	              
	                	bol=true;
	        }
	    });
		parent.setCandidateUsers(window.parent.tab_nodeId,"${"+$("#customUser").val()+"}");
	}
}

function getUsersByBusiness(businessType,businessId){
	var usersMap ;
    var data={
    		"businessType":businessType,
			"businessId":businessId			
    };
	Dialog.post(springUrl+"/auth/users_by_business",data,function(result){
		usersMap=result;
	});
	return usersMap;
}

function assginAll(resIds,authIds,businessType){
	var bool=false;
    var data={
			"resIds":JSON.stringify(resIds),
			"authIds":JSON.stringify(authIds),
			"businessType":businessType
    };
	Dialog.post(springUrl+"/process/processDef_assgin",data,function(result){
			bool=result.operator;
	});
	return bool;
}


//获取url的参数
function getUrlParam(name){

	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

	var r = window.location.search.substr(1).match(reg);  //匹配目标参数

	if (r!=null) return unescape(r[2]); return null; //返回参数值

	} 
