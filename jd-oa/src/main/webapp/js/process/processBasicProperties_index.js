var processDefId;
$(function(){
	$("#topLevelCodeSelect").val(topLevelCode);
//	console.log('hello');
	processDefId=getUrlParam("processId");
//	console.log(processDefId);
	//选择表单,设置#formIdSelect和#formId
	$("#formIdSelect").click(function(){
		window.parent.formIdSelectClick();
	});
	
	//清除formId
	$("#formIdClear").click(function(){
		//console.log($("#formId").html());
		window.parent.formIdClear();
	});
	
	//配置表单的显示列
	$("#formColumnSelect").click(function(){
		window.parent.formColumnSelectClick();
	});
	
	//保存事件
	$("#process_save").click(collectAllDatas);
});


//选择表单，已经移至processDef_index.js
function formIdSelectClick(){
//	console.log('a is clicked..');
	
	//查找表单
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){}
	},
	{
		"label": "保存",
		"class": "btn-success",
		"callback": function(){
			var bol = false; //process_insert(id);
			var data = Table.getSelectedRows(table);
			

			if(data.length>1){
				Dialog.alert("提示","只能选择单个表单！");return false;}
			if(data.length<1){
				Dialog.alert("提示","请选择一个表单 ! ");return false;}
			
			$("#formIdSelect").text(data[0].formName);
			$("#formId").val(data[0].id);
			
			return true;
			}			     							
	}];
	
	Dialog.openRemote('选择表单', '/form/formSearch_index',600,500,buttons);
	
	//return false;
}


//收集所有数据
function collectAllDatas(){
	var formId = $("#formId").val();
	var isEmail = $("input[name='alertMode_email']").attr("checked");
	var isSms = $("input[name='alertMode_sms']").attr("checked");
	var isLync = $("input[name='alertMode_lync']").attr("checked");
	var isComment = $("input[name='isComment']").attr("checked");
	var isUploadAttach = $("input[name='isUploadAttach']").attr("checked");
	var isRelateDoc= $("input[name='isRelateDoc']").attr("checked");
	var isRelateProcessInstance =  $("input[name='isRelateProcessInstance']").attr("checked");
	
//	console.log(formId+"\t"+ isEmail + "\t" + isSms + "\t" +isLync + "\t" +isComment + "\t" +isUploadAttach + "\t" +isRelateDoc + "\t" +isRelateProcessInstance);
	
	var ao = [];
	
	//待修改！！！！！！！！！！！！！！！！！！！！！
	//！！！！！！！！！！！！！！！！！！！
	ao.push({ "name": "processId", "value":getUrlParam("processId") });
	//formId可以为空
	if(formId!=null  )
		ao.push({ "name": "formId", "value":formId });
	if(isEmail)
		ao.push({ "name": "isEmail", "value":"1" });
	if(isSms)
		ao.push({ "name": "isSms", "value":"1" });
	if(isLync)
		ao.push({ "name": "isLync", "value":"1" });
	if(isComment)
		ao.push({ "name": "isComment", "value":"1" });
	if(isUploadAttach)
		ao.push({ "name": "isUploadAttach", "value":"1" });
	if(isRelateDoc)
		ao.push({ "name": "isRelateDoc", "value":"1" });
	if(isRelateProcessInstance)
		ao.push({ "name": "isRelateProcessInstance", "value":"1" });
//	console.log(ao);
	
	ao.push({ "name": "topLevelCode", "value":$("#topLevelCodeSelect").val() });
	ao.push({ "name": "followCodePrefix", "value":$("#followCodePrefix").val() });

	var bol=false;
	//流程基本属性存储
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:"processBasicProperties_insert", 
        data: ao, 
        success: function(resp) {
//        	console.log(resp);
                if(parseInt(resp)==1)               	
                	bol=true;
        },error:function(msg){
            Dialog.alert("操作失败",msg.responseText);
        }
    });
	return bol;
}


function getUrlParam(name){

var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

var r = window.location.search.substr(1).match(reg);  //匹配目标参数

if (r!=null) return unescape(r[2]); return null; //返回参数值

} 