$(function(){
	getFullForms();
	if(extendButton.id!=null){
		$("input[name='id']").val(extendButton.id);
		$("input[name='buttonName']").val(extendButton.buttonName);
		$("input[name='buttonExegesis']").val(extendButton.buttonExegesis);
		$("input[name='functionName']").val(extendButton.functionName);
		$("#functionName").attr("readonly","readonly");
		$("select[name='selectFormName']").find("option[value='"+extendButton.formId+"']").attr("selected",true);
		if(extendButton.isExtendButton){
			$("#isExetendButton").attr("checked","checked");
		}
		$("input[name='orderNo']").val(extendButton.orderNo);
	}
	bindValidate($('#validateForm'));
});

//获取表单
function getFullForms(){
	var formId=$("input[id='formId']").val();
	jQuery.ajax({
		type : "post",
		async:false,
		url : springUrl + "/process/processDef_getFullForms",
		dataType : "json",
		data: {"formId":formId},
		success : function(result) {
			var options = '<option value="" ">请选择</option>';
			$.each(result, function(i, def) {
				options += '<option value="' +def.id + '" >'
				+ def.formName + '</option>';
			});
			$("select[name='selectFormName']").html(options);
		},
        error:function(msg){
        	Dialog.alert("操作失败",msg.responseText);
        }
	});
}

function addDynamicButton(){
	var obj={};
	var id = $("input[name='id']").val();
	var buttonName = $("#buttonName").val();
	var buttonExegesis = $("#buttonExegesis").val();
	var functionName = $("#functionName").val();
	var formId = $("select[name='selectFormName']").val();
	var processDefinitionId=$("input[id='processId']").val();
	var nodeId=$("input[id='nodeId']").val();
	var isExetendButton;
	if($('#isExetendButton').attr("checked")=='checked'){
		isExetendButton = $('#isExetendButton').val();
	}else{
		isExetendButton = 0;
	}
	var orderNo = $("#orderNo").val();
	var data = {
			"id" : id,
			"processDefinitionId" : processDefinitionId,
			"nodeId" : nodeId,
			"formId" : formId,
			"isExtendButton" : isExetendButton,
			"buttonName" : buttonName,
			"buttonExegesis" : buttonExegesis,
			"functionName" : functionName,
			"orderNo" : orderNo
	};
	Dialog.post(springUrl + "/process/extendButton_insert", data, function(
			result) {
        obj.bool=result.operator;
        obj.message=  result.message;
	});
	return obj;
}

