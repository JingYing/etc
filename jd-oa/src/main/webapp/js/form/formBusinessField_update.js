function updateField(){
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            id:$('#formBusinessFieldId').val(),
            fieldChineseName:$('#fieldChineseName').val(),
            tableId:$('#tableId').val()
        },
        url: springUrl+"/form/formBusinessField_isExistFieldChineseName",
        success:function (data) {
            if(data.operator == true){
                Dialog.alert("失败",data.message);
                return false;
            }
            else{
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                    	fieldName:$('#fieldName').val(),
                        fieldChineseName:$('#fieldChineseName').val(),
                        inputType:$('#inputType').val(),
                        dataType:$('#dataType').val(),
                        length:$('#length').val(),
                        lengthDecimal:$('#lengthDecimal').val(),
                        defaultValue:$('#defaultValue').val(),
                        sortNo:$('#sortNo').val(),
                        id:$('#formBusinessFieldId').val()
                    },
                    url: springUrl+"/form/formBusinessField_updateSave",
                    success:function (data) {
                        window.location.href = springUrl+"/form/formBusinessField_index?id="+$("#tableId").val();
                        
                    },
                    error: function (data) {
                        if(data.responseText == null){
                            Dialog.alert("失败","新建数据表失败");
                        }else{
                            Dialog.alert("失败",data.responseText);
                        }
                    }
                });
            }
        },
        error: function (data) {
            if(data.responseText == null){
                Dialog.alert("失败","新建数据表失败");
            }else{
                Dialog.alert("失败",data.responseText);
            }
        }
    });
}

function cancelUpdateField(){
    Dialog.hide();
}


//初始化页面
$(function() {
    /**
     * 数据字典-数据项查询绑定事件
     */
    $('#dataType').click(function() {
        //预留数据类型弹出框
    });
    $('#updateField').click(function() {
        updateField();
    });
    $('#cancelUpdateField').click(function() {
        cancelUpdateField();
    });
    
    $('#inputType').bind('change',function(){
    	var buttons = [{
			"label": "取消",
			"class": "btn-success",
			"callback": function(){
				
			}
		},
		{
			"label": "确定",
			"class": "btn-success",
			"callback": function(){
				Dialog.hide();
			}
		}];
    	Dialog.openRemote('编辑','filed_item_ui_design_index',600,400,buttons);
    });
});
