/**
 * Created with IntelliJ IDEA.
 * Desctription:表单新增
 * User: yujiahe
 * Date: 13-9-13
 * Time: 上午9:46
 * To change this template use File | Settings | File Templates.
 */
/**
 * 初始化操作
 */
$(document).ready(function () {

    loadCreateType();
    
    //模式选择
    $('#newCreateType').change(function(){
        var selectValue = $(this).children('option:selected').val();//select的值
        if(selectValue=='copyModule'){
        	$('#selectForms').show();
        } else {
        	$('#selectForms').hide();
        }
    });
    
    //选择表单,设置#formIdSelect和#formId
	$("#formIdSelect").click(function(){
		formIdSelectClick();
	});
    bindValidate($("#formForm"));// 绑定验证

});

//选择表单
function formIdSelectClick(){
	
	//查找表单
	var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {

            }
        },
	{
		"label": "保存",
		"class": "btn-success",
		"callback": function(){
			var bol = false; //process_insert(id);
			var data = Table.getSelectedRows(formSearchTable);
			
			if(data.length>1){
				Dialog.alert("提示","只能选择单个表单！");return false;}
			if(data.length<1){
				Dialog.alert("提示","请选择一个表单 ! ");return false;}
			
			$("#formIdSelect").text(data[0].formName);
			$("#formId").val(data[0].id);
			
			return true;
		}	     							
	}];
	
	Dialog.openRemote('选择表单',encodeURI(springUrl+ '/form/formSearch_index'),600,400,buttons);
}

/**
 * 加载表单类型
 */
function loadCreateType() {
    jQuery.ajax({
        type: "post",
        cache: false,
        dataType: 'json',
        url: springUrl + "/dict/dictData_findDictDataList?dictTypeCode=createType",
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                $("<option value='" + data[i].dictCode + "'>" + data[i].dictName + "</option>").appendTo($("#newCreateType"));
            }
        },
        error: function (data) {
            Dialog.alert("失败!", "加载表单类型失败");
        }
    });
}

/**
 * 保存新增表单按钮触发函数
 */
var check = true;//校验标志位
function addForm() {
    //判断当前表单名称是否存在
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        async:false,
        data: {
            formName: $('#newFormName').val(),
            parentId:$("#parentId").val()
        },
        url: springUrl + "/form/form_isExistForm",
        beforeSend: function () {//提交前校验
        	var createType = $("#newCreateType").val();
        	var formId = $('#formId').val();
        	//alert(createType + "@@" + formId + "##" + $('#newFormName').val());
        	if(createType=='copyModule'&&(formId==""||typeof(formId)=='undefined')) {
        		Dialog.alert("提示","请选择复制的表单文件！");
        		check = false;
        		return check;
        	}
        },
        success: function (data) {
            if (data == true) {
                Dialog.alert("失败", "已存在相同名称表单，请重置");
                check = false;
            } else {
                addSaveForm();
            }
        },
        error: function (data) {
            Dialog.alert("失败", data.responseText);
        }
        
    });
    return check;
}
/**
 * 保存新增表单事件，此处插入支持主表、子表
 */
var formType = "";
var newCreateType="";
var newFormName="";

function addSaveForm() {
    var parentId = $("#parentId").val();
    //alert(createType + "@@" + formId + "##" + $('#newFormName').val());
    if (parentId == 0) {//如果为0则为新增主表，否则为新增子表
        formType = 'M';
    } else {
        formType = "S";
    }
    newCreateType=$('#newCreateType').val();
    newFormName= $('#newFormName').val();
    //新增数据项
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        async:false,
        data: {
            parentId: $("#parentId").val(),
            formType: formType,
            formName:newFormName,
            createType:newCreateType,
            formDesc:$('#newFormDesc').val(),
            formId: $("#formId").val()
        },
        url: springUrl + "/form/form_addSave",
        success: function (data) {

            if(formType=='S'){

                window.location.reload();
            } else{
               Table.render(formTable);
               setForm(data.formId,newFormName,newCreateType); //直接进入表单设置页面
            }

        },
        error: function (data) {
            if (data.responseText == null) {
                Dialog.alert("失败", "新建表单失败");
            } else {
                Dialog.alert("失败", data.responseText);
            }
        }
    });
}

function setForm(formId,formName,createType,tableId) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        }

    ];
    var textModel = "";
    if(createType=='advancedModule'){
        textModel = "--高级创建模式";
    }else if(createType=='copyModule'){
        textModel = "--复制创建模式";
    }else{
        textModel = "--标准创建模式";
    }
    Dialog.openRemote(formName+textModel, 'formItem_index?formId='
        + formId + '&formName=' + formName + '&createType=' + createType + '&tableId=' + tableId, 1000,580,buttons);
}