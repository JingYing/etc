var treeSetting = {
			check: {
				enable: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			view: {
				expandSpeed: ""
			},
			callback: {
				onClick: treeClickEvent
//				onAsyncSuccess: zTreeOnAsyncSuccess
			}
	};
	
$(document).ready(function() {
	var treeObj = $.fn.zTree.init($("#formuicomponentTree"), treeSetting, zNodes);
	treeObj.expandAll(true); 
	//自动选中节点
	for(var i = 0 ; i < treeObj.getNodes().length; i++){
		var parentTreeNode = treeObj.getNodes()[i];
		for(var j = 0 ; j < parentTreeNode.children.length ; j++){
			var subTreeNode = parentTreeNode.children[j];
			if(subTreeNode.id == document.getElementById('inputTypes').value){
				var selectNode = treeObj.getNodeByTId(subTreeNode.tId);
				treeObj.selectNode(selectNode);
				break;
			}
		}
	}
	document.getElementById('formuicomponentIframe').src = encodeURI(baseUrl + '/form/filed_item_ui_design_display?fieldId=' +  
			document.getElementById('fieldId').value + "&inputType=" + 
			document.getElementById('inputTypes').value + "&formId=" + document.getElementById('formId').value + "&isFormDesign=" + document.getElementById('isFormDesign').value);
});

function treeClickEvent(e, treeId, treeNode){
	if(!treeNode.isParent){
		document.getElementById('formuicomponentIframe').src = baseUrl + '/form/filed_item_ui_design_display?fieldId=' +  
		document.getElementById('fieldId').value + 
		"&inputType=" + treeNode.id + "&formId=" + document.getElementById('formId').value + "&isFormDesign=" + document.getElementById('isFormDesign').value;
	}
	return false;
}
function getUrl(treeId, treeNode) {
	var nodeType = "";
	try{
		nodeType = treeNode.props.nodeType;
	}catch(e){
		
	}
	var param = "id=" + treeId + "&name=" + treeNode.name + "&nodeType=" + nodeType;
	return baseUrl + "/form/getFormUItree_JsonData?" + param;
}
function zTreeOnAsyncSuccess(event, treeId, msg) {
	var zTree = $.fn.zTree.getZTreeObj("formuicomponentTree");
	expandNodes(zTree.getNodes());
}
function getFormuicomponentTreeSelectedNode(){
	var zTree = $.fn.zTree.getZTreeObj("formuicomponentTree");
	return zTree.getSelectedNodes();
}
function expandNodes(nodes) {
	if (!nodes) return;
	var zTree = $.fn.zTree.getZTreeObj("formuicomponentTree");
	for (var i = 0, l = nodes.length; i < l; i++) {
		zTree.expandNode(nodes[i], true, false, false);
		if (nodes[i].isParent) {
			expandNodes(nodes[i].children);
		}
	}
}
