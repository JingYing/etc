/**
 * 数据字典-数据类别新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 初始化操作
 */
$(function() {
	bindValidate($('#businessTableFormUpdate'));
    if($("#tableType").attr("checked")=="checked"){
        $("#isShowUpdateTable").hide();
    }
    else{
        $("#isShowUpdateTable").show();
    }
});
