var editorTemplate;
var printTemplate;
$(function() {
	//初始化kind
    KindEditor.ready(function(K) {
    	editorTemplate = K.create('textarea[name="editTemplate"]', {
            resizeType : 0,
            filterMode : false,
            items : [
                     'source', '|', 'fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold', 'italic', 'underline',
                     'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
                     'insertunorderedlist', '|', 'emoticons', 'link']
        });
    	printTemplate = K.create('textarea[name="printTemplate"]', {
            resizeType : 0,
            filterMode : false,
            items : [
                     'source', '|', 'fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold', 'italic', 'underline',
                     'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
                     'insertunorderedlist', '|', 'emoticons', 'link']
        });
    });
});
/**
 * 删除表单模板
 * @returns {Boolean}
 */
function deleteTemplate(){
	parent.Dialog.confirm("提示","确定要删除该表单模板吗?","确定","取消",function(flag){
		if(flag){
			$.ajax({
		        type : "GET",
		        cache : false,
		        url : springUrl + "/form/formTemplate_delete",
		        data : "formId=" + document.getElementById('formId').value + "&templateId=" + document.getElementById('templateId').value + "&isPrint=" + document.getElementById('isPrint').value,
		        success:function (msg) {
		        	parent.Dialog.alert("提示","删除成功!");
		        	if(document.getElementById('isPrint').value == 'true'){
		        		//parent.document.getElementById('printTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value + "&isPrint=" + document.getElementById('isPrint').value;
		        	}else {
		        		//parent.document.getElementById('editTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value;
		        	}
		        },
		        error : function(msg){
		        	
		        }
		    });
		}
	});
	return false;
}
/**
 * 新建表单模板
 */
function createNewTemplate(){
	parent.bootbox.prompt("新建一个表单模板","取消","确定",function(templateName){
		if(templateName != null && templateName != ''){
			document.getElementById('templateId').value = '';
			document.getElementById('templateName').value = templateName;
			var html = $.ajax({url: "formTemplate_createNew?formId=" + document.getElementById('formId').value + "&templateName=" + encodeURI(templateName) , async: false }).responseText;
			if(document.getElementById('isPrint').value == 'true'){
				printTemplate.html(html);
			} else {
				editorTemplate.html(html);
			}
			return false;
		}
	});
	return false;
}
/**
 * 重命名表单模板
 */
function renameTemplate(){
	var oldTemplateName = document.getElementById('templateName').value;
	parent.bootbox.prompt("重命名","取消","确定",function(templateName){
		if(templateName != null && templateName != ''){
			document.getElementById('templateName').value = templateName;
			var data = "formId=" + document.getElementById('formId').value + "&templateName=" + encodeURI(document.getElementById('templateName').value) + "&templateId=" + document.getElementById('templateId').value;
			jQuery.ajax({
		        type : "POST",
		        cache : false,
		        url : "formTemplate_rename",
		        data : data,
		        success:function (msg) {
		        	parent.Dialog.alert("提示","保存成功!");
		        	parent.document.getElementById('editTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value + "&templateId=" + document.getElementById('templateId').value;
		        },
		        error : function(msg){
		        	
		        }
		    });
			return false;
		}
	},oldTemplateName);
	return false;
}
/**
 * 另存为表单模板
 * @returns {Boolean}
 */
function saveAsTemplate(){
	parent.bootbox.prompt("另存为...","取消","确定",function(templateName){
		if(templateName != null && templateName != ''){
			document.getElementById('templateName').value = templateName;
			var data = "formId=" + document.getElementById('formId').value + "&templateName=" + encodeURI(templateName) + "&templateId=" + document.getElementById('templateId').value;
			saveAsPost(data);
			return false;
		}
	});
	return false;
}
function saveAsPost(data){
	jQuery.ajax({
        type : "GET",
        cache : false,
        url : "formTemplate_saveAs",
        data : data,
        success:function (msg) {
        	parent.Dialog.alert("提示","保存成功!");
        	parent.document.getElementById('editTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value + "&templateId=" + msg;
        },
        error : function(msg){
        	
        }
    });
}
/**
 * 自动生成表单模板
 * @param templateValue
 * @returns {Boolean}
 */
function autoCreateTemplate(templateValue){
	if(document.getElementById('templateName').value == ''){
		document.getElementById('templateName').value = document.getElementById('formName').value;
	}
	var html = $.ajax({url: "formTemplate_createAuto?formId=" + document.getElementById('formId').value + "&templateValue=" + templateValue + "&templateName=" + encodeURI(document.getElementById('templateName').value), async: false }).responseText;
	if(document.getElementById('isPrint').value == 'true'){
		printTemplate.html(html);
	} else {
		editorTemplate.html(html);
	}
	return false;
}
/**
 * 保存表单模板
 */
function saveTemplate(){
	var editorValue;
	if(document.getElementById('isPrint').value == 'true'){
		editorValue = printTemplate.html();
	} else {
		editorValue = editorTemplate.html();
	}
	var templateValue = replaceAll(editorValue,'<!--','<!!--');
	var data = "formId=" + document.getElementById('formId').value + 
		"&templateName=" + document.getElementById('templateName').value +
		"&templateValue=" + encodeURIComponent(templateValue) + 
		"&templateId=" + document.getElementById('templateId').value + 
		"&isPrint=" + document.getElementById('isPrint').value;
	savePost(data);
}
/**
 * 保存提交
 * @param data
 */
function savePost(data){
	jQuery.ajax({
        type : "POST",
        cache : false,
        url : "formTemplate_save",
        data : data,
        success:function (msg) {
        	parent.Dialog.alert("提示","保存成功!");
        	if(document.getElementById('isPrint').value == 'true'){
        		parent.document.getElementById('printTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value + '&isPrint=' + document.getElementById('isPrint').value + '&templateId=' + msg;
        	} else {
        		parent.document.getElementById('editTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value + '&templateId=' + msg;
        	}
        },
        error : function(msg){
        	
        }
    });
}

function replaceAll(streng, soeg, erstat){
    var st = streng;
    if (soeg.length == 0)
     return st;
    var idx = st.indexOf(soeg);
    while (idx >= 0)        
    {  
     st = st.substring(0,idx) + erstat + st.substr(idx+soeg.length);
     idx = st.indexOf(soeg);
    }
    return st;
} 
function changeSelect(){
	 var sel = document.getElementById("formTelmplateSelectOption"); 
	 var selValue = sel.value; 
	 if(document.getElementById('isPrint').value == 'true'){
 		parent.document.getElementById('printTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value + "&isPrint=" + document.getElementById('isPrint').value + "&templateId=" + selValue;
 	}else {
 		parent.document.getElementById('editTeplateFrame').src = 'formTemplate_edit?formId=' + document.getElementById('formId').value + "&templateId=" + selValue;
 	}
	return false;
}
/**
 * 预览
 * @returns {Boolean}
 */
function reviewTemplate(){
	if(document.getElementById('templateId').value == ''){
		alert('请保存!');
		return false;
	}
	var date = new Date();
	window.open(springUrl + '/form/formTemplate_review?templateId=' + document.getElementById('templateId').value + '&formId=' + document.getElementById('formId').value,'newwindow' + date.getTime(),'height=800px,width=800px,top=0,left=0,toolbar=no,menubar=no,scrollbars=yes, resizable=no,location=no, status=no') ;
	return false;
}