/**
 * Created with IntelliJ IDEA.
 * Desctription:表单库首页
 * User: yujiahe
 * Date: 13-9-12
 * Time: 下午2:48
 * To change this template use File | Settings | File Templates.
 */
// 页面初始化加载分页
var formSearchTable;

$(document).ready(
    settable()
);


/**
 * 初始化设置分页相关属性
 */

function settable() {
    var options = Table.options;
    options = {

        useCheckbox: true,
        pageUrl: springUrl + "/form/form_page_def",
        useCheckbox: false,
        defaultSort: [],
        sendData: function (sSource, aoData, fnCallback) {
            var FSformName = $.trim($("input#FSformName").val());
            aoData.push({
                "name": "formName",
                "value": FSformName
            }, {
                "name": "createType",
                "value": ""
            }, {
                "name": "owner",
                "value": ""
            });
            jQuery.ajax({
                type: "POST",
                url: springUrl + "/form/form_page_def",
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    fnCallback(resp);
                }
            });
        },
        // 分页列设置
        columns: [
            {
                "sTitle": "表单名称", "sWidth": 200,
                "fnRender": function (obj) {
                    return obj.aData.formName;
                }
            }
        ],
        // 分页按钮设置
        btns: [],
    };

    formSearchTable = Table.dataTable("formListTable", options);
    $("#FSsearch").click(function () {
        try{
            Table.render(formSearchTable);
        }catch (e){

        }

    });
}
