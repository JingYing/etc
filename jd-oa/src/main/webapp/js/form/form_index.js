/**
 * Created with IntelliJ IDEA.
 * Desctription:表单库首页
 * User: yujiahe
 * Date: 13-9-12
 * Time: 下午2:48
 * To change this template use File | Settings | File Templates.
 */
// 页面初始化加载分页
$(document).ready(
    loadIndexCreateType
);

var createTypeJson;
var formTable;
/**
 * 加载表单类型select下拉框
 */
function loadIndexCreateType() {
    jQuery.ajax({
        type: "post",
        cache: false,
        dataType: 'json',
        url: springUrl + "/dict/dictData_findDictDataList?dictTypeCode=createType",
        success: function (data) {
            createTypeJson = data;
            for (var i = 0; i < data.length; i++) {
                $("<option value='" + data[i].dictCode + "'>" + data[i].dictName + "</option>").appendTo($("#createType"));
            }
            settable();
        },
        error: function (data) {
            Dialog.alert("失败!", "加载表单类型失败");
        }
    });
    jQuery.ajax({
        type: "post",
        cache: false,
        dataType: 'json',
        url: springUrl + "findPowerUserList",
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                $("<option value='" + data[i].userId + "'>" + data[i].realName + "</option>").appendTo($("#owner"));
            }
        },
        error: function (data) {
            Dialog.alert("失败!", "加载表单类型失败");
        }
    });

}
/**
 * 初始化设置分页相关属性
 */

function settable() {
    var options = Table.options;
    options = {
        pageUrl: "form_page",
        useCheckbox: true,
        defaultSort:[[4,"desc"],[3,"desc"]],
        sendData: function (sSource, aoData, fnCallback) {
            var formName = $.trim(($("input#formName").val()));
            var createType = $("select#createType").val();
            var owner = $("select#owner").val();
            aoData.push({
                "name": "formName",
                "value": formName
            }, {
                "name": "createType",
                "value": createType
            }, {
                "name": "owner",
                "value": owner
            });
            jQuery.ajax({
                type: "POST",
                url: "form_page",
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    fnCallback(resp);

                }
            });
        },
        // 分页列设置
        columns: [
            {
                "mDataProp": "formName",
                "sTitle": "表单名称",
                "bSortable":true,
                "sWidth":200,
                "mRender": function (data) {
                    if(data.length>20){
                        return'<span title="'+data+'"> '+data.substr(0,20)+'...'+'</span>';
                    }else{
                        return data;
                    }

                }
            },
            {
                "mDataProp": "createType",
                "sTitle": "创建模式",
                "sWidth":80,
                "mRender": function (data) {
                    for (var i = 0; i < createTypeJson.length; i++) {
                        if (data == createTypeJson[i].dictCode) {
                            return  createTypeJson[i].dictName;
                        }
                    }
                }
            },
            { "mData": "ownerName", "sTitle": "所有人", "sWidth":80 },
            { "mData": "modifyTime", "sTitle": "最后更新", "bSortable": true,"sWidth":80,"mRender": function (data) {
                return new Date(data).format("yyyy-MM-dd");
            }},
            { "mData": "tableId", "sTitle": "同步状态", "sWidth":80,"bSortable": true, "mRender": function (data) {
                if($.trim(data)!=''){
                    return "已同步";
                } else {
                    return "<font color='red'>未同步</font>";
                }
            }},
            {
                "sTitle": "操作",
                "sWidth":300,
                "fnRender": function (obj) {
                    var a = '{"id":"' + obj.aData.id + '"}';
                    var viewHtml = '<a href="#" class="btn" title="查看表单详情" onclick="viewForm(\'' + obj.aData.id + '\')"><img src="'
                        + springUrl
                        + '/static/common/img/view_obj.gif" alt="修改"/></a>&nbsp;&nbsp;';
                    var editHtml = '<a href="#" class="btn" title="编辑" onclick="editForm(\'' + obj.aData.id + '\')"><img src="'
                        + springUrl
                        + '/static/common/img/edit.gif" alt="修改"/></a>&nbsp;&nbsp;';

                    var delHtml = "<a href='#' class='btn' title='删除' onclick='javascript:delForm(\""
                        + obj.aData.id
                        + "\")'><img src='"
                        + springUrl
                        + "/static/common/img/del.png' alt='删除'/></a>&nbsp;&nbsp;";
                    var setHtml = "<a href='#' class='btn' title='配置表单' onclick='javascript:setForm(\""
                        + obj.aData.id +"\",\""+obj.aData.formName + "\",\""+obj.aData.createType + "\",\""+obj.aData.tableId
                        + "\")'><img src='"
                        + springUrl
                        + "/static/common/img/set.png' alt='配置表单'/></a>&nbsp;&nbsp;";
                    var synchDataStrToBu = "<a href='#' class='btn' title='同步至业务表' onclick='javascript:createBusinessTable(\""
                        + obj.aData.id + "\",\""+obj.aData.tableId
                        + "\")'><img src='"
                        + springUrl
                        + "/static/common/img/form.gif' alt='同步至业务表'/></a>";

                    var returnStr = editHtml + setHtml;
                    if(obj.aData.createType!='advancedModule'){
                        returnStr = returnStr + synchDataStrToBu;
                    }
                    returnStr=returnStr  + delHtml;
                    return returnStr;
                }
            }
        ],
        // 分页按钮设置
        btns: [
            {
                "sExtends": "text",
                "sButtonText": "新增",
                "sButtonClass": "btn btn-success",
                "sToolTip": "",
                "fnClick": function (nButton, oConfig, oFlash) {
                    var buttons = [
                        {
                            "label": "取消",
                            "class": "btn-success",
                            "callback": function () {

                            }
                        },
                        {
                            "label": "保存",
                            "class": "btn-success",
                            "callback": function () {
                                if(!validate2($("#formForm"))){
                                    return false;
                                }else{
                                    return addForm();
                                }

                            }
                        }
                    ];
                    Dialog.openRemote('新增表单', 'form_add?parentId=0', 600, 400, buttons);//新增主表，parentId=0
                }
            },
            {
                "sExtends": "text",
                "sButtonText": "变更所有人",
                "sButtonClass": "btn btn-success",
                "sToolTip": "",
                "fnClick": function (nButton, oConfig, oFlash) {
                    var ids = Table.getSelectedRowsIDs(formTable);//复选框选中项目的ID，逗号隔开
                    var buttons = [
                        {
                            "label": "取消",
                            "class": "btn-success",
                            "callback": function () {

                            }
                        },
                        {
                            "label": "保存",
                            "class": "btn-success",
                            "callback": function () {
                                return updateFormOwner();
                            }
                        }
                    ];
                    if (ids.length == 0) {
                        Dialog.alert("消息提示", "没有选中项", "确定");
                    } else {
                        Dialog.openRemote('变更所有人', 'formOwner_update?ids=' + ids, 500, 200, buttons);
                    }

                }
            }
//            ,
//            {
//                "sExtends": "text",
//                "sButtonText": "删除",
//                "sButtonClass": "btn btn-success",
//                "sToolTip": "",
//                "fnClick": function (nButton, oConfig, oFlash) {
//                    var ids = Table.getSelectedRowsIDs(formTable);//复选框选中项目的ID，逗号隔开
//                    if (ids.length == 0) {
//                        Dialog.alert("消息提示", "没有选中项", "确定");
//                    } else {
//                        delForm(ids);
//                    }
//                }
//            }
        ]
    };

    formTable = Table.dataTable("mainFormListTable", options);
    $('#search').click(function () {
        Table.render(formTable);
    });
    $('#jss').click(function () {
    	Dialog.openRemote('JSS 速度测试', '/form/jss_test', 950, 550);
    });
}

/**
 *设置表单具体项
 */
function setForm(formId,formName,createType,tableId) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        }

    ];
    var textModel = "";
    if(createType=='advancedModule'){
        textModel = "--高级创建模式";
    }else if(createType=='copyModule'){
        textModel = "--复制创建模式";
    }else{
        textModel = "--标准创建模式";
    }
    
//    var url = '/form/formItem_index?formId='
//    + formId + '&formName=' + formName + '&createType=' + createType + '&tableId=' + tableId;
//    window.open(url,formName+textModel);
    
    Dialog.openRemote(formName+textModel, 'formItem_index?formId='
        + formId + '&formName=' + formName + '&createType=' + createType + '&tableId=' + tableId, 1000,580,buttons);
}

/**
 *删除单个（批量）表单
 */
function delForm(formIds) {
    Dialog.del('form_delete?ids='+formIds+'&formType=M', '', function (result) {
        Table.render(formTable);
        Dialog.alert("系统提示","表单删除成功！");
    });
}
/**
 * 查看表单详情
 * @param formId
 */
function viewForm(formId) {
    Dialog.openRemote('表单详情', 'form_view?formId=' + formId, 700, 400);
}
/**
 * 编辑表单
 * @param formId
 */
function editForm(formId) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function () {
                if(!validate2($("#editFormForm"))){
                    return false;
                }else{
                    return updateForm();

                }
            }
        }
    ];
    Dialog.openRemote('编辑表单', 'form_update?formId=' + formId, 550, 400, buttons);
}


function showBoxBg(e) {
    var bh = $(document).height();
    var bw = $(document).width();
    $("#fullbg").css({
        height:bh,
        width:bw,
        display:"block"
    });
    $(e).show();
}

function Request(){
    showBoxBg('#loading');
}

//关闭灰色 jQuery 遮罩
function Response(){
    $('#loading').hide();
    $("#fullbg").hide();
}

/**
 *同步表单信息到业务表并创建物理存储表
 */
function createBusinessTable(formId,tableId) {
    $.ajax({
        type: "GET",
        cache: "false",
        url: "synchronization_formInfo_businessTable?formId=" + formId + "&tableId=" + tableId,
        success: function (data) {
            if(data.operator){
                Dialog.alert("成功",data.message);
                Table.render(formTable);
            } else {
                if(typeof(data.message)!='undefined'&& data.message!=''){
                    Dialog.alert("失败",data.message);
                } else {
                    Dialog.alert("失败!", "程序出现异常，请联系管理员！");
                }
            }
            Response();
        },
        error: function (data) {
            Response();
            Dialog.alert("失败!", "程序出现异常，请联系管理员！");
        }
    });
}

/**
 * added by wdx  mod by yjh
 * 2013-9-26
 * UI类型
 * 从formItem_EditItem.js转移过来
 * @param id
 * @param name
 * @param title
 * @param inputType
 */
function editInputType(id,name,title,inputType,formId){
    var editformItem = document.getElementById("editformItem").contentWindow.document;
    setTimeout(function() {
        $('#UI',editformItem).focus();
    }, 0);
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function(){
                editformItem.getElementById("fieldDefaultValue").focus();
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function(){
                var configInputTypeConfig = document.getElementById('formuicomponentIframe').contentWindow.returnInputTypeConfig();
                
                if(typeof(configInputTypeConfig) == 'undefined'){
                    return false;
                }
                $("#inputTypeConfig",editformItem).val(configInputTypeConfig.inputTypeConfig);
                $("#inputType",editformItem).val(configInputTypeConfig.inputType);
                $("#htmlInner",editformItem).val(configInputTypeConfig.htmlInner);
                editformItem.getElementById("fieldDefaultValue").focus();

            }
        }];
    var dialogTitle = '配置表单控件';
    if(title != ''){
        dialogTitle = title + '【' + name + '】';
    }
    Dialog.openRemote(dialogTitle,encodeURI('filed_item_ui_design_index?fieldId=' + id + "&inputType=" + inputType +"&formId=" + formId + "&isFormDesign=true"),1000,430,buttons);
}



/**
 *删除单个（批量）表单
 */
function delSonForm(formIds) {
    Dialog.del('form_delete?ids='+formIds+'&formType=S', '', function (result) {
        var editformItem = document.getElementById("editformItem").contentWindow.document;
        editformItem.location.reload();
        Dialog.alert("系统提示","删除表单成功");
    });
}

/**
 * 编辑表单
 * @param formId
 */
function editSonForm(formId) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function () {
                if(!validate2($("#editFormForm"))){
                    return false;
                }else{
                    return updateForm();
                }
            }
        }
    ];
    Dialog.openRemote('编辑表单', 'form_update?formId=' + formId, 550, 400, buttons);
}
/**
 * 打开数据源映射配置窗口
 * @param formId
 */
function openDataSourceMappingDialog(dataSourceId,dataSourceName,formId,fieldId) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        }
    ];
    Dialog.openRemote('配置【'+ dataSourceName +'】的映射关系', '/di/datasource/ds_mapping?dataSourceId=' + dataSourceId + '&formId=' + formId + '&fieldId=' + fieldId, 850, 450, buttons);
}

