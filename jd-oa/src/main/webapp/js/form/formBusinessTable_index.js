/**
 * 数据字典-数据项列表页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var table;
var subTable;
var subOptions = Table.options;
var options = Table.options;
var flag = 0;

options = {
    pageUrl: springUrl + "/form/formBusinessTable_findPage",
    useCheckbox: true,
    //defaultSort:[[1,"asc"],[2,"desc"]],
    sendData: function (sSource, aoData, fnCallback) {
        var createType = $("#tableType").val();
        var tableChineseName = $("#tableChineseName").val();
        var tableName = $("#tableName").val();
        var tableType = "m";
        if ($("#tableType").val() == "") {
            var createType = null;
        }
        if (tableChineseName == null) tableChineseName = null;
        if (tableName == null) tableName = null;
        aoData.push({ "name": "tableType", "value": tableType }, { "name": "createType", "value": createType }, {"name": "tableChineseName", "value": jQuery.trim(tableChineseName)}, {"name": "tableName", "value": jQuery.trim(tableName)});
        jQuery.ajax({
            type: "POST",
            url: sSource,
            dataType: "json",
            data: aoData,
            success: function (json) {
                fnCallback(json);
            },
            error: function (data) {
                Dialog.alert("失败","数据库表查询失败");
            }
        });
    },
    columns: [
        { "mData": "id", "sTitle": "id", "bSortable": false, "bVisible": false},
        { "mData": "tableChineseName", "sTitle": "数据库表中文名", "bSortable": false, "bVisible": true,
        	"mRender": function (obj) {
        		if(obj.length>20){
                    return'<span title="'+obj+'"> '+obj.substr(0,20)+'...'+'</span>';
                }else{
                   return obj;
                }
            }
        },
        { "mData": "tableName", "sTitle": "数据库表名", "bSortable": false, "sClass": "my_class",
            fnRender: function (obj) {
                var html = "<a href='#' onclick='detailBusinussTable(\"" + obj.aData.id + "\");'>" + obj.aData.tableName + "</a>";
                return html;
            }
        },
        { "mData": "tableType", "sTitle": "类型", "bSortable": false, "sClass": "my_class",
            fnRender: function (obj) {
                var html = "";
                html += obj.aData.createType == "1" ? "手动创建-" : "自动创建-";
                html += obj.aData.tableType == "s" ? "子表" : "主表";
                return html;
            }
        },
        { "mData": "modifier", "sTitle": "操作人", "bSortable": false, "sClass": "my_class"},
        { "mData": "modifyTime", "sTitle": "最后更新", "bSortable": false, "mRender": function (data) {
            return data==null?"":new Date(data).format("yyyy-MM-dd");
        }},
        {"sTitle": "操作", "bSortable": false, fnRender: function (obj) {
            if (obj.aData.tableType == "自动创建-子表") {
                return "";
            }
            else if (obj.aData.tableType == "自动创建-主表") {
                var tempId = obj.aData.id + "-" + obj.aData.tableType;
                return "<a href='#' class='btn' title='查看子表' onclick='javascript:viewChildren(\"" + tempId + "\")'><img src='" + springUrl + "/static/common/img/businessTable_field.png' alt='查看子表'/></a>";
            }
            else if (obj.aData.tableType == "手动创建-主表") {
                var tempId = obj.aData.id + "-" + obj.aData.tableType;
                return  "<a href='#' class='btn' title='查看子表' onclick='javascript:viewChildren(\"" + tempId + "\")'><img src='" + springUrl + "/static/common/img/businessTable_field.png' alt='查看子表'/></a>" +
                    "<a href='#' title='修改' class='btn'><img src='" + springUrl + "/static/common/img/edit.gif' alt='修改'/ onclick='updateFormTable(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='配置字段' class='btn'><img src='" + springUrl + "/static/common/img/set.png' alt='配置字段'/ onclick='toTableField(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='删除' class='btn'><img src='" + springUrl + "/static/common/img/del.png' alt='删除' onclick='deleteFormTable(\"" + obj.aData.id + "\");'/></a>" ;
            }
            else {
                return "<a href='#' title='修改' class='btn'><img src='" + springUrl + "/static/common/img/edit.gif' alt='修改'/ onclick='updateFormTable(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='配置字段' class='btn'><img src='" + springUrl + "/static/common/img/set.png' alt='配置字段'/ onclick='toTableField(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='删除' class='btn'><img src='" + springUrl + "/static/common/img/del.png' alt='删除' onclick='deleteFormTable(\"" + obj.aData.id + "\");'/></a>";
            }
        }
        }

    ],
    btns: [
        {
            "sExtends": "text",
            "sButtonText": "新增",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                var buttons = [
                    {
                        "label": "取消",
                        "class": "btn-success",
                        "callback": function () {
                        }
                    },
                    {
                        "label": "保存",
                        "class": "btn-success",
                        "callback": function () {
                            if (!validate2($('#businessTableForm'))) {
                                return false;
                            }
                            return addTable();
                        }
                    }
                ];
                Dialog.openRemote("创建数据库表", springUrl + "/form/formBusinessTable_add", 700, 420, buttons);
            }
        },
        {
            "sExtends": "text",
            "sButtonText": "删除",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                batchDeleteBusinessTable();
            }
        }
    ]
};

subOptions = {
    pageUrl: springUrl + "/form/formBusinessTable_findPage",
    useCheckbox: true,
    //defaultSort:[[1,"asc"],[2,"desc"]],
    sendData: function (sSource, aoData, fnCallback) {
        var createType = $("#tableType").val();
        var tableChineseName = $("#tableChineseName").val();
        var tableName = $("#tableName").val();
        var parentId = $("#tableId").val();
        if ($("#tableType").val() == "") {
            var createType = null;
        }
        var tableType = "s";
        if (tableChineseName == null) tableChineseName = null;
        if (tableName == null) tableName = null;
        aoData.push({ "name": "tableType", "value": tableType }, { "name": "createType", "value": createType }, {"name": "tableChineseName", "value": jQuery.trim(tableChineseName)}, {"name": "tableName", "value": jQuery.trim(tableName)}, {"name": "parentId", "value": parentId});
        jQuery.ajax({
            type: "POST",
            url: sSource,
            dataType: "json",
            data: aoData,
            success: function (json) {
                fnCallback(json);
            },
            error: function (data) {
                Dialog.alert("失败","数据库表查询失败");
            }
        });
    },
    columns: [
        { "mData": "id", "sTitle": "id", "bSortable": false, "bVisible": false},
        { "mData": "tableChineseName", "sTitle": "数据表名", "bSortable": false, "bVisible": true,
        	"mRender": function (obj) {
            	if(obj.length>20){
                    return'<span title="'+obj+'"> '+obj.substr(0,20)+'...'+'</span>';
                }else{
                   return obj;
                }
            }
        },
        { "mData": "tableName", "sTitle": "数据库表名", "bSortable": false, "sClass": "my_class",
            fnRender: function (obj) {
                var html = "<a href='#' onclick='detailBusinussTable(\"" + obj.aData.id + "\");'>" + obj.aData.tableName + "</a>";
                return html;
            }
        },
        { "mData": "tableType", "sTitle": "类型", "bSortable": false, "sClass": "my_class",
            fnRender: function (obj) {
                var html = "";
                html += obj.aData.createType == "1" ? "手动创建-" : "自动创建-";
                html += obj.aData.tableType == "s" ? "子表" : "主表";
                return html;
            }
        },
        { "mData": "modifier", "sTitle": "操作人", "bSortable": false, "sClass": "my_class"},
        { "mData": "modifyTime", "sTitle": "最后更新", "bSortable": false, "mRender": function (data) {
            return data==null?"":new Date(data).format("yyyy-MM-dd");
        }},
        {"sTitle": "操作", "bSortable": false, fnRender: function (obj) {
            if (obj.aData.tableType == "自动创建-子表") {
                return "";
            }
            else if (obj.aData.tableType == "自动创建-主表") {
                return "<a href='#' class='btn' title='查看子表' onclick='javascript:viewChildren(\"" + obj.aData.id + "\")'><img src='" + springUrl + "/static/common/img/form.gif' alt='查看子表'/></a>";
            }
            else if (obj.aData.tableType == "手动创建-主表") {
                return "<a href='#' title='修改' class='btn'><img src='" + springUrl + "/static/common/img/edit.gif' alt='修改'/ onclick='updateFormTable(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='配置字段' class='btn'><img src='" + springUrl + "/static/common/img/set.png' alt='配置字段'/ onclick='toTableField(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='删除' class='btn'><img src='" + springUrl + "/static/common/img/del.png' alt='删除' onclick='deleteFormTable(\"" + obj.aData.id + "\");'/></a>" +
                    "<a href='#' class='btn' title='查看子表' onclick='javascript:viewChildren(\"" + obj.aData.id + "\")'><img src='" + springUrl + "/static/common/img/form.gif' alt='查看子表'/></a>";
            }
            else {
                return "<a href='#' title='修改' class='btn'><img src='" + springUrl + "/static/common/img/edit.gif' alt='修改'/ onclick='updateFormTable(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='配置字段' class='btn'><img src='" + springUrl + "/static/common/img/set.png' alt='配置字段'/ onclick='toTableField(\"" + obj.aData.id + "\");'></a>&nbsp;" +
                    "<a href='#' title='删除' class='btn'><img src='" + springUrl + "/static/common/img/del.png' alt='删除' onclick='deleteFormTable(\"" + obj.aData.id + "\");'/></a>";
            }
        }
        }

    ],
    btns: [
        {
            "sExtends": "text",
            "sButtonText": "新增",
            "sButtonClass": "btn btn-success hiddenFlag",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                var buttons = [
                    {
                        "label": "取消",
                        "class": "btn-success",
                        "callback": function () {
                        }
                    },
                    {
                        "label": "保存",
                        "class": "btn-success",
                        "callback": function () {
                            if (!validate2($('#businessTableForm'))) {
                                return false;
                            }
                            return addTable();
                        }
                    }
                ];
                Dialog.openRemote("创建数据库表", springUrl + "/form/formBusinessTable_add", 700, 420, buttons);
            }
        },
        {
            "sExtends": "text",
            "sButtonText": "删除",
            "sButtonClass": "btn btn-success hiddenFlag",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                batchDeleteBusinessTable();
            }
        },
        {
            "sExtends": "text",
            "sButtonText": "返回",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                goBack();
            }
        }
    ]
};

//初始化页面
$(function () {
    if ($("#tableTypeFlag").val() == "s") {
        flag = 1;
        $("#magic").empty();
        var tableHtml = "<table cellpadding='0'  cellspacing='0' border='0' class='table table-striped table-bordered display' id='magicTable' width='100%'></table>";
        $("#magic").append(tableHtml);
        subTable = Table.dataTable("magicTable", subOptions);
    }
    else {
        table = Table.dataTable("formBusinessTable", options);
    }
    $('#searchTable').click(function () {
        if(flag == 0){
            Table.render(table);
        }
        else{
            Table.render(subTable);
        }
    });
});

function detailBusinussTable(id) {
    Dialog.openRemote("查看数据库表", springUrl + "/form/formBusinessTable_view?formBusinessTableId=" + id, 700, 420, [
        {
            "label": "关闭",
            "class": "btn-success",
            "callback": function () {
            }
        }
    ]);
}

function updateFormTable(id) {
    Dialog.openRemote("更新数据库表", springUrl + "/form/formBusinessTable_update?formBusinessTableId=" + id, 700, 450, [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function () {
                if (!validate2($('#businessTableFormUpdate'))) {
                    return false;
                }
                updateTable();
            }
        }
    ]);
}

function toTableField(id) {
    if (flag == 0) {
        var tableType = "m";
        window.location.href = springUrl + "/form/formBusinessField_index?id=" + id + "&tableType=" + tableType + "&tableId=" + $("#tableId").val();
    }
    else {
        var tableType = "s";
        window.location.href = springUrl + "/form/formBusinessField_index?id=" + id + "&tableType=" + tableType+ "&tableId=" + $("#tableId").val();
    }
}

function deleteFormTable(id) {
    Dialog.confirm("确认", "确定删除?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: springUrl + "/form/formBusinessTable_delete",
                data: {
                    ids: id
                },
                success: function (data) {
                    if (data.operator == false)
                        Dialog.alert("失败！", data.message);
                    else {
                        Dialog.alert("成功！", data.message);
                        if (flag == 0) {
                            Table.render(table);
                        }
                        else {
                            Table.render(subTable);
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        }
    });
}

function batchDeleteBusinessTable() {
    var dataInfo=Table.getSelectedRows(table);
    var  dataLength = dataInfo.length ;
    var ids = new Array();
    var idsTemp = new Array();
    for( var i=0; i< dataLength; i++ ) {
        if(dataInfo[i].tableType.split("-")[0] == "自动创建"){
            idsTemp.push(dataInfo[i].id);
        }
        else{
            ids.push(dataInfo[i].id);
        }
    }

    var length = ids.length;
    ids = ids.toString();
    if (dataLength == 0) {
        Dialog.alert("消息提示", "没有选中项", "确定");
    } else {
        Dialog.confirm("确认", "确定批量删除?", "是", "否", function (result) {
            if (result) {
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    url: springUrl + "/form/formBusinessTable_delete",
                    data: {
                        ids: ids,
                        length: length
                    },
                    success: function (data) {
                        if (data.operator == false)
                            Dialog.alert("失败！", "自动创建、已被表单引用、存在字段配置不能被删除");
                        else {
                            if(idsTemp.length > 0){
                                Dialog.alert("消息提示", "自动创建的数据表不能被删除", "确定");
                            }
                            else{
                                Dialog.alert("成功！", data.message);
                            }
                            if (flag == 0) {
                                Table.render(table);
                            }
                            else {
                                Table.render(subTable);
                            }
                        }
                    }
                });
            }
        });
    }
}

function viewChildren(id) {
    $("#magic").empty();
    var tableHtml = "<table cellpadding='0'  cellspacing='0' border='0' class='table table-striped table-bordered display' id='magicTable' width='100%'></table>";
    $("#magic").append(tableHtml);
    subTable = Table.dataTable("magicTable", subOptions);
    flag = 1;
    $("#tableId").val(id.split("-")[0]);
    $("#tableTypeFlag").val("s");
    if(id.split("-")[1] == "自动创建"){
        $(".hiddenFlag").hide();
        Table.render(subTable);
        subTable.fnSetColumnVis(0,false);
        subTable.fnSetColumnVis(7,false);
    }
    else{
        Table.render(subTable);
    }
}
function goBack() {
    flag = 0;
    $("#magic").empty();
    var tableHtml = "<table cellpadding='0'  cellspacing='0' border='0' class='table table-striped table-bordered display' id='formBusinessTable' width='100%'></table>";
    $("#magic").append(tableHtml);
    $("#tableId").val("");
    table = Table.dataTable("formBusinessTable", options);
    Table.render(table);
}

function addTable() {
    var tableType;
    if ($("#tableTypeAdd").attr("checked") == "checked") {
        tableType = "m";
    }
    else {
        tableType = "s";
    }
    var tableChineseNameInfo = $('#tableChineseNameAdd').val();
    var tableNameInfo = $('#tableNameAdd').val();
    var parentIdInfo = $('#parentId').val();
    var tableDescInfo = $('#tableDesc').val();
    if (tableType == "m") {
        parentIdInfo = "";
    }
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        data: {
            tableChineseName: tableChineseNameInfo,
            tableName: tableNameInfo
        },
        url: springUrl + "/form/formBusinessTable_isExistTable",
        success: function (data) {
            if (data.operator == true) {
                Dialog.alert("失败", data.message);
                return false;
            }
            else {
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    dataType: 'json',
                    data: {
                        tableChineseName: tableChineseNameInfo,
                        tableName: tableNameInfo,
                        tableType: tableType,
                        createType: 1,
                        parentId: parentIdInfo,
                        tableDesc: tableDescInfo
                    },
                    url: springUrl + "/form/formBusinessTable_addSave",
                    success: function (data) {
                        window.location.href = springUrl + "/form/formBusinessField_index?id=" + data.primaryKey + "&tableType=" + tableType + "&tableId="+parentIdInfo;
                    },
                    error: function (data) {
                        if (data.responseText == null) {
                            Dialog.alert("失败", "新建数据表失败");
                        } else {
                            Dialog.alert("失败", data.responseText);
                        }
                    }
                });
            }
        },
        error: function (data) {
            if (data.responseText == null) {
                Dialog.alert("失败", "新建数据表失败");
            } else {
                Dialog.alert("失败", data.responseText);
            }
        }
    });
}

function updateTable() {
    //新增数据项
    var idInfo = $('#id').val();
    var tableChineseNameInfo = $('#ChineseName').val();
    var tableDescInfo = $('#tableDesc').val();
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        data: {
            tableChineseName: tableChineseNameInfo,
            id: idInfo
        },
        url: springUrl + "/form/formBusinessTable_isExistTableChineseName",
        success: function (data) {
            if (data.operator == true) {
                Dialog.alert("失败", data.message);
                return false;
            }
            else {
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    dataType: 'json',
                    data: {
                        tableChineseName: tableChineseNameInfo,
                        tableDesc: tableDescInfo,
                        id: idInfo
                    },
                    url: springUrl + "/form/formBusinessTable_updateSave",
                    success: function (data) {
                        if (flag == 0) {
                            Table.render(table);
                        }
                        else {
                            Table.render(subTable);
                        }
                    },
                    error: function (data) {
                        if (data.responseText == null) {
                            Dialog.alert("失败", "更新数据表失败");
                        } else {
                            Dialog.alert("失败", data.responseText);
                        }
                    }
                });
            }
        },
        error: function (data) {
            if (data.responseText == null) {
                Dialog.alert("失败", "更新数据表失败");
            } else {
                Dialog.alert("失败", data.responseText);
            }
        }
    });
}
