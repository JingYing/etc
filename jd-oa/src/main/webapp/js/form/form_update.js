/**
 * Created with IntelliJ IDEA.
 * Desctription:编辑表单
 * User: yujiahe
 * Date: 13-9-14
 * Time: 下午2:36
 * To change this template use File | Settings | File Templates.
 */
/**
 * 初始化操作
 */
$(document).ready(function () {
    bindValidate($("#editFormForm"));// 绑定验证
});
/**
 * 表单编辑后保存按钮触发事件
 */
var check = true;//校验标志位
function updateForm() {
    //判断当前表单名称是否存在
    var oldFormName = $("#oldFormName").val();
    var editFormName = $('#editFormName').val();
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        async:false,
        data: {
            formName: editFormName,
            parentId:0
        },
        url: springUrl + "/form/form_isExistForm",
//        beforeSend: function () {//提交前校验
//            $("#editFormName").trigger("change.validation", {submitting: true});
//            if ($("#editFormName").jqBootstrapValidation("hasErrors")) {
//                check = false;
//            } else {
//                check = true;
//            }
//            return   check;
//        },
        success: function (data) {
            if ((data == true) && (editFormName != oldFormName)) { //如果表单名称改变且数据库存在已存在该表明则需要重置
                Dialog.alert("失败", "已存在相同名称表单，请重置");
            } else {
                updataSaveForm();
            }
        },
        error: function (data) {
            Dialog.alert("失败", data.responseText);
        }

    });

    return check;
}
/**
 * 保存编辑表单事件
 */
function updataSaveForm() {
    var editFormId = $('#editFormId').val();
    var editParentId = $("#editParentId").val();
    //新增数据项
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        async:false,
        data: {
            formName: $('#editFormName').val(),
            formDesc: $("#editFormDesc").val(),
//            parentId: editParentId,
            id: editFormId
        },
        url: springUrl + "/form/form_updateSave",
        success: function (data) {
            if(editParentId==null||editParentId==''){
                Table.render(formTable);
            } else{
                var editformItem = document.getElementById("editformItem").contentWindow.document;
                editformItem.location.reload();
            }
            Dialog.alert("系统提示","表单详情更新成功！")


        },
        error: function (data) {
            if (data.responseText == null) {
                Dialog.alert("失败", "新建表单失败");
            } else {
                Dialog.alert("失败", data.responseText);
            }
        }
    });
}
