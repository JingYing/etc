function updateDescText(form) {
	description.innerText = "aaaaaaaaaaa";
}
function execMyCommand() {

}

function getForm(){
	return document.frmMain;
}
function checkNum(obj){
	 var re = /^[1-9]\d*$/;
     if (document.getElementById(obj).value != '' && !re.test(document.getElementById(obj).value)){
       // alert("必须为正整数!");
    	 document.getElementById(obj).value="";
    	 document.getElementById(obj).focus();
        return false;
     }
     return true;
} 

// 根据uiType,获得当前UI组件配置
function getUIComponentConfig(uiType){
	if(uiType=='部门字典'){
		return getDeptTree();
	}else if(uiType=='单行'){
		return getTextSetting();
	}else if(uiType=='货币'){
		return getMoneySetting();
	}else if(uiType=='多行'){
		return getTextAreaSetting();
	}else if(uiType=='树型数据选择'){
		return 树型数据选择_GetSetting();
	}else if(uiType=='字段子表'){
		if(document.frmMain.AWS_SUBSHEET_DISPLAY.value==""){
			alert("请选择字段子表数据源");
			return false;
		}else{
			return document.frmMain.AWS_SUBSHEET_DISPLAY.value;
		}
	}
}

function 树型数据选择_GetSetting(){
	var items=["AWS_COMBOX_SQLSELECT", "did", "pid", "useFields", "isMulSelect", "isColumnTree", "columnInfo", "iconCls", "cls", "AWS_COMBOX_SQLSHOW", "AWS_COMBOX_SQLGET", "fillBackFields", "DBSOURCE"];
	if(document.frmMain.AWS_COMBOX_SQLSELECT.value==""){
		alert("请输入根节点SQL");
		return false;
	}else if(document.frmMain.AWS_COMBOX_SQLSHOW.value==""){
		alert("请输显示字段");
		return false;
	}else if(document.frmMain.AWS_COMBOX_SQLGET.value==""){
		alert("请输入取值字段");
		return false;
	}else if(document.frmMain.fillBackFields.value==""){
		alert("请输入回填字段");
		return false;
	}
	
	createConfig(items);
}

function getMoneySetting(){
	var cons = document.getElementById('AWS_CHECK_CONS').value;
	return cons == ""?"￥":cons;
}

//根据displaySQL初始化UI组件界面
function setUIComponentConfig(uiType, displaySQL){
	if(uiType=='部门字典'){
		setDeptTree(displaySQL);
		Ext.onReady(function(){
			setDeptTreeLS("deptIdText","id");
			setDeptTreeLS("deptNameText","name");
			setVisable('TDID',$("deptId").checked); 
			setVisable('TDNAME',$("deptName").checked); 
		});
	}else if(uiType=='多行'){
	}
}

function getTextAreaSetting(){
	var items = [ 'shortCuts', 'AWS_UI_SELECT' ];
	var validateShortCuts = "";
	var validateSelect = "";
	for ( var i = 0; i < items.length; i++) {
		var el = document.getElementsByName(items[i]);
		if (el[0].type == 'checkbox') {
			for ( var jj = 0; jj < el.length; jj++) {
				if (el[jj].checked) {
					validateShortCuts += el[jj].value+",";
				}
			}
		} else if (el[0].tagName == 'SELECT') {
			validateSelect = el[0].value;
		}
	}
	if (validateShortCuts.length == 0 && validateSelect.length == 0) {
		return "";
	} else if (validateShortCuts.length != 0 && validateSelect.length != 0){
		var v= "<shortCuts>"+validateShortCuts.substring(0,validateShortCuts.length-1)+"</shortCuts><AWS_UI_SELECT>"+validateSelect+"</AWS_UI_SELECT>";
		return v;
	}else {
		alert("请选择快捷键");
		return false;
	}
}



function setDeptTreeLS(applyTo,type){ 
	 var metaDataId=0;
	 if(parent.document.getElementsByName("metaDataId")!=null && parent.document.getElementsByName("metaDataId").length>0){
		 metaDataId="M"+parent.document.getElementsByName("metaDataId")[0].value;
	 }else if(parent.document.getElementsByName("diggerMapId")!=null && parent.document.getElementsByName("diggerMapId").length>0){
		 metaDataId= "D"+parent.document.getElementsByName("diggerMapId")[0].value;
	 }
	 var searchStore = new Ext.data.Store({ 
		proxy : new Ext.data.HttpProxy({ 
			method : 'POST', 
			url : "./login.wf" 
		}), 
		baseParams : { 
			sid : document.getElementById("sid").value, 
			cmd : 'Display_Editor_Depart_Search', 
			metaDataId:metaDataId, 
			searchType:type
		}, 
		reader : new Ext.data.JsonReader({ 
			root : 'rows', 
			totalProperty : 'totalCount' 
		}, ['value']) 
	}); 
	var resultTpl = new Ext.XTemplate( 
			'<table border="0" cellspacing="0" cellpadding="0" width="100%" class="x-combo-list-talbe-ist">', 
			'<tpl for=".">', 
			'<tr class="x-combo-list-item search-item"><td width="50%" >{value}</td></tr>', 
			'</tpl>', '</table>' 
	); 
	var search = new Ext.form.ComboBox({ 
		store : searchStore, 
		applyTo : applyTo, 
		typeAhead : false, 
		loadingText : 'Searching...', 
		pageSize : 10, 
		valueField:'', 
		listWidth : 300, 
		hideTrigger : true, 
		tpl : resultTpl, 
		minChars : 1, 
		frame : true, 
		tpl : resultTpl, 
		onSelect : function(record, index) { 
			var params=record.get("value"); 
	         if(params.indexOf('<')!=-1){
	        	 params=params.substring(0,params.indexOf('<')); 
	         }
			document.getElementById(applyTo).value = params; 
			this.collapse(); 
		} 
	}); 
}

// UI-部门字典
function getDeptTree(){
	var f=getForm();
	var cfg={};
	cfg.range=getRadioValue("range");
	cfg.fullPath=f.fullPath.checked;
	cfg.fillId=f.deptId.checked;
	cfg.fillName=f.deptName.checked;
	var setFill=false;
	if(cfg.fillId){
		if(f.deptIdText.value==""){
			alert("请输入部门ID回填字段");
			f.deptIdText.focus();
			document.getElementById('displayEditor').value="";
			return false;
		}
		cfg.idfn=f.deptIdText.value;
		setFill=true;
	}
	if(cfg.fillName){
		if(f.deptNameText.value==""){
			alert("请输入部门名称回填字段");
			f.deptNameText.focus();
			document.getElementById('displayEditor').value="";
			return false;
		}
		cfg.namefn=f.deptNameText.value;
		setFill=true;
	}

	if(!setFill){
		f.displayEditor.value="";
		alert("请设置回填字段");
		return false;
	}
	document.getElementById('displayEditor').value=Ext.encode(cfg);
}

function setDeptTree(displaySQL){
	var f=getForm();
	if(displaySQL==null || displaySQL==''){
		checkRadio("range",0);
	}else{
		try {
			var cfg = Ext.decode(displaySQL);
			checkRadio("range",cfg.range);
			f.fullPath.checked=cfg.fullPath;
			f.deptId.checked=cfg.fillId;
			f.deptName.checked=cfg.fillName;
			if(cfg.fillId){
				f.deptIdText.value=cfg.idfn;
			}
			
			if(cfg.fillName){
				f.deptNameText.value=cfg.namefn;
			}
		} catch (e) {
		}
	}
}

function execMyCommand2(form, id, mycmd) {
	form.cmd.value = mycmd;
	form.id.value = id;
	form.target = "_self";
	disableAll(form);
	form.submit();
	return false;
}

function storeCaret(textEl) {
	if (textEl.createTextRange)
		textEl.caretPos = document.selection.createRange().duplicate();
}
function insertAtCaret(textEl, text) {
	if (textEl.createTextRange && textEl.caretPos) {
		var caretPos = textEl.caretPos;
		caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' '
				? text + ' '
				: text;
	} else
		textEl.value = text;

}
// 显示设置
function setVisable(id,v) {
	var E=Ext.get(id);
	if(E!=null){
		E.setVisibilityMode(Ext.Element.DISPLAY);
		E.setVisible(v);
	}
}

//显示设置
function visableText(name) {
	try {
		var textStyle = eval('frmMain.' + name + 'Text').style.display;
		if (textStyle == 'none') {
			eval('frmMain.' + name + 'Text').style.display = '';
		} else {
			eval('frmMain.' + name + 'Text').style.display = 'none';
		}
	} catch (e) {
		alert("方法：function visableText(name)\n" + e.description);
	}
}

function visableKnowledge(name){
	
		if(document.getElementById('kmCard').checked){
			
			 document.getElementById('knowledgeText').style.display = '';	
		}else{
			 document.getElementById('knowledgeText').style.display = 'none';	
		}
}

function visableTr(name) {
		try {
			var textStyle = eval(name + 'Text').style.display;
			var obj=document.getElementsByName('upType');
			var flag=false;
			for(var i=0;i<obj.length;i++){
					if(obj[i].checked&&obj[i].value==name){
						flag=true;
						break;
					}
				}
			if(flag){
					eval(name + 'Text').style.display = '';
					if(name == 'office'){
						 document.getElementById('webOfficeTr').style.display = '';	
					}
			}else {
					eval(name + 'Text').style.display = 'none';
					document.getElementById('webOfficeTr').style.display = 'none';
			}
		} catch (e) {
			alert("方法：function visableTr(name)\n" + e.description);
	}
}
//附件属性配置-上传类型限制
function setUpExtComboboxValue(){
	var cmp=Ext.getCmp('upExtCom');
 	var officeType='doc;docx;xls;xlsx;ppt;pptx;pdf';
	var picType='jpg;gif;tiff;bmp;jpeg2000;psd;png;svg;pcx;dxf;wmf;emf;eps;tga';
	var picAndOfficeType='jpg;jif;tiff;bmp;jpeg2000;psd;png;svg;pcx;dxf;wmf;emf;eps;tga;doc;docx;xls;xlsx;ppt;pptx;pdf';
 	var otherType='';
 	var upTypeCheckBox=document.getElementsByName('upType');
	var upTypeValue='';
 	for(var i=0;i<upTypeCheckBox.length;i++){
 		if(upTypeCheckBox[i].checked){
 			upTypeValue=upTypeValue+upTypeCheckBox[i].value+',';
 		}
 	}
	if(upTypeValue.trim().length>0){
		if(upTypeValue.indexOf(',')!=-1){
			upTypeValue=upTypeValue.substring(0,upTypeValue.length-1);
		}
	}
	if(upTypeValue=='pic' || upTypeValue=='pic,knowledge'){
		cmp.setValue(picType);
		picText.style.display = '';
		sendNextStepTr.style.display = 'none';
		document.getElementById('sendNextStep').checked = false;
		document.getElementById('upExt2').value=picType;
	}else if(upTypeValue=='office' || upTypeValue=='office,knowledge' ){
		cmp.setValue(officeType);
		picText.style.display = 'none';
		sendNextStepTr.style.display = '';
		document.getElementById('upExt2').value=officeType;	
	}else if(upTypeValue=='other'){
		cmp.setValue(otherType);
		picText.style.display = 'none';	
		sendNextStepTr.style.display = '';
		document.getElementById('upExt2').value=otherType;
	}else if(upTypeValue=='pic,office'||upTypeValue=='office,pic' || upTypeValue=='pic,office,knowledge'){
		cmp.setValue(picAndOfficeType);
		picText.style.display = 'none';	
		sendNextStepTr.style.display = '';
		document.getElementById('upExt2').value=picAndOfficeType;
	}else {
		cmp.setValue('');	
		picText.style.display = 'none';	
		sendNextStepTr.style.display = '';
		document.getElementById('upExt2').value='';
	}
}
// 
function visableTextFormUIComponentSetting(name) {
	try {
		var radio = document.getElementsByName(name);
		for (var i = 0; i < radio.length; i++) {
			if (radio[i].checked) {
				var returnTagValue = radio[i].value;// 这里得到单选按钮值
				if (returnTagValue == '0') {
					document.getElementById('trSelectDataSource').style.display='none';
				}
				if (returnTagValue == '1') {
					document.getElementById('trSelectDataSource').style.display='';
				}
			}
		}
	} catch (e) {
		
	}
}

//初始化选项卡
function initLayOutConfig(cb){
	cb = unescape(cb);
	if(cb.indexOf('{')!=-1&&cb.lastIndexOf('}')!=-1){
		var index=cb.lastIndexOf('}');
		frmMain.AWS_LAYOUT_CONFIG.value=cb.substring(0,index+1);
		cb=cb.substring(index+1);
		var children = cb.split(',');
		frmMain.AWS_LAYOUT_WIDTH.value=children[1];
		frmMain.AWS_LAYOUT_HEIGHT.value=children[2];
	}
}
// 对列表进行配置
function creatComboxConfig(cbType, cbId) {
	try {
		if (cbType == '0') {
			var inputValue = document.getElementById(cbId).value;
			if (inputValue.length == 0) {
				alert('请填写配置信息');
				return false;
			}
			frmMain.displayEditor.value = inputValue;
		} else if (cbType == '1') {
			// SQL>COMPANYNAME:ID|select * from orgcompany
			var inputValue = 'SQL>'
					+ document.getElementById('AWS_COMBOX_SQLGET').value + ':'
					+ document.getElementById('AWS_COMBOX_SQLSHOW').value + '|'
					+ document.getElementById('AWS_COMBOX_SQLSELECT').value;
			frmMain.displayEditor.value = inputValue;
		}

	} catch (e) {
		alert("方法：function creatComboxConfig(name)\n" + e.description);
	}
}
// 对列表进行‘显示’配置
function visableCombox(name) {
	try {
		var radio = document.getElementsByName(name);
		for (var i = 0; i < radio.length; i++) {
			if (radio[i].checked) {
				var returnTagValue = radio[i].value;// 这里得到单选按钮值
				if (returnTagValue == '常量参考') {
					consText.style.display = '';
					consTrea.style.display = '';
					sqlTrea.style.display = 'none';
					//document.getElementById('cascadeTable').style.display='none';
				}
				if (returnTagValue == 'SQL数据') {
					sqlTrea.style.display = '';
					consText.style.display = 'none';
					consTrea.style.display = 'none';
					//document.getElementById('cascadeTable').style.display='';
				}
			}
		}
	} catch (e) {
		alert("方法：function visableRadio(name)\n" + e.description);
	}
}

//初始化选项卡
function initLayOutConfig(cb){
	cb = unescape(cb);
	if(cb.indexOf('{')!=-1&&cb.lastIndexOf('}')!=-1){
		var index=cb.lastIndexOf('}');
		frmMain.AWS_LAYOUT_CONFIG.value=cb.substring(0,index+1);
		cb=cb.substring(index+1);
		var children = cb.split(',');
		frmMain.AWS_LAYOUT_WIDTH.value=children[1];
		frmMain.AWS_LAYOUT_HEIGHT.value=children[2];
	}
}
// 对列表进行配置
function creatComboxConfig(cbType, cbId) {
	try {
		if (cbType == '0') {
			var inputValue = document.getElementById(cbId).value;
			if (inputValue.length == 0) {
				alert('请填写配置信息');
				return false;
			}
			frmMain.displayEditor.value = inputValue;
		} else if (cbType == '1') {
			// SQL>COMPANYNAME:ID|select * from orgcompany
			var inputValue = 'SQL>'
					+ document.getElementById('AWS_COMBOX_SQLGET').value + ':'
					+ document.getElementById('AWS_COMBOX_SQLSHOW').value + '|'
					+ document.getElementById('AWS_COMBOX_SQLSELECT').value;
			frmMain.displayEditor.value = inputValue;
		}

	} catch (e) {
		alert("方法：function creatComboxConfig(name)\n" + e.description);
	}
}
// 对列表进行初始化回填
function initComboxConfig(cb) {
	try {
		cb = unescape(cb);
		var tempCb=cb.toUpperCase();
		var radio = document.getElementById('AWS_RADIO');
		if (tempCb.indexOf('SQL') != -1 ) {
			sqlTrea.style.display = '';
			radio.children[1].checked = true;
			cb = cb.trim().substring(4);
			var children = cb.split("|");
			frmMain.AWS_COMBOX_SQLSELECT.value = children[1];
			if(children.length>2){
				selectList("DBSOURCE",children[2]);
			}
			
			children = children[0].split(":");
			var getValue="";var showValue="";
			if(typeof(children[0])!='undefined'){
				showValue=children[0];
			}
			if(typeof(children[1])!='undefined'){
				getValue=children[1];
			}
			frmMain.AWS_COMBOX_SQLSHOW.value =getValue ;
			frmMain.AWS_COMBOX_SQLGET.value = showValue;
		} else if(tempCb.indexOf('CASCADE') != -1){
			sqlTrea.style.display = '';
			radio.children[1].checked = true;
			cb = cb.trim().substring(cb.indexOf('>')+1,cb.length);
			var children = cb.split("|");
			frmMain.AWS_COMBOX_SQLSELECT.value = children[1];
			children = children[0].split(":");
			var getValue="";
			var showValue="";
			if(typeof(children[0])!='undefined'){
				showValue=children[0];
			}
			if(typeof(children[1])!='undefined'){
				getValue=children[1];
			}
			frmMain.AWS_COMBOX_SQLSHOW.value =getValue ;
			frmMain.AWS_COMBOX_SQLGET.value = showValue;
		}else{
			frmMain.AWS_COMBOX_CONS.value = cb;
			radio.children[0].checked = true;
			consText.style.display = '';
			consTrea.style.display = '';
		}

	} catch (e) {
		alert("方法：function initComboxConfig(name)\n" + e.description);
	}
}

//树型数据回填控件生成配置使用 cy@2008.12.19
function createConfig(items) {
	var config = "";
	for (var i = 0; i < items.length; i++) {
		var el = document.getElementsByName(items[i]);
		if (el.length > 1 && el[0].tagName == "INPUT" && el[0].type == "radio") {
			for (var ii = 0; ii < el.length; ii++) {
				if (el[ii].checked == true) {
					config += "<" + items[i] + ">" + ii + "</" + items[i] + ">";
					break;
				}
			}
		} else if (el[0].type == 'text' || el[0].type == 'textarea'	|| el[0].tagName == 'SELECT') {
			config += "<" + items[i] + ">" + el[0].value + "</" + items[i]+ ">";
		} else if (el[0].type == 'checkbox') {
			config += "<" + items[i] + ">"
			for (var jj = 0; jj < el.length; jj++) {
				if (el[jj].checked == true) {
					config += el[jj].value + ",";
				}
			}
			for (var kk = 0; kk < el.length; kk++) {
				if (el[kk].checked == true) {
					var num = config.length;
					config = config.substring(0, num - 1);
					break;
				}
			}

			config += "</" + items[i] + ">";
		}
	}
   frmMain.displayEditor.value = config;
}

function initConfig(items) {
	var result = parent.frmMain.displaySQL.value;
	if (result.length > 0) {
		for (var i = 0; i < items.length; i++) {
			var el = document.getElementsByName(items[i]);
			if (el.length > 1 && el[0].tagName == "INPUT"
					&& el[0].type == "radio") {
				var index = getFieldValue(items[i], result);
				if (index.length == 0) {
					index = 0;
				}

				el[index].checked = true;
			} else {
				if (el[0].type == 'text' || el[0].type == 'textarea'
						|| el[0].tagName == 'SELECT') {
					el[0].value = getFieldValue(items[i], result);
				} else if (el[0].type == 'checkbox') {
					var elCheckValue = getFieldValue(items[i], result);
					for (var j = 0; j < el.length; j++) {
						if (elCheckValue.indexOf(el[j].value) != -1) {
							el[j].checked = true;
						}
					}
				}
			}
		}
	}
}

// 初始化树型数据回填控件界面 cy@2008.12.19
function initTreeFillbackConfig(items) {
	var result = parent.frmMain.displaySQL.value;
	if (result.length > 0) {
		for (var i = 0; i < items.length; i++) {
			var el = document.getElementsByName(items[i]);
			if (el.length > 1 && el[0].tagName == "INPUT"
					&& el[0].type == "radio") {
				var index = getFieldValue(items[i], result);
				if (index.length == 0) {
					index = 0;
				}

				el[index].checked = true;
			} else {
				if (el[0].type == 'text' || el[0].type == 'textarea'
						|| el[0].tagName == 'SELECT') {
					el[0].value = getFieldValue(items[i], result);
				} else if (el[0].type == 'checkbox') {
					var elCheckValue = getFieldValue(items[i], result);
					for (var j = 0; j < el.length; j++) {
						if (elCheckValue.indexOf(el[j].value) != -1) {
							el[j].checked = true;
						}
					}
				}
			}
		}
	}
}

function getMulComboxEditor(){
	var radioValues=document.getElementsByName('upType');
	var str={};
	str.type=radioValues[1].checked==true;
	str.sqlGet= $('AWS_COMBOX_SQLGET').value;
	str.sqlShow=$('AWS_COMBOX_SQLSHOW').value;
	str.sqlSelect=$('AWS_COMBOX_SQLSELECT').value;
	str.con=$('AWS_COMBOX_CONS').value;
	str.separated=getRadioValue('separated');
	str.listWidth=$("listWidth").value;
	str.maxHeight=$("maxHeight").value;
	str.DBSOURCE=$("DBSOURCE").value;
	
	if(radioValues[1].checked==true){
		if(str.sqlGet.replace(/^(\s)|(\s)$/g,'')==''||str.sqlSelect.replace(/^(\s)|(\s)$/g,'')==''){
			frmMain.displayEditor.value='';
			return false;
		}
	}else {
		if(str.con.replace(/^(\s)|(\s)$/g,'')==''){
			frmMain.displayEditor.value='';
			return false;
		}
	}

	var radioValues=document.getElementsByName('editable');
	str.editable=radioValues[1].checked;
	frmMain.displayEditor.value=Ext.encode(str);
}

//对列表进行初始化回填
function initAWSMulComboxConfig(str) {
	if(str=='undefined' || typeof(str)!='object'){
		str = {type:false,sqlGet:"",sqlShow:"",sqlSelect:"",con:"",listWidth:420,maxHeight:300,separated:" ",editable:false,DBSOURCE:""};
	}
	
	var radioValues=document.getElementsByName('upType');
	$('AWS_COMBOX_SQLGET').value=str.sqlGet;
	$('AWS_COMBOX_SQLSHOW').value=str.sqlShow;
	$('AWS_COMBOX_SQLSELECT').value=str.sqlSelect;
	$('AWS_COMBOX_CONS').value=str.con;
	$("listWidth").value=str.listWidth;
	$("maxHeight").value=str.maxHeight;
	
	checkRadio("separated",str.separated);
	selectList("DBSOURCE",str.DBSOURCE);
	if (str.type==true) {
		sqlTrea.style.display = '';
		radioValues[1].checked = true;
	}else{
		radioValues[0].checked = true;
		consText.style.display = '';
		consTrea.style.display = '';
	}
	
	var radioValues=document.getElementsByName('editable');
	if(str.editable){
		radioValues[1].checked = true;
	}else{
		radioValues[0].checked = true;
	}
}

// 对附件进行初始化回填
function initFileConfig(items,result) {
//	var result = parent.frmMain.displaySQL.value;
	if (result.length > 0) {
		for (var i = 0; i < items.length; i++) {
			var el = document.getElementsByName(items[i]);
			if (el[0].type == 'text' || el[0].type == 'textarea'|| el[0].tagName == 'SELECT') {
				el[0].value = getFieldValue(items[i], result);
			} else if (el[0].type == 'checkbox') {
				var elCheckValue = getFieldValue(items[i], result);
				for (var j = 0; j < el.length; j++) {
					if (elCheckValue.indexOf(el[j].value) != -1) {
						el[j].checked = true;
						if (el[j].value == 'pic') {
							picText.style.display = '';
						} else if (el[j].value == 'office') {
							officeText.style.display = '';
						}
					}
				}
			} else if (el[0].type == "radio") {
				if(result=='null'){
					el[1].checked = true;
					upBroText.style.display = 'none';
					continue;
				}
				var index = getFieldValue(items[i], result);
				if (index.length == 0) {
					index = 0;
				}
				el[index].checked = true;
				if (index == 0) {
					if (items[i] == 'upPre') {
						upPreText.style.display = '';
					} else if (items[i] == 'upBro') {
						upBroText.style.display = '';
					}
				}
			}
		}
	}
}
// 对input框进行配置
function creatInputConfig(cbId) {
	var inputValue = document.getElementById(cbId).value;
	frmMain.displayEditor.value = inputValue;
}
// 初始化回填
function initInputConfig(cb, cbId) {
	cb = unescape(cb);
	eval('frmMain.' + cbId).value = cb;
}

// 初始化回填
function initButtonConfig(cb) {
	cb = unescape(cb);
	var children = new Array(2);
	if (cb.length > 3) {
		if (cb.indexOf(':') > 9) {
			children = cb.split(":");
			frmMain.buttonValue.value = children[1];
		} else if (cb.indexOf('=') > 2) {
			children = cb.split('=');
			frmMain.buttonValue.value = children[1];
		} else {
			children[0] = 'JAVABEAN';
			frmMain.buttonValue.value = cb;
		}
		var obj = document.getElementById("buttonType");
		for (i = 0; i < obj.children.length; i++) {
			if (obj.children[i].value == children[0].toUpperCase())
				obj.children[i].checked = true;
		}
	}
}
function visableTd(name) {
	var radio = document.getElementsByName(name);
	for (var i = 0; i < radio.length; i++) {
		if (radio[i].checked) {
			var returnTagValue = radio[i].value; // 这里得到单选按钮值
			if (returnTagValue == '是') {
				eval(name + 'Text').style.display = '';
			} else {
				eval(name + 'Text').style.display = 'none';
			}
		}
	}

}
// 打开Sql语句模拟使用窗口
function openAnalog(form,uiType) {
	var displayEditor = form.displayEditor.value;
	var valueFiled = form.AWS_COMBOX_SQLGET.value;
	var valueDisplayFiled="";
	try{
		valueDisplayFiled = form.AWS_COMBOX_SQLSHOW.value;
	}catch(e){}

	var DBSOURCE = "";
	try {
		DBSOURCE = form.DBSOURCE.value;
	} catch (e) {
	}
	var valueSql = form.AWS_COMBOX_SQLSELECT.value;
	var sessionId = form.sid.value;
	parent.analogDialog.purgeListeners();
	parent.analogDialog.addListener('show', function() {
		parent.analogDialogPage.location = encodeURI('./login.wf?sid=' + sessionId
				+ '&cmd=Display_Editor_Sql_Analog&valueFiled=' + valueFiled
				+ '&valueDisplayFiled='+valueDisplayFiled+'&valueSql=' + valueSql+'&uiType='+uiType+'&dbsource='+DBSOURCE);
		this.center();
		return;
	});
	parent.analogDialog.show();
}

// 多行时,检验按键值
function validate(obj) {
	var flag = false;
	if (obj.value != "") {
		var shortCuts = document.getElementsByName("shortCuts");
		for (var i = 0; i < shortCuts.length; i++) {
			if (shortCuts[i].checked) {
				flag = true;
			}
		}
		if (!flag) {
			alert("请选择辅助键");
			return false;
		}
	}
	if (obj.value.length >= 2) {
		alert("按键值错误,请重新添写");
		return false;
	}
}
// 对附件进行‘显示‘配置
function visableCheckBox() {
	var checkBoxs = document.getElementsByName("upType");
	for (var i = 0; i < checkBoxs.length; i++) {
		if (checkBoxs[i].checked) {
			var returnTagValue = checkBoxs[i].value; // 这里得到复选框值
			if (returnTagValue == 'pic') {
				picText.style.display = '';
			} else {
				picText.style.display = 'none';
			}
			if (returnTagValue == 'office') {
				officeText.style.display = '';
			} else {
				officeText.style.display = 'none';
			}
		}
	}

}

function initCombox(items) {
	var result = parent.frmMain.displaySQL.value;
	if (result.length > 0) {
		for (var i = 0; i < items.length; i++) {
			var el = document.getElementsByName(items[i]);
			if (el[0].type == "radio") {
				var index = getFieldValue(items[i], result);
				if (index == 0) {
					consText.style.display = '';
					consTrea.style.display = '';
					sqlTrea.style.display = 'none';
				} else {
					sqlTrea.style.display = '';
					consText.style.display = 'none';
					consTrea.style.display = 'none';
				}
				el[index].checked = true;
			} else if (el[0].type == "text") {
				el[0].value = getFieldValue(items[i], result);
			}
		}
	}
}


function getFieldValue(field, result) {
	var start = "<" + field + ">";
	var end = "</" + field + ">";
	if (result.indexOf(start) > -1 && result.indexOf(end) > -1) {
		return result.substring(result.indexOf(start) + start.length, result.indexOf(end));
	}
	return "";
}

// ---------------生成配置文件
// 生成配置文件
function createFlexAddressUI() {
	var result = '<range>' + getFlexAddressRang() + '</range>' + '\n';
	result += '<selector>' + getOptionValue('checkType') + '</selector>' + '\n';
	result += '<root>' + frmMain.rootInputFrom.value ;
	if(frmMain.rootInputEnd.value != ''){
		result += '|'+frmMain.rootInputEnd.value;		
	}
	 result += '</root>' + '\n';
	var propertise = getPropertiseValue();
	result += '<sourceField>' + propertise[0] + '</sourceField>' + '\n';
	if(propertise[1]==''){
		frmMain.displayEditor.value='';
		return;
	}
	result += '<targetField>' + propertise[1] + '</targetField>' + '\n';
	result += '<leafType>' + getOptionValue('layerType') + '</leafType>' + '\n';
	result += '<layer>' + frmMain.layerInputFrom.value + '|'
			+ frmMain.layerInputEnd.value + '</layer>' + '\n';
	result += '<filterClass>' + frmMain.filterClass.value + '</filterClass>'
			+ '\n';
	result += '<delimiter>' + frmMain.delimiter.value + '</delimiter>'
			+ '\n';
	frmMain.displayEditor.value = result;
}
// 得到选择类型
function getFlexAddressRang() {
	var rang = '';
	if (frmMain.ORG.checked) {
		rang += frmMain.ORG.value + '|';
	}
	if (frmMain.ROLE.checked) {
		rang += frmMain.ROLE.value + '|';
	}
	if (frmMain.TEAM.checked) {
		rang += frmMain.TEAM.value + '|';
	}
	if (rang != '') {
		rang = rang.substr(0, rang.length - 1)
	}
	return rang;
}
// 得到单选的值
function getOptionValue(name) {
	try {
		var option = eval('frmMain.' + name);
		for (var i = 0; i < option.length; i++) {
			if (option[i].checked) {
				return option[i].value;
			}
		}
	} catch (e) {
		alert(1);
		alert(e.description);
	}
	return '';
}
// 组装选定的配置
function getPropertiseValue() {
	var srcField = '';
	var targetField = '';
	if (frmMain.companyId.checked) {
		srcField += frmMain.companyId.value + '|';
		targetField += frmMain.companyIdText.value + '|';
	}
	if (frmMain.CompanyName.checked) {
		srcField += frmMain.CompanyName.value + '|';
		targetField += frmMain.CompanyNameText.value + '|';
	}
	if (frmMain.deptId.checked) {
		srcField += frmMain.deptId.value + '|';
		targetField += frmMain.deptIdText.value + '|';
	}
	if (frmMain.deptName.checked) {
		srcField += frmMain.deptName.value + '|';
		targetField += frmMain.deptNameText.value + '|';
	}
	if (frmMain.userId.checked) {
		srcField += frmMain.userId.value + '|';
		targetField += frmMain.userIdText.value + '|';
	}
	if (frmMain.userUid.checked) {
		srcField += frmMain.userUid.value + '|';
		targetField += frmMain.userUidText.value + '|';
	}
	if (frmMain.userName.checked) {
		srcField += frmMain.userName.value + '|';
		targetField += frmMain.userNameText.value + '|';
	}
	if (frmMain.userNo.checked) {
		srcField += frmMain.userNo.value + '|';
		targetField += frmMain.userNoText.value + '|';
	}
	if (frmMain.roleId.checked) {
		srcField += frmMain.roleId.value + '|';
		targetField += frmMain.roleIdText.value + '|';
	}
	if (frmMain.userEmail.checked) {
		srcField += frmMain.userEmail.value + '|';
		targetField += frmMain.userEmailText.value + '|';
	}
	if (frmMain.positionNo.checked) {
		srcField += frmMain.positionNo.value + '|';
		targetField += frmMain.positionNoText.value + '|';
	}
	if (frmMain.positonName.checked) {
		srcField += frmMain.positonName.value + '|';
		targetField += frmMain.positonNameText.value + '|';
	}
	if (frmMain.msnId.checked) {
		srcField += frmMain.msnId.value + '|';
		targetField += frmMain.msnIdText.value + '|';
	}
	if (frmMain.extend1.checked) {
		srcField += frmMain.extend1.value + '|';
		targetField += frmMain.extend1Text.value + '|';
	}
	if (frmMain.extend2.checked) {
		srcField += frmMain.extend2.value + '|';
		targetField += frmMain.extend2Text.value + '|';
	}
	if (frmMain.extend3.checked) {
		srcField += frmMain.extend3.value + '|';
		targetField += frmMain.extend3Text.value + '|';
	}
	if (frmMain.extend4.checked) {
		srcField += frmMain.extend4.value + '|';
		targetField += frmMain.extend4Text.value + '|';
	}
	if (frmMain.extend5.checked) {
		srcField += frmMain.extend5.value + '|';
		targetField += frmMain.extend5Text.value + '|';
	}

	if (srcField != '') {
		srcField = srcField.substr(0, srcField.length - 1)
	}
	if (targetField != '') {
		targetField = targetField.substr(0, targetField.length - 1)
	}
	var propertise = [srcField, targetField];
	return propertise;
}
// --------------------将原有数据生成界面
function builtFlexAddressUI() {
	// 解析xml 数据 组织数据
	var result = parent.frmMain.displaySQL.value;
	if (result != '') {
		var range = getDisValueElement('range', result);
		setRange(range);
		var selector = getDisValueElement('selector', result);
		// alert(selector);
		setOption('checkType', selector);
		var root = getDisValueElement('root', result);
		setRoot(root);
		var sourceField = getDisValueElement('sourceField', result);
		var targetField = getDisValueElement('targetField', result);
		serPropertise(sourceField, targetField);
		var leafType = getDisValueElement('leafType', result);
		setOption('layerType', leafType);
		var layer = getDisValueElement('layer', result);
		setLayer(layer);
		var filterClass = getDisValueElement('filterClass', result);
		frmMain.filterClass.value = filterClass;
		var delimiter = getDisValueElement('delimiter', result);
		frmMain.delimiter.value = delimiter;
	} else {
		setRange('');
		setOption('checkType', '');
		setOption('layerType', '');
	}
}
// 匹配数据
function getDisValueElement(cmd, result) {
	var resultValue;
	var resultValue2 = '';
	var reg = '<' + cmd + '>' + '[^>]+' + '</' + cmd + '>';
	resultValue = result.match(reg);

	if (resultValue != null) {
		for (var i = 0; i < resultValue.length; i++) {
			resultValue2 = resultValue[i];
			resultValue2 = resultValue2.replace('<' + cmd + '>', '');
			resultValue2 = resultValue2.replace('</' + cmd + '>', '');
			break;
		}
	}
	// alert(resultValue2);
	return resultValue2;
}
// 设置选择类型
function setRange(result) {
	var fieldValues = result.split("|");
	for (var i = 0; i < fieldValues.length; i++) {

		if (fieldValues[i] == 'ORG') {
			frmMain.ORG.checked = true;
		}
		if (fieldValues[i] == 'ROLE') {
			frmMain.ROLE.checked = true;
		}
		if (fieldValues[i] == 'TEAM') {
			frmMain.TEAM.checked = true;
		}
		if (result == '') {
			frmMain.ORG.checked = true;
		}
	}
}
// 设置单选框
function setOption(name, checkValue) {
	try {
		var optionType = eval('frmMain.' + name);
		for (var i = 0; i < optionType.length; i++) {
			if (optionType[i].value == checkValue || checkValue == '') {
				optionType[i].checked = true;
				break;
			}
		}
	} catch (e) {
		alert(e.description);
	}
}
// 设置部门ID根
function setRoot(rootValue) {
	if (rootValue != null && rootValue != '') {  
		frmMain.rootInputFrom.value = rootValue; 
		//var rootValues = rootValue.split('|');
		//if (rootValues.length > 0) {
		//	frmMain.rootInputFrom.value = rootValues[0];
		//}
		//if (rootValues.length > 1) {
		//	frmMain.rootInputEnd.value = rootValues[1];
		//}
	}
}
// 设置 部门层级
function setLayer(layerValue) {
	if (layerValue != null && layerValue != '') {
		var layerValues = layerValue.split('|');
		if (layerValues.length > 0) {
			frmMain.layerInputFrom.value = layerValues[0];
		}
		if (layerValues.length > 1) {
			frmMain.layerInputEnd.value = layerValues[1];
		}
	}
}
// 设置srcfield 和targetfield
function serPropertise(srcValue, targetValue) {
	if (srcValue != null && srcValue != '') {
		var srcValues = srcValue.split('|');
		var targetValues = targetValue.split('|');
		for (var i = 0; i < srcValues.length; i++) {
			if (srcValues[i] == 'ORGCOMPANY.ID') {
				frmMain.companyId.checked = true;
				frmMain.companyIdText.value = targetValues[i];
				frmMain.companyIdText.style.display = '';
			}
			if (srcValues[i] == 'ORGCOMPANY.COMPANYNAME') {
				frmMain.CompanyName.checked = true;
				frmMain.CompanyNameText.value = targetValues[i];
				frmMain.CompanyNameText.style.display = '';
			}
			if (srcValues[i] == 'ORGDEPARTMENT.ID') {
				frmMain.deptId.checked = true;
				frmMain.deptIdText.value = targetValues[i];
				frmMain.deptIdText.style.display = '';
			}
			if (srcValues[i] == 'ORGDEPARTMENT.DEPARTMENTNAME') {
				frmMain.deptName.checked = true;
				frmMain.deptNameText.value = targetValues[i];
				frmMain.deptNameText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.ID') {
				frmMain.userId.checked = true;
				frmMain.userIdText.value = targetValues[i];
				frmMain.userIdText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.UID') {
				frmMain.userUid.checked = true;
				frmMain.userUidText.value = targetValues[i];
				frmMain.userUidText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.USERNAME') {
				frmMain.userName.checked = true;
				frmMain.userNameText.value = targetValues[i];
				frmMain.userNameText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.USERNO') {
				frmMain.userNo.checked = true;
				frmMain.userNoText.value = targetValues[i];
				frmMain.userNoText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.ROLEID') {
				frmMain.roleId.checked = true;
				frmMain.roleIdText.value = targetValues[i];
				frmMain.roleIdText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.EMAIL') {
				frmMain.userEmail.checked = true;
				frmMain.userEmailText.value = targetValues[i];
				frmMain.userEmailText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.POSITION_NO') {
				frmMain.positionNo.checked = true;
				frmMain.positionNoText.value = targetValues[i];
				frmMain.positionNoText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.POSITION_NAME') {
				frmMain.positonName.checked = true;
				frmMain.positonNameText.value = targetValues[i];
				frmMain.positonNameText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.MSNID') {
				frmMain.msnId.checked = true;
				frmMain.msnIdText.value = targetValues[i];
				frmMain.msnIdText.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.EXTEND1') {
				frmMain.extend1.checked = true;
				frmMain.extend1Text.value = targetValues[i];
				frmMain.extend1Text.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.EXTEND2') {
				frmMain.extend2.checked = true;
				frmMain.extend2Text.value = targetValues[i];
				frmMain.extend2Text.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.EXTEND3') {
				frmMain.extend3.checked = true;
				frmMain.extend3Text.value = targetValues[i];
				frmMain.extend3Text.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.EXTEND4') {
				frmMain.extend4.checked = true;
				frmMain.extend4Text.value = targetValues[i];
				frmMain.extend4Text.style.display = '';
			}
			if (srcValues[i] == 'ORGUSER.EXTEND5') {
				frmMain.extend5.checked = true;
				frmMain.extend5Text.value = targetValues[i];
				frmMain.extend5Text.style.display = '';
			}
		}
	}
}	
/**
 * 打开一个编辑XML数据字典的TAB页
 */
function openTabEditDic(){
	var metaDataId=0;
	if(window.parent.parent.document.getElementById('metaDataId')!=null){
		metaDataId=window.parent.parent.document.getElementById('metaDataId').value;
	}
	var url="./login.wf?sid="+document.getElementById('sid').value+"&cmd=Dictionary_Edit_Web&dictName="+document.getElementById('AWS_DICTIONRY').value+"&metaDataId="+metaDataId+"&type=createXD";
	if(parent.parent.parent.parent.document.getElementById('BMPreviewPanelID')!=null){
		parent.parent.parent.parent.createViewTab('AWS_BM_Main_OpenEditDic_Tab_ID', 'XML数据字典编辑', '',url, 'AWS_BM_Main_Workspace_Frame_ID');
	}else{
		window.open(url,'XML数据字典编辑','location=no,menubar=yes,toolbar=no,status=no,directories=no,scrollbars=yes,resizable=yes,width=860,height=500');
	}
	return false;
}
/**
 * 打开一个编辑流水号管理器及通用评分组件workflist
 */
function openTabEditWindow(wfType){
	var	url = encodeURI('./login.wf?sid='+document.getElementById('sid').value+'&cmd=WorkFlow_Execute_Worklist&wfType='+wfType);
	if(parent.parent.parent.parent.document.getElementById('BMPreviewPanelID')!=null){
		parent.parent.parent.parent.createViewTab('AWS_BM_Main_BizNoManager_Tab_ID', '流水号管理器数据维护', '',url, 'AWS_BM_Main_Workspace_Frame_ID');
	}else{
		window.open(url,'流水号管理器数据维护','location=no,menubar=yes,toolbar=no,status=no,directories=no,scrollbars=yes,resizable=yes,width=860,height=500');
	}
	return false;
}
/**
 * 打开电子印章数据维护界面
 */
function openTabEditWindowElect(){
	var	url = encodeURI('./login.wf?sid='+document.getElementById('sid').value+'&cmd=Electroncachet_Manager_Open');
	if(parent.parent.parent.parent.document.getElementById('BMPreviewPanelID')!=null){
		parent.parent.parent.parent.createViewTab('AWS_BM_Main_BizNoManager_Tab_ID', '电子印章数据维护', '',url, 'AWS_BM_Main_Workspace_Frame_ID');
	}else{
		window.open(url,'电子印章数据维护','location=no,menubar=yes,toolbar=no,status=no,directories=no,scrollbars=yes,resizable=yes,width=860,height=500');
	}
	return false;
}
/**
 * 数字校验
 * @param {} obj
 * @return {Boolean}
 */
function checkNumValue(id,obj){
	var tst = /^\d+$/.test(obj.value);
	if(!tst){
		if(id=='upSize'){
			alert('上传单个文件大小必须是一个有效的数值');
		}else if(id=='upBroWidth'){
			alert('直接浏览宽度必须是一个有效的数值');
		}else if(id=='upBroHeight'){
			alert('直接浏览高度必须是一个有效的数值');
		}else if(id=='upPreWidth'){
			alert('预览宽度必须是一个有效的数值');
		}else if(id=='upPreHieght'){
			alert('预览宽度必须是一个有效的数值');
		}	
		return false;
	}else {
		return true
	}
}

function getRadioValue(name, def){
	var rs = document.getElementsByName(name);
	for(var i=0;i<rs.length;i++){
		if(rs[i].checked==true){
			return rs[i].value;
		}
	}
	return def==null ? rs[0].value : def;
}
function getSelectValue(selectTagId){
	 var obj = document.getElementById(selectTagId); 
	 return obj.value;
}
function checkRadio(name,val){
	var rs = document.getElementsByName(name);
	for(var i=0;i<rs.length;i++){
		if(rs[i].value==val){
			rs[i].checked = true;
		}
	}
}
/**
 * 数据源配置 动态回载comboxgrid控件
 */
$(function(){
	if($('#selectDataSource').length > 0){
		 $('#selectDataSource').combogrid({
			 panelWidth:500,
			 panelHeight:100,
			 url: baseUrl + '/di/datasource/datasource_get',
			 idField:'id',
			 textField:'name',
			 mode:'remote',
			 method : 'GET',
			 fitColumns:true,
			 columns:[[
				 {field:'id',title:'Item ID',width:60,hidden:true},
				 {field:'name',title:'名称',width:150},
				 {field:'dataAdapter',title:'数据源',align:'left',width:150}
			   ]],
			 filter: function(q, row){
					var opts = $(this).combogrid('options');
					return row[opts.textField].indexOf(q) == 0;
			  }
		   });
		 var mode = $(this).val();
		 $('#selectDataSource').combogrid({
			 mode: mode
		 });
	}
});
/**
 * 打开数据源映窗口
 * @param dataSourceId
 * @param dataSourceName
 */
function openDataSourceMappingDialog(){
	var dataSource = $('#selectDataSource').combogrid('grid').datagrid('getSelected');
	var formId = document.getElementById('formId').value;
	var fieldId = document.getElementById('fieldId').value;
	parent.openDataSourceMappingDialog(dataSource.id,dataSource.name,formId,fieldId);
	return false;
}