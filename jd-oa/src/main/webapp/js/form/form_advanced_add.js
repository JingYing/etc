function getDemoTheme() {
	return "";
};

$(document).ready(
	function() {
		var theme = getDemoTheme();
		var tableId = $("#tableId").val();
		// prepare the data
		var source_mainTables = {
			datatype : "json",
			datafields : [ {
				name : 'tableChineseName'
			}, {
				name : 'id'
			} ],
			id : 'id',
			url : springUrl + "/form/formBusinessTable_advanced"
		};
		var dataAdapter_mainTables = new $.jqx.dataAdapter(source_mainTables);
		// Create a jqxListBox
		$("#allMainTable").jqxListBox({
			source : dataAdapter_mainTables,
			displayMember : "tableChineseName",
			valueMember : "id",
			width : 200,
			height : 280
		});
		
		var selectTableId='';
		$("#allMainTable").on('select', function(event) {
			if (event.args) {
				var item = event.args.item;
				if (item) {
					selectTableId = item.value;
					source_allFileds.url = springUrl + "/form/selectAllFieldsByTableId?tableId=" + selectTableId;
					source_selectFileds.url = springUrl + "/form/selectedFieldsByTableId?tableId=" + selectTableId;
					$("#allFields").jqxListBox('refresh');
					$("#selectFiled").jqxListBox('refresh');
				}
			}
		});
	
		// prepare the all data
		var source_allFileds = {
			datatype : "json",
			datafields : [ {
				name : 'fieldName'
			},{
				name:"fieldId"
			} ],
			id : 'fieldId',
			url : springUrl + "/form/selectAllFieldsByTableId?tableId=" + tableId
		};
		var dataAdapter_allFileds = new $.jqx.dataAdapter(source_allFileds);
		
		$("#allFields").jqxListBox({
			allowDrop : true,
			allowDrag : true,
			source : dataAdapter_allFileds,
			width : 300,
			height : 280,
			displayMember : "fieldName",
			valueMember : "fieldId",
			theme : theme,
			dragStart : function(item) {
				var mark = item.label.split("-");
				if (mark[0] == "主表"
						|| mark[0] == "子表") {
					return false;
				}
			},
			renderer : function(index, label, value) {
				var mark = label.split("-");
				if (mark[0] == "主表"
						|| mark[0] == "子表") {
					return "<span style='background-color:#CAF2FC;font-size:14px;'>"
							+ label + "</span>";
				}
				return label;
			}
		});
	
		var data2 = [ "主表-DBCPMAIN", "子表-DBCPSunTable1",
						"子表-DBCPSunTable2" ];
		
		// prepare the select data
		var source_selectFileds = {
			datatype : "json",
			datafields : [ {
				name : 'fieldName'
			},{
				name:"fieldId"
			} ],
			id : 'fieldId',
			url : springUrl + "/form/selectedFieldsByTableId?tableId=" + tableId
		};
		var dataAdapter_selectFileds = new $.jqx.dataAdapter(source_selectFileds);
		
		$("#selectFiled").jqxListBox({
			allowDrop : true,
			allowDrag : true,
			source : dataAdapter_selectFileds,
			width : 300,
			height : 280,
			displayMember : "fieldName",
			valueMember : "fieldId",
			theme : theme,
			dragEnd : function(dragItem, dropItem) {
				var mark = dragItem.label.split("-");
				if (mark[0] == "主表"
						|| mark[0] == "子表") {
					return false;
				}
			},
			renderer : function(index, label, value) {
				if(typeof(label)!='undefined'){
					var mark = label.split("-");
					if (mark[0] == "主表"
							|| mark[0] == "子表") {
						return "<span style='background-color:#CAF2FC;font-size:14px;'>"
								+ label + "</span>";
					}
					return label;
				}
			}
		});
	
		$("#allFields, #selectFiled").on('dragStart',
			function(event) {
		});
		$("#allFields, #selectFiled").on('dragEnd',
			function(event) {
		});
		
		$('#save').click(function() {
			var formId = $("#formId").val();
			var items = $("#selectFiled").jqxListBox("getItems");
			var item,mark,valueArr;
			var aoData = new Array();
			var check = false;
			for(var i=0;i<items.length;i++){
				item = items[i];
				mark = item.label.split("-");
				if (mark[0] != "主表"
						&& mark[0] != "子表") {
					valueArr = item.value.split("-");
					if(valueArr[0]=='M'){
						check = true;
					}
					aoData.push({
						"tableType":valueArr[0],
		                "tableId": valueArr[1],
		                "fieldId": valueArr[2]
		            });
				}
			}
			if(!check){
				 Dialog.alert("失败","必须选择主表的字段！");
				 return;
			}
            jQuery.ajax({
                type: "POST",
                cache:false,
                url: "formAdvanced_addSave?formId="+formId,
                dataType: "json",
                data: {data:JSON.stringify(aoData)},
                success:function (data) {
                	if(data.check){
                		Dialog.alert("成功","保存成功");
                		
                		//重新加载form列表
                		Table.render(parent.formTable);
                	}
                },
                error: function (data) {
                    Dialog.alert("失败","保存失败，程序异常结束！");
                }
            });
	  });
});