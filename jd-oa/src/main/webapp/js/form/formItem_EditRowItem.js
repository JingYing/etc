/**
 * Created with IntelliJ IDEA.
 * Desctription:表单行规则配置IFRAME
 * User: yujiahe
 * Date: 13-9-17
 * Time: 上午11:50
 * To change this template use File | Settings | File Templates.
 */
var table;
var mainFormItemCptExpTable;
$(function () {
    setMainFormItemCptExpPage() ;
    $("#sonForm").val("0");
    $("#equal").hide();
    $("#sonForm").change(function () {
        initRow();
    })
    /*
     *点击前台操作符触发函数绑定
     */
    $("#plus").click(function (e) {
        e.preventDefault()
        cptExp("＋", "+");

    });
    $("#minus").click(function (e) {
        e.preventDefault()
        cptExp("－", "-");
    });
    $("#multiply").click(function (e) {
        e.preventDefault()
        cptExp("×", "*");
    });
    $("#division").click(function (e) {
        e.preventDefault()
        cptExp("÷", "/");
    });
    $("#preBrackets").click(function (e) {
        e.preventDefault()
        cptExp("（", "(");
    });
    $("#backBrackets").click(function (e) {
        e.preventDefault()
        cptExp("）", ")");
    });
    $("#equal").click(function (e) {
        e.preventDefault()
        cptExp("＝", "=");
    });
    /**
     * 点击退格按钮触发函数绑定
     */
    $("#delLastExp").click(function (e) {
        e.preventDefault()
        delLastExp();
    });
    /**
     * 保存表达式
     */
    $("#addCptExp").click(function (e) {
        e.preventDefault();
        addCptExp();
    });

});
function initRow(){
    $("#cptExp").html("");
    $("#cptExpDesc").html("");
    var formId = $("#sonForm").val();
    if(formId=='0'){
        $('.rule-edit').hide(200);
        $("#alert").removeClass("alert-error").addClass("alert-info").text(" 行规则：在某明细表中，设定某个数值类型字段，由其他数值类型字段按照表达式计算得到。");
        return ;
    }

    $.ajax({
        type: "GET",
        cache: "false",
        url: "formItem_getFormItemByFormId",
        dataType: "json",
        data: "formId=" + formId,
        success: function (json) {
            var flag=false;
            var numItemHtml = "";
            for (i = 0; i < json.length; i++) {
                if (json[i].dataType == "NUMBER") { //如果类型为数值类型则显示
                    flag=true
                    numItemHtml += "<input type='button' class='btn' id='"+ json[i].fieldName+"' onclick='cptExp(\"" + json[i].fieldChineseName + "\",\"" + json[i].fieldName + "\",\"" + json[i].id + "\");return false;' value=" + json[i].fieldChineseName + ">   ";
                }
            }
            if(flag) {
                $("#numItem").html(numItemHtml)
                $('.rule-edit').show(200);
                $("#alert").removeClass("alert-error").removeClass("alert-success").addClass("alert-info").text(" 请在以下数值类型的字段中，选择要计算的字段。");
            }else{
//                    Dialog.alert("提示消息","该子表下没有可以配置的数据类型字段","","")
                $("#alert").removeClass("alert-info").addClass("alert-error").text("该子表下没有可以配置的数据类型字段");
                $('.rule-edit').hide(200);
            }

        }
    });
}
function setMainFormItemCptExpPage(){
    var options = Table.options;
    options = {
        isPaging: false,
        useCheckbox: false,
        pageUrl: "formItem_mainFormItemCptExpPage",
        defaultSort: [],
        sendData: function (sSource, aoData, fnCallback) {
            aoData.push({
                "name": "formId",
                "value": $("#mainFormId").val()
            });
            jQuery.ajax({
                type: "POST",
                url: "formItem_mainFormItemCptExpPage",
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    fnCallback(resp);
                }
            });
        },
        // 分页列设置
        columns: [
            { "mDataProp": "formId","sTitle":"明细表名","bSortable":true,"bVisible":true,"sWidth":300},
            { "mDataProp": "computeExpressDesc","sTitle":"表达式","bSortable":true,"bVisible":true,"sWidth":300},
            {"mData": "id", "sTitle": "删除", "bSortable": false, "mRender": function (data, type, full) {
                return "<a href='#' title='删除' class='btn'><img src='" + springUrl + "/static/common/img/del.png' alt='删除'/ onclick='delCptExp(\"" + data + "\");'></a>";
            }}
        ],
        // 分页按钮设置
        btns: []
    };
    mainFormItemCptExpTable=Table.dataTable("mainFormItemTable",options);
    Table.render(mainFormItemCptExpTable);
}
/**
 * 表达式生成函数 点击字段名称及操作符时触发
 * @param fieldChineseName   表单项显示名称fieldChineseName，显示操作符
 * @param fieldName     表单项名称    fieldName, 操作符
 * @param id           表单项id
 */

function cptExp(fieldChineseName, fieldName,id) {


    if($("#cptExp").text()==""){
        $("#cptExp").append("<span name=\""+id+"\" >" + fieldName + "</span>");//计算表达式
        $("#cptExpDesc").append("<span class='label label-info'>" + fieldChineseName + "</span>   "); //计算表达式描述
        $("#equal").click();
        $('.operator').show(200);
        $("#"+fieldName).attr("disabled",true);
        $("#alert").removeClass("alert-error").removeClass("alert-success").addClass("alert-info").text(" 请继续填写该字段对应的计算表达式，请注意表达式合理性");
    }else{
        $("#cptExp").append("<span name=\""+id+"\" >" + fieldName + "</span>");//计算表达式
        $("#cptExpDesc").append("<span class='label label-info'>" + fieldChineseName + "</span>   "); //计算表达式描述
    }
}
/**
 * 退格，删除最后一个操作符或者表单项
 */
function delLastExp() {

    if( $("#cptExp span").last().text()=="="){
        $("#cptExp span").last().remove();
        $("#cptExpDesc span").last().remove();
        $("#numItem input").removeAttr("disabled");
    }
    $("#cptExp span").last().remove();
    $("#cptExpDesc span").last().remove();
    if($("#cptExp").text()==""){
        $("#alert").removeClass("alert-error").removeClass("alert-success").addClass("alert-info").text(" 请在以下数值类型的字段中，选择要计算的字段。");
    }
}

function addCptExp() {
    var computeExpress = $("#cptExp").text(); //计算表达式
    var computeExpressDesc = $("#cptExpDesc").text();//计算表达式描述
    var id = $("#cptExp span").first().attr("name"); //formItem ID
    $.ajax({
        type: "post",
        cache: "false",
        url: "formItem_cptExpAdd",
        dataType: "json",
        data: {
            computeExpress: computeExpress,
            computeExpressDesc :computeExpressDesc,
            id:id
        }
        ,
        success: function (json) {
            $("#alert").removeClass("alert-error").removeClass("alert-info").addClass("alert-success").text(" 添加行规则成功");
            Table.render(table);
            Table.render(mainFormItemCptExpTable);
            $("#cptExp").html("");
            $("#cptExpDesc").html("");
            $("#numItem input").removeAttr("disabled");
        },
        beforeSend:function(){//提交前校验
            if(computeExpress==''){
                $("#alert").removeClass("alert-error").removeClass("alert-success").addClass("alert-error").text(" 表达式不能为空");
                return false
            }else{
                var numExpress=computeExpress.replace(/[^\+\-\*\/\(\)=]+/g,1);
                if(test(numExpress.substr(numExpress.indexOf('=')+1))){
                    return true;
                }else{
                    $("#alert").removeClass("alert-error").removeClass("alert-success").addClass("alert-error").html(" 表达式不合法，请修改");
                    return false;
                };
            }
        }
    });

}

function test(s){
    try{
        return !isNaN(eval("("+s+")"));
    }catch(e){
        return false;
    }
}




//function setFormItemCptExpPage(formId) {
//    var options = Table.options;
//    options = {
//        isPaging: false,
//        useCheckbox: false,
//        pageUrl: "formItem_mainFormPage",
//        defaultSort: [],
//        sendData: function (sSource, aoData, fnCallback) {
//            aoData.push({
//                "name": "formId",
//                "value": $("#sonForm").val()
//            },{
//                "name": "computeExpressDesc",
//                "value": 'notNull'
//            });
//            jQuery.ajax({
//                type: "POST",
//                url: "formItem_mainFormPage",
//                dataType: "json",
//                data: aoData,
//                success: function (resp) {
//                    fnCallback(resp);
//                }
//            });
//        },
//        // 分页列设置
//        columns: [
//            { "mDataProp": "computeExpressDesc","sTitle":"表达式","bSortable":true,"bVisible":true,"sWidth":500},
//            {"mData": "id", "sTitle": "删除", "bSortable": false, "mRender": function (data, type, full) {
//                return "<a href='#' title='删除' class='btn'><img src='" + springUrl + "/static/common/img/del.png' alt='删除'/ onclick='delCptExp(\"" + data + "\");'></a>";
//            }}
//        ],
//        // 分页按钮设置
//        btns: []
//    };
//    table=Table.dataTable("formItemCptExp",options);
//    Table.render(table);
//
//}
function delCptExp(id){


    Dialog.del('formItem_cptExpAdd?computeExpress=&computeExpressDesc=&id='+id, '', function (result) {
//        Dialog.alert("系统提示","删除行规则成功");
//            Table.render(table);
        $("#alert").removeClass("alert-error").removeClass("alert-info").addClass("alert-success").text(" 删除行规则成功");
            Table.render(mainFormItemCptExpTable);
    });
//    $.ajax({
//        type: "post",
//        cache: "false",
//        url: "formItem_cptExpAdd",
//        dataType: "json",
//        data: {
//            computeExpress: '',
//            computeExpressDesc :'',
//            id:id
//        }
//        ,
//        success: function (json) {
//            Dialog.alert("系统提示","删除行规则成功");
//            Table.render(table)
//            Table.render(mainFormItemCptExpTable)
//        }
//    });

}
