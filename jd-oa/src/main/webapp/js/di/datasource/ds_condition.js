var datasourceConditionTable;
var datasourceConditionTableData;
var conditionFileds;

//判断是否正在编辑
var nEditing = null;

$(function(){
    getFileds();
    var	datasourceConditionTable_option={
        pageUrl:springUrl+"/di/datasource/condition_page",
        useCheckbox:true,
        defaultSort:[],
        isPaging:true,
        bAutoWidth:true,
        scrollY:"100%",
        callback_fn:function(){
        },
        sendData:function ( sSource, aoData, fnCallback ) {
            var datasourceId=$("input[name='datasourceId']").val();
            aoData.push({"name":"datasourceId","value":datasourceId});
            jQuery.ajax( {
                type: "POST",
                url:sSource,
                dataType: "json",
                data: aoData,
                success: function(resp) {
                    datasourceConditionTableData=resp;
                    fnCallback(resp);
                },
                error:function(msg){
                    Dialog.alert("操作失败",msg.responseText);
                }
            });
        },
        columns: [
            { "mDataProp": "fieldName","sTitle":"名称","sClass": "my_class","sWidth":"100"},
            { "mDataProp": "fieldTitle","sTitle":"标题","sClass": "my_class","sWidth":"70"},
            { "sTitle":"类型","sClass": "my_class","sWidth":"50","fnRender":function(obj){
                return typeConvert(obj.aData.fieldType);
            }},
            { "mDataProp": "compareType","sTitle":"表达式","sClass": "my_class","sWidth":"60"},
            { "mDataProp": "defaultValue","sTitle":"默认值","sClass": "my_class","sWidth":"70"},
            { "sTitle":"UI类型","sClass": "my_class","sWidth":"60","fnRender":function(obj){
                return typeConvert(obj.aData.uiType);
            }},
            { "sTitle":"隐藏UI","sClass": "my_class","sWidth":"60","fnRender":function(obj){
                return typeConvert(obj.aData.isHidden);
            }},
            { "bSortable":false,"sWidth":"60","sTitle":"操作","fnRender":function(obj){
                var FId="datasource_condition_table";
                return " <a href='#' title='修改' class='edit_item' onclick='editOrSave($(this))'>修改</a> " +
                    " <a href='#' title='修改' class='cancel_item' onclick='cancelRow($(this))'></a> " +
                    " <a href='#' title='删除' class='del_item' onclick='deleteRow($(this));return false'>删除</a>";
            }}
        ],
        btns:[
            {
                "sExtends":    "text",
                "sButtonText": "新增",
                "sToolTip": "",
                "fnClick": function ( nButton, oConfig, oFlash ) {
                    if(conditionFileds == null || conditionFileds.length ==0){
                        alert("该数据源没有可用的字段！");
                        return false;
                    }
                    addRow();
                }
            },
            {
                "sExtends":    "text",
                "sButtonText": "删除",
                "sToolTip": "",
                "fnClick": function ( nButton, oConfig, oFlash ) {
                    var ids=Table.getSelectedRowsIDs(datasourceConditionTable);
                    if(ids.length==0){
                        Dialog.alert("消息提示","没有选中项","确定");
                    }else{
                        delAllCondition(ids);
                    }
                }
            }
        ]
    };
    datasourceConditionTable = Table.dataTable("datasource_condition_table",datasourceConditionTable_option);
});
//添加一行记录
function addRow(){
    var arr = [];
    arr.push({"id": "", "fieldName": "", "fieldTitle": "", "fieldType": "", "compareType": "", "defaultValue": "","uiType": "","isHidden":"0"});
    var oSettings = datasourceConditionTable.fnSettings();
    oSettings.oFeatures.bServerSide = false;
    var aiNew = datasourceConditionTable.fnAddData(arr);
    var nRow = datasourceConditionTable.fnGetNodes(aiNew[0]);
    editRow(datasourceConditionTable, nRow,"datasource_condition_table", 'new');
    oSettings.oFeatures.bServerSide = true;
}
//修改
function editRow(oTable, nRow, tableId, flag) {
    if (nEditing == null) {
    } else {
        var currentData = oTable.fnGetData(nEditing);
        if (currentData.id == null || currentData.id == "") {
            var oSettings = oTable.fnSettings();
            oSettings.oFeatures.bServerSide = false;
            oTable.fnDeleteRow(nEditing);
            oSettings.oFeatures.bServerSide = true;
        } else {
            restoreRow(oTable, nEditing);
        }
    }
    nEditing = nRow;

    var aData = oTable.fnGetData(nEditing);
    var jqTds = $('>td', nEditing);
    var jq1html="";
    jq1html =
        '<select id="fieldName"  name="fieldName"   style="width:' + ((jqTds[1].clientWidth - 16 - 18) * 100 / (jqTds[1].clientWidth - 16)) + '%" data-rule="required;">';
    $.each(conditionFileds,function(i,n){
        jq1html +=  '<option value="'+n+'">'+n+'</option>';
    });
    jq1html += '</select>';
    jqTds[1].innerHTML = jq1html;
    jqTds[2].innerHTML = '<input name="fieldTitle" id="fieldTitle" style="width:' + ((jqTds[2].clientWidth - 16 - 18) * 100 / (jqTds[2].clientWidth - 16)) + '%" value="' + aData.fieldTitle + '"  type="text" data-rule="required;nospecial " /> ';
    jqTds[3].innerHTML =
        '<select id="fieldType"  name="fieldType" onchange="changeDefaultValueType($(this))"  style="width:' + ((jqTds[3].clientWidth - 16 - 18) * 100 / (jqTds[3].clientWidth - 16)) + '%" data-rule="required;">'
            + '<option value="STRING">文本</option>'
            + '<option value="NUMBER">数值</option>'
            + '<option value="DATE">日期</option>'
            + '<option value="TIME">时间</option>'
            + '<option value="DATETIME">日期时间</option>'
            + '</select>';
    jqTds[4].innerHTML =
        '<select id="compareType"  name="compareType"  style="width:' + ((jqTds[4].clientWidth - 16 - 18) * 100 / (jqTds[4].clientWidth - 16)) + '%" data-rule="required;">'
            + '<option value="=">=</option>'
            + '<option value="<"><</option>'
            + '<option value=">">></option>'
            + '<option value="<="><=</option>'
            + '<option value=">=">>=</option>'
            + '<option value="!=">!=</option>'
            + '<option value="like">like</option>'
            + '</select>';
    jqTds[5].innerHTML = '<input name="deafultValue" id="deafultValue" style="width:' + ((jqTds[5].clientWidth - 16 - 18) * 100 / (jqTds[5].clientWidth - 16)) + '%" value="' + aData.defaultValue + '"  type="text" data-rule="" /> ';
    jqTds[6].innerHTML =
        '<select id="uiType"  name="uiType"  style="width:' + ((jqTds[6].clientWidth - 16 - 18) * 100 / (jqTds[6].clientWidth - 16)) + '%" data-rule="required;">'
            + '<option value="STRING">文本</option>'
            + '<option value="NUMBER">数值</option>'
            + '<option value="DATE">日期</option>'
            + '<option value="TIME">时间</option>'
            + '<option value="DATETIME">日期时间</option>'
            + '</select>';
    jqTds[7].innerHTML =
        '<select id="isHidden"  name="isHidden"  style="width:' + ((jqTds[7].clientWidth - 16 - 18) * 100 / (jqTds[7].clientWidth - 16)) + '%" data-rule="required;">'
            + '<option value="0">否</option>'
            + '<option value="1">是</option>'
            + '</select>';
    $('.edit_item', jqTds[8]).text('保存');
    if (flag != "new") {
        $('.cancel_item', jqTds[8]).text('取消');
    }
    bindValidate($('#' + tableId));// 绑定验证
    if( aData.fieldName != null){
           $("#fieldName").find("option[value='"+ aData.fieldName+"']").attr("selected",true);
    }
    if( aData.fieldType != null){
           $("#fieldType").find("option[value='"+ aData.fieldType+"']").attr("selected",true);
    }
    if( aData.compareType != null){
        $("#compareType").find("option[value='"+ aData.compareType+"']").attr("selected",true);
    }
    if( aData.uiType != null){
        $("#uiType").find("option[value='"+ aData.uiType+"']").attr("selected",true);
    }
    if( aData.isHidden != null){
        $("#isHidden").find("option[value='"+ aData.isHidden+"']").attr("selected",true);
    }


}
//动态改变defaultValue的校验规则
function changeDefaultValueType(obj){
    var type=obj.val();
    var map=new Array();
    map["STRING"]="numberAndString";
    map["NUMBER"]="float";
    map["DATE"]="date";
    map["TIME"]="time";
    map["DATETIME"]="datetime";
    //还未实现改变
}
//删除一行记录
function deleteRow(el) {
    var oTable =datasourceConditionTable;
    var nRow = el.parents('tr')[0];
    var aData = oTable.fnGetData(nRow);
    if (aData.id == null || aData.id == "") { //未保存到数据库不走后台删除
        var oSettings = oTable.fnSettings();
        oSettings.oFeatures.bServerSide = false;
        oTable.fnDeleteRow(nRow);
        oSettings.oFeatures.bServerSide = true;
    } else {//走后台删除
        var ids=new Array();
        ids.push(aData.id);
        delAllCondition(ids);
    }
    nEditing = null;
}
//删除记录
function delAllCondition(ids){
    var data={
        "idstr":JSON.stringify(ids)
    };
    jQuery.ajax({
        type: "POST",
        url: springUrl + "/di/datasource/condition_delete",
        data:data,
        dataType : 'json',
        success: function (result) {
            if(result.operator == true){
                Dialog.alert("操作成功","删除操作成功！");
                Table.render(datasourceConditionTable);
            }
        },
        error:function(msg){
            Dialog.alert("操作失败",msg.responseText);
        }
    });
}
//保存一行记录
function saveRow(oTable, nRow, tableId) {
    var jqInputs = $('input,select', nRow);
    arr = [];
    //收集数据
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    var id="";
    if(aData != null && aData.id != null){
        id=aData.id;
    }
    var datasourceId=$("input[name='datasourceId']").val();
    if(datasourceId == null || datasourceId == "") {
        alert("未选择数据源，请重新选择数据源！");
        return false;
    }
    arr.push({"name": "id", "value": id});
    arr.push({"name": "fieldName", "value": jqInputs[1].value});
    arr.push({"name": "fieldTitle", "value": jqInputs[2].value});
    arr.push({"name": "fieldType", "value": jqInputs[3].value});
    arr.push({"name": "compareType", "value": jqInputs[4].value});
    arr.push({"name": "defaultValue", "value": jqInputs[5].value});
    arr.push({"name": "uiType", "value": jqInputs[6].value});
    arr.push({"name": "isHidden", "value":  jqInputs[7].value});
    arr.push({"name": "datasourceId", "value": datasourceId});
    if (!validate2($('#' + tableId))) {
        return false;
    }
    //提交数据
    jQuery.ajax({
        type: "POST",
        url: springUrl + "/di/datasource/condition_add",
        data: arr,
        success: function (result) {
            if(result.operator == "success"){
                Dialog.alert("操作成功","数据操作成功！");
                oTable.fnDraw();
                nEditing = null;//校验成功
            }
        },
        error:function(msg){
            Dialog.alert("操作失败",msg.responseText);
        }
    });
}
//修改或者保存一行
function editOrSave(obj){
    var nRow = obj.parents('tr')[0];
    if (nEditing == nRow && obj.text() == "保存") {
        /* This row is being edited and should be saved */
        saveRow(datasourceConditionTable, nRow,"datasource_condition_table");
    } else {
        editRow(datasourceConditionTable, nRow, "datasource_condition_table");
    }
}
//取消行的操作
function cancelRow(obj){
    restoreRow(datasourceConditionTable, nEditing);
}
function restoreRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    oTable.fnUpdate(aData.fieldName, nRow, 1, false);
    oTable.fnUpdate(aData.fieldTitle, nRow, 2, false);
    oTable.fnUpdate(aData.fieldType, nRow, 3, false);
    oTable.fnUpdate(aData.compareType, nRow, 4, false);
    oTable.fnUpdate(aData.defaultValue, nRow, 5, false);
    oTable.fnUpdate(aData.uiType, nRow, 6, false);
    oTable.fnUpdate(aData.isHidden, nRow, 7, false);
    $('.edit_item', jqTds[8]).text('修改');
    $('.cancel_item', jqTds[8]).text('');
}
//将类型的value转成对应的显示文字
function typeConvert(value){
    var map=new Array();
    map["STRING"]="文本";
    map["NUMBER"]="数值";
    map["DATE"]="日期";
    map["TIME"]="时间";
    map["DATETIME"]="日期时间";
    map["0"]="否";
    map["1"]="是";
    return map[value];
}
//获取可用的字段
function getFileds(){
    var data={
        "datasourceId":$("input[name='datasourceId']").val()
    };
    jQuery.ajax({
        type: "POST",
        url: springUrl + "/di/datasource/datasource_getColumns",
        data:data,
        dataType : 'json',
        success: function (result) {
            conditionFileds=result;
        },
        error:function(msg){
            Dialog.alert("操作失败",msg.responseText);
        }
    });
}