var datasourceTable;
var datasourceTableData;

$(function(){
    var	datasourceTable_options={
        pageUrl:springUrl+"/di/datasource/datasourcePage",
        useCheckbox:true,
        defaultSort:[],
        isPaging:true,
        bAutoWidth:true,
        scrollY:"500px",
        callback_fn:function(){
        },
        sendData:function ( sSource, aoData, fnCallback ) {
            var dsName=$("#dsName").val();
            dsName=dsName.replace("_","\\_");
            aoData.push(
                { "name": "dsName", "value":dsName}
            );
            jQuery.ajax( {
                type: "POST",
                url:sSource,
                dataType: "json",
                data: aoData,
                success: function(resp) {
                    datasourceTableData=resp;
                    fnCallback(resp);
                },
                error:function(msg){
                    Dialog.alert("操作失败",msg.responseText);
                }
            });
        },
        columns: [
            {"sTitle":"ID","sClass": "my_class","sWidth":"100","fnRender":function(obj){
                return obj.aData.id;
            }},
            { "mDataProp": "name","sTitle":"名称","sClass": "my_class","sWidth":"100"},
            { "mDataProp": "dataAdapter","sTitle":"数据源","sClass": "my_class","sWidth":"70"},
//            { "mDataProp": "execSql","bSortable":false,"sTitle":"执行语句","sClass": "my_class","sWidth":"200"},
            { "bSortable":false,"sWidth":"100","sTitle":"操作","fnRender":function(obj){
                var id=obj.aData.id;
                var html="<a href='#' class='btn' title='修改' onclick='edit(\""+id+"\");'><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>&nbsp;"
                    +"<a onclick='setConditions(\""+id+"\");' title='查询条件' class='btn' href='#'><img alt='设置查询条件' src='"+springUrl+"/static/common/img/set.png'></a></a>&nbsp;"
                    +"<a onclick='del(\""+id+"\");' title='删除' class='btn' href='#'><img alt='删除' src='"+springUrl+"/static/common/img/del.png'></a>&nbsp;";

                return html;
            }}
        ],
        btns:[
            {
                "sExtends":    "text",
                "sButtonText": "添加",
                "sToolTip": "",
                "fnClick": function ( nButton, oConfig, oFlash ) {
                    Dialog.openRemote("添加数据源",springUrl+"/di/datasource/datasource_add", 550, 450,
                        [
                            {
                                "label": "取消",
                                "class": "btn-success",
                                "callback": function() {}
                            },
                            {
                                "label": "保存",
                                "class": "btn-success",
                                "callback": function(modal,e) {
                                    var validateEl=$('#datasourceAddForm');
                                    if(!validate2(validateEl)){
                                        return false;
                                    }
                                    addDatasource(modal);
                                    return false;
                                }
                            }
                        ]);
                }
            },
            {
                "sExtends":    "text",
                "sButtonText": "删除",
                "sToolTip": "",
                "fnClick": function ( nButton, oConfig, oFlash ) {
                    var ids=Table.getSelectedRowsIDs(datasourceTable);
                    if(ids.length==0){
                        Dialog.alert("消息提示","没有选中项","确定");
                    }else{
                        delAll(ids);
                    }
                 }
            }
         ]
    };
    datasourceTable = Table.dataTable("datasource_table",datasourceTable_options);
    $("#search").click(function(){
        Table.render(datasourceTable);
    });
});
//删除操作
function del(id){
    var ids=new Array();
    ids.push(id);
    delAll(ids);
}
//批量删除操作
function delAll(ids){
    var data={
        "idstr":JSON.stringify(ids)
    };
    Dialog.del(springUrl+"/di/datasource/datasource_delete",data,function(result){
        Table.render(datasourceTable);
        Dialog.alert("操作结果",result.message);
    });
}
//修改操作
function edit(id){
    Dialog.openRemote("修改数据源",springUrl+"/di/datasource/datasource_add?id="+id, 550,450,
        [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function() {}
            },
            {
                "label": "保存",
                "class": "btn-success",
                "callback": function(modal,e) {
                    var validateEl=$('#datasourceAddForm');
                    if(!validate2(validateEl)){
                        return false;
                    }
                    addDatasource(modal);
                    return false;
                }
            }
        ]);
}
//设置查询条件
function setConditions(id){
    Dialog.openRemote("设置查询条件",springUrl+"/di/datasource/condition?id="+id, 900,450,
        [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function() {}
            }
        ]);
}


