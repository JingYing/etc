/**
 * 文档-文件夹管理首页JS
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var zTree;

//根节点
var zNodes = {
    "id":"null",
    "name":"京东文档",
    "isParent":true,
    "iconSkin":"diy1"
};

var setting = {
    async : {
        enable : true,
        url : springUrl+"/doc/wffolder_treeLoad",
        autoParam : ["id"]
    },
    callback : {
        onClick : zTreeOnClick
    },
    data : {
        simpleData : {
            enable : true,
            idKey : 'id',
            idPKey : 'pId',
            rootPid : null
        }
    },
    view: {
        addHoverDom: addHoverDom,
        removeHoverDom: removeHoverDom,
        dblClickExpand: false,
        showLine: true,
        selectedMulti: false,
        expandSpeed: "fast"
    }
};
function beforeClick(){
    return true;
}
function zTreeOnClick(event, treeId, treeNode) {
    if(treeNode.id!="null"){
        if(treeNode.id.indexOf("tmp_") == 0){
            $("#box-right").show();
            $("#box-right").load(springUrl+"/doc/wffolder_add?parentId="+treeNode.pId);
        }else{
            $("#box-right").load(springUrl+"/doc/wffolder_view?id="+treeNode.id);
            $("#box-right").show();
        }
    }
     zTree.expandNode(treeNode);
//    if (treeNode.level != 0) {
//        if(treeNode.id){
//            var params = "";
//            if(treeNode.props.id != undefined){
//                params = params + "?id=" + treeNode.props.id;
//                if(treeNode.props.parentId != undefined){
//                    params = params + "&parentId=" + treeNode.props.parentId;
//                }
//            }
//            else{
//                if(treeNode.props.parentId != undefined){
//                    params = params + "?parentId=" + treeNode.props.parentId;
//                }
//            }
//            $("#box-right").load("resource!input.do" + params);
//        }
//        // 新建节点，没有ID
//        else{
//            $("#box-right").load("resource!input.do" + (treeNode.parentId?"?parentId=" + treeNode.parentId:""));
//        }
//        return true;
//    }
}

function addHoverDom(treeId, treeNode) {
    // 由于根节点是应用数据，因此只有新增资源的功能，别的节点可以同时具有新增、修改和删除的功能
    if(treeNode.level > 0){
        var sObj = $("#" + treeNode.tId + "_span");
        if (treeNode.editNameFlag || $("#removeBtn_"+treeNode.id).length>0) return;
        // 新增的临时节点，只有删除功能
        if(treeNode.id.indexOf("tmp_") == 0){
            var domStr = "<span class='button remove' id='removeBtn_" + treeNode.id
                    + "' title='删除' onfocus='this.blur();'></span>";
            sObj.after(domStr);
            var btn = $("#removeBtn_"+treeNode.id);
            if (btn) btn.bind("click", function(){
//                var zTree = jQuery.fn.zTree.getZTreeObj("tree");
                zTree.removeNode(treeNode);
                $("#box-right").hide();
            });
        }
        // 其它节点，具有新增、编辑、删除功能
        else{
            // 临时节点保存后，成为正式节点，对应的浮动按钮需要先删除，然后再加上新的按钮菜单
            $("span[id^=addBtn_]").unbind().remove();
            $("span[id^=editBtn_]").unbind().remove();
            $("span[id^=removeBtn_]").unbind().remove();


            var domStr = "<span class='button add' id='addBtn_" + treeNode.id
                    + "' title='新增' onfocus='this.blur();'></span>"
                    + "<span class='button edit' id='editBtn_" + treeNode.id
                    + "' title='修改' onfocus='this.blur();'></span>"
                    + "<span class='button remove' id='removeBtn_" + treeNode.id
                    + "' title='删除' onfocus='this.blur();'></span>"
                ;
            sObj.after(domStr);
            var btn = $("#addBtn_"+treeNode.id);
            if (btn) btn.bind("click", function(){
//                var zTree = jQuery.fn.zTree.getZTreeObj("tree");
                var timestamp = new Date().getTime();
                zTree.addNodes(treeNode, {isParent:true, id:("tmp_"+timestamp), pId:treeNode.id, name:"未命名", props:{parentId:treeNode.props.id}});
                // 选中新增的节点
                zTree.selectNode(zTree.getNodeByParam("id","tmp_"+timestamp,treeNode));
                $("#box-right").show();
                $("#box-right").load(springUrl+"/doc/wffolder_add?parentId="+treeNode.id);
                return false;
            });
            btn = $("#editBtn_"+treeNode.id);
            if (btn) btn.bind("click", function(){
                $("#box-right").load(springUrl+"/doc/wffolder_update?id="+treeNode.props.id);
//                var zTree = jQuery.fn.zTree.getZTreeObj("tree");
                zTree.selectNode(treeNode);
                $("#box-right").show();
                return false;
            });
            btn = $("#removeBtn_"+treeNode.id);
            //删除按钮
            if (btn) {
                btn.bind("click", function () {
                        Dialog.confirm("提示信息", "确认要删除此文件夹么", "确认", "取消", function (result) {
                            if (result) {
//                                var zTree = jQuery.fn.zTree.getZTreeObj("tree");
                                jQuery.ajax({
                                    type: "post",
                                    dataType: "json",
                                    url: springUrl + "/doc/wffolder_getChildCount",
                                    data: {
                                        id: treeNode.props.id,
                                        parentId: treeNode.props.parentId
                                    },
                                    success: function (data) {
                                        if (data == 0) {
                                            jQuery.ajax({
                                                type: "post",
                                                dataType: "json",
                                                url: springUrl + "/doc/wffolder_delete",
                                                data: {
                                                    id: treeNode.props.id,
                                                    parentId: treeNode.props.parentId
                                                },
                                                success: function (data) {
                                                    if (data == null) {
                                                        zTree.removeNode(treeNode);
                                                        $("#box-right").hide();
                                                        Dialog.alert("成功", "文件夹删除成功");
                                                    } else {
                                                        Dialog.alert("失败","文件夹删除失败");
                                                    }
                                                },
                                                error: function (data) {
                                                    Dialog.alert("失败","文件夹删除失败");
                                                }
                                            });
                                        } else {
                                            Dialog.alert("失败", "删除失败,文件夹下存在文件或文件夹!");
                                        }
                                    },
                                    error: function (data) {
                                        Dialog.alert("失败", "文件夹删除失败");
                                    }
                                })
                            }
                        });
                        return false;
                    }
                );
            }
        }
    }
    // 由于根节点是应用数据，因此只有新增资源的功能
    else{
        var sObj = $("#" + treeNode.tId + "_span");
        if (treeNode.editNameFlag || $("#addBtn_"+treeNode.id).length>0) return;
        var addStr = "<span class='button add' id='addBtn_" + treeNode.id
            + "' title='新增' onfocus='this.blur();'></span>";
        sObj.after(addStr);
        var btn = $("#addBtn_"+treeNode.id);
        if (btn) btn.bind("click", function(){
            var zTree = $.fn.zTree.getZTreeObj("tree");
            var timestamp = new Date().getTime();
            zTree.addNodes(treeNode, {isParent:true, id:("tmp_"+timestamp), pId:treeNode.id, name:"未命名", props:{}});
            // 选中新增的节点
            zTree.selectNode(zTree.getNodeByParam("id","tmp_"+timestamp,treeNode));
            $("#box-right").show();
            $("#box-right").load(springUrl+"/doc/wffolder_add?parentId="+treeNode.id);
            return false;
        });
    }
}

/**
 * 刷新父节点
 * @param pId 父节点ID
 */
function refreshNode(pId){
//    var zTree =jQuery.fn.zTree.getZTreeObj("tree");
    if(pId == null){
        var treeNode = zTree.getNodeByTId("tree_1");
        zTree.reAsyncChildNodes(treeNode, "refresh");
    }else{
        var nodes = zTree.transformToArray(zTree.getNodes());
        var tId = "";
        for(var i=0; i<nodes.length; i++){
            if(nodes[i].id == pId){
                tId = nodes[i].tId;
                break;
            }
        }
        if(tId != ""){
            var treeNode = zTree.getNodeByTId(tId);
            zTree.reAsyncChildNodes(treeNode, "refresh");
        }
    }
}

function removeHoverDom(treeId, treeNode) {
    if(treeNode.level > 0){
        $("#addBtn_"+treeNode.id).unbind().remove();
        $("#editBtn_"+treeNode.id).unbind().remove();
        $("#removeBtn_"+treeNode.id).unbind().remove();
    }
    else{
        $("#addBtn_"+treeNode.id).unbind().remove();
    }
}
/**
 * 初始化操作
 */
$(function() {
    jQuery.fn.zTree.init($("#tree"), setting, zNodes);
    zTree = $.fn.zTree.getZTreeObj("tree");
});
