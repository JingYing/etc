/**
 * 文档-文件夹修改页面JS
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 修改文件夹-提交按钮响应函数
 */
function updateFloder(){
    //判断当前桶下否存在同名文件夹
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            id:$("#id").val(),
            parentId:$("#parentId").val(),
            folderName:$("#folderName").val()
        },
        url:springUrl + "/doc/wffolder_getFolderCount",
        success:function (data) {
            if(data > 0){
                Dialog.alert("失败","已存在相同文件夹");
            }else{
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                        id:$("#id").val(),
                        parentId:$("#parentId").val(),
                        folderName:$("#folderName").val(),
                        folderDesc:$("#folderDesc").val()
                    },
                    url:springUrl + "/doc/wffolder_updateSave",
                    success:function (data) {
                        refreshNode($("#parentId").val());
                        $("#box-right").load(springUrl+"/doc/wffolder_view?id="+data.id);
                    },
                    error: function (data) {
                        Dialog.alert("失败","更新文件夹失败");
                    }
                });
            }
        },
        error: function (data) {
            Dialog.alert("失败","更新文件夹失败");
        }
    });
}

/**
 * 修改文件夹-取消按钮响应函数
 */
function cancelFloder(){
    $("#box-right").hide();
    refreshNode($("#parentId").val());
}

/**
 * 初始化操作
 */
$(function() {
    //绑定保存按钮事件
    $('#updateFloder').click(function() {
        if(!validate2($('#updateFolderForm'))){
            return false;
        }
        updateFloder();
    });
    //绑定取消按钮事件
    $('#cancelFloder').click(function() {
        cancelFloder();
    });
    //表单验证
    bindValidate($('#updateFolderForm'));
});
