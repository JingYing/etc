/**
 * 文档-文件管理首页JS
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

//数据表格初始化
var fileTable;
var options = Table.options;
var expressionId ="";
options = {
    defaultSort:[['1','desc']],
    pageUrl:springUrl+"/doc/wffile_findFileList",
    sendData:function ( sSource, aoData, fnCallback ) {
        var folderId = $("#folderId").val();       //文件夹ID
        var fileName = $("#fileName").val();       //文件名称
        var fileSuffix = $("#fileSuffix").val();   //文件扩展名
        aoData.push(
            {"name":"folderId","value":folderId},
            {"name":"fileName","value":fileName},
            {"name":"fileSuffix","value":fileSuffix}
        );
        jQuery.ajax( {
            type: "POST",
            url:sSource,
            dataType: "json",
            data: aoData,
            success: function(data) {
                fnCallback(data);
            },
            error: function (data) {
                if(data.responseText == null){
                    Dialog.alert("失败","文件列表查询失败");
                }else{
                    Dialog.alert("失败",data.responseText);
                }

            }
        });
    },
    columns: [
        {"mData": "modifyTime","sTitle":"文件ID" ,"bSortable":false,"bVisible":false},
        {"mData": "id","sTitle":"文件ID" ,"bSortable":false,"bVisible":false},
        {"mData": "fileName","sTitle":"文件名称" ,"bSortable":false,"sWidth":350,"fnRender": function(obj){
            return '<a href="#" onclick="fileShow(\''+obj.aData.id+'\')">'+obj.aData.fileName+'</a>';
        }},
        {"mData": "folderName","sTitle":"所属文件夹" ,"bSortable":false},
        {"mData": "fileSuffix","sTitle":"文件扩展名" ,"bSortable":false},
        {"mData": "modifier","sTitle":"操作人" ,"bSortable":false},
        {"mData": "modifyTime","sTitle":"最后更新" ,"bSortable":false,"mRender":function(data){
            return new Date(data).format("yyyy-MM-dd");
        }},
        {"mData": "id","sTitle":"操作","bSortable":false,"sWidth":200,"mRender":function(data){
              return "<a href='#' class='btn' title='修改' onclick='updateFile(\""+data+"\");'><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>&nbsp;"+
                     "<a href='#' class='btn' title='授权' onclick='enjoyFile(\""+data+"\");'><img src='"+springUrl+"/static/common/img/enjoy.png' alt='授权'/></a>&nbsp;"+
                     "<a href='#' class='btn' title='移动' onclick='moveFile(\""+data+"\");'><img src='"+springUrl+"/static/common/img/move.png' alt='移动'/></a>&nbsp;"+
                     "<a href='#' class='btn' title='删除' onclick='deleteFile(\""+data+"\");'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'/></a>";
        }}
    ],
    btns:[{
            "sExtends":    "text",
            "sButtonText": "新增",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function ( nButton, oConfig, oFlash ) {
                addFile();
            }
        },{
            "sExtends":    "text",
            "sButtonText": "共享",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function ( nButton, oConfig, oFlash ) {
                batchEnjoyFile();
            }
        },{
            "sExtends":    "text",
            "sButtonText": "移动",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function ( nButton, oConfig, oFlash ) {
                batchMoveFile();
            }
        },{
            "sExtends":    "text",
            "sButtonText": "删除",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function ( nButton, oConfig, oFlash ) {
                batchDelFile();
            }
        }
    ]
};

/**
 * 查看文件详情
 * @param id
 */
function fileShow(id){
    Dialog.openRemote("查看文档",springUrl + "/doc/wffile_view?id="+id,"500","650",[{
        'label': '返回',
        'class': 'btn-success',
            'callback': function() {
        }
    }
    ]);
//    window.location.href = springUrl + "/doc/wffile_view?id="+id;
}

/**
 * 新增文件
 */
function addFile(){
    window.location.href = springUrl+"/doc/wffile_add";
}

/**
 * 移动文件
 */
function moveFile(id){
    openMoveDialog('[{"id":"'+id+'"}]');
}

/**
 * 批量移动文件
 */
function batchMoveFile(){
    var arrObj = [];
    var ids = Table.getSelectedRows(fileTable);
    if(ids.length == 0){
        Dialog.alert("提示信息", "请选择要移动的文件");
    }else{
        for(var i=0; i<ids.length; i++){
            var obj = {};
            obj.id = ids[i].id;
            arrObj.push(obj);
        }
        openMoveDialog(JSON.stringify(arrObj));
    }

}
/**
 * 弹出文件夹树型框-移动文件专用
 */
function openMoveDialog(ids){
    Dialog.openRemote("文件夹树",springUrl+"/doc/wffolder_treeIndex","305","400",[{
            'label': '取消',
            'class': 'btn-success',
            'callback': function() {
            }
        },{
            'label': '确定',
            'class': 'btn-success',
            'callback': function() {
                var folder = getSelectedNodes();
                if(folder[0].id != "null"){
                    moveFileSave(ids,folder[0].id);
                }
            }
        }
    ]);
}

/**
 * 移动文件-保存
 * @param ids 支持文件对象的JSON串
 * @param folderId 文件夹ID
 */
function moveFileSave(ids,folderId){
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType:'json',
        url:springUrl + "/doc/wffile_getFileNumbers",
        data:{
            ids: ids,
            folderId:folderId
        },
        success:function (data) {
            if(data==0){
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType:'json',
                    url:springUrl + "/doc/wffile_moveSave",
                    data:{
                        ids: ids,
                        folderId:folderId
                    },
                    success:function (data) {
                        Table.render(fileTable);
                        Dialog.alert("成功","文件移动成功");
                    },
                    error : function(data){
                        Dialog.alert("失败","文件移动失败");
                    }
                });
            }else{
                Dialog.alert("失败","该文件夹下已存在相同名称的文档");
            }
        },
        error : function(data){
            Dialog.alert("失败","获取同名文档失败");
        }
    });
}

/**
 * 删除文件
 * @param id 文件ID
 */
function deleteFile(id){
    Dialog.confirm("删除提示","确定删除选中的文档?","确定","取消",function(result){
         if(result){
             jQuery.ajax({
                 type:"POST",
                 cache:false,
                 dataType:'json',
                 url:springUrl + "/doc/wffile_delete",
                 data:{
                     id:'[{"id":"'+id+'"}]'
                 },
                 success:function (data) {
                     Dialog.alert("成功","文件删除成功");
                     Table.render(fileTable);
                 },
                 error : function(data){
                     Dialog.alert("失败","文件删除失败");
                 }
             });
         }
    });
}

/**
 * 批量删除文件
 */
function batchDelFile(){
    var arrObj = [];
    var ids = Table.getSelectedRows(fileTable);
    if(ids.length == 0){
        Dialog.alert("提示信息", "请选择要删除的文件");
        return;
    }
    for(var i=0; i<ids.length; i++){
        var obj = {};
        obj.id = ids[i].id;
        arrObj.push(obj);
    }
    Dialog.confirm("删除提示","确定删除选中的文档?","确定","取消",function(result){
           if(result){
               jQuery.ajax({
                   type:"POST",
                   cache:false,
                   dataType:'json',
                   url:springUrl + "/doc/wffile_delete",
                   data:{
                       id: JSON.stringify(arrObj)
                   },
                   success:function (data) {
                       Dialog.alert("成功","文件删除成功");
                       Table.render(fileTable);
                   },
                   error : function(data){
                       Dialog.alert("失败","文件删除失败");
                   }
               });
           }
    });
}

/**
 * 修改文件
 */
function updateFile(id){
	window.location.href = springUrl+"/doc/wffile_update?id="+id;
}
/**
 * 文件共享时 获取已选择的权限ID
 */
function findExpression(fileId){
	expressionId ="";
	jQuery.ajax({
		async:false,
        type:"POST",
        cache:false,
        dataType:'json',
        url:springUrl + "/doc/wffile_findBusinessExpression?fileId="+fileId,
        data:{
        },
        success:function (data) {
        	//注释by wangdongxing  2013-9-6
        	/*for(var i=0;i<data.length;i++){
        		if(i==(data.length-1)){
        			expressionId+=data[i].id;
        		}else{
        			expressionId+=data[i].id+',';
        		}	
        	} 
        	expressionId="["+expressionId+"]";*/
        	
        	//添加 by wangdongxing
        	var ids=new Array();
        	for(var i=0;i<data.length;i++){
        		ids.push(data[i].id);
        	} 
        	expressionId=JSON.stringify(ids);
        	
        },
        error : function(data){
           Dialog.alert("提示信息","获取已选表达式失败");
        }
    });
}
/**
 * 共享文件
 */
function enjoyFile(fileId){
findExpression(fileId);
Dialog.openRemote('授权用户',springUrl+'/auth/authExpression_select?selectedIds='+expressionId,1000,500,[
     {
	     'label': '取消',
	     'class': 'btn-success',
	     'callback': function() {
	    	
	     	}
	     },
	 {	
         'label': '确定',
         'class': 'btn-success',
         'callback': function() {
        	 var ids=selectOk();   
        	 if(ids.length<=0){
                 deleteBusinessExpression('[{"id":"'+fileId+'"}]');
        	  }else{
        		  enjoyFileSave(ids,fileId,"");
        	  }
    	     return false;
         }
	 }]);                  
}
/**
 * 批量共享文件
 */
function batchEnjoyFile(){
	  //选择的数据ID
        expressionId ="";
	  fileIds = Table.getSelectedRowsIDs(fileTable);
	  if(fileIds.length==0){
		  Dialog.alert("提示信息", "请选择文档");
	  }else{
          Dialog.openRemote('授权用户',springUrl+'/auth/authExpression_select?selectedIds=""',1000,500,
              [
                  {
                      'label': '取消',
                      'class': 'btn-success',
                      'callback': function() {

                      }
                  },
                  {
                      'label': '确定',
                      'class': 'btn-success',
                      'callback': function() {
                          var ids=selectOk();
                          if(ids.length==0){
                              var arrObj = [];
                              for(var j=0; j<fileIds.length;j++){
                                  var obj = {};
                                  obj.id=fileIds[j];
                                  arrObj.push(obj);
                              }
                              deleteBusinessExpression(JSON.stringify(arrObj));
                          }else{
                              enjoyFileSave(ids,fileIds,"batch");
                          }
                          return false;
                      }
                  }]);
	  }
}
/**
 * 共享文件保存功能
 * ids 权限表达式的ID集合
 * fileId 文件ID
 */
function  enjoyFileSave(ids,fileIds,type){
	  var arrObj = [];
	  if(type!="batch"){
		  for(var i=0; i<ids.length; i++){
		        var obj = {};
		        obj.businessType='1';
		        obj.businessId=fileIds;
		        obj.authExpressionId=ids[i];
		        arrObj.push(obj);
		    }
	  }else{//type==batch为批量共享
		  for(var j=0; j<fileIds.length;j++){
			  for(var i=0; i<ids.length; i++){
			        var obj = {};
			        obj.businessType='1';
			        obj.businessId=fileIds[j];
			        obj.authExpressionId=ids[i];
			        arrObj.push(obj);
			    }
		  }
	  }
	   jQuery.ajax({
	        type:"POST",
	        cache:false,
	        dataType:'json',
	        url:springUrl + "/doc/wffile_shared",
	        data:{
	        	id:JSON.stringify(arrObj)
	        },
	        success:function (data) {
	      	  	Dialog.hide();
	            Dialog.alert("成功","文件共享成功");

	        },
	        error : function(data){
	            if(data.responseText == null){
	                Dialog.alert("失败","文件共享失败");
	            }else{
	                Dialog.alert("失败",data.responseText);
	            }
	        }
	    });
}

/**
 * 文件夹树
 */
function loadFolderTree(){
	 Dialog.openRemote("文件夹树",springUrl+"/doc/wffolder_treeIndex","305","400",
	[
   	 {
		 'label': '取消',
		 'class': 'btn-success',
		 'callback': function() {
   	
    	}
	},
	{	
       'label': '确定',
       'class': 'btn-success',
       'callback': function() {
      	 var folder=getSelectedNodes();
      	 if(folder[0].id=="null"){
      		$("#folderId").val("");
    		$("#folderName").val("");
    		$("#selectFolder").html("请选择");
      	 }else{
      		$("#folderId").val(folder[0].id);
    		$("#folderName").val(folder[0].name);
             if(folder[0].name.length>10){
//               '<a href="#" onclick="fileShow(\''+obj.aData.id+'\')">'+obj.aData.fileName+'</a>';
                 $("#selectFolder").html('<spn title="'+folder[0].name+'">'+folder[0].name.substring(0,10)+'......</span>');
             }else{
    	        $("#selectFolder").html('<spn title="'+folder[0].name+'">'+folder[0].name+'</span>');
             }
      	 }
       }
    }]);
}

/**
 * 加载文件类型
 */
function loadDocType(){
    jQuery.ajax({
        type:"post",
        cache:false,
        dataType:'json',
        url:springUrl+"/dict/dictData_findDictDataList?dictTypeCode=docType",
        success:function (data) {
            for(var i=0;i<data.length;i++){
                $("<option value='"+data[i].dictCode+"'>"+data[i].dictName+"</option>").appendTo($("#fileSuffix"));
            }
        },
        error : function(data){
              Dialog.alert("失败!","加载文档类型失败");
        }
    });
}

function deleteBusinessExpression (fileId){
    Dialog.confirm("提示信息","继续执行此操作将会去掉所有权限","确定","取消",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                dataType:'json',
                url:springUrl + "/doc/wffile_DeleteBusinessExpression",
                data:{
                    fileId: fileId
                },
                success:function (data) {
                      Dialog.hide();
                      Dialog.alert("成功","文件共享成功");
                },
                error : function(data){
                      Dialog.alert("失败","文件共享失败");
                }
            });
        }

    }) ;

}
/**
 * 初始化页面
 */
$(document).ready(function() {
    //加载文件类型
    loadDocType();
    //加载文件列表
    fileTable = Table.dataTable("wffile_table",options);
    //加载文件夹
    $('#selectFolder').click(function() {
        loadFolderTree();
    });
    //查询按钮绑定事件
    $('#search').click(function() {
        Table.render(fileTable);
    });
});

