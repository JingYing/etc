/**
 * 文档-查看文档
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 初始化操作
 */
$(function() {
    //返回按钮绑定事件
    $("#callback").click(function(){
        showCancel()
    });

    //下载按钮绑定事件
    $("#downLoadFile").click(function(){
        downLoadFile();
    });
});
/**
 * 关闭详细页
 */
function showCancel(){
	window.location.href=springUrl+"/doc/wffile_index";
}
/**
 * 下载响应
 */
function downLoadFile(){
    $("#downForm").attr("method","post");
    $("#downForm").attr("action",springUrl+"/doc/wffile_download");
    $("#downForm").submit();
}