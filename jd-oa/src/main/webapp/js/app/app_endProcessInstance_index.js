/**
 * 任务处理页面JS
 * User: zhaoming
 * Date: 13-10-15
 * Time: 下午5:43
 * To change this template use File | Settings | File Templates.
 */

$(function(){
    var processInstanceId = $("#processInstanceId").val();
    var processDefinitionIsOuter = $("#processDefinitionIsOuter").val();
    var businessId =$("#businessId").val();
    if(processDefinitionIsOuter == "0"){
        document.getElementById('taskInfoFrame').src = springUrl + "/app/processApp_open_bindReport?processInstanceId="+processInstanceId+"&processInstanceStatus=5";
    }else if(processDefinitionIsOuter =="2"){
        //统一待办表单
        document.getElementById('taskInfoFrame').src = springUrl+"/app/processApp_open_bindReport?businessObjectId="+businessId+"&type=unite";
    }else{

    }
    document.getElementById('taskInfoFrame').onload = function(){iFrameHeight();};
//    $("#loadTaskInfo").load(springUrl + "/app/processApp_open_bindReport?processInstanceId="+processInstanceId+"&processInstanceStatus=5");
    getProcessTrajectory(processInstanceId);
});
/**
 * 调整iframe高度
 */
function iFrameHeight() {
	var ifm= document.getElementById("taskInfoFrame");   
	var subWeb = document.frames ? document.frames["taskInfoFrame"].document : ifm.contentDocument;   
	if(ifm != null && subWeb != null) {
	   ifm.height = subWeb.body.scrollHeight;
	}
	$("#showbox").removeClass("hide");
    try{ //解决IE8显示不全的问题
        $("#showbox").parent().parent().css("overflow-y","auto");
    }catch (e){
    }
}
/**
 * 获取流程实例运行轨迹
 * @param processInstanceId 流程实例ID
 */
function getProcessTrajectory(processInstanceId){
    jQuery.ajax( {
        type: "POST",
        url: springUrl + "/app/processTrajectory",
        dataType: "json",
        data: {
            processInstanceId: processInstanceId
        },
        success: function(data) {
            var html = "";
            for(var i = 0; i<data.length; i++){
                html = html + "<tr>";
                html = html + "<td width='100px'>" + isEmpty(data[i].taskName) + "</td>";
                html = html + "<td width='70px'>" + isEmpty(data[i].userId) + "</td>";
                html = html + "<td width='60px'>" + isEmpty(data[i].result).replace("0","提交申请").replace("1","批准").replace("2","拒绝").replace("3","驳回").replace("4","重新申请 ").replace("5","加签审批 ").replace("6","加签 ") + "</td>";
                html = html + "<td style = 'text-align: left' width = '300px'>" + isEmpty(data[i].comment) + "</td>";
                html = html + "<td width='140px'>" + dateHandle(data[i].beginTime) + "</td>";
                html = html + "<td width='140px'>" + dateHandle(data[i].endTime) + "</td>";
                html = html + "</tr>";
            }
            $("#processTrajectory").append(html);
        },
        error: function (data) {
            Dialog.alert("失败","获取任务处理历史失败");
        }
    });
}

function isEmpty(obj){
    if(obj == null || obj=="" || obj=="null"){
        return "";
    }else{
        return obj;
    }
}

function dateHandle(obj){
    if(obj == null || obj=="" || obj=="null"){
        return "";
    }else{
        return new Date(obj).format("yyyy-MM-dd hh:mm:ss");
    }
}

Date.prototype.format = function(format)
{
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "h+" : this.getHours(),   //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter
        "S" : this.getMilliseconds() //millisecond
    };
    if(/(y+)/.test(format)){
        format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    for(var k in o){
        if(new RegExp("("+ k +")").test(format)){
            format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
        }
    }
    return format;
}
