var personFlag = '';
var hiddenTrFlag = 1;
var table;
var dataTableQueryAsync=true;
var options = Table.options;
/**
 * 分页配置
 * @type {{pageUrl: string, scrollY: string, sendData: Function, columns: Array, btns: Array}}
 */
options = {
    /*pageUrl: springUrl + "/app/processSearch_findPageForMyApply",*/
    pageUrl:"/app",
    scrollY: "100%",
    //defaultSort:[[1,"asc"],[2,"desc"]],
    sendData: function (sSource, aoData, fnCallback) {

    	if(arrayParam != null){
    		arrayParam = [];
    	}
    	if(objectRowData != null){
    		objectRowData = [];
    	}
    	
        var id = $("#processInstanceName").val();
        if (id == null) id = "";
        var handleTime = $("#handleTime").val();
        if (handleTime == null) handleTime = "";
        else {
            handleTime = handleTime.split("-")[1];
        }
        var receiveTime = $("#receiveTime").val();
        if (receiveTime == null) receiveTime = "";
        else {
            receiveTime = receiveTime.split("-")[1];
        }
        var applyTime = $("#applyTime").val();
        if (applyTime == null) applyTime = "";
        else {
            applyTime = applyTime.split("-")[1];
        }
        var processStatue = $("#processStatue").val();
        if (processStatue == null) processStatue = "";
        var proxyPersonId = $("#proxyPersonId").val();
        if (proxyPersonId == null) proxyPersonId = "";
        var applyPersonId = $("#applyPersonId").val();
        if (applyPersonId == null) applyPersonId = "";
        var proxiedPersonId = $("#proxiedPersonId").val();
        if (proxiedPersonId == null) proxiedPersonId = "";
        var processTypeInfoId = $("#processTypeInfoId").val();
        if (processTypeInfoId == null) processTypeInfoId = "";
        var processInstanceName = $("#processInstanceNameZT").val();
        if (processInstanceName == null) processInstanceName = "";
        aoData.push({ "name": "id", "value": id }, { "name": "beginTime", "value": applyTime }, { "name": "status", "value": processStatue },
            { "name": "starterId", "value": applyPersonId}, { "name": "proxyId", "value": proxyPersonId}, { "name": "proxiedId", "value": proxiedPersonId}
            , { "name": "receiveTime", "value": receiveTime}, { "name": "dueTime", "value": handleTime}, { "name": "processTypeId", "value": processTypeInfoId},
            { "name": "processInstanceName", "value": processInstanceName});
        jQuery.ajax({
            type: "POST",
            url: sSource,
            async:dataTableQueryAsync,
            dataType: "json",
            data: aoData,
            success: function (json) {
                fnCallback(json);
                dataTableQueryAsync=true;
            },
            error: function (data) {
            	var sAjaxSource;
            	if(table!=null){
            		sAjaxSource = table.fnSettings().sAjaxSource;
            	}
                if(sAjaxSource==null||sAjaxSource=="/app"){
                }else{
                	parent.Dialog.alert("失败","查询流程任务列表失败");
                }
                dataTableQueryAsync=true;
                changeHeight();
            }
        });
    },
    columns: [
        { "mData": "id", "sTitle": "id", "bSortable": false, "bVisible": false},
        { "mData": "code", "sTitle": "编号", "bSortable": false, "bVisible": false},
        { "mData": "processInstanceName", "sTitle": "主题", "bSortable": false, "sClass": "my_class", fnRender: function (obj) {
            var status = obj.aData.status;
            var textSelect = $("#processTypeId").find("option:selected").val();
            
            var html = getShiyouHtml(status,textSelect,obj);
            return html;
        }},
        { "mData": "starterId", "sTitle": "申请人", "bSortable": false, "sClass": "my_class", fnRender: function (obj) {
            return   obj.aData.realName + "(" + obj.aData.organizationName + ")";
        }},
        { "mData": "beginTime", "sTitle": "申请时间", "bSortable": false, "sClass": "my_class", "mRender": function (obj) {
            return obj == null ? "" : new Date(obj).format("yyyy-MM-dd hh:mm:ss");
        }},
        {"sTitle": "状态", "bSortable": false, "bVisible": true, fnRender: function (obj) {
            var statusDisplay;
            switch (obj.aData.status) {
                case "0" :
                    statusDisplay = "<font color=#236B8E>草稿</font>";
                    break;
                case "1" :
                    statusDisplay = obj.aData.currentNoteName + "-" + "<font color=#01DF01>审批中</font>";
                    break;
                case "2" :
                    statusDisplay = obj.aData.currentNoteName + "-" + "<font color=#FF0000>拒绝</font>";
                    break;
                case "3" :
                    statusDisplay = obj.aData.currentNoteName + "-" + "<font color=#FF0000>驳回</font>";
                    break;
                case "4" :
                    statusDisplay = "<font color=#FF0000>取消</font>";
                    break;
                case "5" :
                    statusDisplay = "<font color=#00FF00>已完成</font>";
                    break;
                default:
                    statusDisplay = "";
                    break;
            }
            return statusDisplay;
        }},
        {"mData": "id", "sTitle": "操作", sWidth:"200px","bSortable": false, fnRender: function (obj) {
        	var processInstanceId = obj.aData.processInstanceId;
        	var processInstanceName =obj.aData.processInstanceName;
        	
        	var processInstanceKey = obj.aData.id;
        	var mark = obj.aData.mark;
        	var status = obj.aData.status;

            var processDefinitionIsOuter = 0;
            if(obj.aData.currentTaskNoteInfo != null){
                processDefinitionIsOuter = obj.aData.currentTaskNoteInfo.processDefinitionIsOuter;
            }

        	var html = "";
            if (obj.aData.status == "0" && $("#processTypeId").val().split('-')[1] == "applyYourself") {
            	html = "<a href='#' onclick='cancelApplyForDraft(\"" + processInstanceKey 
            		+ "\",\"" + processInstanceId + "\");'><i class='icon-edit'></i>取消申请</a>";
            } else if (obj.aData.status == "3" 
            	&& $("#processTypeId").val().split('-')[1] == "applyYourself") {
            	if(typeof(obj.aData.currentTaskNoteInfo) != 'undefined'){
            		var taskId = obj.aData.currentTaskNoteInfo.id;
                	var taskName =obj.aData.currentTaskNoteInfo.name;
                	var taskDefinitionKey = obj.aData.currentTaskNoteInfo.taskDefinitionKey;
                	
                	html = "<a href='#' onclick='cancelApply(\"" + processInstanceId +  "\",\"" + status + "\");'><i class='icon-edit'></i>取消申请</a>";
                	html = html + "<a href='#' onclick='openTaskHandle(\""
                			 + taskId + "\",\"" 
                             + taskName + "\",\"" 
                             + processInstanceId + "\",\"" 
                             + taskDefinitionKey + "\");'><i class='icon-edit'></i>重新申请</a>";
            	}
            } else if (obj.aData.status == "2" 
            	&& $("#processTypeId").val().split('-')[1] == "applyYourself" && obj.aData.isFirstNode) {
            	var taskId = obj.aData.currentTaskNoteInfo.id;
            	var taskName =obj.aData.currentTaskNoteInfo.name;
            	var taskDefinitionKey = obj.aData.currentTaskNoteInfo.taskDefinitionKey;
            	
            	html = "<a href='#' onclick='cancelApply(\"" + processInstanceId + "\",\"" + status + "\");'><i class='icon-edit'></i>取消申请</a>";
            	html = html + "<a href='#' onclick='openTaskHandle(\""
            			+ taskId + "\",\"" 
		                + taskName + "\",\"" 
		                + processInstanceId + "\",\"" 
		                + taskDefinitionKey + "\");'><i class='icon-edit'></i>重新申请</a>";
            } else if(typeof(mark) != 'undefined' && mark == '6') {
            	//申请后但是下级经理还未审批的
            	html = "<a href='#' onclick='cancelApply(\"" + processInstanceId +  "\",\"" + mark + "\");'><i class='icon-edit'></i>取消申请</a>";
            } else {
            	var valueSelect = $("#processTypeId").find("option:selected").val();

            	switch(valueSelect) {
                    case 'DICTDATA_0000106_02-tobeapproved' ://要审批的
                    case 'DICTDATA_0000106_06-toBeApprovalForOthers' ://要代别人审批的
                    	var taskType = obj.aData.currentTaskNoteInfo.taskType;
                    	if(processDefinitionIsOuter=="0"&&taskType=='owner'){
                    		objectRowData[processInstanceId]=obj;
                        	html = "<a href='#' onclick='approve(\"" + processInstanceId + "\");'><i class='icon-edit'></i>批准</a>";
                        	html = html + "<a href='#' onclick='reback(\"" + processInstanceId + "\");'><i class='icon-edit'></i>拒绝</a>";
                        	html = html + "<a href='#' onclick='reject(\"" + processInstanceId + "\");'><i class='icon-edit'></i>驳回</a>";
                    	}else if(processDefinitionIsOuter=="2"&&taskType=='owner'){
                            objectRowData[processInstanceId]=obj;
                            html = "<a href='#' onclick='approveForUnite(\"" + processInstanceId + "\");'><i class='icon-edit'></i>批准</a>";
                            html = html + "<a href='#' onclick='rebackForUnite(\"" + processInstanceId + "\");'><i class='icon-edit'></i>拒绝</a>";
                        }else{}
                        break;
                    case 'DICTDATA_0000106_04-toBeSupervise'://要督办的
                    	objectRowData[processInstanceId]=obj;
                    	html = "<a href='#' onclick='addSupervise(\"" + processInstanceId + "\");'><i class='icon-edit'></i>督办</a>";
                    default:                   
                        break;
                };
            }
            if(processDefinitionIsOuter=="0"&&status != '0'){
        		//增加流程跟踪图查看链接--只有流程中心待办任务才有流程状态图
        		html = html + "<a href='#' onclick='processTrackDetail(\"" + processInstanceId +  "\");'><i class='icon-edit'></i>流程状态</a>";
        	}
            return html;
        }}
    ],
    btns: [],
    callback_fn:function(){
        changeHeight();
    }
};

/**
 * 流程跟踪图
 * @param processInastanceId
 * @param processInatanceName
 */
function processTrackDetail(processInastanceId) {
      parent.Dialog.openRemote('流程图跟踪', '/app/processApply_detail?instanceId=' + processInastanceId, 1000, 520);
}

/**
 * 重新申请 驳回或拒绝到第一个节点时调用
 * @param taskId
 * @param taskName
 * @param processInstanceId
 * @param taskDefinitionKey
 */
function openTaskHandle(taskId, taskName, processInstanceId, taskDefinitionKey) {
    parent.Dialog.openRemote("重新申请", springUrl + "/app/taskHandle_againApply?taskId=" + taskId 
    		+ "&taskName=" + encodeURI(encodeURI(taskName)) + "&processInstanceId=" + processInstanceId 
    		+ "&taskDefinitionKey=" + taskDefinitionKey + "&mark=1", "1000", "620"
    );
}

/**
 * 批准
 * @param processInstanceId
 * @param taskId
 * @param nodeId
 */
var objectRowData = [];
function approve(processInstanceId){
	var obj = objectRowData[processInstanceId];
    var nodeId=obj.aData.currentTaskNoteInfo.nodeId;
    var businessId = obj.aData.businessInstanceId;
    var taskId = obj.aData.currentTaskNoteInfo.id;
    var processDefinitionId= obj.aData.currentTaskNoteInfo.processDefinitionId;
    var taskType=obj.aData.currentTaskNoteInfo.taskType;
    var processInstanceId = obj.aData.processInstanceId;
    var taskName =obj.aData.currentTaskNoteInfo.name;
    var bySignerUserId= obj.aData.bySignerUserId;
    var startTime= obj.aData.currentTaskNoteInfo.startTime;
    var processDefinitionKey =obj.aData.currentTaskNoteInfo.processDefinitionKey;
    var processInstanceName = $(obj.aData.processInstanceName).text();
    var followCode = obj.aData.followCode;
    var assignerId = obj.aData.assignee;  //代理人
    var isProxy = obj.aData.isProxy;  //是否代理
    
        jQuery.ajax({
            type:"POST",
            cache:false,
            dataType : 'json',
            data:{
                id :taskId,
                processDefinitionId: processDefinitionId,
                nodeId:nodeId,
                taskType:taskType,
                buinessInstanceId:businessId,
                processInstanceId:processInstanceId,
                name:taskName,
                addSignerUserId: bySignerUserId,
                startTime:   startTime,
                processDefinitionKey:processDefinitionKey,
                processInstanceName:processInstanceName,
                assignerId: assignerId,   //代理人
                isProxy: isProxy,
                comments:"",
                isQuick:"1"
            },
            url:springUrl + "/app/processTask_complete",
            success:function (data) {
                if(data.Success){
                	refershTable();
                	parent.Dialog.alert("提示信息",data.message);
                } else{
                	if(data.message=="notNull"){
                		var obj = objectRowData[processInstanceId];
                		openTaskHandleIndex_daiban(processInstanceId,processInstanceName,startTime.replace(" ","T"));
                	}else{
                		parent.Dialog.alert("提示信息",data.message);
                	}
                }
            },
            error: function (data) {
            	parent.Dialog.alert("提示信息","审批失败");
            }
        });
}
/**
 * 批准-统一待办任务批准操作
 * @param processInstanceId
 */
function approveForUnite(processInstanceId){
    var obj = objectRowData[processInstanceId];
    var nodeId=obj.aData.currentTaskNoteInfo.nodeId;
    var businessId = obj.aData.businessInstanceId;
    var taskId = obj.aData.currentTaskNoteInfo.id;
    var processDefinitionId= obj.aData.currentTaskNoteInfo.processDefinitionId;
    var taskType=obj.aData.currentTaskNoteInfo.taskType;
    var processInstanceId = obj.aData.processInstanceId;
    var taskName =obj.aData.currentTaskNoteInfo.name;
    var bySignerUserId= obj.aData.bySignerUserId;
    var startTime= obj.aData.currentTaskNoteInfo.startTime;
    var processDefinitionKey =obj.aData.currentTaskNoteInfo.processDefinitionKey;
    var processInstanceName = $(obj.aData.processInstanceName).text();
    var followCode = obj.aData.followCode;
    var assignerId = obj.aData.assignee;  //代理人
    var isProxy = obj.aData.isProxy;  //是否代理

    jQuery.ajax({
        type:"POST",
        cache:false,
        async : true,
        dataType : 'json',
        data:{
            id :taskId,
            processDefinitionId: processDefinitionId,
            nodeId:nodeId,
            buinessInstanceId: businessId,
            taskType:taskType,
            processDefinitionKey:processDefinitionKey,
            startTime: startTime,
            name: taskName,
            assignerId:bySignerUserId,
            isProxy:'1',
            addSignerUserId: '',
            processInstanceId: processInstanceId,
            processInstanceName: processInstanceName,
            comments:'',
            resultType:"1"
        },
        url:springUrl + "/app/processTask_complete_unite",
        success:function (data) {
            if(data.Success){
                refershTable();
                parent.Dialog.alert("提示信息",data.message);
            } else{
                if(data.message=="notNull"){
                    var obj = objectRowData[processInstanceId];
                    openTaskHandleIndex_daiban(processInstanceId,processInstanceName,startTime.replace(" ","T"));
                }else{
                    parent.Dialog.alert("提示信息",data.message);
                }
            }
        },
        error: function (data) {
            parent.Dialog.alert("提示信息","审批失败");
        }
    });
}

/**
 * 驳回操作
 * @param obj
 */
function reject(processInstanceId){
	var obj = objectRowData[processInstanceId];
    var nodeId=obj.aData.currentTaskNoteInfo.nodeId;
    var businessId = obj.aData.businessInstanceId;
    var taskId = obj.aData.currentTaskNoteInfo.id;
    var processDefinitionId= obj.aData.currentTaskNoteInfo.processDefinitionId;
    var taskType=obj.aData.currentTaskNoteInfo.taskType;
    var processInstanceId = obj.aData.processInstanceId;
    var taskName =obj.aData.currentTaskNoteInfo.name;
    var bySignerUserId= obj.aData.bySignerUserId;
    var startTime= obj.aData.currentTaskNoteInfo.startTime;
    var processDefinitionKey =obj.aData.currentTaskNoteInfo.processDefinitionKey;
    var processInstanceName = $(obj.aData.processInstanceName).text();
    var assignerId = obj.aData.assignee;  //代理人
    var isProxy = obj.aData.isProxy;  //是否代理
    
    parent.Dialog.openRemote("驳回意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.$("#approveComment").val();
                if(!parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
            		async:true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        taskType:taskType,
                        buinessInstanceId:businessId,
                        processInstanceId:processInstanceId,
                        name:   taskName,
                        addSignerUserId:  bySignerUserId,
                        startTime:   startTime,
                        processDefinitionKey:processDefinitionKey,
                        processInstanceName:processInstanceName,
                        assignerId: assignerId,   //代理人
                        isProxy: isProxy,
                        comments:comment
                    },
                    url:springUrl + "/app/processTask_RejectToFirst",
                    success:function (data) {
                        if(data.Success){
                        	parent.Dialog.alert("提示信息",data.message);
                        	refershTable();
                        	parent.Dialog.hideModal(modal);
                        }else{
                        	parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                    	parent.Dialog.alert("提示信息","驳回失败!");
                    }
                });
                return false;
            }
        }
    ]);
}

/**
 * 拒绝操作
 * @param obj
 */
function reback(processInstanceId){
	var obj = objectRowData[processInstanceId];
    var nodeId=obj.aData.currentTaskNoteInfo.nodeId;
    var businessId = obj.aData.businessInstanceId;
    var taskId = obj.aData.currentTaskNoteInfo.id;
    var processDefinitionId= obj.aData.currentTaskNoteInfo.processDefinitionId;
    var taskType=obj.aData.currentTaskNoteInfo.taskType;
    var processInstanceId = obj.aData.processInstanceId;
    var taskName =obj.aData.currentTaskNoteInfo.name;
    var bySignerUserId= obj.aData.bySignerUserId;
    var startTime= obj.aData.currentTaskNoteInfo.startTime;
    var processDefinitionKey =obj.aData.currentTaskNoteInfo.processDefinitionKey;
    var processInstanceName = $(obj.aData.processInstanceName).text();
    var assignerId = obj.aData.assignee;  //代理人
    var isProxy = obj.aData.isProxy;  //是否代理
    
    parent.Dialog.openRemote("拒绝意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.$("#approveComment").val();
                if(!parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    async:true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        taskType:taskType,
                        buinessInstanceId:businessId,
                        processInstanceId:processInstanceId,
                        name:   taskName,
                        addSignerUserId:  bySignerUserId,
                        startTime:   startTime,
                        processDefinitionKey:processDefinitionKey,
                        processInstanceName:processInstanceName,
                        assignerId: assignerId,   //代理人
                        isProxy: isProxy,
                        comments:comment
                    },
                    url:springUrl + "/app/processTask_RejectToPrevious",
                    success:function (data) {
                        if(data.Success){
                        	parent.Dialog.alert("提示信息",data.message);
                        	refershTable();
                        	parent.Dialog.hideModal(modal);
                          } else{
                        	parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                    	parent.Dialog.alert("提示信息","拒绝失败!");
                    }
                });
                return false;
            }
        }
    ]);
}


/**
 * 拒绝操作 for 统一待办任务
 * @param obj
 */
function rebackForUnite(processInstanceId){
    var obj = objectRowData[processInstanceId];
    var nodeId=obj.aData.currentTaskNoteInfo.nodeId;
    var businessId = obj.aData.businessInstanceId;
    var taskId = obj.aData.currentTaskNoteInfo.id;
    var processDefinitionId= obj.aData.currentTaskNoteInfo.processDefinitionId;
    var taskType=obj.aData.currentTaskNoteInfo.taskType;
    var processInstanceId = obj.aData.processInstanceId;
    var taskName =obj.aData.currentTaskNoteInfo.name;
    var bySignerUserId= obj.aData.bySignerUserId;
    var startTime= obj.aData.currentTaskNoteInfo.startTime;
    var processDefinitionKey =obj.aData.currentTaskNoteInfo.processDefinitionKey;
    var processInstanceName = $(obj.aData.processInstanceName).text();
    var assignerId = obj.aData.assignee;  //代理人
    var isProxy = obj.aData.isProxy;  //是否代理

    parent.Dialog.openRemote("拒绝意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.$("#approveComment").val();
                if(!parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    async : true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        buinessInstanceId: businessId,
                        taskType:taskType,
                        processDefinitionKey:processDefinitionKey,
                        startTime: startTime,
                        name: taskName,
                        assignerId:bySignerUserId,
                        isProxy:'1',
                        addSignerUserId: '',
                        processInstanceId: processInstanceId,
                        processInstanceName: processInstanceName,
                        comments:comment,
                        resultType:"2"
                    },
                    url:springUrl + "/app/processTask_complete_unite",
                    success:function (data) {
                        if(data.Success){
                            parent.Dialog.alert("提示信息",data.message);
                            refershTable();
                            parent.Dialog.hideModal(modal);
                        } else{
                            parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                        parent.Dialog.alert("提示信息","拒绝失败!");
                    }
                });
                return false;
            }
        }
    ]);
}


/**
 * 督办操作
 * @param processInstanceId
 */
function addSupervise(processInstanceId){
	var obj = objectRowData[processInstanceId];
	if(typeof(obj.aData.currentTaskNoteInfo) != 'undefined'){
		var nodeId=obj.aData.currentTaskNoteInfo.nodeId;
		var taskId = obj.aData.currentTaskNoteInfo.id;
		var params = [];
		params.push({ "name": "processInstanceId", "value":processInstanceId}) ;
		params.push({ "name": "nodeId", "value":nodeId}) ;
		params.push({ "name": "taskId", "value":taskId}) ;
		jQuery.ajax( {
			async: true ,
	        url:springUrl + "/app/processSupervise_add", 
	        data: params, 
	        success: function(resp) {
	        	if(resp!=null && parseInt(resp) >0){
	        		parent.Dialog.alert("操作结果","操作成功！");
	        		refershTable();
	        	}
	        	else if(resp!=null && parseInt(resp) == 0)
	        		parent.Dialog.alert("操作结果","操作失败：没有获取到督办人！");
	        	else
	        		parent.Dialog.alert("操作结果","操作失败！");
	         },
            error: function (data) {
            	parent.Dialog.alert("失败","获取督办人失败");
            }
		});
	} else {
		parent.Dialog.alert("操作结果","操作失败：异常流程，没有获取到流程当前任务！");
	}
}

/**
 * 申请人，代理人，被代理人点击弹出框 选择人员
 */
function remoteOrgUserInfo() {
    var buttons = [
        {
            "label": "取消",
            "class": "inactive",
            "callback": function () {
//                cancel();
            }
        },
        {
            "label": "确定",
            "class": "active1",
            "callback": function () {
                var selectRecords = Table.getSelectedRows(parent.orgUserDataTable);
                if (selectRecords.length > 1) {
                    parent.Dialog.alert("提示信息", "每次只能选择一个人员");
                    return false;
                } else {
                    if (selectRecords.length == 0) {
                        setUserInfo('', '');
                    }
                    else {
                        setUserInfo(selectRecords[0].id, selectRecords[0].realName);
                    }
                }

            }
        }
    ];
    parent.Dialog.openRemote('选择人员', '/system/sysAddress_getOrgUser', 800, 500, buttons);
}


function setUserInfo(userId, userName) {
    $("#" + personFlag + "PersonId").val(userId);
    $("#" + personFlag + "Person").val(userName);
}

function processStatusChangeInit(processStatueForQuery) {
    $("#processStatue").empty();
    if ($("#processTypeId").val() != "") {
        jQuery.ajax({
            type: "POST",
            async:false,
            url: springUrl + "/dict/dictData_findDictDataListByPid",
            data: {
                pid: $("#processTypeId").val().split("-")[0]
            },
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    if(processStatueForQuery== data[i].dictCode) {
                        $("#processStatue").append("<option value=" + data[i].dictCode + " selected>" + data[i].dictName + "</option>");
                    }else{
                        $("#processStatue").append("<option value=" + data[i].dictCode + ">" + data[i].dictName + "</option>");
                    }

                }
                reDrawTable();
            },
            error: function (data) {
            	parent.Dialog.alert("失败","流程状态查询条件初始化失败");
            }
        });
    }
}
function processStatusChange() {
    $("#processStatue").empty();
    if ($("#processTypeId").val() != "") {
        jQuery.ajax({
            type: "POST",
            url: springUrl + "/dict/dictData_findDictDataListByPid",
            data: {
                pid: $("#processTypeId").val().split("-")[0]
            },
            success: function (data) {
                for (var i = 0; i < data.length; i++) {

                    $("#processStatue").append("<option value=" + data[i].dictCode + ">" + data[i].dictName + "</option>");

                }
            },
            error: function (data) {
            	parent.Dialog.alert("失败","流程状态值联动失败");
            }
        });
    }
}

/**
 * 页面初始化加载
 */
$(document).ready(function () {
    if (processStatueForQuery != "") {
        dataTableQueryAsync = false;
        table = Table.dataTable("processSearchTable", options);
        processStatusChangeInit(processStatueForQuery);
    } else {
        dataTableQueryAsync = false;
        $("#processStatue").val("approved");
        $("#processTypeId").val("DICTDATA_0000106_01-applyYourself");
        table = Table.dataTable("processSearchTable", options);
        reDrawTable();
    }

    /**
     * 数据字典-数据项查询绑定事件
     */
    $('#processSearchFun').click(function () {
        var textSelect = $("#processTypeId").find("option:selected").text();
        var valSelect = $("#processTypeId").find("option:selected").val();
        switch (valSelect) {
            case 'DICTDATA_0000106_01-applyYourself' :  //自己申请的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApply";
                break;
            case 'DICTDATA_0000106_02-tobeapproved' :  //要审批的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApproval";
                break;
            case 'DICTDATA_0000106_03-approvalOver' :  //审批过的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApprovalOver";
                break;
            case 'DICTDATA_0000106_06-toBeApprovalForOthers' :  //要代别人审批的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForInsteadApproval";
                break;
            case 'DICTDATA_0000106_07-ApprovalOverForOthers' :  //代别人审批过的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForInsteadApprovalOver";
                break;
            case 'DICTDATA_0000106_08-othersForwarding' :  //别人加签的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForForward";
                break;
            case 'DICTDATA_0000106_04-toBeSupervise' :  //要督办的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForSupervise";
                break;
            case 'DICTDATA_0000106_05-superviseOver' :  //督办过的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForSuperviseOver";
                break;
            default:
                break;
        }
        Table.render(table);
    });
    //选择员工
    $('#applyPerson,#applyPersonSpan').click(function () {
        personFlag = 'apply';
        remoteOrgUserInfo();
    });
    $('#proxyPerson,#proxyPersonSpan').click(function () {
        personFlag = 'proxy';
        remoteOrgUserInfo();
    });
    $('#proxiedPerson,#proxiedPersonSpan').click(function () {
        personFlag = 'proxied';
        remoteOrgUserInfo();
    });

});
/**
 * 复制一份表单
 * @param processDefinitionKey
 * @param businessObjectId
 * @param modal
 */
function copyForm(processDefinitionKey,businessObjectId,modal){
    var data={
        "processDefinitionKey":processDefinitionKey,
        "businessObjectId":businessObjectId
    };
    jQuery.ajax( {
        type: "POST",
        url:springUrl+"/app/processApply_copy",
        dataType: "json",
        data: data,
        success: function(result) {
            if(result=="success"){
                parent.Dialog.hideModal(modal);
                parent.Dialog.alert("复制成功","复制表单成功，请到申请&待办页面查看使用！");
            }
        },
        error:function(msg){
            parent.Dialog.alert("操作失败",msg.responseText);
        }
    });
}

function processApplyButtonClick(businessId, processName, pafKey, status){
//    var bool = confirm("编辑表单是否弹出新窗口页面?");
    var bool = false;
    if(bool){
        var url=springUrl+'/app/processApp_open_bindReport_alone?processDefinitionKey='+businessId;
        window.open(url);
    }else{
        openTaskHandleIndex(businessId, processName, pafKey, status);
    }
}
/**
 * 弹出任务处理页面 申请时使用
 * @param processName 流程名称
 * @param processInstanceId 流程实例ID
 * @param taskInstanceId 任务实例ID
 */
function openTaskHandleIndex(businessId, processName, pafKey, status) {
    var url = springUrl + "/app/process_apply?processDefinitionKey=" + businessId;
    var buttons = [
        {
            "label": "取消",
            "class": "inactive",
            "callback": function () {
            }
        },
        {
            "label": "复制表单",
            "class": "active1",
            "callback": function (modal) {
                copyForm(pafKey,businessId,modal);
                return false;
            }
        },
        {
            "label": "保存草稿",
            "id" : "saveDraftButton",
            "class": "active1",
            "callback": function (modal) {
                var frame = parent.document.getElementById('bindReportFrame').contentWindow;
                frame.saveDraft(modal);
                return false;
            }
        },
        {
            "label": "提交",
            "id" : "submitProcessButton",
            "class": "active1",
            "callback": function (modal) {
                var frame = parent.document.getElementById('bindReportFrame').contentWindow;
//                parent.Dialog.confirm("系统消息", "提交后将无法修改，确定提交申请？", "确定", "取消", function (result) {//提交前确认
//                	if(result){
//                		 frame.submitProcess(modal);
//                         return false;
//                	}
//                });
                frame.submitProcess(modal);
                return false;
            }
        }
    ];
    parent.Dialog.openRemote(processName, url, 1000, 620, buttons);
}

/**
 * 草稿状态重新打开
 * @param processDefinitionKey 流程定义Key
 * @param processDefinitionName 流程定义名称
 * @param businessObjectId 业务实例ID
 */
function draftInfoProcess(processInstanceStatus, processDefinitionKey, businessObjectId, processDefinitionName) {
//    var bool = confirm("编辑表单是否弹出新窗口页面?");
    var bool = false;
    if(bool){
        var url = springUrl + "/app/processApp_open_bindReport_alone?processDefinitionKey=" + processDefinitionKey
            + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=" + processInstanceStatus;
        window.open(url);
    }else{
        var url = springUrl + "/app/process_apply?processDefinitionKey=" + processDefinitionKey
            + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=" + processInstanceStatus;
        var buttons = [
            {
                "label": "取消",
                "class": "btn-cancel",
                "callback": function () {
                }
            },
            {
                "label": "复制表单",
                "class": "active1",
                "callback": function (modal) {
                    copyForm(processDefinitionKey,businessObjectId,modal);
                    return false;
                }
            },
            {
                "label": "保存草稿",
                "class": "active1",
                "callback": function (modal) {
                    var frame = parent.document.getElementById('bindReportFrame').contentWindow;
                    frame.saveDraft(modal);
                    return false;
                }
            },
            {
                "label": "提交",
                "class": "active1",
                "callback": function (modal) {
                    var frame = parent.document.getElementById('bindReportFrame').contentWindow;
//                    parent.Dialog.confirm("系统消息", "提交后将无法修改，确定提交申请？", "确定", "取消", function (result) {//提交前确认
//                        if(result){
//                            frame.submitProcess(modal);
//                            return false;
//                        }
//                    });
                    frame.submitProcess(modal);
                    return false;
                }
            }
        ];
        parent.Dialog.openRemote(processDefinitionName, url, 1000, 620, buttons);
    }
}
/**
 * 草稿取消状态
 */
function draftCancel(processInstanceStatus, processDefinitionKey, businessObjectId, processDefinitionName){
    var url = springUrl + "/app/process_apply?processDefinitionKey=" + processDefinitionKey
        + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=7" ;
    var buttons = [
        {
            "label": "取消",
            "class": "btn-cancel",
            "callback": function () {
            }
        }]
    parent.Dialog.openRemote(processDefinitionName, url, 1000, 620, buttons);
}

/**
 * 更多条件--精简条件
 * @param thisLi
 */
function toggleCondition(thisLi) {
    if (hiddenTrFlag != 0) {
        $(".hiddenTr").show();
        thisLi.removeClass("close11").html("精简筛选条件<b></b>");
        hiddenTrFlag = 0;
    }
    else {
        $(".hiddenTr").hide();
        thisLi.addClass("close11").html("更多筛选条件<b></b>");
        hiddenTrFlag = 1;
    }
    changeHeight();
}
/**
 * 取消申请
 * @param processInstanceKey
 */
function cancelApply(processInstanceKey,status) {
	var url = springUrl + "/app/processSearch_cancelApplyForRejected";
	if(status == '6'){
		url = springUrl + "/app/processSearch_cancelApplyForUnapproved";
	}
    parent.Dialog.confirm("确认", "确定取消申请?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                url: url,
                data: {
                    processInstanceId: processInstanceKey
                },
                success: function (data) {
                    if (data.operator == false) {
                        parent.Dialog.alert("失败！", data.message);
                        return false;
                    }
                    else {
                        parent.Dialog.alert("成功！", data.message);
                        Table.render(table);
                    }
                }
            });
        }
    });
}

/**
 * 草稿取消申请
 * @param processInstanceKey
 * @param processInstanceId
 */
function cancelApplyForDraft(processInstanceKey, processInstanceId) {
    parent.Dialog.confirm("确认", "确定取消申请?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                url: springUrl + "/app/processSearch_cancelApplyForDraft",
                data: {
                    processInstanceId: processInstanceId,
                    processInstanceKey: processInstanceKey
                },
                success: function (data) {
                    if (data.operator == false) {
                        parent.Dialog.alert("失败！", data.message);
                        return false;
                    }
                    else {
                        parent.Dialog.alert("成功！", data.message);
                        Table.render(table);
                    }
                },
                error: function (data) {
                	parent.Dialog.alert("失败","取消申请失败");
                }
            });
        }
    });
}

function loadCondition(divId, id, thisEle) {
    $("li").removeClass("curr");
    thisEle.addClass("curr");
    $("#" + divId).load(springUrl + "/app/processSearch_toggleCondition?processTypeId=" + id + "&flag=" + hiddenTrFlag);
    resetSelect(id);
}

function resetSelect(id) {
    jQuery.ajax({
        type: "POST",
        url: springUrl + "/app/processSearch_resetCondition",
        data: {
            processTypeId: id
        },
        success: function (data) {
        	$("#processTypeInfoId").val(id);
            $("#processInstanceName").empty();
            $("#processInstanceName").append("<option value=''>请选择...</option>");
            for (var i = 0; i < data.length; i++) {
                $("#processInstanceName").append("<option value=" + data[i].pafProcessDefinitionId + ">" + data[i].processDefinitionName + "</option>");
            }
        },
        error: function (data) {
        	parent.Dialog.alert("失败","重置流程状态查询条件失败");
        }
    });
}

/**
 * 分类查找
 */

function reDrawTable() {
    var textSelect = $("#processTypeId").find("option:selected").text();    
    var valSelect = $("#processTypeId").find("option:selected").val();
    switch (valSelect) {
	    case 'DICTDATA_0000106_01-applyYourself' :  //自己申请的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApply";
	        break;
	    case 'DICTDATA_0000106_02-tobeapproved' :  //要审批的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApproval";
	        break;
	    case 'DICTDATA_0000106_03-approvalOver' :  //审批过的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApprovalOver";
	        break;
	    case 'DICTDATA_0000106_06-toBeApprovalForOthers' :  //要代别人审批的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForInsteadApproval";
	        break;
	    case 'DICTDATA_0000106_07-ApprovalOverForOthers' :  //代别人审批过的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForInsteadApprovalOver";
	        break;
	    case 'DICTDATA_0000106_08-othersForwarding' :  //别人加签的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForForward";
	        break;
	    case 'DICTDATA_0000106_04-toBeSupervise' :  //要督办的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForSupervise";
	        break;
	    case 'DICTDATA_0000106_05-superviseOver' :  //督办过的
	        var oSettings = table.fnSettings();
	        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForSuperviseOver";
	        break;
	    default:
	        break;
    }
    Table.render(table);
}

/**
 * 查看流程实例历史详情
 * @param processInstanceId
 */
function detailProcessInstance(processInstanceId,pafKey,businessId) {
    var buttons = [
        {
            "label": "返回",
            "class": "active1",
            "callback": function () {
            }
        }
    ];
    if(pafKey !=null && pafKey!=""){
        buttons[1]={
            "label": "复制表单",
            "class": "active1",
            "callback": function (modal) {
                copyForm(pafKey,businessId,modal);
                return false;
            }
        }
    }
    parent.Dialog.openRemote("查看流程实例历史详情", springUrl + "/app/endProcessInstance_index?processInstanceId=" + processInstanceId, 1000, 620, buttons);
}

/**
 * 主题点击弹出页面 代办情况下
 * @param status
 * @param valueSelect
 * @param obj
 */
var arrayParam = [];
function getShiyouHtml(status,valueSelect,obj){
	var html = "";
	switch(valueSelect)
    {
      	case 'DICTDATA_0000106_01-applyYourself' ://自己申请的         
      		if(status == '0'){
        		html = "<a href='#' onclick='draftInfoProcess(\""+status+"\",\""+obj.aData.pafProcessDefinitionId + "\",\""
        			+obj.aData.businessInstanceId+"\",\""+obj.aData.processDefinitionName+ "\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";
        	} else {
                if( typeof(obj.aData.processInstanceId) == "undefined"&&status==4){//草稿取消状态
                    html = "<a href='#' onclick='draftCancel(\""+status+"\",\""+obj.aData.pafProcessDefinitionId + "\",\""
                        +obj.aData.businessInstanceId+"\",\""+obj.aData.processDefinitionName+ "\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";
                }else{
                    html = "<a href='#' onclick='detailProcessInstance(\"" + obj.aData.processInstanceId + "\",\""+obj.aData.pafProcessDefinitionId+"\",\""+
                        obj.aData.businessInstanceId +"\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";
                }

        	}
            break;
        case 'DICTDATA_0000106_02-tobeapproved' ://要审批的
        case 'DICTDATA_0000106_06-toBeApprovalForOthers' ://要代别人审批的
        	var arrObj = [];
            var object = {};
            object.nodeId = typeof(obj.aData.currentTaskNoteInfo.nodeId) != 'undefined'?obj.aData.currentTaskNoteInfo.nodeId:"";
            object.processInstanceId = obj.aData.processInstanceId;
            object.buinessInstanceId = obj.aData.businessInstanceId;
            object.taskType = obj.aData.currentTaskNoteInfo.taskType;
            object.id = obj.aData.currentTaskNoteInfo.id;
            object.name=encodeURI(encodeURI(obj.aData.currentTaskNoteInfo.name));
            var startTime=typeof(obj.aData.currentTaskNoteInfo.startTime) != 'undefined'?
            	obj.aData.currentTaskNoteInfo.startTime.replace(" ","T"):"";;
            object.addSignerUserId=obj.aData.bySignerUserId;   //加签人
            object.processDefinitionKey = obj.aData.currentTaskNoteInfo.processDefinitionKey;
            object.processDefinitionId=obj.aData.currentTaskNoteInfo.processDefinitionId;
            object.processInstanceName= encodeURIComponent(encodeURIComponent(obj.aData.processInstanceName)); //主题名称
            object.assignerId=obj.aData.assignee;  //代理人
            object.isProxy=obj.aData.isProxy;  //是否代理
            object.processDefinitionIsOuter=obj.aData.currentTaskNoteInfo.processDefinitionIsOuter;  //是否待办任务
            arrObj.push(object);
            var processName = typeof(obj.aData.currentTaskNoteInfo.processDefinitionName) != 'undefined'?
            	obj.aData.currentTaskNoteInfo.processDefinitionName:obj.aData.processInstanceName;
            var parmObj = JSON.stringify(arrObj);
            arrayParam[object.processInstanceId] = parmObj;
            html='<a href="#" onclick="openTaskHandleIndex_daiban(\''+object.processInstanceId+'\',\''+processName+'\',\''+startTime+'\')">'+concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName)+'</a>';
            break;
        case 'DICTDATA_0000106_08-othersForwarding' ://别人加签的
        	var arrObj = [];
            var object = {};
            object.nodeId = typeof(obj.aData.currentTaskNoteInfo.nodeId) != 'undefined'?obj.aData.currentTaskNoteInfo.nodeId:"";
            object.processInstanceId = obj.aData.processInstanceId;
            object.buinessInstanceId = obj.aData.businessInstanceId;
            object.taskType = obj.aData.currentTaskNoteInfo.taskType;
            object.id = obj.aData.currentTaskNoteInfo.id;
            object.name=encodeURI(encodeURI(obj.aData.currentTaskNoteInfo.name));
            var startTime=typeof(obj.aData.currentTaskNoteInfo.startTime) != 'undefined'?
            	obj.aData.currentTaskNoteInfo.startTime.replace(" ","T"):"";;
            object.addSignerUserId=obj.aData.bySignerUserId;   //加签人
            object.processDefinitionKey = obj.aData.currentTaskNoteInfo.processDefinitionKey;
            object.processDefinitionId=obj.aData.currentTaskNoteInfo.processDefinitionId;
            object.processInstanceName= encodeURI(encodeURI(obj.aData.processInstanceName)); //主题名称
            object.processDefinitionIsOuter=obj.aData.currentTaskNoteInfo.processDefinitionIsOuter;  //是否待办任务
            arrObj.push(object);
            var processName = typeof(obj.aData.currentTaskNoteInfo.processDefinitionName) != 'undefined'?
            	obj.aData.currentTaskNoteInfo.processDefinitionName:obj.aData.processInstanceName;
            var parmObj = JSON.stringify(arrObj);
            arrayParam[object.processInstanceId] = parmObj;
            html='<a href="#" onclick="openTaskHandleIndex_daiban(\''+object.processInstanceId+'\',\''+processName+'\',\''+startTime+'\')">'+concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName)+'</a>';
            break;
        case 'DICTDATA_0000106_03-approvalOver' : //审批过的 
        case 'DICTDATA_0000106_05-superviseOver' : //督办过的
        case 'DICTDATA_0000106_07-ApprovalOverForOthers'://代别人审批过的
        case 'DICTDATA_0000106_04-toBeSupervise' ://要督办的 
        	html = "<a href='#' onclick='detailProcessInstance(\"" + obj.aData.processInstanceId + "\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";
            break;
        default:                   
            break;
    }
	return html;
}

/**
 * 弹出任务处理页面 流程代办时使用
 * @param processName 流程名称
 * @param processInstanceId 流程实例ID
 * @param taskInstanceId 任务实例ID
 */
function openTaskHandleIndex_daiban(processInstanceId,processName,startTime){
	parent.Dialog.openRemote(processName,springUrl + "/app/taskHandle_index?oaTask="+arrayParam[processInstanceId]+"&startTime="+startTime,"1000","520");
}

/**
 * 刷新table
 */
function refershTable(){
	var oSettings = table.fnSettings();
	if(oSettings.sAjaxSource == '/app'){
		oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApply";
	}
	Table.render(table);
}

function concatProcessInsanceName(followCode,processInstanceName){
    if(followCode != null && followCode != ''){
        return '['+followCode+']' + processInstanceName;
    }
    return processInstanceName;
}