$(function(){
	reloadTaskNum();
	getProcessTaskAll();
});
function reloadTaskNum(){
	parent.getMyTaskNum("owner","",false);
	parent.getMyTaskNum("addsigner","",false);
	var parentAllNum=parent.$(".ownerNum:eq(0)").text();
	if(parentAllNum!=null&&parentAllNum!=0){
		$(".allNum").text("("+parentAllNum+")");
		$(".allNum").parent().show();
		var parentAddsignerNum=parent.$(".addsignerNum:eq(0)").text();
		if(parentAddsignerNum!=null&&parentAddsignerNum!=0){
			$(".addsignerNum").text("("+parentAddsignerNum+")");
			$(".addsignerNum").parent().show();
		}
	}else{
		$(".allNum").parent().parent().parent().html($("#noDataDiv").html());
	}
	parent.autoTabHeight($("#taskContainer").height()+30,'mytask');
}
function show(obj,key){
	$(".curr1").removeClass("curr1");
	obj.addClass("curr1");
	if(key=="all"){
		$(".taskPane:not(:first)").remove();
		getProcessTaskAll();
	}else if(key=="forwardTask"){
		$(".taskPane:not(:first)").remove();
		getProcessTaskAll("addsigner");
	}else{
		$("#taskContainer").find("div[id^='pdfId']").not("#pdfId"+key).each(function(){
			$(this).hide();
		});
		$("#pdfId"+key).show();
	}
	parent.autoTabHeight($("#taskContainer").height()+30,'mytask');
}
//更多页面的跳转方法
//author wangdongxing@jd.com
function more(processDefinitionId){
	var portlet=$("#pdfId"+processDefinitionId);
	var processDefinitionKey=portlet.find("tr:eq(0)").attr("processDefinitionKey");
	var taskType="owner";
	if($(".curr1").hasClass("forward")==true){
		taskType="addsigner";
	}
	var url=springUrl+"/app/processSearch_index?"
			+"taskType="+taskType+"&"
			+"processDefinitionId="+processDefinitionId+"&"
			+"processDefinitionKey="+processDefinitionKey;
	parent.parent.$("#mainFrame").attr("src",url);
}
//流程定义的刷新方法
//author wangdongxing@jd.com
function reload(processDefinitionId){
	var portlet=$("#pdfId"+processDefinitionId);
	//当前刷新流程的值
	var dataNum=$.trim(portlet.find(".dataNum").text());
	//当前标签数
	var addsignerNum=0;
	if($(".curr1").hasClass('forward')==true){
		addsignerNum=$.trim($(".addsignerNum").text()).replace("(","").replace(")","");
	}
	//全部总数
	var allNum=$.trim($(".allNum").text()).replace("(","").replace(")","");
	
	var processDefinitionId=portlet.find("tr:eq(0)").attr("processDefinitionId");
	var processDefinitionName=portlet.find("tr:eq(0)").attr("processDefinitionName");
	var processDefinitionKey=portlet.find("tr:eq(0)").attr("processDefinitionKey");
	var tableName=portlet.find("tr:eq(0)").attr("tableName");
	var processColumns=portlet.find("tr:eq(0)").attr("processColumns");
	var bussinessColumns=portlet.find("tr:eq(0)").attr("bussinessColumns");
	var taskType="";
	if($(".forward").hasClass("curr1")){
		taskType="addsigner";
	}
	var data={
		"processDefinitionId":processDefinitionId,	
		"processDefinitionName":processDefinitionName,	
		"processDefinitionKey":processDefinitionKey,	
		"tableName":tableName,	
		"processColumnsString":processColumns,
		"bussinessColumnString":bussinessColumns,
		"taskType":taskType
	};
	jQuery.ajax( {
        type: "POST",
        async:true,
        url:springUrl+'/app/processTask_reload',
        dataType: "json",
        data:data,
        success: function(result) {
        	displayProcessTask(result,"reload");
        	var total=parseInt(allNum)-parseInt(dataNum)+result.totalNum;
        	if(total>0){
            	$(".allNum").text("("+total+")");
            	parent.$(".ownerNum").text(total);
            	parent.getMyTaskNum("addsigner","");
        		if(parent.$(".addsignerNum:eq(0)").text()!=null&&parent.$(".addsignerNum:eq(0)").text()!="0"){
        			$(".addsignerNum").text("("+parent.$(".addsignerNum:eq(0)").text()+")");
        		}else{
        			$(".addsignerNum").remove();
        		}
        	}else{
        		parent.setNum("owner",0);
        		parent.$(".ownerNum").text(0);
        		$(".allNum").parent().parent().parent().html($("#noDataDiv").html());
        	}
        }
	});
}
function getProcessTaskAll(taskType){
	var data={
		"taskType":taskType
	};
	jQuery.ajax( {
		type: "POST",
		async:true,
		url:springUrl+'/app/processTask_list',
		dataType: "json",
		data:data,
		success: function(result) {
			$(".processtab").find("li:gt(1)").remove();
			var hasData=false;
			for(var key in result){
				hasData=true;
				displayProcessTask(result[key],"add");
			}
			if(!hasData){
				if($(".allNum").html()==null){
				}else{
					$("div#noDataDiv").show();
				}
			}else{
				$("div#noDataDiv").hide();
			}
		}
	});
}
function displayProcessTask(processTask,addOrRealod){
	var newDisplay;
	if(addOrRealod=="reload"){
		newDisplay=$("#taskContainer").find("#pdfId"+processTask.processDefinitionId);
		//无记录不显示
		if(processTask.totalNum==0){
			newDisplay.remove();
			$(".key"+processTask.processDefinitionId).remove();
			parent.autoTabHeight($("#taskContainer").height()+30,'mytask');
			return;
		}
		newDisplay.find("tbody").html("");
	}else if(addOrRealod=="add"){
		//无记录不显示
		if(processTask.totalNum==0) return;
		newDisplay=$("#taskContainer").find(".taskPane:eq(0)").clone();
		$("div#taskContainer").append(newDisplay);
	}
	newDisplay.attr("id","pdfId"+processTask.processDefinitionId);
	var dataListSize=0;
	if(processTask.dataList!=null){
		dataListSize=processTask.dataList.length;
	}
	var titleHtml='<div class="fl"><span class="ftx-05"> '+processTask.processDefinitionName+'- </span>有<span class="ftx-04 dataNum"> '+processTask.totalNum+'  </span>笔等待您审批</div>';
	if(processTask.totalNum>5){
		titleHtml+='<div class="fr"><a href="#" onclick="more(\''+processTask.processDefinitionId+'\');">更多<s class="icon-more"></s></a></div>';
	}
	newDisplay.find(".grid-ex:eq(0)").html(titleHtml);
	var th="<tr processDefinitionName='"+processTask.processDefinitionName+"'" +
			" processDefinitionId='"+processTask.processDefinitionId+"'"+
			" processDefinitionKey='"+processTask.processDefinitionKey+"'"+
			" tableName='"+processTask.tableName+"'"+
			" processColumns='"+JSON.stringify(processTask.processColumns)+"'"+
			" bussinessColumns='"+JSON.stringify(processTask.bussinessColumns)+"'>";
	th+="<td class='status'>&nbsp;</td>";
	$.each(processTask.processColumns,function(i,c){
		if(i==0){
			th+="<th align='left'>"+c+"</th>";
		}else{
			th+="<th>"+c+"</th>";
		}
	});
	th+="<th>操作</th>";
	th+="</tr>";
	newDisplay.find("thead").html(th);
	if(dataListSize!=0){
		$.each(processTask.dataList,function(i,d){
			var tr="<tr class='todo' taskType='"+d.taskType+"'" +
			" processInstanceId='"+d.processInstanceId+"'"+
			" businessId='"+d.businessId+"'"+
			" taskId='"+d.taskId+"'"+
			" taskName='"+d.taskName+"'"+
			" nodeId='"+d.nodeId+"'"+
			" bySignerUserId='"+d.bySignerUserId+"'"+
			" realName='"+d.realName+"'"+
			" organizationName='"+d.organizationName+"'"+
			" startTime='"+d.startTime+"'"+
			" status='"+d.status+"'"+
             "processInstanceName='" +d.processInstanceName+"'"+
             "followCode='" +d.followCode+"'"+
             "processDefinitionIsOuter='" +d.processDefinitionIsOuter+"'"+
			">";
			tr+="<td class='status'>&nbsp;</td>";
			var reasonTitle="";
			if(d.taskType=="addsigner"){
				reasonTitle+='加签：';
			}
			reasonTitle+=concatProcessInsanceName(d.followCode,d.processInstanceName);
			if(d.isFirstNode==true){
				return true;
			}
			if(d.status==2){ //拒绝的任务显示
				reasonTitle+='<span class="ftx-04">[拒绝]</span>';
			}else if(d.status==3){ //驳回的任务是我的申请，不应在此地显示
				return true;
			}
			tr+="<td width='25%' align='left' style='padding:0 0 0 10px'><a href='#' onclick=\"openTaskHandleIndex($(this))\">"+reasonTitle+"</a></td>";
			tr+="<td width='70'>"+d.applyName+"</td>";
			tr+="<td width='140'>"+d.startTime+"</td>";
			$.each(processTask.bussinessColumns,function(i,c){
				if(d.data!=null){
					if(d.data[c]==null){
						tr+="<td>&nbsp;</td>";
					}else{
						tr+="<td>"+d.data[c]+"</td>";
					}
				}else{
					tr+="<td>&nbsp;</td>";
				}
			});
            if(d.processDefinitionIsOuter == "0" && d.taskType=="owner")  {
                tr+="<td width='200'><a  href='#' onclick=\"approve($(this))\"><i class='icon-edit'></i>批准</a>&nbsp;"+
                "<a href='#' onclick=\"reback($(this))\"><i class='icon-edit'></i>拒绝</a>&nbsp;" +
                "<a href='#' onclick=\"reject($(this))\"><i class='icon-edit'></i>驳回</a>" +
                "<a href='#' onclick=\"detail($(this))\"><i class='icon-edit'></i>流程状态</a>" +
                '</td>';
            }else{
                tr+="<td width='200'><a  href='#' onclick=\"approveForUnite($(this))\"><i class='icon-edit'></i>批准</a>&nbsp;"+
                    "<a href='#' onclick=\"rebackForUnite($(this))\"><i class='icon-edit'></i>拒绝</a>&nbsp;" +
                    '</td>';
            }
			tr+="</tr>";
			newDisplay.find("tbody").append(tr);
		});
		if($(".key"+processTask.processDefinitionId).html()==null||$(".key"+processTask.processDefinitionId).html()==""){
			var processtabHtml='<li class="key'+processTask.processDefinitionId+'" data-widget="tab-item1" onclick="show($(this),\''+processTask.processDefinitionId+'\');">'+processTask.processDefinitionName+'<span class="num">('+processTask.totalNum+')</span></li>';
			$(".processtab").append(processtabHtml);
		}else{
			$(".key"+processTask.processDefinitionId).find(".num").html('('+processTask.totalNum+')');
		}
	}else{
		newDisplay.find("tbody").append("暂无数据");
	}
	newDisplay.removeClass("hide");
	parent.autoTabHeight($("#taskContainer").height()+30,'mytask');
}

//节点图回调函数
function detail(obj) {
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    parent.parent.Dialog.openRemote(processInstanceName + '|流程图跟踪', '/app/processApply_detail?instanceId=' + processInstanceId, 1000, 520);
}
/**
 * 弹出任务处理页面
 * @param processName 流程名称
 * @param processInstanceId 流程实例ID
 * @param taskInstanceId 任务实例ID
 */
//function openTaskHandleIndex(businessId,taskType,processName,processInstanceId,taskInstanceId,processDefinitionId,nodeId){
function openTaskHandleIndex(obj){
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    var nodeId=obj.parent().parent().attr("nodeId");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskType=obj.parent().parent().attr("taskType");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionKey=obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processName =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionName");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskName =obj.parent().parent().attr("taskName");
    var startTime= obj.parent().parent().attr("startTime");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var processDefinitionIsOuter = obj.parent().parent().attr("processDefinitionIsOuter");
    var arrObj = [];
    var obj = {};
    obj.nodeId = nodeId;
    obj.processInstanceId = processInstanceId;
    obj.buinessInstanceId = businessId;
    obj.taskType = taskType;
    obj.id = taskId;
    obj.name=encodeURI(encodeURI(taskName));
    startTime=startTime.replace(" ","T");
    obj.addSignerUserId=bySignerUserId;   //加签人
    obj.processDefinitionKey = processDefinitionKey;
    obj.processDefinitionId=processDefinitionId;
    obj.processDefinitionIsOuter=processDefinitionIsOuter;
    obj.processInstanceName= encodeURIComponent(encodeURIComponent(processInstanceName)); //主题名称
    arrObj.push(obj);
//    Dialog.openRemote(processName,springUrl + "/app/taskHandle_index?businessId="+businessId+"&taskType="+taskType+"&processInstanceId="+processInstanceId+"&taskInstanceId="+taskInstanceId+"&processDefinitionId="+processDefinitionId+"&nodeId="+nodeId,"750","450");
   parent.parent.Dialog.openRemote(processName,springUrl + "/app/taskHandle_index?oaTask="+JSON.stringify(arrObj)+"&startTime="+startTime,1000,520);
}
/**
 * 防止重复点击超链接
 * @param flag
 */
function buttonStatus(flag){
    if(flag){
        $("[href='#']").attr("unable", true);
        $("[href='#']").addClass('disable');
    }else{
        $("[href='#']").attr("unable",false);
        $("[href='#']").removeAttr("disabled");
    }
}
/**
 * 批准
 * @param processInstanceId
 * @param taskId
 * @param nodeId
 */
function approve(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    var comment  = parent.parent.$("#approveComment").val();
            jQuery.ajax({
                type:"POST",
                cache:false,
                dataType : 'json',
                data:{
                    id :taskId,
                    processDefinitionId: processDefinitionId,
                    nodeId:nodeId,
                    taskType:taskType,
                    buinessInstanceId:businessId,
                    processInstanceId:processInstanceId,
                    name:taskName,
                    addSignerUserId: bySignerUserId,
                    startTime:   startTime,
                    processDefinitionKey:processDefinitionKey,
                    processInstanceName:processInstanceName,
                    comments:comment,
                    isQuick:"1"
                },
                url:springUrl + "/app/processTask_complete",
                success:function (data) {
                    if(data.Success){
                        reload(processDefinitionId) ;
                        parent.parent.Dialog.alert("提示信息",data.message);
                    }else{
                    	if(data.message=="notNull"){
                    		openTaskHandleIndex(obj);
                    	}else{
                    		parent.parent.Dialog.alert("提示信息",data.message);
                    	}
                    }
                },
                error: function (data) {
                	parent.parent.Dialog.alert("提示信息","审批失败");
                }
            });
}

//驳回作
function reject(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    parent.parent.Dialog.openRemote("驳回意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.parent.$("#approveComment").val();
                if(!parent.parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
            		async:true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        taskType:taskType,
                        buinessInstanceId:businessId,
                        processInstanceId:processInstanceId,
                        name:   taskName,
                        addSignerUserId:  bySignerUserId,
                        startTime:   startTime,
                        processDefinitionKey:processDefinitionKey,
                        processInstanceName:processInstanceName,
                        comments:comment
                    },
                    url:springUrl + "/app/processTask_RejectToFirst",
                    success:function (data) {
                        if(data.Success){
                        	reload(processDefinitionId) ;
                            parent.parent.Dialog.hideModal(modal);
                            parent.parent.Dialog.alert("提示信息",data.message);
                        }else{
                            parent.parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                        parent.parent.Dialog.alert("提示信息","驳回失败!");
                    }
                });
                return false;
            }
        }
    ]);
}
//拒绝操作
function reback(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    parent.parent.Dialog.openRemote("拒绝意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.parent.$("#approveComment").val();
                if(!parent.parent.checkValue()){
                   return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    async:true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        taskType:taskType,
                        buinessInstanceId:businessId,
                        processInstanceId:processInstanceId,
                        name:   taskName,
                        addSignerUserId:  bySignerUserId,
                        startTime:   startTime,
                        processDefinitionKey:processDefinitionKey,
                        processInstanceName:processInstanceName,
                        comments:comment
                    },
                    url:springUrl + "/app/processTask_RejectToPrevious",
                    success:function (data) {
                        if(data.Success){
                        	reload(processDefinitionId) ;
                            parent.parent.Dialog.hideModal(modal);
                            parent.parent.Dialog.alert("提示信息",data.message);
                          } else{
                        	parent.parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                    	parent.parent.Dialog.alert("提示信息","拒绝失败!");
                    }
                });
                return false;
            }
        }
    ]);
}
function setProxy(){
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
        },
        url:springUrl + "/process/processProxy_getProxyCount",
        success:function (data) {
            if(data==0){
                comfProxy(data);
            }else{
                openProxy(data) ;
            }
        },
        error: function (data) {
        	parent.parent.Dialog.alert("提示信息","操作失败");
        }
    });
}
/**
 * 是否添加代理人
 */
function comfProxy(count){
	parent.parent.Dialog.confirm("我的代理人","你还没有添加代理。","添加代理人","取消",function(result){
     if(result){
         openProxy(count);
     }
  });
}
function openProxy(count){
    var buttons = [
        {
            "label": "取消",
            "class": "inactive",
            "callback": function () {
            }
        } ,{
            "label": "确定",
            "class": "active1",
            "callback": function(){
                return true;
            }}
    ];
    parent.parent.Dialog.openRemote('选择人员', springUrl + '/process/processProxy_proxy_index?count='+count,700, 500,buttons);
}
function downloadfile(id){
	 var url = springUrl + "/app/process_downloadfile?id=" + id;
	 window.location.href = encodeURI(url);
}

function concatProcessInsanceName(followCode,processInstanceName){
    if(followCode != null && followCode != ''){
        return '['+followCode+']' + processInstanceName;
    }
    return processInstanceName;
}

/**
 * 批准 for 统一待办任务
 */
function approveForUnite(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    var comment  = parent.parent.$("#approveComment").val();

    jQuery.ajax({
        type:"POST",
        cache:false,
        async : true,
        dataType : 'json',
        data:{
            id :taskId,
            processDefinitionId: processDefinitionId,
            nodeId:nodeId,
            buinessInstanceId: businessId,
            taskType:taskType,
            processDefinitionKey:processDefinitionKey,
            startTime: startTime,
            name: taskName,
            assignerId:bySignerUserId,
            isProxy:'1',
            addSignerUserId: '',
            processInstanceId: processInstanceId,
            processInstanceName: processInstanceName,
            comments:comment,
            resultType:"1"
        },
        url:springUrl + "/app/processTask_complete_unite",
        success:function (data) {
            if(data.Success){
                reload(processDefinitionId) ;
                parent.parent.Dialog.alert("提示信息",data.message);
            }else{
                if(data.message=="notNull"){
                    openTaskHandleIndex(obj);
                }else{
                    parent.parent.Dialog.alert("提示信息",data.message);
                }
            }
        },
        error: function (data) {
            parent.parent.Dialog.alert("提示信息","审批失败");
        }
    });
}

//拒绝操作 for 统一待办
function rebackForUnite(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    parent.parent.Dialog.openRemote("拒绝意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.parent.$("#approveComment").val();
                if(!parent.parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    async : true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        buinessInstanceId: businessId,
                        taskType:taskType,
                        processDefinitionKey:processDefinitionKey,
                        startTime: startTime,
                        name: taskName,
                        assignerId:bySignerUserId,
                        isProxy:'1',
                        addSignerUserId: '',
                        processInstanceId: processInstanceId,
                        processInstanceName: processInstanceName,
                        comments:comment,
                        resultType:"2"
                    },
                    url:springUrl + "/app/processTask_complete_unite",
                    success:function (data) {
                        if(data.Success){
                            reload(processDefinitionId) ;
                            parent.parent.Dialog.hideModal(modal);
                            parent.parent.Dialog.alert("提示信息",data.message);
                        } else{
                            parent.parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                        parent.parent.Dialog.alert("提示信息","审批失败");
                    }
                });
                return false;
            }
        }
    ]);
}