/**
 * 数据字典-数据类别修改、查看页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var dicttypeCode = $('#dicttypeCode').val();
/**
 * 修改文件夹-提交按钮响应函数
 */
function updateDictType(){
	var validateEl=$('#dictTypeForm');
	if(!validate2(validateEl)){
		return false;
	}
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            dicttypeName:$('#dicttypeName').val(),
            dicttypeCode:$('#dicttypeCode').val(),
            parentId:$('#parentName').val(),
            id:$('#id').val(),
            dicttypeDesc:$('#dicttypeDesc').val()
        },
        url:springUrl+"/dict/dictType_updateSave",
        /*beforeSend:function(){//提交前校验
            $("#dicttypeName").trigger("change.validation",{submitting: true});
            if( $("#dicttypeName").jqBootstrapValidation("hasErrors")){
                return false;
            }
        },*/
        success:function (data) {
            $("#box-right").load(springUrl+"/dict/dictType_view?dicttypeCode="+dicttypeCode);
            if($("#parentName").val() == ""){
                refreshNode("null");
            }
            else{
                refreshNode($("#parentName").val());
            }
        },
        error: function (data) {
            if(data.responseText == null){
                Dialog.alert("失败","修改数据类别失败");
            }else{
                Dialog.alert("失败",data.responseText);
            }
        }
    });
}

/**
 * 修改数据类别-取消按钮响应函数
 */
function cancelDictType(){
    $("#box-right").html("");
    refreshNode($("#parentName").val());
}

/**
 * 初始化操作
 */
$(function() {
   /*  $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();*/
    //绑定保存按钮事件
    $('#updateDictType').click(function() {
        updateDictType();
    });
    //绑定取消按钮事件
    $('#cancelDictType').click(function() {
        cancelDictType();
    });
    
    bindValidate($('#dictTypeForm'));
});