var cDT_UseDateStart_old;
var cDT_UseDateEnd_old;
$(document).ready(function () {
    try {
        parent.iFrameHeight();
    } catch (e) {
    }

//$($($($("table")[2]).find('tr'))[2]).text('');
//$($($($("table")[3]).find('tr'))[4]).text('');
    $(".combo-text").live("focus", function () {
        $(".combo-text ").attr('disabled', 'disabled');
    });
    $($($($("table")[4]).find('tr'))[2]).text('');//    去掉原本的子表表头
    if ($("#processInstanceName").val() == "") {//主题默认值
        $("#processInstanceName").val("证照借用申请");
    }
    var currentStartDate = $("#cDT_UseDateStart").datebox("getValue");
//    使用开始时间绑定事件
    if ($("#processInstanceName").attr("readonly") != "readonly") {
        $('#cDT_UseDateStart').datebox({
            onSelect: function (date) {

                var time = $('#cDT_UseDateStart').datebox('getText');
                var z_length = $("#T_JDOA_Identity_Z").datagrid("getRows").length;
                if (z_length > 0) {
                    if (Date.parse(cDT_UseDateStart_old) != Date.parse(time)) {
                        var b = confirm('修改使用结束时间段会导致子表项全部删除，确定修改吗？');
                        if (!b) {
                            currentStartDate = cDT_UseDateStart_old;
                            $('#cDT_UseDateStart').datebox('setValue', currentStartDate);
                            $('#cDT_UseDateStart').datebox('hidePanel');
                        } else {
                            delSubTable();
                            $('#cDT_UseDateStart').datebox('getValue');
                        }
                    } else {
                        $('#cDT_UseDateStart').datebox('getValue');
                    }
                } else {
                    $('#cDT_UseDateStart').datebox('getValue');
                }
            }

        }).datebox('setValue', currentStartDate);
//        $('#cDT_UseDateStart').datebox('setValue',new Date().format("yyyy-MM-dd"));
        //使用结束时间判断
        var currentEndDate = $("#cDT_UseDateEnd").datebox("getValue");
        $('#cDT_UseDateEnd').datebox({
            onSelect: function (date) {
                var time = $('#cDT_UseDateEnd').datebox('getText');
                var z_length = $("#T_JDOA_Identity_Z").datagrid("getRows").length;
                if (z_length > 0) {
                    if (Date.parse(cDT_UseDateEnd_old) != Date.parse(time)) {
                        var b = confirm('修改使用结束时间段会导致子表项全部删除，确定修改吗？');
                        if (!b) {
//                            $('#cDT_UseDateEnd').datebox('setValue', cDT_UseDateEnd_old);
                            currentEndDate = cDT_UseDateEnd_old
                            $('#cDT_UseDateEnd').datebox('hidePanel');
                            $('#cDT_UseDateEnd').datebox('setValue', currentEndDate);
                        } else {
                            delSubTable();
                            $('#cDT_UseDateEnd').datebox('getValue');
                        }
                    } else {
                        $('#cDT_UseDateEnd').datebox('getValue');
                    }
                } else {
                    $('#cDT_UseDateEnd').datebox('getValue');
                }
            }
        }).datebox('setValue', currentEndDate);
    }
});
/**
 * 删除子表内容
 */
function delSubTable() {
    var rows = $("#T_JDOA_Identity_Z").datagrid("getRows");
    if (rows) {
        var l = rows.length;
        for (var i = 0; i < l; i++) {
            var index = $('#T_JDOA_Identity_Z').datagrid('getRowIndex', rows[i]);
            if (rows[i] && (rows[i].ID || rows[i].id)) {
                $.ajax({
                    type: "POST",
                    url: springUrl + "/app/processApp_delete_bindReport_data",
                    async: false,
                    data: "boTableName=T_JDOA_Identity_Z" + "&id=" + (rows[i].ID || rows[i].id),
                    success: function (businessObjectId) {
                        $('#T_JDOA_Identity_Z').datagrid('deleteRow', index);
                    },
                    error: function (msg) {
                        alert('业务数据删除失败!');
                        return false;
                    }
                });
            } else {
                $('#T_JDOA_Identity_Z').datagrid('deleteRow', index);
            }
        }//end for

    }
}

function certificate() {
    $(".certificateRow").remove();
    var cChr_CompanyValue = $("#jd_cn").val();
    var cChr_CompanyName = $("select[id='jd_cn'] option[value =" + cChr_CompanyValue + "]").text();//借用证照公司名称
    var cRadio_RequestClass = $("input[name = 'cRadio_RequestClass']:checked").next().text();//申请类别
    var cDT_UseDateStart = $('#cDT_UseDateStart').datetimebox('getText');//使用开始时间
    var cDT_UseDateEnd = $('#cDT_UseDateEnd').datetimebox('getText');//使用结束时间
    //部分校验
    var today = new Date().format("yyyy-MM-dd");
    ;
    if (cDT_UseDateStart == '' || cDT_UseDateEnd == '') {
        alert("使用开始时间、使用结束时间不能为空");
    } else if (cRadio_RequestClass == '') {
        alert("请选择申请类别");
    }
    else if ((Date.parse(cDT_UseDateStart) < Date.parse(today)) ||
        (Date.parse(cDT_UseDateEnd) < Date.parse(cDT_UseDateStart))) {
        alert("使用开始时间应不早于今天，使用结束时间应不小于使用开始时间");
    } else {//校验通过
        //弹出框上公司及日期信息显示
        $("#certificateInfo").html("<p><strong>借用证照公司名称：</strong>" + cChr_CompanyName + "</p>" +
            "<p><strong>使用开始时间：</strong>" + cDT_UseDateStart + "</p>" +
            "<p><strong>使用结束时间：</strong>" + cDT_UseDateEnd + "</p>");
        if (cRadio_RequestClass == '扫描件') {
            $("[name='isAble']").hide();
            $("[name='usedTime']").hide();
        } else {
            $("[name='isAble']").show();
            $("[name='usedTime']").show();
        }
        //弹出框代码
        var dialog = $('#selectCarInfoTable').dialog({
            title: "证件库",
            width: 900,
            height: 350,
            top: 200,
            closed: false,
            cache: false,
            modal: false,
            buttons: [
                {
                    text: '确定',
                    handler: function () {
                        var tr = $("input[name='certificateId']:checked").parent().parent();
                        var Request_ID = tr.find("td#Request_ID").text();
                        var cChr_CertificateName = tr.find("td#cChr_CertificateName").text();
                        var cChr_CertificateType = tr.find("td#cChr_CertificateType").text();
                        var cRadio_RequestClass = tr.find("td#cRadio_RequestClass").text();
                        var usedTime = tr.find("td#usedTime").text();
                        var isAble = tr.find("td#isAble").text();
                        if (isAble.indexOf("不") > -1) {
                            alert("你选择的证件“申请时间”与“已占用时间”冲突，请修改申请时间！");
                        } else {
                            $('#T_JDOA_Identity_Z').datagrid('appendRow', {
                                cChr_CertificateRequestId: Request_ID,
                                cChr_CompanyName: cChr_CompanyName,
                                cChr_CertificateName: cChr_CertificateName,
                                cChr_CertificateType: cChr_CertificateType,
                                cRadio_RequestClass: cRadio_RequestClass });
                            $('#selectCarInfoTable').dialog('close');
                            cDT_UseDateEnd_old = cDT_UseDateEnd;
                            cDT_UseDateStart_old = cDT_UseDateStart;
                            try {
                                parent.iFrameHeight();
                            } catch (e) {
                            }

                        }

                    }
                },
                {
                    text: '取消',
                    handler: function () {
                        $('#selectCarInfoTable').dialog('close');
                    }
                }
            ]
        });
        $('#selectCarInfoTable').dialog('open');
        var params_add = '{"cChr_CompanyName":"' + cChr_CompanyName + '","cDT_QueryDateTime":"' + new Date().format("yyyy/MM/dd hh:mm:ss") + '"}';
        var data_add = {
            "datasourceId": "486db34e51bb4d97ab16f8dd120644d1",
            "paramsStr": params_add
        };

        jQuery.ajax({
            type: "POST",
            url: "/di/datasource/datasource_getResultList",
            dataType: "json",
            data: data_add,
            success: function (certificateHistory) {
                findCertifcate(certificateHistory, cChr_CompanyName, cRadio_RequestClass, cDT_UseDateStart, cDT_UseDateEnd, cRadio_RequestClass);
            },
            error: function (msg) {
                alert(msg);
            }
        });
    }
}

function findCertifcate(certificateHistory, cChr_CompanyName, cRadio_RequestClass, cDT_UseDateStart, cDT_UseDateEnd, cRadio_RequestClass) {
    //查询证件库
    var params = '{"cChr_CompanyName":"' + cChr_CompanyName + '"}';
    var data = {
        "datasourceId": "ecd79bd928ac4f9490eb7b06940403d1",
        "paramsStr": params
    };
    jQuery.ajax({
        type: "POST",
        url: "/di/datasource/datasource_getResultList",
        dataType: "json",
        data: data,
        success: function (resp) {//证件库与借用记录进行合并
            var table1 = $('#certificateTable');
            $('#certificateTable').parent().show()
            var firstTr = table1.find('tbody>tr:first');
            var rows = $("#T_JDOA_Identity_Z").datagrid("getRows");
            var ids = '';
            for (var i = 0; i < rows.length; i++) {
                ids += rows[i].cChr_CertificateRequestId + ','
            }
            jQuery.each(resp, function (i, o) {
                if (ids.indexOf(o.Request_ID) == -1) {//若没有选择过
                    var usedTime = '';
                    var isAble = "<span style='color:darkgreen'>可借用</span>";
                    var flag = false;//是否被占用
                    if (cRadio_RequestClass != '扫描件') {
                        jQuery.each(certificateHistory, function (i, h) {
                            if (h.cChr_CertificateName == o.cChr_CertificateName && h.cChr_CertificateType == o.cChr_CertificateType) {
                                if ((Date.parse(h.cDT_UseDateStart.split(' ')[0]) > Date.parse(cDT_UseDateEnd)) || (Date.parse(h.cDT_UseDateEnd.split(' ')[0]) < Date.parse(cDT_UseDateStart))) {
                                    usedTime += "<span style='color:darkgreen'>" + h.cDT_UseDateStart.split(' ')[0] + " 到 " + h.cDT_UseDateEnd.split(' ')[0] + "</span><br>";
                                } else {
                                    isAble = "<span style='color:darkred'>不可借用</span>";
                                    flag = true;
                                    usedTime += "<span style='color:darkred'>" + h.cDT_UseDateStart.split(' ')[0] + "到" + h.cDT_UseDateEnd.split(' ')[0] + "</span><br>";
                                }
                            }
                        });
                    }
                    if (flag == true) {
                        var row = $("<tr class ='certificateRow error '></tr>");
                    } else {
                        var row = $("<tr class ='certificateRow success'></tr>");
                    }
                    if (cRadio_RequestClass != '扫描件') {
                        var td = $("<td><input name='certificateId' id ='" + o.Request_ID + "' value='' type='radio' /></td>" +
                            "<td id='Request_ID' name='Request_ID' hidden='hidden'>" + o.Request_ID + "</td>" +
                            "<td id='cChr_CertificateName' name='cChr_CertificateName'>" + o.cChr_CertificateName + "</td>" +
                            "<td  id='cChr_CertificateType' name='cChr_CertificateType'>" + o.cChr_CertificateType + "</td>" +
                            "<td  id='cRadio_RequestClass' name ='cRadio_RequestClass'>" + cRadio_RequestClass + "</td>" +
                            "<td id='usedTime' name='usedTime'>" + usedTime + "</td>" +
                            "<td id='isAble' name='isAble'>" + isAble + "</td>");
                    } else {
                        var td = $("<td><input name='certificateId'  id ='" + o.Request_ID + "' value='' type='radio' /></td>" +
                            "<td id='Request_ID' name='Request_ID' hidden='hidden'>" + o.Request_ID + "</td>" +
                            "<td id='cChr_CertificateName' name='cChr_CertificateName'>" + o.cChr_CertificateName + "</td>" +
                            "<td  id='cChr_CertificateType' name='cChr_CertificateType'>" + o.cChr_CertificateType + "</td>" +
                            "<td  id='cRadio_RequestClass' name ='cRadio_RequestClass'>" + cRadio_RequestClass + "</td>");
                    }
                    row.append(td);
                    table1.append(row);
                }
            });
        },
        error: function (msg) {
            alert(msg);
        }
    });

}
function checkValueExtendCallback() {
    var rows = $("#T_JDOA_Identity_Z").datagrid("getRows");
    if (rows.length == 0) {
        alert("子表内容不能为空！请先添加证件记录");
        return false;
    }
    return true;
}