var t_processInstanceName;
var sub_T_JDOA_TRIPPER_INFO;
var sub_T_JDOA_TRAVEL_INFO;
var sub_T_JDOA_PLANE_TICKET_INFO;
var t_cDpl_Inland = 0;//申请状态原值
var t_isWhole = false;//默认是否包干为否
//预算信息
var old_TollMoney;
var old_StayMoney;
var old_CityTrafficMoney;
var old_TvlAllowanceMoney;
var old_CityTrafficMoney2;
var old_OtherMoney;
jQuery(function () {
//    打印按钮只有在归档状态才出现
   if($("#processInstanceStatus").val()!=5){
       $("#print").hide();
   }

    $(".ke-zeroborder").nextAll().children().children().children().children().click(function () {
        if (this.id != "showDeptBudgetDiv") {//排除查看预算的按钮 此按钮只需要验证行程填写即可
            try {
                getPlaneVariables();
            } catch (e) {
            }
            if (validateTripper()) {
                getTripperVariables();
                if (validateTrip()) {
                    getTravelVariables();
                }
            }
        }
    })
    $("#print").click(function () {//打印按钮
        window.open("http://rpt.itsm.jd.com/birt/frameset?__report=reports/JDTvl/TravalPrint2Approval.rptdesign&TRID=" + $("#businessObjectId").val());
    })
//    切换申请类型时，清空行程列表。提示语：“修改申请类型将清空行程子表全部内容，确定修改吗？”
    $("input[name='cDpl_Inland']").click(function () {
        if ($("input[name='cDpl_Inland']:checked").val() != t_cDpl_Inland) {
            delSubSheetForTypeChange();
        }
    })

    $($($("table")[6]).find("td")[0]).append(" <span style='font-size:12px;' class='text-error'><span class='badge badge-important'>说明</span> 二级部门相同，行程完全相同的出差人，可以统一提交一笔差旅申请。 </span>")
    t_processInstanceName = $("#processInstanceName");
    sub_T_JDOA_TRIPPER_INFO = $("#T_JDOA_TRIPPER_INFO");
    sub_T_JDOA_TRAVEL_INFO = $("#T_JDOA_TRAVEL_INFO");
    sub_T_JDOA_PLANE_TICKET_INFO = $("#T_JDOA_PLANE_TICKET_INFO");
    if (t_processInstanceName.val() == "") {
        t_processInstanceName.val('差旅申请');
    }
    sub_T_JDOA_TRIPPER_INFO.datagrid({
        onBeforeEdit: function (rowIndex, rowData) {
            var erp = $("#cChr_RequestERP").val();
            setTimeout(function () {
                var rows = sub_T_JDOA_TRIPPER_INFO.datagrid("getRows"); //这段代码是获取当前页的所有行。
                console.log(rows)
                if (rows == null || rows.length <= 1) {
                    getUserByErp(erp);
                }
            }, 1000);
        }
    }); //end of datagrid
    sub_T_JDOA_TRAVEL_INFO.datagrid({
        onBeforeEdit: function (rowIndex, rowData) {
            setTimeout(function () {
                var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows"); //这段代码是获取当前页的所有行。
                if ($("#processInstanceName").attr("readonly") == "readonly" && (rows[rowIndex].cDpl_ChangeFinal == '0' || rows[rowIndex].cDpl_ChangeFinal == '2')) {
                    $("td [field = 'cChr_S_From']").find("input").attr("disabled", "disabled");
                    $("td [field = 'cChr_S_From']").find("span .combo-arrow").hide();

                    $("td [field = 'cChr_S_Destination']").find("input").attr("disabled", "disabled");
                    $("td [field = 'cChr_S_Destination']").find("span .combo-arrow").hide();

                    $("td [field = 'cDpl_FlightDone']").find("input").attr("disabled", "disabled");
                    $("td [field = 'cDpl_FlightDone']").find("span .combo-arrow").hide();

                    $("td [field = 'cChr_S_DepartureTime']").find("input").attr("disabled", "disabled");
                    $("td [field = 'cChr_S_DepartureTime']").find("span .combo-arrow").hide();

                    $("td [field = 'cDT_S_StartDate']").find("input").attr("disabled", "disabled");
                    $("td [field = 'cDT_S_StartDate']").find("span .combo-arrow").hide();

                }
                $("td [field = 'cDpl_ChangeFinal']").find("input").attr("disabled", "disabled");//行程状态只读
                $("td [field = 'cDpl_ChangeFinal']").find("span .combo-arrow").hide();//行程状态只读
                $("td [field = 'cInt_S_Distance']").find("input").attr("disabled", "disabled");//里程只读
            }, 1000);
        }
    }); //end of datagrid
    $("#cDel_B_TollMoney").blur(moneyFieldBlur);
    $("#cDel_B_StayMoney").blur(moneyFieldBlur);
    $("#cDel_B_CityTrafficMoney").blur(moneyFieldBlur);
    $("#cDel_B_TvlAllowanceMoney").blur(moneyFieldBlur);
    $("#cDel_B_CityTrafficMoney2").blur(moneyFieldBlur);
    $("#cDel_B_OtherMoney").blur(moneyFieldBlur);

    $("#showDeptBudget").click(showDeptBudgetOnclick);

    var cDpl_B_Whole = $("input[name='cDpl_B_Whole']:checked").val();//1 否  0 是
    if (cDpl_B_Whole == '0') {//包干
        changeFee(true);
    }
    var cChr_IsUpdateBudget = $("input[name='cChr_IsUpdateBudget']:checked").val();//是否变更预算  1：是 0：否
    if (cChr_IsUpdateBudget == "0") {//不变更预算  将预算信息保存
        old_TollMoney = $("#cDel_B_TollMoney").val();
        old_StayMoney = $("#cDel_B_StayMoney").val();
        old_CityTrafficMoney = $("#cDel_B_CityTrafficMoney").val();
        old_TvlAllowanceMoney = $("#cDel_B_TvlAllowanceMoney").val();
        old_CityTrafficMoney2 = $("#cDel_B_CityTrafficMoney2").val();
        old_OtherMoney = $("#cDel_B_OtherMoney").val();
        $("#cDel_B_TollMoney").attr("disabled", true);
        changeFee(true);
    }
    $("input[name='cChr_IsUpdateBudget']").click(function () {
        var isUpdateBudget = $("input[name='cChr_IsUpdateBudget']:checked").val();//是否变更预算  1：是 0：否
        if (isUpdateBudget == "0") {    //否  则应还原为表单打开时预算
            $("#cDel_B_TollMoney").val(old_TollMoney);
            $("#cDel_B_StayMoney").val(old_StayMoney);
            $("#cDel_B_CityTrafficMoney").val(old_CityTrafficMoney);
            $("#cDel_B_TvlAllowanceMoney").val(old_TvlAllowanceMoney);
            $("#cDel_B_CityTrafficMoney2").val(old_CityTrafficMoney2);
            $("#cDel_B_OtherMoney").val(old_OtherMoney);
            $("#cDel_B_TollMoney").attr("disabled", true);
            changeFee(true);
        } else if (isUpdateBudget == "1") {
            $("#cDel_B_TollMoney").attr("disabled", false);
            checkBWhole();
        }
    })
});

//根据erp获取人员信息
function getUserByErp(erp) {
    var arr = '[{"REAL_NAME":""},{"USER_NAME":"' + erp + '"}]';
    var d = {'condition': arr, 'page': 1, 'rows': 10};
    $.ajax({
        type: 'POST',
        async: true,
        url: '/di/datasource/mapping_getBusinessData?dataSourceId=5c68ddd55c1d45e38669e440b135d8e9',
        data: d,
        success: function (msg) {
            console.log(msg);
            if (msg != null) {
                try {
                    var cChr_P_ERP = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_ERP'});
                    $(cChr_P_ERP.target).searchbox('setValue', erp)
                    var p_name = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_Name'});
                    $(p_name.target).parent().find('input').val(msg.rows[0].REAL_NAME);
                    var p_erp = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_ERP'});
                    $(p_erp.target).parent().find('input').val(msg.rows[0].USER_NAME);
                    var p_male = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cDpl_Male'});
                    $(p_male.target).parent().find('input').val(msg.rows[0].SEX);
                    var p_email = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_Email'});
                    $(p_email.target).parent().find('input').val(msg.rows[0].EMAIL);
                    var p_mobile = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_S_Mobile'});
                    $(p_mobile.target).parent().find('input').val(msg.rows[0].MOBILE);
                    var p_company = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_Company'});
                    $(p_company.target).parent().find('input').val(msg.rows[0].ORGANIZATION_COMPANYNAME);
                    var p_org = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_Org'});
                    $(p_org.target).parent().find('input').val(msg.rows[0].ORGANIZATION_1_NAME);
                    var p_dept = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_Dept'});
                    $(p_dept.target).parent().find('input').val(msg.rows[0].ORGANIZATION_2_NAME);
                    var p_rank = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_Rank'});
                    $(p_rank.target).parent().find('input').val(msg.rows[0].LEVEL_CODE);
                    var p_deptCode = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_DeptCode'});
                    $(p_deptCode.target).parent().find('input').val(msg.rows[0].ORGANIZATION_2_CODE);
                    var p_orgCode = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_OrgCode'});
                    $(p_orgCode.target).parent().find('input').val(msg.rows[0].ORGANIZATION_1_CODE);
                    var p_companyCode = sub_T_JDOA_TRIPPER_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_P_CompanyCode'});
                    $(p_companyCode.target).parent().find('input').val(msg.rows[0].ORGANIZATION_COMPANYCODE);
                } catch (e) {
                }
            }
        }
    })
}
//检查是否包干
function checkBWhole() {
    var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    //如果里程子表有两条记录，则进行校验，并进行预算
    if (rows && rows.length >= 2) {
        var rowBegin = rows[0].cDT_S_StartDate;
        var rowEnd = rows[rows.length - 1].cDT_S_StartDate;
        console.log(rowBegin);
        console.log(rowEnd);
        var d1 = new Date(rowBegin);
        var d2 = new Date(rowEnd);
        var days = (d2 - d1) / (24 * 60 * 60 * 1000) + 1
        if (days <= 15 || rows.length > 4) {
            console.log('非包干');
            $("input[name = 'cDpl_B_Whole'][value ='1']").attr('checked', true)
            if (t_isWhole) {//如果之前为包干，转为非包干，需要清空
                initFee()
            }
            t_isWhole = false;
            $("#cDpl_B_Whole").val(1);
            if ($("input[name='cChr_IsUpdateBudget']:checked").val() != '0') {
                changeFee(false);
            }
            caculate();
            return "1";
        } else {
            console.log('包干');
            $("input[name = 'cDpl_B_Whole'][value ='0']").attr('checked', true)
            if (!t_isWhole) {//如果之前为非包干，转为包干，需要清空
                initFee()
            }
            t_isWhole = true;
            changeFee(true);
            caculate();
            return "0";
        }
    } else {
        alert("请先填写【行程信息】子表,且至少两条记录！");
        $("#cDpl_B_Whole").val('');
        return "-1";
    }
}
function initFee() {
    $("#cDel_B_StayMoney").val("");
    $("#cDel_B_CityTrafficMoney").val("");
    $("#cDel_B_TvlAllowanceMoney").val("");
    $("#cDel_B_CityTrafficMoney2").val("");
    $("#cDel_B_OtherMoney").val("");
}
/**
 * 各种费用 --是否禁用
 * @param isDisabled
 */
function changeFee(isDisabled) {
    $("#cDel_B_StayMoney").attr("disabled", isDisabled);
    $("#cDel_B_CityTrafficMoney").attr("disabled", isDisabled);
    $("#cDel_B_TvlAllowanceMoney").attr("disabled", isDisabled);
    $("#cDel_B_CityTrafficMoney2").attr("disabled", isDisabled);
    $("#cDel_B_OtherMoney").attr("disabled", isDisabled);
}
//当确定是否为包干后，才能调用次函数
function caculate() {
    var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    var rowBegin = rows[0].cDT_S_StartDate;
    var rowEnd = rows[rows.length - 1].cDT_S_StartDate;
    var d1 = new Date(rowBegin);
    var d2 = new Date(rowEnd);
    var days = (d2 - d1) / (24 * 60 * 60 * 1000) + 1;
    if (days <= 15 || rows.length > 4) {
        $("#cDel_B_TotalMoney").val(getTotal($("#cDel_B_TollMoney").val(), $("#cDel_B_StayMoney").val(), $("#cDel_B_CityTrafficMoney").val(), $("#cDel_B_CityTrafficMoney2").val(), $("#cDel_B_TvlAllowanceMoney").val(), $("#cDel_B_OtherMoney").val()));
    } else {
        $("#cDel_B_TotalMoney").val(getTotal($("#cDel_B_TollMoney").val(), days * 180));
    }
}

function moneyFieldBlur(event) {
    var check = checkBWhole();
    if (check == "0" || check == "1") {
        caculate();
    } else {
        $(event.target || event.srcElement).val("0.00");
    }
    event.preventDefault();
}
function destinationFieldBlur(event) {
    console.log('destination field blur..');
    var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    if (rows && rows.length >= 2) {
        var from = $.trim(rows[0].cChr_S_From);
        var t_S_Destination = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_S_Destination'});
        var to = $(t_S_Destination.target).val();
        if (from != "" && to != "" && from != to) {
            alert('子表【行程信息】第一条记录的【出发地】和最后一条记录【目的地】不符！');
            $(t_S_Destination.target).val('');
        }
    }
}
//返回两位小数
function getTotal() {
    var total = 0;
    for (var i = 0; i < arguments.length; i++) {
        total += Math.floor(( parseFloat(arguments[i]) ? arguments[i] : 0 ) * 100);
    }
    return total / 100;
}
//获取里程
function getDistance() {
    var params = '{' +
        ' "pcompanycode":"' + RequestCompanyCode + '", ' +
        ' "pdeptcode":"' + UseDeptCode + '", ' +
        ' "psrcsystem":"' + psrcsystem + '", ' +
        ' "pinvokesystem":"' + pinvokesystem + '"' +
        '}';
    var data = {
        "datasourceId": "0a61de3a32e945149b1646079a03851a",
        "paramsStr": "{\"InputParameters\":" + params + "}"
    };
    jQuery.ajax({
        type: "POST",
        url: "/di/datasource/datasource_getResultList",
        dataType: "json",
        data: data,
        success: function (resp) {
            console.log(resp);
        },
        error: function (msg) {
            alert(msg.responseText);
        }
    });
}
/**
 * 行程信息子表-查询路线
 * @param rec
 */
function fromToSelected(rec) {
    console.log(rec);
    var t_S_From = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_S_From'});
    var from = $(t_S_From.target).parent().find("input.combo-value").val();
    var t_S_Destination = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_S_Destination'});
    var to = $(t_S_Destination.target).parent().find("input.combo-value").val();
    var t_S_Distance = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cInt_S_Distance'});
    var params = '{' +
        ' "cChr_S_From":"' + from + '", ' +
        ' "cChr_S_Destination":"' + to + '"' +
        '}';
    var data = {
        "datasourceId": "47a1847913a04180bb3d52fdc4ebf04f",
        "paramsStr": params
    };
    $(t_S_Distance.target).parent().find('input').val('');
    jQuery.ajax({
        type: "POST",
        url: "/di/datasource/datasource_getResultList",
        data: data,
        success: function (resp) {
            if (resp != null && resp[0]) {
                $(t_S_Distance.target).parent().find('input').val(resp[0].cInt_Distance);
            } else {
                $(t_S_Distance.target).parent().find('input').val('');
                alert('暂无该路线.');
                $(t_S_Destination.target).combobox("setValue", '');
            }
        },
        error: function (msg) {
            alert(msg.responseText);
            $(t_S_Distance.target).parent().find('input').val('');
        }
    });
}
//提交表单之前调用
function checkValueExtendCallback(flag) {

    if (validateTripper() && validateTripInfo()) {
        getTripperVariables();
        getTravelVariables();
        try {
            getPlaneVariables();
        } catch (e) {
        }
        if (flag == 1) {
            submitFormInfo(flag);
        }
        else if (flag == 3&& $("button[title = '新增新行程']").length>0) { //审批界面 且还有 新增新行程 按钮 ，即为【申请人确认差旅单】
            var cDel_ValidTripCount = $("#cDel_ValidTripCount").val();
            var cDel_NewTripCount = $("#cDel_NewTripCount").val();
            var cDel_CancelTripCount = $("#cDel_CancelTripCount").val();
            var cDel_AirCancelCount = $("#cDel_AirCancelCount").val();
            var cChr_IsUpdateBudget = $("input[name='cChr_IsUpdateBudget']:checked").val();//是否变更预算  1：是 0：否
            if(cChr_IsUpdateBudget=='1'){
            var cChr_ChangeReason = $("#cChr_ChangeReason").val();
                if(cChr_ChangeReason==null||cChr_ChangeReason==""){
                      alert("变更预算时变更原因不能为空！");
                    return false;
                }
            }
            if (cDel_ValidTripCount == "0" || ( cDel_NewTripCount == '0' && cDel_CancelTripCount == '0' && cDel_AirCancelCount == '0')) {
                var b = confirm('确认行程后无法变更出差申请，请确认是否继续？');
                if (!b) {
                    return false;
                }else{
                    if( saveBusinessDataNoCheckValue()){
                        approveInfo("approve");
                    }
                }
            } else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    return false;

}
/**
 *    非特殊申请，需提前2个（含2个）工作日提交申请，CXO例外。填写行程时，进行校验。
 * @returns {boolean}
 */
function validateCXOStartDate() {
    var type = $("input[name=cDpl_Inland]:checked").val();//申请类型
    if (isCXO == "1" || type != 0) {
        return true;
    } else {
        var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
        var rowBegin = getFirstTravelRow().cDT_S_StartDate;
        var d1 = new Date(rowBegin);
        //消除默认的时区 +8h
        d1.setTime(d1.getTime() - 8 * 60 * 60 * 1000);
        var d2 = new Date();
        //需提前2个（含2个）工作日提交申请
        var days = (d1 - d2) / (24 * 60 * 60 * 1000) + 1;
        if (days < 2) {
            alert("非特殊申请，需提前2个（含2个）工作日提交申请!");
            return false;
        }
        return true;
    }

}
var budgetDialogOptions = {
    title: "部门预算参考信息",
    width: 400,
    height: 260,
    top: ( ($(document).height() - 260) / 2),
    closed: false,
    cache: false,
    modal: false,
    buttons: [
        {
            text: '关闭',
            handler: function () {
                $('#budgetDiv').dialog('close');
            }
        }
    ]
};

function showDeptBudgetOnclick() {
    if (validateTripper()) {
        getTripperVariables();
        var tbl = $('#budgetDiv table');
        var par = {};
        par.cChr_CompanyCode = $("#cChr_DeptCode1").val();
        par.cDT_RequestTime = $("#cDT_RequestTime").val();
        if (tbl.attr('inited') == 'false') {
            $.post("/di/datasource/datasource_getResultList",
                {"datasourceId": "5b34b9f5eb7a4711a8d34d5863054173", "paramsStr": JSON.stringify(par)},
                budgetCallback);
            tbl.removeAttr('inited');
        } else {
            $('#budgetDiv').dialog('open');
        }
    }
}
function budgetCallback(data) {
    if (typeof(data) == 'undefined' || data == '') {
        alert('无法获取部门预算信息');
        return;
    }
    var budget = eval("(" + data[0].data + ")");
    var tbl = $("#budgetDiv table");
    if (budget.length == 0) {
        tbl.append("<tr><td colspan=4>暂无信息</td></tr>");
    }
    for (var i = 0; i < budget.length; i++) {
        tbl.append("<tr><td>"
            + budget[i].org1Name + "</td><td>" + budget[i].monthlyBudget + "</td><td>"
            + budget[i].usedBudget + "</td><td>" + budget[i].remainBudget + "</td></tr>");
    }
    $('#budgetDiv').show().dialog(budgetDialogOptions).dialog('open');
}


//edit by wangdongxing
//打开新增机票窗口
function openPlaneTicketInfoHtml() {
    var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    if (rows == null || rows.length == 0) {
        alert("没有行程信息，无法新增机票");
        return false;
    }
    initFlightTicketInfoHtml(rows);
    $('#openPlaneTicketHtml').show();
    var dialog = $('#openPlaneTicketHtml').dialog({
        title: "新增机票信息",
        width: 700,
        height: 260,
        top: ( ($(document).height() - 260) / 2),
        closed: false,
        cache: false,
        modal: false,
        buttons: [
            {
                text: '确定',
                handler: function () {
                    if (addPlaneFight2SubTable()) {
                        $('#openPlaneTicketHtml').dialog('close');
                    }
                }
            },
            {
                text: '取消',
                handler: function () {
                    $('#openPlaneTicketHtml').dialog('close');
                }
            }
        ]
    });
    $('#openPlaneTicketHtml').dialog('open');
}
//向机票子表添加记录
function addPlaneFight2SubTable() {
    var cInt_FlightsCount = $("#addFlightInfo").find("select#tripNo").val();
    var cDt_FlightDate = $("#addFlightInfo").find("input[name='startdate']").val();
    var cChr_S_From = $("#addFlightInfo").find("input[name='startplace']").val();
    var cChr_S_Destination = $("#addFlightInfo").find("input[name='arriveplace']").val();
    var cChr_time = $("#addFlightInfo").find("input[name='flightTime']").val();
    var cChr_FlightNO = $("#addFlightInfo").find("input[name='flightNo']").val();
    var cChr_FlightTerminal = $("#addFlightInfo").find("input[name='flightFloor']").val();
    if (cInt_FlightsCount == '0') {
        alert("请先选择行程！")
        return false;
    }
    {
        var rows = sub_T_JDOA_TRIPPER_INFO.datagrid("getRows");
        $.each(rows, function (i, row) {
            var planeTicketInfoData = {
                cDpl_FlightCheckStatus: '0',
                cInt_FlightsCount: cInt_FlightsCount,
                cChr_FlightPName: row.cChr_P_Name,
                cDt_FlightDate: cDt_FlightDate,
                cChr_time: cChr_time,
                cChr_S_From: cChr_S_From,
                cChr_FlightNO: cChr_FlightNO,
                cChr_S_Destination: cChr_S_Destination,
                cChr_FlightTerminal: cChr_FlightTerminal,
                cDec_Money: null,
                cChr_SubmitterName: null,
                cDT_ProcessDate: null,
                cChr_F_Remark: ""
            };
            sub_T_JDOA_PLANE_TICKET_INFO.datagrid('appendRow', planeTicketInfoData);
            try {
                parent.iFrameHeight();
            } catch (e) {
            }
        });
        return true;
    }

};
//更改机票状态
function changePlaneTicketStatus(i, status) {
    sub_T_JDOA_PLANE_TICKET_INFO.datagrid('updateRow', {index: i, row: {
        cDpl_FlightCheckStatus: status
    }});
}
//保存子表所有数据
function subSheetSaveForPlaneTickets(obj) {
    var rows = sub_T_JDOA_PLANE_TICKET_INFO.datagrid('getRows');
    if (rows.length > 0) {
        $.ajax({
            type: "POST",
            url: springUrl + "/app/processApp_save_bindReport_s",
            async: false,
            data: "boTableName=" + obj + "&businessObjectId=" + $('#businessObjectId').val() + "&datas=" + encodeURIComponent(JSON.stringify(rows)) + '&formId=' + $('#formId').val() + '&processNodeId=' + $('#processNodeId').val(),
            success: function (businessObjectId) {
                $('#' + obj).datagrid('acceptChanges');
                $('#' + obj).datagrid({'url': springUrl + '/app/processApp_get_bindReportDataJson?boTableName=' + obj + '&businessObjectId=' + $('#businessObjectId').val()});
                subSheetRefresh(obj);
                if ($('#businessObjectId').val() == null || $('#businessObjectId').val() == '' || $('#businessObjectId').val() == '0') {

                }
            },
            error: function (msg) {
                alert('业务数据保存失败!');
                return false;
            }
        });
    }
}
function subSheetSaveForTravelInfo(obj) {
    var rows = sub_T_JDOA_TRAVEL_INFO.datagrid('getRows');
    if (rows.length > 0) {
        $.ajax({
            type: "POST",
            url: springUrl + "/app/processApp_save_bindReport_s",
            async: false,
            data: "boTableName=" + obj + "&businessObjectId=" + $('#businessObjectId').val() + "&datas=" + encodeURIComponent(JSON.stringify(rows)) + '&formId=' + $('#formId').val() + '&processNodeId=' + $('#processNodeId').val(),
            success: function (businessObjectId) {
                $('#' + obj).datagrid('acceptChanges');
                $('#' + obj).datagrid({'url': springUrl + '/app/processApp_get_bindReportDataJson?boTableName=' + obj + '&businessObjectId=' + $('#businessObjectId').val()});
                subSheetRefresh(obj);
                if ($('#businessObjectId').val() == null || $('#businessObjectId').val() == '' || $('#businessObjectId').val() == '0') {

                }
            },
            error: function (msg) {
                alert('业务数据保存失败!');
                return false;
            }
        });
    }
}

//行政出票按钮方法
function issuePlaneTickets(all) {
    if (all && all == "all") {
        //全部操作
        var rows = sub_T_JDOA_PLANE_TICKET_INFO.datagrid("getRows");
        $.each(rows, function (i, r) {
            changePlaneTicketStatus(i, '2');
        });
    } else {
        //单条操作
        var row = sub_T_JDOA_PLANE_TICKET_INFO.datagrid('getSelected');
        if (row) {
            var index = sub_T_JDOA_PLANE_TICKET_INFO.datagrid('getRowIndex', row);
            changePlaneTicketStatus(index, '2');
        }
    }
    subSheetSaveForPlaneTickets('T_JDOA_PLANE_TICKET_INFO');
}
//申请人确认机票
function confirmPlaneTicketInfo(all) {
    if (all && all == "all") {
        //全部操作
        var rows = sub_T_JDOA_PLANE_TICKET_INFO.datagrid("getRows");
        $.each(rows, function (i, r) {
            changePlaneTicketStatus(i, '1');
        });
    } else {
        //单条操作
        var row = sub_T_JDOA_PLANE_TICKET_INFO.datagrid('getSelected');
        if (row) {
            var index = sub_T_JDOA_PLANE_TICKET_INFO.datagrid('getRowIndex', row);
            changePlaneTicketStatus(index, '1');
        }
    }
    subSheetSaveForPlaneTickets('T_JDOA_PLANE_TICKET_INFO');
}
//申请人取消机票
function canclePlaneTicketInfo(all) {
    if (all && all == "all") {
        //全部操作
        var rows = sub_T_JDOA_PLANE_TICKET_INFO.datagrid("getRows");
        $.each(rows, function (i, r) {
            changePlaneTicketStatus(i, '3');
        });
    } else {
        //单条操作
        var row = sub_T_JDOA_PLANE_TICKET_INFO.datagrid('getSelected');
        if (row) {
            var index = sub_T_JDOA_PLANE_TICKET_INFO.datagrid('getRowIndex', row);
            changePlaneTicketStatus(index, '3');
        }
    }
    subSheetSaveForPlaneTickets('T_JDOA_PLANE_TICKET_INFO');
}
//初始化新增机票页面
function initFlightTicketInfoHtml(rows) {
    var planeRows = sub_T_JDOA_PLANE_TICKET_INFO.datagrid("getRows");

    //行程序号选择框事件
    $("select#tripNo").change(function () {
        var option = $(this).find("option:selected");
        $("#addFlightInfo").find("input[name='startdate']").val(option.attr("startdate"));
        $("#addFlightInfo").find("input[name='startplace']").val(option.attr("startplace"));
        $("#addFlightInfo").find("input[name='arriveplace']").val(option.attr("arriveplace"));
    });
    //清理页面数据
    $("#addFlightInfo").find("input[name='startdate']").val("");
    $("#addFlightInfo").find("input[name='startplace']").val("");
    $("#addFlightInfo").find("input[name='arriveplace']").val("");
    $("#addFlightInfo").find("input[name='flightTime']").val("");
    $("#addFlightInfo").find("input[name='flightNo']").val("");
    $("#addFlightInfo").find("input[name='flightFloor']").val("");
    $("select#tripNo").html("");
    $("select#tripNo").append("<option value='0'>请选择行程</option>");
    $.each(rows, function (i, row) {
        if (row.cDpl_FlightDone == 0) {
            var planeFlag = false;//是否已经添加该行程机票，默认没有
            $.each(planeRows, function (j, planeRow) {
                if (planeRow.cInt_FlightsCount == row.cInt_FlightsCount) {//遍历机票子表，如果有相同编号，则已有
                    planeFlag = true;
                }
            });
            if (!planeFlag) {//如果没有相同
                var option = "<option " +
                    "startdate='" + row.cDT_S_StartDate + "' " +
                    "startplace='" + row.cChr_S_From + "' " +
                    "arriveplace='" + row.cChr_S_Destination + "' " +
                    " value='" + row.cInt_FlightsCount + "'>" + row.cInt_FlightsCount +
                    "</option>";
                $("select#tripNo").append(option);
            }
        }
    });

}
//选择人员信息子表回调事件，二级部门不同则删除填写行
function extendDictionarySubsheetCallBack(returnData) {
    //判断一级部门，二级部门是否相同
    if (checkTriperOrgIsSame(returnData.ORGANIZATION_1_NAME, returnData.ORGANIZATION_2_NAME, 0) == false) {
        alert("所有出差出差人必须一级部门、二级部门相同！");
        sub_T_JDOA_TRIPPER_INFO.datagrid("deleteRow", sub_T_JDOA_TRIPPER_INFO.datagrid("getRows").length - 1);
        return false;
    }
}
//判断出差出差人的组织结构是否不一致
function checkTriperOrgIsSame(org1Name, org2Name, isSubmit) {
    var tirperRows = sub_T_JDOA_TRIPPER_INFO.datagrid("getRows");
    if (tirperRows == null || tirperRows.length == 0) {
        return true;
    }
    if (tirperRows && tirperRows.length > 0) {
        //判断所有人是否都是同一个部门
        var isAllTriperSameOrg = true;
        if (org1Name == null) {
            org1Name = tirperRows[0].cChr_P_Org;
        }
        if (org2Name == null) {
            org2Name = tirperRows[0].cChr_P_Dept;
        }
        var len = tirperRows.length;
        if (isSubmit == 0) {
            len = tirperRows.length - 1;
        }
        for (var i = 0; i < len; i++) {
            var row = tirperRows[i];
            if (row.cChr_P_Dept != org2Name || row.cChr_P_Org != org1Name) {
                isAllTriperSameOrg = false;
                break;
            }
        }
        return isAllTriperSameOrg;
    }
}

/**
 * 确认信息弹出框信息初始化
 */
function initSubmitFormInfo() {
    //清空信息
    $("#headInfo").find("#InvoiceHead").text("");
    $("#headInfo").find("#TotalMoney").text("");
    $("#tripInfo").find("tbody:eq(0)").find("tr").not(":eq(0)").remove();
    $("#tripInfo").find("tbody:eq(1)").find("tr").not(":eq(0)").remove();
    //发票抬头信息
    var InvoiceHead = $("select#cChr_InvoiceHead").find("option:selected").text();
    $("#headInfo").find("#InvoiceHead").text(InvoiceHead);
    //预算信息
    var TotalMoney = $("#cDel_B_TotalMoney").val();
    $("#headInfo").find("#TotalMoney").text(TotalMoney);
    //出差人信息
    var tirperRows = sub_T_JDOA_TRIPPER_INFO.datagrid("getRows");
    if (tirperRows && tirperRows.length > 0) {
        var tr = $("#tripInfo").find("tbody:eq(0)").find("tr:eq(0)");
        $.each(tirperRows, function (i, row) {
            var newTr = tr.clone();
            newTr.find("#P_Name").text(row.cChr_P_Name);
            var cDpl_S_CredentialsType_value = '';
            if (row.cDpl_S_CredentialsType == '0') {
                cDpl_S_CredentialsType_value = '身份证';
            } else if (row.cDpl_S_CredentialsType == '1') {
                cDpl_S_CredentialsType_value = '护照';
            } else if (row.cDpl_S_CredentialsType == '2') {
                cDpl_S_CredentialsType_value = '军官证';
            }
            newTr.find("#S_CredentialsType").text(cDpl_S_CredentialsType_value);
            newTr.find("#S_CredentialsNo").text(row.cChr_S_CredentialsNo);
            $("#tripInfo").find("tbody:eq(0)").append(newTr);
        });
    }
    //行程信息
    var travelRows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    if (travelRows && travelRows.length > 0) {
        var tr = $("#tripInfo").find("tbody:eq(1)").find("tr:eq(0)");
        $.each(travelRows, function (i, row) {
            var newTr = tr.clone();
            newTr.find("#S_From").text(row.cChr_S_From);
            newTr.find("#S_Destination").text(row.cChr_S_Destination);
            newTr.find("#S_StartDate").text(row.cDT_S_StartDate);
            $("#tripInfo").find("tbody:eq(1)").append(newTr);
        });
    }
}
/**
 * 确认信息弹出框
 */
function submitFormInfo(flag) {
    initSubmitFormInfo();
    $('#submitApplyInfoDiv').show();
    var dialog = $('#submitApplyInfoDiv').dialog({
        title: "请确认差旅信息",
        width: 700,
        height: 360,
        top: 200,
        closed: false,
        cache: false,
        modal: false,
        buttons: [
            {
                text: '确定提交',
                handler: function () {
                    submitNoValidate('1');
                    return true;
                }
            },
            {
                text: '取消',
                handler: function () {
                    $('#submitApplyInfoDiv').dialog('close');
                    return false;
                }
            }
        ]
    });
}
//edit by wangdongxing end
/**
 * 校验差旅人信息
 * @returns {boolean}
 */
function validateTripper() {
//     sub_T_JDOA_TRIPPER_INFO.datagrid('acceptChanges');
    endEdit(sub_T_JDOA_TRIPPER_INFO);
    var tripperRows = sub_T_JDOA_TRIPPER_INFO.datagrid("getRows");
    var validateTripper = true;
    if (tripperRows.length == 0) {//校验是否有行程人信息
        alert("请先填写行程人信息！");
        return false;
    } else {//校验是否有信息未填写
        for (var i = 0; i < tripperRows.length; i++) {
            if (tripperRows[i].cChr_P_Company == ''
                || tripperRows[i].cChr_P_Dept == ''
                || tripperRows[i].cChr_P_ERP == ''
//                || tripperRows[i].cChr_P_Email == ''
                ||tripperRows[i].cChr_P_ERP == ''
                || tripperRows[i].cChr_P_Name == ''
                || tripperRows[i].cChr_P_Org == ''
//                || tripperRows[i].cChr_S_Mobile == ''
                || tripperRows[i].cDpl_Male == ''
                || tripperRows[i].cDpl_S_CredentialsType == '') {
                alert("请先将行程人信息填写完整！");
                return false;
            }
        }
    }
    if (checkTriperOrgIsSame(null, null, 1) == false) {//校验二级部门
        alert("行程人二级部门必须相同！");
        return false;
    }
    return true;
}
/**
 * 校验身份证号不为空
 * @returns {boolean}
 */
function validateCredentialsNo() {
    endEdit(sub_T_JDOA_TRIPPER_INFO);
    var tripperRows = sub_T_JDOA_TRIPPER_INFO.datagrid("getRows");
    var validateTripper = true;
    if (tripperRows.length == 0) {//校验是否有行程人信息
        alert("请先填写行程人信息！");
        return false;
    } else {//校验是否有信息未填写
        for (var i = 0; i < tripperRows.length; i++) {
            if (tripperRows[i].cChr_S_CredentialsNo == '') {//cChr_S_CredentialsNo
                alert("预定机票身份证号不能为空！");
                return false;
            }
        }
    }
    return true;
}
/**
 * 行程信息 新增（扩展）
 * 校验行程人信息 通过即新增
 * yjh
 */
function subSheetAdd_T_JDOA_TRAVEL_INFO() {
    subSheetNormalAdd_T_JDOA_TRAVEL_INFO(0)
}

//从出差人子表获取全局变量 Add By WXJ At 2014-6-28
var isCXO = "0";
function getTripperVariables() {
    var rows = sub_T_JDOA_TRIPPER_INFO.datagrid("getRows");
    $("#cChr_CompanyCode").val(rows[0].cChr_P_CompanyCode);
    $("#cChr_DeptCode1").val(rows[0].cChr_P_OrgCode);
    $("#cChr_DeptCode2").val(rows[0].cChr_P_DeptCode);
    var maxLevelCode = "";
    var levelCode = "";
    var maxLevelUserName = "";
    $.each(rows, function (i, row) {
        levelCode = row.cChr_P_Rank;
        if (levelCode.indexOf("M") >= 0 && levelCode > maxLevelCode) {
            maxLevelCode = levelCode;
            maxLevelUserName = row.cChr_P_ERP;
        }
    });
    if (maxLevelCode == "") maxLevelCode = rows[0].cChr_P_Rank;
    if (maxLevelUserName == "") maxLevelUserName = rows[0].cChr_P_ERP;
    $("#cChr_MaxLevel").val(maxLevelCode);
    $("#cChr_MaxLevelUserName").val(maxLevelUserName);
    if (maxLevelCode > "M6") {
        isCXO = "1";
    } else {
        isCXO = "0";
    }
    console.log(isCXO);
}

//从行程子表获取全局变量 Add By WXJ At 2014-6-28
function getTravelVariables() {
    var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    var cDel_AirTripCount = 0; //预定机票的行程总数
    var cDel_ValidTripCount = 0; //有效的行程总数
    var cDel_NewTripCount = 0;//新行程数量
    var cDel_CancelTripCount = 0;//已取消行程数
    $.each(rows, function (i, row) {
        if (row.cDpl_FlightDone == '0')
            cDel_AirTripCount++;
        if (row.cDpl_ChangeFinal == '0' || row.cDpl_ChangeFinal == '正常' || row.cDpl_ChangeFinal == '3')
            cDel_ValidTripCount++;
        if (row.cDpl_ChangeFinal == '3')
            cDel_NewTripCount++;
        if (row.cDpl_ChangeFinal == '2')
            cDel_CancelTripCount++;
    })
    $("#cDel_AirTripCount").val(cDel_AirTripCount);
    $("#cDel_ValidTripCount").val(cDel_ValidTripCount);
    $("#cDel_NewTripCount").val(cDel_NewTripCount);
    $("#cDel_CancelTripCount").val(cDel_CancelTripCount);
}
//从机票子表获取全局变量 Add By WXJ At 2014-6-28
function getPlaneVariables() {
    var rows = sub_T_JDOA_PLANE_TICKET_INFO.datagrid("getRows");
    var cDel_AirCount = 0; //预定机票总数
    var cDel_AirCancelCount = 0;

    $.each(rows, function (i, row) {
        if (row.cDpl_FlightCheckStatus == '1' || row.cDpl_FlightCheckStatus == '2')
            cDel_AirCount++;
        if (row.cDpl_FlightCheckStatus == '3')
            cDel_AirCancelCount++;
    })
    $("#cDel_AirCount").val(cDel_AirCount);
    $("#cDel_AirCancelCount").val(cDel_AirCancelCount);
}

//申请人取消行程
function cancleTravelInfo() {
    var row = sub_T_JDOA_TRAVEL_INFO.datagrid("getSelected");
    if (row && row.cDpl_ChangeFinal == '0') {
        var index = sub_T_JDOA_TRAVEL_INFO.datagrid('getRowIndex', row);
        sub_T_JDOA_TRAVEL_INFO.datagrid('updateRow', {index: index, row: {
            cDpl_ChangeFinal: '2'
        }});

    } else {
        alert("只能取消正常状态行程！")
    }
    subSheetSaveForTravelInfo('T_JDOA_TRAVEL_INFO');
}

/**
 * 校验行程信息
 * yjh
 */
function validateTrip() {
    endEdit(sub_T_JDOA_TRAVEL_INFO);
    if (validateTripInfo()) {
        checkBWhole();
        getTravelVariables();
    }
}
/**
 * 校验行程信息
 * @returns {boolean}
 */
function validateTripInfo() {
    var tripInfoRows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    var type = $("input[name=cDpl_Inland]:checked").val();//申请类型
    var isNeedCheckFight = type == '0' ? true : false;//如果为国内常规，则需校验是否可以坐飞机
    if (isTripAllCancel()) {
        return  true;
    } else if (tripInfoRows && getSumOfOkTripCount() < 2) {
        alert('请先填写【行程信息】子表,且至少两条记录！');
        return false;
    } else {
        for (var i = 0; i < tripInfoRows.length; i++) {
            if (tripInfoRows[i].cDpl_ChangeFinal != '2' && tripInfoRows[i].cDpl_ChangeFinal != '4') {
                if (tripInfoRows[i].cInt_S_Distance == null || tripInfoRows[i].cInt_S_Distance == '') {
                    alert('第' + tripInfoRows[i].cInt_FlightsCount + '条行程【出发地】【目的地】不能为空！');
                    return false;
                }
                if (tripInfoRows[i].cDT_S_StartDate == null || tripInfoRows[i].cDT_S_StartDate == '') {
                    alert('第' + tripInfoRows[i].cInt_FlightsCount + '条行程【出发日期】不能为空！');
                    return false;
                }
                if (i > 0) { //getNoCancelTrip
                    if (typeof (getNoCancelTrip(tripInfoRows, i + 1).cChr_S_Destination) != "undefined" && (tripInfoRows[i].cChr_S_From != getNoCancelTrip(tripInfoRows, i + 1).cChr_S_Destination)) {
                        alert('第' + tripInfoRows[i].cInt_FlightsCount + '条行程【出发地】与第' + getNoCancelTrip(tripInfoRows, i + 1).cInt_FlightsCount + '条行程【目的地】不同，不符合差旅规定，请修改！');
                        return false;
                    }
                    if (typeof (getNoCancelTrip(tripInfoRows, i + 1).cDT_S_StartDate) != "undefined" && (tripInfoRows[i].cDT_S_StartDate < getNoCancelTrip(tripInfoRows, i + 1).cDT_S_StartDate)) {
                        alert('第' + tripInfoRows[i].cInt_FlightsCount + '条行程【出发日期】比第' + getNoCancelTrip(tripInfoRows, i + 1).cInt_FlightsCount + '条行程【出发日期】早，不符合差旅规定，请修改！');
                        return false;
                    }
                }
                if (isNeedCheckFight && tripInfoRows[i].cDpl_FlightDone == '0' && tripInfoRows[i].cInt_S_Distance < 1500) {
                    alert('第' + tripInfoRows[i].cInt_FlightsCount + '条行程为国内出差，大于1500公里可坐飞机！请修改【是否预订机票】');
                    return false;
                } else if (tripInfoRows[i].cDpl_FlightDone == '0') { //cDT_earliestFlightDepartureTime
                    if (tripInfoRows[i].cDT_earliestFlightDepartureTime == "" || tripInfoRows[i].cDT_latetFlightDepartureTime == "") {
                        alert('第' + tripInfoRows[i].cInt_FlightsCount + '条行程预定机票【航班最早起飞时间 】【航班最晚起飞时间 】不能为空！');
                        return false;
                    }
                    if (tripInfoRows[i].cDT_earliestFlightDepartureTime > tripInfoRows[i].cDT_latetFlightDepartureTime) {
                        alert('第' + tripInfoRows[i].cInt_FlightsCount + '条行程预定机票【航班最早起飞时间 】不能晚于【航班最晚起飞时间 】！')
                        return false;
                    }
                    if (!validateCredentialsNo()) {
                        return  false;
                    }
                }
            }
        }
        if (tripInfoRows[tripInfoRows.length - 1].cChr_S_Destination != getFirstTravelRow().cChr_S_From) {
            alert('子表【行程信息】第' + getFirstTravelRow().cInt_FlightsCount + '条记录的【出发地】和' + tripInfoRows[tripInfoRows.length - 1].cInt_FlightsCount + '条记录【目的地】不符！');
            return false;
        }
    }
    return   validateCXOStartDate();//非特殊申请，需提前2个（含2个）工作日提交申请，CXO例外
    return true;
}
/**
 * 判断是否全部为 取消 或者 失效状态
 * @returns {boolean}
 */
function isTripAllCancel() {
    var travelRows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    for (var i = 0; i < travelRows.length; i++) {
        if (travelRows[i].cDpl_ChangeFinal != '2' && travelRows[i].cDpl_ChangeFinal != '4') {
            return false;
        }
    }
    return true;
}
/**
 *获取第一条有效行程信息
 * @returns {*}
 */
function getFirstTravelRow() {
    var travelRows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    for (var i = 0; i < travelRows.length; i++) {
        if (travelRows[i].cDpl_ChangeFinal != '2' && travelRows[i].cDpl_ChangeFinal != '4') {
            return travelRows[i];
        }
    }
}
/**
 * 获取 非取消 非失效 行程的个数
 * @returns {number}
 */
function getSumOfOkTripCount() {
    var travelRows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    var okTripCount = 0;
    for (var i = 0; i < travelRows.length; i++) {
        if (travelRows[i].cDpl_ChangeFinal != '2' && travelRows[i].cDpl_ChangeFinal != '4') {
            okTripCount++
        }
    }
    return okTripCount;
}
/**
 * 修改申请类型 清空行程信息
 * @returns {boolean}
 */
function delSubSheetForTypeChange() {
    console.log("delSubSheetForTypeChange");
    var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
    console.log(rows);
    if (null != rows && rows.length > 0) {
        var b = confirm('修改申请类型将清空行程子表全部内容，确定修改吗？');
        if (!b) {
            $("input[name='cDpl_Inland'][value = '" + t_cDpl_Inland + "']").attr('checked', 'true')
            $("input[name='cDpl_Inland'][value = '" + t_cDpl_Inland + "']").click();
            return false;
        }
        t_cDpl_Inland = $("input[name='cDpl_Inland']:checked").val();
        if (rows) {
            var l = rows.length;
            for (var i = 0; i < l; i++) {
                var index = sub_T_JDOA_TRAVEL_INFO.datagrid('getRowIndex', rows[i]);
                if (rows[i] && (rows[i].ID || rows[i].id)) {
                    $.ajax({
                        type: "POST",
                        url: springUrl + "/app/processApp_delete_bindReport_data",
                        async: false,
                        data: "boTableName=T_JDOA_TRAVEL_INFO" + "&id=" + (rows[i].ID || rows[i].id),
                        success: function (businessObjectId) {
                            sub_T_JDOA_TRAVEL_INFO.datagrid('deleteRow', index);
                        },
                        error: function (msg) {
                            alert('业务数据删除失败!');
                            return false;
                        }
                    });
                } else {
                    sub_T_JDOA_TRAVEL_INFO.datagrid('deleteRow', index);
                }
            }//end for
        }
    }
    t_cDpl_Inland = $("input[name='cDpl_Inland']:checked").val();
}
/**
 * 行程子表按钮--新增新行程
 */
function addNewTrip() {
    subSheetNormalAdd_T_JDOA_TRAVEL_INFO(3)
}
/**
 * 新增新行程
 * @param flag 3：新行程  0 ：正常
 */
function subSheetNormalAdd_T_JDOA_TRAVEL_INFO(flag) {
    if (validateTripper()) {//如果校验成功
        getTripperVariables();//获取出差人子表变量
        var rows = sub_T_JDOA_TRAVEL_INFO.datagrid("getRows");
        var cInt_FlightsCount = 1;
        if (rows && rows.length > 0) {
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].cInt_FlightsCount > cInt_FlightsCount)
                    cInt_FlightsCount = rows[i].cInt_FlightsCount;
            }
            cInt_FlightsCount = cInt_FlightsCount + 1;
        }
        sub_T_JDOA_TRAVEL_INFO.datagrid('endEdit', lastIndex);
        sub_T_JDOA_TRAVEL_INFO.datagrid('appendRow', {
            cInt_FlightsCount: cInt_FlightsCount,
            cDpl_ChangeFinal: '',
            cChr_S_From: '',
            cChr_S_Destination: '',
            cInt_S_Distance: '',
            cDT_S_StartDate: '',
            cDpl_FlightDone: '',
            cChr_S_DepartureTime: ''});
        lastIndex = sub_T_JDOA_TRAVEL_INFO.datagrid('getRows').length - 1;
        sub_T_JDOA_TRAVEL_INFO.datagrid('selectRow', lastIndex);
        sub_T_JDOA_TRAVEL_INFO.datagrid('beginEdit', lastIndex);
        var cDpl_ChangeFinal = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cDpl_ChangeFinal'});
        $(cDpl_ChangeFinal.target).combobox("setValue", flag);//状态默认值
        $("td [field = 'cDpl_ChangeFinal']").find("input").attr("disabled", "disabled");//行程状态只读
        $("td [field = 'cDpl_ChangeFinal']").find("span .combo-arrow").hide();//行程状态只读
        $("td [field = 'cInt_S_Distance']").find("input").attr("disabled", "disabled");//里程只读
        try {
            parent.iFrameHeight();
        } catch (e) {
        }
        if (cInt_FlightsCount > 1) {
            validateCXOStartDate();//非特殊申请，需提前2个（含2个）工作日提交申请，CXO例外。填写行程时，进行校验
            var cChr_S_From = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_S_From'});
            var lastedDestination = getNoCancelTrip(rows, rows.length).cChr_S_Destination;//获取非取消状态的最近上条行程目的地
            if (lastedDestination != '' && typeof (lastedDestination) != "undefined") {
                $(cChr_S_From.target).combobox("setValue", lastedDestination);//出发地默认为上一条的 目的地
            }
        }
        var cDpl_FlightDone = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cDpl_FlightDone'});
        $(cDpl_FlightDone.target).combobox("setValue", 1);//是否预订飞机 默认值：否
        var cChr_S_DepartureTime = sub_T_JDOA_TRAVEL_INFO.datagrid('getEditor', {index: lastIndex, field: 'cChr_S_DepartureTime'});
        $(cChr_S_DepartureTime.target).combobox("setValue", 6);//时段默认在 8:00-10：00
    }
}
/**
 * 获取上面最近一条的有效行程信息
 * @param rows
 * @param length
 * @returns {*}
 */
function getNoCancelTrip(rows, length) {
    for (var i = length - 2; i >= 0; i--) {
        if (rows[i].cDpl_ChangeFinal != '2' && rows[i].cDpl_ChangeFinal != '4') {
            return rows[i];
            break;
        }
    }
    return "";
}
/**
 * 行程子表按钮--【删除新行程】
 */
function delNewTrip() {
    var row = sub_T_JDOA_TRAVEL_INFO.datagrid('getSelected');
    if (row && (row.cDpl_ChangeFinal == '3' || typeof (row.cDpl_ChangeFinal) == "undefined" || row.cDpl_ChangeFinal == '')) {
        var index = sub_T_JDOA_TRAVEL_INFO.datagrid('getRowIndex', row);
        $.ajax({
            type: "POST",
            url: springUrl + "/app/processApp_delete_bindReport_data",
            async: false,
            data: "boTableName=T_JDOA_TRAVEL_INFO&id=" + row.ID,
            success: function (businessObjectId) {
                sub_T_JDOA_TRAVEL_INFO.datagrid('deleteRow', index);
                setTimeout(function () {
                    try {
                        updateComputeExpress(obj);//触发列规则计算
                    } catch (e) {

                    }
                }, 1000);
            },
            error: function (msg) {
                alert('业务数据删除失败!');
                return false;
            }
        });
    } else {
        alert("非新行程不可删除！")
    }
    return;
}