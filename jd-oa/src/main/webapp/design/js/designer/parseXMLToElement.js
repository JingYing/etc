function parseProcessDescriptor(data) {
	var descriptor = jq(data);
	var definitions = descriptor.find('definitions');
	var process = descriptor.find('process');
	var startEvent = descriptor.find('startEvent');
	var endEvent = descriptor.find('endEvent');
	var userTasks = descriptor.find('userTask');

	var serviceTask = descriptor.find('serviceTask');
	var scriptTask = descriptor.find('scriptTask');
	var receiveTask = descriptor.find('receiveTask');
	var intermediateCatchEvent = descriptor.find('intermediateCatchEvent');
	
	var exclusiveGateway = descriptor.find('exclusiveGateway');
	var parallelGateway = descriptor.find('parallelGateway');
	var lines = descriptor.find('sequenceFlow');
	var shapes = descriptor.find('bpmndi\\:BPMNShape');
	var edges = descriptor.find('bpmndi\\:BPMNEdge');
	workflow.process.category = definitions.attr('targetNamespace');
	workflow.process.id = process.attr('id');
	workflow.process.name = process.attr('name');
	
	//更新流程id与流程name
	populateProcessProperites();
	
	var documentation = trim(descriptor.find('process > documentation').text());
	if (documentation != null && documentation != "")
		workflow.process.documentation = documentation;
	var extentsion = descriptor.find('process > extensionElements');
	if (extentsion != null) {
		var listeners = extentsion.find('activiti\\:executionListener');
		workflow.process.setListeners(parseListeners(listeners,
				"draw2d.Process.Listener", "draw2d.Process.Listener.Field"));
	}
	jq.each(processDefinitionVariables, function(i, n) {

		var variable = new draw2d.Process.variable();
		variable.name = n.name;
		variable.type = n.type;
		variable.scope = n.scope;
		variable.defaultValue = n.defaultValue;
		variable.remark = n.remark;
		workflow.process.addVariable(variable);
	});
	startEvent.each(function(i) {
		var start = new draw2d.Start(
				"js/designer/icons/type.startevent.none.png");
		start.id = jq(this).attr('id');
		start.eventId = jq(this).attr('id');
		start.eventName = jq(this).attr('name');
		start.initiator=jq(this).attr('activiti:initiator'); 
		
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == start.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addFigure(start, x, y);
				return false;
			}
		});
	});
	endEvent.each(function(i) {
		var end = new draw2d.End("js/designer/icons/type.endevent.none.png");
		end.id = jq(this).attr('id');
		end.eventId = jq(this).attr('id');
		end.eventName = jq(this).attr('name');
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == end.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addFigure(end, x, y);
				return false;
			}
		});
	});

	serviceTask.each(function(i) {
		//邮件
		if(jq(this).attr('activiti:type')=='mail'){
			var task = new draw2d.MailTask();
			task.id = jq(this).attr('id');
			task.taskId=task.id;

			task.taskName = jq(this).attr('name');
			task.setContent(task.taskName);
			task.asynchronous=jq(this).attr('activiti:async');
			
			var fields=jq(this).find('extensionElements').find('activiti\\:field');
			fields.each(function(i,field){
				name=jq(field).attr('name');
				name_value=jq(this).find('activiti\\:string').text();
				switch (name) {
			    case 'to':
			    	task.mail_to=name_value;
			        break;
			    case 'from':
			    	task.mail_from=name_value;
				    break;
				    
			    case 'subject':
			    	task.mail_subject=name_value;
				    break;
			    case 'cc':
			    	task.mail_cc=name_value;
				    break;
			    case 'bcc':
			    	task.mail_bcc=name_value;
				    break;
			    case 'charset':
			    	task.mail_charset=name_value;
				    break;
			    case 'html':
			    	task.mail_htmlText=name_value;
				    break;	
				case 'text':
				    task.mail_NhtmlText=name_value;
					break;
			     }
			});
			
			
		}else{//正常服务
			var task = new draw2d.ServiceTask();
			task.id = jq(this).attr('id');
			task.taskId=task.id;

			task.taskName = jq(this).attr('name');
			task.setContent(task.taskName);
			task.asynchronous=jq(this).attr('activiti:async');
			task.service_class=jq(this).attr('activiti:class');
			 task.service_type="Java_class";
			if(is_Empty(task.service_class)){
				task.service_class=jq(this).attr('activiti:expression');
				task.service_type="Expression";
			}

			task.result_variable=jq(this).attr('activiti:resultVariableName');
			
		}
		
		
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == task.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addFigure(task, x, y);
				return false;
			}
		});
	});

	
	
	scriptTask.each(function(i) {
		var task = new draw2d.ScriptTask();
		task.id = jq(this).attr('id');
		task.taskId=task.id;

		task.taskName = jq(this).attr('name');
		task.setContent(task.taskName);
		task.asynchronous=jq(this).attr('activiti:async');
		task.script_content=jq(this).find('script').text();
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == task.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addFigure(task, x, y);
				return false;
			}
		});
	});
	
	
	receiveTask.each(function(i) {
		var task = new draw2d.ReceiveTask();
		task.id = jq(this).attr('id');
		task.taskId=task.id;

		task.taskName = jq(this).attr('name');
		task.setContent(task.taskName);
		task.asynchronous=jq(this).attr('activiti:async');
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == task.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addFigure(task, x, y);
				return false;
			}
		});
	});
	
	
	intermediateCatchEvent.each(function(i) {
		var task = new draw2d.TimerCatchEvent();
		task.id = jq(this).attr('id');
		task.taskId=task.id;

		task.taskName = jq(this).attr('name');
		task.setContent(task.taskName);
		//task.asynchronous=jq(this).attr('activiti:async');
		var timeDuration=jq(this).find("timerEventDefinition").find('timeDate').text();
		if(!is_Empty(timeDuration)){
			task.time_date=timeDuration;
		}
		var timeDuration=jq(this).find("timerEventDefinition").find('timeDuration').text();
         if(!is_Empty(timeDuration)){
        	 task.time_duration=timeDuration;
		}
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == task.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addFigure(task, x, y);
				return false;
			}
		});
	});
	userTasks.each(function(i) {
				var task = new draw2d.UserTask();
				var tid = jq(this).attr('id');
				task.id = tid;
				var tname = jq(this).attr('name');
				var assignee = jq(this).attr('activiti:assignee');
				var candidataUsers = jq(this).attr('activiti:candidateUsers');
				var candidataGroups = jq(this).attr('activiti:candidateGroups');
				var formKey = jq(this).attr('activiti:formKey');
				var async = jq(this).attr('activiti:async');
				
				if(!is_Empty(async)){
                    task.async=async;
				}
				if (!is_Empty(assignee)) {
					task.assignee = assignee;

				}
				if (!is_Empty(candidataUsers)) {

					task.candidateUsers = candidataUsers;

				}
				if (!is_Empty(candidataGroups)) {

					task.candidateGroups = candidataGroups;

				}
				if (!is_Empty(formKey)) {
					task.formKey = formKey;
				}
				var documentation = trim(jq(this).find('documentation').text());
				if (!is_Empty(documentation))
					task.documentation = documentation;
				task.taskId = tid;
				task.taskName = tname;
					task.setContent(tname);
				var listeners = jq(this).find('extensionElements').find(
						'activiti\\:taskListener');
				task.setListeners(parseListeners(listeners,task,
						"draw2d.Task.Listener", "draw2d.Task.Listener.Field"));
				var performersExpression = jq(this).find('potentialOwner')
						.find('resourceAssignmentExpression').find(
								'formalExpression').text();
				if (performersExpression.indexOf('user(') != -1) {
					task.performerType = "candidateUsers";
				} else if (performersExpression.indexOf('group(') != -1) {
					task.performerType = "candidateGroups";
				}

				// 处理会签
				var multiInstance = jq(this).find(
						'multiInstanceLoopCharacteristics');
				task.collection = multiInstance.attr('activiti:collection');
				task.loop_cardinality = jq(this).find('loopCardinality').text();
				task.element_variable = multiInstance
						.attr('activiti:elementVariable');
				task.completion_condition = jq(this)
						.find('completionCondition').text();
				task.multiInstance_sequential = multiInstance
						.attr('isSequential');
				var performers = performersExpression.split(',');
				jq.each(performers, function(i, n) {
					var start = 0;
					var end = n.lastIndexOf(')');
					if (n.indexOf('user(') != -1) {
						start = 'user('.length;
						var performer = n.substring(start, end);
						task.addCandidateUser({
							sso : performer
						});
					} else if (n.indexOf('group(') != -1) {
						start = 'group('.length;
						var performer = n.substring(start, end);
						task.addCandidateGroup(performer);
					}
				});
				shapes.each(function(i) {
					var id = jq(this).attr('bpmnElement');
					if (id == task.id) {
						var x = parseInt(jq(this).find('omgdc\\:Bounds').attr(
								'x'));
						var y = parseInt(jq(this).find('omgdc\\:Bounds').attr(
								'y'));
						workflow.addModel(task, x, y);
						return false;
					}
				});
			});
	exclusiveGateway.each(function(i) {

		var gateway = new draw2d.ExclusiveGateway(
				"js/designer/icons/type.gateway.exclusive.png");
		var gtwid = jq(this).attr('id');
		var gtwname = jq(this).attr('name');
		gateway.id = gtwid;
		gateway.gatewayId = gtwid;
		gateway.gatewayName = gtwname;
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == gateway.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addModel(gateway, x, y);
				return false;
			}
		});
	});
	parallelGateway.each(function(i) {

		var gateway = new draw2d.ParallelGateway(
				"js/designer/icons/type.gateway.parallel.png");
		var gtwid = jq(this).attr('id');
		var gtwname = jq(this).attr('name');
		gateway.id = gtwid;
		gateway.gatewayId = gtwid;
		gateway.gatewayName = gtwname;
		shapes.each(function(i) {
			var id = jq(this).attr('bpmnElement');
			if (id == gateway.id) {
				var x = parseInt(jq(this).find('omgdc\\:Bounds').attr('x'));
				var y = parseInt(jq(this).find('omgdc\\:Bounds').attr('y'));
				workflow.addModel(gateway, x, y);
				return false;
			}
		});
	});
	lines.each(function(i) {
		var lid = jq(this).attr('id');
		var name = jq(this).attr('name');
		var condition = jq(this).find('conditionExpression');
		condition = condition.text();
		// var condition=jq.parseXML(condition);
		var sourceRef = jq(this).attr('sourceRef');
		var targetRef = jq(this).attr('targetRef');
		var source = workflow.getFigure(sourceRef);
		var target = workflow.getFigure(targetRef);
		edges.each(function(i) {
			var eid = jq(this).attr('bpmnElement');
			if (eid == lid) {
				var startPort = null;
				var endPort = null;
				var points = jq(this).find('omgdi\\:waypoint');
				var startX = jq(points[0]).attr('x');
				var startY = jq(points[0]).attr('y');
				var endX = jq(points[1]).attr('x');
				var endY = jq(points[1]).attr('y');
				var sports = source.getPorts();
				var endMin=9999;
				for ( var i = 0; i < sports.getSize(); i++) {
					var s = sports.get(i);
					var x = s.getAbsoluteX();
					var y = s.getAbsoluteY();
					var gapValue=Math.abs(x - startX) +Math.abs(y - startY);
					if ( gapValue< endMin) {
						startPort = s;
						endMin=gapValue;
					}
				}

				var tports = target.getPorts();
			    endMin=9999;
				for ( var i = 0; i < tports.getSize(); i++) {
					var t = tports.get(i);
					var x = t.getAbsoluteX();
					var y = t.getAbsoluteY();
					var gapValue=Math.abs(x - endX)+Math.abs(y - endY);
					if (gapValue < endMin) {
						endPort = t;
						endMin=gapValue;
					}
				}
				if (startPort != null && endPort != null) {
					var cmd = new draw2d.CommandConnect(workflow, startPort,
							endPort);
					var connection = new draw2d.DecoratedConnection();
					connection.id = lid;
					connection.lineId = lid;
					if (name != undefined) {
						connection.lineName = name;
					} else {
						connection.lineName = " ";
					}

					if (lid != name && name != undefined)
						connection.setLabel(name);
					if (condition != null && condition != "") {
						if (condition.indexOf("[CDATA") != -1) {
							condition = condition.slice(condition
									.lastIndexOf("[") + 1, condition
									.indexOf("]"));
						}
						condition=condition.replace(/(^\s*)|(\s*$)/g, "");
						connection.condition = condition;
						
					}
					cmd.setConnection(connection);
					workflow.getCommandStack().execute(cmd);
				}
				return false;
			}
		});
	});
	if (typeof setHightlight != "undefined") {
		setHightlight();
	}
}

function parseListeners(listeners,task,listenerType, fieldType) {
	var parsedListeners = new draw2d.ArrayList();
	listeners.each(function(i) {
		var clazz = jq(this).attr('class');
		var event = jq(this).attr('event');
		if(clazz=="com.jd.swp.listener.AssignMailNotificationListener"){
	    	var fields = jq(this).find('activiti\\:field');
	    	fields.each(function(i) {
	    		name = jq(this).attr('name');
	    		if(name=="subject"){
	    			task.mail_c_subject=jq(this).find('activiti\\:expression').text();
	    			task.mail_c_subject=task.mail_c_subject.replace(/(^\s*)|(\s*$)/g, "");
	    		}
	    		if(name=="htmlText"){
	    			task.mail_c_content=jq(this).find('activiti\\:expression').text();
	    			task.mail_c_content=task.mail_c_content.replace(/(^\s*)|(\s*$)/g, "");
	    		}
	    	});
	
		}else if(clazz=="com.jd.swp.listener.CompleteMailNotificationListener"){
			var fields = jq(this).find('activiti\\:field');
	    	fields.each(function(i) {
	    		name = jq(this).attr('name');
	    		if(name=="subject"){
	    			task.mail_e_subject=jq(this).find('activiti\\:expression').text();
	    			task.mail_e_subject=task.mail_e_subject.replace(/(^\s*)|(\s*$)/g, "");
	    		}
	    		if(name=="htmlText"){
	    			task.mail_e_content=jq(this).find('activiti\\:expression').text();
	    			task.mail_e_content=task.mail_e_content.replace(/(^\s*)|(\s*$)/g, "");
	    		}
	    	});
		}
		else if(clazz=="com.jd.swp.listener.PhoneMessageNotificationTaskListener"){
			var fields = jq(this).find('activiti\\:field');
		     fields.each(function(i) {		     
		     if(event=="create"){
		    	task.message_c_content=jq(this).find('activiti\\:expression').text(); 
				task.message_c_content=task.message_c_content.replace(/(^\s*)|(\s*$)/g, "");
		     }
		     if(event=="complete"){
		    	task.message_e_content=jq(this).find('activiti\\:expression').text(); 
 				task.message_e_content=task.message_e_content.replace(/(^\s*)|(\s*$)/g, "");
		     }
		     
		     });
		}
		else{
			var listener = eval("new " + listenerType + "()");
			listener.event = jq(this).attr('event');
			var expression = jq(this).attr('expression');
			if (expression != null && expression != "") {
				listener.serviceType = 'expression';
				listener.serviceExpression = expression;
			} else if (clazz != null && clazz != "") {
				listener.serviceType = 'javaClass';
				listener.serviceClass = clazz;
			}
			var fields = jq(this).find('activiti\\:field');
			fields.each(function(i) {
				var field = eval("new " + fieldType + "()");
				field.name = jq(this).attr('name');
				// alert(field.name);
				var string = jq(this).find('activiti\\:string').text();
				var expression = jq(this).find('activiti\\:expression').text();
				expression=expression.replace(/(^\s*)|(\s*$)/g, "");
				// alert("String="+string.text()+"|"+"expression="+expression.text());
				if (string != null && string != "") {
					field.type = 'string';
					field.value = string;
				} else if (expression != null && expression != "") {
					field.type = 'expression';
					field.value = expression;
				}
				listener.setField(field);
			});
			parsedListeners.add(listener);
		}
			 
	});
	
	return parsedListeners;
}



