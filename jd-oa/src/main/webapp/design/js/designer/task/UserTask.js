draw2d.UserTask=function(){
	draw2d.Task.call(this);
	this.setTitle("人工任务");
	this.setIcon();
};
draw2d.UserTask.prototype=new draw2d.Task();
draw2d.UserTask.prototype.type="draw2d.UserTask";
draw2d.UserTask.newInstance=function(userTaskXMLNode){
	var task = new draw2d.UserTask();
	task.id=userTaskXMLNode.attr('id');
	task.taskId=userTaskXMLNode.attr('id');
	task.taskName=userTaskXMLNode.attr('name');
	task.setContent(userTaskXMLNode.attr('name'));
	return task;
};
draw2d.UserTask.prototype.setWorkflow=function(_5019){
	draw2d.Task.prototype.setWorkflow.call(this,_5019);
};
draw2d.UserTask.prototype.getContextMenu=function(){
	var menu = draw2d.Task.prototype.getContextMenu.call(this);
  return menu;
};
draw2d.UserTask.prototype.setIcon = function(){
	var icon=draw2d.Task.prototype.setIcon.call(this);
	icon.className="user-task-icon";
};
draw2d.UserTask.prototype.getStartElementXML=function(){
	var xml="";
	var name = this.taskId;
	var taskName = trim(this.taskName);
	if(taskName != null && taskName != "")
		name = taskName;
	if(!is_Empty(this.taskId)){
		xml=xml+'<userTask id="'+this.taskId+"";
	}else{
		return xml;
	}
     if(!is_Empty(name)){
    	 xml=xml+'" name="'+name+'" ';  
	}
   
    
//    if(!is_Empty(this.async)){
//    	xml=xml+' activiti:async="'+this.async+'" ';
//	}else{
//		xml=xml+' activiti:async="true" ';
//	}
     xml=xml+' activiti:async="false" '; 

	if(!is_Empty(this.assignee)){
          xml=xml+' activiti:assignee="'+this.assignee+'" ';
	}
	if(!is_Empty(this.candidateUsers)){
          xml=xml+' activiti:candidateUsers="'+this.candidateUsers+'" ';
	}
	
	xml=xml+'>\n';
	return xml;
};
draw2d.UserTask.prototype.getEndElementXML=function(){
	var xml = '</userTask>\n';
	return xml;
};

draw2d.UserTask.prototype.getExtensionElementsXML=function(){
	if(this.listeners.getSize()==0)return '';
	xml=xml+this.getListenersXML();
	return xml;
};
draw2d.UserTask.prototype.getListenersXML=function(){
	var xml = '';
	for(var i=0;i<this.listeners.getSize();i++){
		var listener = this.listeners.get(i);
		xml=xml+listener.toXML();
	}
	return xml;
};
draw2d.UserTask.prototype.getPerformersBPMNXML=function(){
	var xml = '';
	if(this.performerType=='candidateUsers'){
		if(this.candidateUsers.getSize()!=0){
			xml=xml+'<potentialOwner>\n';
			xml=xml+'<resourceAssignmentExpression>\n';
			xml=xml+'<formalExpression>\n';
			xml=xml+'<![CDATA[';
			for(var i=0;i<this.candidateUsers.getSize();i++){
				var user = this.candidateUsers.get(i);
				xml=xml+'user('+user.sso+'),';
			}
			xml=xml.substring(0, xml.length-1);
			xml=xml+']]>\n';
			xml=xml+'</formalExpression>\n';
			xml=xml+'</resourceAssignmentExpression>\n';
			xml=xml+'</potentialOwner>\n';
		}
	}else if(this.performerType=='candidateGroups'){
		if(this.candidateGroups.getSize()!=0){
			xml=xml+'<potentialOwner>\n';
			xml=xml+'<resourceAssignmentExpression>\n';
			xml=xml+'<formalExpression>\n';
			xml=xml+'<![CDATA[';
			for(var i=0;i<this.candidateGroups.getSize();i++){
				var group = this.candidateGroups.get(i);
				xml=xml+'group('+group+'),';
			}
			xml=xml.substring(0, xml.length-1);
			xml=xml+']]>\n';
			xml=xml+'</formalExpression>\n';
			xml=xml+'</resourceAssignmentExpression>\n';
			xml=xml+'</potentialOwner>\n';
		}
	}
	return xml;
};
//增加会签功能
draw2d.UserTask.prototype.getMultiInstanceXML=function(){
	if(is_Empty(this.collection)){
	return '';
	}
	var xml = '';
	xml='<multiInstanceLoopCharacteristics isSequential="'+this.multiInstance_sequential+'"  activiti:collection="'+this.collection+'"  activiti:elementVariable="'+this.element_variable+'">';
    xml=xml+'<loopCardinality>';
    if(this.loop_cardinality != null){
        xml=xml+this.loop_cardinality;
    }
    xml=xml+'</loopCardinality>' ;
    xml=xml+'<completionCondition>'+this.completion_condition+'</completionCondition></multiInstanceLoopCharacteristics>';
	return xml;
};

//增加邮件提醒
draw2d.UserTask.prototype.getMailXML=function(){
	var xml="";
	if(is_Empty(this.mail_c_content)&&is_Empty(this.mail_e_content)){
		return "";
	}
	if(!is_Empty(this.mail_c_content)){
		xml=xml+'<activiti:taskListener event="create" class="com.jd.swp.listener.AssignMailNotificationListener">';
		xml=xml+'<activiti:field name="subject"><activiti:expression>'+this.mail_c_subject+'</activiti:expression></activiti:field><activiti:field name="htmlText">';
        xml=xml+'<activiti:expression><![CDATA['+this.mail_c_content+']]></activiti:expression></activiti:field></activiti:taskListener>';
	}
	if(!is_Empty(this.mail_e_content)){
		xml=xml+'<activiti:taskListener event="complete" class="com.jd.swp.listener.CompleteMailNotificationListener">';
		xml=xml+'<activiti:field name="subject"><activiti:expression>'+this.mail_e_subject+'</activiti:expression></activiti:field><activiti:field name="htmlText">';
        xml=xml+'<activiti:expression><![CDATA['+this.mail_e_content+']]></activiti:expression></activiti:field></activiti:taskListener>';
	}
	return xml;
};

//增加短信提醒
draw2d.UserTask.prototype.getMessageXML=function(){
	var xml="";
	if(is_Empty(this.message_c_content)&&is_Empty(this.message_e_content)){
		return "";
	}
	if(!is_Empty(this.message_c_content)){
		xml=xml+'<activiti:taskListener event="create" class="com.jd.swp.listener.PhoneMessageNotificationTaskListener">';
		xml=xml+'<activiti:field name="msgContent"><activiti:expression><![CDATA['+this.message_c_content+']]></activiti:expression></activiti:field>';
        xml=xml+'</activiti:taskListener>';
	}
	if(!is_Empty(this.message_e_content)){
		xml=xml+'<activiti:taskListener event="complete" class="com.jd.swp.listener.PhoneMessageNotificationTaskListener">';
		xml=xml+'<activiti:field name="msgContent"><activiti:expression><![CDATA['+this.message_e_content+']]></activiti:expression></activiti:field>';
        xml=xml+'</activiti:taskListener>';
	}
	return xml;
};

draw2d.UserTask.prototype.getExtensionTagXML=function(flag){
	if(is_Empty(this.message_c_content)&&is_Empty(this.message_e_content)&&is_Empty(this.mail_c_content)&&is_Empty(this.mail_e_content)){
	return '';
	}
	var xml = '';
	if(flag=="start"){
		xml = '<extensionElements>\n';
	}
	if(flag=="end"){
		xml = '</extensionElements>\n';
	}
	return xml;
};

draw2d.UserTask.prototype.toXML=function(){
	var xml=this.getStartElementXML();
//	xml=xml+this.getDocumentationXML();
	xml=xml+this.getPerformersBPMNXML();
	xml=xml+this.getExtensionTagXML("start");
	xml=xml+this.getExtensionElementsXML();
    xml=xml+this.getMultiInstanceXML();
	xml=xml+this.getMailXML();
	xml=xml+this.getMessageXML();
	xml=xml+this.getExtensionTagXML("end");
	xml=xml+this.getEndElementXML();
	return xml;
};
draw2d.UserTask.prototype.toBpmnDI=function(){
	var w=this.getWidth();
	var h=this.getHeight();
	var x=this.getAbsoluteX();
	var y=this.getAbsoluteY();
	var xml='<bpmndi:BPMNShape bpmnElement="'+this.taskId+'" id="BPMNShape_'+this.taskId+'">\n';
	xml=xml+'<omgdc:Bounds height="'+h+'" width="'+w+'" x="'+x+'" y="'+y+'"/>\n';
	xml=xml+'</bpmndi:BPMNShape>\n';
	return xml;
};