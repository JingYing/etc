draw2d.TimerCatchEvent=function(){
	draw2d.Task.call(this);
	this.setTitle("定时器");
	this.setIcon();
};
draw2d.TimerCatchEvent.prototype=new draw2d.Task();
draw2d.TimerCatchEvent.prototype.type="draw2d.TimerCatchEvent";
draw2d.TimerCatchEvent.newInstance=function(TimerCatchEventXMLNode){
	var task = new draw2d.TimerCatchEvent();
	task.id=TimerCatchEventXMLNode.attr('id');
	task.taskId=TimerCatchEventXMLNode.attr('id');
	task.taskName=TimerCatchEventXMLNode.attr('name');
	task.setContent(TimerCatchEventXMLNode.attr('name'));
	return task;
};
draw2d.TimerCatchEvent.prototype.setWorkflow=function(_5019){
	draw2d.Task.prototype.setWorkflow.call(this,_5019);
};
draw2d.TimerCatchEvent.prototype.getContextMenu=function(){
	var menu = draw2d.Task.prototype.getContextMenu.call(this);
  return menu;
};
draw2d.TimerCatchEvent.prototype.setIcon = function(){
	var icon=draw2d.Task.prototype.setIcon.call(this);
	icon.className="timer-boundary-event-icon";
};
draw2d.TimerCatchEvent.prototype.getStartElementXML=function(){
	var xml="";
	var name = this.taskId;
	var taskName = trim(this.taskName);
	if(taskName != null && taskName != "")
		name = taskName;
	if(!is_Empty(this.taskId)){
		xml=xml+'<intermediateCatchEvent id="'+this.taskId+"";
	}else{
		return xml;
	}
     if(!is_Empty(name)){
    	 xml=xml+'" name="'+name+'" ';  
	}
    if(!is_Empty(this.asynchronous)){
    	xml=xml+'activiti:async="'+this.asynchronous+'" ';
    }
	var xml=xml+' >';
	
	
	return xml;
};
draw2d.TimerCatchEvent.prototype.getEndElementXML=function(){
	var xml = '</intermediateCatchEvent>\n';
	return xml;
};



draw2d.TimerCatchEvent.prototype.getMainConfigXML=function(){
	var xml = '';
	if(!is_Empty(this.time_date)){
     xml=xml+' <timerEventDefinition><timeDate>'+this.time_date+'</timeDate></timerEventDefinition> '
     return xml; 
	}
	if(!is_Empty(this.time_duration)){
     xml=xml+'<timerEventDefinition><timeDuration>'+this.time_duration+'</timeDuration></timerEventDefinition> '

	}
	
	return xml;
};


draw2d.TimerCatchEvent.prototype.getServiceStartEndXML=function(){
	return '>\n';
};
draw2d.TimerCatchEvent.prototype.toXML=function(){
	var xml=this.getStartElementXML();
	xml=xml+this.getMainConfigXML();
   

	xml=xml+this.getEndElementXML();
	return xml;
};
draw2d.TimerCatchEvent.prototype.toBpmnDI=function(){
	var w=this.getWidth();
	var h=this.getHeight();
	var x=this.getAbsoluteX();
	var y=this.getAbsoluteY();
	var xml='<bpmndi:BPMNShape bpmnElement="'+this.taskId+'" id="BPMNShape_'+this.taskId+'">\n';
	xml=xml+'<omgdc:Bounds height="'+h+'" width="'+w+'" x="'+x+'" y="'+y+'"/>\n';
	xml=xml+'</bpmndi:BPMNShape>\n';
	return xml;
};