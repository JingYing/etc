draw2d.ReceiveTask=function(){
	draw2d.Task.call(this);
	this.setTitle("接收调用");
	this.setIcon();
};
draw2d.ReceiveTask.prototype=new draw2d.Task();
draw2d.ReceiveTask.prototype.type="draw2d.ReceiveTask";
draw2d.ReceiveTask.newInstance=function(ReceiveTaskXMLNode){
	var task = new draw2d.ReceiveTask();
	task.id=ReceiveTaskXMLNode.attr('id');
	task.taskId=ReceiveTaskXMLNode.attr('id');
	task.taskName=ReceiveTaskXMLNode.attr('name');
	task.setContent(ReceiveTaskXMLNode.attr('name'));
	return task;
};
draw2d.ReceiveTask.prototype.setWorkflow=function(_5019){
	draw2d.Task.prototype.setWorkflow.call(this,_5019);
};
draw2d.ReceiveTask.prototype.getContextMenu=function(){
	var menu = draw2d.Task.prototype.getContextMenu.call(this);
  return menu;
};
draw2d.ReceiveTask.prototype.setIcon = function(){
	var icon=draw2d.Task.prototype.setIcon.call(this);
	icon.className="receive-task-icon";
};
draw2d.ReceiveTask.prototype.getStartElementXML=function(){
	var xml="";
	var name = this.taskId;
	var taskName = trim(this.taskName);
	if(taskName != null && taskName != "")
		name = taskName;
	if(!is_Empty(this.taskId)){
		xml=xml+'<receiveTask id="'+this.taskId+"";
	}else{
		return xml;
	}
     if(!is_Empty(name)){
    	 xml=xml+'" name="'+name+'" ';  
	}
    if(!is_Empty(this.asynchronous)){
    	xml=xml+'activiti:async="'+this.asynchronous+'" ';
    }
	
	xml=xml+'>\n';
	return xml;
};
draw2d.ReceiveTask.prototype.getEndElementXML=function(){
	var xml = '</receiveTask>\n';
	return xml;
};




draw2d.ReceiveTask.prototype.toXML=function(){
	var xml=this.getStartElementXML();
	
	xml=xml+this.getEndElementXML();
	return xml;
};
draw2d.ReceiveTask.prototype.toBpmnDI=function(){
	var w=this.getWidth();
	var h=this.getHeight();
	var x=this.getAbsoluteX();
	var y=this.getAbsoluteY();
	var xml='<bpmndi:BPMNShape bpmnElement="'+this.taskId+'" id="BPMNShape_'+this.taskId+'">\n';
	xml=xml+'<omgdc:Bounds height="'+h+'" width="'+w+'" x="'+x+'" y="'+y+'"/>\n';
	xml=xml+'</bpmndi:BPMNShape>\n';
	return xml;
};