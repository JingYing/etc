function setTaskAsync(id,status){
	task = workflow.getFigure(id);
	task.async=status;
}
 
function getTaskAsync(id){
	task = workflow.getFigure(id);
    return task.async; 
}

//type="create" or  "complete"
function setMessages(id,type,content){
	task = workflow.getFigure(id);
	if(type=="create"){
		task.message_c_content=content;
		task.message_c_event=type;
    }
    if(type=="complete"){
    	task.message_e_content=content;
    	task.message_e_event=type;
    }
    if(type==""){
    	task.message_c_content="";
		task.message_c_event="";
		task.message_e_content="";
    	task.message_e_event="";
	}
}
  
function getMessages(id){
	task = workflow.getFigure(id);
    return task; 
}

function setInitiator(id,users){
	task = workflow.getFigure(id);
	if (task==null)
		alert("请添加[开始]节点。");
	task.initiator=users;
}

function getInitiator(id){
	task = workflow.getFigure(id);
    return task.initiator; 
}

//设置人工节点参与人
function setAssignee(id,users){
task = workflow.getFigure(id);
 task.assignee=users;
}
  
//获取人工节点参与人，需要传一个节点id
function getAssignee(id){
task = workflow.getFigure(id);
    return task.assignee; 
}
//获取会签节点通过比例
function getCounterSignPercent(id){
    task = workflow.getFigure(id);
    var flag = task.multiInstance_sequential;
    if(flag !=null || flag !=''){
        var condition = task.completion_condition;
        return condition.substring(condition.length-4,condition.length-1);
    }
    return 0;
}


//设置人工节点参与人
function setCandidateUsers(id,users){
	task = workflow.getFigure(id);
    var isCounterSignVal = isCounterSignNode();
    if(isCounterSignVal == "1"){
        task.multiInstance_sequential = false;
        task.assignee = '${assignee}';
        task.element_variable = 'assignee';
//        task.collection = 'assigneeList_' + id;
        task.collection = 'assigneeList';
        var percent = $("#completionCondition").val();
        var condition = "${nrOfCompletedInstances / nrOfInstances >= "+percent+"}";
        task.completion_condition= condition;
        task.loop_cardinality = null;
        task.candidateUsers = null;
    }else{
    	task.candidateUsers=users;
    }
}

//type="create" or  "complete"
function setUMail(id,type,subject,content){
	task = workflow.getFigure(id);
	if(type=="create"){
		task.mail_c_subject=subject;
		task.mail_c_content=content;
		task.mail_c_event=type;
	}
	if(type=="complete"){
		task.mail_e_subject=subject;
		task.mail_e_content=content;
		task.mail_e_event=type;
	}
	if(type==""){
		task.mail_c_subject="";
		task.mail_c_content="";
		task.mail_c_event="";	
		task.mail_e_subject="";
		task.mail_e_content="";
		task.mail_e_event="";
	}
}


//获取人工节点参与人，需要传一个节点id
function getCandidateUsers(id){
	task = workflow.getFigure(id);
    return task.candidateUsers;	
}

//节点id
/*
 * create:
 *  task.mail_c_subject
     task.mail_c_content
     task.mail_c_event
     
     
    "complete"
     task.mail_e_subject
     task.mail_e_content
     task.mail_e_event
 */

function getUMail(id){
	task = workflow.getFigure(id);
    return task;	
	
}

//获取流程定义文件
function getProcess(){
	return workflow.toXML();
}


//加载流程
function uploadProcess(data){
	transformation(data);
}

//获取流程id
function getProcessId(){
   return workflow.process.id;
}

/*
 * 使用方法示例（返回的结果为一个task的对象，该对象包含两个属性为id与name）：
 * var result=getAllUserTask();
	for(var i=0;i<result.length;i++){
		alert(result[i].id+" "+result[i].name);
	}
 */
function getAllUserTask(){
    return getUserTasks();
}

//退出或刷新确认保存
function confirmSaveProcess(ctx){	
	Dialog.confirm("保存提示","是否需要保存流程定义?","是","否",function(result){
        if(result){
        	saveProcess(ctx);
        }
    });
}
		
//保存流程定义文件
function saveProcess(ctx){
	//设置Initiator
	setInitiator("start","init");
    $.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            id : $("#processDefinitionId").val(),
            processDefinitionFile : getProcess(),
            allUserTask : JSON.stringify(getAllUserTask())
        },
        url:ctx+"/process/saveProcessDefinitionToJss",
        success:function (data) {
        	if (addProcessDef(ctx)){
        		Dialog.alert("成功","保存成功");
        	}else{
        		Dialog.alert("失败","保存失败");
        	}  
        },
        error: function (data) {
            Dialog.alert("失败","保存失败");
        }
    });
}

//保存流程KEY
function addProcessDef(ctx) {
	var bool = false;
	var pafProcessDefinitionId = getProcessId();
	
	var data = {
			"id" : $("#processDefinitionId").val(),
			"pafProcessDefinitionId" : pafProcessDefinitionId
	};
	Dialog.post(ctx + "/process/processDef_addSave", data, function(
			result) {
		bool = result.operator;		
	});
	return bool;
}
