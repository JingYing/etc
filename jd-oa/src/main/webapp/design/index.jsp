<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>Process Diagram</title> <!-- framework CSS -->
<link href="themes/default/css/style.css" type="text/css"
	rel="stylesheet" title="blue" />



<!-- JQuery EasyUi CSS-->
<link type="text/css" href="js/jquery-easyui/themes/gray/easyui.css" rel="stylesheet" title="blue">
<link href="js/jquery-easyui/themes/icon.css" type="text/css" rel="stylesheet" />
<!-- JQuery validate CSS-->
<link href="js/validate/jquery.validate.extend.css" type="text/css" rel="stylesheet" />
<!-- JQuery AutoComplete -->
<link rel="stylesheet" type="text/css" href="js/jquery-autocomplete/jquery.autocomplete.css" />
<!--<link rel="stylesheet" type="text/css" href="js/jquery-autocomplete/lib/thickbox.css" />-->

<!-- JQuery-->
<script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
<!--<script src="js/jquery-1.6.min.js" type="text/javascript"></script>-->

<!-- JQuery EasyUi JS-->
<script src="js/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>

<!-- 增加的一些工具类-->
<script src="js/util/until.js" type="text/javascript"></script>

<SCRIPT src="js/interface.js"></SCRIPT>

<!-- JQuery validate JS-->
<script src="js/validate/jquery.validate.js" type="text/javascript"></script>
<script src="js/validate/jquery.metadata.js" type="text/javascript"></script>
<script src="js/validate/jquery.validate.method.js"
	type="text/javascript"></script>
<script src="js/validate/jquery.validate.extend.js"
	type="text/javascript"></script>

<!-- JQuery form Plugin -->
<script src="js/jquery.form.js" type="text/javascript"></script>

<!-- JSON JS-->
<script src="js/json2.js" type="text/javascript"></script>

<!-- JQuery AutoComplete -->
<script type='text/javascript'
	src='js/jquery-autocomplete/lib/jquery.bgiframe.min.js'></script>
<script type='text/javascript'
	src='js/jquery-autocomplete/lib/jquery.ajaxQueue.js'></script>
<!--<script type='text/javascript' src='js/jquery-autocomplete/lib/thickbox-compressed.js'></script>-->
<script type='text/javascript'
	src='js/jquery-autocomplete/jquery.autocomplete.min.js'></script>

<!-- framework JS -->
<script src="js/skin.js" type="text/javascript"></script>
<link href="js/designer/designer.css" type="text/css" rel="stylesheet" />

<!-- common, all times required, imports -->
<SCRIPT src='js/draw2d/wz_jsgraphics.js'></SCRIPT>
<SCRIPT src='js/draw2d/mootools.js'></SCRIPT>
<SCRIPT src='js/draw2d/moocanvas.js'></SCRIPT>
<SCRIPT src='js/draw2d/draw2d.js'></SCRIPT>


<!-- example specific imports -->
<SCRIPT src="js/designer/MyCanvas.js"></SCRIPT>
<SCRIPT src="js/designer/ResizeImage.js"></SCRIPT>
<SCRIPT src="js/designer/event/Start.js"></SCRIPT>
<SCRIPT src="js/designer/event/End.js"></SCRIPT>
<SCRIPT src="js/designer/connection/MyInputPort.js"></SCRIPT>
<SCRIPT src="js/designer/connection/MyOutputPort.js"></SCRIPT>
<SCRIPT src="js/designer/connection/DecoratedConnection.js"></SCRIPT>
<SCRIPT src="js/designer/task/Task.js"></SCRIPT>
<SCRIPT src="js/designer/task/UserTask.js"></SCRIPT>
<SCRIPT src="js/designer/task/ManualTask.js"></SCRIPT>
<SCRIPT src="js/designer/task/ServiceTask.js"></SCRIPT>
<SCRIPT src="js/designer/task/ScriptTask.js"></SCRIPT>
<SCRIPT src="js/designer/task/ReceiveTask.js"></SCRIPT>
<SCRIPT src="js/designer/task/MailTask.js"></SCRIPT>
<SCRIPT src="js/designer/task/TimerCatchEvent.js"></SCRIPT>


<SCRIPT src="js/designer/gateway/ExclusiveGateway.js"></SCRIPT>
<SCRIPT src="js/designer/gateway/ParallelGateway.js"></SCRIPT>

<SCRIPT src="js/designer/designer.js"></SCRIPT>


<!-- 将流程定义文件解析为节点 -->
<SCRIPT src="js/designer/parseXMLToElement.js"></SCRIPT>

<SCRIPT src="js/index.js"></SCRIPT>
<!-- Design -->
<link href="<%=request.getContextPath()%>/static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/common/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/bootstrap/js/bootbox.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/common/js/Dialog.js"></script>
    <script>
	    $(document).ready(function(){  
	         window.onbeforeunload = function(){  	       
	        	 return("您的表单已修改，尚未保存！");
	        }
	 	});  
    
        $(function(){
            $.ajax({
                type:"POST",
                cache:false,
                dataType : 'json',
                data:{
                    id : $("#processDefinitionId").val()
                },
                url:"<%=request.getContextPath()%>/process/loadProcessDefinitionFile",
                success:function (data) {
                	if(data!=null&&data.processDefinitionFile!=null&&data.processDefinitionFile!=""){
                    	uploadProcess(data.processDefinitionFile);
                	}
                },
                error: function (data) {
					//Dialog.alert("失败","读取流程图失败");
                }
            });

        })
    </script>
</head>


//测试
<div id="loadxml" class="easyui-dialog" closed="true"
	title="Basic Dialog" data-options="iconCls:'icon-save'"
	style="width:400px;height:200px;padding:10px">
<textarea id="loadxmlTxt" name="textarea"></textarea>
<a href="##" class="easyui-linkbutton" iconCls="icon-save"
	onclick="transformation()">导入</a>
</div>


<body id="designer" class="easyui-layout">

<div region="west" split="true" iconCls="palette-icon" title="BPM流程元素"
	style="width:150px;">
<div class="easyui-accordion" fit="true" border="false">

<a href="##" class="easyui-linkbutton" plain="true"
	iconCls="start-event-icon" wfModel="Start">开始</a><br>

	<a href="##"
	class="easyui-linkbutton" plain="true" iconCls="end-event-icon" wfModel="End">结束</a><br>

<a href="##" class="easyui-linkbutton" plain="true"
	iconCls="user-task-icon" wfModel="UserTask">用户任务</a><br> 
<a href="##" class="easyui-linkbutton" plain="true" iconCls="service-task-icon" wfModel="ServiceTask">服务任务</a><br>
<a href="##" class="easyui-linkbutton" plain="true" iconCls="script-task-icon" wfModel="ScriptTask">脚本任务</a><br>
<a href="##" class="easyui-linkbutton" plain="true"
	iconCls="receive-task-icon" wfModel="ReceiveTask">接收任务</a><br> 
<a href="##" class="easyui-linkbutton" plain="true"
	iconCls="timer-boundary-event-icon" wfModel="TimerCatchEvent">定时器</a><br>
<a href="##" class="easyui-linkbutton" plain="true"
	iconCls="exclusive-gateway-icon" wfModel="ExclusiveGateway">异或门</a><br>
</div>
</div>
<div id="process-panel" region="center" split="true"  iconCls="process-icon" title="流程">
<input id="processDefinitionId" type="hidden" value="<%=request.getParameter("processDefinitionId")%>" />
<input id="formId" type="hidden" value="<%=request.getParameter("formId")%>" />
<input id="pafProcessDefinitionId" type="hidden" value="<%=request.getParameter("pafProcessDefinitionId")%>" />

<!-- toolbar -->
<div id="toolbar-panel" style="background:#F3F3F3;">
<div style="background:#F3F3F3;padding:5px;">
<a href="#"  onclick="saveProcess('<%=request.getContextPath()%>')" title="Edit"><img src="js/jquery-easyui/themes/icons/filesave.png"/>保存</a>&nbsp;&nbsp;
<a href="#"  onclick="redo()" title="Edit"><img src="js/jquery-easyui/themes/icons/redo.png"/>重做</a>&nbsp;&nbsp;
<a href="#"  onclick="undo()" title="Edit"><img src="js/jquery-easyui/themes/icons/undo.png"/>撤消</a>&nbsp;&nbsp;
</div>
</div>
<div id="process-definition-tab" title>
							<div id="designer-area" title="Diagram" style="POSITION: absolute;width:100%;height:100%;padding: 0;border: none;overflow:auto;">
								<div id="paintarea" style="POSITION: absolute;WIDTH: 3000px; HEIGHT: 3000px" ></div>
							</div>
							<div id="xml-area" title="XML" style="width:100%;height:100%;overflow:hidden;overflow-x:hidden;overflow-y:hidden;">
								<textarea id="descriptorarea" rows="38" style="width: 100%;height:100%;padding: 0;border: none;" readonly="readonly"></textarea>
							</div>
				</div>
</div>
<div id="properties-panel" region="east" split="true"
	iconCls="properties-icon" title="流程属性" style="width:250px;"> </div>


</script>
<!-- task context menu -->

<!-- listener configuration window -->
<div id="listener-win" title="Listener Configuration"
	style="width:750px;height:500px;"> </div>
<!-- candidate configuration window -->
<div id="task-candidate-win" title="" style="width:750px;height:500px;">
</div>
</body>
</html>
<script type="text/javascript">
<!--
	createCanvas(false);
//-->
</script>