<%@ page contentType="text/html; charset=utf-8"%>
<meta http-equiv="Content-Type" content="text/html charset=utf-8">
<script type="text/javascript">
<!--
var tid = "<%=request.getParameter("id")%>";

var task = workflow.getFigure(tid);
jq(function(){
			 jq('#task-properties-layout').mouseleave(function() {
          saveTaskProperties(); 
       });
	populateTaskProperites();
	//switchTaskCandidatesList(jq('#performerType').combobox('getValue'));
});


function saveTaskProperties(){
	//alert(tid);
	task.taskId=jq('#id').val();
	task.taskName=jq('#name').val();
	task.setContent(jq('#name').val());
	
	//task.formKey=jq('#formKey').val();
	if(jq('#asynchronous:checkbox').attr('checked')=="checked"){
		task.asynchronous="true";
	}else{
        task.asynchronous="false";
	}
	task.mail_to=jq('#mail_to').val();
	task.mail_from=jq('#mail_from').val();
	task.mail_subject=jq('#mail_subject').val();
	task.mail_cc=jq('#mail_cc').val();
	task.mail_bcc=jq('#mail_bcc').val();
	task.mail_charset=jq('#mail_charset').val();
	task.mail_htmlText=jq('#mail_htmlText').val();
	task.mail_NhtmlText=jq('#mail_NhtmlText').val();
}

function populateTaskProperites(){
	jq('#id').val(task.taskId);
	jq('#name').val(task.taskName);
	jq('#performerType').combobox('setValue',task.performerType);
	jq('#expression').val(task.expression);
	//jq('#formKey').val(task.formKey);
	
	
	if(task.asynchronous=="true"){
		jq('#asynchronous:checkbox').attr('checked',true);
	}else{
        jq('#asynchronous:checkbox').attr('checked',false);
	}
	jq('#mail_to').val(task.mail_to);
	jq('#mail_from').val(task.mail_from);
	jq('#mail_subject').val(task.mail_subject);
	jq('#mail_cc').val(task.mail_cc);
	jq('#mail_bcc').val(task.mail_bcc);
	jq('#mail_charset').val(task.mail_charset);
	jq('#mail_htmlText').val(task.mail_htmlText);
	jq('#mail_NhtmlText').val(task.mail_NhtmlText);
	
	
	loadTaskListeners();
}
function loadTaskListeners(){
	var listeners = task.listeners;
	var listener_grid_rows=[];
	//alert(listeners.getSize());
	for(var i=0;i<listeners.getSize();i++){
		var listener = listeners.get(i);
		var nlistener = {
					id:listener.getId(),
					listenerImplimentation:listener.getServiceImplementation(),
					type:listener.serviceType,
					event:listener.event,
					fields:listener.getFieldsString(),
					action:''
				};
		listener_grid_rows[i]=nlistener;
	};
	//alert(listener_grid_rows);
	var listener_grid_data={
			total:listeners.getSize(),
			rows:listener_grid_rows
	};
	jq('#task-listeners-list').datagrid('loadData',listener_grid_data);
}
//-->
</script>
<div id="task-properties-layout" class="easyui-layout" fit="true">
	<div id="task-properties-panel" region="center" border="true">
		<div id="task-properties-accordion" class="easyui-accordion" fit="true" border="false">
				<table id="general-properties">
					<tr>
						<td align="right">Id:</td>
						<td><input type="text" id="id" name="id" size="10" value="" readonly="true"/></td>
					</tr>
					<tr>
						<td align="right">标签:</td>
						<td><input type="text" id="name" name="name" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">异步:</td>
						<td><input name="asynchronous" id="asynchronous"  type="checkbox"  checked="checked" /></td>
					</tr>	
					<tr>
						<td align="right">To:</td>
						<td><input type="text" id="mail_to" name="collection" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">From:</td>
						<td><input type="text" id="mail_from" name="collection" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">Subject:</td>
						<td><input type="text" id="mail_subject" name="collection" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">CC:</td>
						<td><input type="text" id="mail_cc" name="collection" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">BCC:</td>
						<td><input type="text" id="mail_bcc" name="collection" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">Charset:</td>
						<td><input type="text" id="mail_charset" name="collection" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">HtmlText:</td>
						<td><textarea id="mail_htmlText" name="textarea"></textarea></td>
					</tr>

					<tr>
						<td align="right">Non HtmlText:</td>
						<td><textarea id="mail_NhtmlText" name="textarea"></textarea></td>
					</tr>
					
				</table>

		</div>
	</div>
</div>