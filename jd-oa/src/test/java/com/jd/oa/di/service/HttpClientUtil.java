/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-4-8
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.di.service;

import com.jd.oa.common.file.FileUtil;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpClientUtil {

	private static  HttpClientUtil httpClientUtil;
	public enum REQUEST_TYPE{
		GET,POST
	}
	public int maxConnections = 100;
	public int maxPerRoutes = 20;

	public int requestTimeout = 3000;
	public int waitTimeout = 3000;

	public  int i =0;

	//线程安全的连接池
	PoolingClientConnectionManager pccm;

	private  HttpClientUtil(){
		init();
	}

	private  HttpClientUtil(int maxConnections,int maxPerRoutes){
		this.maxConnections = maxConnections;
		this.maxPerRoutes = maxPerRoutes;
		init();
	}

	public static HttpClientUtil getInstance(){
		if(httpClientUtil == null){
			httpClientUtil = new HttpClientUtil();
		}
		return httpClientUtil;
	}

	public static HttpClientUtil getInstance(int maxConnections,int maxPerRoutes){
		if(httpClientUtil == null){
			httpClientUtil = new HttpClientUtil(maxConnections,maxPerRoutes);
		}
		return httpClientUtil;
	}

	private void init(){
		pccm = new PoolingClientConnectionManager();
		pccm.setMaxTotal(this.maxConnections);
		pccm.setDefaultMaxPerRoute(this.maxPerRoutes);
	}

	public HttpClient newClient(){
		HttpParams params = new BasicHttpParams();
		params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION,HttpVersion.HTTP_1_1);
		params.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,requestTimeout);
		params.setParameter(CoreConnectionPNames.SO_TIMEOUT,waitTimeout);
		return new DefaultHttpClient(pccm,params);
	}

	public void release(){
		if(pccm != null) pccm.shutdown();
	}

	public  byte[] request(REQUEST_TYPE type,String url){
		return request(type, url,new HashMap<String, String>());
	}

	public  byte[] request(REQUEST_TYPE type,String url,Map<String,String> paramsMap){
		List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
		if(paramsMap!=null && paramsMap.size() > 0){
			for(Map.Entry<String,String> entry:paramsMap.entrySet() ){
				nameValuePairList.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
			}
		}
		return request(type, url,nameValuePairList);
	}

	public byte[] request(REQUEST_TYPE type,String url,List<NameValuePair> params){
		HttpClient client = newClient();
		HttpResponse httpResponse = null;
		InputStream inputStream = null;
		switch (type){
			case POST:
				httpResponse = post(client,url,params);
				break;
			case GET:
				httpResponse = get(client,url);
				break;
		}
		try {
			inputStream  = httpResponse.getEntity().getContent();
			return  FileUtil.getBytes(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(inputStream != null) inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return null;
	}

	/**
	 * post方式请求数据
	 * @param client
	 * @param url
	 * @param params
	 * @return
	 */
	public HttpResponse post(HttpClient client,String url,List<NameValuePair> params){
		HttpPost httpPost = new HttpPost(url);
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			return  client.execute(httpPost);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return   null;
	}

	/**
	 * get方式请求数据
	 * @param client
	 * @param url
	 * @return
	 */
	public HttpResponse get(HttpClient client,String url){
		HttpGet httpGet = new HttpGet(url);
		try {
			return client.execute(httpGet);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return  null;
	}

}
