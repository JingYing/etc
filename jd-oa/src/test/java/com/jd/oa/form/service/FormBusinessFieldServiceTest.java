package com.jd.oa.form.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class FormBusinessFieldServiceTest extends SpringContextTestCase {

	@Autowired
	private FormBusinessFieldService formBusinessFieldService;

	/*@Test
	public void testCreateBusinessTable() {
		Map<String,Object> map =formBusinessFieldService.createBusinessTable("0e0a10b8c1b245b89a4a87aca1331e21");
		System.out.println(map.toString());
	}*/

	/*@Test
	public void testCreateTableIncludeChild() {
		Map<String,Object> map =formBusinessFieldService.createTableIncludeChild("0e0a10b8c1b245b89a4a87aca1331e21");
		System.out.println(map.toString());
	}*/
	
	@Test
	public void testFindCreatedTable(){
		List<String> tableNameList = new ArrayList<String>();
		tableNameList.add("qfq");
		tableNameList.add("gfd4");
		tableNameList.add("hth55");
		tableNameList.add("dsa");
		tableNameList.add("T_JDOA_7069081501503224331");
		tableNameList.add("T_JDOA_7196852878194948847");
		
		List<String> resultList = formBusinessFieldService.findCreatedTable(tableNameList);
		System.out.println(resultList.toString());
	}

}
