package com.jd.oa.form.service;

import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class FormSequenceServiceTest extends SpringContextTestCase {

	@Autowired
	private FormSequenceService formSequenceService;

	@Test
	public void testCreateTableIncludeChild() {
		int sequenceValue = formSequenceService.getFormSequenceValue("test", formSequenceService.FORM_SEQUENCE_TYPE_BUSINESS);
		System.out.println(">>>>>>>>>>>>>>sequenceValue = " + sequenceValue + "<<<<<<<<<<<<<<<<<");
	}

}
