package com.jd.oa.app.service;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.paf.service.PafService;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessType;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProcessTaskServiceTest {
	
	
	private ProcessTaskService processTaskService;
	
	@Before()
    public void setUp() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
        processTaskService = (ProcessTaskService)applicationContext.getBean("processTaskService");
    }

	//@Test
	public void parseMailTask(){
		try {
			processTaskService.parseMailTask();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void dealMailTask(){
		try {
			processTaskService.dealMailTask("瀹℃壒缁撴灉锛歔鎵瑰噯] 瀹℃壒鎰忚锛歔 ] PID[1/oa-6231d50f-d33f-11e3-a256-74867ae971f8/oa-e9eeefa8-d33e-11e3-a256-74867ae971f8/UserTask1393556463749/閮ㄩ棬鎬荤洃鎴栦互涓婂鎵?Sun May 04 11:51:33 CST 2014]","tanqi@jd.com");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
//	@Test
//	public void dealSmsTask(){
//		String content="";
//		try {
//			processTaskService.parseSmsTask(content);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
}
