package com.jd.oa.app.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.common.paf.model.PafResult;
import com.jd.oa.common.paf.model.TaskInstance;
import com.jd.oa.common.paf.service.PafService;
import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProcessSearchServiceImplTest extends SpringContextTestCase {

	private int forCondition = 10;
	@Autowired
	private ProcessSearchService processSearchService;
	@Autowired
	private PafService pafSafService;
	
	@Test
	public void testGetMyApplyProTaskInfo(){
		testTaskQueryAdvanced();
	}
	private void testTaskQueryAdvanced(){
		for(int i = 0; i < forCondition; i++){
			long beginTime = System.currentTimeMillis();
			 Map<String, Object> conditions = new HashMap<String, Object>();
		        conditions.put("init","jsmwj");
		        conditions.put("start","0");
		        conditions.put("size","10");
		        conditions.put("sort", "createTime");
		        conditions.put("order", "desc");
			//processSearchService.getMyApplyProTaskInfo("jsmwj");
		    PafResult<List<TaskInstance>> result = pafSafService.taskQueryAdvanced("jsmwj", conditions);
			System.out.println("pafSafService.taskQueryAdvanced 方法执行时间 : " + String.valueOf(System.currentTimeMillis()-beginTime));
		}
	}

}
