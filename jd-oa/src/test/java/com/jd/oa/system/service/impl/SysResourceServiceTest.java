package com.jd.oa.system.service.impl;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import redis.clients.jedis.Jedis;

import com.jd.cachecloud.driver.jedis.JedisOperation;
import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.oa.common.redis.redis.RedisCacheSerializer;
import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.system.model.Resource;
import com.jd.oa.system.model.SysPrivilege;
import com.jd.oa.system.model.SysResource;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysPrivilegeService;
import com.jd.oa.system.service.SysResourceService;
import com.jd.oa.system.service.SysRoleService;
import com.jd.oa.system.service.SysUserService;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class SysResourceServiceTest extends SpringContextTestCase {

	@Autowired
	private SysResourceService sysResourceService;

	@Autowired
	private SysUserService sysUserService;

	@Autowired
	private SysOrganizationService sysOrganizationService;

	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private SysPrivilegeService sysPrivilegeService;
	
	@Autowired
	 private ShardedXCommands redisClient;

	//@Test
	public void testFindByParentId() {

		/*Set<SysResource> sortResources1 = new TreeSet<SysResource>(
				new Comparator() {
					@Override
					public int compare(Object arg0, Object arg1) {
						int r = 0;
						SysResource resource0 = (SysResource) arg0;
						SysResource resource1 = (SysResource) arg1;
						if (resource0.getId() != resource1.getId()) {
							if (resource1.getParent() == null
									&& resource0.getParent() != null) {
								r = 1;
							} else if (resource0.getParent() == null
									&& resource1.getParent() != null) {
								r = -1;
							} else if (resource0.getParent() == null
									&& resource1.getParent() == null) {
								if (resource0.getSortNo() > resource1
										.getSortNo()) {
									r = 1;
								} else if (resource0.getSortNo() < resource1
										.getSortNo()) {
									r = -1;
								}
							} else {
								if (!resource0.getParent().getId()
										.equals(resource1.getParent().getId())) {
									if (resource0.getParent().getSortNo() > resource1
											.getParent().getSortNo()) {
										r = 1;
									} else if (resource0.getParent()
											.getSortNo() < resource1
											.getParent().getSortNo()) {
										r = -1;
									}
								} else {
									if (resource0.getSortNo() > resource1
											.getSortNo()) {
										r = 1;
									} else if (resource0.getSortNo() < resource1
											.getSortNo()) {
										r = -1;
									}
								}
							}

						}
						return r;
					}
				});*/

		Set<SysResource> sortResources = new TreeSet<SysResource>(
				new Comparator() {
					@Override
					public int compare(Object arg0, Object arg1) {
						int r = 0;
						SysResource resource0 = (SysResource) arg0;
						SysResource resource1 = (SysResource) arg1;
						if (resource0.getId() != resource1.getId()) {
							if (resource0.getParent() == null) {
								if (resource1.getParent() == null) {
									if (resource0.getSortNo() > resource1
											.getSortNo())
										r = 1;
									else if (resource0.getSortNo() < resource1
											.getSortNo())
										r = -1;
									else
										r = 1;
								} else {
									if (resource1.getParent().getId()
											.equals(resource0.getId()))
										r = -1;
									else {
										if (resource0.getSortNo() > resource1
												.getParent().getSortNo())
											r = 1;
										else if (resource0.getSortNo() < resource1
												.getParent().getSortNo())
											r = -1;
										else
											r = 1;
									}
								}
							} else {
								if (resource1.getParent() == null) {
									if (resource0.getParent().getId()
											.equals(resource1.getId()))
										r = 1;
									else {
										if (resource0.getParent().getSortNo() > resource1
												.getSortNo())
											r = 1;
										else if (resource0.getParent()
												.getSortNo() < resource1
												.getSortNo())
											r = -1;
										else
											r = 1;
									}
								} else {
									if (resource0
											.getParent()
											.getId()
											.equals(resource1.getParent()
													.getId())) {
										if (resource0.getSortNo() > resource1
												.getSortNo())
											r = 1;
										else if (resource0.getSortNo() < resource1
												.getSortNo())
											r = -1;
										else
											r = 1;
									} else {
										if (resource0.getParent().getSortNo() > resource1
												.getParent().getSortNo())
											r = 1;
										else if (resource0.getParent()
												.getSortNo() < resource1
												.getParent().getSortNo())
											r = -1;
										else
											r = 1;
									}
								}
							}
						}
						return r;
					}
				});

		// 获取用户被分配的资源列表，只包含最底层的资源数据
		Set<SysResource> assignedResources = new HashSet<SysResource>(sysResourceService.findByUserId(sysUserService.getUserIdByUserName("admin")));
		
		for (SysResource re : assignedResources) {
			logger.info("=" + re.getId() + ":" + re.getResourceName() + ":"
					+ re.getSortNo());
		}

		for (SysResource r : assignedResources) {
			sortResources.addAll(getResourcesTree(r));
			for (SysResource re : sortResources) {
				logger.info(r.getResourceName() + "===="+re.getId() + ":" + re.getResourceName() + ":"
						+ re.getSortNo());
			}
		}
		for (SysResource re : sortResources) {
			logger.info(re.getId() + ":" + re.getResourceName() + ":"
					+ re.getSortNo());
		}
	}

	private Set<SysResource> getResourcesTree(SysResource resource) {

		Set<SysResource> resources = new HashSet<SysResource>();

		// 是否有父节点
		if (resource.getParent() != null) {
			resources.addAll(getResourcesTree(resource.getParent()));
		}
		resources.add(resource);
		return resources;
	}

	//@Test
	public void testFindAssignedResources() throws IOException {
		for(int i=0; i<3; i++){
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedResources/bjlxdong"));
			sysResourceService.findAssignedResources("bjlxdong");
		}
	}
	
	@Test
	public void testFindAssignedPrivileges() throws IOException, ClassNotFoundException {
		
		 if ("*".endsWith("*")) {
			redisClient.execute(new JedisOperation<Void>() {
	            @Override
	            public Void call(Jedis jedis) throws Throwable {
	            	jedis.set("USER1123","zhouhuaqi");
	            	jedis.set("USER2345", "bjlxdong");
	            	jedis.set("USER_2345", "bjlxdong");
	            	
	                Set<String> stringKeys = jedis.keys("USER*");
	                for(String stringKey: stringKeys){
	                	logger.info(stringKey);
	                }
	                
	            	jedis.set(RedisCacheSerializer.serialize("USER1123"), RedisCacheSerializer.serialize("zhouhuaqi"));
	            	jedis.set(RedisCacheSerializer.serialize("USER2345"), RedisCacheSerializer.serialize("bjlxdong"));
	            	
	                Set<byte[]> keys = jedis.keys(RedisCacheSerializer.serialize("USER*"));
	                for(byte[] key: keys){
	                	logger.info(RedisCacheSerializer.deSerialize(key));
	                }
	                if (keys != null && keys.size() > 0) {
	                    jedis.del(keys.toArray(new byte[0][0]));
	                }
	                return null;
	            }
	        });
		 } else {
			 redisClient.del(RedisCacheSerializer.serialize("JDOA/Users/wangxiaojun/1"));
	     }
		for(int i=0; i<3; i++){
			redisClient.del(RedisCacheSerializer.serialize("JDOA/Users/bjlxdong/1"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/Users/wangxiaojun/1"));
			SysUser user = (SysUser) sysUserService.getByUserName("wangxiaojun");
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/ERP0002887"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/ERP0005662"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/wangxiaojun"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_app1"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_app2"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_app3"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_app4"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_app5"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_fin1"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_fin2"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_fin3"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_fin4"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_law"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_reg"));
			redisClient.del(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/jdoauser_ceo"));
			sysPrivilegeService.findAssignedPrivileges("bjlxdong");
		}
		
		//logger.info(RedisCacheSerializer.deSerialize(redisClient.get(RedisCacheSerializer.serialize("JDOA/AssignedPrivileges/bjlxdong"))));
	}
}
