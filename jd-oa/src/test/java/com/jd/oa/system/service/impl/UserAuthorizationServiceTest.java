package com.jd.oa.system.service.impl;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.process.model.ProcessType;
import com.jd.oa.process.service.ProcessTypeService;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.UserAuthorizationService;


@ContextConfiguration(locations = { "/spring-config.xml" })
public class UserAuthorizationServiceTest  extends SpringContextTestCase{

	@Autowired
	private UserAuthorizationService userAuthorizationService;
	
	@Autowired
	private SysAuthExpressionService sysAuthExpressionService;
	
	@Autowired
	private ProcessTypeService processTypeService ; 
	
	
	
//	@Test
	public void testgetUserListBySysAuthExpression(){
		SysAuthExpression sysAuthExpression = new SysAuthExpression( ) ;
		
		String[] str = new String[]{
				"fb81a0ef41f54e14a0d1779e6a74103e", //所有人
				"e5f340f08dad4d4da3d040ee18e4f0aa"  //华北区域分公司-区总办
		};
		
		for (int i = 0; i < str.length ; i++) {
			sysAuthExpression = sysAuthExpressionService.get( str[i] );
					
			List<SysUser> list = userAuthorizationService.getUserListBySysAuthExpression(sysAuthExpression) ;
			
			System.out.println( i + "-----------------------" + list.size());
			System.out.println(JsonUtils.toJsonByGoogle(list));
		}
		

	}
	
//	@Test
	public void testFindByProcessDefId(){
		ProcessType processType = processTypeService.findByProcessDefId("221d7f865b1c40f29ad28dccce692da4");
		System.out.println(processType.getProcessTypeName());
	}
	
	@Test
	public void testgetAllParentOrganizationByCode(){
		List<String> str = userAuthorizationService.getAllParentOrganizationByCode("00004625") ;
		System.out.println( str.toString() );
	}
}
