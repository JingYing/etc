package com.jd.oa.system.service.impl;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.oa.common.redis.redis.RedisCacheSerializer;
import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class SysUserServiceImplTest extends SpringContextTestCase {

	@Autowired
	private ShardedXCommands redisClient;

	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysOrganizationService sysOrganizationService;

	@Test
	public void testgetUserIdByUserName() {
//		for (int i = 0; i < 3; i++) {
//			String userName = sysUserService.getUserIdByUserName("admin");
//			System.out.println(userName);
//		}
		long beginTime = System.currentTimeMillis();
		SysUser sysUser = sysUserService.getByUserName("liubo", "1");
		System.out.println("查询耗时>>>>>>>>>" + (System.currentTimeMillis() - beginTime));
		
		beginTime = System.currentTimeMillis();
		sysUser = sysUserService.getByUserName("liubo", "1");
		System.out.println("查询耗时>>>>>>>>>" + (System.currentTimeMillis() - beginTime));
	}

//	@Test
	public void testgetByUserName() throws IOException {
		redisClient.del(RedisCacheSerializer.serialize("JDOA/Users/*"));
		for (int i = 0; i < 3; i++) {
			SysUser sysUser = sysUserService.getByUserName("admin");
			SysUser sysUser1 = sysUserService.getByEmailAndMobile("zhouhuaqi@jd.com",null);
			SysUser sysUser2 = sysUserService.getByEmailAndMobile(null, "15810901386");
			System.out.println(sysUser.toString());
		}
	}
	
//	@Test
	public void getSysUserByUserName() throws IOException {
		//sysUserService.transferUserInfoFromHR()
	}
	//@Test
	public void findUnderlingListTest (){
		List<String> result = sysOrganizationService.findUnderlingList("bjyangyanqiang");
		System.out.print(result+"+++++++++++++++++++++++++++++++++++");
	}
}
