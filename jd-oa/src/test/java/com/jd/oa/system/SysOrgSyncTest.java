package com.jd.oa.system;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.OtherDbUtil;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysPositionService;
import com.jd.oa.system.service.SysUserService;
/**
 * 组织机构导入测试类
 * @author bjliubo
 *
 */
@ContextConfiguration(locations = { "/spring-config.xml" })
public class SysOrgSyncTest extends SpringContextTestCase{
	
	@Autowired
	SysUserService sysUserService;
	
	@Autowired
	SysOrganizationService sysOrganizationService;
	
	@Autowired
	SysPositionService sysPositionService;
	/**
	 * 用户同步
	 */
//	@Test
	public void syncSysUser(){
		System.out.println("============================加载用户信息》》》》》》》》》》》开始=========================");
		List<SysUser> allUser = OtherDbUtil.getAllUserFromEHR();
		System.out.println("============================加载用户信息》》》》》》》》》》》结束=========================");
		Set<String> partitions = new HashSet<String>();
//		for (SysUser user : allUser) {
//			String partition = String.valueOf(Math.abs(user.getUserCode().hashCode()) % 12);
//			if (partitions.contains(partition)) {
//				result.add(user);
//			}
//		}
		System.out.println("============================同步用户信息》》》》》》》》》》》开始=========================");
		if(sysUserService == null){
			sysUserService = SpringContextUtils.getBean(SysUserService.class);
		}
		sysUserService.transferUserInfoFromHR(allUser);
		System.out.println("============================同步用户信息》》》》》》》》》》》结束=========================");
	}
	/**
	 * 组织同步
	 */
//	@Test
	public void syncSysOrg(){
		System.out.println("============================加载组织信息》》》》》》》》》》》开始=========================");
		List<SysOrganization> allOrg = OtherDbUtil.getOrganizationFromEHR();
		System.out.println("============================加载组织信息》》》》》》》》》》》结束=========================");
		Set<String> partitions = new HashSet<String>();
		System.out.println("============================加载组织信息》》》》》》》》》》》开始=========================");
		if(sysOrganizationService == null){
			sysOrganizationService = SpringContextUtils.getBean(SysOrganizationService.class);
		}
		sysOrganizationService.transferOrgInfoFromHR(allOrg);
		System.out.println("============================加载组织信息》》》》》》》》》》》结束=========================");
	}
	/**
	 * 岗位信息同步
	 */
	@Test
	public void syncSysPosition(){
		System.out.println("============================加载岗位信息》》》》》》》》》》》开始=========================");
		List<SysPosition> allOrg = OtherDbUtil.getPositionFromEHR();
		System.out.println("============================加载岗位信息》》》》》》》》》》》结束=========================");
		Set<String> partitions = new HashSet<String>();
		System.out.println("============================同步岗位信息》》》》》》》》》》》开始=========================");
		if(sysPositionService == null){
			sysPositionService = SpringContextUtils.getBean(SysPositionService.class);
		}
		sysPositionService.transferPositionInfoFromHR(allOrg);
		System.out.println("============================同步岗位信息》》》》》》》》》》》结束=========================");
	}
}
