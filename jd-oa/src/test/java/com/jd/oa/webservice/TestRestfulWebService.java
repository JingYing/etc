package com.jd.oa.webservice;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.webservice.CXFRestfulClient;
import com.jd.oa.common.webservice.RESULT_DATA_TYPE;
import com.jd.oa.di.service.HttpClientUtil;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class TestRestfulWebService extends SpringContextTestCase{
	@Test
	public void test(){
		try {
			String resultString = load("http://webservice.webxml.com.cn/WebServices/WeatherWS.asmx/getRegionCountry","");
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}
	private String load(String url,String query) throws Exception{
//		URL restURL = new URL(url);
//		HttpURLConnection conn = (HttpURLConnection) restURL.openConnection();
//		conn.setRequestMethod("POST");
//		conn.setDoOutput(true);
//		conn.setAllowUserInteraction(false);
//		PrintStream ps = new PrintStream(conn.getOutputStream());
//		ps.print(query);
//		ps.close();
//		BufferedReader bReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//		String line;
//		String resultStr = "";
//		while(null != (line=bReader.readLine())){
//			resultStr +=line;
//		}
//		bReader.close();
		 InputStream inputStream = null;
		List providers = new ArrayList();
        providers.add(new JacksonJsonProvider());
        WebClient sweWebClient = WebClient.create(url, providers);
        sweWebClient.type("multipart/form-data");
        
        Response response = sweWebClient.invoke("test",new Object[] {"genericServicexxx"});

        inputStream = (InputStream) response.getEntity();
        StringBuffer buffer = new StringBuffer();
        byte[] bytes = new byte[inputStream.available()];
        while(inputStream.read(bytes) != -1){
            buffer.append(new String(bytes,"utf-8"));
        }
        if(buffer.toString()!=null && !buffer.toString().equals("") && buffer.toString().equals("true")){
        	System.out.println("sucess");
        }else{
        	System.out.println("fail");
        }
		return "";
	}

	@Test
	public void restfulclient(){
		CXFRestfulClient client = new CXFRestfulClient("http://webservice.webxml.com.cn/WebServices/WeatherWS.asmx");
		Map<String,Object> paramsMap = new HashMap<String,Object>();
		paramsMap.put("theRegionCode", "3117");
		Object oeb = client.Get(RESULT_DATA_TYPE.MAP,"getSupportCityDataset",paramsMap);
//		Object oeb = client.Post("getSupportCityDataset",paramsMap);
		System.out.println(oeb);
	}

	@Test
	public void httpclient(){
		CXFRestfulClient client = new CXFRestfulClient("http://oa.jd.com/app/service");
		Map<String,Object> paramsMap = new HashMap<String,Object>();
		paramsMap.put("userName","bjlxdong");
		paramsMap.put("token","a6834b654f0d2a03dfe311fe0369ec7e");
		Object oeb = client.Get(RESULT_DATA_TYPE.MAP,"getMyApplys",paramsMap);
		System.out.println(oeb);
	}


	@Test
	public void testHttpClient(){
		byte[] result=HttpClientUtil.getInstance().request(HttpClientUtil.REQUEST_TYPE.GET,"http://webservice.webxml.com.cn/WebServices/WeatherWS.asmx/getSupportCityDataset?theRegionCode=3117");
		System.out.println(new String(result));
	}
}
