/**
 * 
 */
package com.jd.oa.testsaf;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author 360buy
 *
 */

public interface TestHelloService {
	
	public String echoStr(String str,String p) throws Exception;
	
	public String test(String str);
	
	
	public Map<Object, Object> getMap(Map<Object, Object> map);
	
	public List<Object> getList(List<Object> list);
	
	public Set<Object> getSet(Set<Object> map);
	
	public String getZkStr();
	
}
