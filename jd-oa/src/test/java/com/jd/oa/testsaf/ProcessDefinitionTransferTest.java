package com.jd.oa.testsaf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.transfer.exp.service.ProcessDefinitionExportService;
import com.jd.oa.transfer.imp.service.ProcessDefinitionImportService;

/**
 * @author qiuyang
 *
 */
@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProcessDefinitionTransferTest extends SpringContextTestCase {

	@Autowired
	private ProcessDefinitionExportService processDefinitionExportService;
	
	@Autowired
    private ProcessDefinitionImportService processDefinitionImportService;
	
	@Test
	@Transactional
	public void testProcessDefExport(){
		String processdefId = "ee4215b318c0420491bdcad5ce5051a8";
		processDefinitionExportService.exportProcessDefinition(processdefId);
	}
	
	@Test
	@Transactional
	public void testProcessDefImport(){
		String path = "D:\\testImportFile";
		String fileName = "workflow_ee4215b318c0420491bdcad5ce5051a8.zip";
		File file = new File(path+File.separator+fileName);
		try {
			FileInputStream in = new FileInputStream(file);
			processDefinitionImportService.importProcessDefinition(in);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
