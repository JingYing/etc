package com.jd.oa.saf;

import com.alibaba.dubbo.rpc.service.GenericService;
import com.jd.bk.saf.config.SAFReferenceConfig;
import com.jd.bk.saf.config.SAFRegistryConfig;
import com.jd.oa.common.saf.SafClient;
import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.SpringContextUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class TestSafHelloService extends SpringContextTestCase{
	
	private static Logger logger = LoggerFactory.getLogger(TestSafHelloService.class);
	
	// register spring
//	@Test
	public void testSafService() {
//		TestHelloService helloService = (TestHelloService)SpringContextUtils.getBean("myMessageSender");
//		helloService.test("liubo");
	}
	// saf client api
	@Test
	public void testDynSafService(){
		// Application实现（可选）
//		SAFApplicationConfig safApplication = new SAFApplicationConfig("testsaf-client-api");
//		logger.info("实例SAFApplicationConfig");

		// 注册中心实现（必须）
		SAFRegistryConfig safRegistry = new SAFRegistryConfig();
		safRegistry.setAddress("192.168.229.53:2181,192.168.229.54:2181,192.168.229.55:2181");
		safRegistry.setProtocol("jdZooKeeper");
		List<SAFRegistryConfig> registries = new ArrayList<SAFRegistryConfig>();
		registries.add(safRegistry);
		logger.info("实例SAFRegistryConfig");

		// 引用远程服务（必须）
		SAFReferenceConfig<GenericService> referenceConfig = new SAFReferenceConfig<GenericService>(); // 该实例很重量，里面封装了所有与注册中心及服务提供方连接，请缓存
//		referenceConfig.setApplication(safApplication);
		referenceConfig.setRegistries(registries);
		referenceConfig.setInterface("com.jd.oa.testsaf.TestHelloService"); // 弱类型接口名
		referenceConfig.setGroup("WMS");
		referenceConfig.setVersion("0.0.1");
		referenceConfig.setGeneric(true); // 声明为泛化接口
		referenceConfig.setProtocol("mydubbo");
		referenceConfig.setFilter("default,-monitor,jsonstrfilter"); // 加上此句返回结果为jsonString，要不然返回Map
		logger.info("实例SAFReferenceConfig");

		// 在本地使用GenericService（必须）
		GenericService genericService = (GenericService) referenceConfig.get();
		// 基本类型以及Date,List,Map等不需要转换，直接调用 
		Object result = genericService.$invoke("test", new String[] {"java.lang.String"}, new Object[] {"genericServicexxx"}); //用用配置的接口方法
		logger.info("远程调用{}方法，返回结果{}", "echostr", result);
		
		// 对象需要先将属性放到Map里
		Map<String, Object> obj = new HashMap<String, Object>(); 
		obj.put("name", "username"); 
		obj.put("passwd", "!@#$qwer"); 
		result =  genericService.$invoke("echoObj", new String[] {"com.jd.testsaf.TestObj"}, new Object[] {obj}); 
		logger.info("远程调用{}方法，返回结果{}", "echoObj", result);
	}


	@Test
	public void testSafClient(){
		SafClient safClient =  SpringContextUtils.getBean("safClient");
		Object result = safClient.invoke("com.jd.oa.service.ws.ProcessService","getMyApplys",new String[] {"java.lang.String"}, new Object[] {"bjlxdong"});
		System.out.println(result);
	}
	
}
