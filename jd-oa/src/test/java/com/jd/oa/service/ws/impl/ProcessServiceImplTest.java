package com.jd.oa.service.ws.impl;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.service.dto.MyApply;
import com.jd.oa.service.dto.MyTask;
import com.jd.oa.service.dto.ServiceResponse;
import com.jd.oa.service.ws.ProcessService;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProcessServiceImplTest extends SpringContextTestCase {
	
	private static final Logger logger = Logger.getLogger(ProcessServiceImplTest.class);

	//@Test
	public void testGetMyApplys() {
		String serviceUrl = "http://oa.jd.com/services/ws/processService";
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(ProcessService.class);
        factory.setAddress(serviceUrl);
        ProcessService processService = (ProcessService) factory.create();
 
        ServiceResponse<MyApply> result = processService.getMyApplys("bjlxdong");
        
        logger.info(result);
	}

	@Test
	public void testGetMyTasks() {
		String serviceUrl = "http://oa.jd.com/services/ws/processService";
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(ProcessService.class);
        factory.setAddress(serviceUrl);
        ProcessService processService = (ProcessService) factory.create();
 
        ServiceResponse<MyTask> result = processService.getMyTasks("lizhuo");
        
        logger.info(result);
	}

}
