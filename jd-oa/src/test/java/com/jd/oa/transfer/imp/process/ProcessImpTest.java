package com.jd.oa.transfer.imp.process;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.process.xpackage.FormTemplateDocument.FormTemplate;
import org.apache.xmlbeans.process.xpackage.NodesDocument;
import org.apache.xmlbeans.process.xpackage.NodesDocument.Nodes;
import org.apache.xmlbeans.process.xpackage.PackageDocument;
import org.apache.xmlbeans.process.xpackage.NodeDocument.Node;
import org.apache.xmlbeans.process.xpackage.PackageHeaderDocument.PackageHeader;
import org.apache.xmlbeans.process.xpackage.PrintTemplateDocument.PrintTemplate;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessNode;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProcessImpTest extends SpringContextTestCase{
	@Test
	public void test(){
		PackageDocument doc = parseXml("d:/exp/process/workflow_test.xml");
		impProcess(doc);
	}
	public void impProcess(PackageDocument doc) {
		PackageDocument.Package packageElement = doc.getPackage();
		/** GET process Definition * */
		PackageHeader packageHeaderAttribute = packageElement.getPackageHeader();
		ProcessDefinition processDefinition = new ProcessDefinition();
		/**
		 * 流程定义 start
		 */
		processDefinition.setProcessTypeId(processDefinition.getProcessTypeId());
		processDefinition.setPafProcessDefinitionId(processDefinition.getPafProcessDefinitionId());
		processDefinition.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
		processDefinition.setOrganizationType(processDefinition.getOrganizationType());
		processDefinition.setFilePath(processDefinition.getFilePath());
		processDefinition.setIsIssue(processDefinition.getIsIssue());
		processDefinition.setFormId(processDefinition.getFormId());
		processDefinition.setAlertMode(processDefinition.getAlertMode());
		processDefinition.setIsComment(processDefinition.getIsComment());
		processDefinition.setIsUploadAttach(processDefinition.getIsUploadAttach());
		processDefinition.setIsRelateDoc(processDefinition.getIsRelateDoc());
		processDefinition.setIsRelateProcessInstance(processDefinition.getIsRelateProcessInstance());
		processDefinition.setIsTemplate(processDefinition.getIsTemplate());
		processDefinition.setIsManagerInspect(processDefinition.getIsManagerInspect());
		processDefinition.setSortNo(processDefinition.getSortNo());
		processDefinition.setYn(processDefinition.getYn());
		processDefinition.setOwner(processDefinition.getOwner());
		
		/**
		 * 流程定义end
		 */
		/**
		 * 节点定义
		 */
		getNodes(packageElement);
	}
	/**
	 * create node schema
	 * @param packageElement
	 * @param processDefId
	 */
	private void getNodes(PackageDocument.Package packageElement){
		List<ProcessNode> nodes = new ArrayList<ProcessNode>();
		Nodes nodesAttribute = packageElement.getNodes();
		Node[] nodeArray = nodesAttribute.getNodeArray();
		for (int i = 0; i < nodeArray.length; i++) {
			Node node = nodeArray[i];
			if(node != null){
				ProcessNode processNode = new ProcessNode();
				processNode.setId(node.getId());
				processNode.setProcessDefinitionId(node.getProcessDefinitionId());
				processNode.setNodeId(node.getNodeId());
				processNode.setFormTemplateId(node.getFormTemplateId());
				processNode.setPrintTemplateId(node.getPrintTemplateId());
				processNode.setIsFirstNode(node.getIsFirstNode());
				processNode.setAddsignRule(node.getAddsignRule());
				processNode.setManagerNodeId(node.getManagerNodeId());
				processNode.setYn(node.getYn());
				nodes.add(processNode);
				/**
				 * 加载node 对应的 formTemplate
				 */
				getFormTemplate(node);
				/**
				 * 加载node 对应的 printTemplate
				 */
				getPrintTemplate(node);
			}
		}
	}
	/**
	 * 节点对应的formtemplate
	 * @param node
	 */
	private void getFormTemplate(Node node){
		com.jd.oa.form.model.FormTemplate formTemplate = new com.jd.oa.form.model.FormTemplate();
		FormTemplate[] formTemplateArray = node.getFormTemplateArray();
		for(int i = 0 ; i < formTemplateArray.length ; i++){
			formTemplate.setFormId(formTemplateArray[i].getFormId());
			formTemplate.setTemplateDesc(formTemplateArray[i].getTemplateDesc());
			formTemplate.setYn(formTemplateArray[i].getYn());
			formTemplate.setTemplateName(formTemplateArray[i].getTemplateName());
			formTemplate.setTemplatePath(formTemplateArray[i].getTemplatePath());
			formTemplate.setTemplateType(formTemplateArray[i].getTemplatePath());
		}
		
	}
	/**
	 * 节点对应的printtemplate
	 * @param node
	 */
	private void getPrintTemplate(Node node){
		com.jd.oa.form.model.FormTemplate formTemplate = new com.jd.oa.form.model.FormTemplate();
		PrintTemplate[] printTemplateArray = node.getPrintTemplateArray();
		for(int i = 0 ; i < printTemplateArray.length ; i++){
			formTemplate.setFormId(printTemplateArray[i].getFormId());
			formTemplate.setTemplateDesc(printTemplateArray[i].getTemplateDesc());
			formTemplate.setYn(printTemplateArray[i].getYn());
			formTemplate.setTemplateName(printTemplateArray[i].getTemplateName());
			formTemplate.setTemplatePath(printTemplateArray[i].getTemplatePath());
			formTemplate.setTemplateType(printTemplateArray[i].getTemplatePath());
		}
	}
	
	/**
	 * 将要解析的xml文件转换成xmlbeans的PackageDocument操作对象
	 * 
	 * @param file xml文件全路径
	 * @return PackageDocument
	 */
	public PackageDocument parseXml(String file) {
		File xmlfile = new File(file);
		PackageDocument doc = null;
		try {
			doc = PackageDocument.Factory.parse(xmlfile);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.out.println("解析xml模型文件格式时失败，期望格式是一个符合BPDL-Process.xsd格式的xml文件，错误信息参考：" + e.toString());
		} 
		return doc;
	}
}
