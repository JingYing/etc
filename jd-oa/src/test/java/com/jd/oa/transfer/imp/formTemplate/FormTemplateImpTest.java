package com.jd.oa.transfer.imp.formTemplate;

import java.io.File;

import org.apache.xmlbeans.form.xpackage.PackageDocument;
import org.apache.xmlbeans.form.xpackage.PackageHeaderDocument.PackageHeader;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.transfer.exp.process.ProcessExpTest;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class FormTemplateImpTest extends SpringContextTestCase{
	@Test
	public void test(){
		PackageDocument doc = parseXml("d:/exp/formTemplate/OA_formTemplate_test.xml");
		impFormTemplate(doc);
	}
	/**
	 * 将要解析的xml文件转换成xmlbeans的PackageDocument操作对象
	 * 
	 * @param file xml文件全路径
	 * @return PackageDocument
	 */
	public PackageDocument parseXml(String file) {
		File xmlfile = new File(file);
		PackageDocument doc = null;
		try {
			doc = PackageDocument.Factory.parse(xmlfile);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			System.out.println("解析xml模型文件格式时失败，期望格式是一个符合BPDL-FormTemplate.xsd格式的xml文件，错误信息参考：" + e.toString());
		} 
		return doc;
	}
	/**
	 * 读取xml文件描述信息，将包含的信息安装到当前AWS平台
	 * 
	 * @param doc 一个已经将xml文件换成xmlbeans可识别的PackageDocument操作对象
	 * @return 成功返回一个大于0的表单模型id值
	 */
	public void impFormTemplate(PackageDocument doc) {
		// Retrieve the <package> element and add a new <package> element.
		PackageDocument.Package packageElement = doc.getPackage();
		/** GET FORM TEMPLATE * */
		PackageHeader packageHeaderAttribute = packageElement.getPackageHeader();
		FormTemplate formTemplate = new FormTemplate();
		formTemplate.setFormId(packageHeaderAttribute.getFormId());
		formTemplate.setTemplateDesc(packageHeaderAttribute.getTemplateDesc());
		formTemplate.setYn(packageHeaderAttribute.getYn());
		formTemplate.setTemplateName(packageHeaderAttribute.getTemplateName());
		formTemplate.setTemplatePath(packageHeaderAttribute.getTemplatePath());
		formTemplate.setTemplateType(packageHeaderAttribute.getTemplateType1());
		/**
		 * 具体业务逻辑
		 */
		System.out.println("导入Form模型文件:[" + packageElement.getName() + "] --成功!");
		
	}
}
