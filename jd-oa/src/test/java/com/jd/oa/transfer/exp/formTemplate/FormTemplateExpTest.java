package com.jd.oa.transfer.exp.formTemplate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.form.xpackage.ExtendedAttributeDocument.ExtendedAttribute;
import org.apache.xmlbeans.form.xpackage.ExtendedAttributesDocument;
import org.apache.xmlbeans.form.xpackage.PackageDocument;
import org.apache.xmlbeans.form.xpackage.PackageHeaderDocument.PackageHeader;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.transfer.exp.ExportContext;
@ContextConfiguration(locations = { "/spring-config.xml" })
public class FormTemplateExpTest extends SpringContextTestCase{
	@Autowired
	private FormTemplateService formTemplateService;
	@Test
	public void test(){
		FormTemplateExpTest dt = new FormTemplateExpTest();
		// Create an instance of a Datetime type based on the received XML's
		// schema
		PackageDocument doc = dt.parseXml("d:/exp/formTemplate/OA_formTemplate_init.xml");
		// Prints the element values from the XML
		/**
		 * test template name = EBS运维流程-主数据流程
		 */
		dt.createDocument(doc, "d:/exp/formTemplate/OA_formTemplate_test.xml", "8c21c71e4652489583aeab6e773a91b5");
	}
	
	/**
	 * 解析一个表单模型到指定xml文件
	 */
	public boolean createDocument(PackageDocument doc, String file, String formTemplateId) {
		PackageDocument.Package packageElement = doc.getPackage();// .getPackage();
		try {
			/** FormTemplate * */
			if(formTemplateService == null){
				formTemplateService = SpringContextUtils.getBean(FormTemplateService.class);
			}
			FormTemplate formTemplate = (FormTemplate) formTemplateService.get(formTemplateId);
			// insert package attribute
			packageElement.setId(formTemplate.getId());
			packageElement.setName(formTemplate.getTemplateName());
			// insert packageHeader attribute
			PackageHeader packageHeaderAttribute = packageElement.addNewPackageHeader();
			packageHeaderAttribute.setBPDLModel("eForm(View) Model");
			packageHeaderAttribute.setBPDLVersion(ExportContext.getVersion());
			packageHeaderAttribute.setBPDLVendor(ExportContext.getVendor());
			packageHeaderAttribute.setCreateDate(new Date().toString());
			packageHeaderAttribute.setUpdataDate(new Date().toString());
			packageHeaderAttribute.setDescription("");
			packageHeaderAttribute.setType("");
			packageHeaderAttribute.setFormId(formTemplate.getFormId());
			packageHeaderAttribute.setTemplateDesc(formTemplate.getTemplateDesc());
			packageHeaderAttribute.setYn(formTemplate.getYn());
			packageHeaderAttribute.setTemplateName(formTemplate.getTemplateName());
			packageHeaderAttribute.setTemplatePath(formTemplate.getTemplatePath());
			packageHeaderAttribute.setTemplateType1(formTemplate.getTemplatePath());
			/** form end * */
			
			// //insert ExtendedAttributes attribute
			ExtendedAttributesDocument.ExtendedAttributes extendedAttributesElement = packageElement.addNewExtendedAttributes();
			ExtendedAttribute extendedAttribute = extendedAttributesElement.addNewExtendedAttribute();
			extendedAttribute.setName("");
			extendedAttribute.setValue("");
			//		   	   
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		}

		// Validate the new XML
		boolean isXmlValid = validateXml(doc, file);
		if (isXmlValid) {
			File f = new File(file);
			try {
				doc.save(f, ExportContext.getDefaultXmlOptions());
				System.out.println("成功导出表单模型文件名为:[" + f.getPath() + "]的表单模型");
				return true;
			} catch (IOException e) {
				System.out.println("导出表单模型文件失败:" + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
		} else {
			return false;
		}
	}
	/**
	 * Creates a File from the XML path provided in main arguments, then parses
	 * the file's contents into a type generated from schema.
	 */
	public PackageDocument parseXml(String file) {
		File xmlfile = new File(file);
		PackageDocument doc = null;

		try {
			doc = PackageDocument.Factory.parse(xmlfile);
		} catch (XmlException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}
	/**
	 * <p>
	 * Validates the XML, printing error messages when the XML is invalid. Note
	 * that this method will properly validate any instance of a compiled schema
	 * type because all of these types extend XmlObject.
	 * </p>
	 * 
	 * <p>
	 * Note that in actual practice, you'll probably want to use an assertion
	 * when validating if you want to ensure that your code doesn't pass along
	 * invalid XML. This sample prints the generated XML whether or not it's
	 * valid so that you can see the result in both cases.
	 * </p>
	 * 
	 * @param xml
	 *            The XML to validate.
	 * @return <code>true</code> if the XML is valid; otherwise,
	 *         <code>false</code>
	 */
	public boolean validateXml(XmlObject xml, String fileName) {
		boolean isXmlValid = false;

		// A collection instance to hold validation error messages.
		ArrayList validationMessages = new ArrayList();

		// Validate the XML, collecting messages.
		isXmlValid = xml.validate(new XmlOptions().setErrorListener(validationMessages));

		if (!isXmlValid) {
			System.out.println("XML文件完整性错误:" + fileName);
			for (int i = 0; i < validationMessages.size(); i++) {
				XmlError error = (XmlError) validationMessages.get(i);
				System.out.println(error.getMessage());
				System.out.println(error.getObjectLocation());
			}
		}
		return isXmlValid;
	}
}
