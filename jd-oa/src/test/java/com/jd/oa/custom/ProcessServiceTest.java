package com.jd.oa.custom;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.Test;

import com.jd.oa.custom.ProcessService;
import com.jd.oa.di.service.HelloService;

public class ProcessServiceTest {
	@Test
	public void testFindProcessTaskHistoryByBusinessId() throws Exception {

		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf
				.createClient("http://oa.jd.com/services/ws/processService?wsdl");
		Object[] res = client.invoke("findProcessTaskHistoryByBusinessId",
				"1b8fc708558f49bd8c7edff06b31f11e");
		System.out.println(res[0] + "++++++++++++++++");

	}

	// @Test
	public void testhelloWorld() throws Exception {

		ClientProxyFactoryBean factory = new ClientProxyFactoryBean();

		factory.setServiceClass(HelloService.class);

		factory.setAddress("http://oa.jd.com/services/ws/helloService?wsdl");

		// factory.getServiceFactory().setDataBinding(new AegisDatabinding());

		HelloService client = (HelloService) factory.create();

		System.out.println(client.sayHello("2", "2"));

		System.exit(0);

	}
	@Test
	public void updatePurchaseDetail() throws Exception {

		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		String parm="<purchaseDetails>"+
						"<purchaseDetail sn=\"01\">"+
						 	"<id>a5b761e3e37f49cf93141d24ebec3cc8</id>"+
						    "<comments></comments>"+
						    "<productStatus>1</productStatus>"+
						    "<inputNumCount>2</inputNumCount>"+
						    "<purchaseLogs></purchaseLogs>"+//采购员操作日志
						    "<purchasePeople>dbbjzjr</purchasePeople>"+//采购员
						    "<purchaseArea>db</purchaseArea>"+//采购区域
						    "<statusReason>refuseg嘎嘎</statusReason>"+
						"</purchaseDetail>"+
					"</purchaseDetails>";
		Client client = dcf
				.createClient("http://oa.jd.com/services/ws/processService?wsdl");
		Object[] res = client.invoke("updatePurchaseDetail",
				parm);
		System.out.println(res[0] + "++++++++++++++++");

	}

}
