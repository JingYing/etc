package com.jd.oa.agent.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.agent.service.UnifyAgentReceiveService;
import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.SpringContextUtils;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class UnifyAgentReceiveServiceImplTest2 extends SpringContextTestCase {
	
	private UnifyAgentReceiveService service;
	
	@Test
	public void testRecive(){
		if(service == null){
			service = SpringContextUtils.getBean(UnifyAgentReceiveService.class);
		}
		String xmlData;
		try {
			xmlData = read();
			service.receive(xmlData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private String read() throws IOException {
		File file = new File("D:/abc.xml");
        FileReader reader = new FileReader(file);
        int fileLen = (int)file.length();
        char[] chars = new char[fileLen];
        reader.read(chars);
        String txt = String.valueOf(chars);
        System.out.println(txt);
        
        return txt;
	}
}
