package com.jd.oa.common.jss;

import com.jcloud.jss.JingdongStorageService;
import com.jcloud.jss.domain.Bucket;
import com.jcloud.jss.domain.ObjectListing;
import com.jcloud.jss.domain.ObjectSummary;
import com.jcloud.jss.domain.StorageObject;
import com.jcloud.jss.exception.StorageClientException;
import com.jd.common.util.StringUtils;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.process.model.ProcessDefinition;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.*;
import java.util.List;
import java.util.Map;


/**
 * JSS测试类
 */
public class JssServiceTestNew {

    JssService jssService;

    /**
     *  初始化配置文件
     */
    @Before()
    public void setUp() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config-jss-test.xml");
        jssService = (JssService)applicationContext.getBean("jssService");
    }


    @Test
    public void saveProcessDefinitionToJss(){ 
    	String bucketName = "jd-oa";
    	String key = "af4aeaae87d84af4a119c6fd1d5222dd.bpmn20.xml";
    	//String key = "dbadc19ff7c44d4bacac1bdc5f29e919.bpmn20.xml";
	    InputStream inputStringStream = null;
		try {
			inputStringStream = new FileInputStream(new File("D:\\OA\\def_process1385193597447.xml"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    jssService.deleteObject(bucketName, key);
	    jssService.uploadFile(bucketName, key, inputStringStream);
    }
	    
}
