package com.jd.oa.common.mail.impl;

import static org.junit.Assert.fail;

import java.util.List;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.mail.MailReceiveService;
import com.jd.oa.common.mail.MailReceiveService.MailFlag;
import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class MailReceiveServiceImplTest extends SpringContextTestCase {

	private static final Logger logger = Logger
			.getLogger(MailReceiveServiceImplTest.class);

	@Autowired
	private MailReceiveService mailReceiveService;

	@Test
	public void testGetAllMails() throws MessagingException {
		logger.info("getMessageCount:" + mailReceiveService.getMessageCount());
		logger.info("getNewMessageCount:" + mailReceiveService.getNewMessageCount());
		logger.info("getUnreadMessageCount:" + mailReceiveService.getUnreadMessageCount());
		logger.info("getDeletedMessageCount:" + mailReceiveService.getDeletedMessageCount());
		List<Message> messages = mailReceiveService.getNewMails();
		List<Message> unreadMessages = mailReceiveService.getUnreadMails();
		List<Message> allMessages = mailReceiveService.getAllMails();
		for (Message message : unreadMessages) {
			logger.info(mailReceiveService.getSubject(message));
			logger.info(mailReceiveService.getFrom(message));
			logger.info(mailReceiveService.getTOAddress(message));
			
			logger.info(mailReceiveService.getReplySign(message));
			logger.info(mailReceiveService.isContainAttach(message));
			if(mailReceiveService.getSubject(message).equals("流程中心_审批任务：职能研发部申请京豆_dec3(批准)")){
				logger.info(mailReceiveService.getContent(message));
			}
			logger.info(mailReceiveService.getSentDate(message));
			logger.info(mailReceiveService.getTOAddress(message));
			//logger.info(mailReceiveService.getContent(message));
		}
	}

	public void testGetNewMails() {
		mailReceiveService.getNewMails();
	}

	//@Test
	public void setMailFlag() {
		try {
			List<Message> messages = mailReceiveService.getAllMails();
			for (Message message : messages) {
				if (!message.getFolder().isOpen())
					message.getFolder().open(Folder.READ_WRITE);
				logger.info(((MimeMessage) message).getMessageID() + "\t"
						+ message.getSubject());
				message.getFolder().close(true);
				mailReceiveService.setMailFlag(message, MailFlag.SEEN);
			}
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	//@Test
	public void testGetMessageCount() {
		logger.info(mailReceiveService.getMessageCount());
	}

	//@Test
	public void testGetNewMessageCount() {
		logger.info(mailReceiveService.getNewMessageCount());
	}

	//@Test
	public void testGetUnreadMessageCount() {
		logger.info(mailReceiveService.getUnreadMessageCount());
	}

	// @Test
	public void testGetTOAddress() {
		fail("Not yet implemented");
	}

	// @Test
	public void testGetCCAddress() {
		fail("Not yet implemented");
	}

	// @Test
	public void testGetBCCAddress() {
		fail("Not yet implemented");
	}

	// @Test
	public void testGetFrom() {
		fail("Not yet implemented");
	}

	// @Test
	public void testGetSubject() {
		fail("Not yet implemented");
	}

	// @Test
	public void testGetSentDate() {
		fail("Not yet implemented");
	}

	// @Test
	public void testGetReplySign() {
		fail("Not yet implemented");
	}

	// @Test
	public void testIsContainAttach() {
		fail("Not yet implemented");
	}

	// @Test
	public void testGetContent() {
		fail("Not yet implemented");
	}

}
