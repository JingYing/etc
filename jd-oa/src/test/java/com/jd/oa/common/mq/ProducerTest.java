package com.jd.oa.common.mq;

import javax.annotation.Resource;
import javax.jms.JMSException;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.jd.activemq.producer.JMSProducer;
import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProducerTest extends SpringContextTestCase{
	
	@Resource(name = "producer")
	private JMSProducer producer;

	@Test
	public void test() {
		for(int i = 0 ; i < 10 ; i++){
			try {
				/********************** 在配置文件中已经配置destination **********************/
//				producer.send("消息内容", "消息归档时的标识，比如订单的ID");
				
				/********************** 在配置文件中没有配置destination **********************/
				// 如果XXX消息是Topic类型
				producer.send("VirtualTopic.mmsOA", "test send mq", "消息归档时的标识，ID[" + i + "]");
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
//				// 如果XXX消息是Queu类型
//				producer.send("mmsOA", "消息内容", "消息归档时的标识，比如订单的ID");
				
//				/********************** 需要发送多个消息时，配置文件中无需配置destination, 例如要发送消息XXX和YYY **********************/
//				// 如果XXX消息是Topic类型
//				producer.send("VirtualTopic.XXX", "消息内容", "消息归档时的标识，比如订单的ID");
//				// 如果XXX消息是Queu类型
//				producer.send("XXX", "消息内容", "消息归档时的标识，比如订单的ID");
//
//				// 如果YYY消息是Topic类型
//				producer.send("VirtualTopic.YYY", "消息内容", "消息归档时的标识，比如订单的ID");
//				// 如果YYY消息是Queu类型
//				producer.send("YYY", "消息内容", "消息归档时的标识，比如订单的ID");
				
				
			} catch (JMSException e) {
				e.printStackTrace();
			}
			logger.info("Send 消息内容  message success");
		}
	}
}
