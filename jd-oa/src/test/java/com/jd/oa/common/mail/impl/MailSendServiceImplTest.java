package com.jd.oa.common.mail.impl;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.mail.MailSendService;
import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class MailSendServiceImplTest extends SpringContextTestCase {

	@Autowired
	private MailSendService mailSendService;
	
	//@Test
	public void testSendSimpleMail() {
		//mailSendService.sendSimpleMail(new String[]{"zhouhuaqi@jd.com"}, "订火车票", "123456测试");
	}

	@Test
	public void testSendHtmlMail() {
		//mailSendService.sendHtmlMail(new String[]{"yfwangxiaojun@jd.com","zhouhuaqi@jd.com"}, "订火车票", "<htm><head><h2>订火车票</h2></head><body><table><tr><th>序号</th><th>姓名</th><th>邮件</th></tr><tr><td>1</td><td>zhouhuaqi</td><td>zhouhuaqi@jd.com</td></tr></table></body></html>");
		mailSendService.sendHtmlMail(new String[]{"yfwangxiaojun@jd.com","zhouhuaqi@jd.com"}, "email", "<html><body>您好！<BR>您有一个待办任务需要处理，详细信息如下：<BR><a href='www.baidu.com' onclick=\"mailto:jd-oa@jd.com?subject=京东OA-审批&body=审批意见：<input type=text id='comment'><BR>审批结果：1 <BR>任务ID：${task.getId()}<BR>流程实例ID：${task.getProcessInstanceId()}\">测试</a><BR>任务创建时间：${task.getCreateTime()}<BR>任务名称：${task.getName()}<BR><BR><input type='button' value='批准' onclick=\"mailto:jd-oa@jd.com?subject=京东OA-审批&body=审批意见：<input type=text id='comment'><BR>审批结果：1 <BR>任务ID：${task.getId()}<BR>流程实例ID：${task.getProcessInstanceId()}\"><BR><input type='button' value='拒绝' onclick=\"mailto:jd-oa@jd.com?subject=京东OA-审批&body=审批意见：<input type=text id='comment'><BR>审批结果：2 <BR>任务ID：${task.getId()}<BR>流程实例ID：${task.getProcessInstanceId()}\"><BR><input type='button' value='驳回' onclick=\"mailto:jd-oa@jd.com?subject=京东OA-审批&body=审批意见：<input type=text id='comment'><BR>审批结果：3 <BR>任务ID：${task.getId()}<BR>流程实例ID：${task.getProcessInstanceId()}\"><BR></body></html>");

	}

	//@Test
	public void testSendAttachedFileMail() {
		fail("Not yet implemented");
	}

	//@Test
	public void testSendTemplateMail() {
		fail("Not yet implemented");
	}

}
