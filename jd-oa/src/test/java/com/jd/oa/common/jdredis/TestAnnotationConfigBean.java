package com.jd.oa.common.jdredis;

import com.jd.cachecloud.driver.jedis.JedisOperation;
import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.oa.common.redis.annotation.*;
import com.jd.oa.common.redis.redis.RedisCacheSerializer;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.system.model.SysPrivilege;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

@Component
@CacheProxy
public class TestAnnotationConfigBean {
    @Resource
    private ShardedXCommands redisClient;

    @CachePut(key = @CacheKey(template = "CACHE/${p0}/${p1}"), expire = 43200)
    public HashMap getUserMap(String a, String b) {
        HashMap hashMap = new HashMap();
        for (int i = 0; i < 100; i++) {
            hashMap.put(i, "value" + i);
        }
        return hashMap;
    }

    @CachePut(key = @CacheKey(template = "WfFile/${p0.id}"), expire = 43200)
    public List<WfFile> getWfFileList(WfFile file) {

        List<WfFile> list = new ArrayList<WfFile>();
        for (int i = 0; i < 100; i++) {
            WfFile wfFile = new WfFile();
            wfFile.setId(IdUtils.uuid2());
            list.add(wfFile);
        }
        System.out.println("=====================================");
        System.out.println("from db getWfFileList");
        System.out.println("=====================================");
        return list;
    }

    public List<WfFile> getWfFileList1(WfFile file) throws IOException, ClassNotFoundException {

        Object obj = redisClient.get("test".getBytes());
        if(obj == null){
            List<WfFile> list = new ArrayList<WfFile>();
            for (int i = 0; i < 100; i++) {
                WfFile wfFile = new WfFile();
                wfFile.setId(IdUtils.uuid2());
                list.add(wfFile);
            }
            obj = list;

            redisClient.setex("test".getBytes(),6000000,Arrays.toString(list.toArray()).getBytes());
            System.out.println("=====================================");
            System.out.println("from db getWfFileList");
            System.out.println("=====================================");
        }
        return (List<WfFile>)obj;
    }

    @RemoveCache(key = @CacheKey(template = "WfFile*", simple = true))
    public void addFile(WfFile file) {
        System.out.println("=====================================");
        System.out.println("from db addFile");
        System.out.println("=====================================");
    }

    @RemoveCache(key = @CacheKey(template = "cztest", simple = true))
    @SimpleCache(key = "cztest", expire = 240)
    public List getUsers() {
        List list = new ArrayList();
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        System.out.println("=====================================");
        System.out.println("from db getUsers");
        System.out.println("=====================================");
        return list;
    }

    @CachePut(key = @CacheKey(template = "JDOA/AssignedPrivileges/${p0}"), expire = 43200)
    public List<SysPrivilege> testV14(String username) {
        System.out.println("=====================================");
        System.out.println("testV14 ");
        System.out.println("=====================================");
        return null;
    }


    public void test223() {
        final RedisCacheSerializer redisCacheSerializer = new RedisCacheSerializer();
        final Set<byte[]> bytesKey = new HashSet<byte[]>();
        final Set<String> stringsKey = new HashSet<String>();
        redisClient.execute(new JedisOperation<Void>() {
            @Override
            public Void call(Jedis jedis) throws Throwable {
                stringsKey.addAll(jedis.keys("*"));
                bytesKey.addAll(jedis.keys("*".getBytes()));
                jedis.del(stringsKey.toArray(new String[0]));
                return null;  //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        for (byte[] s : bytesKey) {
            try {
                System.out.println((String) RedisCacheSerializer.deSerialize(s));

            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (ClassNotFoundException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        System.out.println("111111111");
    }
}