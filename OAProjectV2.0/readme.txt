1、静态代理
	* 给某个对象提供一个代理对象,并由代理对象控制对源对象(目标对象)的引用.
	* 代理对象除了实现目标对象的功能外,它还可以提供额外的功能实现.
	* 静态代理缺陷:需要创建多个代理类
2、动态代理
3、事务的动态代理
	* 静态代理是编译时加入的,动态代理是运行时加入的.
4、 利用tomcat管理来配置连接池
	* 拷贝admin工程到webapps下
	* 拷贝admin.xml到conf\Catalina\localhost下
	* 修改tomcat-users.xml添加用户
5、数据库连接池配置
	* conf\Catalina\localhost下创建OAProjectV2.xml
	* 拷贝数据库驱动到Tomcat lib下