package com.bjpowernode.service;

import java.util.Map;

import com.bjpowernode.bean.Users;

public interface UserService {
	//判断用户名是否存在
	public boolean isLoginUser(String username);
	
	//判断用户名和密码是否匹配
	public boolean isLoginUser(String username,String password);
	
	public void updateLogonTimeAndCount(String username);
	
	public Users getUsersInfo(String username);
	
	// 添加用户
	public void addUser(Map dto);
}
