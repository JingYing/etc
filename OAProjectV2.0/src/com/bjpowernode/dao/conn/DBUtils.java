package com.bjpowernode.dao.conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.bjpowernode.system.XmlConfigReader;
import com.bjpowernode.system.bean.JdbcConfig;

public class DBUtils {
	// 日志的记录
	private static Logger logs = Logger.getLogger(DBUtils.class);

	// 资源文件的名称
	// final private static String OPTION_FILE_NAME = "DBOption";
	// 数据库的连接串
	private static String url = null;
	// 数据库的驱动
	private static String driver = null;
	// 数据库用户名称
	private static String user = null;
	// 数据库密码
	private static String password = null;

	private final static ThreadLocal<Connection> threadlocal = new ThreadLocal<Connection>();

	// 连接来源标记(是否使用数据库连接池)
	private static boolean use_datasource = false;
	private static DataSource datasource = null;
	static {
		try {
			// 生成资源文件的解析对象
			/*
			 * ResourceBundle res=ResourceBundle.getBundle(OPTION_FILE_NAME);
			 * url=res.getString("URL").trim();
			 * driver=res.getString("DRIVER").trim();
			 * user=res.getString("USERNAME").trim();
			 * password=res.getString("PASSWORD").trim();
			 */

			// 通过读取xml初始化数据库配置信息
			JdbcConfig jdbcConfig = XmlConfigReader.getInstance()
					.getJdbcConfig();
			String ds = jdbcConfig.getUse_datasource();
			if (ds.equals("true")) {
				use_datasource = true;
				String dsName = jdbcConfig.getDatasource_name();
				// 初始化上下文环境
				InitialContext ict = new InitialContext();
				// 通过JNDI生成数据源对象
				datasource = (DataSource) ict.lookup(dsName);
			} else {
				url = jdbcConfig.getUrl();
				driver = jdbcConfig.getDriverName();
				user = jdbcConfig.getUserName();
				password = jdbcConfig.getPassword();
				// 加载数据库的驱动
				Class.forName(driver);
				logs.info("数据库加载驱动!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 创建数据库的连接
	public static Connection getConnection() {
		// Connection conn = null;
		// 从当前线程当中获取Connection
		Connection conn = threadlocal.get();
		try {
			if (conn == null || conn.isClosed()) {
				if (use_datasource) {
					logs.info("通过连接池创建Connection");
					conn = datasource.getConnection();
				} else {
					logs.info("创建数据库连接");
					conn = DriverManager.getConnection(url, user, password);
				}
				// 将创建的Connection保存到当前线程当中
				threadlocal.set(conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void close(ResultSet rs) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void close(Statement pstm) {
		try {
			if (pstm != null)
				pstm.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 设置事务的提交方式
	public static void setAutoCommit(boolean b) {
		try {
			Connection conn = DBUtils.getConnection();
			conn.setAutoCommit(b);
			logs.info("设置事务");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 事务的回滚
	public static void rollback() {
		try {
			Connection conn = DBUtils.getConnection();
			if (!conn.getAutoCommit())
				conn.rollback();
			logs.info("回滚事务");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 事务的提交
	public static void commit() {
		try {
			Connection conn = DBUtils.getConnection();
			if (!conn.getAutoCommit())
				conn.commit();
			logs.info("提交事务");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void close() {
		try {
			Connection conn = (Connection) threadlocal.get();
			if (conn != null && !conn.isClosed()) {
				threadlocal.set(null);
				conn.close();
				logs.info("关闭数据库连接");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.out.println(DBUtils.getConnection());
	}
}
