package com.bjpowernode.dao;

import java.util.Map;

import com.bjpowernode.bean.Users;

public interface UserDao {
	//判断用户名是否存在
	public boolean isLoginUser(String username);
	
	//判断用户名和密码是否匹配
	public boolean isLoginUser(String username,String password);
	
	//用户登录成功修改登录时间和登录次数
	public void updateLogonTimeAndCount(String username);
	
	//登录成功获取用户信息
	public Users getUsersInfo(String username);
	
	//添加用户
	@SuppressWarnings("unchecked")
	public void addUser(Map dto);
}
