package com.bjpowernode.dao.impl;

import java.util.Map;
import java.util.Set;

import com.bjpowernode.bean.Users;
import com.bjpowernode.dao.UserDao;
import com.bjpowernode.system.OAUtils;

public class UserDaoImpl extends BaseDaoSupport implements UserDao {

	public boolean isLoginUser(String username) {
		String sql = "select count(*) from users where username=?";
		return OAUtils.bigDecimalToInt(queryForObject(sql, username)) > 0 ? true
				: false;
	}

	public boolean isLoginUser(String username, String password) {
		String sql = "select count(*) from users where username=? and password=?";
		int count = OAUtils.bigDecimalToInt(queryForObject(sql, username,
				password));
		return count > 0 ? true : false;
	}

	public void updateLogonTimeAndCount(String username) {
		String sql = "update users set logontime=?,lastlogontime=logontime,logoncount=logoncount+1 where username=?";
		update(sql, OAUtils.getSysCurrentTime(), username);
	}

	public Users getUsersInfo(String username) {
		String sql = "select id,username,nickname,lastlogontime,logoncount from users where username=?";
		return (Users) queryForObject(Users.class, sql, username);
	}

	// 添加用户
	@SuppressWarnings("unchecked")
	public void addUser(Map dto) {
		/*
		 * String sql =
		 * "inser into users(id, username, password, nickname, worktime, sex, depid, duty, email, mobile,homephone, workphone, fax, msn, birthday, httpaddress, address, content) values(req_oa.nextval,?)"
		 * ; insert(OAUtils.sqlConverter(sql, dto), dto.get("username"), dto
		 * .get("password"), dto.get("nickname"),
		 * OAUtils.stringToTime(dto.get("worktime").toString()), dto
		 * .get("sex"), OAUtils.stringToBigDecimal(dto.get("depid").toString()),
		 * dto.get("duty"), dto .get("email"), dto.get("mobile"),
		 * dto.get("homephone"), dto .get("workphone"), dto.get("fax"),
		 * dto.get("msn"), OAUtils.stringToTime(dto
		 * .get("birthday").toString()), dto.get("httpaddress"),
		 * dto.get("address"), dto.get("content")
		 * 
		 * );
		 */
		String sql = "insert into users values(?)";
		insertAdapter(sql, dto);
	}

	private void insertAdapter(String sql, Map<?, ?> map) {
		String[] sql1 = sql.split("values");
		String str1 = "id,";
		String str2 = "req_oa.nextval,";
		Set<?> set = map.keySet();
		// 获取参数map所有的key
		Object[] obj = set.toArray();
		// 定义数组给sql语句占位符赋值
		Object[] param = new Object[map.size()];
		for (int i = 0; i < obj.length; i++) {
			str1 = str1 + obj[i];
			str2 = str2 + "?";
			if (i != obj.length - 1) {
				str1 = str1 + ",";
				str2 = str2 + ",";
			}
			//如果是日期类型进行强制转换
			if (OAUtils.isValidDate(map.get(obj[i]).toString()))
				param[i] = OAUtils.stringToTime(map.get(obj[i]).toString());
			else
				param[i] = map.get(obj[i]);
		}
		sql = sql1[0] + "(" + str1 + ")" + " values" + "(" + str2 + ")";
		insert(sql, param);
	}
}
