package com.bjpowernode.test.factory;

public class BenzFactory implements CarFactoryInterface {

	public Car createCar() {
		return new Benz();
	}

}
