package com.bjpowernode.test.factory;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//传统模式
		/*Car car=new Audi();
		car.lock();
		car.gun();*/
		//简单工厂
		/*Car car=CarFactory.createCar("audi");
		car.lock();
		car.gun();*/
		//工厂方法
		Car car=new AudiFactory().createCar();
		car.lock();
		car.gun();
		 car=new VoloFactory().createCar();
		 car.lock();
		 car.gun();
	}

}
