package com.bjpowernode.test.factory;

public class AudiFactory implements CarFactoryInterface {

	public Car createCar() {
		
		return new Audi();
	}

}
