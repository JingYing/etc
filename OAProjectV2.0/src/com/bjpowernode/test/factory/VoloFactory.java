package com.bjpowernode.test.factory;

public class VoloFactory implements CarFactoryInterface {

	public Car createCar() {
		return new Volo();
	}

}
