package com.bjpowernode.test.factory;

import com.bjpowernode.system.ApplicationException;
//缺陷:生产其他产品时,需要修改工厂,没有实现OCP原则
public class CarFactory {
	public static Car createCar(String type){
		Car car=null;
		if(type.equals("audi"))
			car=new Audi();
		else if(type.equals("benz"))
			car=new Benz();
		else
			throw new ApplicationException("此工厂不生产该产品");
		return car;
	}
}
