package com.bjpowernode.file.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface FileService {
	//文件的上传
	public void fileUpload(HttpServletRequest request);
	
	public void insertFileInfo(String fileName, String content);
	
	//获取所有可供下载的资源
	public List<?> getAllFile();
}
