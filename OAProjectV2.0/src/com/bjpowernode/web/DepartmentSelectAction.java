package com.bjpowernode.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.service.DepartmentService;
import com.bjpowernode.service.impl.DepartmentServiceImpl;

public class DepartmentSelectAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7447620576074798208L;
	private DepartmentService depService;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		depService=new DepartmentServiceImpl();
		List<?> list=depService.getAllDep();
		request.setAttribute("deplist", list);
		request.getRequestDispatcher("/jsp/person_manage/bmtj.jsp").forward(request, response);
	}

}
