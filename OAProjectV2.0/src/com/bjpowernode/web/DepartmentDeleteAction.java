package com.bjpowernode.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.service.DepartmentService;
import com.bjpowernode.service.impl.DepartmentServiceImpl;

public class DepartmentDeleteAction extends BaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DepartmentService depService;
	@Override
	protected void execute(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		depService=new DepartmentServiceImpl();
		String args[] = req.getParameterValues("chbox");
		depService.deleteDep(args);
		success(req, resp);
	}
}
