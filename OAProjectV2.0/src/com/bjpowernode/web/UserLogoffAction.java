package com.bjpowernode.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserLogoffAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1720721622999401607L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getSession().setAttribute("users", null);
		String path=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
		response.sendRedirect(path+"/userGetCookie.action");
	}
	
}
