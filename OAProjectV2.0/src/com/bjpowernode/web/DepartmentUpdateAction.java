package com.bjpowernode.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.bean.Department;
import com.bjpowernode.service.DepartmentService;
import com.bjpowernode.service.impl.DepartmentServiceImpl;
import com.bjpowernode.system.OAUtils;

public class DepartmentUpdateAction extends BaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DepartmentService depService;
	@Override
	protected void execute(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		depService=new DepartmentServiceImpl();
		String action=req.getParameter("action");
		if("select".equals(action)){
			//获取修改的部门信息
			String id=req.getParameter("depid");
			Department dep=depService.getDepById(Integer.parseInt(id));
			//获取所有的上级部门
			List<?> list=depService.getAllDep();
			req.setAttribute("depbyid", dep);
			req.setAttribute("deplist", list);
			req.getRequestDispatcher("/jsp/person_manage/bmtj.jsp").forward(req, resp);
		}
		if("update".equals(action)){
			Department dep=new Department();
			dep.setId(OAUtils.stringToBigDecimal(req.getParameter("id")));
			dep.setDepname(req.getParameter("dname"));
			dep.setContent(req.getParameter("content"));
			dep.setEmail(req.getParameter("email"));
			dep.setPhone(req.getParameter("phone"));
			dep.setPid(OAUtils.stringToBigDecimal(req.getParameter("pid")));
			depService.updateDep(dep);
			success(req, resp);
		}
	}

}
