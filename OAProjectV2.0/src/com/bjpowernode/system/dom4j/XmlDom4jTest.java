package com.bjpowernode.system.dom4j;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class XmlDom4jTest {
	//通过dom4j来创建xml文档
	public static void createXML()throws Exception{
		//创建文本文档对象
		Document document=DocumentHelper.createDocument();
		//创建跟节点
		Element root=DocumentHelper.createElement("动力节点");
		//将根节点添加到文档
		document.setRootElement(root);
		//创建子节点
		Element school=DocumentHelper.createElement("学校详情");
		//创建子节点
		Element cls=DocumentHelper.createElement("班级");
		
		Element address=DocumentHelper.createElement("学校地址");
		Element aentry=null;
		int i=0;
		//添加班级子节点元素
		for(;i<20;i++){
			aentry=DocumentHelper.createElement("学生");
			aentry.addAttribute("id", ""+i);
			aentry.addAttribute("date", "2000-01-01");
			aentry.addAttribute("description", "学生"+i);
			aentry.setText("学生"+i);
			cls.add(aentry);
		}
		address.addAttribute("地址", "北京金苑大厦");
		address.addAttribute("成立时间", "2008-09-01");
		school.add(address);
		root.add(school);
		root.add(cls);
		//输入格式设置
		OutputFormat outFmt=OutputFormat.createPrettyPrint();
		outFmt.setEncoding("GB18030");
		XMLWriter xmlWriter=new XMLWriter(new FileWriter("d:/dom4j.xml"),outFmt);
		xmlWriter.write(document);
		xmlWriter.close();
	}
	
	//现有xml文档添加元素
	public static void insertXML()throws Exception{
		//创建解析器
		SAXReader sr=new SAXReader();
		//创建文档
		Document document=sr.read(new FileReader("d:/dom4j.xml"));
		Element school=document.getRootElement().element("学校详情");
		Element address=DocumentHelper.createElement("学校地址");
		address.addAttribute("地址", "北京交通大厦");
		address.addAttribute("成立时间", "2011-01-01");
		school.add(address);
		OutputFormat outFmt=OutputFormat.createPrettyPrint();
		outFmt.setEncoding("GB18030");
		XMLWriter xmlWriter=new XMLWriter(new FileWriter("d:/dom4j.xml"),outFmt);
		xmlWriter.write(document);
		xmlWriter.close();
		
	}
	
	public static void getXML()throws Exception{
		//创建解析器
		SAXReader sr=new SAXReader();
		//创建文档
		Document document=sr.read(new FileReader("d:/dom4j.xml"));
		List list=document.selectNodes("/动力节点/班级/学生");
		for(int i=0;i<list.size();i++){
			StringBuilder str=new StringBuilder();
			str.append("学生:"+((Element)list.get(i)).getTextTrim()+", 生日:"+((Element)list.get(i)).attributeValue("date"));
			System.out.println(str.toString());
		}
	}
	public static void main(String[] args) {
		try {
			//createXML();
			//insertXML();
			getXML();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
