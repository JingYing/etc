package com.bjpowernode.system;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class InitServlet extends HttpServlet {

	@Override
	public void init() throws ServletException {
		SysBeanFactory.getInstance().createBean();
	}
}
