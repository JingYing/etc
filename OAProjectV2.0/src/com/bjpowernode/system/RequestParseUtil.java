package com.bjpowernode.system;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class RequestParseUtil {
	// 将请求对象转换为表单对象(将页面提交的数据封装到Formbean当中)
	public static void requestDataToFormBean(HttpServletRequest request,
			Object obj) {
		try {
			// 获取表单对象的所有属性
			Class formClass = obj.getClass();
			// 获取对象的所有set方法
			Method[] ms = formClass.getMethods();
			// 循环处理
			for (Method m : ms) {
				String methodName = m.getName();
				if (methodName.startsWith("set")) {
					// set方法
					// 获取属性的名称
					String fieldName = methodName.substring(3, 4).toLowerCase()
							+ methodName.substring(4);
					// 获取请求的参数
					String param = request.getParameter(fieldName);
					if (param != null && !"".equals(param.trim())) {
						// 请求参数存在

						// 获取方法的参数类型
						Class[] paramClass = m.getParameterTypes();
						if (paramClass[0].isArray()) {
							// 参数类型是数组的情况下
						} else {
							// 参数类型不是数组的情况
							Class pClass = paramClass[0];
							if (pClass == Integer.class || pClass == int.class) {
								// 执行set方法
								m.invoke(obj, Integer.parseInt(param));
							} else if (pClass == BigDecimal.class) {
								m
										.invoke(obj, OAUtils
												.stringToBigDecimal(param));
							} else if (pClass == Timestamp.class) {
								m.invoke(obj, OAUtils.stringToTime(param));
							} else {
								m.invoke(obj, param);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/*
	 *  将FormBean转换成DTO数据传输对象
	 *  obj:表示需要转换的FormBean
	 *  nullable:表示是否保存null值
	 */
	public static Map<String, String> makeFormToDTO(Object obj, boolean nullable) {
		Map<String, String> paramMap = new HashMap<String, String>();
		try {
			Class formClass = obj.getClass();
			// 获取对象所有的get方法
			Method[] ms = formClass.getDeclaredMethods();
			for (Method m : ms) {
				String methodName = m.getName();
				if (methodName.startsWith("get")) {
					String fieldName = methodName.substring(3, 4).toLowerCase()
							+ methodName.substring(4);
					// 相当于调用get方法
					Object j = m.invoke(obj);
					// 是否过滤值为null
					if (nullable) {
						//String fieldValue = (String) j;
						String fieldValue=j.toString();
						paramMap.put(fieldName, fieldValue);
					} else {
						if (j != null && !"".equals(j.toString())) {
							String fieldValue = j.toString();
							paramMap.put(fieldName, fieldValue);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paramMap;
	}
}
