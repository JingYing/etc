package com.bjpowernode.system.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class SysServiceImplProxy implements InvocationHandler {

	private Object targetObj;

	public Object crateProxyInstance(Object targetObj) {
		this.targetObj = targetObj;
		return Proxy.newProxyInstance(targetObj.getClass().getClassLoader(), targetObj
				.getClass().getInterfaces(), this);
	}

	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		Object ret=null;
		System.out.println("method:"+method.getName());
		for(int i=0;i<args.length;i++){
			System.out.println(args[i]);
		}
		System.out.println("proxy:"+proxy.getClass().getName());
		ret=method.invoke(targetObj, args);
		return ret;
	}
	
	private void log(){
		System.out.println("��־��¼");
	}
}
