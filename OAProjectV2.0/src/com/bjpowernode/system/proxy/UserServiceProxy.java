package com.bjpowernode.system.proxy;

public class UserServiceProxy implements UserService {

	private UserService userService;
	public UserServiceProxy(UserService userService){
		this.userService=userService;
	}
	public void addUser(String username, String password) {
		log();
		//实现目标对象的方法
		userService.addUser(username, password);
		log();
	}

	public String getUser(String username) {
		return userService.getUser(username);
	}

	private void log(){
		System.out.println("日志记录");
	}
}
