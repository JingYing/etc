package com.bjpowernode.system.proxy;

public interface UserService {
	public  void addUser(String username,String password);
	public String getUser(String username);
}
