package com.bjpowernode.system;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckSessionFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;

		// 获取用户请求的URI
		String requestURI = request.getRequestURI().substring(
				request.getRequestURI().indexOf("/", 1),
				request.getRequestURI().length());
		System.out.println(requestURI);
		
		if(!"/jsp/login.jsp".equals(requestURI)
				&& !"/".equals(requestURI)
				&& !"/userGetCookie.action".equals(requestURI)
				&& !"/user.action".equals(requestURI)
		){
			Object user=request.getSession().getAttribute("users");
			//判断如果没有取到用户信息,就跳转到登录页面
			if(user==null){
				response.sendRedirect(request.getContextPath()+"/userGetCookie.action");
				return;
			}
		}
		chain.doFilter(arg0, arg1);
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

}
