package com.bjpowernode.workmanage.dao;

import java.util.Map;

import com.bjpowernode.dao.impl.BaseDaoSupport;
import com.bjpowernode.system.OAUtils;

public class WorkDaoImpl extends BaseDaoSupport implements WorkDao {

	public void addWork(Map<?, ?> dto) {
		String sql="insert into workmanage values(seq_oa.nextval,?,?,?)";
		insert(sql,dto.get("title"),dto.get("content"),OAUtils.getSysCurrentTime());
	}
	
	//添加日报日志
	public void addLog(String loginfo){
		String sql="insert into t_logger values(?,seq_oa.nextval)";
		insert(sql,loginfo);
	}

}
