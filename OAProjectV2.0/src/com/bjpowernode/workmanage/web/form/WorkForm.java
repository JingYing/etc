package com.bjpowernode.workmanage.web.form;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class WorkForm {
	private BigDecimal id;
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	private String title;
	private String content;
	private Timestamp time;
}
