package com.bjpowernode.workmanage.web;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.system.RequestParseUtil;
import com.bjpowernode.workmanage.service.WorkService;
import com.bjpowernode.workmanage.service.WorkServiceImpl;
import com.bjpowernode.workmanage.web.form.WorkForm;

public class WorkManageAction extends BaseAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WorkService workService;
	public void add(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String path="/jsp/success.jsp";
		WorkForm workForm=new WorkForm();
		RequestParseUtil.requestDataToFormBean(req, workForm);
		Map<?, ?> dto=RequestParseUtil.makeFormToDTO(workForm, false);
		workService=new WorkServiceImpl();
		try{
			workService.addWork(dto);
		}
		catch(Exception e){
			e.printStackTrace();
			path="/jsp/error.jsp";
			req.setAttribute("message", e.getMessage());
		}
		
		req.getRequestDispatcher(path).forward(req, resp);
	}
}
