package com.bjpowernode.workmanage.service;

import java.util.Map;

import com.bjpowernode.dao.conn.DBUtils;
import com.bjpowernode.system.ApplicationException;
import com.bjpowernode.workmanage.dao.WorkDao;
import com.bjpowernode.workmanage.dao.WorkDaoImpl;

public class WorkServiceImpl implements WorkService {

	private WorkDao workDao = new WorkDaoImpl();

	public void addWork(Map<?, ?> dto) throws Exception {
		try {
			DBUtils.setAutoCommit(false);
			// 添加工作日报
			workDao.addWork(dto);
			// 添加日报日志
			workDao.addLog("添加工作日报");
			DBUtils.commit();
		} catch (Exception e) {
			DBUtils.rollback();
			e.printStackTrace();
			throw new ApplicationException("工作日报保存失败");
		} finally {
			//DBUtils.getConnection().close();
		}
	}
}
