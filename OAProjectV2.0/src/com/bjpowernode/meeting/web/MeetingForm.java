
package com.bjpowernode.meeting.web;

import java.math.BigDecimal;

/**
 * 解析页面传递过来的request, 将相关属性设到formbean中, 
 * 再将该formbean转换成dto, 在各层之间传送. DTO一般不用POJO, 而用MAP代替以减少DTO数量
 * @author JingYing 2012-12-24
 */
public class MeetingForm {
	private BigDecimal id;
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getRoom_name() {
		return room_name;
	}
	public void setRoom_name(String roomName) {
		room_name = roomName;
	}
	public String getRoom_content() {
		return room_content;
	}
	public void setRoom_content(String roomContent) {
		room_content = roomContent;
	}
	public String getRoom_pic() {
		return room_pic;
	}
	public void setRoom_pic(String roomPic) {
		room_pic = roomPic;
	}
	private String room_name;
	private String room_content;
	private String room_pic;
}
