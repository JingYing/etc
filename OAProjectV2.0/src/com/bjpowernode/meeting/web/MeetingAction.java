package com.bjpowernode.meeting.web;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.bjpowernode.meeting.service.MeetingService;
import com.bjpowernode.system.RequestParseUtil;
import com.bjpowernode.workmanage.web.BaseAction;

public class MeetingAction extends BaseAction {
	
	private Logger logs=Logger.getLogger(MeetingAction.class);
	private MeetingService meetingService;
	
	public void setMeetingService(MeetingService meetingService) {
		this.meetingService = meetingService;
	}

	public void add(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		MeetingForm mForm=new MeetingForm();
		RequestParseUtil.requestDataToFormBean(req, mForm);
		Map dto=RequestParseUtil.makeFormToDTO(mForm, false);
		String path="/jsp/success.jsp";
		try{
			meetingService.addMeetingRoom(dto);
		}
		catch(Exception e){
			path="/jsp/error.jsp";
			req.setAttribute("message", e.getMessage());
		}
		forward(path, req, resp);
	}
	
	public void zcrtj(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException{
		//获取所有的部门
		List depList=meetingService.getAllDep();
		req.setAttribute("deplist", depList);
		//获取所有的员工
		List userList=meetingService.getAllUser();
		req.setAttribute("userlist", userList);
		forward("/jsp/meeting/zcrtj.jsp", req, resp);
	}
	
	public void yhrytj(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException{
		List userList=meetingService.getAllUser();
		req.setAttribute("userlist", userList);
		forward("/jsp/meeting/yhrytj.jsp", req, resp);
	}
}
