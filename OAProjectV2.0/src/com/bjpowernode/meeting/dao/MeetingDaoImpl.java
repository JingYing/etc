package com.bjpowernode.meeting.dao;

import java.util.List;
import java.util.Map;

import com.bjpowernode.dao.impl.BaseDaoSupport;

public class MeetingDaoImpl extends BaseDaoSupport implements MeetingDao {

	public void addMeetingRoom(Map dto) {
		String sql="insert into meetng_room(id,room_name,room_content) values(seq_oa.nextval,?,?)";
		insert(sql, dto.get("room_name"),dto.get("room_content"));
	}
	
	public List getAllDep(){
		String sql="select id,depname from department";
		List list=queryForListByMap(sql);
		return list;
	}
	public List getAllUser(){
		String sql="select id,nickname from users";
		List list=queryForListByMap(sql);
		return list;
	}
}
