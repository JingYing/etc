<%@ page language="java" import="java.util.*" pageEncoding="GB18030"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>添加会议主持人_纪要人</title>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">
<script language="javascript">
function makeValue()
{
	
}
function selectPerson(opt)
{
	//得到选中项的索引
	index=opt.selectedIndex;
	document.getElementById("txtName").value=opt[index].text;
	document.getElementById("idName").value=opt[index].value;
	opener.document.getElementById("userid").value=opt[index].value;
	opener.document.getElementById("nickname").value=opt[index].text;
}
function clickOk()
{
	var name=document.getElementById("txtName").value;
	if(name==null || name=="")
	{
		alert('请填写会议主持人');
		document.getElementById("txtName").focus();
		return false;
	}
	window.returnValue = name;
	window.close();
}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="2%" valign="middle" background="${pageContext.request.contextPath}/images/bg_03.gif">&nbsp;</td>
    <td width="2%" valign="middle" background="${pageContext.request.contextPath}/images/bg_03.gif"><img src="${pageContext.request.contextPath}/images/main_28.gif" width="9" height="9" align="absmiddle"></td>
    <td height="30" valign="middle" background="${pageContext.request.contextPath}/images/bg_03.gif"><div align="left"><font color="#FFFFFF">添加会议主持人</font></div></td>
  </tr>
</table><br>
<table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="td_page"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td width="15%" height="30"><div align="center">部门选择&nbsp;</div></td>
          <td height="30"><span class="style4">
            <select name="select" id="select" class="box" style="width:170px " onChange="makeValue()">
              <option value="0" selected>部门选择</option>
              	<c:forEach items="${deplist}" var="dep">
              		 <option value="${dep.ID }">${dep.DEPNAME }</option>
              	</c:forEach>
            </select>
          </span></td>
        </tr>
        <tr>
			<td>&nbsp; <div align="center"></div></td>
          <td>
          <select name="selectBasRight" size="16" style="width:95% " id="dept" onChange="selectPerson(this)">
          		<c:forEach items="${userlist}" var="user">
              		 <option value="${user.ID }">${user.NICKNAME }</option>
              	</c:forEach>
          </select></td>
        </tr>
        <tr>
          <td height="30"><div align="center">会议主持人</div></td>
          <td>
          <input name="idName" type="hidden" class="input" id="idName" style="width:95% ">
          <input name="txtName" type="text" class="input" id="txtName" style="width:95% "></td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<table width="95%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="td_page">
      <div align="center">
        <input name="Submit" type="button" class="buttonface02" value="  确认  " onClick="clickOk()">
        &nbsp;
        <input name="Submit" type="button" class="buttonface02" value="  关闭  " onClick="window.close()">
      </div></td></tr>
</table>
</body>
</html>
