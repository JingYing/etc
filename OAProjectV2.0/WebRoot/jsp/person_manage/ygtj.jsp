<%@page contentType="text/html;charset=GB18030"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		<title>员工管理</title>
		<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" language="JavaScript1.2"
			src="${pageContext.request.contextPath}/js/Calendar1.js"></script>
		<script>
function windowOpen(theURL,winName,features,width,hight,scrollbars,top,left) 
{
  var parameter="top="+top+",left="+left+",width="+width+",height="+hight;
  if(scrollbars=="no")
 {parameter+=",scrollbars=no";}
  else
 {parameter+=",scrollbars=yes";}
  window.open(theURL,winName,parameter);
}
function tianjia()
{

}
</script>
		<script type="text/javascript">
			var xmlHttp;
			function isUser(){
				//1、创建XMLHttpRequest对象
				if(window.XMLHttpRequest){
					//如果当前浏览器是IE7\IE8,FireFox.......
					xmlHttp=new XMLHttpRequest();
					if(xmlHttp.overrideMimeType){
						xmlHttp.overrideMimeType("text/xml");
					}
				}else if(window.ActiveXObject){
					//低版本的浏览器 
					var activexName=["MSXML2.XMLHTTP.6.0","MSXML2.XMLHTTP.5.0",
									 "MSXML2.XMLHTTP.4.0","MSXML2.XMLHTTP.3.0",
									 "MSXML2.XMLHTTP","Miscrosoft.XMLHTTP"];
					for(i=0;i<activexName.length;i++){
						try{
							xmlHttp=new ActiveXObject(activexName[i]);
						}catch(e){}
						
					}
				}
				if(xmlHttp==undefined || xmlHttp==null){
					alert("当前浏览器不支持Ajax!");
					return;
				}
				//2、注册回调方法，获取服务器 端的数据赋给XMLHttpRequest，然后调用回调方法。
				xmlHttp.onreadystatechange=callback;
				var uname=document.getElementById("username").value;
				
				//3、设置和服务器端交互参数 
				var url="${pageContext.request.contextPath}/userManage.action?action=isUser";
				xmlHttp.open("POST",url,true);
				xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
				//4、设置向服务器端发送的数据 
				xmlHttp.send("uname="+uname);
			}
		
			function callback(){
				//5、判断和服务器端德交互是完成的，是否正确返回了数据 
				if(xmlHttp.readyState==4){
					//表示交互完成 
					if(xmlHttp.status==200){
						//表示服务器的响应代码是200,正确返回了数据 
						//接收纯文本方式 
						var message=xmlHttp.responseText;
						var div=document.getElementById('showmessage');
						div.innerHTML=message;
					}
				}
			}
		</script>

	
	</head>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<form method="post" name="form1" action="${pageContext.request.contextPath}/userManage.action?action=addUser">
			<center>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" align="center" valign="bottom" class="td06">
							<table width="100%" border="0" align="center" cellpadding="0"
								cellspacing="0">
								<tr>
									<td width="2%" valign="middle"
										background="${pageContext.request.contextPath}/images/bg_03.gif">
										&nbsp;
									</td>
									<td width="2%" valign="middle"
										background="${pageContext.request.contextPath}/images/bg_03.gif">
										<img src="${pageContext.request.contextPath}/images/main_28.gif" width="9" height="9"
											align="absmiddle">
									</td>
									<td height="30" valign="middle"
										background="${pageContext.request.contextPath}/images/bg_03.gif">
										<div align="left">
											<font color="#FFFFFF">员工添加</font>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br>
				<table width="95%" border="0" cellpadding="0" cellspacing="0"
					class="td_page"">
					<tr>
						<td width="10%" height="30" align="left">
							入职时间
						</td>
						<td width="40%" height="30">
							<input name="worktime" type="text" class="input"
								style="width: 95%" onclick="calendar();">
						</td>
						<td width="10%" align="left">
							&nbsp;
						</td>
						<td width="40%" align="left">
							<div id="showmessage"></div>
						</td>
					</tr>
					<tr>
						<td width="10%" height="30" align="left">
							员工姓名
						</td>
						<td width="40%" height="30">
							<input name="nickname" type="text" class="input"
								style="width: 95%">
						</td>
						<td width="10%" align="left">
							性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别
						</td>
						<td width="40%">
							<select name="sex" size="1" class="box" style="width: 95%">
								<option value="1" selected>
									男
								</option>
								<option value="2">
									女
								</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="10%" height="30" align="left">
							登陆帐号
						</td>
						<td width="40%" height="30">
							<input name="username" type="text" class="input"
								style="width: 95%" onchange="password.value=this.value;if(this.value!=''){isUser();}else{showmessage.innerHTML='';}">
						</td>
						<td width="10%" align="left">
							登陆密码
						</td>
						<td width="40%" valign="middle">
							<input name="password" type="password" class="input"
								style="width: 95%">
						</td>
					</tr>
					<tr>
						<td width="10%" height="30" align="left">
							所属部门
						</td>
						<td width="40%" height="30">
							<font> <select name="depid" size="1" class="box"
									style="width: 95%">
									<option value="1" selected>
										财务部
									</option>
									<option value="2">
										市场部
									</option>
								</select> </font>
						</td>
						<td width="10%" align="left">
							职 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;务
						</td>
						<td width="40%" valign="middle">
							<select name="duty" size="1" class="box" style="width: 95%">
								<option value="1" selected>
									经理
								</option>
								<option value="2">
									主任
								</option>
								<option value="3">
									一般员工
								</option>
							</select>
						</td>
					</tr>
					<!--  <tr>
      <td width="10%" height="30" align="left"> 直属上级</td>
      <td width="40%" height="30"><font>
        <select name="fldSex" size="1" class="box" style="width:95%">
          <option value="经理" selected>经理　　　　　　　　　　　　　</option>
          <option value="主任">主任　　　　　　　　　　　　</option>
        </select>
      </font></td>
      <td width="10%" align="left"> 内部邮件</td>
      <td width="40%" valign="middle"><input name="queryName" type="password" class="input" style="width:95% ">
      </td>
    </tr> -->
					<tr>
						<td width="10%" height="30" align="left">
							外部邮件
						</td>
						<td width="40%" height="30">
							<font> <input name="email" type="text"
									class="input" style="width: 95%"> </font>
						</td>
						<td width="10%" align="left">
							移动电话
						</td>
						<td width="40%" valign="middle">
							<input name="mobile" type="text" class="input"
								style="width: 95%">
						</td>
					</tr>
					<tr>
						<td width="10%" height="30" align="left">
							家庭电话
						</td>
						<td width="40%" height="30">
							<font> <input name="homephone" type="text"
									class="input" style="width: 95%"> </font>
						</td>
						<td width="10%" align="left">
							办公电话
						</td>
						<td width="40%" valign="middle">
							<input name="workphone" type="text" class="input"
								style="width: 95%">
						</td>
					</tr>
					<tr>
						<td width="10%" height="30" align="left">
							传真号码
						</td>
						<td width="40%" height="30">
							<font> <input name="fax" type="text"
									class="input" style="width: 95%"> </font>
						</td>
						<td width="10%" align="left">
							MSN
						</td>
						<td width="40%" valign="middle">
							<input name="msn" type="text" class="input"
								style="width: 95%">
						</td>
					</tr>
					<tr>
						<td width="10%" height="30" align="left">
							生日
						</td>
						<td width="40%" height="30">
							<font> <input name="birthday" type="text"
									class="input" style="width: 95%" onclick="calendar();"> </font>
						</td>
						<td width="10%" align="left">
							个人主页
						</td>
						<td width="40%" valign="middle">
							<input name="httpaddress" type="text" class="input"
								style="width: 95%">
						</td>
					</tr>
					<tr>
						<td height="30" colspan="4" align="left">
							<font>&nbsp; </font>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="10%">
										通讯地址
									</td>
									<td>
										<font> <input name="address" type="text"
												class="input" style="width: 98%"> </font>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="4" align="left">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="10%">
										备注
									</td>
									<td>
										<textarea name="content" rows="5" class="box"
											style="width: 98%"></textarea>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br>
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="right" class="td_page">
							<div>
								<input name="Submit" type="submit" class="buttonface02"
									onClick="tianjia()" value="  添加  ">
								&nbsp;
								<input name="Submit" type="reset" class="buttonface02"
									value="  重置  ">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="&#21592;&#24037;&#31649;&#29702;.htm">返回上一页</a>
								&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</td>
					</tr>
				</table>
			</center>
		</form>
	</body>
</html>



