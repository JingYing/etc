package com.paipai.cms.common.jqgrid;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.paipai.cms.common.PageModel;

/**
 * 把实体集合序列化jqgrid所要求的字符串.
 */
public class JqgridSerializer	{
	
	/**
	 * 主算法, 将pagemodel对象转化为jqgrid字符串
	 * total主页数, page当前页, record记录数
	 * id主键
	 * {"total":"1","page":"1","records":"2",
	 * "rows":[
	 * 		{"id":"1","cell":["属性1","属性2","属性3","属性4"]},
	 * 		{"id":"2","cell":["属性a","属性b","属性c","属性d"]}]
	 * }
	 * @param pagemodel
	 * @return
	 */
	public <T> String toMaingrid(PageModel<T> pageModel, ColumnMapper<T> columnMapper)	{
		JsonArray jsonArray = new JsonArray();
		for(T t : pageModel.getList()){
			if(t == null) break;
			JsonObject json = new JsonObject();
			List<String> columns = columnMapper.mapColumn(t);
			json.addProperty("id", columnMapper.getRowId(t));
			
			JsonArray ja = new JsonArray();
			for(String s : columns)	{
				if(s == null) s = "";
				ja.add(new JsonPrimitive(s));
			}
			json.add("cell", ja);
			
			jsonArray.add(json);
		}
		
		JsonObject finalJson = new JsonObject();
		if((pageModel.getTotal() % pageModel.getPageSize()) != 0)
			finalJson.addProperty("total", (pageModel.getTotal()/pageModel.getPageSize() + 1) + "");
		else
			finalJson.addProperty("total", (pageModel.getTotal()/pageModel.getPageSize()) + "");
		finalJson.addProperty("page", pageModel.getPageNo() + "");
		finalJson.addProperty("records", pageModel.getTotal() + "");
		finalJson.add("rows", jsonArray);
		
		return finalJson.toString();
	}
	
	
	/**
	 * 生成subGrid用字符串, 格式如下
	 * {"rows": [
	 * 		{"id":"1", "cell":["拉菲正牌-1999",5,150.0,750.0]},
	 * 		{"id":"2", "cell":["ww正牌-2000",3,250.0,750.0]}  ]
	 * }
	 * @param <T>
	 * @param list
	 * @return
	 */
	public <T> String toSubgrid(PageModel<T> pageModel, ColumnMapper<T> columnMapper)	{
		JsonObject json = new JsonParser().parse(toMaingrid(pageModel, columnMapper)).getAsJsonObject();
		json.remove("total");
		json.remove("page");
		json.remove("records");
		return json.toString();
	}

	/**
	 * 生成subGrid用字符串, 格式如下
	 * {"rows": [
	 * 		{"id":"1", "cell":["拉菲正牌-1999",5,150.0,750.0]},
	 * 		{"id":"2", "cell":["ww正牌-2000",3,250.0,750.0]}  ]
	 * }
	 * @param <T>
	 * @param list
	 * @return
	 */
	public <T> String toSubgrid(List<T> list, ColumnMapper<T> columnMapper)	{
		JsonObject json = new JsonObject();
		JsonArray finalArray = new JsonArray();
		for(T t : list)	{
			JsonObject jo = new JsonObject();
			List<String> columns = columnMapper.mapColumn(t);
			
			JsonArray array = new JsonArray();
			for(String s : columns)	{
				array.add(new JsonPrimitive(s));
			}
			jo.addProperty("id", columnMapper.getRowId(t));
			jo.add("cell", array);
			finalArray.add(jo);
		}
		json.add("rows", finalArray);
		return json.toString();
	}
	
//	public static void main(String[] args) {
//	//调用方式:
//		PageModel p = new PageModel();
//		String json = new JqgridSerializer().toMaingrid(p, new ColumnMapper<Example>() {
//
//			@Override
//			public List<String> mapColumn(Example t) {
//				List<String> l = new ArrayList<String>();
//				l.add(t.getKey());
//				l.add(t.getValue());
//				return l;
//			}
//
//			@Override
//			public String getRowId(Example t) {
//				return t.getId()+"";
//			}
//		});
//		System.out.println(json);
//	}
}
