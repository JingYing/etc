package com.jd.common.sso;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.LongBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * 对.net 加密的密文进行解密
 * 
 * @author zhuyanbo@360buy.com
 * 
 * @author xiaofei
 *         <p>
 *         port from
 *         com.jd.common.struts.interceptor.dotnet.DotnetAuthenticationUtil,
 *         just rename package name.
 * 
 * @see com.jd.common.struts.interceptor.dotnet.DotnetAuthenticationUtil
 * 
 */
public class DotnetAuthenticationUtil {
    private static final String ENCODING = "UTF-16LE"; // @@

    public static byte[] hexToByte(String s) throws IOException {
        int i = s.length() / 2;
        byte abyte0[] = new byte[i];
        int j = 0;
        if (s.length() % 2 != 0)
            throw new IOException(
                    "hexadecimal string with odd number of characters");
        for (int k = 0; k < i; k++) {
            char c = s.charAt(j++);
            int l = "0123456789abcdef0123456789ABCDEF".indexOf(c);
            if (l == -1)
                throw new IOException(
                        "hexadecimal string contains non hex character");
            int i1 = (l & 0xf) << 4;
            c = s.charAt(j++);
            l = "0123456789abcdef0123456789ABCDEF".indexOf(c);
            i1 += l & 0xf;
            abyte0[k] = (byte) i1;
        }

        return abyte0;
    }

    public static byte[] decrypt(String str, String key)
            throws Exception {
        if (null == str || str.trim().length() < 1) {
            return null;
        }

        byte[] keybytes = hexToByte(key);
        byte[] ivbytes = new byte[16];

        SecretKeySpec skeySpec = new SecretKeySpec(keybytes, "AES");

        IvParameterSpec iv = new IvParameterSpec(ivbytes);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

        byte[] encrypted1 = hexToByte(str);

        byte[] original = cipher.doFinal(encrypted1);

        return original;
    }

    // 转换成十六进制字符串

    public static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";

        for (int n = 0; n < b.length; n++) {
            stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1)
                hs = hs + "0" + stmp;
            else
                hs = hs + stmp;

        }
        return hs.toUpperCase();
    }

    public static String encrypt(String str, String key)
            throws Exception {
        if (null == str || str.trim().length() < 1) {
            return null;
        }

        byte[] keybytes = hexToByte(key);
        byte[] ivbytes = new byte[16];

        SecretKeySpec skeySpec = new SecretKeySpec(keybytes, "AES");

        IvParameterSpec iv = new IvParameterSpec(ivbytes);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

        byte[] encrypted1 = str.getBytes(ENCODING);

        byte[] original = cipher.doFinal(encrypted1);

        String originalString = byte2hex(original);
        return originalString;

    }

    public static byte[] makeTicketBlob(byte[] random, byte version,
            byte[] username,byte[] issueDate, byte persist,
            byte[] expires, byte[] userdata, byte[] appPath) throws Exception {
        if (random.length != 8)
            throw new Exception("random");
        byte[] buffer = null;
        int bufferLength = 7;
        bufferLength += random.length;
        bufferLength += username.length;
        bufferLength += issueDate.length;
        bufferLength += expires.length;
        if (userdata != null)
            bufferLength += userdata.length;
        bufferLength += appPath.length;
        buffer = new byte[bufferLength];
        int pos = 0;
        // int len = 0;

        System.arraycopy(random, 0, buffer, pos, random.length);
        pos += random.length;
        buffer[pos] = version;
        pos++;
        System.arraycopy(username, 0, buffer, pos, username.length);
        pos += username.length;
        buffer[pos] = 0;
        pos++;
        buffer[pos] = 0;
        pos++;
        System.arraycopy(issueDate, 0, buffer, pos, issueDate.length);
        pos += issueDate.length;
        buffer[pos] = persist;
        pos++;
        System.arraycopy(expires, 0, buffer, pos, expires.length);
        pos += expires.length;
        if (userdata != null) {
            System.arraycopy(userdata, 0, buffer, pos, userdata.length);
            pos += userdata.length;
        }
        buffer[pos] = 0;
        pos++;
        buffer[pos] = 0;
        pos++;
        System.arraycopy(appPath, 0, buffer, pos, appPath.length);
        pos += appPath.length;
        buffer[pos] = 0;

        return buffer;
    }

    /**
     * 从解密后字串中得出utf16le编码内容。utf16le双字节存储，以连续0000结尾
     * 
     * @param ticketBytes
     * @param start
     *            ticketBytes的起始位置
     * @return 单独内容。长度
     */
    private static byte[] readUtf16le(byte[] ticketBytes, int start) {
        int end = checkUtf16leEnd(ticketBytes, start);
        if (end < 0) {
            // 例外。。。怎么能传入这样的参数
            return new byte[0];
        } else {
            int len = end - start;
            byte[] desc = new byte[len];
            System.arraycopy(ticketBytes, start, desc, 0, len);
            return desc;
        }
    }

    private static int checkUtf16leEnd(byte[] ticketBytes, int start) {
        int end = ticketBytes.length; // 默认到末尾

        for (int i = start; i < ticketBytes.length - 1; i += 2) {
            byte i1 = ticketBytes[i];
            byte i2 = ticketBytes[i + 1];
            if (i1 == 0 && i2 == 0) {
                end = i;
                break;
            }
        }
        return end;
    }

    public static DotnetAuthenticationTicket parseTicket(byte[] ticketBytes)
            throws Exception {

        DotnetAuthenticationTicket ticket = null;
        int pos = 8;

        int version = (int) ticketBytes[pos];
        pos++;

        // 分析出用户名。utf16-le编码是两字节。所以结束也是0000
        byte[] usernames = readUtf16le(ticketBytes, pos);
        pos += usernames.length + 2;

        // 分析创建时间
        byte[] createTimeBytes = new byte[8];
        System.arraycopy(ticketBytes, pos, createTimeBytes, 0,
                createTimeBytes.length);
        pos += createTimeBytes.length;

        // 分析是否持久
        int isPersist = (int) ticketBytes[pos];
        pos++;

        // 分析出过期时间
        byte[] expireTimeBytes = new byte[8];
        System.arraycopy(ticketBytes, pos, expireTimeBytes, 0,
                expireTimeBytes.length);
        pos += expireTimeBytes.length;

        // 分析出userdata
        byte[] userdatas = readUtf16le(ticketBytes, pos);
        pos += userdatas.length + 2;

        // 分析出路径

        byte[] paths = readUtf16le(ticketBytes, pos);
        pos += paths.length + 2;

        String username = new String(usernames, ENCODING);
        String userData = new String(userdatas, ENCODING);
        String appPath = new String(paths, ENCODING);
        Date createTime = ByteArrayToDate(createTimeBytes);
        Date expiration = ByteArrayToDate(expireTimeBytes);
        boolean isPersistent = isPersist == 1;
        ticket = new DotnetAuthenticationTicket(username, userData, appPath,
                createTime, expiration, version, isPersistent);
        return ticket;
    }

    private static byte[] md5HashForData(byte[] buf, int start, int length,
            String validationKeyString) throws NoSuchAlgorithmException,
            IOException {

        java.security.MessageDigest md = java.security.MessageDigest
                .getInstance("MD5");
        md.getDigestLength();
        byte[] validationKeyBytes = hexToByte(validationKeyString);
        int num = length + validationKeyBytes.length; //

        byte[] dst = new byte[num];
        System.arraycopy(buf, start, dst, 0, length);

        System.arraycopy(validationKeyBytes, 0, dst, length,
                validationKeyBytes.length);
        md.update(dst);

        return md.digest();
    }

    private static boolean isSignatureVerified(byte[] decryptedData,
            String validationKeyString) throws IOException,
            NoSuchAlgorithmException {
        byte[] signatureeBytes = md5HashForData(decryptedData, 0,
                decryptedData.length - 20, validationKeyString);
        if (signatureeBytes.length < 20) {
            byte[] dst = new byte[20];
            System.arraycopy(signatureeBytes, 0, dst, 0, signatureeBytes.length);
            signatureeBytes = dst;
        }

        for (int i = 0; i < 20; i++) {
            if (signatureeBytes[i] != decryptedData[(decryptedData.length - 20)
                    + i])
                return false;
        }
        return true;
    }

    public static DotnetAuthenticationTicket getFormsAuthenticationTicket(
            String desc, String key) throws Exception {
        return parseTicket(decrypt(desc, key));
    }

    public static byte[] longToByteArray(long l) {
        byte[] bArray = new byte[8];
        ByteBuffer bBuffer = ByteBuffer.wrap(bArray);
        bBuffer.order(ByteOrder.LITTLE_ENDIAN);
        LongBuffer lBuffer = bBuffer.asLongBuffer();
        lBuffer.put(0, l);
        return bArray;
    }

    public static long byteArrayToLong(byte[] bArray) {
        ByteBuffer bBuffer = ByteBuffer.wrap(bArray);
        bBuffer.order(ByteOrder.LITTLE_ENDIAN);
        LongBuffer lBuffer = bBuffer.asLongBuffer();
        long l = lBuffer.get(0);
        return l;
    }

    public static byte[] DateToByteArray(Date date) {
        long longDate = date.getTime();
        longDate *= 10000;
        longDate += 116444736000000000L;
        return longToByteArray(longDate);
    }

    public static Date ByteArrayToDate(byte[] bytes) throws Exception {
        if (bytes.length != 8)
            throw new Exception("must be 8 bytes");
        long date = byteArrayToLong(bytes);
        return new Date((date - 116444736000000000L) / 10000);
    }
    

	public static void main(String[] args) throws Exception {
    	String cookie = "A758DAEC95615683F11BE6A593E45E53C2B52174AA79F61B3C05D703C430C788D77B70128F2109A6CE44E0BE03D711D8B2A3D2A5433D987918F2A0FD160266D6B14B7969B8B9D93F4F1772FB8BA54529",
    			key = "8B6697227CBCA902B1A0925D40FAA00B353F2DF4359D2099";
    	decrypt(cookie, key);
    	cookie = "6019ACC9CF3B49E360463CEBBE9AEA55BE0F9B0C4BDDB37280D761EB96BDD677F7471D6B3F952640501E0E6F766290603144AD4504A9B1A6FAAED40CB78F8BD3AD056F762C780DA83B8F2CE593DEE807";
    	key = "C602924B0D1090D931E3771D74ABBF9733A8C3545CFE1810";
//    	byte[] bs = decrypt(cookie, key);
//    	System.out.println(new String(bs, "utf-8"));
//    	System.out.println(new String(bs, "gbk"));
    	for(int i=0; i<10; i++)	{
	    	long s = System.currentTimeMillis();
	    	byte[] bs = decrypt(cookie, key);
	    	parseTicket(bs);
	    	System.out.println(System.currentTimeMillis() - s);
    	}
    	
    	/*
        String decrytionKey = "8B6697227CBCA902B1A0925D40FAA00B353F2DF4359D2099";

        String validationKey = "282487E295028E59B8F411ACB689CCD6F39DDD21E6055A3EE480424315994760ADF21B580D8587DB675FA02F79167413044E25309CCCDB647174D5B3D0DD9141";
        // String str = "this is a AES test.xxxx.ooooo....";
        // String desc = encrypt(str, decrytionKey);
        // System.out.println("encr=" + desc);
        // System.out.println("decr="+ decrypt(desc,key)); d
        // String desc =
        // "D52E2420322F60506A2F21581A98AE5D671928BB1693717743F6CD1D5EDC166C318DF1272B2D567DAE684469178D9A68B1EFF26A2A88767C565653A34F2028E5";
        String desc = "C57594E38CA7BF188FA2A70C3A88D9EA781767EC11D2B109225B7C625E5B1838E55DCFAA8937EA1D9FAD1B9A81C300875DF3A9EE0F89F2EBFD3D9FD14C195929FC85147E3FEB70EF8269456CE2D9DF9D";
        // System.out.println("decr="+ new String(decrypt(desc,key)));

        byte[] txt = decrypt(desc, decrytionKey);
        // boolean isVerified = isSignatureVerified(txt, validationKey);
        // System.out.println("When use a correct validation key, isVerified = "
        // + isVerified);
        // change the first char from 2 to 1, and the verify result should not
        // be true.
        // validationKey =
        // "182487E295028E59B8F411ACB689CCD6F39DDD21E6055A3EE480424315994760ADF21B580D8587DB675FA02F79167413044E25309CCCDB647174D5B3D0DD9141";
        // isVerified = isSignatureVerified(txt, validationKey);
        // System.out.println("We changed the first char of the key, isVerified = "
        // + isVerified);
        DotnetAuthenticationTicket ft = parseTicket(txt);
        System.out.println("The cookie content is :\n[" + ft + "]");
        ft = parseTicket(decrypt(
                "294CDF78F08365BFFE4EA8EF4402C82BD2AAC12D6A95C343FE2D5F409854CE6F959819540F48692A8011B9480C293AE93E50E2F52BDC19AE3EA0F058710B601528CE61C6A0513D9CBFC290533C18D1D3",
                decrytionKey));
        System.out.println("The cookie content is :\n[" + ft + "]");
        ft = parseTicket(decrypt(
                "EDE365CE46A24A58BA56F0D39E2151ED8F5F4D343A9486776C824C4D575EF2C4A300854D75FD5E436A8510E6A68FCA372888F785CB61961EC9D866D8B5B55199",
                decrytionKey));
        System.out.println("The cookie content is :\n[" + ft + "]");
        ft = parseTicket(decrypt(
                "9F62F89C62EDA8E1E63086E981843B46D44AA61D5E3EAEEC5DC076351DFDE85A3E9685ED05929F4EC629D347F12CBF015958E18C92F01ACED045F2F200752FC5",
                decrytionKey));
        System.out.println("The cookie content is :\n[" + ft + "]");
        ft = parseTicket(decrypt(
                "1D4C809E9B1828354451728E7D4AEFD0BF2BA5B938DB58F63CCD880C8C3B1A17503538B8AF25E8408EF786E4974125604536AA1C01037334057822EC25D789FD",
                decrytionKey));
        System.out.println("The cookie content is :\n[" + ft + "]");
        ft = parseTicket(decrypt(
                "E74F52B228DE2EFB9C0695D2E355E3F3B1B6D25443550079F963FF5262E5C0C7A8E5895F083B8A6F6AC3E636D8EE9F53E23216F1382C13BCE9F5F4AD8B4D2C88",
                decrytionKey));
        System.out.println("The cookie content is :\n[" + ft + "]");
        ft = parseTicket(decrypt(
                "AE09ADB3921820353D950B8C0D20167726367FD4C1B38532512E81FC3F53ACC9020E60D91120BB2E89A78C63EEC4024E5BDCC576338E1DA55044D8F937C88C16",
                decrytionKey));
        System.out.println("The cookie content is :\n[" + ft + "]");
        ft = parseTicket(decrypt(
                "CE19BF61EEA9531D1634DAE95B659A28C160B5436D87D35AC361D616C4021980C085004926F988CD433A52B3F678CFB0342701B896010A912F0D105FB88FBE7F71CC7873A25FEF58DDCD913DB5C709BF",
                decrytionKey));
        System.out.println("The cookie content is :\n[" + ft + "]");
        */
    }

}