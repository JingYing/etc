package com.paipai.cms.dao;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.paipai.cms.common.Pager;


public class BaseDao<T> {
	
	@Resource
	protected HibernateTemplate hibernateTemplate;
	
	
	/**
	 * hql简单分页查询
	 * hql中不能带distinct. 有些比较复杂的hql也不能使用这种方式,需要自己写分页查询语句
	 * @param hql
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	protected Pager<T> simplePageQuery(String hql, int offset, int pageSize)	{
		return simplePageQuery(hql, new Object[]{}, offset, pageSize);
	}
	
	
	/**
	 * hql简单分页查询, 使用?号式查询参数
	 * hql中不能带distinct. 有些比较复杂的hql也不能使用这种方式,需要自己写分页查询语句
	 * @param hql
	 * @param values ?号所对应的值
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Pager<T> simplePageQuery(String hql, final Object[] values, int offset, int pageSize)	{
		if(hql.contains(" distinct"))
			throw new IllegalArgumentException("hql中不能包含distinct");
		final int o = offset, p = pageSize;
		final String h = hql;
		List list = (List)hibernateTemplate.execute(new HibernateCallback(){

			@Override
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Query query = session.createQuery(h);
				for(int i=0; i<values.length; i++)	{
					query.setParameter(i, values[i]);
				}
				return query.setFirstResult(o).setMaxResults(p).list();
			}
		});
		
		String hqlChanged = h.replaceAll(".*from ", "select count(*) from ");	//以from为标记,将hql替换成记录数查询语句
		Long count = (Long)hibernateTemplate.find(hqlChanged, values).get(0);
		return new Pager<T>(list, count.intValue());
	}
	
}
