package com.paipai.cms.domain;


public class Rule {

    
	private String key;//key
    private String desc;//描述
    private int type;//类型
    private String validate;//校验
    private String fill;//数据填充
    private int level;//级别 
    
    @Override
	public String toString() {
		return "Rule [key=" + key + ", desc=" + desc + ", type=" + type
				+ ", validate=" + validate + ", fill=" + fill + ", level="
				+ level + "]";
	}
    
    
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getValidate() {
		return validate;
	}
	public void setValidate(String validate) {
		this.validate = validate;
	}
	public String getFill() {
		return fill;
	}
	public void setFill(String fill) {
		this.fill = fill;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
    



}
