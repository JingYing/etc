package com.paipai.cms.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="customer")
public class Customer {

    @Id
    private String id;

    private String erp;//erp账号
    private String name;//姓名
    private long register_time;//注册时间（首次登陆时间）
    private long login_time;//记录登陆的时间
    

    public Customer() {}

    
    public Customer(String erp, String name, long register_time, long login_time) {
		super();
		this.erp = erp;
		this.name = name;
		this.register_time = register_time;
		this.login_time = login_time;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getErp() {
		return erp;
	}


	public void setErp(String erp) {
		this.erp = erp;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public long getRegister_time() {
		return register_time;
	}


	public void setRegister_time(long register_time) {
		this.register_time = register_time;
	}


	public long getLogin_time() {
		return login_time;
	}


	public void setLogin_time(long login_time) {
		this.login_time = login_time;
	}


	@Override
    public String toString() {
        return String.format(
                "Customer[id=%s, erp='%s', name='%s', register_time='%s', login_time='%s']",
                id, erp, name, register_time, login_time);
    }

}
