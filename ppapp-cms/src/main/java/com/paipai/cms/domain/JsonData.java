package com.paipai.cms.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="jsondata")
public class JsonData {
    
	@Id
    private String _id;//主键
	
	
    private String desc;//版本等信息描述
    private int state;//状态  [1:草稿 或 发布到IDC；2：准备发布；3：发布到gamma；]
    private int is_lock;//是否被锁定  
    private String lock_erp;//修改人
    private long lock_time;//修改时间",
    private String json_data_draft;//草稿
    private String json_data_ready;//准备发布
    private String json_data_online;//线上版本
    private String json_data_lastline;//上次线上版本
    private String json_data_demo;//数据示例
    private String json_data_rule;//校验规则
    private int isShow;//是否在菜单栏显示
    
    
	@Override
	public String toString() {
		return "JsonData [_id=" + _id + ", desc=" + desc + ", state=" + state + ", is_lock=" + is_lock
				+ ", lock_erp=" + lock_erp + ", lock_time=" + lock_time
				+ ", json_data_draft=" + json_data_draft
				+ ", json_data_ready=" + json_data_ready
				+ ", json_data_online=" + json_data_online
				+ ", json_data_lastline=" + json_data_lastline
				+ ", json_data_demo=" + json_data_demo + ", json_data_rule="
				+ json_data_rule + ", isShow=" + isShow + "]";
	}
	
	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getIs_lock() {
		return is_lock;
	}
	public void setIs_lock(int is_lock) {
		this.is_lock = is_lock;
	}
	public String getLock_erp() {
		return lock_erp;
	}
	public void setLock_erp(String lock_erp) {
		this.lock_erp = lock_erp;
	}
	public long getLock_time() {
		return lock_time;
	}
	public void setLock_time(long lock_time) {
		this.lock_time = lock_time;
	}
	public String getJson_data_draft() {
		return json_data_draft;
	}
	public void setJson_data_draft(String json_data_draft) {
		this.json_data_draft = json_data_draft;
	}
	public String getJson_data_online() {
		return json_data_online;
	}
	public void setJson_data_online(String json_data_online) {
		this.json_data_online = json_data_online;
	}
	public String getJson_data_lastline() {
		return json_data_lastline;
	}
	public void setJson_data_lastline(String json_data_lastline) {
		this.json_data_lastline = json_data_lastline;
	}
	public String getJson_data_demo() {
		return json_data_demo;
	}
	public void setJson_data_demo(String json_data_demo) {
		this.json_data_demo = json_data_demo;
	}
	public String getJson_data_rule() {
		return json_data_rule;
	}
	public void setJson_data_rule(String json_data_rule) {
		this.json_data_rule = json_data_rule;
	}
	public int getIsShow() {
		return isShow;
	}
	public void setIsShow(int isShow) {
		this.isShow = isShow;
	}

	public int getState() {
		return state;
	}

	public String getJson_data_ready() {
		return json_data_ready;
	}

	public void setJson_data_ready(String json_data_ready) {
		this.json_data_ready = json_data_ready;
	}

	public void setState(int state) {
		this.state = state;
	}
	
}
