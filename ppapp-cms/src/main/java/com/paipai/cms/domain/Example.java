package com.paipai.cms.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table
public class Example {

	private Integer id;
	private String key, value;

	@Id
	@GeneratedValue
	@Column(name = "eid")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="name")
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Column(name="sex")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
