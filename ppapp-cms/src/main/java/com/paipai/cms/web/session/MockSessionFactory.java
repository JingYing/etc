package com.paipai.cms.web.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.paipai.cms.common.ApplicationException;
import com.paipai.cms.common.redis.RedisTemplate;

/**
 * 生成MockSession
 */
public class MockSessionFactory {
	
	private RedisTemplate redisTemplate;
	
	private Class<? extends MockSession> defaultImpl = CookieMode.class;//默认的mocksession实现方式
	
	/**
	 * 根据配置文件或request创建session/cookie/redis容器
	 * @param request
	 * @return
	 */
	public MockSession create(HttpServletRequest request, HttpServletResponse response)	{
		try {
			if(defaultImpl == RedisMode.class)	{
				return new RedisMode(redisTemplate);
			} else if(defaultImpl == CookieMode.class)	{
				return new CookieMode(request, response);
			} else if(defaultImpl == SessionMode.class)	{
				return new SessionMode(request);
			} else	{
				throw new ApplicationException("不识别的defaultImpl:" + defaultImpl.getName());
			}
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
	
	public void setRedisTemplate(RedisTemplate redisTemplate)	{
		this.redisTemplate = redisTemplate;
	}
}
