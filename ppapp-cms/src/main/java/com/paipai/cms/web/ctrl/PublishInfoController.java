package com.paipai.cms.web.ctrl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.jd.common.sso.SsoTool;
import com.paipai.cms.common.HttpClient;
import com.paipai.cms.common.Result;
import com.paipai.cms.domain.JsonData;
import com.paipai.cms.domain.PublishInfo;
import com.paipai.cms.service.IJsonDataService;
import com.paipai.cms.service.IPublishInfoService;
import com.paipai.cms.web.session.MockSession;
import com.paipai.cms.web.session.SessionBinder;

@Controller
@RequestMapping("/publishInfo")
public class PublishInfoController extends BaseController{
	
	@Resource
	IPublishInfoService service;
	@Resource
	IJsonDataService jsonDataService;
	@Resource
	SsoTool ssoTool;
	/**
	 * 查询所有的需要发布的模块
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/findAll")
	public Result findAll(HttpServletResponse response) {
		Result result = new Result();
		try {
			List<PublishInfo> findAll = service.findAll();
			result.setData(findAll);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("出错了！");
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 生成本地文件
	 * @param id
	 * @param response
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/createLocalData")
	public Result createLocalData(String id, HttpServletResponse response){
		Result result = new Result();
		try {
			result.setSuccess(true);
			String fileName = service.createLocalData(id);
			result.setData(fileName);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			e.printStackTrace();
		}
		return result;
	}
	
	
	/**
	 * 发布文件
	 * @param id
	 * @param response
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/publishData")
	public Result publishData(String id, String benv, String eenv, @SessionBinder MockSession session, HttpServletRequest request, HttpServletResponse response){
		Result result = new Result();
		try {
			JsonNode jsonNode = service.publishData(
					id, benv, eenv, session.getErpUser(), 
					"erp1.jd.com=" + getCookie(request, "erp1.jd.com"));
			
			result.setSuccess(true);
			result.setData(jsonNode);
		} catch (JsonProcessingException e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			e.printStackTrace();
		} catch (IOException e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	/**
	 * 跳转到编辑页面
	 * @param _id
	 * @return
	 */
	@RequestMapping("/toMainTainEdit")
	public ModelAndView toMainTainEdit(String _id, @SessionBinder MockSession session){
		JsonData jsonData = jsonDataService.getById(_id);
		return new ModelAndView("mainTainEdit")
				.addObject("jsonDataDemo", jsonData.getJson_data_demo())
				.addObject("jsonDataRule", jsonData.getJson_data_rule())
				.addObject("_id", _id);
	}
	
	/**
	 * 跳转到编辑页面
	 * @param _id
	 * @return
	 */
	@RequestMapping("/toEdit")
	public ModelAndView toEdit(String _id, @SessionBinder MockSession session){
		JsonData jsonData = jsonDataService.getById(_id);
		if(jsonData.getIs_lock() == 0 || session.getErpUser().equals(jsonData.getLock_erp())){//未加锁
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("lock_erp",session.getErpUser());
			map.put("is_lock", 1);
			jsonDataService.updateById(_id, map);
			return new ModelAndView("edit").addObject("_id", _id);
		}else{
			return new ModelAndView("refuseEdit").addObject("lock_erp", jsonData.getLock_erp());
		}
	}
	
	
	/**
	 * 跳转到发布页面
	 * @param _id
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@RequestMapping("/toPublish")
	public ModelAndView toPublish(){
		return new ModelAndView("publish");
	}
	
	
	
	/**
	 * 跳转到发布页面
	 * @param _id
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@ResponseBody
	@RequestMapping("/getMissionStatus")
	public Result getMissionStatus(String missionCode, HttpServletRequest request, HttpServletResponse response) {
		Result result = new Result();
		try {
			String string = HttpClient.get("http://t.pp.jd.com/eos/api/api.ajax.php?act=GetMissionStatus&id=" + missionCode, "erp1.jd.com=" + getCookie(request, "erp1.jd.com"));
			result.setData(string);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
	
	
	
//	/**
//	 * 获取用户发布记录
//	 * 暂时没用到
//	 * @return
//	 * @throws IOException 
//	 * @throws ClientProtocolException 
//	 */
//	@RequestMapping("/getAllMissionByUser")
//	public String getAllMissionByUser(HttpServletRequest request,HttpServletResponse response){
//		String data = null;
//		final String cookie = "erp1.jd.com=" + getCookie(request, "erp1.jd.com");
//		if(Config.sysDebug()){
//			data = "{\"list\":[{\"createDate\":\"2015-03-17 13:38:54\",\"createUser\":\"liubenlong3\",\"missionCode\":1027666,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 14:14:04\",\"createUser\":\"liubenlong3\",\"missionCode\":1027815,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 14:18:53\",\"createUser\":\"liubenlong3\",\"missionCode\":1027851,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 14:33:48\",\"createUser\":\"liubenlong3\",\"missionCode\":1027946,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 14:33:51\",\"createUser\":\"liubenlong3\",\"missionCode\":1027948,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 14:35:50\",\"createUser\":\"liubenlong3\",\"missionCode\":1027953,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 14:35:53\",\"createUser\":\"liubenlong3\",\"missionCode\":1027954,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 16:46:18\",\"createUser\":\"liubenlong3\",\"missionCode\":1028322,\"missionSubject\":\"aaaaaaaa\",\"userid\":795},{\"createDate\":\"2015-03-17 16:57:07\",\"createUser\":\"liubenlong3\",\"missionCode\":1028340,\"missionSubject\":\"aaaaaaaa\",\"userid\":795}],\"sumCount\":9}";
//		}else{
//			String url="http://t.pp.jd.com/eos/api/api.ajax.php?act=getAllMissoinByUser&username=liubenlong3";  
//			data = HttpClient.get(url, cookie);
//		}
//		
//		JsonParser parser = new JsonParser();
//		JsonObject jsonObject = parser.parse(data).getAsJsonObject();
//		
//		Gson gson = new Gson();
//		Type type = new TypeToken<List<Missoin>>(){}.getType();
//		List<Missoin> missoins = gson.fromJson(jsonObject.get("list").toString(), type);
//		
//		PageModel<Missoin> p = new PageModel<Missoin>();
//		p.setTotal(jsonObject.get("sumCount").getAsLong());
//		p.setList(missoins);
//		p.setPageNo(1);
//		p.setPageSize(15);
//		
//		JsonObject json = new JqgridSerializer().toMaingrid(p, new ColumnMapper<Missoin>() {
//
//			@Override
//			public List<String> mapColumn(Missoin t) {
//				List<String> l = new ArrayList<String>();
//				l.add(t.getMissionCode());
//				l.add(t.getCreateDate());
//				l.add(t.getCreateUser());
//				l.add(t.getMissionSubject());
//				
//				String string = HttpClient.get("http://t.pp.jd.com/eos/api/api.ajax.php?act=GetMissionStatus&id=" + t.getMissionCode(), cookie);
//				if(StringUtils.isNotBlank(string)){
//					String[] split = string.split(",");
//					l.add(split[2].split(":")[1]);
//				}
//				
//				return l;
//			}
//
//			@Override
//			public String getRowId(Missoin t) {
//				return t.getMissionCode();
//			}
//		});
//		result.setData(json);
//		return doPrint(response, result);
//	}
	
}

