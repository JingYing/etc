package com.paipai.cms.web.ctrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jd.common.sso.SsoTool;
import com.jd.image.common.ImageUpload;
import com.paipai.cms.common.Config;
import com.paipai.cms.common.Result;
import com.paipai.cms.common.Util;
import com.paipai.cms.web.session.MockSession;
import com.paipai.cms.web.session.SessionBinder;

@Controller
@RequestMapping("/img")
public class ImgController extends BaseController{
	
	
	@Resource
	SsoTool ssoTool;
	/**
	 * 跳转到图片上传页面
	 * @param _id
	 * @return
	 */
	@RequestMapping("/toImgUpload")
	public ModelAndView aaa(String _id, @SessionBinder MockSession session){
		return new ModelAndView("fileUpload");
	}
	
	
	/**
	 * 跳转到图片上传页面
	 * @param _id
	 * @return
	 */
	@RequestMapping("/toImgUpload")
	public ModelAndView toImgUpload(String _id, @SessionBinder MockSession session){
		session.get
		return new ModelAndView("fileUpload");
	}
	
	
	
	
//	@RequestMapping("/progress")
//	public void uploadFile(HttpServletRequest request,
//	HttpServletResponse response,
//	@RequestParam("file") CommonsMultipartFile file) throws IOException {
//	response.setContentType("text/html");
//	response.setCharacterEncoding("GBK");
//	PrintWriter out;
//	boolean flag = false;
//	if (file.getSize() > 0) {
//	//文件上传的位置可以自定义
//	flag = FileUploadUtil.upLoadFile(file, request);
//	}
//	out = response.getWriter();
//	if (flag == true) {
//	out.print("1");
//	} else {
//	out.print("2");
//	}
//	}
	
	
	/**
	 * 上传图片
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/uploadImgtest")
	public Result uploadImgtest() {
		Result result = new Result();
		try {
			String string = "";
			if(Config.sysDebug()){
				string = ImageUpload.uploadFile(new File("d://6605764819167962605.jpg"),getAucode());
			}else{
				string = ImageUpload.uploadFile(new File("d://6605764819167962605.jpg"),getAucode());
			}
			
			System.out.println(string);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("出错了！");
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 上传图片
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/uploadImg")
	public Result uploadImg(@RequestParam MultipartFile[] img, HttpServletRequest request) {
		Result result = new Result();
		try {
			
//			System.out.println(img);
//			System.out.println(fileName);
			
			List<File> list = new ArrayList<File>();
			
			for(MultipartFile myfile : img){  
	            if(myfile.isEmpty()){  
	            	result.setSuccess(false);
	    			result.setMsg("文件未上传");
//	                System.out.println("文件未上传");  
	            }else{  
//	                System.out.println("文件长度: " + myfile.getSize());  
//	                System.out.println("文件类型: " + myfile.getContentType());  
//	                System.out.println("文件名称: " + myfile.getName());  
//	                System.out.println("文件原名: " + myfile.getOriginalFilename());  
//	                System.out.println("========================================");  
	                //如果用的是Tomcat服务器，则文件会上传到\\%TOMCAT_HOME%\\webapps\\YourWebProject\\WEB-INF\\upload\\文件夹中  
	                String realPath = request.getSession().getServletContext().getRealPath(File.separator + "WEB-INF" + File.separator + "upload");  
	                File file = new File(realPath, myfile.getOriginalFilename());
	                FileUtils.copyInputStreamToFile(myfile.getInputStream(), file);  
	                
	                list.add(file);
	            }  
	        }  
			
			if(list.size() > 0){
				String string = ImageUpload.uploadFile(list, getAucode());
				System.out.println(string);
				result.setData(string);
			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("出错了！");
			e.printStackTrace();
		}
		return result;
	}
	
	
	/**
	 * @return
	 */
	@RequestMapping(value = "/imgUploadtest")
	@ResponseBody
	public void imgUploadtest(HttpServletRequest request, HttpServletResponse response) {
		String string = "";
		FileOutputStream fileOutputStream = null;
		try {
			String fileName = File.separator + "usr" + File.separator + "local" + File.separator + "workspace" + File.separator +
					"paipai-halo" + File.separator + "WebRoot" + File.separator + "img" + File.separator + "banner1.jpg";
			System.out.println(fileName);
			
			File f = new File(fileName);  
	        if (!f.exists()) {  
	            throw new FileNotFoundException(fileName);  
	        }  
	        System.out.println("FileUploadController.imgUploadtest()");
	        System.out.println(f.getAbsolutePath());
	        byte[] array = null;
	        FileChannel channel = null;  
	        FileInputStream fs = null;  
	        try {  
	            fs = new FileInputStream(f);  
	            channel = fs.getChannel();  
	            ByteBuffer byteBuffer = ByteBuffer.allocate((int) channel.size());  
	            while ((channel.read(byteBuffer)) > 0) {  
	            }  
	           array = byteBuffer.array();
	           System.out.println(Arrays.toString(array));
	        } catch (IOException e) {  
	            e.printStackTrace();  
	            throw e;  
	        } finally {  
	            try {  
	                channel.close();  
	            } catch (IOException e) {  
	                e.printStackTrace();  
	            }  
	            try {  
	                fs.close();  
	            } catch (IOException e) {  
	                e.printStackTrace();  
	            }  
	        }  
			
            fileOutputStream = new FileOutputStream(fileName);
            fileOutputStream.write(array);
			string = ImageUpload.uploadFile(new File(fileName), "d3a313cb0d444049260ab6c0a6535f3c");
			
			System.out.println(string);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(null != fileOutputStream)fileOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	/**
	 * 1. 运维益园测试环境整体搬迁，新图片系统测试环境：192.168.150.54 upload.erp.360buyimg.com img*.360buyimg.com
2. 图片系统有问题，联系群里的周亮（zhouliang@jd.com），郭卫龙
3. 图片使用文档地址：
http://storage.jd.com/doc/jd-image.pdf

	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
//		System.out.println("enclosing_type.enclosing_method()");
//		
//		String aucode = "d3a313cb0d444049260ab6c0a6535f3c";//测试	授权码
//		
//		//测试的url：     http://img30.360buyimg.com/test/jfs-test/t10/140/96513883/86374/8eccc000/55104cccN8d5ba665.jpg
//		
//		//aucode			是申请业务时的授权码
//		String string = ImageUpload.uploadFile(new File("d://6605764819167962605.jpg"),aucode);
//		
//		System.out.println(string);
		
		String a = "[{\"id\":\"1\",\"msg\":\"jfs-test/t4/127/75785022/20168/caa1aac6/55112ae4N2aedffd3.jpg\"},"
				+ "{\"id\":\"1\",\"msg\":\"jfs-test/t4/127/75785022/20168/caa1aac6/55112ae4N2aedffd3.jpg\"}]";
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode readTree = objectMapper.readTree(a);
		
		for (JsonNode jsonNode : readTree) {
			if(jsonNode.get("id").asInt() == 1){
				System.out.println(getUrl() + jsonNode.get("msg").asText());
			}else{//失败
				System.out.println(jsonNode.get("msg").asText());
			}
		}
		
	}
	
	public static String getUrl(){
		if(Config.sysDebug()){
			return "http://img30.360buyimg.com/test/";
		}else{
			String[] domainName = new String [] {
				"http://img10.360buyimg.com/paipai/", 
				"http://img11.360buyimg.com/paipai/", 
				"http://img12.360buyimg.com/paipai/", 
				"http://img13.360buyimg.com/paipai/", 
				"http://img14.360buyimg.com/paipai/", 
				"http://img20.360buyimg.com/paipai/", 
				"http://img30.360buyimg.com/paipai/"};
			return Util.random(domainName);
		}
	}
	
	public static String getAucode(){
		if(Config.sysDebug()){
			return "d3a313cb0d444049260ab6c0a6535f3c";
		}else{
			return "c4ccbdf1092710454808fdd01cf7612a";
		}
	}
}

