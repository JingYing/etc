package com.paipai.cms.web.session;

import com.jd.common.sso.DotnetAuthenticationTicket;

/**
 * 为了在无session的服务器上实现类似session的功能, 封装了session/cookie/redis/jimdb的一致性操作
 * 包含一个解析单点登录cookie的方法 
 * @author JingYing
 * @date 2015年2月3日
 */
public abstract class MockSession {
	
	
	/**
	 * 获得单点登录票据中的ERP账号
	 * @return
	 */
	public String getErpUser() {
		return ticket == null ? null : ticket.getUsername();
	}
	
	/**
	 * 获得单点登录票据
	 * @return
	 */
	public DotnetAuthenticationTicket getTicket() {
		return ticket;
	}

	/**
	 * 向用户容器中存放字符串或对象
	 * cookie模式下:会进行加密
	 * redis模式下:需要检查bizkey是否在RedisKey类中注册.并且对象要实现Serializable.
	 * 		(TODO)如果redis使用场景多,考虑使用第三方开源工具做序列化
	 * @param bizkey
	 * @param value
	 */
	public abstract void set(String bizkey, Object value);
	
	/**
	 * 取得对象
	 * @param bizkey
	 * @return
	 */
	public abstract Object get(String bizkey);
	
	/**
	 * 直接取成字符串
	 * @param bizkey
	 * @return
	 */
	public abstract String getAsString(String bizkey);

//	String get(String key);//cookie
//	void set(String key);//cookie
//	
//	Object get(String key);//session
//	void set(String key, Object value);	//session
//	
//	byte[] getAsBytes(String key);//redis
//	void set(String key, byte[] value, int maxage);//redis
	
	protected DotnetAuthenticationTicket ticket;
	
	public void setTicket(DotnetAuthenticationTicket ticket) {
		this.ticket = ticket;
	}
}
