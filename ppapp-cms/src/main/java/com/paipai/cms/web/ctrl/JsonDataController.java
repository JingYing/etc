package com.paipai.cms.web.ctrl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jd.common.sso.SsoTool;
import com.paipai.cms.common.Result;
import com.paipai.cms.domain.JsonData;
import com.paipai.cms.service.IJsonDataService;
import com.paipai.cms.web.session.MockSession;
import com.paipai.cms.web.session.SessionBinder;

@Controller
@RequestMapping("/jsonData")
public class JsonDataController extends BaseController{
	
	@Resource
	IJsonDataService service;
	@Resource
	SsoTool ssoTool;
	
	
	/**
	 * 查找所有数据
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/findAll")
	public  Result findAll(HttpServletResponse response) {
		Result result = new Result();
		try {
			List<JsonData> datas = service.findAll();
			result.setData(datas);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 查询左侧菜单显示 的数据
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/findShow")
	public Result findShow(HttpServletResponse response) {
		Result result = new Result();
		try {
			List<JsonData> datas = service.findShow();
			result.setData(datas);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 查询id查询配置的规则
	 * @param _id
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/getJsonData")
	public Result getJsonData(String _id, HttpServletResponse response) throws IOException {
		Result result = new Result();
		try {
			Map<String, Object> map = service.getRuleById(_id);
			result.setData(map);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg(e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 校验数据
	 * @param _id
	 * @param jsonData
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@ResponseBody
	@RequestMapping("/checkJsonData")
	public  Result checkJsonData(String _id, String jsonData, HttpServletResponse response) throws UnsupportedEncodingException{
		Result result = new Result();
		try {
			jsonData = URLDecoder.decode(jsonData, "utf-8");
			ObjectMapper mapper = new ObjectMapper();
			List<String> list = service.checkJsonData(_id, mapper.readTree(jsonData));
			if(list.size() > 0 ){
				result.setSuccess(false);
				result.setData(list);
			}
		} catch (JsonProcessingException e) {
			result.setSuccess(false);
			result.setMsg(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			result.setSuccess(false);
			result.setMsg(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 保存草稿
	 * @param _id
	 * @param jsonData
	 * @param response
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/saveDraft")
	public  Result saveDraft(String _id, String jsonData, @SessionBinder MockSession session){
		Result result = new Result();
		try {
			jsonData = URLDecoder.decode(jsonData, "utf-8");
			service.saveDraft(session.getErpUser(), _id, jsonData);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			e.printStackTrace();
		}
		return result;
	}
	
	
	/**
	 * 保存配置的demo及规则
	 * @param _id
	 * @param jsonData
	 * @param response
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/saveDemoAndRule")
	public  Result saveDemoAndRule(String _id, String jsonDataDemo, String jsonDataRule, @SessionBinder MockSession session){
		Result result = new Result();
		try {
			jsonDataDemo = URLDecoder.decode(jsonDataDemo, "utf-8");
			jsonDataRule = URLDecoder.decode(jsonDataRule, "utf-8");
			service.saveDemoAndRule(_id, jsonDataDemo, jsonDataRule);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
			result.setSuccess(false);
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	
	/**
	 * 准备发布
	 * @param _id
	 * @param jsonData
	 * @param response
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/readyPublish")
	public  Result readyPublish(String _id, String jsonData, @SessionBinder MockSession session){
		Result result = new Result();
		try {
			jsonData = URLDecoder.decode(jsonData, "utf-8");
			ObjectMapper mapper = new ObjectMapper();
			List<String> list = service.checkJsonData(_id, mapper.readTree(jsonData));
			if(null == list || list.size() == 0){
				service.saveDraft(session.getErpUser(), _id, jsonData);
				service.readyPublish(session.getErpUser(), _id, jsonData);
			}else{
				result.setSuccess(false);
				result.setMsg(list);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg(e.getMessage());
		}

		return result;
	}
	
}

