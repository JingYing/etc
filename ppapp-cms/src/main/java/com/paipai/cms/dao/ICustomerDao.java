package com.paipai.cms.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.paipai.cms.domain.Customer;

public interface ICustomerDao extends MongoRepository<Customer, String> {

	/**
	 * 命名方法见  http://wbj0110.iteye.com/blog/1941816
	 * @param firstName
	 * @return
	 */
    public Customer findByErp(String firstName);
//    public List<Customer> findByErp(String lastName);

}
