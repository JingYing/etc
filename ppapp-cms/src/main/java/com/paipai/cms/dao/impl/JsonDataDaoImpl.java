package com.paipai.cms.dao.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonProcessingException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.paipai.cms.dao.IJsonDataDao;
import com.paipai.cms.domain.JsonData;

@Component
public class JsonDataDaoImpl implements IJsonDataDao{
	
	@Resource
	MongoTemplate mongoTemplate;
	
	public List<JsonData> findAll()	{
		return mongoTemplate.findAll(JsonData.class);
	}
	
	
	public List<JsonData> findByParam(Query query)	{
		return mongoTemplate.find(query, JsonData.class);
	}
	
	public JsonData findById(String _id)	{
		return mongoTemplate.findById(_id, JsonData.class);
	}
	
	
	public List<JsonData> getByIds(List<String> ids)	{
		Criteria criteria = new Criteria("_id").in(ids);
		return mongoTemplate.find(new Query(criteria), JsonData.class);
	}

	
	/**
	 * 根据id修改数据
	 * @param _id
	 * @param map
	 * @return
	 */
	@SuppressWarnings("static-access")
	public void updateById(String _id, Map<String, Object> map)	{
		Criteria criteria = new Criteria("_id").is(_id);
		Update update = new Update();
		Set<Entry<String,Object>> entrySet = map.entrySet();
		for (Entry<String, Object> entry : entrySet) {
			update.set(entry.getKey(), entry.getValue());
		}
		mongoTemplate.updateFirst(new Query(criteria), update.set("lock_time", new Date().getTime()), JsonData.class);
	}
	
	
	public void saveDraft(String _id, String data)	{
		Criteria criteria = new Criteria("_id").is(_id);
		Object parse = new JSON().parse(data);
		DBObject draft = (DBObject) parse;
		mongoTemplate.updateFirst(new Query(criteria), new Update().set("json_data_draft", draft), JsonData.class);
	}
	
	
	
	public void readyPublish(String _id, String data)	{
		Criteria criteria = new Criteria("_id").is(_id);
		DBObject draft = (DBObject) new JSON().parse(data);
		mongoTemplate.updateFirst(new Query(criteria), new Update().set("json_data_ready", draft).set("is_lock", 0).set("lock_time", 0).set("state", 2), JsonData.class);
	}
	
	public void unLock(String _id)	{
		Criteria criteria = new Criteria("_id").is(_id);
		mongoTemplate.updateFirst(new Query(criteria), new Update().set("is_lock", 0), JsonData.class);
	}


	@Override
	public void saveDemoAndRule(String _id, String jsonDataDemo,
			String jsonDataRule) {
		Criteria criteria = new Criteria("_id").is(_id);
		Object parse = new JSON().parse(jsonDataDemo);
		DBObject demo = (DBObject) parse;
		Object parse1 = new JSON().parse(jsonDataRule);
		DBObject rule = (DBObject) parse1;
		mongoTemplate.updateFirst(new Query(criteria), new Update().set("json_data_demo", demo).set("json_data_rule", rule), JsonData.class);
	}
	
}
