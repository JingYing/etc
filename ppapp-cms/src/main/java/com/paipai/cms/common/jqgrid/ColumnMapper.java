package com.paipai.cms.common.jqgrid;

import java.util.List;

/**
 * 定义实体类在jqgrid中的列映射关系. 和jqgridService结合使用.
 */
public interface ColumnMapper<T> {
	
	/**
	 * 定义jqgrid的列映射关系. list中的值顺序和页面上显示顺序一样.
	 * @param t
	 * @return
	 */
	List<String> mapColumn(T t);
	
	/**
	 * 定义jqgrid的行id, 一般为实体类的id
	 * @param t
	 * @return
	 */
	String getRowId(T t);

}
