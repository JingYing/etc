package com.paipai.cms.common.jqgrid;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * 根据jqgrid请求字符串的oper字段值, 分发到各个增删改查方法中.
 */
public class CrudExecutor {
	
	public static String execute(Map<String, String[]> paramMap, Crud crud) throws JsonParseException, JsonMappingException, IOException	{
		JqgridReq req = JqgridReq.parse(paramMap);  
		String json = "";
		if(Const.OPER_ADD.equals(req.getOper()))	{
			crud.add(req);
		} else if(Const.OPER_EDIT.equals(req.getOper()))	{
			json = crud.edit(req);
		} else if(req.isSearch() && req.getFilters() == null)	{
			json = crud.search(req);
		} else if(req.isSearch() && req.getFilters() != null)	{	
			json = crud.filterSearch(req);
		} else	{	
			json = crud.query(req);
		}
		return json;
	}

}
