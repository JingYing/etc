package com.paipai.cms.common.redis;

public interface RedisTemplateCallback {
	
	Object execute(RedisTemplate redisTemplate);

}
