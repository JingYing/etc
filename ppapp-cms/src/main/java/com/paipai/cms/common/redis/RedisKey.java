package com.paipai.cms.common.redis;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class RedisKey {
	
	/**
	 * redis的键前缀, 所有操作redis的key前缀应当在这里注册, 避免误覆盖别人存放的数据.
	 * 以双斜线结尾
	 */
	public static final String 
		用户会话_权限 = "session/privilege//";	//例如:session/privilege//zhangsan=11111111111111
	
	
	/**
	 * 检查redisKey是否合法
	 * @param key
	 */
	public static void checkKey(String key)	{
		boolean b = false;
		for(String s : allConstants)	{
			if(key.startsWith(s))	{
				b = true;
			}
		}
		if(b == false)
			throw new UnregRediskeyException(key);
	}
	
	private static List<String> allConstants = new ArrayList<String>();
	static	{
		for(Field f : RedisKey.class.getFields())	{
			try	{
				allConstants.add((String)f.get(new RedisKey()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
