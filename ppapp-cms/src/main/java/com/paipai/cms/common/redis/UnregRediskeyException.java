package com.paipai.cms.common.redis;

/**
 * 未在RedisKey中注册的redisKey
 *
 */
public class UnregRediskeyException extends RuntimeException{

	public UnregRediskeyException() {
		super();
	}

	public UnregRediskeyException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnregRediskeyException(String message) {
		super(message);
	}
}
