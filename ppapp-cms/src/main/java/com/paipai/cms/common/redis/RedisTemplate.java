package com.paipai.cms.common.redis;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * 封装一些Jedis常用操作, 还有获取连接,回收连接. 每一个方法执行后都会将连接放回连接池.
 * 如有特殊需要,调用execute(RedisCallback callback),直接操作jedis.
 * 
 * 在springIOC容器中获取该类
 *
 */
public class RedisTemplate {
	
	private static Logger log = LoggerFactory.getLogger(RedisTemplate.class);

	private static final String CHARSET = "utf-8";

	private JedisPool jedisPool;
	
	/**
	 * 使用jedis原始API操作
	 * @param callback
	 * @return
	 */
	public Object execute(JedisCallback callback) {
		checkPool();
		Jedis jedis = jedisPool.getResource();
		try {
			return callback.doWithJedis(jedis);
		} finally {
			jedisPool.returnResource(jedis); 
		}
	}

	/**
	 * KEY对应的value是否存在
	 * @param key
	 * @return
	 */
	public Boolean exists(String key) {
		Jedis jedis = jedisPool.getResource();
		try {
			return jedis.exists(key);
		} finally {
			jedisPool.returnResource(jedis); 
		}
	}

	/**
	 * 删除KEY值对应的VALUE
	 * @param keys
	 * @return
	 */
	public Long del(String... keys) {
		Jedis jedis = jedisPool.getResource();
		try {
			return jedis.del(keys);
		} finally {
			jedisPool.returnResource(jedis); 
		}
	}

	/**
	 * 获取KEY值对应的字节数组
	 * @param key
	 * @return
	 */
	public byte[] getBytes(String key) {
		Object o = this.getObject(key);
		if(o == null)
			return null;
		else if(o instanceof byte[])
			return (byte[])o;
		else
			throw new ClassCastException("redis中取出的类型为:" + o.getClass().toString());
	}
	
	/**
	 * 直接获取KEY对应的字符串
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		Object o = this.getObject(key);
		if(o == null)
			return null;
		else if(o instanceof String)
			return (String)o;
		else
			throw new ClassCastException("redis中取出的类型为:" + o.getClass().toString());
	}

	/**
	 * 获取KEY对应的对象. 
	 * @param key
	 * @return
	 */
	public Object getObject(String key) {
		Jedis jedis = jedisPool.getResource();
		try {
			byte[] bs = jedis.get(key.getBytes(CHARSET));
			Object ret = ObjectSerializer.deSerialize(bs);
			if(ret == null)	{
				return null;
			} else if(ret instanceof Bytes)	{	//如果类型为Bytes
				return ((Bytes)ret).getBytes();
			} else	{
				return ret;
			}
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} finally {
			jedisPool.returnResource(jedis);
		}
	}

	/**
	 * 向redis存放对象, 并设置缓存时间.
	 * 如果对象为byte[], 被Bytes包装后, 再使用ObjectSerializer存入redis.
	 * 如果对象为String或其它类, 使用ObjectSerializer序列化后存入redis.
	 * @param key
	 * @param value 该对象的关联对象需全部实现Serializable
	 */
	public void set(String key, Object value) {
		RedisKey.checkKey(key);
		Jedis jedis = jedisPool.getResource();
		try {
			if (value instanceof byte[]) {
				value = new Bytes((byte[])value);
			} 
			jedis.set(key.getBytes(CHARSET), ObjectSerializer.serialize(value));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} finally {
			jedisPool.returnResource(jedis); // must be
		}
	}
	
	/**
	 * 判断是否过期
	 * @param key
	 * @return
	 */
	public long ttl(String key) {
		Jedis jedis = jedisPool.getResource();
		try {
			return jedis.ttl(key);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally{
			jedisPool.returnResource(jedis); // must be
		}
	}
	
	
	
	/**
	 * 
	 * @param key
	 * @param seconds
	 * @param value
	 * @return
	 */
	public long setnx(String key, String value) {
		Jedis jedis = jedisPool.getResource();
		try {
			return jedis.setnx(key, value);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally{
			jedisPool.returnResource(jedis); // must be
		}
	}
	
	
	/**
	 * 设置有效期
	 */
	public long expire(String key, int seconds) {
		Jedis jedis = jedisPool.getResource();
		try {
			return jedis.expire(key, seconds);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally{
			jedisPool.returnResource(jedis); // must be
		}
	}
	
	
	/**
	 * 尝试从redis获得对象, 不成功时进行回调, 将回调得到的值放入redis中. 不向外抛出异常,以免redis出问题时影响主服务.
	 * @param key
	 * @param expireSec
	 * @param callback 指示如何生成值,以放入redis. 
	 * @return
	 */
	public Object getAndSet(String key, RedisTemplateCallback callback)	{
		Object cache = null;
		try {
			cache = getObject(key);
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		
		if(cache != null)	{
			return cache;
		} else	{
			Object o = callback.execute(this);
			try {
				if(o != null)	{
					set(key, o);
				}
			} catch (Exception e) {
				log.debug(e.getMessage(), e);
			}
			return o;
		}
	}
	
	private void checkPool()	{
		if(jedisPool == null)
			throw new RuntimeException("RedisTemplate的JedisPool不能为空");
	}

	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}
}
