package com.jd.common.sso;

public class SsoException extends RuntimeException{

	public SsoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SsoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SsoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SsoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
