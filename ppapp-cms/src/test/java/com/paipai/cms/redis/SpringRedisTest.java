package com.paipai.cms.redis;

import java.net.URL;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import com.paipai.cms.BaseTest;

import redis.clients.jedis.Jedis;

public class SpringRedisTest extends BaseTest{

	// inject the actual template
	@Autowired
	private RedisTemplate<String, String> template;

	// inject the template as ListOperations
	// can also inject as Value, Set, ZSet, and HashOperations
	@Resource(name = "springRedisTemplate")
	private ListOperations<String, String> listOps;

	@Test
	public void test1()	{
		template.execute(new RedisCallback<Object>() {

			@Override
			public Object doInRedis(RedisConnection connection) throws DataAccessException {
				Jedis jedis = (Jedis) connection.getNativeConnection();
				jedis.set("aaa", "ddddddddddddddddddddd");
				System.out.println(jedis.get("aaa"));
				return null;
			}
		});
	}
	
	@Test
	public void test2()	{
		ValueOperations<String, String> oper = template.opsForValue();
		oper.set("aaa", "eeeeeeeeeeeeee");
		System.out.println(oper.get("aaa"));
	}
	
	private void addLink(String userId, URL url) {
		listOps.leftPush(userId, url.toExternalForm());
		// or use template directly
		template.boundListOps(userId).leftPush(url.toExternalForm());
	}
}
