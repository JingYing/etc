package com.paipai.cms;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations={"classpath:applicationContext-common.xml",
								//"classpath:applicationContext-mysql.xml",
								"classpath:applicationContext-mongo.xml"})
public class BaseTest extends AbstractJUnit4SpringContextTests{

}
