<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
 "http://www.w3.org/TR/html4/loose.dtd" >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="<%=basePath %>/static/jquery/jquery-2.1.3.js"></script>
    
	<link rel="stylesheet" type="text/css" href="<%=basePath %>/static/jsoneditor/jsoneditor.css">
  <script type="text/javascript" src="<%=basePath %>/static/jsoneditor/jsoneditor.js"></script>
  <!-- ace editor -->
  <script type="text/javascript" src="<%=basePath %>/static/jsoneditor/asset/ace/ace.js"></script>
  <!-- json lint -->
  <script type="text/javascript" src="<%=basePath %>/static/jsoneditor/asset/jsonlint/jsonlint.js"></script>
  
  
  <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/frame/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>/static/frame/stylesheets/theme.css">

  <style type="text/css">
    body {
      font: 10.5pt arial;
      color: #4d4d4d;
      line-height: 150%;
      width: 500px;
    }

    code {
      background-color: #f5f5f5;
    }

    #jsoneditor {
      width: 100%;
      height: 500px;
    }
    
    
    #jsoneditorDemo {
      width: 100%;
      height: 500px;
    }
  </style>
  <script type="text/javascript">
  
  	
  </script>
</head>
<body style="width: 100%;">
	<div class="row-fluid">
    <div class="block span6" style="width: 40%;">
        <div class="block-heading" data-toggle="collapse" data-target="#tablewidget">效果展示</div>
        <div id="tablewidget" class="block-body collapse in" >
        	<img id="reference" alt="" src="<%=basePath %>/img/banner1.jpg">
        </div>
    </div>
    <div class="block span6">
        <div class="block-heading" data-toggle="collapse" data-target="#widget1container">字段说明 </div>
        <div  id="widget1container" class="block-body collapse in">
            <table class="table">
              <thead>
                <tr>
                  <th>key</th>
                  <th>描述</th>
                  <th style="width: 30px;">类型</th>
                  <th>校验</th>
                  <th>数据填充</th>
                  <th style="width: 25px;">级别</th>
                </tr>
              </thead>
              <tbody id="dataRule"></tbody>
            </table>
        </div>
    </div>
</div>


<div class="row-fluid">
    <div class="block span6" style="width: 40%;">
        <div class="block-heading" data-toggle="collapse" data-target="#demoData">示例数据</div>
        <div id="demoData" class="block-body collapse in" >
        	<div id="jsoneditorDemo"></div>
        </div>
    </div>
    <div class="block span6">
        <div class="block-heading" data-toggle="collapse" data-target="#editJson">编辑json </div>
        <div id="editJson" class="block-body collapse in">
			<div id="jsoneditor"></div>
        </div>
    </div>
</div>

<div style="text-align: center;">
	<button onclick="checkData()">数据校验</button>&nbsp;&nbsp;&nbsp;
	<button onclick="saveDraft()">保存草稿</button>&nbsp;&nbsp;&nbsp;
	<button onclick="readyPublish()">准备发布</button>
</div>

	<script type="text/javascript" >
	  var container = document.getElementById('jsoneditor');
	
	  var options = {
	    mode: 'code',
	    modes: ['code', 'form', 'text', 'tree', 'view'], // allowed modes
	    error: function (err) {
	      alert(err.toString());
	    }
	  };
	
	  var json = {};
	  var editor = new JSONEditor(container, options, json);
	  
	  var containerDemo = document.getElementById('jsoneditorDemo');
	  var editorDemo = new JSONEditor(containerDemo, options, json);
	  
	  $.ajax({
		   type: "POST",
		   url: "<%=basePath %>/jsonData/getJsonData.php",
		   data: {
			   "_id": "${_id }"
		   },
		   success: function(result){
			   var data = result.data;
			   	var rule = data.rule;
			   
			   	for(var i = 0 ; i < rule.length; i ++){
			   		$("#dataRule").append("<tr><td>" + rule[i].key + "</td><td>" + rule[i].desc + "</td><td>" + 
			   				getKeyVal(rule[i].type) + "</td><td>" + (rule[i].validate == null ? "" : rule[i].validate) + "</td><td>" + 
			   				(rule[i].fill == null ? "" : rule[i].fill) + "</td><td>" + rule[i].level + "</td></tr>");
			   	}
			   	
				editorDemo.set(data.demoData);
			   	editor.set(data.draft.data);
		   }
		});
	  
	  
	  function getKeyVal(type){
		  switch (type){
	   		case 1:
	   			return "文本";
	   			break;
	   		case 2:
	   			return "对象";
	   			break;
	   		case 3:
	   			return "数组";
	   			break;
	   		case 4:
	   			return "单选";
	   			break;
	   		case 5:
	   			return "复选";
	   			break;
	   		default :
	   			return "文本";
	   			break;
	   		}
	  }
	  
	  function checkData(){
		  try {
			  editorDemo.format();
			  
			  $.ajax({
				   type: "POST",
				   url: "<%=basePath %>/jsonData/checkJsonData.php",
				   data: {
					   "_id": "${_id }",
					   "jsonData":encodeURI(JSON.stringify(editor.get()))
				   },
				   success: function(result){
					   	if(result.success){
					   		alert("ok");
					   		return true;
					   	}else{
					   		alert(result.data);
					   		return false;
					   	}
				   }
				});
		  }catch (err) {
			  editorDemo._onError(err);
			  return false;
		  }
	  }
	  
	  function saveDraft(){
		  try {
			  editorDemo.format();
			  
			  $.ajax({
				   type: "POST",
				   url: "<%=basePath %>/jsonData/saveDraft.php",
				   data: {
					   "_id": "${_id }",
					   "jsonData":encodeURI(JSON.stringify(editor.get()))
				   },
				   success: function(result){
					   if(result.success){
						   alert("ok");
					   }else{
					   	  alert(result.msg);
					   }
				   }
				});
		  }catch (err) {
			  editorDemo._onError(err);
			  return false;
		  }
		  
	  }
	  
	  function readyPublish(){
		  try {
			  editorDemo.format();
			  
			  $.ajax({
				   type: "POST",
				   url: "<%=basePath %>/jsonData/readyPublish.php",
				   data: {
					   "_id": "${_id }",
					   "jsonData":encodeURI(JSON.stringify(editor.get()))
				   },
				   success: function(result){
					   if(result.success){
						   alert("ok");
					   }else{
						   alert(result.msg);
					   }
				   }
				});
		  }catch (err) {
			  editorDemo._onError(err);
			  return false;
		  }
		  
	  }
	</script>
	
	<script src="<%=basePath %>/static/frame/lib/bootstrap/js/bootstrap.js"></script>
	
</body>
</html>