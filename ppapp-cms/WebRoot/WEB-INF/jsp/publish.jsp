<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
 "http://www.w3.org/TR/html4/loose.dtd" >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="<%=basePath %>/static/jquery/jquery-2.1.3.js"></script>
<script type="text/javascript" src="<%=basePath %>/static/jqueryui/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="<%=basePath %>/static/jqgrid/i18n/grid.locale-cn.js"></script>
<script type="text/javascript" src="<%=basePath %>/static/jqgrid/jquery.jqGrid.min.js"></script>

  
  

<link type="text/css" rel="stylesheet" href="<%=basePath %>/static/jqueryui/cupertino/jquery-ui-1.10.3.custom.min.css">
<link type="text/css" rel="stylesheet" href="<%=basePath %>/static/jqgrid/ui.jqgrid.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>/static/frame/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>/static/frame/stylesheets/theme.css">

  <style type="text/css">
    body {
      font: 10.5pt arial;
      color: #4d4d4d;
      line-height: 150%;
      width: 500px;
    }

    code {
      background-color: #f5f5f5;
    }
	#shade {
	    background-color:#000;
	    position:absolute;
	    z-index:49;
	    display:none;
	    width:100%;
	    height:100%;
	    opacity:0.2;
	    filter: alpha(opacity=60);
	    -moz-opacity: 0.6;
	    margin: 0px;
	    left: 0px;
	    top: 0px;
	    right: 0px;
	    bottom: 0px;
	}
  </style>
  <script type="text/javascript">
  var intervalId = null;
  function initPublish(){
	  $.ajax({
		   type: "POST",
		   url: "<%=basePath %>/publishInfo/findAll.php",
		   success: function(result){
			   	data = result.data;
			   	for(var i = 0 ; i < data.length; i ++){
				   	$("#publishdiv").append("<input type=\"radio\" name=\"publish\" value='" + data[i]._id + "'/>" + data[i].name + "</br>");
			   	}
		   }
		});
  }
  
  function createLocalData(){
	  
	  var p = $("input[name=publish]:checked").val();
	  if(null == p){
		  alert("请先选择要生成的数据！");
		  return ;
	  }
	  
	  $.ajax({
		   type: "POST",
		   url: "<%=basePath %>/publishInfo/createLocalData.php",
		   data: {
			   "id": $("input[name=publish]:checked").val()
		   },
		   success: function(result){
// 			   	$("#shade").show();
// 			   	$("#Layer1").show();
			   	if(result.success){
			   		alert("操作完成。       <%=basePath %>data/" + result.data);
// 				   	$("#resultspan").html("</br>操作完成。</br></br>");
<%-- 				   	$("#resultspan").append("<%=basePath %>data/" + data.msg + "</br></br>"); --%>
			   	}else{
			   		alert("操作失败！       " + data.msg);
// 			   		$("#resultspan").html("</br>操作失败！</br></br>");
// 				   	$("#resultspan").append(data.msg);
			   	}
		   }
		});
  }
  function publishData(benv, eenv){
	  
	  var p = $("input[name=publish]:checked").val();
	  if(null == p){
		  alert("请先选择要发布的数据！");
		  return ;
	  }
	  
	  $.ajax({
		   type: "POST",
		   url: "<%=basePath %>/publishInfo/publishData.php",
		   data: {
			   "id": $("input[name=publish]:checked").val(),
			   "benv": benv,
			   "eenv": eenv
		   },
		   success: function(result){
			   	$("#shade").show();
// 			   	$("#Layer1").show();
			   var data = result.data;
			   var pResult = data.result.split(",");
			   
			   if(result.success){
				   if(pResult[0].split(":")[1] == 0){
				   		$("#result").html("操作完成。 ");
				   		$("#fileUrl").html("<%=basePath %>data/" + data.filename);
				   		$("#missionStatus").html("状态：已下发。");
				   		intervalId = window.setInterval("getMissionStatus(" + pResult[1].split(":")[1] + ")", 2000); //设置每500毫秒查询一次状态
				   }
			   	}else{
			   		alert("操作失败！          " + pResult[2].split(":")[1]);
			   	}
		   }
		});


// 	$.ajax({
// 	   type: "POST",
<%-- 	   url: "<%=basePath %>/publishInfo/publishData.php", --%>
// 	   data: {
// 		   "id": $("input[name=publish]:checked").val(),
// 		   "benv": benv,
// 		   "eenv": eenv
// 	   },
// 	   success: function(result){
// //		   	$("#shade").show();
// //		   	$("#Layer1").show();
// 		   	if(result.success){
// 		   		alert("操作完成。");
// 		   	}else{
// 		   		alert("操作失败！       " + result.msg);
// 		   	}
// 	   }
// 	});
  }
  
  
  function getMissionStatus(missionCode){
	  $.ajax({
		   type: "POST",
		   url: "<%=basePath %>/publishInfo/getMissionStatus.php",
		   data: {
			   "missionCode":missionCode
		   },
		   success: function(result){
			   var data = result.data;
			   $("#missionStatus").html("状态：" + data.split(",")[2].split(":")[1]);
			   
			   if(data.split(",")[0].split(":")[1] == 7){
				   $("#shade").hide();
				   window.clearInterval(intervalId);
			   }
		   }
		});
  }
  
  
//   $(document).ready(function(){
// 	    $("#list4").jqGrid({
<%-- 	        url: "<%=basePath %>/publishInfo/getAllMissionByUser.php", --%>
// 	        datatype:"json", //数据来源，本地数据
// 	        mtype:"POST",//提交方式
// 	        height:420,//高度，表格高度。可为数值、百分比或'auto'
// 	        //width:1000,//这个宽度不能为百分比
// 	        autowidth:true,//自动宽
// 	        colNames:['id', '创建日期', '创建人', '主题' , '状态'],
// 	        colModel:[
// 	            {name:'missionCode',index:'missionCode', width:'25%',align:'center', sortable:false},
// 	            {name:'createDate',index:'createDate', width:'25%',align:'center', sortable:false},
// 	            {name:'createUser',index:'createUser', width:'25%', align:"center", sortable:false},
// 	            {name:'missionSubject',index:'missionSubject', width:'25%', align:"center", sortable:false},
// 	            {name:'retMessage',index:'retMessage', width:'25%', align:"center", sortable:false}
// 	        ],
	        
// 	        rownumbers:true,//添加左侧行号
// 	        //altRows:true,//设置为交替行表格,默认为false
// 	        //sortname:'createDate',
// 	        //sortorder:'asc',
// 	        viewrecords: true,//是否在浏览导航栏显示记录总数
// 	        rowNum:15,//每页显示记录数
// 	        rowList:[15,20,25],//用于改变显示行数的下拉列表框的元素数组。
// 	        jsonReader:{
// 	        	root: "rows",
// 	        	total: "total",
// 	        	page: "page",
// 	        	records: "records",
// 	        	repeatitems: true 
// 	        },
// 	        pager:$('#gridPager')
// 	    });
// 	});
  </script>
</head>
<body style="width: 100%;" onload="initPublish()">
<div id="shade">
</div>

<div class="row-fluid">
    <div id="publishdiv" style="margin-top: 20px;text-align: left;margin-left: 30%;"></div>
	
	<div style="margin-left: 30%;margin-top: 20px;">
		<input class="confirm" type="submit" value="生成本地文件" onclick="createLocalData()">&nbsp;&nbsp;
		<input class="confirm" type="submit" value="发布到GAMMA" onclick="publishData('dev', 'gamma')">&nbsp;&nbsp;
		<input class="confirm" type="submit" value="发布到IDC" onclick="publishData('gamma', 'idc')">
	</div>
	
	
	
	<div style="margin-left: 30%;margin-top: 30px;">
		<span id="result"></span></br>
		<span id="fileUrl"></span></br>
		<span id="missionStatus"></span>
	</div>

<!-- jqGrid table list4 -->
<!-- <table id="list4"></table> -->
<!-- jqGrid 分页 div gridPager -->
<!-- <div id="gridPager"></div> -->

	
</div>

	<script type="text/javascript" >
	</script>
	
	<script src="<%=basePath %>/static/frame/lib/bootstrap/js/bootstrap.js"></script>
	
</body>
</html>