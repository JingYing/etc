new Vue({
	  data: {
		  operators: operators,
	      channels:channels,
	      partners:partners,
	      settings:[],
	      loading:false,
	      showSuccess:false
	  },
	  el: '#partner-segments',
	  created: function() {
		  this.channels.unshift({'key':'null','value':'默认'});
		  this.operators[0].active=true;
		  this.activeChannel(0);
		  this.$nextTick(function() {
			  $('button[name=batchSet]').popup({
    			  //popup : $('.ui.popup'),
    			  on    : 'click',
    			  position:'bottom center',
    			  inline     : true,
    			  variation:'wide',
    			  onVisible: function() {
    				  $('#popup-dropdown').dropdown();
    			  }
    			});
	      });
	  },
	  mounted: function() {
	  },
	  updated: function() {
	  },
	  methods: {
	    activeOperator: function (index) {
	    	for(var v in this.operators) {
	    		this.operators[v].active=false;
	    		this.$set(this.operators, v, this.operators[v]);
	    	}
	    	this.operators[index].active=true;
	    	this.$set(this.operators, index, this.operators[index]);
	    	this.load();
	    },
	    activeChannel: function (index) {
	    	for(var v in this.channels) {
	    		this.channels[v].active=false;
	    		this.$set(this.channels, v, this.channels[v]);
	    	}
	    	this.channels[index].active=true;
	    	this.$set(this.channels, index, this.channels[index]);
	    	this.load();
	    },
	    load: function () {
	    	var me=this;
	    	me.loading=true;
	    	me.showSuccess=false;
	    	$.ajax({
	    		url:path+'/api/partnerInfo?operators='+this.getActiveOperator()+'&channel='+this.getActiveChannel(),
	    		dataType:'json',
	    		//async:false,
	    		success:function(data) {
		    	  me.settings=data;
		    	  me.$nextTick(function () {
	    		  	$('[name=p-selection]').dropdown({
	    		  		onChange: function(value, text, $choice){
	    		  			var index=$(this).attr('data-index');
	    		  			me.settings[index].partner=value;	//因为partner值未绑定视图,所以不用通知VUE刷新视图
	    		  		}
	    		  	});
	    		  	
	    		  	this.initPartnerDropdown();
		    	  	me.loading=false;
		    	  });
	    		}
	    	});
	    },
	    
	    initPartnerDropdown: function() {
	    	$('[name=p-selection]').each(function(i){
    			var $drop=$(this);
    			$drop.dropdown('clear');
    			var before=$drop.find(':hidden').attr('data-beforechange');	//不传value时使用html属性
    			$.each(before.split(','), function(i,n){
    				$drop.dropdown('set selected', n);	//依赖于onchange事件
    			});
		  });
	    },
	    getActiveOperator: function() {
	    	for(var i in this.operators) {
	    		var obj=this.operators[i];
	    		if(true==obj.active) return obj.key;
	    	}
	    	return null;
	    },
	    getActiveChannel: function() {
	    	for(var i in this.channels) {
	    		var obj=this.channels[i];
	    		if(true==obj.active) return obj.key;
	    	}
	    	return null;
	    },
	    save: function() {
	    	var me=this;
	    	me.showSuccess=false;
	    	var data={
	    		channel:this.getActiveChannel(),
	    		operators:this.getActiveOperator(),
	    	};
	    	$.each(this.settings, function(i,n){
	    		data['r'+n.recid]=n.partner;
	    	});
	    	me.loading=true;
	    	$.post(path+'/api/savePartnerInfo',data,function(data){
	    		if(data.result_code=='0') me.showSuccess=true;
	    		else alert('保存失败,请联系网站开发者');	    			
	    		me.loading=false;
	    	},'json');
	    },
	    restore: function() {
	    	var me=this;
	    	$('#restoreWindow').modal({
				closable:false,
				duration:100,
				onVisible : function(){
				},
				onHidden: function(){
				},
				onApprove: function() {
					var arr=[];
					$.each($(this).find('.checkbox'),function(i,n){
						if($(this).checkbox('is checked')){
							arr.push($(this).find(':checkbox').val());
						}
					});
					var ret=false;
					$.ajax({
						url:path+'/api/unifyConf',
						method:'POST',
						async:false,//同步提交
						data:{type:'partner',channel:arr.join(',')},
						dataType:'json',
						success: function(data){
				    		if(data.err==0){
				    			me.load();
				    			$('#formMsg').dimmer('show');
				    			window.setTimeout(function(){
			    			    	$('#formMsg').dimmer('hide');
				    				$('#addWindow').modal('close');
				    			},1000);
				    			ret=true;
				    		} else {
				    			alert(data.msg);
								ret=false;
				    		}
				    	}
					});
					return ret;
				}
			}).modal('show');
	    },
	    batchSet : function(event) {
	    	var val=$('#batchSetValue').val();
    		$('[name=p-selection]').each(function(i){
    			var $drop=$(this);
    			$drop.dropdown('clear');
		    	if(val) {
	    			$.each(val.split(','), function(i,n){
	    				$drop.dropdown('set selected', n);	//依赖于onchange事件
	    			});
    			}
		  	});
	    }
	  }
	});