<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp"%>
<style type="text/css">
div.slider {
    display: none;
    margin: 0 20px 0;
}
<%--
#more a{color:black !important;}
#more a:link{color:black !important;}
#more a:visited{color:black !important;}
#more a:hover{color:black !important;}
#more a:active{color:black !important;}
--%>
#more label {
min-width: 5em;
text-align: right;
}
.secitem {
margin: 0 .4em 0;
}
.secitem:hover {
	cursor: pointer;
}
</style>
<!-- 
<script src="static/datatables/extension/button/js/dataTables.buttons.min.js"></script>
<script src="static/datatables/extension/button/js/buttons.semanticui.min.js"></script>
<script src="static/datatables/extension/button/js/buttons.colVis.min.js"></script>
<script src="static/datatables/extension/button/js/buttons.html5.min.js"></script>
<script src="static/js/jszip.min.js"></script>
 -->

<div style="margin: 0 6% 0;">
<form id="searchForm" class="ui tiny form " onsubmit="return false;">
<input type="hidden" name="teddy" value="teddy">
	<div class="fields">
		<div class="field">
	       <label>&nbsp;</label>
	        <div class="ui left action input" style="max-width: 300px;">
			  <div class="ui dropdown small button">
			  	<input type="hidden" name="searchType" value="orderId">
			    <div class="text">IMEI</div>
			    <i class="dropdown icon"></i>
			    <div class="menu">
			      <div class="item" data-value="orderId">订单号</div>
			      <div class="item" data-value="phone">手机号</div>
			      <div class="item" data-value="three">第3方单号</div>
			    </div>
			  </div>
			  <input type="text" name="searchValue" placeholder="输入.." value="">
			</div>
	    </div>
    
	     <div class="field">
	       <label>日期范围</label>
		       <div class="ui calendar" id="rangestart" style="display: inline-block;">
		         <div class="ui input left icon">
		           <i class="calendar icon"></i>
		           <input name="startTime" type="text" value="${start}" placeholder="包含所选日期" autocomplete="off">
		         </div>
		       </div> -
		      <div class="ui calendar" id="rangeend"  style="display: inline-block;">
		        <div class="ui input left icon">
		          <i class="calendar icon"></i>
		          <input name="endTime" type="text" value="${end}" placeholder="包含所选日期" autocomplete="off">
		        </div>
		      </div>
	    </div>
    
	    <div class="field two wide" style="position: relative;">
	 	    <div style="position:absolute; bottom:0%;">
	    	<button class="ui basic compact icon button" onclick="stat.toggle(this)" title="显示更多条件"><i class="angle double down icon"></i></button>
	    	<button class="ui primary tiny button" id="search" onclick="stat.search(this)">查询</button>
	    	<button class="ui tiny button" onclick="stat.download(this)">下载</button>
	    	</div>
	    </div> 
    </div>
    
    
  <div id="more" style="display: none;">
     	<div class="inline field" >
       <label>订单状态:</label>
       <input type="hidden" name="status"/>
       	  <span class="secitem ui blue circular label" data-value="">全部</span>
		  <c:forEach items="${status}" var="v">
		  <span class="secitem" data-value="${v.key}">${v.value}</span>
		  </c:forEach>
    </div>
    <div class="inline field" style="line-height: 2em;">
       <label>归属地:</label>
       	<input type="hidden" name="province"/>
		  <span class="secitem ui blue circular label" data-value="">全部</span>
		  <c:forEach items="${provinces}" var="v">
		  <span class="secitem" data-value="${v}">${v}</span>
		  </c:forEach>
    </div>
    <div class="inline field" >
       <label>运营商:</label>
       <input type="hidden" name="operators"/>
		  <span class="secitem ui blue circular label" data-value="">全部</span>
		  <c:forEach items="${operators}" var="v">
		  <span class="secitem" data-value="${v.value}">${v.value}</span><!-- 用名称传值查询 -->
		  </c:forEach>
    </div>
    <div class="inline field" >
       <label>厂商:</label>
       <input type="hidden" name="channel"/>
		  <span class="secitem ui blue circular label" data-value="">全部</span>
		  <c:forEach items="${channels}" var="v">
		  <span class="secitem" data-value="${v.key}">${v.value}</span>
		  </c:forEach>
    </div>
  	<div class="inline field" >
       <label>规格:</label>
       <input type="hidden" name="packageSize"/>
		   <span class="secitem ui blue circular label" data-value="">全部</span>
		  <c:forEach items="${pkgs}" var="v">
		  <span class="secitem" data-value="${v.key}">${v.value}</span>
		  </c:forEach>
    </div>
  	<div class="inline field" >
       <label>订购渠道:</label>
       <input type="hidden" name="partner"/>
		   <span class="secitem ui blue circular label" data-value="">全部</span>
		  <c:forEach items="${partners}" var="v">
		  <span class="secitem" data-value="${v.key}">${v.value}</span>
		  </c:forEach>
    </div>
  	<div class="inline field" >
       <label>支付方式:</label>
		  <input type="hidden" name="payStyle">
		  <span class="secitem ui blue circular label" data-value="">全部</span>
		  <c:forEach items="${payStyles}" var="v">
		  <span class="secitem" data-value="${v.key}">${v.value}</span>
		  </c:forEach>
    </div>
    
  </div>
  </form>


 <!-- single line: 确保不换行 -->
	<table id="table" class="ui inverted brown table single line selectable center aligned" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>序号</th>
				<th>订单号</th>
				<th>订单状态	</th>
				<th>充值时间</th>
				<th>充值反馈时间	</th>
				<th>手机号</th>
				<th>归属地</th>
				<th>运营商</th>
				<th>厂商</th>
				<th>规格</th>
				<th>订购渠道</th>
				<th>售卖价</th>
				<th>支付方式</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
	
	</div>
	
<script type="text/javascript">
var stat = (function() {
	var lastCondition={};	//table翻页时要取旧参数
	var table, tableOpt = {
     	"ajax": {
			"url":'${path}/api/orderInfo',
            "type": "POST",
			"data":function(d){
				$.extend(d,lastCondition);
			}
		},
		//buttons: ['colvis','excel' ],
        //language: {buttons: {colvis: '选择列',excel:'导出'}},
		"searching": false,
		"ordering": false,
		"lengthChange": false,
		"lengthMenu": [50,100],
		"processing": true,
		//"pagingType":'simple',
        "serverSide": true,
        fixedHeader: {//需要引入插件js/css
            //headerOffset: $('#top_navbar_main').height(),
            header: false
        },
        "columns": [
            { "data": "recid"},
			{ "data": "orderId"},
            { "data": "resultCode" },
            { "data": "createTime" },
            { "data": "updateTime" },
            { "data": "phone" },
			{ "data": "province" },
            { "data": "operators" },
            { "data": "channel" },
            { "data": "flowSize" },
            { "data": "partner",sDefaultContent: ""},
            { "data": "price" },
            { "data": "payStyle" }
         ],
         "initComplete": function(settings, json) {
        	 /*
	    	 table.buttons().container()
	    		.appendTo( $('div.eight.column:eq(0)', table.table().container()) );
        	 */
	   	 },
         drawCallback: function() {
         },
	     "columnDefs": [
		 ]
	};
	
	function validateParam() {
		var data={};
		var arr=$.grep($('#searchForm :input'), function(n,i){
			return $('#more').is(':visible') || $(n).parents("#more").length==0;
		});
        jQuery.each( arr, function(i, field){
        	if(name=$(field).attr('name')) 
        		data[name]=$(field).val();  
       	});
		lastCondition = data;
		var msg = ((!lastCondition.startTime||!lastCondition.endTime)&&'请选择日期');  
		if(msg) {
			alert(msg);return false;
		}
		var search=lastCondition.searchValue;
		search && (lastCondition[lastCondition.searchType]=search);
		delete lastCondition.searchType;
		delete lastCondition.searchValue;
		/*
		$.each(lastCondition, function(i,n){
			if(n=='all') lastCondition[i]='';
		});
		*/
		return true;
	}
	
	function search(btn) {
		if(validateParam()) {
			//$(btn).addClass('disabled loading');
			if(table) {	table.ajax.reload();
			} else {
				table=$('#table').DataTable(tableOpt);
			}
		}		
	}
	
	function download(btn) {
		if(validateParam()) {
			var arr=[];
			var keys=Object.keys(lastCondition);
			for(var i in keys) {
				arr.push(keys[i]+'='+lastCondition[keys[i]]);
			}
			window.location.href='${path}/api/downloadOrderInfo?'+arr.join('&');
		}
	}
	
	function toggle(btn) {
		$('#more').slideToggle(function () {
			var i=$(btn).find('i'), fields=$('#more').find('select');
			if(i.hasClass('down')) {
				i.removeClass('down').addClass('up').attr('v','up');
				$(btn).attr('title','收起更多条件');
				fields.removeAttr('disabled');
			} else {
				i.removeClass('up').addClass('down').attr('v','down');
				$(btn).attr('title','展示更多条件');
				fields.attr('disabled','disabled');
			}
		});
	}
	
	return {
		tableOpt:tableOpt,
		table:table, 
		download:download,
		search:search,
		toggle:toggle
	}
})();

	$('.selection.dropdown').dropdown();
	
	$('#rangestart').calendar({ 
		maxDate : new Date('${end}'),
		endCalendar: $('#rangeend')		
	});
	$('#rangeend').calendar({	
		maxDate : new Date('${end}'),
		startCalendar: $('#rangestart')	
	});

	$('.ui.dropdown').dropdown();
	$('.primary.button').click();
	$('.secitem').click(function(){
		$(this).siblings().removeClass('ui blue label circular');
		$(this).addClass('ui blue label circular');
		$(this).siblings(':hidden').val($(this).attr('data-value'));
		$('#search').click();
	})
	
</script>

