<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp"%>
<style type="text/css">
div.slider {
    display: none;
    margin: 0 20px 0;
}
</style>
<script src="static/js/jquery.form.js"></script>
<script src="static/js/my.js"></script>

<div style="margin: 0 12% 0;">

<div class="ui right aligned grid">
<div class="sixteen wide column">
	<button class="ui primary button" id="addBtn">添加</button>
  </div>
</div>

 <!-- single line: 确保不换行 -->
	<table id="table" class="ui inverted brown table single line center aligned compact" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>#</th>
				<th>流量供应商</th>
				<th>运营商</th>
				<th>省份</th>
				<th>维护开始时间</th>
				<th>维护结束时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
	
	</div>
	
 <div class="ui popup" id="alert" style="min-width: 300px;">
	  <div class="header"><i class="warning sign icon"></i>删除?</div>
	  <div class="content">
	  <div class="" style="float: right;">
		  <div class="ui tiny button" onclick="$('.ui.popup').popup('hide all');">不了 </div>
	      <div class="ui red tiny button" onclick="pause._delete(this)">是的</div>
	      <input type="hidden"/>
	  </div>
	  </div>
	</div>
  
  <div id="addWindow" class="ui small modal">
  <div class="heiti header">添加</div>
  <div class="content">
  <form id="addForm"  method="post" action="api/savePauseInfo"  class="ui mini form">
   		<div class="fields">
   		<input type="hidden"  autofocus="true"/>
	 	 		<div class="field">
			       	<label>流量供应商</label>
					<select class="ui dropdown" name="partner">
					  <c:forEach items="${partners}" var="v">
					  <option value="${v.key}">${v.value}</option>
					  </c:forEach>
					</select>
				</div>
		 	 	<div class="field">
			       <label>运营商</label>
					 <select class="ui dropdown" name="operators">
					 <c:forEach items="${operators}" var="v">
					  <option value="${v.key}">${v.value}</option>
					 </c:forEach>
					</select>
				</div>
		 	 	<div class="field">
			       <label>省份</label>
					 <select class="ui dropdown" name="province">
					  <option value="全国">全国</option>
					 <c:forEach items="${provinces}" var="v">
					  <option value="${v}">${v}</option>
					 </c:forEach>
					</select>
				</div>
			</div>
			<div class="fields">
			<div class="field">
			       <label>开始时间</label>
			       <div class="ui calendar" id="rangestart">
			         <div class="ui input left icon">
			           <i class="calendar icon"></i>
			           <input name="startTime" type="text" value="${start}" autocomplete="off">
			         </div>
			       </div>
			    </div>
			    <div class="field" >
			      <label>结束时间</label>
			      <div class="ui calendar" id="rangeend">
			        <div class="ui input left icon">
			          <i class="calendar icon"></i>
			          <input name="endTime" type="text" value="${end}" autocomplete="off">
			        </div>
			      </div>
			    </div>
			</div>
  <div class="ui error message"></div>
  </form>
  </div>
  <div class="actions">
      <div class="ui cancel button">取消</div>
      <div class="ui positive right labeled icon button">提交<i class="checkmark icon"></i> </div>
    </div>
  </div>
  
  <div id="formMsg" class="ui dimmer">
    <div class="content">
      <div class="center">
        <h2 class="ui inverted icon header"><i class="checkmark icon"></i> 添加成功! </h2>
      </div>
    </div>
  </div>
  
  
<script type="text/javascript">
var pause = (function() {
	var table, tableOpt = {
		"ajax": {
            "url": "${path}/api/pauseInfo",
            "type": "GET"
     	},
		"searching": false,
		"ordering": false,
		"lengthChange": false,
		"lengthMenu": [ 10,100],
		"processing": true,
		//"pagingType":'simple',
        "serverSide": false,
        "columns": [
            { "data": "__dt__sequence__" },
            { "data": "partner" },
            { "data": "operators" },
            { "data": "province" },
            { "data": "startTime"},
            { "data": "endTime"},
            { "data": null, "render": function ( data, type, row, meta) {
            	return '<button class="ui mini inverted button" delbtn data-value='+row.id+'><i class="remove icon"></i>删除</button>';
            }},
            { "data": "id",visible:false},
         ],
         "initComplete": function(settings, json) {
        	 //pause.table.buttons().container().appendTo( $('div.eight.column:eq(0)', pause.table.table().container()) ); //TODO 如何自定义btn位置
	   	 },
         fnRowCallback: function( nRow, aData, iDisplayIndex ) {
       	 },
       	 drawCallback: function() {
        	 $('button[delbtn]').popup({
	   			on:'click',
	   			hoverable:true,
	   			position:'top right',
	   			onShow: function(module) {
	   				$(this).find(':hidden').val($(module).attr('data-value'));
	   			},
	   		    popup : $('#alert')
	   		  })
	   		;
         },
	     "columnDefs": [
		 ]
	};
	
	function _delete(elem) {
		var id=$(elem).next(':hidden').val();
		$.post('${path}/api/delPauseInfo',{ids:id},function(data){
			pause.table.ajax.reload();
		},'json');
		$('.ui.popup').popup('hide all');
	}
	
	return {
		_delete:_delete,
		table:table, 
		tableOpt:tableOpt
	}
})();

	pause.table = $('#table').DataTable(pause.tableOpt); 
	$('[name=operators],[name=channel]').dropdown({
		onChange:function(value, text, $choice){
			pause.table.ajax.reload();
		}
	});
	$('#addForm').form();
	
	$('#addWindow').modal({
		closable:false,
		//blurring: true,	//duration和blurring在chrome下帧数太低
		duration:100,
		//transition:'drop',
		onVisible : function(){
			var $form=$(this).find('form');
			$form.form({
			    fields: {
			    	startTime  : 'empty',
			    	endTime  : 'empty'
		    	}
			});
			$(this).find('.ui.dropdown').dropdown({showOnFocus:false});
			var start=$(this).find('#rangestart'),
			end=$(this).find('#rangeend');
			start.calendar({ 
				type:'datetime',
				endCalendar: end	
			});
			end.calendar({ 
				type:'datetime',
				startCalendar: start		
			});
		},
		onHidden: function(){
			var $form=$(this).find('form');
			$form.find('.ui.error.message').html('');
			$form.form('reset');
		},
		onApprove: function() {
			var form=$(this).find('form');
			if(!form.form('validate form'))return false; //不要直接使用domId. 原因和modal有关,具体不明
			var ret;
			form.ajaxSubmit({
				async:false,//同步提交
				success: function(data){
		    		data=$.parseJSON(data);
		    		if(data.result_code==0){
		    			pause.table.ajax.reload();
		    			$('#formMsg').dimmer('show');
		    			window.setTimeout(function(){
	    			    	$('#formMsg').dimmer('hide');
		    				$('#addWindow').modal('close');
		    			},1500);
		    			ret=true;
		    		} else {
		    			alert(data.result_msg);
						ret=false;
		    		}
		    	}
			});
			return ret;
		}
	}).modal('attach events', '#addBtn', 'show');
</script>

