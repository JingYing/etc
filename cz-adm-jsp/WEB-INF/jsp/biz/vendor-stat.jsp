<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp"%>
<style type="text/css">
div.slider {
    display: none;
    margin: 0 20px 0;
}
</style>
<div style="margin: 0 10% 0;">

<form id="searchForm" class="ui tiny form " onsubmit="return false;">
<input type="hidden" name="teddy" value="teddy">
<div class="fields">
    <div class="field two wide" >
       <label>运营商</label>
		 <select class="ui compact dropdown" name="operators">
		  <option value="0">全部</option>
		  <c:forEach items="${operators}" var="v">
		  <option value="${v.key}">${v.value}</option>
		  </c:forEach>
		</select>
    </div>
    <div class="field two wide" >
       <label>归属地</label>
		 <select class="ui compact dropdown" name="province">
		  <option value="all">全部</option>
		  <c:forEach items="${provinces}" var="v">
		  <option value="${v}">${v}</option>
		  </c:forEach>
		</select>
    </div>
    <div class="field two wide" >
       <label>统计方式</label>
		 <select class="ui compact dropdown" name="statType">
		  <option value="1">按天</option>
		  <option value="2">按月</option>
		</select>
    </div>
    <div class="field two wide">
       <label>开始日期</label>
       <div class="ui calendar" id="rangestart">
         <div class="ui input left icon">
           <i class="calendar icon"></i>
           <input name="startTime" type="text" value="${start}" placeholder="包含所选日期" autocomplete="off">
         </div>
       </div>
    </div>
    <div class="field two wide">
      <label>结束日期</label>
      <div class="ui calendar" id="rangeend">
        <div class="ui input left icon">
          <i class="calendar icon"></i>
          <input name="endTime" type="text" value="${end}" placeholder="包含所选日期" autocomplete="off">
        </div>
      </div>
    </div> 
    <div class="field one wide">
    	<button class="ui primary tiny button" onclick="stat.search(this)" style="position:absolute;bottom:4%;">查询</button>
    </div>
  </div>
  </form>


 <!-- single line: 确保不换行 -->
	<table id="table" class="ui inverted brown table single line selectable center aligned" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>统计时间</th>
				<th>付款订单数</th>
				<th>付款订单金额	</th>
				<th>成功订单</th>
				<th>成功订单金额	</th>
				<th>充值中订单数</th>
				<th>充值中金额</th>
				<th>失败订单	</th>
				<th>失败订单金额	</th>
				<th>利润金额	</th>
				<th>成功率</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>
	
<script type="text/javascript">
var stat = (function() {
	var lastCondition=collectParam({});	//table翻页时要取旧参数
	var table, tableOpt = {
     	"ajax": {
			"url":'${path}/api/vendorstat',
            "type": "POST",
			"data":function(d){
				$.extend(d,lastCondition);
			}
		},
		"searching": false,
		"ordering": false,
		"lengthChange": false,
		"lengthMenu": [ 99999],
		"processing": true,
		//"pagingType":'simple',
        "serverSide": false,
        fixedHeader: {//需要引入插件js/css
            //headerOffset: $('#top_navbar_main').height(),
            header: false
        },
        "columns": [
			{ "data": "date"},
            { "data": "orders"},
            { "data": "money" },
            { "data": "successOrders" },
            { "data": "successMoney" },
            { "data": "rechargingOrders" },
			{ "data": "rechargingMoney" },
            { "data": "failOrders" },
            { "data": "failMoney" },
            { "data": "profit" },
            { "data": "successRate","render": function ( data, type, row, meta) {
            	return data+'%';
            } }
         ],
         drawCallback: function() {
         },
	     "columnDefs": [
		 ]
	};
	
	function collectParam(data) {
		var arr = $('#searchForm').serializeArray();
        jQuery.each( arr, function(i, field){
        	data[field.name] = field.value;  
       	});
        return data;
	}
	
	function search(btn) {
		lastCondition = collectParam({});
		var msg = ((!lastCondition.startTime||!lastCondition.endTime)&&'请选择日期');  
		if(msg) {
			alert(msg);return false;
		}
		if(lastCondition.statType==2) {
			lastCondition.startTime+='-01';
			lastCondition.endTime+='-01';
		}
		if(lastCondition.province=='all')	lastCondition.province='';
		//$(btn).addClass('disabled loading');
		
		if(table) {
			table.ajax.reload();
		} else {
			table=$('#table').DataTable(tableOpt);
		}
	}
	
	
	return {
		tableOpt:tableOpt,
		table:table, 
		search:search
	}
})();

	$('.selection.dropdown').dropdown();
	
	$('select[name=statType]').change(function(){
		if(this.value==1) {
			$('#rangestart').calendar({ 
				maxDate : new Date('${end}'),
				endCalendar: $('#rangeend')		
			});
			$('#rangeend').calendar({	
				maxDate : new Date('${end}'),
				startCalendar: $('#rangestart')	
			});
		} else {
			$('#rangestart').calendar({ 
				type:'month',
				maxDate : new Date('${end}'),
				endCalendar: $('#rangeend')		
			}).calendar('clear');
			$('#rangeend').calendar({	
				type:'month',
				maxDate : new Date('${end}'),
				startCalendar: $('#rangestart')	
			}).calendar('clear');
		}
	}).change();

	$('.ui.dropdown').dropdown();
	$('.primary.button').click();
	
</script>

