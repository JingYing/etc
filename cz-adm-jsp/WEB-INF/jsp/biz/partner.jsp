<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp"%>
<style type="text/css">
/*
div.slider {
    display: none;
    margin: 0 20px 0;
}
*/
</style>
<script src="static/js/jquery.form.js"></script>
<script src="static/js/my.js"></script>
<script src="static/js/vue.min.js"></script>

<div style="margin: 0 10% 0;">

<div id="partner-segments" class="ui grid divided">
  <div class="three wide column">
      <div class="ui vertical pointing fluid inverted brown menu">
	    <div class="header item">1.选择厂商</div>
		<a class="item" v-for="(v, index) in channels" :key="v.key" v-on:click="activeChannel(index)" v-bind:class="{ active: v.active }">{{v.value}}</a>
	  </div>
  </div>
  <div class="two wide column" style="min-width: 150px; max-width: 15%;">
    <div class="ui vertical pointing fluid inverted brown menu">
    <div class="header item">2.选择运营商</div>
	<a class="item" v-for="(v, index) in operators" v-on:click="activeOperator(index)" 
		v-bind:class="{ active: v.active }">{{v.value}}</a>
	</div>
  </div>
  <div class="twelve wide column ui segment inverted brown" style="max-width: 65%;">
	    <div class="item">
	    	<button class="ui inverted yellow button" v-on:click="save"><i class="icon save"></i>保存</button>
	    	<button class="ui inverted button" v-on:click="restore"><i class="icon history"></i>还原..</button>
	    	<button class="ui inverted button" name="batchSet"><i class="icon edit"></i>全部设为..</button>
	    	<div class="ui popup" style="min-width: 100%;">
	    		<div class="ui grid">
	    		<div class="thirteen wide column">
				<div class="ui multiple selection dropdown" id="popup-dropdown" style="min-width:100%;"> 
				  <input type="hidden" id="batchSetValue"/>
				  <i class="dropdown icon"></i>
				  <div class="default text">配置供应商</div>
				  <div class="menu">
				    <div class="item" v-for="partner in partners" :data-value="partner.key">{{partner.value}}</div>
				  </div>
	    		</div>
	    		</div>
	    		<div class="three wide column">
				<button class="ui fluid secondary button" v-on:click="batchSet($event)">填写到下方</button>
				</div>
				</div>
			</div>
			<div class="ui small inline inverted loader" v-bind:class="{active:loading}"></div>
			<span v-bind:class="{ hidden: !showSuccess}">保存成功</span>
	    </div>
  
	   <div class="ui divided list">
		  <div class="item" v-for="(v,index) in settings" :key="v.key" style="position: relative; line-height: 50px;">
		  	{{v.province}} 
			<div class="ui multiple selection dropdown" name="p-selection" :data-index="index" style="position: absolute; left:50px;bottom:10px;"> 
			  <input type="hidden" :data-beforechange="v.partner"/>
			  <i class="dropdown icon"></i>
			  <div class="default text">配置供应商</div>
			  <div class="menu">
			    <div class="item" v-for="partner in partners" :data-value="partner.key">{{partner.value}}</div>
			  </div>
			</div>
		  </div>
		</div>
	</div>

</div>  
</div>  

	<div id="restoreWindow" class="ui small modal">
		<div class="header">选择要还原的厂商配置</div>
		<div class="content">
			<div class="ui one column relaxed equal height divided grid" id="grid">
			    <div class="column">
			      <div class="ui link list">
			      	<c:forEach items="${channelList }" var="c">
			        <a class="item">&nbsp;&nbsp;&nbsp;
			        	<div class="ui checkbox">
					      <input id="cbox_${c.key}" type="checkbox" value="${c.key}">
					      <label for="cbox_${c.key}">${c.value}</label>
					    </div>
			        </a>
			      	</c:forEach>
			      </div>
			  </div>
			  </div>
		</div>
		<div class="actions">
		 	<div class="ui cancel button">取消</div>
		 	<div class="ui positive right labeled icon button">还原为默认配置<i class="checkmark icon"></i> </div>
		 </div>
	 </div>



  
<script type="text/javascript">
var operators = ${operators},channels=${channels},partners=${partners},path='${path}';
</script>
<script type="text/javascript" src="static/js/biz/partner.js"></script>

