<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title>欢迎登录</title>
	<link rel="stylesheet" href="static/semantic/semantic.min.css">
	<link rel="stylesheet" href="static/datatables/css/dataTables.semanticui.min.css">
	<style type="text/css">
	.no-webp .login-body {
	  	background: url("static/img/main_bg.png") no-repeat top !important;
	  	background-color: #DADADA;
		background-size: 100% auto !important; 
		background-attachment:fixed !important;
	}
	
	.webp .login-body{
	  	background: url("static/img/main_bg.webp") no-repeat top !important;
	  	background-color: #DADADA;
		background-size: 100% auto !important; 
		background-attachment:fixed !important;
	}

    body,input,div,button {
      font-family:"Microsoft Yahei" !important;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
	<script src="static/js/modernizr-webp.js"></script>
	<script src="static/js/jquery-3.1.0.min.js"></script>
	<script src="static/js/ajax-noauth.js"></script>
	<script src="static/js/md5.js"></script>
	<script src="static/semantic/semantic.min.js"></script>
	<script src="static/datatables/js/jquery.dataTables.min.js"></script>
	<script src="static/datatables/js/dataTables.semanticui.min.js"></script>
	<script type="text/javascript">
		var formOption = {
           fields: {
	            account: {
	              rules: [{type   : 'empty',prompt : '请输入您的用户名'}]
	            },
	            pwd: {
	              rules: [{type   : 'empty',prompt : '请输入您的密码'}]
	            },
	            captcha: {
	            	rules: [{type   : 'empty',prompt : '请输入验证码'}]
	            }
	          },
	          onSuccess : function (event, fields){
	        	  $('#btn').addClass('loading disabled');
	        	  $.post($('form')[0].action, {account:$('#account').val(), pwd:md5($('#pwd').val()), captcha:$('#captcha').val()}, function(data){
	        	  	  $('#btn').removeClass('loading disabled');
	        		  if(data.err==0) window.location.href="${path}/";
	        		  else {
	        			  refresh();
	        			  $('#hint').html('用户名、密码或验证码错误').show();
	        			  $('#pwd').val('').focus();
	        		  }
	        	  }, 'json');
	        	  return false;
	          }
        }
		
		function refresh() {
			$.get('${path}/passport/refreshCaptcha.do',function(data){
				$('#captchaImg').attr('src', data);
			});
		}
		$(function(){
			$('.ui.form').form(formOption);
			$('form:first *:input[type!=hidden]:first').focus();
			$('#captchaImg').click(refresh);
		})
		
	</script>
</head>
<body class="login-body">
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <img src="static/img/logo3.png" class="image">
      <div class="content" style="color:white;">欢迎登录泰迪熊</div>
    </h2>
    <form class="ui large form" method="post" action="passport/api/login.do">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" id="account" name="account" placeholder="用户名">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" id="pwd" name="pwd" placeholder="密码">
          </div>
        </div>
	        <div class="field">
	          <div class="ui input">
	            <input type="text" id="captcha" name="captcha" placeholder="请输入右侧验证码"  autocomplete="off">
	            &nbsp;&nbsp;&nbsp;&nbsp;
	            <img id="captchaImg" src="${captcha}" width="100" height="30" style="cursor:pointer">
	          </div>
	        </div>
        <div id="btn" class="ui fluid large teal submit button">登&nbsp;&nbsp;&nbsp;录</div>
      </div>

      <div id="hint" class="ui error message">
      </div>

    </form>
<%--
    <div class="ui message">
      没有账号? <a href="#">注册</a>
    </div>
 --%>
  </div>
</div>

</body>
</html>