<%@ page contentType="text/html;charset=UTF-8" import="java.io.*"%>
<%@ include file="../taglib.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<html>
<base href="<%=basePath%>" />
<meta charset=utf-8>
<meta name=viewport	content="initial-scale=1, minimum-scale=1, width=device-width">
<title>出错了</title>
<style>
* {
	margin: 0;
	padding: 0
}

html, code {
	font: 15px/22px arial, sans-serif
}

html {
	background: #fff;
	color: #222;
	padding: 15px
}

body {
	margin: 7% auto 0;
	max-width: 390px;
	min-height: 180px;
	padding: 30px 0 15px
}

*>body {
	background: url('img/robot.png') 100% 5px
		no-repeat;
	padding-right: 205px
}

p {
	margin: 11px 0 22px;
	overflow: hidden
}

ins {
	color: #777;
	text-decoration: none
}

a img {
	border: 0
}

@media screen and (max-width:772px) {
	body {
		background: none;
		margin-top: 0;
		max-width: none;
		padding-right: 0
	}
}

#logo {
	background:
		url()
		no-repeat;
	margin-left: -5px
}

@media only screen and (min-resolution:192dpi) {
	#logo {
		background:
			url()
			no-repeat 0% 0%/100% 100%;
		-moz-border-image:
			url()
			0
	}
}

@media only screen and (-webkit-min-device-pixel-ratio:2) {
	#logo {
		background:
			url()
			no-repeat;
		-webkit-background-size: 100% 100%
	}
}

#logo {
	display: inline-block;
	height: 54px;
	width: 150px
}
</style>
<script type="text/javascript">
	if (self != top) { //处理框架页+地址栏访问时的错误页
		top.location.href = location;
	} //其它为地址栏正常访问
</script>
<span id=logo></span>
<p>
	<b>500.</b>
	<ins>出错了,请联系网站技术人员.</ins>

<div style="display: none">
	${stacktrace}
</div>