package com.jd.supplier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;


//@ContextConfiguration(locations="classpath:testSpring.xml")
@ContextConfiguration(locations={"classpath:spring-config.xml","classpath:spring-config-mvc.xml"})
public class BaseTest extends AbstractJUnit4SpringContextTests{

}
