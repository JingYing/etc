package com.jd.supplier.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static void main(String args[]) {
	DateUtil dateUtil=new DateUtil ();
	System.out.println(dateUtil.dayMove(new Date(), -7));
	System.out.println(dateUtil.monthMove(new Date(), -1));
    }

    public static String dayMove(Date date, int len) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	try {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.add(Calendar.DATE, len);
	    return sdf.format(cal.getTime());
	} catch (Exception e) {
	    return "";
	}
    }

    public static String monthMove(Date date, int len) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	try {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.add(Calendar.MONTH, len);
	    return sdf.format(cal.getTime());
	} catch (Exception e) {
	    return "";
	}
    }
   
    public static String DateToStrHM(Date date){  
     SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
     String date1=dd.format(date); 
     return date1;
    }
}
