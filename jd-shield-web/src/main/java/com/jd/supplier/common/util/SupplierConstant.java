package com.jd.supplier.common.util;

/**
 * @author duandongdong
 * @date 2014年5月21日
 * @desc 
 */
public class SupplierConstant {
    // 0表示不可用
    public final static String AVAILABLESTATUS = "1";
    // 1表示可用
    public final static String NOTAVAILABLESTATUS = "0";
    // 需求状态 begin
    // 需求提交
    public final static String REQCOMMIT = "1";
    public final static String REQCOMMIT_CN = "需求提交";
    // 需求确认
    public final static String REQCONFIG = "2";//需求确认
    public final static String REQREFUSE = "3";//驳回需求
    public final static String REQCONFIG_CN = "需求确认";
    //需求立项
    public final static String REQPROJECT_CN = "需求立项";
    //输出中标结果
    public final static String OUTPUTTIME_CN = "输出中标结果";
    //需求确认会阶段
    public final static String REQCONFIGMETTING = "4";
    
    //需求状态  end 
    
    //附件业务类型 begin
    public final static String BUSSINETYPE_REQCOMMIT="1";
    public final static String BUSSINETYPE_REQADUITMETTING="2";
    
    //上传附件业务类型  end
    
    
    //任务状态
    //任务指派
    public final static String TASKASSIGN = "1";
    
    //公告的类型
    public final static String NOTICETYPE_BIDNOTICE = "1";
    public final static String NOTICETYPE_SELECTEDNOTICE = "2";
    public final static String NOTICETYPE_WINBIDNOTICE = "3";

}
