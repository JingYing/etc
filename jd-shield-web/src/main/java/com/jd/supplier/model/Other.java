package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

public class Other extends IdModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long supplierId;

	private String isCooperation;

	private String cooperation;

	private String isVisit;

	private String mark;

	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getIsCooperation() {
		return isCooperation;
	}

	public void setIsCooperation(String isCooperation) {
		this.isCooperation = isCooperation == null ? null : isCooperation
				.trim();
	}

	public String getCooperation() {
		return cooperation;
	}

	public void setCooperation(String cooperation) {
		this.cooperation = cooperation == null ? null : cooperation.trim();
	}

	public String getIsVisit() {
		return isVisit;
	}

	public void setIsVisit(String isVisit) {
		this.isVisit = isVisit == null ? null : isVisit.trim();
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark == null ? null : mark.trim();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
}