package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.jd.official.core.model.IdModel;

public class Supplier extends IdModel {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    public Supplier(){
    	GregorianCalendar gc=new GregorianCalendar();
    	applyCreateTime = gc.getTime();
    }
    
    private String name;

    private String userCode;

    private String password;

    private String mobile;

    private String email;

    private Long recommended;

    private String dataIntegrity;

    private String auditReason;

    private String supplierStatus;

    private String dataStatus;

    private String recommendStatus;

    private String recommendContent;

    private String mark;

    private List<Contacts> contacts = null;

    private Enterprise enterprise;

    private Finance finance;

    private Quality quality;

    private Other other;

    private List<RequirementAffix> affix = null;
    
    private String infoStatus;

    private String realName;

    //页面显示属性   报名项目日期
    private Date applyCreateTime;
    

    public Date getApplyCreateTime() {
        return (Date)applyCreateTime.clone();
    }

    public void setApplyCreateTime(Date applyCreateTime) {
    	if(applyCreateTime != null)
			this.applyCreateTime = new Date(applyCreateTime.getTime());
		else
			this.applyCreateTime = null;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name == null ? null : name.trim();
    }

    public String getUserCode() {
	return userCode;
    }

    public void setUserCode(String userCode) {
	this.userCode = userCode == null ? null : userCode.trim();
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password == null ? null : password.trim();
    }

    public String getMobile() {
	return mobile;
    }

    public void setMobile(String mobile) {
	this.mobile = mobile;
    }

    public static long getSerialversionuid() {
	return serialVersionUID;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email == null ? null : email.trim();
    }

    public String getDataIntegrity() {
	return dataIntegrity;
    }

    public void setDataIntegrity(String dataIntegrity) {
	this.dataIntegrity = dataIntegrity;
    }

    public String getAuditReason() {
	return auditReason;
    }

    public void setAuditReason(String auditReason) {
	this.auditReason = auditReason == null ? null : auditReason.trim();
    }

    public String getSupplierStatus() {
	return supplierStatus;
    }

    public void setSupplierStatus(String supplierStatus) {
	this.supplierStatus = supplierStatus == null ? null : supplierStatus.trim();
    }

    public String getDataStatus() {
	return dataStatus;
    }

    public void setDataStatus(String dataStatus) {
	this.dataStatus = dataStatus == null ? null : dataStatus.trim();
    }

    public List<Contacts> getContacts() {
	return contacts;
    }

    public void setContacts(List<Contacts> contacts) {
	this.contacts = contacts;
    }

    public Enterprise getEnterprise() {
	return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
	this.enterprise = enterprise;
    }

    public Quality getQuality() {
	return quality;
    }

    public void setQuality(Quality quality) {
	this.quality = quality;
    }

    public Other getOther() {
	return other;
    }

    public void setOther(Other other) {
	this.other = other;
    }

    public List<RequirementAffix> getAffix() {
	return affix;
    }

    public void setAffix(List<RequirementAffix> affix) {
	this.affix = affix;
    }

    public Finance getFinance() {
	return finance;
    }

    public void setFinance(Finance finance) {
	this.finance = finance;
    }

    public Long getRecommended() {
	return recommended;
    }

    public void setRecommended(Long recommended) {
	this.recommended = recommended;
    }

    public String getRealName() {
	return realName;
    }

    public void setRealName(String realName) {
	this.realName = realName;
    }

    public String getRecommendStatus() {
	return recommendStatus;
    }

    public void setRecommendStatus(String recommendStatus) {
	this.recommendStatus = recommendStatus;
    }

    public String getRecommendContent() {
	return recommendContent;
    }

    public void setRecommendContent(String recommendContent) {
	this.recommendContent = recommendContent;
    }

    public String getMark() {
	return mark;
    }

    public void setMark(String mark) {
	this.mark = mark;
    }

	public String getInfoStatus() {
		return infoStatus;
	}

	public void setInfoStatus(String infoStatus) {
		this.infoStatus = infoStatus;
	}
    
}