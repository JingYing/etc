package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;

public class Notice extends IdModel {

    private static final long serialVersionUID = 1659168822681619710L;
    
    public Notice(){
    	GregorianCalendar gc=new GregorianCalendar();
    	biddate = gc.getTime();
    	publishdate = gc.getTime();
    	enddate = gc.getTime();
    	
    }
    
    private String title;

    private Long requirementId;
    
    private String requirementName;
    
    private Long supplierId;
    
    private String supplierName;

    private String content;

    private Date biddate;
    
    private String biddatestr;

    private Date publishdate;
    
    private String publishdatestr;

    private String bidprincipal;

    private String phone;

    private String telephone;

    private String email;

    private Date enddate;
    
    private String enddatestr;

    private String status;
    
    private String type;
    
    
    
    public String getBiddatestr() {
        return biddatestr;
    }

    public void setBiddatestr(String biddatestr) {
        this.biddatestr = biddatestr;
    }

    public String getPublishdatestr() {
        return publishdatestr;
    }

    public void setPublishdatestr(String publishdatestr) {
        this.publishdatestr = publishdatestr;
    }

    public String getEnddatestr() {
        return enddatestr;
    }

    public void setEnddatestr(String enddatestr) {
        this.enddatestr = enddatestr;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRequirementName() {
        return requirementName;
    }

    public void setRequirementName(String requirementName) {
        this.requirementName = requirementName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getRequirementId() {
	return requirementId;
    }

    public void setRequirementId(Long requirementId) {
	this.requirementId = requirementId;
    }


    public Date getBiddate() {
	return (Date)biddate.clone();
    }

    public void setBiddate(Date biddate) {
    	if(biddate != null)
			this.biddate = new Date(biddate.getTime());
		else
			this.biddate = null;
    }

    public Date getPublishdate() {
	return (Date)publishdate.clone();
    }

    public void setPublishdate(Date publishdate) {
    	if(publishdate != null)
			this.publishdate = new Date(publishdate.getTime());
		else
			this.publishdate = null;
    }

    public String getBidprincipal() {
	return bidprincipal;
    }

    public void setBidprincipal(String bidprincipal) {
	this.bidprincipal = bidprincipal == null ? null : bidprincipal.trim();
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone == null ? null : phone.trim();
    }

    public String getTelephone() {
	return telephone;
    }

    public void setTelephone(String telephone) {
	this.telephone = telephone == null ? null : telephone.trim();
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email == null ? null : email.trim();
    }

    public Date getEnddate() {
	return (Date)enddate.clone();
    }

    public void setEnddate(Date enddate) {
    	if(enddate != null)
			this.enddate = new Date(enddate.getTime());
		else
			this.enddate = null;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status == null ? null : status.trim();
    }
}