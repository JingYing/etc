package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

public class Finance  extends IdModel{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long supplierId;

    private String bankLicense;

    private String bankLinked;

    private String bankName;

    private String bankAccount;

    private String bankRate;

    private String taxType;

    private String currency;

    private Integer salesCount;   
    
    private String currencyTax;

    private Integer enterpriseTax;

    private String mark;

    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getBankLicense() {
        return bankLicense;
    }

    public void setBankLicense(String bankLicense) {
        this.bankLicense = bankLicense == null ? null : bankLicense.trim();
    }

    public String getBankLinked() {
        return bankLinked;
    }

    public void setBankLinked(String bankLinked) {
        this.bankLinked = bankLinked == null ? null : bankLinked.trim();
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount == null ? null : bankAccount.trim();
    }

    public String getBankRate() {
        return bankRate;
    }

    public void setBankRate(String bankRate) {
        this.bankRate = bankRate == null ? null : bankRate.trim();
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType == null ? null : taxType.trim();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

	public Integer getSalesCount() {
        return salesCount;
    }

    public void setSalesCount(Integer salesCount) {
        this.salesCount = salesCount;
    }

	public Integer getEnterpriseTax() {
        return enterpriseTax;
    }

    public void setEnterpriseTax(Integer enterpriseTax) {
        this.enterpriseTax = enterpriseTax;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark == null ? null : mark.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

	public String getCurrencyTax() {
		return currencyTax;
	}

	public void setCurrencyTax(String currencyTax) {
		this.currencyTax = currencyTax;
	}
    
}