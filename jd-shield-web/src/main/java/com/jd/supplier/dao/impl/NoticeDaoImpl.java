package com.jd.supplier.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.NoticeDao;
import com.jd.supplier.model.Notice;
import com.jd.supplier.web.vo.NoticeVO;

@Component("noticeDao")
public class NoticeDaoImpl extends MyBatisDaoImpl<Notice, Long> implements NoticeDao {

    @Override
    public Notice getByNodeId(Long nodeId) {
	return this.getSqlSession().selectOne("com.jd.supplier.model.Notice.getByNodeId", nodeId);
    }

    @Override
    public int countNotice(NoticeVO noticevo) {
	return (Integer)this.getSqlSession().selectOne("com.jd.supplier.model.Notice.countNotice", noticevo);
    }

    @Override
    public List<Notice> listNotice(NoticeVO noticevo) {
	return this.getSqlSession().selectList("com.jd.supplier.model.Notice.listNotice", noticevo);
    }

}
