package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Finance;

public interface IFinanceDao extends BaseDao<Finance, Long>{

}
