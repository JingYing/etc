package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Requirement;

public interface RequirementDao extends BaseDao<Requirement, Long> {

	Requirement getByNodeId(Long nodeId);
}
