package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.RequirementAffix;

public interface RequirementAffixDao extends BaseDao<RequirementAffix, Long> {
	RequirementAffix findByPara(RequirementAffix requirementAffix);
}
