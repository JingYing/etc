package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.IOtherDao;
import com.jd.supplier.model.Other;
@Component("otherDao")
public class OtherDaoImpl extends MyBatisDaoImpl<Other, Long> implements IOtherDao{

}
