package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.IFinanceDao;
import com.jd.supplier.model.Finance;
@Component("financeDao")
public class FinanceDaoImpl extends MyBatisDaoImpl<Finance, Long> implements IFinanceDao{

}
