package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Quality;

public interface IQualityDao extends BaseDao<Quality, Long>{

}
