package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.RequirementDao;
import com.jd.supplier.model.Requirement;

@Component("requirementDao")
public class RequirementDaoImpl extends MyBatisDaoImpl<Requirement, Long> implements RequirementDao{

	@Override
	public Requirement getByNodeId(Long nodeId) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.Requirement.getByNodeId", nodeId);
	}

}
