package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Equip;

public interface IEquipDao extends BaseDao<Equip, Long>{

}
