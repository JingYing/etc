package com.jd.supplier.service;


import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.Requirement;

public interface RequirementService extends BaseService<Requirement, Long> {
    }
