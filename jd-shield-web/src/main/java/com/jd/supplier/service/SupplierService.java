package com.jd.supplier.service;

import java.util.List;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.Supplier;

public interface SupplierService extends BaseService<Supplier, Long> {
	
	/**
	 * 
	 * @param supplier
	 */
	void save4Register(Supplier supplier);
	
	Supplier findByAccount(String account);
	
	/**
	 * 插入供应商基本信息
	 * @param supplier
	 * @return
	 * @throws Exception 
	 */
	public  int  insertData(Supplier supplier) throws Exception;


}
