package com.jd.supplier.service;

import java.util.Map;

import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Advertising;

public interface AdvertisingService {

	Map<String, String> findPositionValue();

	Page<Advertising> findByPosition(String position, int offset, int pageSize);

}
