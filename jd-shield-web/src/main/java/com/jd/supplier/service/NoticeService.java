package com.jd.supplier.service;

import com.jd.common.util.PaginatedList;
import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.Notice;
import com.jd.supplier.web.vo.NoticeVO;

public interface NoticeService extends BaseService<Notice, Long> {
    
    public PaginatedList<Notice> pageSearchSrSupplier(NoticeVO noticevo);
}
