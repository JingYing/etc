package com.jd.supplier.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.RequirementDao;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.service.RequirementService;

@Service("requirementService")
@Transactional
public class RequirementServiceImpl extends BaseServiceImpl<Requirement, Long> implements RequirementService {
    @Autowired
    private RequirementDao requirementDao;
    

    public RequirementDao getDao() {
	return requirementDao;
    }
    
    

}
