package com.jd.supplier.service;

import java.util.Map;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.Task;

public interface TaskService extends BaseService<Task, Long>{
	Boolean init(Map<String,Object> map);
}
