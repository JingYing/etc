package com.jd.supplier.web;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jd.common.security.Base32;
import com.jd.common.security.DESCoder;
import com.jd.official.core.exception.BusinessException;


/**
 * 在SPRING中配置密钥
 */
public class CookieUtils {
	
	private static final String CHARSET = "gbk";	//使用GBK
	
    private String key;		//密钥
    
    public CookieUtils()	{    }
    
    public CookieUtils(String key)	{
    	this.key = key;
    }

    /**
     * 加密
     * @param str
     * @return
     * @throw BusinessException 加密失败时抛异常
     */
    public String encrypt(String str) {
        try {
            return Base32.encode(
            			DESCoder.encrypt(
            				str.getBytes(CHARSET), key));
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }
    
    /**
     * 解密
     * @param str
     * @return
     * @throw BusinessException 解密失败时抛异常
     */
    public String decrypt(String str) {
        try {
            return new String(
            			DESCoder.decrypt(
            				Base32.decode(str), key), CHARSET);
        } catch (Exception e) {
        	throw new BusinessException(e);
        }
    }

	/**
     * 从cookie中取值值，会自动解密(如果是加密保存)。
     * 
     * @param servletRequest
     * @param name
     * @return
     */
    public  String getCookieValue(HttpServletRequest servletRequest, String name) {
        Cookie[] cookies = servletRequest.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    return decrypt(cookie.getValue());
                }
            }
        }
        return null;
    }
	
	/**
	 * 清除path为根路径下的cookie 
	 * @param servletResponse
	 * @param name
	 */
	public  void deleteCookie(HttpServletResponse servletResponse, String...name) {
		for(String s : name)	{
			Cookie cookie = new Cookie(s, null);
			cookie.setPath("/");
			cookie.setMaxAge(0);
			servletResponse.addCookie(cookie);
        }
    }
	
	public void setKey(String key) {
		this.key = key;
	}
}
