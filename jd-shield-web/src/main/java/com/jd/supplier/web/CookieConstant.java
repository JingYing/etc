package com.jd.supplier.web;

public class CookieConstant {
	
	/**
	 * 供应商用户的ID,该值在jsp中有引用,不要修改
	 * 加密
	 */
	public static final String USER_ID = "UID";	

	/**
	 * 供应商用户的名称,该值在jsp中有引用,不要修改.
	 * 不加密.使用utf-8 URLencode后存储
	 */
	public static final String USER_NAME = "UNAME";	
	
	/**
	 * 忘记密码时,向用户邮箱发送的验证码.
	 * 存储方式:向redis存放json, {"xxx@jd.com":"123456"}.key为邮箱地址, value为向该邮箱发送的验证码
	 */
	public static final String AUTHCODE_KEY = "nefkiohvcxm";
	
	/**
	 * 登录的图片验证码
	 * 存储方式:向redis存放
	 */
	public static final String LOGIN_AUTH_KEY = "lxjoielsdfla";
	
	/**
	 * 注册的图片验证码
	 * 存储方式:向redis存放
	 */
	public static final String REG_AUTH_KEY = "lasdfduelgorip";
	
	
	/**
	 * 自动登录时, cookie过期时间,秒
	 */
	public static final int DEFAULT_COOKIE_MAX_AGE = 3600 * 24 * 30;
	
}
