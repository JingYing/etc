package com.jd.supplier.web.controller;

import static com.jd.supplier.web.CookieConstant.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import nl.captcha.Captcha;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.official.core.mail.MailSendService;
import com.jd.supplier.common.Util;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.service.SupplierService;
import com.jd.supplier.web.CookieUtils;
import com.jd.supplier.web.vo.RegForm;


/**
 * 登录,注销,注册相关
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年5月19日
 */
@Controller
@RequestMapping(value="/passport")
public class PassportController {
	
	private static Logger log = Logger.getLogger(PassportController.class);
	
	@Resource
	private SupplierService supplierService; 
	@Resource
	private MailSendService mailSendService;
	@Resource
	private CookieUtils cookieUtil;
	@Resource
	private ShardedXCommands redisClient;
	
	/**
	 * 登录
	 * @param account
	 * @param pwd
	 * @param autoLogin
	 * @param resp
	 * @return 0成功,  1验证码过期, 2验证码不对, 3无此账号或密码不对, 4supplier.dataStatus不可用
	 */
	@RequestMapping(value="login", method=RequestMethod.POST)
	@ResponseBody
	public String login(@RequestParam String account, 
						@RequestParam String pwd, 
						@RequestParam String verify, 
						@RequestParam boolean autoLogin, 
						@CookieValue(LOGIN_AUTH_KEY)String loginAuthKey, 
						HttpServletResponse resp)	{
		
		int err;
		String loginAuth = getFromRedis(loginAuthKey);
		if(loginAuth == null)	{
			err = 1;
		} else if(!verify.equalsIgnoreCase(loginAuth)){
			err = 2;
		} else	{
			Supplier su = supplierService.findByAccount(account);
			if(su == null)	{
				err = 3;
			} else if(!"1".equals(su.getDataStatus())) {
				err = 4;
			} else if(su.getPassword().equalsIgnoreCase(pwd))	{
				String enc = su.getName();
				try {
					enc = URLEncoder.encode(su.getName(), "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
				Cookie c1 = new Cookie(USER_ID, cookieUtil.encrypt(su.getId()+""));
				Cookie c2 = new Cookie(USER_NAME, enc);
				c1.setPath("/");
				c2.setPath("/");
				if(autoLogin)	{
					c1.setMaxAge(DEFAULT_COOKIE_MAX_AGE);
					c2.setMaxAge(DEFAULT_COOKIE_MAX_AGE);
				}
				resp.addCookie(c1);
				resp.addCookie(c2);

				err = 0;
			} else {
				err = 3;
			}
		}
		
		removeRedis(loginAuth);
		removeCookie(resp, LOGIN_AUTH_KEY);
		return "{\"err\":" + err + "}";
	}
	
	/**
	 * 注销
	 * @param resp
	 * @return
	 */
	@RequestMapping("logout")
	public String logout(HttpServletResponse resp)	{
		cookieUtil.deleteCookie(resp, USER_ID, USER_NAME);
		return "redirect:/supplier/index";	//重定向到其它controller
	}
	
	/**
	 * 注册页面
	 * @return
	 */
	@RequestMapping("register")
	public ModelAndView register(RegForm form)	{
		return new ModelAndView("supplier/register")
					.addObject("form", form);
	}
	
	/**
	 * 注册
	 * @param name
	 * @param account
	 * @param pwd
	 * @param mobile
	 * @param email
	 * @return 0成功, 1参数不合法, 500其它
	 */
	@RequestMapping(value="doRegister",method=RequestMethod.POST)
	public ModelAndView doRegister(RegForm form,
									@CookieValue(value=REG_AUTH_KEY, required=false)String regAuthKey,
									HttpServletResponse resp)	{
		
		String regAuth = null;
		if(regAuthKey != null)	{
			regAuth = getFromRedis(regAuthKey);
			removeRedis(regAuthKey);
		} 
		removeCookie(resp, REG_AUTH_KEY);
		
		if(!form.getVerify().equals(regAuth))	{
			form.setIsVerifyErr(true);
			return register(form);	//返回登录页面
		} else	{
			int stat = 0;
			try {
				checkReg(form);
				Supplier su = new Supplier();
				su.setName(form.getName());
				su.setUserCode(form.getAccount());
				su.setPassword(form.getPwd());
				su.setMobile(form.getMobile());
				su.setEmail(form.getEmail());
				su.setCreateTime(new Date());	
				su.setCreator("用户自主注册");	//无效,会被baseDao修改
				supplierService.save4Register(su);
			} catch (IllegalArgumentException e) {
				log.error(e.getMessage(), e);
				stat = 1;
			} catch(Exception e)	{
				log.error(e.getMessage(), e);
				stat = 500;
			}
			
			return new ModelAndView("supplier/afterReg")
						.addObject("stat", stat)
						.addObject("account", form.getAccount());
		}
	}
	
	/**
	 * 检查账号是否被占用
	 * @param account
	 * @return 1已用, 0未用
	 */
	@RequestMapping("checkAccount")
	@ResponseBody
	public String checkAccount(String account)	{
		Supplier su = supplierService.findByAccount(account);
		return su == null ? "0" : "1";
	}
	
	/**
	 * 检测URL传参是否合法
	 * @param name
	 * @param account
	 * @param pwd
	 * @param mobile
	 * @param email
	 */
	private void checkReg(RegForm form)	{
		if("".equals(form.getName()))	{
			throw new IllegalArgumentException("企业名不能为空");
		}
		if(supplierService.findByAccount(form.getAccount()) != null)	{
			throw new IllegalArgumentException("账户名已被使用:" + form.getAccount());
		}
		if(!form.getAccount().matches(".{3,20}"))	{
			throw new IllegalArgumentException("不合法的账户名:" + form.getAccount());
		}
		if(!form.getPwd().matches("[\\da-fA-F]{32}"))	{
			throw new IllegalArgumentException("不合法的密码:" + form.getPwd());
		}
		if(!form.getMobile().matches("^1\\d{10}$"))	{
			throw new IllegalArgumentException("不合法的手机号:" + form.getMobile());
		}
		if(!form.getEmail().matches("^.+@.+[.]\\D+$"))	{
			throw new IllegalArgumentException("不合法的邮箱格式:" + form.getEmail());
		}
	}
	
	
	/**
	 * 忘记密码页面
	 * @return
	 */
	@RequestMapping("forgetPwd")
	public String forgetPwd()	{
		return "supplier/forgetPwd";
	}
	
	
	/**
	 * 发送邮件验证码
	 * @param account
	 * @param email
	 * @return 0成功,1邮箱不对, 500其它错误
	 */
	@RequestMapping(value="sendAuthcode",method=RequestMethod.POST)
	@ResponseBody
	public String sendAuthcode(@RequestParam String account, 
								@RequestParam String email,
								HttpServletResponse resp)	{
		int err = 0;
		final Supplier su = supplierService.findByAccount(account);
		if(!email.equalsIgnoreCase(su.getEmail()))	{
			err = 1;
		} else	{
			final String authcode = Util.randStr(6);
			Map<String,String> email_authcode = new HashMap<String,String>();
			email_authcode.put(email, authcode);
			String json = new Gson().toJson(email_authcode);
			
			String redisKey = UUID.randomUUID().toString();
			putToRedis(redisKey, json, 10 * 60);
			addToCookie(resp, AUTHCODE_KEY, redisKey, 10*60);
			
			new Thread(new Runnable(){	//异步发邮件并捕捉发送异常
				@Override
				public void run() {
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("authcode", authcode);
					map.put("supplier", su);
					try {
						mailSendService.sendTemplateMail(new String[]{su.getEmail()}, "京东招投标管理中心 - 邮箱验证", 
							"mail/emailAuthcode.vm", map, new File[]{});
					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
				}
			}).start();
		}
		return "{\"err\":" + err + "}";
	}
	
	
	/**
	 * 校验输入的验证码
	 * @param account
	 * @param email
	 * @param authcode
	 * @return 0成功, 1用户名与邮箱不对, 2验证码不对
	 */
	@RequestMapping(value="checkAuthcode",method=RequestMethod.POST)
	@ResponseBody
	public String checkAuthcode(@RequestParam String account, 
								@RequestParam String email, 
								@RequestParam String authcode,
								@CookieValue(AUTHCODE_KEY)String authcodeKey)	{
		
		int err = 0;
		Supplier su = supplierService.findByAccount(account);
		if(su == null || !email.equalsIgnoreCase(su.getEmail()))	{
			err = 1;
		} else{
			String json = getFromRedis(authcodeKey);
			Map<String, String> email_authcode = new Gson().fromJson(json, Map.class);
			if(authcode.equals(email_authcode.get(email)))	{
				err = 0;
			} else	{
				err = 2;
			}
		}
		return "{\"err\":" + err + "}";
	}
	
	/**
	 * 忘记密码后,设置新密码
	 * @param account
	 * @param email
	 * @param authcode
	 * @param newPwd
	 * @return 见{@link com.jd.supplier.web.controller.PassportController#checkAuthcode}返回值 
	 */
	@RequestMapping(value="resetPwd",method=RequestMethod.POST)
	@ResponseBody
	public String resetPwd(@RequestParam String account, 
							@RequestParam String email, 
							@RequestParam String authcode,
							@RequestParam String newPwd,
							@CookieValue(AUTHCODE_KEY)String authcodeKey,
							HttpServletResponse resp)	{
		
		String res = checkAuthcode(account, email, authcode, authcodeKey);
		JsonObject json = new JsonParser().parse(res).getAsJsonObject();
		if(json.get("err").getAsInt() == 0)	{
			Supplier su = supplierService.findByAccount(account);
			su.setPassword(newPwd);
			supplierService.save4Register(su);
		} 
		
		removeRedis(authcodeKey);	//验证失败或成功后, 都要清除
		removeCookie(resp, AUTHCODE_KEY);
		return json.toString();
	}
	
	/**
	 * 修改密码页面
	 */
	@RequestMapping("changePwd")
	public String changePwd()	{
		return "supplier/changePwd";
	}
	
	/**
	 * 修改密码。cookie中user_id有值才行。
	 * @param id
	 * @param oldPwd
	 * @param newPwd
	 * @return 0成功, 1密码不符， 500其它错误
	 */
	@RequestMapping(value="doChangePwd",method=RequestMethod.POST)
	@ResponseBody
	public String doChangePwd(@CookieValue(USER_ID)String userId,
							@RequestParam String oldPwd, 
							@RequestParam String newPwd)	{
		
		try	{
			long l = Long.parseLong(cookieUtil.decrypt(userId));
			Supplier su = supplierService.get(l);
			if(oldPwd.equals(su.getPassword()))	{
				su.setPassword(newPwd);
				supplierService.save4Register(su);
				return "{\"err\":0}";
			} else	{
				return "{\"err\":1}";
			}
		} catch(Exception e)	{
			log.error(e.getMessage(), e);
			return "{\"err\":500}";
		}
	}
	
	/**
	 * 生成图片验证码
	 * @param type 1登录用, 2注册用
	 * @param width 图片宽
	 * @param height 图片高
	 * @param resp
	 * @return
	 */
	@RequestMapping(value="verifypic", produces=MediaType.IMAGE_PNG_VALUE)
	@ResponseBody
	public byte[] verifypic(@RequestParam int type,
							@RequestParam(defaultValue="150")int width, 
							@RequestParam(defaultValue="50")int height, 
							HttpServletResponse resp)	{
		
		Captcha cap = new Captcha.Builder(width, height)
			.addText().addNoise().addBackground().build();

		String cookieKey;
		String redisKey = UUID.randomUUID().toString();
		int expireSec;
		if(type == 1)	{
			expireSec = 30;
			cookieKey = LOGIN_AUTH_KEY;
		} else if(type == 2)	{
			expireSec = 300;
			cookieKey = REG_AUTH_KEY;
		} else	{
			resp.setStatus(404);
			return null;
		}
		
		putToRedis(redisKey, cap.getAnswer(), expireSec);
		addToCookie(resp, cookieKey, redisKey, expireSec);
			
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(cap.getImage(), "png", baos);
			return baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			resp.setStatus(404);
			return null;
		}
	}
	
	/**
	 * 调试loginToolbar.jsp
	 * @return
	 */
	@RequestMapping("debugToolbar")
	public String debugToolbar()	{
		return "supplier/include/loginToolbar";
	}
	
	private String getFromRedis(String key)	{
		return redisClient.get(key);
	}
	
	private void putToRedis(String key, String value, int expireSec)	{
		redisClient.setex(key, expireSec, value);
	}
	
	private void removeRedis(String key)	{
		redisClient.del(key);
	}
	
	/**
	 * 删除passport路径下的cookie
	 * @param resp
	 * @param name
	 */
	private void removeCookie(HttpServletResponse resp, String...name)	{
		for(String s : name)	{
			addToCookie(resp, s, null, 0);
        }
	}
	
	private void addToCookie(HttpServletResponse resp, String key, String value, int maxAge)	{
		Cookie cookie = new Cookie(key, value);
		cookie.setMaxAge(maxAge);
		resp.addCookie(cookie);
	}
	

}
