package com.jd.supplier.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jd.supplier.model.Advertising;
import com.jd.supplier.service.AdvertisingService;

@Controller
@RequestMapping("adv")
public class AdvertisingController {
	
	@Resource
	private AdvertisingService advertisingService;

	@RequestMapping("bottom")
	public ModelAndView bottom()	{
		List<Advertising> bottomList = advertisingService.findByPosition(Advertising.POSITION_BOTTOM, 0, Integer.MAX_VALUE).getResult();
		return new ModelAndView("/supplier/include/link")
				.addObject("bottomList", bottomList);
	}
}
