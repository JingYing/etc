package com.jd.supplier.web.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.jss.service.JssService;

/**
 * @author WangYunLong
 * @date 2014年5月5日
 * @desc 公用下载controller
 */
@Controller
@RequestMapping("/supplier")
public class DownLoadFileController {
	
	 @Autowired
	 private JssService jssService;
	 private static final Logger logger = Logger.getLogger(DownLoadFileController.class);
	 
    @RequestMapping(value = "/downFilename")
    public void downFilename(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String fileName =request.getParameter("filename");// 保存窗口中显示的文件名
	String fileKey = new String(request.getParameter("filekey").getBytes("iso-8859-1"), "utf-8");// 保存窗口中显示key
	String parserFileName = parserFileName(request, fileName);
	try {
	    response.reset();
	    response.setContentType("APPLICATION/OCTET-STREAM");
	    // 要显示到客户端的文件名转码是必需的，特别是中文名， 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + parserFileName + "\"");
	    ServletOutputStream out = response.getOutputStream();
	    java.io.InputStream inStream = jssService.downloadFile("jd.sheild.doc", fileKey);
	    // 循环取出流中的数据
	    byte[] b = new byte[1024];
	    int len;
	    while ((len = inStream.read(b)) > 0)
		out.write(b, 0, len);
	    response.setStatus(response.SC_OK);
	    response.flushBuffer();
	    out.close();
	    inStream.close();
	} catch (Exception e) {
		logger.error("下载文件异常",e);

	}
    }
    private String parserFileName(HttpServletRequest request, String docFileName) {
        try {
            if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0 ||
                    request.getHeader("User-Agent").toUpperCase().indexOf("TRIDENT") > 0) {
                return URLEncoder.encode(docFileName, "UTF-8");
            } else {
                String enableFileName = "=?UTF-8?B?" + (new String(Base64.encodeBase64(docFileName.getBytes("UTF-8")))) + "?=";
                return enableFileName;
            }
        } catch (UnsupportedEncodingException e) {
            logger.error("解析文件名出错",e);
            throw new BusinessException(e);
        }
    }
}
