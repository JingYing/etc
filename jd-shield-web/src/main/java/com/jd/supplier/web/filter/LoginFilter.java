package com.jd.supplier.web.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.utils.SpringContextUtils;
import com.jd.supplier.web.CookieConstant;
import com.jd.supplier.web.CookieUtils;

public class LoginFilter extends OncePerRequestFilter{
	
	
	private static final Logger logger = Logger.getLogger(LoginFilter.class);
	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		
		// 不过滤的uri  
		String safeName = this.getFilterConfig().getInitParameter("safeUrls");
		String safeUrls[] = safeName.split(",");
  
        // 请求的uri  
        String uri = request.getRequestURI();
        
        // 是否过滤  
        boolean doFilter = true;  
        for (String s : safeUrls) {  
        	if (uri.indexOf(s) != -1 && !s.equals("/") || uri.equals("/")) {  
        		// 如果uri中包含不过滤的uri，则不进行过滤  
        		doFilter = false;  
        		break;  
        	}  
        }  
        if (doFilter) {
        	// 执行过滤  
        	Cookie[] cookies = request.getCookies();
        	if (null != cookies){
        		Boolean isLogin = false;
        		for(int i=0;i<cookies.length;i++){
        			Cookie c = cookies[i];
        			if(CookieConstant.USER_ID.equalsIgnoreCase(c.getName())){
        				CookieUtils cookieUtils = SpringContextUtils.getBean("cookieUtils");
        				try{
        					//通过cookieUtils解密UID
        					cookieUtils.decrypt(c.getValue());
        				}catch(BusinessException e){
        					//如果解密失败，则认为无效cookie，重新跳转的首页
        					try {
								toHomePage(request, response);
							} catch (Exception e1) {
								logger.error("登陆cookie解密失败！", e);
							}
//                    		response.sendRedirect(homePage);
        					return;
        				}
        				isLogin = true;
        				break;
        			}
        		}
        		if(isLogin)
        			filterChain.doFilter(request, response);
        		else{
        			toHomePage(request, response);
            		return;
        		}
        	}else {  
        		toHomePage(request, response);
        		return;
        	} 
        } else {  
        	// 如果不执行过滤，则继续  
        	filterChain.doFilter(request, response);
        }  
	}


	
	private void toHomePage(HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		//首页链接
    	final String homePage = request.getContextPath()+"/supplier/index?loginStatus=1";
//		request.setCharacterEncoding("UTF-8");  
		response.setCharacterEncoding("UTF-8");
//		PrintWriter out = response.getWriter();  
//		StringBuilder builder = new StringBuilder();  
//		builder.append("<script type=\"text/javascript\">");  
//		builder.append("alert('请登录！');");  
//		builder.append("window.top.location.href='");  
//		builder.append(homePage);  
//		builder.append("';");  
//		builder.append("</script>");  
//		out.print(builder.toString());
		
		response.sendRedirect(homePage);
	}
	 //效验
//    protected static boolean sqlValidate(String str) {
//        str = str.toLowerCase();//统一转为小写
//        //String badStr = "'|and|exec|execute|insert|select|delete|update|count|drop|*|%|chr|mid|master|truncate|char|declare|sitename|net user|xp_cmdshell|;|or|-|+|,|like";
//        String badStr = "'|and|exec|execute|insert|create|drop|table|from|grant|use|group_concat|column_name|" +
//                "information_schema.columns|table_schema|union|where|select|delete|update|order|by|count|*|" +
//                "chr|mid|master|truncate|char|declare|or|;|-|--|+|,|like|//|/|%|#";//过滤掉的sql关键字，可以手动添加
//        String[] badStrs = badStr.split("|");
//        for (int i = 0; i < badStrs.length; i++) {
//            if (str.indexOf(badStrs[i]) !=-1) {
//                return true;
//            }
//        }
//        return false;
//    }
}
