package com.jd.supplier.web.controller;

import static com.jd.supplier.web.CookieConstant.USER_ID;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.common.util.PaginatedList;
import com.jd.official.core.utils.DateUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.supplier.common.enm.DeleteEnum;
import com.jd.supplier.common.enm.SupplierSelectionEnum;
import com.jd.supplier.common.util.DateUtil;
import com.jd.supplier.common.util.SupplierConstant;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.Task;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.service.SupplierService;
import com.jd.supplier.service.TaskService;
import com.jd.supplier.service.TaskSuppSelectionService;
import com.jd.supplier.web.CookieUtils;
import com.jd.supplier.web.vo.PreSelectionVO;

@Controller
@RequestMapping("/supplier")
public class PreSelectionController {

    private static Logger logger = Logger.getLogger(PreSelectionController.class);

    @Autowired
    private TaskSuppSelectionService taskSuppSelectionService;

    @Autowired
    private DictDataService dictDataService;

    @Autowired
    private TaskService taskService;

    @Resource
    private CookieUtils cookieUtil;

    @Autowired
    private SupplierService supplierService;

    /**
     * author duandongdong date 2014年5月23日 desc 供应商报名
     */
    @RecordLog(operationType = OperationTypeValue.add, entityName = "TaskSuppSelection")
    @RequestMapping(value = "/preSelectionApply",method = RequestMethod.POST)
    @ResponseBody
    public String preSelectionApplySave(@CookieValue(USER_ID) String id, TaskSuppSelection taskSuppSelection,
	    HttpServletRequest request) {
	String supplierId = cookieUtil.decrypt(id);
	taskSuppSelection.setSupplierId(Long.valueOf(supplierId));
	Map<String, Object> taskSuppselectionMap = new HashMap<String, Object>();
	taskSuppselectionMap.put("requirementId", taskSuppSelection.getRequirementId());
	taskSuppselectionMap.put("supplierId", supplierId);
	Supplier supplier = (Supplier) supplierService.get(Long.valueOf(supplierId));
	// 供应商是否审核
	if (supplier.getSupplierStatus().equals("1")) {
	    List<TaskSuppSelection> taskSuppSelectionList = taskSuppSelectionService.find("findByMap",
		    taskSuppselectionMap);
	    // 是否已经报名
	    if (taskSuppSelectionList != null && taskSuppSelectionList.size() > 0) {
		return "1";
	    } else {
		Map<String, Object> taskMap = new HashMap<String, Object>();
		taskMap.put("requirementId", taskSuppSelection.getRequirementId());
		List<Task> taskList = taskService.find("findByMap", taskMap);
		if (taskList != null) {
		    for (int i = 0; i < taskList.size(); i++) {
			taskSuppSelection.setTaskId(((Task) taskList.get(i)).getId());
			savePreSelection(taskSuppSelection);
		    }
		}

		return "0";
	    }
	} else {
	    return "2";
	}
    }

    private void savePreSelection(TaskSuppSelection taskSuppSelection) {
	taskSuppSelection.setStatus(DeleteEnum.NO.getStatus());// 1状态为可用
	taskSuppSelection.setFinalFlag(SupplierSelectionEnum.PREVIOUS.getType());
	taskSuppSelectionService.insert(taskSuppSelection);
    }

    @RecordLog(operationType = OperationTypeValue.select, entityName = "TaskSuppSelection")
    @RequestMapping(value = "/queryApplyRequirement",method = RequestMethod.POST)
    @ResponseBody
    public Object queryPreSelectionApply(@CookieValue(USER_ID) String id, HttpServletRequest request,
	    HttpServletResponse response) {
	// 创建pageWrapper
	PageWrapper<TaskSuppSelection> pageWrapper = new PageWrapper<TaskSuppSelection>(request);

	// 添加搜索条件
	String supplierId = cookieUtil.decrypt(id);
	String searchProjectName = request.getParameter("searchProjectName");
	String time = request.getParameter("time");
	String begintime = "";
	String endtime = "";
	pageWrapper.addSearch("status", SupplierConstant.AVAILABLESTATUS);
	pageWrapper.addSearch("supplierId", supplierId);
	pageWrapper.addSearch("searchProjectName", searchProjectName);
	if (time != null && !time.equals("")) {
	    if (time.equals("1")) {// 一周内
		begintime = DateUtil.dayMove(new Date(), -7);
	    } else if (time.equals("2")) {// 一月内
		begintime = DateUtil.monthMove(new Date(), -1);
	    }
	    endtime = DateUtils.datetimeFormat(new Date());
	}
	pageWrapper.addSearch("begintime", begintime);
	pageWrapper.addSearch("endtime", endtime);
	// 后台取值
	taskSuppSelectionService
		.find(pageWrapper.getPageBean(), "findApplyRequirement", pageWrapper.getConditionsMap());

	// 返回到页面的数据
//	List<TaskSuppSelection> taskSuppSelections = (List<TaskSuppSelection>) pageWrapper.getResult().get("aaData");
	response.setContentType("text/html; charset=utf-8");
	String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());

	return json;
    }

    @RequestMapping(value = "/myRequirement")
    public ModelAndView myRequirement(@CookieValue(USER_ID) String id,
	    @ModelAttribute(value = "supplierQuery") PreSelectionVO preSelectionVO, HttpServletRequest request,
	    Model model) {
	ModelAndView modelAndView = new ModelAndView("supplier/myRequirement");
	Supplier su = supplierService.get(Long.parseLong(cookieUtil.decrypt(id)));
	if (su.getDataIntegrity() != null && !"".equals(su.getDataIntegrity())) {
	    DictData dd = (DictData) dictDataService.get(Long.valueOf(su.getDataIntegrity()));
	    if (dd != null) {
		su.setDataIntegrity(dd.getDictDataName());
	    } else {
		su.setDataIntegrity("0");
	    }
	}
	// 查询所有投标项目
	String begintime = "";
	String endtime = "";
	String searchProjectName = request.getParameter("searchProjectName");
	String time = request.getParameter("time");
	if (time != null && !time.equals("")) {
	    if (time.equals("1")) {// 一周内
		begintime = DateUtil.dayMove(new Date(), -7);
	    } else if (time.equals("2")) {// 一月内
		begintime = DateUtil.monthMove(new Date(), -1);
	    }
	    endtime = DateUtils.datetimeFormat(new Date());
	}
	PaginatedList<TaskSuppSelection> taskSuppSelectionList = null;
	preSelectionVO.setStatus(SupplierConstant.AVAILABLESTATUS);
	preSelectionVO.setSupplierId(cookieUtil.decrypt(id));
	preSelectionVO.setSearchProjectName(searchProjectName);
	preSelectionVO.setBegintime(begintime);
	preSelectionVO.setEndtime(endtime);

	taskSuppSelectionList = taskSuppSelectionService.pageSearchTaskSuppSelection(preSelectionVO);

	model.addAttribute("taskSuppSelectionList", taskSuppSelectionList);
	model.addAttribute("supplier", su);
	return modelAndView;
    }

}
