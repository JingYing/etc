package com.jd.supplier.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.JsonObject;
import com.jcloud.jss.exception.StorageClientException;
import com.jd.common.constants.CommonConstant;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.jss.service.JssService;
import com.jd.official.core.utils.IdUtils;

/**
 * @author WangYunLong
 * @date 2014年7月31日
 * @desc 上传文件类
 */
public class UploadController extends HttpServlet {
	
	private static final Logger log = Logger.getLogger(UploadController.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static final String ENCODE = "utf-8";
	
	private static final String bucketName = "jd.sheild.doc";
	
	private static final String jssdomainName = "storage.jd.com";

	/**
	 * 实现多文件的同时上传
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ServletContext application = getServletContext();
		WebApplicationContext wac = WebApplicationContextUtils
				.getWebApplicationContext(application);
		JssService jssService = (JssService) wac.getBean("jssService");
		request.setCharacterEncoding(ENCODE);

		try {
			DiskFileItemFactory fac = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(fac);
			upload.setHeaderEncoding(ENCODE);
			// 获取多个上传文件
			List<?> fileList = upload.parseRequest(request);
			// 遍历上传文件写入磁盘
			Iterator<?> it = fileList.iterator();
			String key = null;
			if(it != null){
				while (it.hasNext()) {
					Object obit = it.next();
					if (obit instanceof DiskFileItem) {
						DiskFileItem item = (DiskFileItem) obit;						 
						// 获得文件名及路径,如果item是文件上传表单域
						String fileName = item.getName();
						if (fileName != null) {
							try {
								//InputStream inputStream = item.getInputStream();								
								String uuid = IdUtils.uuid2()+fileName.substring(fileName.lastIndexOf("."),fileName.length());							
								//jssService.uploadFile(bucketName, uuid, inputStream);
								//暂时先按照key存文件名,后期jss升级以后再做调整
								jssService.getJssService().bucket(bucketName).object(uuid).entity(item.getInputStream().available(), item.getInputStream()).disposition(uuid).put();
								key = "http://"+jssdomainName+"/" + bucketName + "/"+ uuid;
//								key = new StringBuffer("http://"+jssdomainName+"/" + bucketName + "/"+ uuid).append(CommonConstant.Symbol.COMMA).toString();
//								fileName = new StringBuffer(fileName).append(CommonConstant.Symbol.COMMA).toString();
								JsonObject json = new JsonObject();
								json.addProperty("fileName", fileName);
								json.addProperty("fileKey", key);
								response.setContentType("text/json;charset="+ENCODE);
								response.getWriter().print(json.toString());
							} catch (StorageClientException e) {
								response.getWriter().print("0");
								log.debug("上传云存储文件异常............." + e.getMessage());
							}catch(BusinessException be){
								log.error("Jss服务调用失败", be);
								response.getWriter().print("0");
							}catch (Exception e) {
								log.error("上传发生异常", e);
								response.getWriter().print("0");
							}
							
						}
					}
				}
			}
		} catch (org.apache.commons.fileupload.FileUploadException ex) {
			ex.printStackTrace();
			response.getWriter().print("0");
			return;
		}

	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
}