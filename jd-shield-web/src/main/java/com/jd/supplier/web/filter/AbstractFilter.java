
package com.jd.supplier.web.filter;


import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AbstractFilter
  implements Filter
{
  protected static final Log logger = LogFactory.getLog(AbstractFilter.class);
  private FilterConfig filterConfig;
  private boolean ignoreException;

  public void init(FilterConfig filterConfig)
    throws ServletException
  {
    this.filterConfig = filterConfig;

    this.ignoreException = Boolean.valueOf(findInitParameter("ignoreException", "true")).booleanValue();

    if (this.ignoreException) {
      logger.info("Set ignore ServletException to " + this.ignoreException);
    }

    init();
  }

  public void init()
    throws ServletException
  {
  }

  public FilterConfig getFilterConfig()
  {
    return this.filterConfig;
  }

  public ServletContext getServletContext()
  {
    return getFilterConfig().getServletContext();
  }

  public String findInitParameter(String paramName, String defaultValue)
  {
    String value = StringUtils.trimToNull(getFilterConfig().getInitParameter(paramName));

    if (value == null) {
      value = StringUtils.trimToNull(getServletContext().getInitParameter(paramName));
    }

    if (value == null) {
      value = defaultValue;
    }

    return value;
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException
  {
    if ((!(request instanceof HttpServletRequest)) || (!(response instanceof HttpServletResponse)) || (request.getAttribute(getClass().getName()) != null))
    {
      chain.doFilter(request, response);

      return;
    }

    request.setAttribute(getClass().getName(), Boolean.TRUE);
    try
    {
      HttpServletRequest req = (HttpServletRequest)request;

      HttpServletResponse res = (HttpServletResponse)response;

      doFilter(req, res, chain);
    }
    catch (ServletException e) {
      if (this.ignoreException) {
        logger.error("Failed to execute the filter chain", e);
      }
      else
        throw e;
    }
  }

  public abstract void doFilter(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse, FilterChain paramFilterChain)
    throws IOException, ServletException;

  protected void forwardPage(ServletRequest request, ServletResponse response, String url)
    throws IOException, ServletException
  {
    ServletContext servletContext = this.filterConfig.getServletContext();
    RequestDispatcher reqDispatcher = servletContext.getRequestDispatcher(url);
    reqDispatcher.forward(request, response);
  }

  protected String dumpRequest(HttpServletRequest request)
  {
    StringBuffer buffer = new StringBuffer(request.getMethod());

    buffer.append(" ").append(request.getRequestURI());

    String queryString = StringUtils.trimToNull(request.getQueryString());

    if (queryString != null) {
      buffer.append("?").append(queryString);
    }

    return buffer.toString();
  }

  public void destroy()
  {
    this.filterConfig = null;
  }
}