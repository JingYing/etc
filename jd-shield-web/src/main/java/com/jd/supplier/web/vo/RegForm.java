package com.jd.supplier.web.vo;

/**
 * 用户注册用的表单
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年7月1日
 */
public class RegForm {

	private String name, account, pwd, mobile, email, verify;
	private boolean isVerifyErr;
	
	public boolean getIsVerifyErr() {
		return isVerifyErr;
	}

	public void setIsVerifyErr(boolean isVerifyErr) {
		this.isVerifyErr = isVerifyErr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVerify() {
		return verify;
	}

	public void setVerify(String verify) {
		this.verify = verify;
	}

}
