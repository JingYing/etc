var table;
var tableData;
$(function(){ 

//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='requirement' value=\""+data.aData.id+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	            {"mData": "title","bSortable":false,"sTitle":"标题","sDefaultContent":""},
		        {"mData": "requirementName", "sTitle": "项目名称" , "bSortable": true},
		        {"mData": "biddate","bSortable":false,"sTitle":"预计招标时间","sDefaultContent":"","mRender":function(data){
		        	   return parseDate(data);
		        }},
		        {"mData": "publishdate","bSortable":false,"sTitle":"发布时间","sDefaultContent":"","mRender":function(data){
		        	   return parseDate(data);
		        }}
	        ];

var  tableBtns  =[
				];


 table = $('#hello').dataTable( {
	 		"sDefaultContent":"notice",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":"/supplier/notice_page",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
            	var type=2;
				aoData.push(
						{ "name": "type", "value":type }
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	//alert(oSetting);
            	
            	//alert("ddd");
				/*添加回调方法*/
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );

  	
  	parseDate= function(data){
  		 if(data!=""){
  			    var date = new Date(data);// 或者直接new Date();
			    return date.format("yyyy-MM-dd");
  		 }else return "";
  	}
	
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}


Date.prototype.format = function(format) {
		    var o = {
		    "M+" : this.getMonth() + 1, //month
		    "d+" : this.getDate(), //day
		    "h+" : this.getHours(), //hour
	        "m+" : this.getMinutes(), //minute
		    "s+" : this.getSeconds(), //second
		    "q+" : Math.floor((this.getMonth() + 3) / 3), //quarter
		    "S" : this.getMilliseconds()
		    //millisecond
		    }
		    if (/(y+)/.test(format)) {
		        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		    }
		 
		    for ( var k in o) {
		        if (new RegExp("(" + k + ")").test(format)) {
		        format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
	        }
		    }
		    return format;
		}


