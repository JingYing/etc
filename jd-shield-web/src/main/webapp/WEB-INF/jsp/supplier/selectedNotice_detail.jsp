<%@page import="com.jd.official.core.utils.DateUtils"%>
<%@page import="com.jd.supplier.model.Supplier"%>
<%@page import="com.jd.supplier.model.TaskSuppSelection"%>
<%@page import="com.jd.supplier.model.Notice"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="../../../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../../../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../../../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../common/js/slider.js"></script>
<script type="text/javascript">
	function showProjectDetail(reqId){
		window.open("/supplier/requirement_viewpage?id="+reqId);
	}
</script>
</head>
<body>
<jsp:include page="../supplier/include/loginToolbar.jsp" />
    <jsp:include page="../supplier/include/header.jsp" />
    
    <div class="cg_content">
        <!-- 招标预告和公告栏 S -->
        <div class="grid_c1">
            <div class="page_detail"> 
                <div class="page_detail_hd">
                    <h2>入围标题：${notice.title}</h2>
                </div>
                <div class="page_detail_bd">
                 	<div class="detail_item">
                        <div class="detail_item_tit">项目名称：</div>
                        <div class="detail_item_cont"><a href="#" onclick="showProjectDetail(${notice.requirementId})" >${notice.requirementName}</a></div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">预计招标时间：</div>
                        <div class="detail_item_cont">${notice.biddatestr}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">发布时间：</div>
                        <div class="detail_item_cont">${notice.publishdatestr}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">招标负责人：</div>
                        <div class="detail_item_cont">${notice.bidprincipal}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">联系电话：</div>
                        <div class="detail_item_cont">${notice.phone}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">手机：</div>
                        <div class="detail_item_cont">${notice.telephone}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">电子邮箱：</div>
                        <div class="detail_item_cont">${notice.email}</div>
                    </div> 
                    <div class="detail_item">
                        <div class="detail_item_tit">报名截止日期：</div>
                         <div class="detail_item_cont">${notice.enddatestr}</div>
                    </div>
                       
                    <div class="detail_action">
                         <div class="detail_item_tit"></div>
                    </div>
                </div>
                
                
                
                <div class="page_detail_bd">
                    <div class="detail_item">
                        <div class="detail_item_tit">投标单位列表：</div>
                        <div class="detail_item_cont">
                            <table class="mod_table table_yugao_detail">
                                <thead>
                                <tr>
                                    <th class="col1">序号</th>
                                    <th class="col2">投标单位</th>
                                    <th>报名日期</th>
                                    <th>状态</th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                               			List<Supplier> supplierList =(List<Supplier>)request.getAttribute("supplierList");
		                               	if(supplierList!=null){
				                	    for(int i=0;i<supplierList.size();i++){
				                		Supplier supplier=(Supplier)supplierList.get(i);
                                %>
                                
                                    <tr>
                                        <td class="col1">1</td>
                                        <td class="col2"><div class="col2_in"><a href="#" ><%=supplier.getName()%></a></div></td>
                                        <td><%=DateUtils.dateFormat(supplier.getApplyCreateTime())%></td>
                                        <td>入围</td>
                                    </tr>
                                  <%
				                	    }
		                              }
                                  %> 
                                  
                                </tbody>
                            </table>
                        </div>
                    </div> 
                </div>
                
                
                
                
            </div>
        </div> 
        <!-- 招标预告和公告栏 E --> 
        <c:import url="${pageContext.request.contextPath}/adv/bottom"></c:import>
    </div>

	
	   <jsp:include page="../supplier/include/footer.jsp" />








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>
