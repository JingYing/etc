<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%-- <base href="<%=basePath%>"> --%>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" /> 
	<title>京东采购_京盾招投标_首页</title>
	<link rel="stylesheet" href="http://misc.360buyimg.com/lib/skin/2013/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/global.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/findpassword.css" type="text/css" media="screen" />
	<script type="text/javascript" src="../common/js/jquery.min.js"></script>
	<script type="text/javascript" src="../common/js/jquery.md5.js"></script>
	<script type="text/javascript" src="../common/js/slider.js"></script>
	<script type="text/javascript" >
		$(function(){
		    $(".form_item .input_wrap input").focus(function(){
		        $(this).parent().find(".form_item").addClass("focus");
		        //$(this).removeClass("placeholder").val('');
		    });
		    $(".form_item .input_wrap input").blur(function(){
	            $(this).parent().find(".form_item").removeClass("focus");
	            /*
	            if($(this).val()=="") {
	                $(this).addClass("placeholder");
	                if($(this).hasClass("password")) {$(this).val("由6-20个英文字母（区分大小写）或数字组成");}
	                if($(this).hasClass("repassword")) {$(this).val("请再输入一遍您上面填写的密码");}
	            }
	            */
		    });
		    
		    
		    /* '获取邮件验证码'按钮状态变化 */
		    $(".btn_vali").click(function(){
		    	if($(this).hasClass("btn_getting"))		return; 
		    	var account = $.trim($("#account").val()), 
		    		email = $("#email").val(),
					v1 = validateCallback($("#account"), account != ''),
					v2 = validateCallback($("#email"), /^.+@.+[.]\D+$/i.test(email));
				if(v1 == false || v2 == false)	{
					return;
				}
		    	
		    	var $this = $(this), param = {"account":account, "email" : email}; 
		    	$("#loading").toggle();
	    		$.post("sendAuthcode", param, function(data){
		    		$("#loading").toggle();
	    			if(data.err == 0)	{
			           countdown($this);
			           $("#authcode").removeAttr('disabled');
			           $("#submit1").removeAttr('style').click(function(){
			        	   param.authcode = $("#authcode").val();
			        	   $.post("checkAuthcode", param, function(data){
			        		  if(data.err == 0)	{
			        			  toStep(2);
			        		  } else	{
			        			  validateCallback($("#authcode"), false);
			        		  }
			        	   },'json');
			           });
	    			} else	{
	    				alert("用户名或邮箱不存在");
	    			}
	    		}, 'json');
            });
		    
		});
		
		function countdown(btn)	{
			var time=59;
			$(btn).addClass("btn_getting").html("<span>" + (time+1) + "秒后可再次获取</span>");
            function countdown(){
				if(time >= 0){
				    $(btn).html("<span>"+time+"秒后可再次获取</span>");
				    time--;
				    count = setTimeout(countdown,1000);
				}else {
				    $(btn).removeClass("btn_getting").html("<span>获取邮件验证码</span>");
				    clearTimeout(count);
				}
            }
            var count = setTimeout(countdown,1000);
		}
		
		/**
		 * input校验后改变样式
		 * @param inputEle 输入框
		 * @param isSuccess 输入是否有效
		 * @return isSuccess
		 */
		function validateCallback(inputEle, isSuccess)	{
			var div = $(inputEle).parents(".form_item").removeClass("error right");
			if(isSuccess)	div.addClass("right");
			else			div.addClass("error");
			return isSuccess;
		}
		
		
		//跳转step
		function toStep(index)	{
			$("#stepBar").attr('class', "step").addClass("step" + index);	//把stepBar的class还原成step, 并加上stepX的class
			if(index == 1 || index == 2)	{
				$("div[id^=step][id!=stepBar]").hide();
			} else if(index == 3)	{
				$("div[id^=step]").hide();	//第3步时,隐藏所有stepX和stepBar
			}
			$("#step" + index).show();
		}
		
		function submit2()	{
			var v1 = validateCallback($('#pwd'), /^.{6,20}$/i.test($("#pwd").val())),
				v2 = validateCallback($('#pwd2'), $("#pwd2").val() != '' && $("#pwd").val() == $("#pwd2").val());
			if(v1==false || v2==false)	{
				return;
			}
			var param = {"account":$("#account").val(), "email":$("#email").val(), "authcode":$("#authcode").val(), "newPwd":$.md5($("#pwd").val())}; 
			$.post("resetPwd", param, function(data){
				if(data.err == 0)	{
					toStep(3);
				} else	{
					alert("修改密码失败!");
				}
			},'json');
		}
	</script>
	
</head>
<body> 
	<div class="cg_header">
		<div class="grid_c1">
			<h2 class="title"><a href="#" title="京东采购">京东采购<img src="../common/img/logo.png" alt="易迅商户后台"></a></h2> 
		</div>
	</div>
    <div class="cg_content"> 
        <div class="grid_c1">
            <div class="mod_page">
                <h2 class="mod_page_hd">找回密码</h2>
                <div class="mod_page_bd">
                    <!-- 找回密码 步骤1 S --> 
                    <div id="stepBar" class="step step1">
                        <div class="bg"></div>
                        <ol class="step_list clearfix">
                            <li class="step_list_li1"><span>验证身份</span></li>
                            <li class="step_list_li2"><span>设置新密码</span></li>
                            <li class="step_list_li3"><span>完成</span></li>
                        </ol>
                    </div>
                    
                    <div id="step1" class="mod_form">
                        <!-- 获得焦点则给加上类focus; 正确给 form_item 加上类right ;错误则加上类error; -->
                        <div class="form_item">
                            <label class="item_lb" for="">用户名：</label>
                            <span class="input_wrap">
                                <input id="account" class="input_n" type="text" autocomplete="off"/>
                                <i class="i_icon i_right"></i>
                            </span>
                            <p class="p_tip"></p>
                            <p class="p_error">请输入用户名</p>
                        </div><!-- / 用户名 -->
                        <div class="form_item">
                            <label class="item_lb" for="">邮箱：</label>
                            <span class="input_wrap">
                                <input id="email" class="input_l" type="text" autocomplete="off"/>
                                <i class="i_icon i_right"></i>
                            </span>
                            <a href="javascript:void(0);" class="btn_vali"><span>获取邮件验证码</span></a>
                            <img src="../common/img/loading.gif" width=20 height=20 id="loading" style="display:none"/>        
                            <!--<a href="javascript:;" class="btn_vali btn_getting"><span>60秒后可再次获取</span></a>-->
                            <p class="p_error">请输入正确的邮箱格式</p>
                        </div><!-- / 邮箱 -->
                        <div class="form_item">
                            <label class="item_lb" for="">邮件验证码：</label>
                            <span class="input_wrap">
                                <input id="authcode" class="input_m" type="text" autocomplete="off" disabled/>
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <p class="p_error">验证码输入错误</p>
                        </div><!-- / 邮件验证码 -->
                        <div class="form_action">
                            <a id="submit1" href="javascript:void(0);" class="btn_submit" style="color:grey;cursor:default;"><span>提交</span></a><%-- 未通过验证时,按钮禁用 --%>
                        </div>
                    </div>
                    <!-- 找回密码 步骤1 E -->
                    <!-- 找回密码 步骤2 S -->
                    
                    
                    <div id="step2" class="mod_form" style="display:none">
                        <!-- 获得焦点则给加上类focus; 正确给 form_item 加上类right ;错误则加上类error; -->
                        <div class="form_item">
                            <label class="item_lb" for="">新密码：</label>
                            <span class="input_wrap">
                                <input class="input_xxl placeholder password" id="pwd" type="password" placeholder="由6-20个英文字母（区分大小写）或数字组成" />
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <p class="p_tip"></p>
                            <p class="p_error">由6-20个英文字母（区分大小写）或数字组成</p>
                        </div><!-- / 新密码 -->
                        <div class="form_item">
                            <label class="item_lb" for="">确认密码：</label>
                            <span class="input_wrap">
                                <input class="input_xxl placeholder repassword" id="pwd2" type="password" placeholder="请再输入一遍您上面填写的密码" />
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <p class="p_tip"></p>
                            <p class="p_error">两次输入不一致</p>
                        </div><!-- / 确认密码 -->
                        <div class="form_action">
                            <a class="btn_submit" href="javascript:;" onclick="submit2()"><span>提交</span></a>
                        </div>
                    </div>
                    <!-- 找回密码 步骤2 E -->
                    <!-- 找回密码 步骤3 S -->
                    <div id="step3" class="fp_succeed" style="display:none">
                        <i class="i_succeed"></i>
                        <p class="succeed">恭喜您，新密码设置成功！</p>
                        <p><a href="${pageContext.request.contextPath}/supplier/index" class="back">返回主页</a></p>
                    </div>
                    <!-- 找回密码 步骤3 E -->
                </div><!-- / fp_bd -->
            </div> <!-- / findpassword -->
        </div><!-- grid_c1 -->
    </div><!-- cg_content -->
    
    <%@include file="include/footer.jsp" %>

</body>
</html>
