<%@page import="com.jd.supplier.model.Notice"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="../../../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../../../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../../../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../common/js/slider.js"></script>

<link href="../../../static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../../../static/datatables/css/TableTools.css" rel="stylesheet" type="text/css"/>
<link href="../../../static/datatables/css/jquery.dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="../../../static/jquery/jquery.1.8.2.js"></script>
<script type="text/javascript" src="../../../static/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="../../../static/bootstrap/js/bootbox.js"></script>
<script type="text/javascript" src="../../../static/bootstrap/js/Dialog.js"></script>
<script type="text/javascript" src="../../../static/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../static/datatables/js/TableTools.min.js"></script>
<script type="text/javascript" src="../../../static/datatables/js/jquery.dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../../../static/datatables/js/ColReorderWithResize.js"></script>

	
<script type="text/javascript" src="../../../js/supplier/bidNotice_index.js"></script>
</head>
<body>
   <jsp:include page="../supplier/include/loginToolbar.jsp" />
   <jsp:include page="../supplier/include/header.jsp" />
    <div class="cg_content">
        <!-- 招标预告和公告栏 S -->
        <div class="grid_c2b page_zhaobiao">
            <div class="grid_m">
                <div class="grid_m_inner">
                    <div class="mod_col">
                        <div class="mod_col_hd">
                            <h2 class="title">招标预告</h2>
                        </div>
                        <div class="mod_col_bd">
                            
                             <table cellpadding="0"  cellspacing="0" border="0" class="table table-striped table-bordered display" id="hello" width="100%">
   				   			 </table>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid_s cg_notice mod_col">
                <div class="mod_col_hd">
                    <h2 class="title">公告栏</h2>
                </div>
                <div class="mod_col_bd">
                    <ul class="mod_list">
                        			<%
	                                	List<Notice> bidNoticeList =(List<Notice>)request.getAttribute("bidNoticeList");
		                               	if(bidNoticeList!=null){
			                               	int length=0;
			                               	int size=0;
			                               	if(bidNoticeList.size()>0&&bidNoticeList.size()<10){
			                               	    size=bidNoticeList.size();
			                               	}else{
			                               	    size=10;
			                               	}
		                               
                                	    for(int i=0;i<size;i++){
                                		 	Notice notice=(Notice)bidNoticeList.get(i);
                                	%>	
                    
                      						  <li><a href="#" target="_blank" title="<%=notice.getTitle()%>"><%=notice.getTitle()%></a></li>
                        			 <%
                                		}
                                      }
                                    %>
                    </ul>
                </div>
            </div>
        </div> 
        <!-- 招标预告和公告栏 E --> 
       <c:import url="${pageContext.request.contextPath}/adv/bottom"></c:import>
    </div>

	
	<jsp:include page="../supplier/include/footer.jsp" />








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>
