<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="../../../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../../../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../../../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../common/js/slider.js"></script>

<script type="text/javascript">
	function showProjectDetail(reqId){
		window.open("/supplier/requirement_viewpage?id="+reqId);
	}
	
	function preSelectionSave(requirementId){
			var urlpara ="/supplier/preSelectionApply?requirementId="+requirementId;
			//把表单的数据进行序列化 
			$.ajax({
				url : urlpara,
				type : "POST",
				dataType : "json",
				success : function(data) {
					if(data=="1"){
						alert("已经报名");
					}else if(data=="2"){
						alert("没有审核!");
					}else{
						alert("报名成功!");
					}
				},
				error : function(xhr, st, err) {
					alert("报名失败!");
				}
			});
	}
</script>
</head>
<body>
<jsp:include page="../supplier/include/loginToolbar.jsp" />
    <jsp:include page="../supplier/include/header.jsp" />
    
    <div class="cg_content">
        <!-- 招标预告和公告栏 S -->
        <div class="grid_c1">
            <div class="page_detail"> 
                <div class="page_detail_hd">
                    <h2>预告标题：${notice.title}</h2>
                </div>
                <div class="page_detail_bd">
                 	<div class="detail_item">
                        <div class="detail_item_tit">项目名称：</div>
                        <div class="detail_item_cont"><a href="#" onclick="showProjectDetail(${notice.requirementId})" >${notice.requirementName}</a></div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">项目介绍：</div>
                        <div class="detail_item_cont">
                            <div class="detail_intro">
                           ${notice.content}
                            </div>
                        </div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">预计招标时间：</div>
                        <div class="detail_item_cont">${notice.biddatestr}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">发布时间：</div>
                        <div class="detail_item_cont">${notice.publishdatestr}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">招标负责人：</div>
                        <div class="detail_item_cont">${notice.bidprincipal}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">联系电话：</div>
                        <div class="detail_item_cont">${notice.phone}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">手机：</div>
                        <div class="detail_item_cont">${notice.telephone}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">电子邮箱：</div>
                        <div class="detail_item_cont">${notice.email}</div>
                    </div> 
                    <div class="detail_item">
                        <div class="detail_item_tit">报名截止日期：</div>
                        <div class="detail_item_cont">${notice.enddatestr}</div>
                    </div>
                        
                    <div class="detail_action">
                        <a href="#" class="btn_mini" onclick="preSelectionSave(${notice.requirementId})"><span>报名</span></a>
                    </div>
                </div>
                
                
                
                
            </div>
        </div> 
        <!-- 招标预告和公告栏 E --> 
        <c:import url="${pageContext.request.contextPath}/adv/bottom"></c:import>
    </div>

	
	   <jsp:include page="../supplier/include/footer.jsp" />








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>
