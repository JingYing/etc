<%@ page language="java" pageEncoding="UTF-8"%>
<div class="grid_s">
	<div class="side_menu">
		<ul class="mod_list">
			<li><a href="/supplier/myRequirement" target="_self" title="我的投标"><b></b>我的投标项目</a></li>
			<li><a href="/supplier/supplier_detail" target="_self" title="我的资料"><b></b>我的资料</a></li>
			<li><a href="/passport/changePwd" target="_self" title="修改密码"><b></b>修改密码</a></li>
		</ul>
	</div>
</div>

<script type="text/javascript">
	$(".mod_list li a").each(function(){
		if(this.href == window.location)	{
			$(this).parent("li").addClass("curr"); //改变选中状态
		}
	});
</script>