<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" import="java.util.*,java.net.*" pageEncoding="UTF-8"%>

<%-- 本页面依赖jquery.js, global.css
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/css/global.css" type="text/css" media="screen" />
 --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/common/css/login.css" type="text/css" media="screen" />
<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.md5.js"></script>
<script type="text/javascript">
<!--
	$(function(){	//校验元素ID是否冲突
		checkConflict();
	
		$("#name,#pwd,#verify").focus(function(){
		    $(this).parent().removeClass("error").addClass("focus");
		    $(this).next().find(".lg_tip").removeClass("lg_tip_placeholder");
		}).blur(function(){
		    $(this).parent().removeClass("focus");
		    if($(this).val()==""){$(this).next().find(".lg_tip").addClass("lg_tip_placeholder");}
		}).keypress(function(event){
		    if(event.keyCode == 13)	logIn();
		});
		
		$("#autoLogin").keypress(function(event){
		    if(event.keyCode == 13)	logIn();
		});
		
		<c:if test="${loginStatus == 1}">
			showLogin();	//未登陆时弹出登陆框
		</c:if>
	});
	
	function checkConflict()	{
		var arr1 = new Array("username","hint", "verify", "verifypic",
							"verifyHint", "name","pwd","autoLogin");
		var arr2 = new Array();
		for(i in arr1)	{
			if($("[id=" + arr1[i] + "]").length > 1)
				arr2.push(arr1[i]);
		}
		if(arr2.length > 0)	{
			alert("标签id:" + arr2.join(",") + "已被loginToolbar占用，请更换element的id");
		}
	}
	
	function showLogin()	{
		refreshPic();
		$('#loginDiv').show().find("input:first").focus();
	}
	
	function logIn()	{
		var name = $("#name").val(), pwd = $("#pwd").val(), 
			verify = $("#verify").val(),	
			autoLogin = document.getElementById('autoLogin').checked;
		if(name == '' || pwd=='')	{
			$("#hint").text("请输入用户名和密码");
			$(".lg_password,.lg_name").addClass("error");
			return;
		}
		if(verify == '')	{
			$(".lg_verification").addClass("error");
			$("#verifyHint").text("请输入验证码");
			return;
		}
		var param = {"account":name, "pwd":$.md5(pwd), "autoLogin":autoLogin, "verify":verify};
		$.post("${pageContext.request.contextPath}/passport/login", param, function(data){
			if(data.err == 0)	{
				$('#loginDiv').hide();
				window.location = window.location.href.replace("loginStatus=1", "loginStatus=0");
			} else if(data.err == 4)	{
				$(".lg_password,.lg_name").addClass("error");
				$("#hint").text("该用户还未通过审核");
				$("#pwd").val('');
			} else if(data.err == 1)	{
				$(".lg_verification").addClass("error");
				$("#verifyHint").text("验证码过期，请重新输入");
			} else if(data.err == 2)	{
				$(".lg_verification").addClass("error");
				$("#verifyHint").text("验证码输入错误");
			} else 	{
				$("#hint").text("用户名或密码错误");
				$(".lg_password,.lg_name").addClass("error");
				$("#pwd").val('');
			}
			$("#verify").val('');
			refreshPic();
		},'json');
	}
	
	function refreshPic()	{
		setTimeout(function(){	//使用定时器,防止低版本ie下直接修改img.src时,加载被终止
			var img = $("#verifypic");
			img[0].src = "${pageContext.request.contextPath}/passport/verifypic?type=1&width=" 
					+ img.attr('width') + "&height=" + img.attr('height') + "&_=" + Math.random();
		}, 1);
	}
//-->
</script>

<div class="cg_toolbar">
    <div class="grid_c1">
        <span class="login_wrap">
        <c:choose>
	        <c:when test="${cookie.UID ne null}">
	        	你好，<span id="username"></span> |	<a href="${pageContext.request.contextPath}/passport/logout">注销</a>
				<script type="text/javascript">
					document.getElementById("username").innerHTML = decodeURI("${cookie.UNAME.value}");
				</script>
	        </c:when>
	        <c:otherwise>
	        	<a href="javascript:void(0);" onclick="showLogin()">登录</a>/
	        	<a href="${pageContext.request.contextPath}/passport/register">注册</a>
	        </c:otherwise>
        </c:choose>
       	</span>
    </div>
</div>


<div id="loginDiv" style="display:none">
	<iframe class="thickframe" id="" marginwidth="0" marginheight="0" frameborder="0" scrolling="no"></iframe>
	<div class="thickdiv" id=""></div>
	<div class="thickbox" style="left:35%; top:20%; width:368px;">
	    <div class="thickwrap" style="width:368px;">
	        <div class="thicktitle" id="thicktitler" style="width:368px">
	            <span>登录</span>
	        </div>
	        <div class="thickcon" id="thickconr" style="width: 368px; height: 300px; padding-left: 10px; padding-right: 10px;">
	            <form action="post">
	            <div class="lg_wrap">
	                <!-- 如果出错则加上类error -->
	                <!-- 如果获得焦点则加上类focus -->
	                <div class="lg_name">
	                    <input type="text" id="name" class="input_xl" placeholder="请输入用户名" autocomplete="off" value="${account}"/>
	                    <div class="lg_tip_wrap">
	                    <!-- 
	                        <p class="lg_tip lg_tip_placeholder">请输入用户名</p>
	                     -->
	                        <p class="lg_error"></p>
	                    </div>
	                    <i class="i_name"></i>
	                    <i class="i_error"></i>
	                </div>
	                <!-- 如果出错则加上类error -->
	                <!-- 如果获得焦点则加上类focus -->
	                <div class="lg_password">
	                    <input type="password" id="pwd" class="input_xl" placeholder="请输入密码"/>
	                    <div class="lg_tip_wrap">
	                    <!-- 
	                        <p class="lg_tip lg_tip_placeholder">请输入密码</p>
	                     -->
	                        <p id="hint" class="lg_error"></p>
	                    </div>
	                    <i class="i_password"></i>
	                    <i class="i_error"></i>
	                </div>
	                <div class="lg_verification">
	                    <input type="text" class="input_n" id="verify" autocomplete="off" placeholder="请输入验证码"/> 
                    	<img id="verifypic" width="120" height="40" class="img_vertify" src="${pageContext.request.contextPath}/common/img/border.gif" onclick="refreshPic()"/>
	                    <div class="lg_tip_wrap">
	                        <p id="verifyHint" class="lg_error"></p>
	                    </div>
	                    <i class="i_error"></i>
                	</div>
	                <div class="lg_other">
	                    <input id="autoLogin" class="lg_auto_chk" type="checkbox" />
	                    <label class="lg_auto_lb" for="autoLogin">自动登录</label>
	                    <a class="lg_forget" href="${pageContext.request.contextPath}/passport/forgetPwd" target="_blank">忘记密码</a>
	                </div>
	                <div class="lg_action">
	                    <a class="btn btn_login" href="javascript:void(0);" onclick="logIn()"><span>登录</span></a>
	                    <a class="btn btn_register" href="${pageContext.request.contextPath}/passport/register"><span>注册</span></a>
	                </div>
	            </div>
	            </form>
	        </div>
	        <a href="javascript:void(0);" onclick="$('#loginDiv').hide()" class="thickclose" id="thickcloser">×</a>
	    </div>
	</div>
</div>
