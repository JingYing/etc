<%@ page language="java" pageEncoding="UTF-8"%>
<div class="cg_header">
	<div class="grid_c1">
		<h1 class="title"><a><img src="${pageContext.request.contextPath}/common/img/logo.png"></a></h1>
		<div class="nav">
			<ul id="ul">
				<!-- li class='curr'为选中 -->
                <li><a href="/supplier/index">首页</a></li>
				<li><a href="/supplier/notice_page?type=1">招标预告</a></li>
				<li><a href="/supplier/notice_page?type=2">入围公告</a></li>
				<li><a href="/supplier/notice_page?type=3">中标公告</a></li>
				<li><a href="/supplier/myRequirement">我的投标</a></li>
			</ul>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#ul li a").each(function(){
		if(this.href == window.location)	{
			$(this).parent("li").addClass("curr"); //改变选中状态
		}
	});
</script>
