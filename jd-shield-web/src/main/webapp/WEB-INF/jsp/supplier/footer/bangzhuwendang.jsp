<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>
<title>常见问题</title>
<div class="grid_c1">
	<div class="mod_single">
		<h2 class="mod_single_hd">京东全国招标中心</h2>
		<div class="mod_single_bd">
			<h3 class="qa_q">Q：注册或登陆该网站时，出现异常如何处理？</h3>
			<p class="qa_a">A：您可以发邮件到邮箱 tendering@jd.comd或者直接拨打电话 010-56348982处理。</p>

			<h3 class="qa_q">Q：如何查询网页上的相关信息，为什么有些网页我不能打开？</h3>
			<p class="qa_a">A：网页上部分信息需要注册用户才能打开，请先注册。</br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;有些公告信息，需要相关权限才能打开，注册后，请联系管理员开通相关权限。</p>

			<h3 class="qa_q">Q：如何成为京东的合作供应商？</h3>
			<p class="qa_a">
				A：注册，登陆并提供相关资料，一般注册完毕，资料提交齐全后，3个工作日内，京东会对供应商进行线上审核，如有必要，会启动现场审核。
			</p>
		</div>

	</div>
</div>
