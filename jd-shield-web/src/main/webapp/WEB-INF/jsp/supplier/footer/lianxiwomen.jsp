<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<title>联系我们</title>
<div class="grid_c1">
	<div class="page_map clearfix">
		<div class="map">
			<img src="${pageContext.request.contextPath}/common/images/map.jpg" width="610" height="390" alt="" />
		</div>
		<div class="addr">
			<h2>京东全国招标中心</h2>
			<p>地址：北京市朝阳区北辰西路8号北辰世纪中心A座1519室</p>
			<p>电话：010-56348982、56348998</p>
			<p>邮箱：tendering@jd.com</p>
		</div>
	</div>
</div>
