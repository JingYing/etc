<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ page errorPage="error.jsp"%>
<%-- footer链接页的共用元素 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/common/css/global.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/common/css/index.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/common/js/jquery.min.js"></script>
</head>
<body>
	<jsp:include page="../include/loginToolbar.jsp" />
	<jsp:include page="../include/header.jsp" />
    <div class="cg_content">
    	<jsp:include page="${jsp}.jsp"/>
    	<c:import url="${pageContext.request.contextPath}/adv/bottom"></c:import>
    </div>
	<jsp:include page="../include/footer.jsp" />
</body>
</html>
