<%@page import="com.jd.official.core.utils.DateUtils"%>
<%@page import="com.jd.supplier.model.Notice"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="../../../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../../../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../../../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../common/js/slider.js"></script>
<script type="text/javascript">
function showNoticeList(type){
	if(type==1){
		window.open("/supplier/notice_page?type=1");	
	}else if(type==2){
		window.open("/supplier/notice_page?type=2");
	}else if(type==3){
		window.open("/supplier/notice_page?type=3");
	}
}

function showNoticeDetail(type,id){
	if(type==1){
		window.open("/supplier/bidNotice_detailPage?id="+id);
	}else if(type==2){
		window.open("/supplier/selectedNotice_detailPage?id="+id);
	}else if(type==3){
		window.open("/supplier/winBidNotice_detailPage?id="+id);
	}
}

function showProjectDetail(reqId){
	window.open("/supplier/requirement_viewpage?id="+reqId);
}
</script>
</head>
<body>
	<jsp:include page="../supplier/include/loginToolbar.jsp" />
	<jsp:include page="../supplier/include/header.jsp" />
    <div class="cg_content">
        <!-- banner和公告栏 S -->
        <div class="grid_c2b cg_f1">
            <div class="grid_m">
                <div class="grid_m_inner banner_wrap">
                    <div id="J_slider" class="slider">
                        <ul class="slider_ctn" id="J_sliderCtn">
                        <c:forEach items="${advList}" var="adv">
                            <li><a target="_blank" href="${adv.linkUrl}" title="${adv.linkUrl}">
                            <%--
                            <img data-lazy="${adv.picUrl}" srcs='../common/img/placeholder.png'/>
                             --%>
                            <img src="${adv.picUrl}"/>
                            </a></li>
                        </c:forEach>
                        </ul>
                        <ul class="slider_nav" id="J_sliderNav">
                        <c:forEach items="${advList}" var="adv" varStatus="varStatus">
                        	<li <c:if test="${varStatus.index == 0}"> class="on"</c:if>>
                        		${varStatus.index+1}
                        	</li>
                        </c:forEach>
                        </ul>
                        <span class="slider_btn slider_btn_prev"></span>
                        <span class="slider_btn slider_btn_next"></span>
                    </div>
                </div>
            </div>
            <div class="grid_s cg_notice mod_col">
                <div class="mod_col_hd">
                    <h2 class="title">公告栏</h2>
                </div>
                <div class="mod_col_bd">
                    <ul class="mod_list">
                   				 <%
	                                	List<Notice> bidNoticeList =(List<Notice>)request.getAttribute("bidNoticeList");
		                               	if(bidNoticeList!=null&&bidNoticeList.size()>0){
			                               	int length=0;
			                               	int size=0;
			                               	if(bidNoticeList.size()>0&&bidNoticeList.size()<10){
			                               	    size=bidNoticeList.size();
			                               	}else{
			                               	    size=10;
			                               	}
		                               
                                	    for(int i=0;i<size;i++){
                                		 	Notice notice=(Notice)bidNoticeList.get(i);
                                	%>	
                    
                      						  <li><a href="#" onclick="showNoticeDetail(1,<%=notice.getId()%>)" title="<%=notice.getTitle()%>"><%=notice.getTitle()%></a></li>
                        			 <%
                                		}
                                      }
                                    %>
                        
                    </ul>
                </div>
            </div>
        </div> 
        <!-- banner和公告栏 E -->
        <!-- 招标预告和入围公告 S -->
        <div class="grid_c2 cg_f2">
            <div class="grid_m">
                <div class="grid_m_inner">
                    <div class="mod_col cg_f2_yugao">
                        <div class="mod_col_hd">
                            <h2 class="title">招标预告</h2>
                            <a class="more" href="#" onclick="showNoticeList(1)" >更多</a>
                        </div>
                        <div class="mod_col_bd">
                            <table class="mod_table table_yugao">
                                <thead>
                                    <tr>
                                        <th class="col1">预告标题</th>
                                        <th>预计招标时间</th>
                                        <th>发布时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                	<%
	                                	List<Notice> bidNoticeList1 =(List<Notice>)request.getAttribute("bidNoticeList");
		                               	if(bidNoticeList1!=null&&bidNoticeList1.size()>0){
			                               	int length=0;
			                               	int size=0;
			                               	if(bidNoticeList1.size()>0&&bidNoticeList1.size()<5){
			                               	    size=bidNoticeList1.size();
			                               	}else{
			                               	    size=5;
			                               	}
		                               
                                	    for(int i=0;i<size;i++){
                                		 	Notice notice=(Notice)bidNoticeList1.get(i);
                                	%>	 
                                	
                                   		 <tr>
	                                        <td class="col1"><div class="col1_in"><a href="#" onclick="showNoticeDetail(1,<%=notice.getId()%>)"><%=notice.getTitle()%></a></div></td>
	                                        <td><%=DateUtils.dateFormat(notice.getBiddate())%></td>
	                                        <td><%=DateUtils.dateFormat(notice.getPublishdate())%></td>
                                   		 </tr>
                                    <%
                                		}
                                      }
                                    %>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid_s">
                    <div class="mod_col cg_f2_ruwei">
                        <div class="mod_col_hd">
                            <h2 class="title">入围公告</h2>
                            <a class="more" href="#"  onclick="showNoticeList(2)">更多</a>
                        </div>
                        <div class="mod_col_bd">
                            <table class="mod_table table_ruwei">
                                <thead>
                                    <tr>
                                        <th class="col1">入围标题</th>
                                        <th>发布时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                               <%
	                                	List<Notice> selectedNoticeList =(List<Notice>)request.getAttribute("selectedNoticeList");
		                               	if(selectedNoticeList!=null&&selectedNoticeList.size()>0){
		                               	int length=0;
		                               	int size=0;
			                               	if(selectedNoticeList.size()>0&&selectedNoticeList.size()<5){
			                               	    size=selectedNoticeList.size();
			                               	}else{
			                               	    size=5;
			                               	}
		                               
                                	    for(int i=0;i<size;i++){
                                		 	Notice notice=(Notice)selectedNoticeList.get(i);
                                	%>	 
                                	
                                   		  <tr>
                                       		 <td class="col1"><div class="col1_in"><a href="#" onclick="showNoticeDetail(2,<%=notice.getId()%>)"><%=notice.getTitle()%></a></div></td>
                                       		 <td><%=DateUtils.dateFormat(notice.getPublishdate())%></td>
                                  		  </tr>
                                    <%
                                		}
                                      }
                                    %>
                                	
                                    
                                </tbody>
                            </table> 
                        </div>
                    </div> 
            </div>
        </div>
        <!-- 招标预告和入围公告 E --> 
        <!-- 中标公告 S -->
        <div class="grid_c1">
                   <div class="mod_col cg_f3_zhongbiao">
                        <div class="mod_col_hd">
                            <h2 class="title">中标公告</h2>
                            <a class="more" href="#"  onclick="showNoticeList(3)">更多</a>
                        </div>
                        <div class="mod_col_bd">
                            <table class="mod_table table_zhongbiao">
                                <thead>
                                    <tr>
                                        <th class="col1">中标标题</th>
                                        <th class="col2">中标单位</th>
                                        <th>发布时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                  <%
	                                	List<Notice> winBidNoticeList =(List<Notice>)request.getAttribute("winBidNoticeList");
		                               	if(winBidNoticeList!=null&&winBidNoticeList.size()>0){
			                               	int length=0;
			                               	int size=0;
			                               	if(winBidNoticeList.size()>0&&winBidNoticeList.size()<5){
			                               	    size=winBidNoticeList.size();
			                               	}else{
			                               	    size=5;
			                               	}
		                               
                                	    for(int i=0;i<size;i++){
                                		 	Notice notice=(Notice)winBidNoticeList.get(i);
                                	%>	 
                                	
                                   		  <tr>
	                                        <td class="col1"><div class="col1_in"><a href="#" onclick="showProjectDetail(<%=notice.getRequirementId()%>)"><%=notice.getTitle()%></a></div></td>
	                                        <td class="col2"><div class="col2_in"><a href="#" ><%=notice.getSupplierName()%></a></div></td>
	                                        <td><%=DateUtils.dateFormat(notice.getPublishdate())%></td>
                                   		 </tr>
                                    <%
                                		}
                                      }
                                    %>
                                    
                                </tbody>
                            </table> 
                        </div>
                    </div> 
        </div>
        <!-- 中标公告 E --> 
      	<c:import url="${pageContext.request.contextPath}/adv/bottom"></c:import>
      
    </div>

	
	<jsp:include page="../supplier/include/footer.jsp" />








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>
