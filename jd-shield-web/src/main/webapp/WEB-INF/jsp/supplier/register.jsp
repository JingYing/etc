<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://zb.jd.com/customfunc" prefix="fnx" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<%-- <base href="<%=basePath%>"> --%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	<title>京东采购_京盾招投标_首页</title>
	<link rel="stylesheet" href="http://misc.360buyimg.com/lib/skin/2013/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/global.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/findpassword.css" type="text/css" media="screen" />
	<script type="text/javascript" src="../common/js/jquery.min.js"></script>
	<script type="text/javascript" src="../common/js/jquery.md5.js"></script>
	<script type="text/javascript" src="../common/js/slider.js"></script>
	<script type="text/javascript">
		$(function(){
		    
		    $(".form_item .input_wrap input").focus(function(){
				$(this).parent().find(".form_item").addClass("focus");
				//$(this).removeClass("placeholder").val("");
		    });
		    
		    $(".form_item .input_wrap input").blur(function(){
				$(this).parent().find(".form_item").removeClass("focus");
		    });
		    
		    initcheckForm(true);
		    
		    <c:if test="${form.isVerifyErr == true}">
		    	validateCallback($("#verify"), false);
		    </c:if>
		});
		
		var noticeBlank = true;	//是否要处理值为空的情况
		function initcheckForm(ignoreBlank)	{
			$("#name").blur(function(){
		    	if(noticeBlank && this.value == '')	return;
		    	validateCallback(this, this.value != '');
		    });
		    
		    $("#account").blur(function(){
		    	var thisEle = this;
		    	if(validateCallback(this, /^.{3,20}$/i.test(this.value)))	{
			    	$("#loading").toggle();
			    	$.get("${pageContext.request.contextPath}/passport/checkAccount?account=" + this.value, function(data){
			    		$("#account_hint").html("该账号已被使用");
			    		validateCallback(thisEle, data == "0");
			    		$("#loading").toggle();
			    	});
		    	} else	{
		    		$("#account_hint").html("3-20个字母或数字(不区分大小写)");
		    	}
		    });
		    
		    $("#pwd").blur(function(){
		    	if(noticeBlank && this.value == '')	return;
		    	validateCallback(this, /^.{6,20}$/i.test(this.value));
		    });
		    
		    $("#pwd2").blur(function(){
		    	var pwd = $("#pwd").val();
		    	if(noticeBlank && (pwd == '' || this.value == ''))	return;
		    	validateCallback(this, this.value == pwd);
		    });
		    
		    $("#mobile").blur(function(){
		    	if(noticeBlank && this.value == '')	return;
		    	validateCallback(this, /^1\d{10}$/i.test(this.value));
		    });
		    
		    $("#email").blur(function(){
		    	if(noticeBlank && this.value == '')	return;
		    	validateCallback(this, /^.+@.+[.]\D+$/i.test(this.value));
		    });
		    $("#verify").blur(function(){
		    	if(noticeBlank && this.value == '')	return;
		    	validateCallback(this, this.value != '');
		    });
		    refreshPic();
		}
		
		/**
		 * input校验后改变样式
		 * @param inputEle 输入框
		 * @param isSuccess 输入是否有效
		 * @return isSuccess
		 */
		function validateCallback(inputEle, isSuccess)	{
			var div = $(inputEle).parents(".form_item").removeClass("error right");
			if(isSuccess)	div.addClass("right");
			else			div.addClass("error");
			return isSuccess;
		}
		
		function submitReg()	{
			noticeBlank = false;
			$("#name,#account,#pwd,#pwd2,#mobile,#email,#verify").blur();
			noticeBlank = true;
			
			var isError = false;
			$(".form_item").each(function(){
				if(!$(this).hasClass("right"))	{
					isError = true;
					return false;
				}
			});
			
			if(!isError)	{
				$("#submitBtn").css({color:'grey'}).removeAttr('onclick');
				$("#form").append("<input type=\"hidden\" name=\"pwd\" value=\"" + $.md5($('#pwd').val()) + "\"/>").submit();
			}	
		}
		
		function refreshPic()	{
			setTimeout(function(){	//使用定时器,防止低版本ie下直接修改img.src时,加载被终止
				var img = $("#verifypic");
				img[0].src = "${pageContext.request.contextPath}/passport/verifypic?type=2&width=" 
						+ img.attr('width') + "&height=" + img.attr('height') + "&_=" + Math.random();
			}, 1);
		}
	</script>
</head>
<body> 
	<div class="cg_header">
		<div class="grid_c1">
			<h2 class="title"><a href="#" title="京东采购"><img src="../common/img/logo.png" alt="京东采购"/></a></h2> 
		</div>
	</div>
    <div class="cg_content"> 
        <div class="grid_c1">
            <div class="mod_page">
                <h2 class="mod_page_hd">注册</h2>
                <div class="mod_page_bd">
                    <div class="mod_form">
                    <form id="form" action="doRegister" method="post">
                        <!-- 企业名称 -->
                        <!-- 获得焦点则给加上类focus; 正确给 form_item 加上类right ;错误则加上类error; -->
                        <div class="form_item">
                            <label class="item_lb" for=""><span class="cg_strong">*</span>企业名称：</label>
                            <span class="input_wrap">
                                <input id="name" name="name" class="input_xxl company" type="text" placeholder="请输入企业名称" autocomplete="off" value="${fnx:escapeHtml3(form.name)}"/>
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                        </div><!-- / 企业名称 -->
                        <!-- 登录账号 -->
                        <div class="form_item">
                            <label class="item_lb" for=""><span class="cg_strong">*</span>登录账号：</label>
                            <span class="input_wrap">
                                <input id="account" name="account" class="input_xxl account" type="text" placeholder="3-20个字母或数字(不区分大小写)" autocomplete="off" value="${fnx:escapeHtml3(form.account)}"/>
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <img width=20 height=20 src="../common/img/loading.gif" id="loading" style="display:none"/>
                            <p class="p_tip"></p>
                            <p class="p_error" id="account_hint"></p>
                        </div><!-- / 登录账号 -->
                        <!-- 密码 -->
                        <div class="form_item">
                            <label class="item_lb" for=""><span class="cg_strong">*</span>密码：</label>
                            <span class="input_wrap">
                                <input id="pwd" class="input_xxl password" type="password" placeholder="6-20个字母或数字(区分大小写)"/>
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <p class="p_tip"></p>
                            <p class="p_error">6-20个字母或数字(区分大小写)</p>
                        </div><!-- / 密码 -->
                        <!-- 确认密码 -->
                        <div class="form_item">
                            <label class="item_lb" for=""><span class="cg_strong">*</span>确认密码：</label>
                            <span class="input_wrap">
                                <input id="pwd2" class="input_xxl repassword" type="password" placeholder="请再次输入密码"/>
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <p class="p_tip"></p>
                            <p class="p_error">两次密码输入不一致</p>
                        </div><!-- / 确认密码 -->
                        <!-- 手机联系号码 -->
                        <div class="form_item">
                            <label class="item_lb" for=""><span class="cg_strong">*</span>手机号码：</label>
                            <span class="input_wrap">
                                <input id="mobile" name="mobile" class="input_l phone" type="text" placeholder="请填写手机号码" autocomplete="off" value="${fnx:escapeHtml3(form.mobile)}"/>
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <p class="p_tip"></p>
                            <p class="p_error">无效的手机号码</p>
                        </div><!-- / 手机联系号码 -->
                        <!-- 邮箱 -->
                        <div class="form_item">
                            <label class="item_lb" for=""><span class="cg_strong">*</span>邮箱：</label>
                            <span class="input_wrap">
                                <input id="email" name="email" class="input_xxl" type="text" autocomplete="off" value="${fnx:escapeHtml3(form.email)}"/>
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
                            <p class="p_tip"></p>
                            <p class="p_error">无效的邮箱格式</p>
                        </div><!-- / 邮箱 -->
                        <div class="form_item">
                            <label class="item_lb"><span class="cg_strong">*</span>验证码：</label>
                            <span class="input_wrap">
			                    <input id="verify" name="verify" type="text" class="input_n" autocomplete="off" placeholder="请输入验证码"/> 
                                <i class="i_icon i_right"></i>
                                <i class="i_icon i_error"></i>
                            </span>
	                    	<img id="verifypic" style="cursor:pointer;" width="120" height="40" onclick="refreshPic()" src="${pageContext.request.contextPath}/common/img/border.gif"/>
                            <p class="p_tip"></p>
                            <p class="p_error">请输入正确的验证码</p>
                        </div>
                        <!-- 
                        <div class="lg_verification">
		                    <div class="lg_tip_wrap">
		                        <p id="verifyHint" class="lg_error"></p>
		                    </div>
	                    <i class="i_error"></i>
	                	</div>
                         -->
                        <div class="form_action">
                            <a id="submitBtn" class="btn_submit" href="javascript:void(0);" onclick="submitReg()"><span>提交</span></a>
                        </div>
                    </form>
                         
                    </div>
                </div><!-- / fp_bd -->
            </div> <!-- / findpassword -->
        </div><!-- grid_c1 -->
    </div><!-- cg_content -->

    <%@include file="include/footer.jsp" %>

</body>
</html>
