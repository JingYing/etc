<%@page import="com.jd.official.core.utils.DateUtils"%>
<%@page import="com.jd.supplier.model.TaskSuppSelection"%>
<%@page import="com.jd.common.util.PaginatedList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_我的投标_首页</title>
<link rel="stylesheet" href="../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../common/js/slider.js"></script>

<!-- 
<link href="../static/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../static/datatables/css/TableTools.css" rel="stylesheet" type="text/css"/>
<link href="../static/datatables/css/jquery.dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
 -->

<script type="text/javascript" src="../static/jquery/jquery.1.8.2.js"></script>
<script type="text/javascript" src="../static/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="../static/bootstrap/js/bootbox.js"></script>
<script type="text/javascript" src="../static/bootstrap/js/Dialog.js"></script>
<script type="text/javascript" src="../static/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../static/datatables/js/TableTools.min.js"></script>
<script type="text/javascript" src="../static/datatables/js/jquery.dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../static/datatables/js/ColReorderWithResize.js"></script>
<script type="text/javascript">

function showProjectDetail(reqId){
	window.open("/supplier/requirement_viewpage?id="+reqId);
}

function query(){
	document.getElementById("searchForm").submit();
}

function pageSubmit(page){
	$("#page").val(page);
	$("#searchProjectName").val("");
	$("#searchForm").submit();
}
</script>
</head>
<body class="ic_ie6">
    <jsp:include page="../supplier/include/loginToolbar.jsp" />
	<jsp:include page="../supplier/include/header.jsp" />
    <div class="cg_content">
        <!-- 招标预告和公告栏 S -->
        <div class="grid_c2a page_wotb">
            <div class="grid_m">
                <div class="grid_m_inner">
                
                <%-- 供应商基本资料.开始 --%>
                 <div class="brand">
                        <div class="brand_name">
                            <h2>${supplier.name}</h2>
                        </div>
                        <div class="brand_info">
                            <div class="brand_info_row">
                                <div class="brand_info_col">
                                    <span class="tit">联系人：</span>${supplier.contacts[0].contactor}
                                </div>
                                <div class="brand_info_col">
                                    <span class="tit">资料完整度：</span>
                                    <!-- 100%则加类full变成绿色 -->
                                    <span class="wrap_mod_stock <c:if test="${supplier.dataIntegrity == '100'}">full</c:if>"> 
                                    <span class="mod_stock"><i class="mod_stock_in" style="width:${supplier.dataIntegrity}%"></i>
                                    </span>${supplier.dataIntegrity}%</span>
                                </div>
                            </div>
                            <div class="brand_info_row">
                                <div class="brand_info_col">
                                    <span class="tit">联系电话：</span>
                                    <span class="">${supplier.mobile}</span>
                                </div>
                                <div class="brand_info_col">
                                    <span class="tit">审核状态：</span>
                                    <span class="">${supplier.supplierStatus eq "0" ? "未通过" : "已通过"}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <%-- 供应商基本资料.结束--%>
                    
                    
                   <div class="mod_col">
                    <form action="/supplier/myRequirement" id="searchForm">
                        <div class="mod_col_hd">
                            <h2 class="title">我的投标项目</h2>
                            <span class="search">
                               
                                <input class="input_search" type="text" name="searchProjectName" id="searchProjectName" value="请输入关键词" />
                                <a class="btn_normal_m" href="#" onclick="query()"><span>搜索</span></a>
                                
                            </span>
                        </div>
                        <div class="mod_col_bd">
                            <div class="search_rang">
                                <select id="time" name="time">
                                	<option value="">请选择</option>
                                    <option value="1">一周内</option>
                                    <option value="2">一月内</option>
                                </select>
                            </div>
                           
                            <table class="mod_table table_my_toubiao" id="resultTable">
                                <thead>
                                    <tr>
                                        <th width="10%">序号</th>
                                        <th width="20%">项目</th>
                                        <th width="20%">投标单位</th>
                                        <th width="10%">报名日期</th>
                                        <th width="10%">状态</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                
                                <%
                                			PaginatedList <TaskSuppSelection> list=(PaginatedList)request.getAttribute("taskSuppSelectionList");
                                			for( int i=0;i<list.size();i++){
                                			    TaskSuppSelection taskSuppSelection =(TaskSuppSelection)list.get(i);
                                	%>
                                
                                    
                                    <tr>
                                        <td width="10%"><%=(i+1)%></td>
                                        <td width="20%"><div class="col2_in"><a href="#" onclick="showProjectDetail(<%=taskSuppSelection.getRequirementId()%>)"><%= taskSuppSelection.getProjectName()%></a></div></td>
                                        <td width="20%"><div class="col2_in"><a href="#" ><%= taskSuppSelection.getEnterpriseName()%></a></div></td>
                                        <td><%=DateUtils.dateFormat(taskSuppSelection.getCreateTime())%></td>
                                        <td><%if(taskSuppSelection.getFinalFlag()!=null&&!taskSuppSelection.getFinalFlag().equals("")){
                                            	if(taskSuppSelection.getFinalFlag()==0){
                                            	out.print("初选!");
                                            	}else{
                                            	    out.print("入围");
                                            	}
                                            
                                        }%></td>
                                    </tr>
                                   
                                    <%
                                    }
                                    %>
                                   
                                </tbody>
                            </table>
                            <%
                            	//总页数为0时不显示分页结果
                            	if(list.getTotalPage()!=0){
                            %>
                             <div class="page_wrap">
                                <div class="page_navigater">
                                    <span class="page_num">
                                        <strong class="cg_strong"><%=list.getIndex()%></strong><span>/<%=list.getTotalPage() %></span>
                                        <input type="hidden" name="page" id="page" value="1"/>
                                    </span>
                                    <%
                                    //只有一页  点击上一页超链接为当前页
                                    if(list.getIndex()==1){
                                	%>
                                	 <a class="page_prev page_prev_disabled" href="#" ><span>上一页</span></a>
                                	<%
                                    	}else{
                                    %>
                                    	 <a class="page_prev page_prev_disabled" href="#" onclick="pageSubmit(<%=list.getIndex()-1 %>)" ><span>上一页</span></a>
                                    	    <%
                                    	}
                                    %>
                                    
                                   <%
                                   	if(list.getTotalPage()==list.getIndex()){
                                   	    %>
                                   	      <a class="page_next"  href="#" ><span>下一页</span></a>
                                   	    <%
                                   	}else{
                                   	    %>
                                   	    <a class="page_next"  href="#" onclick="pageSubmit(<%=list.getIndex()+1 %>)"><span>下一页</span></a>
                                   	    <%
                                   	}
                                   %>
                                   
                                    
                                </div> 
                            </div>
                            <%} %>
                        </div>
                    </div>
                   </form>  
                   
                   
                </div>
            </div>
            <jsp:include page="../supplier/include/left.jsp" />
        </div> 
        <!-- 招标预告和公告栏 E --> 
    </div>

	
	<jsp:include page="../supplier/include/footer.jsp" />








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>

