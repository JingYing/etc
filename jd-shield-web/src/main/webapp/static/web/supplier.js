$(function (){
	
	$('#uploadify1').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName1").val(strOne[0]);
						$("#fileKey1").val(strTwo[0]);
 						$("#result1").attr('href', strTwo[0]);
 						$("#result1").text(strOne[0]);
 						$("#result2").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
		
	$('#uploadify2').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName2").val(strOne[0]);
						$("#fileKey2").val(strTwo[0]);
						$("#result3").attr('href', strTwo[0]);
 						$("#result3").text(strOne[0]);
 						$("#result4").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
	$('#uploadify3').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName3").val(strOne[0]);
						$("#fileKey3").val(strTwo[0]);
						$("#result5").attr('href', strTwo[0]);
 						$("#result5").text(strOne[0]);
 						$("#result6").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
	$('#uploadify4').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName4").val(strOne[0]);
						$("#fileKey4").val(strTwo[0]);
						$("#result7").attr('href', strTwo[0]);
 						$("#result7").text(strOne[0]);
 						$("#result8").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
	$('#uploadify5').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName5").val(strOne[0]);
						$("#fileKey5").val(strTwo[0]);
						$("#result9").attr('href', strTwo[0]);
 						$("#result9").text(strOne[0]);
 						$("#result10").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
	$('#uploadify6').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName6").val(strOne[0]);
						$("#fileKey6").val(strTwo[0]);
						$("#result11").attr('href', strTwo[0]);
 						$("#result11").text(strOne[0]);
 						$("#result12").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
	$('#uploadify7').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName7").val(strOne[0]);
						$("#fileKey7").val(strTwo[0]);
						$("#result13").attr('href', strTwo[0]);
 						$("#result13").text(strOne[0]);
 						$("#result14").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
	$('#uploadify8').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName8").val(strOne[0]);
						$("#fileKey8").val(strTwo[0]);
						$("#result15").attr('href', strTwo[0]);
 						$("#result15").text(strOne[0]);
 						$("#result16").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
	$('#uploadify9').uploadify(
			{
			    debug     : false, //开启调试    
		        auto      :true,  //是否自动上传    
		        buttonText:'选择文件',  //按钮上的文字  
		        swf       : "/static/uploadify3.2.1/uploadify.swf",//flash      
		        queueID   :'fileQueue',  //文件选择后的容器ID    
		        uploader  :"/servlet/fileUpload",  //servlet  
		        width     :'75',  //按钮宽度  
		        height    :'24',  //按钮高度  
		        multi     :false,  //是否支持多文件上传，默认就是true  
		        fileObjName:'fileData',//后台接收的参数，如：使用struts2上传时，后台有get,set方法的接收参数  
		        fileTypeExts:'*.*',  //允许上传文件的类型  
		        fileSizeLimit: 100*1024*1024,  //文件上传的最大大小  
		        removeCompleted : true,//上传成功取消进度条
		        removeTimeout:1,    
		        onUploadSuccess      : function(file, data, response) {	//每个文件上传后的回调函数
			    	var json = eval("(" + data + ")");
			    	var strOne;
			    	var strTwo;
					if(json != 0){
						strOne = json.fileName.split(',');
						strTwo = json.fileKey.split(',');
						$("#fileName9").val(strOne[0]);
						$("#fileKey9").val(strTwo[0]);
						$("#result17").attr('href', strTwo[0]);
 						$("#result17").text(strOne[0]);
 						$("#result18").remove();
					} else	{
						alert("上传失败");
					}
		        }
			});
});
